#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  get_stimulus.pl
#
#        USAGE:  ./get_stimulus.pl  
#
#  DESCRIPTION:  VCD Parser
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Debjit Pal, dpal2@illinois.edu
#      COMPANY:  University of Illinois at Urbana-Champaign
#      VERSION:  1.0
#      CREATED:  03/03/2015 10:23:12 AM
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;
use autodie;
use File::Find;
use String::Util qw(trim);
use Getopt::Long;
use Path::Tiny qw(path);
use IO::Handle;
use Array::Utils qw(:all);

my $file	= "architecture.vcd";
my $verbose = '';
my @words;
my @signal_symbol;
my $chars="10b";

my %vcd_value_hash = ();
my %vcd_signal_hash = ();
my %vcd_bus_hash = ();
my %vcd_complete_hier_signal_hash = ();

#print "1 - Hash Size: is $size\n";


GetOptions (	"file=s"	=>	\$file,	#string
   		"verbose"	=> 	\$verbose)	#flag
or die("Error in command line arguments\n");
print "The file name received: $file\n";

my $curr_time;
my @module_hierarchy;
my $file_handle = path($file)->openr;
while ( my $line = $file_handle->getline() )  {
	# This part looks for the module name hierarchy in the VCD file so that we can supply the signal values of some internal signal as well
	if ( $line =~ m/\bscope\s/i) {
		@words = split(/ /, $line);
		push(@module_hierarchy, $words[2]);
	}
	elsif ( $line =~ m/\bupscope\s/i ) {
		pop @module_hierarchy;
	}
	# This part looks for the exact string var
	elsif( $line =~ m/\bvar\s/i ) {
		#print "@module_hierarchy\n";
		#print "$line\n";
		@words = split(/ /, $line);
		push(@signal_symbol, $words[3]);
		$vcd_complete_hier_signal_hash{$words[3]} = join(".", join(".", @module_hierarchy), $words[4]);
		#print "Signal Name: $words[4], Handle Name: $words[3]\n";
		chomp($words[3]);
		if( $vcd_value_hash{$words[3]} ) {
			next;
		}
		else {
			#print "#$words[3]#\n";
			$vcd_value_hash{$words[3]} = '';
			$vcd_signal_hash{$words[3]} = $words[4];
			$vcd_bus_hash{$words[3]} = $words[2];
			#print "Initiated: $vcd_value_hash{$words[3]}\n";
		}
		my @keys = keys % vcd_value_hash;
		my $size = @keys;
		#print "1 - Hash Size: is $size\n";
	}
	elsif ( $line =~ m/^[$chars]/ ) {
		if( $line =~  m/^b/ ) {
			my @line_content = split(/ /, $line);
			chomp($line_content[1]);
			#print "#$line_content[1]#\n";
			#print "Entered\n";
			my $content = $vcd_value_hash{$line_content[1]};
			$content = join(',',$content, "$curr_time:$line_content[0]");
			$vcd_value_hash{$line_content[1]} = $content;
			#print "1 - Symbol: $line_content[1], Content: $content\n";
		}
	}
	elsif ( $line =~ m/^#/ ) {
		$curr_time = $line;
		chomp($curr_time);
		#print "Current Time: $curr_time\n";
	}
}

my $signal_size = @signal_symbol;
for (my $i = 0; $i < $signal_size; $i++) {
	#print "Signal Name: $vcd_signal_hash{$signal_symbol[$i]}\nSignal Handle: $signal_symbol[$i]\n$vcd_value_hash{$signal_symbol[$i]}\n\n";
}

my $verilog_filename = 'trace_signal_stim.v';
$curr_time = 0;
my $prev_time = 0;
open(my $fh, '>', $verilog_filename) or die "Could not open file '$verilog_filename' $!";
for (my $i = 0; $i < $signal_size; $i++) {
	print $fh "initial begin\n";
	my @data_content = split(/,/, $vcd_value_hash{$signal_symbol[$i]});
	my $bus_size = $vcd_bus_hash{$signal_symbol[$i]};
	my $signal_name = $vcd_complete_hier_signal_hash{$signal_symbol[$i]};
	foreach my $data (@data_content) {
		my @data_val = split(/:/, $data);
		if(not defined $data_val[0] or not defined $data_val[1]) {
			next;
		}
		else {
			chomp($data_val[0]);
			chomp($data_val[1]);
			#$data_value[0] =~ s/#/g;
			$curr_time = $data_val[0];
			$curr_time =~ s/#//g;
		}
		my $time_string = $curr_time - $prev_time;
		print $fh "\t#$time_string $signal_name $bus_size'$data_val[1];\n";
		$prev_time = $curr_time;
	}
	print $fh "end\n\n";
	$curr_time = 0;
	$prev_time = 0;
	$signal_name = '';
}
close $fh;

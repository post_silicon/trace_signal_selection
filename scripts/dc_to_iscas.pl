#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: dc_to_iscas.pl
#
#        USAGE: ./dc_to_iscas.pl  
#
#  DESCRIPTION: This script takes a Synopsys DC Compiler netlist and converts it into
#               a ISCAS bench format. 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Debjit Pal ( dpal2@illinois.edu ) 
# ORGANIZATION: University of Illinois at Urbana-Champaign
#      VERSION: 1.0
#      CREATED: 03/05/15 12:57:15
#     REVISION: 1) Trying to consolidate different NOR, NAND, AND, OR gates in the
#                  single for loop for each kind of gate in the branch dpal2
#===============================================================================

use strict;
use warnings;
use utf8;
use autodie;
use File::Find;
use String::Util qw(trim);
use Getopt::Long;
use Path::Tiny qw(path);
use IO::Handle;
use Array::Utils qw(:all);
use 5.010;

# Declaration of my own functions
sub mytrim($);
sub stripchars;

my $file = "dc_netlist.v";
my $top = "top";
my $verbose = '';
my $chars=" /[";

GetOptions (    "file=s"    => \$file,  # File name
                "top=s"     => \$top,   # Name of the top module
                "verbose"   => \$verbose) #flag
or die ("Errior in command line arguments\n");
print "The Synopsys Design Compiler File received: $file\n";

# Module related details
my $module_name;
my @input_vector;
my @output_vector;

# Logic elements related details
my @dff_vector;
my @nand_vector;
my @and_vector;
my @nor_vector;
my @or_vector;
my @not_vector;

## Flags
my $module_found = 0;

my $file_handle = path($file)->openr;
while ( my $line = $file_handle->getline() ) {
    # Module Name
    if( $line =~ m/\bmodule\s/i ) {
        my @words = split(/ /, $line);
        $module_name = $words[1];
        #print "Module Name Found: $module_name\n";
        if($module_name ne $top) {
            #print "UNEQUAL\n";
            $module_found = 0;
            next;
        }
        else {
            $module_found = 1;
        }
    }
    elsif ( $module_found == 0) {
        next;
    }
    # Input ports
    elsif ( $line =~ m/\binput\s/i ) {
        my $vector_sig_names;
        my @words = split(/\s{1,}/, $line);
        #print "#$words[0]#, #$words[1]#, #$words[2]#, #$words[3]#\n";
        if(index($words[2], "[") != -1) {
            #printf "Vector input seen.\n";
            my @vector_size = split(/:/, $words[2]);
            chomp($vector_size[0]);
            $vector_size[0] =~ s/\[//g;
            $vector_size[0] = $vector_size[0] + 1;
            #print "Vector Size: $vector_size[0]\n";
            $words[3] =~ s/\;//g;
            #print "$words[3]\n";
            for(my $i = 0; $i < $vector_size[0]; $i++) {
                push(@input_vector, join("", $words[3], $i));
            }
        }
        else {
            #printf "Non-Vector inout seen\n";
            my $index_val = index($line, "input");
            $index_val = $index_val + length("input");
            $vector_sig_names = substr($line, $index_val);
            push(@input_vector, split(/,/, $vector_sig_names));
            #print @input_vector;
        }
    }
    #Output Ports
    elsif ( $line =~ m/\boutput\s/i ) {
        my $vector_sig_names;
        my @words = split(/\s{1,}/, $line);
        #print "#$words[0]#, #$words[1]#, #$words[2]#, #$words[3]#\n";
        if(index($words[2], "[") != -1) {
            #printf "Vector output seen.\n";
            my @vector_size = split(/:/, $words[2]);
            $vector_size[0] =~ s/\[//g;
            chomp($vector_size[0]);
            $vector_size[0] = $vector_size[0] + 1;
            #print "Vector Size: $vector_size[0]\n";
            $words[3] =~ s/\;//g;
            #print "$words[3]\n";
            for(my $i = 0; $i < $vector_size[0]; $i++) {
                push(@output_vector, join("", $words[3], $i));
            }
        }
        else {
            #printf "Non-Vector Output seen\n";
            my $index_val = index($line, "output");
            $index_val = $index_val + length("output");
            $vector_sig_names = substr($line, $index_val);
            push(@output_vector, split(/,/, $vector_sig_names));
            #print @output_vector;
        }

    }
    # DFF
    elsif ( $line =~ m/\bDFFR_X1\s/i ) {
        my $next_line = '';
        my @inputs;
        my @outputs;
        chomp($line);
        my @dff_config= (".D(", ".CK(", ".RN(", ".Q(", ".QN(");
        $line = mytrim($line);
        my @DFF_Instantiation_Name = split(/ /, $line);
        #print "DFFR_X1 FOUND.\n";
        if( index($line, ";") == -1) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        #print "Concatenated Line: #$line#\n";
        for(my $i = 1; $i <= 3; $i++) {
            my $in_index_start = index($line, $dff_config[$i-1]); my $in_index_stop = index(substr($line, $in_index_start + length($dff_config[$i-1])), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length($dff_config[$i-1]), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $in . "\n";
            }
            push(@inputs, $in);
        }
        for(my $i = 4; $i <= 5; $i++) {
            my $out_index_start = index($line, $dff_config[$i-1]); 
            if($out_index_start == -1) {
                next;
            }
            my $out_index_stop = index(substr($line, $out_index_start + length($dff_config[$i-1])), ")");
            #print "$out_index_start, $out_index_stop\n";
            my $out = substr($line, $out_index_start + length($dff_config[$i-1]), $out_index_stop); $out = mytrim($out); 
            if(index($out, "/") != -1 or index($out, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
            }
            push(@outputs, $out);
        }
        my $dff_string = "$outputs[0] = DFF($inputs[0]) \t\t# $DFF_Instantiation_Name[1]\n";
        if(defined $outputs[1]) {
            my $not_string = "$outputs[1] = NOT($outputs[0])\t\t# QN of the Q of D Flipflop $DFF_Instantiation_Name[1]\n";
            push(@not_vector, $not_string );
        }
        push(@dff_vector, $dff_string );
    }
    # End of DFF
    elsif ( $line =~ m/\bTINV_X1\s/i ) {
        my $next_line = '';
        chomp($line);
        #print "TINV_X1 FOUND.\n";
        if( index($line, ";") == -1 ) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
    }
    elsif ( $line =~ m/\bTINV_X1\s/i ) {
        my $next_line = '';
        chomp($line);
        #print "TINV_X1 FOUND.\n";
        if( index($line, ";") == -1 ) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
    }
    # NAND2
    elsif ( $line =~ m/\bNAND2_X2\s/i ) {
        my $next_line = '';
        my @inputs;
        my $out = '';
        chomp($line);
        $line = mytrim($line);
        #print "NAND2_X2 FOUND.\n";
        if( index($line, ";") == -1 ) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        for(my $i = 1; $i <= 2; $i++) {
            my $in_index_start = index($line, ".A$i("); my $in_index_stop = index(substr($line, $in_index_start + length(".A$i(")), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length(".A$i("), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $out . "\n";
            }
            push(@inputs, $in);
        }
        my $out_index_start = index($line, ".ZN("); my $out_index_stop = index(substr($line, $out_index_start + length(".ZN(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $out = substr($line, $out_index_start + length(".ZN("), $out_index_stop); $out = mytrim($out);
        if(index($out, "/") != -1 or index($out, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
        }
        my $nand_string =  $out . " = NAND(" .  join(",", @inputs) . ")\n";
        push(@nand_vector, $nand_string );
    }
    # End of NAND2
    elsif ( $line =~ m/\bNAND2_X1\s/i ) {
        my $next_line = '';
        my @inputs;
        my $out = '';
        chomp($line);
        $line = mytrim($line);
        #print "NAND2_X1 FOUND.\n";
        if( index($line, ";") == -1) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        for(my $i = 1; $i <= 2; $i++) {
            my $in_index_start = index($line, ".A$i("); my $in_index_stop = index(substr($line, $in_index_start + length(".A$i(")), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length(".A$i("), $in_index_stop); $in = mytrim($in); 
             if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $out . "\n";
            }
            push(@inputs, $in);
        }
        my $out_index_start = index($line, ".ZN("); my $out_index_stop = index(substr($line, $out_index_start + length(".ZN(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $out = substr($line, $out_index_start + length(".ZN("), $out_index_stop); $out = mytrim($out);
        if(index($out, "/") != -1 or index($out, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
        }
        my $nand_string =  $out . " = NAND(" .  join(",", @inputs) . ")\n";
        push(@nand_vector, $nand_string );
    }
    elsif ( $line =~ m/\bNAND3_X1\s/i ) {
        my $next_line = '';
        my @inputs;
        my $out = '';
        chomp($line);
        $line = mytrim($line);
        #print "NAND3_X1 FOUND.\n";
        if( index($line, ";") == -1) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        for(my $i = 1; $i <= 3; $i++) {
            my $in_index_start = index($line, ".A$i("); my $in_index_stop = index(substr($line, $in_index_start + length(".A$i(")), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length(".A$i("), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $out . "\n";
            }
            push(@inputs, $in);
        }
        my $out_index_start = index($line, ".ZN("); my $out_index_stop = index(substr($line, $out_index_start + length(".ZN(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $out = substr($line, $out_index_start + length(".ZN("), $out_index_stop); $out = mytrim($out);
        if(index($out, "/") != -1 or index($out, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
        }
        my $nand_string =  $out . " = NAND(" .  join(",", @inputs) . ")\n";
        push(@nand_vector, $nand_string );

    }
    elsif ( $line =~ m/\bNAND4_X2\s/i ) {
        my $next_line = '';
        my @inputs;
        my $out;
        chomp($line);
        $line = mytrim($line);
        #print "NAND4_X2 FOUND.\n";
        if( index($line, ";") == -1) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        for(my $i = 1; $i <= 4; $i++) {
            my $in_index_start = index($line, ".A$i("); my $in_index_stop = index(substr($line, $in_index_start + length(".A$i(")), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length(".A$i("), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $out . "\n";
            }
            push(@inputs, $in);
        }
        my $out_index_start = index($line, ".ZN("); my $out_index_stop = index(substr($line, $out_index_start + length(".ZN(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $out = substr($line, $out_index_start + length(".ZN("), $out_index_stop); $out = mytrim($out);
        if(index($out, "/") != -1 or index($out, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
        }
        my $nand_string =  $out . " = NAND(" .  join(",", @inputs) . ")\n";
        push(@nand_vector, $nand_string );
    }
    elsif ( $line =~ m/\bAND2_X1\s/i ) {
        my $next_line = '';
        my @inputs;
        my $out = '';
        chomp($line);
        $line = mytrim($line);
        #print "AND2_X1 FOUND.\n";
        if( index($line, ";") == -1) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        for(my $i = 1; $i <= 2; $i++) {
            my $in_index_start = index($line, ".A$i("); my $in_index_stop = index(substr($line, $in_index_start + length(".A$i(")), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length(".A$i("), $in_index_stop); $in = mytrim($in); 
             if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $out . "\n";
            }
            push(@inputs, $in);
        }
        my $out_index_start = index($line, ".ZN("); my $out_index_stop = index(substr($line, $out_index_start + length(".ZN(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $out = substr($line, $out_index_start + length(".ZN("), $out_index_stop); $out = mytrim($out);
        if(index($out, "/") != -1 or index($out, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
        }
        my $and_string =  $out . " = AND(" .  join(",", @inputs) . ")\n";
        push(@and_vector, $and_string );
    }
    elsif ( $line =~ m/\bAND3_X1\s/i ) {
        my $next_line = '';
        my @inputs;
        my $out = '';
        chomp($line);
        $line = mytrim($line);
        #print "AND3_X1 FOUND.\n";
        if( index($line, ";") == -1 ) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        for(my $i = 1; $i <= 3; $i++) {
            my $in_index_start = index($line, ".A$i("); my $in_index_stop = index(substr($line, $in_index_start + length(".A$i(")), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length(".A$i("), $in_index_stop); $in = mytrim($in);
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $out . "\n";
            }
            push(@inputs, $in);
        }
        my $out_index_start = index($line, ".ZN("); my $out_index_stop = index(substr($line, $out_index_start + length(".ZN(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $out = substr($line, $out_index_start + length(".ZN("), $out_index_stop); $out = mytrim($out);
        if(index($out, "/") != -1 or index($out, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
        }
        my $and_string =  $out . " = AND(" .  join(",", @inputs) . ")\n";
        push(@and_vector, $and_string );
    }
    elsif ( $line =~ m/\bINV_X4\s/i ) {
        my $next_line = '';
        my $in = '';
        my $out = '';
        chomp($line);
        $line = mytrim($line);
        #print "#$line#";
        #print "INV_X4 FOUND.\n";
        if( index($line, ";") == -1 ) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        my $in_index_start = index($line, ".A("); my $in_index_stop = index(substr($line, $in_index_start + length(".A(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $in = substr($line, $in_index_start + length(".A("), $in_index_stop); $in = mytrim($in);
        if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $out . "\n";
        }
        my $out_index_start = index($line, ".ZN("); my $out_index_stop = index(substr($line, $out_index_start + length(".ZN(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $out = substr($line, $out_index_start + length(".ZN("), $out_index_stop); $out = mytrim($out);
        if(index($out, "/") != -1 or index($in, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
        }
        #print "$out = NOT($in)\n";
        push(@not_vector, "$out = NOT($in)\n");
    }
    elsif ( $line =~ m/\bINV_X1\s/i ) {
        my $next_line = '';
        my $in = '';
        my $out = '';
        chomp($line);
        $line = mytrim($line);
        #print "#$line#";
        #print "INV_X1 FOUND.\n";
        if( index($line, ";") == -1 ) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        my $in_index_start = index($line, ".A("); my $in_index_stop = index(substr($line, $in_index_start + length(".A(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $in = substr($line, $in_index_start + length(".A("), $in_index_stop); $in = mytrim($in);
        if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $out . "\n";
        }
        my $out_index_start = index($line, ".ZN("); my $out_index_stop = index(substr($line, $out_index_start + length(".ZN(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $out = substr($line, $out_index_start + length(".ZN("), $out_index_stop); $out = mytrim($out);
        if(index($out, "/") != -1 or index($in, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
        }
        #print "$out = NOT($in)\n";
        push(@not_vector, "$out = NOT($in)\n");
    }
    elsif ( $line =~ m/\bOR2_X1\s/i ) {
        my $next_line = '';
        my @inputs;
        my $out = '';
        chomp($line);
        $line = mytrim($line);
        #print "OR2_X1 FOUND.\n";
        if( index($line, ";") == -1 ) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        for(my $i = 1; $i <= 2; $i++) {
            my $in_index_start = index($line, ".A$i("); my $in_index_stop = index(substr($line, $in_index_start + length(".A$i(")), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length(".A$i("), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $out . "\n";
            }
            push(@inputs, $in);
        }
        my $out_index_start = index($line, ".ZN("); my $out_index_stop = index(substr($line, $out_index_start + length(".ZN(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $out = substr($line, $out_index_start + length(".ZN("), $out_index_stop); $out = mytrim($out);
        if(index($out, "/") != -1 or index($out, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
        }
        my $or_string =  $out . " = OR(" .  join(",", @inputs) . ")\n";
        push(@or_vector, $or_string );
    }
    elsif ( $line =~ m/\bOR3_X1\s/i ) {
        my $next_line = '';
        my @inputs;
        my $out = '';
        chomp($line);
        $line = mytrim($line);
        #print "OR3_X1 FOUND.\n";i
        if( index($line, ";") == -1 ) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        for(my $i = 1; $i <= 3; $i++) {
            my $in_index_start = index($line, ".A$i("); my $in_index_stop = index(substr($line, $in_index_start + length(".A$i(")), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length(".A$i("), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $out . "\n";
            }
            push(@inputs, $in);
        }
        my $out_index_start = index($line, ".ZN("); my $out_index_stop = index(substr($line, $out_index_start + length(".ZN(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $out = substr($line, $out_index_start + length(".ZN("), $out_index_stop); $out = mytrim($out);
        if(index($out, "/") != -1 or index($out, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
        }
        my $or_string =  $out . " = OR(" .  join(",", @inputs) . ")\n";
        push(@or_vector, $or_string );

    }
    elsif ( $line =~ m/\bNOR2_X1\s/i ) {
        my $next_line = '';
        my @inputs;
        my $out = '';
        chomp($line);
        $line = mytrim($line);
        #print "NOR2_X1 FOUND.\n";
        if( index($line, ";") == -1 ) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        for(my $i = 1; $i <= 2; $i++) {
            my $in_index_start = index($line, ".A$i("); my $in_index_stop = index(substr($line, $in_index_start + length(".A$i(")), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length(".A$i("), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $out . "\n";
            }
            push(@inputs, $in);
        }
        my $out_index_start = index($line, ".ZN("); my $out_index_stop = index(substr($line, $out_index_start + length(".ZN(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $out = substr($line, $out_index_start + length(".ZN("), $out_index_stop); $out = mytrim($out);
        if(index($out, "/") != -1 or index($out, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
        }
        my $nor_string =  $out . " = NOR(" .  join(",", @inputs) . ")\n";
        push(@nor_vector, $nor_string );
    }
    elsif ( $line =~ m/\bNOR3_X1\s/i ) {
        my $next_line = '';
        my @inputs;
        my $out = '';
        chomp($line);
        $line = mytrim($line);
        #print "NOR3_X1 FOUND.\n";
        if( index($line, ";") == -1 ) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        for(my $i = 1; $i <= 3; $i++) {
            my $in_index_start = index($line, ".A$i("); my $in_index_stop = index(substr($line, $in_index_start + length(".A$i(")), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length(".A$i("), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $out . "\n";
            }
            push(@inputs, $in);
        }
        my $out_index_start = index($line, ".ZN("); my $out_index_stop = index(substr($line, $out_index_start + length(".ZN(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $out = substr($line, $out_index_start + length(".ZN("), $out_index_stop); $out = mytrim($out);
        if(index($out, "/") != -1 or index($out, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
        }
        my $nor_string = $out . " = NOR(" .  join(",", @inputs) . ")\n";
        push(@nor_vector, $nor_string );
    }
    elsif ( $line =~ m/\bNOR4_X1\s/i ) {
        my $next_line = '';
        my @inputs;
        my $out = '';
        chomp($line);
        $line = mytrim($line);
        #print "NOR4_X1 FOUND.\n";
        if( index($line, ";") == -1 ) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        for(my $i = 1; $i <= 4; $i++) {
            my $in_index_start = index($line, ".A$i("); my $in_index_stop = index(substr($line, $in_index_start + length(".A$i(")), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length(".A$i("), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $out . "\n";
            }
            push(@inputs, $in);
        }
        my $out_index_start = index($line, ".ZN("); my $out_index_stop = index(substr($line, $out_index_start + length(".ZN(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $out = substr($line, $out_index_start + length(".ZN("), $out_index_stop); $out = mytrim($out);
        if(index($out, "/") != -1 or index($out, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
        }
        my $nor_string = $out . " = NOR(" .  join(",", @inputs) . ")\n";
        push(@nor_vector, $nor_string );
    }
}

my $file_name = File::Spec->splitpath(+$file);
my $iscas_filename = join(".", substr($file_name, 0, length($file_name) - 2), "bench");
print "ISCAS File Name to write: $iscas_filename\n";
open(my $fh, '>', $iscas_filename) or die "Could not open file '$iscas_filename' $!";
my $and_gates = scalar @and_vector; my $or_gates = scalar @or_vector; my $nand_gates = scalar @nand_vector; my $nor_gates = scalar @nor_vector;
my $total_gates = $and_gates + $nand_gates + $or_gates + $nor_gates;

print $fh "# " . scalar @input_vector . " inputs\n";
print $fh "# " . scalar @output_vector . " outputs\n";
print $fh "# " . scalar @dff_vector . " D-type flipflops\n";
print $fh "# " . scalar @not_vector . " inverters\n";
print $fh "# $total_gates gates ($and_gates ANDs + $nand_gates NANDs + $or_gates ORs + $nor_gates NORs)\n";
print $fh "\n";
foreach my $input (@input_vector) {
    $input =~ s/\;//g;
    $input =  mytrim($input);
    print $fh "INPUT($input)\n";
}
print $fh "\n";
foreach my $output (@output_vector) {
    $output =~ s/\;//g;
    $output = mytrim($output);
    print $fh "OUTPUT($output)\n";
}
print $fh "\n";
foreach my $dff_string (@dff_vector) {
    print $fh "$dff_string";
}
print $fh "\n";
foreach my $not_string (@not_vector) {
    print $fh "$not_string";
}
print $fh "\n";
foreach my $nor_string (@nor_vector) {
    print $fh "$nor_string";
}
print $fh "\n";
foreach my $or_string (@or_vector) {
    print $fh "$or_string";
}
print $fh "\n";
foreach my $and_string (@and_vector) {
    print $fh "$and_string";
}
print $fh "\n";
foreach my $nand_string (@nand_vector) {
    print $fh "$nand_string";
}
print $fh "\n";
close $fh;


## Trim function for removing any leading or trailing white spaces
sub mytrim($) {
  my $string = shift;
  $string =~ s/^\s+//;
  $string =~ s/\s+$//;
  return $string;
}
sub stripchars {
    my ($s, $chars) = @_;
    $s =~ s/[$chars]//g;
    $s =~ s/\\//g;
    $s =~ s/\]//g;
    return $s;
}



    module mesi_isc_broad_cntl_CBUS_CMD_WIDTH3_BROAD_TYPE_WIDTH2_BROAD_ID_WIDTH5 ( 
        clk, rst, cbus_ack_array_i, fifo_status_empty_i, fifo_status_full_i, 
        broad_snoop_type_i, broad_snoop_cpu_id_i, broad_snoop_id_i, 
        cbus_cmd_array_o, broad_fifo_rd_o );
  input [3:0] cbus_ack_array_i;
  input [1:0] broad_snoop_type_i;
  input [1:0] broad_snoop_cpu_id_i;
  input [4:0] broad_snoop_id_i;
  output [11:0] cbus_cmd_array_o;
  input clk, rst, fifo_status_empty_i, fifo_status_full_i;
  output broad_fifo_rd_o;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15,
         N16, N17, N18, N19, N20, N21, N22, N23, N24, N25, N26,
         broadcast_in_progress, N27, N28, N29, N30, N31, N32, N33, N34, N35,
         N36, N37, N38, N39, N40, N41, N42, N43, N44, N45, N46, N47, N48, N49,
         N50, N51, N52, N53, N54, N55, N56, N57, N58, N59, N60, N61, N62, N63,
         N64, N65, N66, N67, N68, N69, N70, N71, N72, N73, N74, N75, N76, N77,
         N78, N79, N80, N81, N82, N83, N84, N85, N86, N87, N88, N89, N90, N91,
         N92, N93, N94, N95, N96, N97, N98, N99, N100, N101, N102, N103, N104,
         N105, N106, N107, N108, N109, N110, N111, N112, N113;
  wire   [3:0] cbus_active_broad_array;
  wire   [3:0] cbus_active_en_access_array;
  wire   [3:0] cbus_active_en_access_and_not_cbus_ack_array;

  GTECH_AND2 C125 ( .A(N33), .B(N34), .Z(N35) );
  GTECH_OR2 C127 ( .A(broad_snoop_cpu_id_i[1]), .B(N34), .Z(N36) );
  GTECH_OR2 C130 ( .A(N33), .B(broad_snoop_cpu_id_i[0]), .Z(N38) );
  GTECH_AND2 C132 ( .A(broad_snoop_cpu_id_i[1]), .B(broad_snoop_cpu_id_i[0]), 
        .Z(N40) );
  \**SEQGEN**  broad_fifo_rd_o_reg ( .clear(rst), .preset(1'b0), .next_state(
        N62), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        broad_fifo_rd_o), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \cbus_active_broad_array_reg[3]  ( .clear(rst), .preset(1'b0), 
        .next_state(N68), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        cbus_active_broad_array[3]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N63) );
  \**SEQGEN**  \cbus_active_broad_array_reg[2]  ( .clear(rst), .preset(1'b0), 
        .next_state(N67), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        cbus_active_broad_array[2]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N63) );
  \**SEQGEN**  \cbus_active_broad_array_reg[1]  ( .clear(rst), .preset(1'b0), 
        .next_state(N66), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        cbus_active_broad_array[1]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N63) );
  \**SEQGEN**  \cbus_active_broad_array_reg[0]  ( .clear(rst), .preset(1'b0), 
        .next_state(N65), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        cbus_active_broad_array[0]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N63) );
  \**SEQGEN**  \cbus_active_en_access_array_reg[3]  ( .clear(rst), .preset(
        1'b0), .next_state(N72), .clocked_on(clk), .data_in(1'b0), .enable(
        1'b0), .Q(cbus_active_en_access_array[3]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(N79) );
  \**SEQGEN**  \cbus_active_en_access_array_reg[2]  ( .clear(rst), .preset(
        1'b0), .next_state(N71), .clocked_on(clk), .data_in(1'b0), .enable(
        1'b0), .Q(cbus_active_en_access_array[2]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(N79) );
  \**SEQGEN**  \cbus_active_en_access_array_reg[1]  ( .clear(rst), .preset(
        1'b0), .next_state(N70), .clocked_on(clk), .data_in(1'b0), .enable(
        1'b0), .Q(cbus_active_en_access_array[1]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(N79) );
  \**SEQGEN**  \cbus_active_en_access_array_reg[0]  ( .clear(rst), .preset(
        1'b0), .next_state(N69), .clocked_on(clk), .data_in(1'b0), .enable(
        1'b0), .Q(cbus_active_en_access_array[0]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(N79) );
  \**SEQGEN**  broadcast_in_progress_reg ( .clear(rst), .preset(1'b0), 
        .next_state(N64), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        broadcast_in_progress), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N63) );
  GTECH_NOT I_0 ( .A(broad_snoop_type_i[0]), .Z(N80) );
  GTECH_OR2 C227 ( .A(N80), .B(broad_snoop_type_i[1]), .Z(N81) );
  GTECH_NOT I_1 ( .A(N81), .Z(N82) );
  SELECT_OP C255 ( .DATA1({1'b0, N81, N82}), .DATA2({N81, N82, N82}), .DATA3({
        1'b0, 1'b0, 1'b0}), .CONTROL1(N0), .CONTROL2(N1), .CONTROL3(N17), .Z(
        cbus_cmd_array_o[11:9]) );
  GTECH_BUF B_0 ( .A(cbus_active_broad_array[3]), .Z(N0) );
  GTECH_BUF B_1 ( .A(N15), .Z(N1) );
  SELECT_OP C256 ( .DATA1({1'b0, N81, N82}), .DATA2({N81, N82, N82}), .DATA3({
        1'b0, 1'b0, 1'b0}), .CONTROL1(N2), .CONTROL2(N3), .CONTROL3(N20), .Z(
        cbus_cmd_array_o[8:6]) );
  GTECH_BUF B_2 ( .A(cbus_active_broad_array[2]), .Z(N2) );
  GTECH_BUF B_3 ( .A(N18), .Z(N3) );
  SELECT_OP C257 ( .DATA1({1'b0, N81, N82}), .DATA2({N81, N82, N82}), .DATA3({
        1'b0, 1'b0, 1'b0}), .CONTROL1(N4), .CONTROL2(N5), .CONTROL3(N23), .Z(
        cbus_cmd_array_o[5:3]) );
  GTECH_BUF B_4 ( .A(cbus_active_broad_array[1]), .Z(N4) );
  GTECH_BUF B_5 ( .A(N21), .Z(N5) );
  SELECT_OP C258 ( .DATA1({1'b0, N81, N82}), .DATA2({N81, N82, N82}), .DATA3({
        1'b0, 1'b0, 1'b0}), .CONTROL1(N6), .CONTROL2(N7), .CONTROL3(N26), .Z(
        cbus_cmd_array_o[2:0]) );
  GTECH_BUF B_6 ( .A(cbus_active_broad_array[0]), .Z(N6) );
  GTECH_BUF B_7 ( .A(N24), .Z(N7) );
  SELECT_OP C259 ( .DATA1({1'b1, 1'b1, 1'b1, 1'b0}), .DATA2({1'b1, 1'b1, 1'b0, 
        1'b1}), .DATA3({1'b1, 1'b0, 1'b1, 1'b1}), .DATA4({1'b0, 1'b1, 1'b1, 
        1'b1}), .CONTROL1(N8), .CONTROL2(N9), .CONTROL3(N10), .CONTROL4(N11), 
        .Z({N44, N43, N42, N41}) );
  GTECH_BUF B_8 ( .A(N35), .Z(N8) );
  GTECH_BUF B_9 ( .A(N37), .Z(N9) );
  GTECH_BUF B_10 ( .A(N39), .Z(N10) );
  GTECH_BUF B_11 ( .A(N40), .Z(N11) );
  SELECT_OP C260 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b1}), .DATA2({1'b0, 1'b0, 1'b1, 
        1'b0}), .DATA3({1'b0, 1'b1, 1'b0, 1'b0}), .DATA4({1'b1, 1'b0, 1'b0, 
        1'b0}), .CONTROL1(N8), .CONTROL2(N9), .CONTROL3(N10), .CONTROL4(N11), 
        .Z({N48, N47, N46, N45}) );
  SELECT_OP C261 ( .DATA1({N44, N43, N42, N41}), .DATA2({1'b0, 1'b0, 1'b0, 
        1'b0}), .CONTROL1(N12), .CONTROL2(N13), .Z({N52, N51, N50, N49}) );
  GTECH_BUF B_12 ( .A(N32), .Z(N12) );
  GTECH_BUF B_13 ( .A(fifo_status_empty_i), .Z(N13) );
  SELECT_OP C262 ( .DATA1({N48, N47, N46, N45}), .DATA2({1'b0, 1'b0, 1'b0, 
        1'b0}), .CONTROL1(N12), .CONTROL2(N13), .Z({N56, N55, N54, N53}) );
  SELECT_OP C263 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(N61), 
        .CONTROL1(N14), .CONTROL2(N74), .CONTROL3(N77), .CONTROL4(N31), .Z(N62) );
  GTECH_BUF B_14 ( .A(N27), .Z(N14) );
  SELECT_OP C264 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N14), .CONTROL2(N74), .CONTROL3(N77), .CONTROL4(N31), .Z(N63) );
  SELECT_OP C265 ( .DATA1(N32), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N14), 
        .CONTROL2(N74), .CONTROL3(N77), .Z(N64) );
  SELECT_OP C266 ( .DATA1({N52, N51, N50, N49}), .DATA2({N57, N58, N59, N60}), 
        .DATA3({1'b0, 1'b0, 1'b0, 1'b0}), .CONTROL1(N14), .CONTROL2(N74), 
        .CONTROL3(N77), .Z({N68, N67, N66, N65}) );
  SELECT_OP C267 ( .DATA1({N56, N55, N54, N53}), .DATA2({1'b0, 1'b0, 1'b0, 
        1'b0}), .CONTROL1(N14), .CONTROL2(N77), .Z({N72, N71, N70, N69}) );
  GTECH_AND2 C270 ( .A(N87), .B(N88), .Z(N15) );
  GTECH_AND2 C271 ( .A(N86), .B(cbus_active_en_access_array[3]), .Z(N87) );
  GTECH_NOT I_2 ( .A(N85), .Z(N86) );
  GTECH_OR2 C273 ( .A(N84), .B(cbus_active_broad_array[0]), .Z(N85) );
  GTECH_OR2 C274 ( .A(N83), .B(cbus_active_broad_array[1]), .Z(N84) );
  GTECH_OR2 C275 ( .A(cbus_active_broad_array[3]), .B(
        cbus_active_broad_array[2]), .Z(N83) );
  GTECH_NOT I_3 ( .A(broad_fifo_rd_o), .Z(N88) );
  GTECH_OR2 C279 ( .A(N15), .B(cbus_active_broad_array[3]), .Z(N16) );
  GTECH_NOT I_4 ( .A(N16), .Z(N17) );
  GTECH_AND2 C282 ( .A(N93), .B(N88), .Z(N18) );
  GTECH_AND2 C283 ( .A(N92), .B(cbus_active_en_access_array[2]), .Z(N93) );
  GTECH_NOT I_5 ( .A(N91), .Z(N92) );
  GTECH_OR2 C285 ( .A(N90), .B(cbus_active_broad_array[0]), .Z(N91) );
  GTECH_OR2 C286 ( .A(N89), .B(cbus_active_broad_array[1]), .Z(N90) );
  GTECH_OR2 C287 ( .A(cbus_active_broad_array[3]), .B(
        cbus_active_broad_array[2]), .Z(N89) );
  GTECH_OR2 C291 ( .A(N18), .B(cbus_active_broad_array[2]), .Z(N19) );
  GTECH_NOT I_6 ( .A(N19), .Z(N20) );
  GTECH_AND2 C293 ( .A(N98), .B(N88), .Z(N21) );
  GTECH_AND2 C294 ( .A(N97), .B(cbus_active_en_access_array[1]), .Z(N98) );
  GTECH_NOT I_7 ( .A(N96), .Z(N97) );
  GTECH_OR2 C296 ( .A(N95), .B(cbus_active_broad_array[0]), .Z(N96) );
  GTECH_OR2 C297 ( .A(N94), .B(cbus_active_broad_array[1]), .Z(N95) );
  GTECH_OR2 C298 ( .A(cbus_active_broad_array[3]), .B(
        cbus_active_broad_array[2]), .Z(N94) );
  GTECH_OR2 C302 ( .A(N21), .B(cbus_active_broad_array[1]), .Z(N22) );
  GTECH_NOT I_8 ( .A(N22), .Z(N23) );
  GTECH_AND2 C304 ( .A(N103), .B(N88), .Z(N24) );
  GTECH_AND2 C305 ( .A(N102), .B(cbus_active_en_access_array[0]), .Z(N103) );
  GTECH_NOT I_9 ( .A(N101), .Z(N102) );
  GTECH_OR2 C307 ( .A(N100), .B(cbus_active_broad_array[0]), .Z(N101) );
  GTECH_OR2 C308 ( .A(N99), .B(cbus_active_broad_array[1]), .Z(N100) );
  GTECH_OR2 C309 ( .A(cbus_active_broad_array[3]), .B(
        cbus_active_broad_array[2]), .Z(N99) );
  GTECH_OR2 C313 ( .A(N24), .B(cbus_active_broad_array[0]), .Z(N25) );
  GTECH_NOT I_10 ( .A(N25), .Z(N26) );
  GTECH_AND2 C315 ( .A(N104), .B(N88), .Z(N27) );
  GTECH_NOT I_11 ( .A(broadcast_in_progress), .Z(N104) );
  GTECH_OR2 C318 ( .A(N106), .B(cbus_active_broad_array[0]), .Z(N28) );
  GTECH_OR2 C319 ( .A(N105), .B(cbus_active_broad_array[1]), .Z(N106) );
  GTECH_OR2 C320 ( .A(cbus_active_broad_array[3]), .B(
        cbus_active_broad_array[2]), .Z(N105) );
  GTECH_OR2 C324 ( .A(N28), .B(N27), .Z(N29) );
  GTECH_OR2 C325 ( .A(broad_fifo_rd_o), .B(N29), .Z(N30) );
  GTECH_NOT I_12 ( .A(N30), .Z(N31) );
  GTECH_NOT I_13 ( .A(fifo_status_empty_i), .Z(N32) );
  GTECH_NOT I_14 ( .A(broad_snoop_cpu_id_i[1]), .Z(N33) );
  GTECH_NOT I_15 ( .A(broad_snoop_cpu_id_i[0]), .Z(N34) );
  GTECH_NOT I_16 ( .A(N36), .Z(N37) );
  GTECH_NOT I_17 ( .A(N38), .Z(N39) );
  GTECH_AND2 C338 ( .A(cbus_active_broad_array[3]), .B(N107), .Z(N57) );
  GTECH_NOT I_18 ( .A(cbus_ack_array_i[3]), .Z(N107) );
  GTECH_AND2 C340 ( .A(cbus_active_broad_array[2]), .B(N108), .Z(N58) );
  GTECH_NOT I_19 ( .A(cbus_ack_array_i[2]), .Z(N108) );
  GTECH_AND2 C342 ( .A(cbus_active_broad_array[1]), .B(N109), .Z(N59) );
  GTECH_NOT I_20 ( .A(cbus_ack_array_i[1]), .Z(N109) );
  GTECH_AND2 C344 ( .A(cbus_active_broad_array[0]), .B(N110), .Z(N60) );
  GTECH_NOT I_21 ( .A(cbus_ack_array_i[0]), .Z(N110) );
  GTECH_NOT I_22 ( .A(N113), .Z(N61) );
  GTECH_OR2 C347 ( .A(N112), .B(
        cbus_active_en_access_and_not_cbus_ack_array[0]), .Z(N113) );
  GTECH_OR2 C348 ( .A(N111), .B(
        cbus_active_en_access_and_not_cbus_ack_array[1]), .Z(N112) );
  GTECH_OR2 C349 ( .A(cbus_active_en_access_and_not_cbus_ack_array[3]), .B(
        cbus_active_en_access_and_not_cbus_ack_array[2]), .Z(N111) );
  GTECH_NOT I_23 ( .A(N27), .Z(N73) );
  GTECH_AND2 C351 ( .A(N28), .B(N73), .Z(N74) );
  GTECH_NOT I_24 ( .A(N28), .Z(N75) );
  GTECH_AND2 C353 ( .A(N73), .B(N75), .Z(N76) );
  GTECH_AND2 C354 ( .A(broad_fifo_rd_o), .B(N76), .Z(N77) );
  GTECH_AND2 C355 ( .A(cbus_active_en_access_array[3]), .B(N107), .Z(
        cbus_active_en_access_and_not_cbus_ack_array[3]) );
  GTECH_AND2 C357 ( .A(cbus_active_en_access_array[2]), .B(N108), .Z(
        cbus_active_en_access_and_not_cbus_ack_array[2]) );
  GTECH_AND2 C359 ( .A(cbus_active_en_access_array[1]), .B(N109), .Z(
        cbus_active_en_access_and_not_cbus_ack_array[1]) );
  GTECH_AND2 C361 ( .A(cbus_active_en_access_array[0]), .B(N110), .Z(
        cbus_active_en_access_and_not_cbus_ack_array[0]) );
  GTECH_NOT I_25 ( .A(N74), .Z(N78) );
  GTECH_AND2 C364 ( .A(N63), .B(N78), .Z(N79) );
endmodule


module mesi_isc_basic_fifo_DATA_WIDTH41_FIFO_SIZE4_FIFO_SIZE_LOG22 ( clk, rst, 
        wr_i, rd_i, data_i, data_o, status_empty_o, status_full_o );
  input [40:0] data_i;
  output [40:0] data_o;
  input clk, rst, wr_i, rd_i;
  output status_empty_o, status_full_o;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15,
         \entry[3][40] , \entry[3][39] , \entry[3][38] , \entry[3][37] ,
         \entry[3][36] , \entry[3][35] , \entry[3][34] , \entry[3][33] ,
         \entry[3][32] , \entry[3][31] , \entry[3][30] , \entry[3][29] ,
         \entry[3][28] , \entry[3][27] , \entry[3][26] , \entry[3][25] ,
         \entry[3][24] , \entry[3][23] , \entry[3][22] , \entry[3][21] ,
         \entry[3][20] , \entry[3][19] , \entry[3][18] , \entry[3][17] ,
         \entry[3][16] , \entry[3][15] , \entry[3][14] , \entry[3][13] ,
         \entry[3][12] , \entry[3][11] , \entry[3][10] , \entry[3][9] ,
         \entry[3][8] , \entry[3][7] , \entry[3][6] , \entry[3][5] ,
         \entry[3][4] , \entry[3][3] , \entry[3][2] , \entry[3][1] ,
         \entry[3][0] , \entry[2][40] , \entry[2][39] , \entry[2][38] ,
         \entry[2][37] , \entry[2][36] , \entry[2][35] , \entry[2][34] ,
         \entry[2][33] , \entry[2][32] , \entry[2][31] , \entry[2][30] ,
         \entry[2][29] , \entry[2][28] , \entry[2][27] , \entry[2][26] ,
         \entry[2][25] , \entry[2][24] , \entry[2][23] , \entry[2][22] ,
         \entry[2][21] , \entry[2][20] , \entry[2][19] , \entry[2][18] ,
         \entry[2][17] , \entry[2][16] , \entry[2][15] , \entry[2][14] ,
         \entry[2][13] , \entry[2][12] , \entry[2][11] , \entry[2][10] ,
         \entry[2][9] , \entry[2][8] , \entry[2][7] , \entry[2][6] ,
         \entry[2][5] , \entry[2][4] , \entry[2][3] , \entry[2][2] ,
         \entry[2][1] , \entry[2][0] , \entry[1][40] , \entry[1][39] ,
         \entry[1][38] , \entry[1][37] , \entry[1][36] , \entry[1][35] ,
         \entry[1][34] , \entry[1][33] , \entry[1][32] , \entry[1][31] ,
         \entry[1][30] , \entry[1][29] , \entry[1][28] , \entry[1][27] ,
         \entry[1][26] , \entry[1][25] , \entry[1][24] , \entry[1][23] ,
         \entry[1][22] , \entry[1][21] , \entry[1][20] , \entry[1][19] ,
         \entry[1][18] , \entry[1][17] , \entry[1][16] , \entry[1][15] ,
         \entry[1][14] , \entry[1][13] , \entry[1][12] , \entry[1][11] ,
         \entry[1][10] , \entry[1][9] , \entry[1][8] , \entry[1][7] ,
         \entry[1][6] , \entry[1][5] , \entry[1][4] , \entry[1][3] ,
         \entry[1][2] , \entry[1][1] , \entry[1][0] , \entry[0][40] ,
         \entry[0][39] , \entry[0][38] , \entry[0][37] , \entry[0][36] ,
         \entry[0][35] , \entry[0][34] , \entry[0][33] , \entry[0][32] ,
         \entry[0][31] , \entry[0][30] , \entry[0][29] , \entry[0][28] ,
         \entry[0][27] , \entry[0][26] , \entry[0][25] , \entry[0][24] ,
         \entry[0][23] , \entry[0][22] , \entry[0][21] , \entry[0][20] ,
         \entry[0][19] , \entry[0][18] , \entry[0][17] , \entry[0][16] ,
         \entry[0][15] , \entry[0][14] , \entry[0][13] , \entry[0][12] ,
         \entry[0][11] , \entry[0][10] , \entry[0][9] , \entry[0][8] ,
         \entry[0][7] , \entry[0][6] , \entry[0][5] , \entry[0][4] ,
         \entry[0][3] , \entry[0][2] , \entry[0][1] , \entry[0][0] , N16, N17,
         N18, N19, N20, N21, N22, N23, N24, N25, N26, N27, N28, N29, N30, N31,
         N32, N33, N34, N35, N36, N37, N38, N39, N40, N41, N42, N43, N44, N45,
         N46, N47, N48, N49, N50, N51, N52, N53, N54, N55, N56, N57, N58, N59,
         N60, N61, N62, N63, N64, N65, N66, N67, N68, N69, N70, N71, N72, N73,
         N74, N75, N76, N77, N78, N79, N80, N81, N82, N83, N84, N85, N86, N87,
         N88, N89, N90, N91, N92, N93, N94, N95, N96, N97, N98, N99, N100,
         N101, N102, N103, N104, N105, N106, N107, N108, N109, N110, N111,
         N112, N113, N114, N115, N116, N117, N118, N119, N120, N121, N122,
         N123, N124, N125, N126, N127, N128, N129, N130, N131, N132, N133,
         N134, N135, N136, N137, N138, N139, N140, N141, N142, N143, N144,
         N145, N146, N147, N148, N149, N150, N151, N152, N153,
         fifo_depth_decrease, N154, N155, fifo_depth_increase, N156, N157,
         N158, N159, N160, N161, N162, N163, N164, N165, N166, N167, N168,
         N169, N170, N171, N172, N173;
  wire   [1:0] ptr_wr;
  wire   [1:0] ptr_rd_plus_1;
  wire   [1:0] ptr_rd;
  wire   [1:0] fifo_depth;

  \**SEQGEN**  \ptr_wr_reg[1]  ( .clear(rst), .preset(1'b0), .next_state(N19), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(ptr_wr[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(wr_i) );
  \**SEQGEN**  \ptr_wr_reg[0]  ( .clear(rst), .preset(1'b0), .next_state(N18), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(ptr_wr[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(wr_i) );
  \**SEQGEN**  \entry_reg[3][40]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[40]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][40] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][39]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[39]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][39] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][38]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[38]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][38] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][37]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[37]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][37] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][36]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[36]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][36] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][35]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[35]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][35] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][34]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[34]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][34] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][33]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[33]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][33] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][32]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[32]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][32] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][31]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[31]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][31] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][30]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[30]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][30] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][29]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[29]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][29] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][28]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[28]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][28] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][27]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[27]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][27] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][26]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[26]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][26] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][25]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[25]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][25] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][24]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[24]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][24] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][23]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[23]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][23] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][22]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[22]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][22] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][21]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[21]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][21] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][20]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[20]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][20] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][19]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[19]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][19] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][18]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[18]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][18] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][17]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[17]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][17] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][16]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[16]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][16] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][15]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[15]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][15] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][14]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[14]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][14] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][13]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[13]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][13] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][12]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[12]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][12] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][11]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[11]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][11] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][10]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[10]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][10] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][9]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[9]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][9] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][8]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[8]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][8] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][7]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[7]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][7] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][6]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[6]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][6] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][5]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[5]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][5] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][4]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[4]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][4] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][3]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[3]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][3] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][2]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[2]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][2] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][1]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[1]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][1] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[3][0]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[0]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[3][0] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N23) );
  \**SEQGEN**  \entry_reg[2][40]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[40]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][40] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][39]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[39]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][39] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][38]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[38]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][38] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][37]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[37]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][37] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][36]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[36]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][36] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][35]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[35]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][35] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][34]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[34]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][34] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][33]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[33]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][33] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][32]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[32]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][32] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][31]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[31]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][31] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][30]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[30]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][30] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][29]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[29]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][29] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][28]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[28]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][28] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][27]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[27]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][27] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][26]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[26]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][26] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][25]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[25]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][25] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][24]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[24]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][24] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][23]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[23]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][23] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][22]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[22]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][22] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][21]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[21]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][21] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][20]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[20]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][20] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][19]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[19]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][19] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][18]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[18]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][18] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][17]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[17]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][17] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][16]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[16]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][16] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][15]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[15]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][15] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][14]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[14]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][14] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][13]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[13]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][13] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][12]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[12]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][12] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][11]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[11]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][11] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][10]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[10]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][10] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][9]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[9]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][9] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][8]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[8]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][8] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][7]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[7]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][7] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][6]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[6]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][6] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][5]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[5]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][5] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][4]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[4]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][4] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][3]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[3]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][3] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][2]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[2]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][2] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][1]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[1]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][1] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[2][0]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[0]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[2][0] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N22) );
  \**SEQGEN**  \entry_reg[1][40]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[40]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][40] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][39]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[39]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][39] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][38]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[38]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][38] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][37]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[37]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][37] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][36]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[36]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][36] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][35]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[35]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][35] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][34]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[34]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][34] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][33]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[33]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][33] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][32]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[32]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][32] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][31]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[31]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][31] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][30]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[30]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][30] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][29]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[29]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][29] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][28]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[28]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][28] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][27]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[27]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][27] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][26]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[26]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][26] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][25]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[25]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][25] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][24]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[24]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][24] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][23]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[23]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][23] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][22]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[22]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][22] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][21]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[21]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][21] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][20]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[20]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][20] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][19]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[19]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][19] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][18]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[18]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][18] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][17]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[17]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][17] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][16]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[16]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][16] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][15]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[15]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][15] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][14]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[14]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][14] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][13]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[13]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][13] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][12]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[12]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][12] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][11]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[11]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][11] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][10]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[10]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][10] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][9]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[9]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][9] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][8]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[8]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][8] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][7]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[7]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][7] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][6]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[6]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][6] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][5]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[5]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][5] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][4]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[4]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][4] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][3]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[3]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][3] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][2]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[2]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][2] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][1]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[1]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][1] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[1][0]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[0]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][0] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N21) );
  \**SEQGEN**  \entry_reg[0][40]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[40]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][40] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][39]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[39]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][39] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][38]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[38]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][38] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][37]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[37]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][37] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][36]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[36]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][36] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][35]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[35]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][35] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][34]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[34]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][34] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][33]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[33]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][33] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][32]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[32]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][32] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][31]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[31]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][31] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][30]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[30]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][30] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][29]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[29]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][29] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][28]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[28]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][28] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][27]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[27]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][27] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][26]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[26]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][26] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][25]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[25]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][25] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][24]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[24]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][24] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][23]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[23]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][23] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][22]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[22]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][22] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][21]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[21]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][21] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][20]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[20]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][20] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][19]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[19]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][19] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][18]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[18]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][18] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][17]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[17]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][17] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][16]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[16]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][16] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][15]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[15]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][15] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][14]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[14]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][14] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][13]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[13]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][13] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][12]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[12]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][12] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][11]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[11]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][11] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][10]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[10]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][10] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][9]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[9]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][9] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][8]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[8]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][8] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][7]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[7]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][7] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][6]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[6]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][6] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][5]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[5]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][5] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][4]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[4]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][4] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][3]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[3]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][3] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][2]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[2]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][2] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][1]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[1]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][1] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \entry_reg[0][0]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[0]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][0] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N20) );
  \**SEQGEN**  \data_o_reg[40]  ( .clear(rst), .preset(1'b0), .next_state(N148), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[40]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[39]  ( .clear(rst), .preset(1'b0), .next_state(N147), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[39]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[38]  ( .clear(rst), .preset(1'b0), .next_state(N146), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[38]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[37]  ( .clear(rst), .preset(1'b0), .next_state(N145), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[37]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[36]  ( .clear(rst), .preset(1'b0), .next_state(N144), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[36]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[35]  ( .clear(rst), .preset(1'b0), .next_state(N143), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[35]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[34]  ( .clear(rst), .preset(1'b0), .next_state(N142), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[34]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[33]  ( .clear(rst), .preset(1'b0), .next_state(N141), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[33]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[32]  ( .clear(rst), .preset(1'b0), .next_state(N140), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[32]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[31]  ( .clear(rst), .preset(1'b0), .next_state(N139), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[31]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[30]  ( .clear(rst), .preset(1'b0), .next_state(N138), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[30]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[29]  ( .clear(rst), .preset(1'b0), .next_state(N137), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[29]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[28]  ( .clear(rst), .preset(1'b0), .next_state(N136), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[28]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[27]  ( .clear(rst), .preset(1'b0), .next_state(N135), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[27]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[26]  ( .clear(rst), .preset(1'b0), .next_state(N134), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[26]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[25]  ( .clear(rst), .preset(1'b0), .next_state(N133), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[25]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[24]  ( .clear(rst), .preset(1'b0), .next_state(N132), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[24]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[23]  ( .clear(rst), .preset(1'b0), .next_state(N131), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[23]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[22]  ( .clear(rst), .preset(1'b0), .next_state(N130), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[22]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[21]  ( .clear(rst), .preset(1'b0), .next_state(N129), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[21]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[20]  ( .clear(rst), .preset(1'b0), .next_state(N128), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[20]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[19]  ( .clear(rst), .preset(1'b0), .next_state(N127), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[19]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[18]  ( .clear(rst), .preset(1'b0), .next_state(N126), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[18]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[17]  ( .clear(rst), .preset(1'b0), .next_state(N125), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[17]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[16]  ( .clear(rst), .preset(1'b0), .next_state(N124), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[16]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[15]  ( .clear(rst), .preset(1'b0), .next_state(N123), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[15]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[14]  ( .clear(rst), .preset(1'b0), .next_state(N122), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[14]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[13]  ( .clear(rst), .preset(1'b0), .next_state(N121), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[13]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[12]  ( .clear(rst), .preset(1'b0), .next_state(N120), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[12]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[11]  ( .clear(rst), .preset(1'b0), .next_state(N119), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[11]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[10]  ( .clear(rst), .preset(1'b0), .next_state(N118), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[10]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[9]  ( .clear(rst), .preset(1'b0), .next_state(N117), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[8]  ( .clear(rst), .preset(1'b0), .next_state(N116), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[7]  ( .clear(rst), .preset(1'b0), .next_state(N115), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[6]  ( .clear(rst), .preset(1'b0), .next_state(N114), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[5]  ( .clear(rst), .preset(1'b0), .next_state(N113), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[4]  ( .clear(rst), .preset(1'b0), .next_state(N112), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[3]  ( .clear(rst), .preset(1'b0), .next_state(N111), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[2]  ( .clear(rst), .preset(1'b0), .next_state(N110), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[1]  ( .clear(rst), .preset(1'b0), .next_state(N109), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[0]  ( .clear(rst), .preset(1'b0), .next_state(N108), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \ptr_rd_reg[1]  ( .clear(rst), .preset(1'b0), .next_state(N152), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(ptr_rd[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(rd_i) );
  \**SEQGEN**  \ptr_rd_reg[0]  ( .clear(rst), .preset(1'b0), .next_state(N151), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(ptr_rd[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(rd_i) );
  EQ_UNS_OP eq_172 ( .A(fifo_depth), .B(1'b1), .Z(N153) );
  EQ_UNS_OP eq_176 ( .A(fifo_depth), .B(1'b0), .Z(N155) );
  \**SEQGEN**  status_empty_reg ( .clear(1'b0), .preset(rst), .next_state(N154), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(status_empty_o), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N159) );
  EQ_UNS_OP eq_186 ( .A(fifo_depth), .B({1'b1, 1'b1}), .Z(N160) );
  EQ_UNS_OP eq_190 ( .A(fifo_depth), .B(1'b0), .Z(N162) );
  \**SEQGEN**  status_full_reg ( .clear(rst), .preset(1'b0), .next_state(N161), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(status_full_o), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N166) );
  SUB_UNS_OP sub_207 ( .A(ptr_wr), .B(ptr_rd), .Z(fifo_depth) );
  ADD_UNS_OP add_158 ( .A(ptr_rd), .B(1'b1), .Z(ptr_rd_plus_1) );
  ADD_UNS_OP add_155 ( .A(ptr_rd), .B(1'b1), .Z({N152, N151}) );
  ADD_UNS_OP add_123 ( .A(ptr_wr), .B(1'b1), .Z({N19, N18}) );
  GTECH_AND2 C1248 ( .A(ptr_wr[0]), .B(ptr_wr[1]), .Z(N167) );
  GTECH_AND2 C1249 ( .A(N0), .B(ptr_wr[1]), .Z(N168) );
  GTECH_NOT I_0 ( .A(ptr_wr[0]), .Z(N0) );
  GTECH_AND2 C1250 ( .A(ptr_wr[0]), .B(N1), .Z(N169) );
  GTECH_NOT I_1 ( .A(ptr_wr[1]), .Z(N1) );
  GTECH_AND2 C1251 ( .A(N2), .B(N3), .Z(N170) );
  GTECH_NOT I_2 ( .A(ptr_wr[0]), .Z(N2) );
  GTECH_NOT I_3 ( .A(ptr_wr[1]), .Z(N3) );
  SELECT_OP C1252 ( .DATA1({N167, N168, N169, N170}), .DATA2({1'b0, 1'b0, 1'b0, 
        1'b0}), .CONTROL1(N4), .CONTROL2(N5), .Z({N23, N22, N21, N20}) );
  GTECH_BUF B_0 ( .A(wr_i), .Z(N4) );
  GTECH_BUF B_1 ( .A(N17), .Z(N5) );
  SELECT_OP C1253 ( .DATA1(data_i), .DATA2({N26, N27, N28, N29, N30, N31, N32, 
        N33, N34, N35, N36, N37, N38, N39, N40, N41, N42, N43, N44, N45, N46, 
        N47, N48, N49, N50, N51, N52, N53, N54, N55, N56, N57, N58, N59, N60, 
        N61, N62, N63, N64, N65, N66}), .DATA3({N67, N68, N69, N70, N71, N72, 
        N73, N74, N75, N76, N77, N78, N79, N80, N81, N82, N83, N84, N85, N86, 
        N87, N88, N89, N90, N91, N92, N93, N94, N95, N96, N97, N98, N99, N100, 
        N101, N102, N103, N104, N105, N106, N107}), .CONTROL1(N6), .CONTROL2(
        N150), .CONTROL3(N25), .Z({N148, N147, N146, N145, N144, N143, N142, 
        N141, N140, N139, N138, N137, N136, N135, N134, N133, N132, N131, N130, 
        N129, N128, N127, N126, N125, N124, N123, N122, N121, N120, N119, N118, 
        N117, N116, N115, N114, N113, N112, N111, N110, N109, N108}) );
  GTECH_BUF B_2 ( .A(status_empty_o), .Z(N6) );
  SELECT_OP C1254 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N7), 
        .CONTROL2(N8), .CONTROL3(N158), .Z(N159) );
  GTECH_BUF B_3 ( .A(N154), .Z(N7) );
  GTECH_BUF B_4 ( .A(N156), .Z(N8) );
  SELECT_OP C1255 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N9), 
        .CONTROL2(N10), .CONTROL3(N165), .Z(N166) );
  GTECH_BUF B_5 ( .A(N161), .Z(N9) );
  GTECH_BUF B_6 ( .A(N163), .Z(N10) );
  MUX_OP C1256 ( .D0({\entry[0][0] , \entry[0][1] , \entry[0][2] , 
        \entry[0][3] , \entry[0][4] , \entry[0][5] , \entry[0][6] , 
        \entry[0][7] , \entry[0][8] , \entry[0][9] , \entry[0][10] , 
        \entry[0][11] , \entry[0][12] , \entry[0][13] , \entry[0][14] , 
        \entry[0][15] , \entry[0][16] , \entry[0][17] , \entry[0][18] , 
        \entry[0][19] , \entry[0][20] , \entry[0][21] , \entry[0][22] , 
        \entry[0][23] , \entry[0][24] , \entry[0][25] , \entry[0][26] , 
        \entry[0][27] , \entry[0][28] , \entry[0][29] , \entry[0][30] , 
        \entry[0][31] , \entry[0][32] , \entry[0][33] , \entry[0][34] , 
        \entry[0][35] , \entry[0][36] , \entry[0][37] , \entry[0][38] , 
        \entry[0][39] , \entry[0][40] }), .D1({\entry[1][0] , \entry[1][1] , 
        \entry[1][2] , \entry[1][3] , \entry[1][4] , \entry[1][5] , 
        \entry[1][6] , \entry[1][7] , \entry[1][8] , \entry[1][9] , 
        \entry[1][10] , \entry[1][11] , \entry[1][12] , \entry[1][13] , 
        \entry[1][14] , \entry[1][15] , \entry[1][16] , \entry[1][17] , 
        \entry[1][18] , \entry[1][19] , \entry[1][20] , \entry[1][21] , 
        \entry[1][22] , \entry[1][23] , \entry[1][24] , \entry[1][25] , 
        \entry[1][26] , \entry[1][27] , \entry[1][28] , \entry[1][29] , 
        \entry[1][30] , \entry[1][31] , \entry[1][32] , \entry[1][33] , 
        \entry[1][34] , \entry[1][35] , \entry[1][36] , \entry[1][37] , 
        \entry[1][38] , \entry[1][39] , \entry[1][40] }), .D2({\entry[2][0] , 
        \entry[2][1] , \entry[2][2] , \entry[2][3] , \entry[2][4] , 
        \entry[2][5] , \entry[2][6] , \entry[2][7] , \entry[2][8] , 
        \entry[2][9] , \entry[2][10] , \entry[2][11] , \entry[2][12] , 
        \entry[2][13] , \entry[2][14] , \entry[2][15] , \entry[2][16] , 
        \entry[2][17] , \entry[2][18] , \entry[2][19] , \entry[2][20] , 
        \entry[2][21] , \entry[2][22] , \entry[2][23] , \entry[2][24] , 
        \entry[2][25] , \entry[2][26] , \entry[2][27] , \entry[2][28] , 
        \entry[2][29] , \entry[2][30] , \entry[2][31] , \entry[2][32] , 
        \entry[2][33] , \entry[2][34] , \entry[2][35] , \entry[2][36] , 
        \entry[2][37] , \entry[2][38] , \entry[2][39] , \entry[2][40] }), .D3(
        {\entry[3][0] , \entry[3][1] , \entry[3][2] , \entry[3][3] , 
        \entry[3][4] , \entry[3][5] , \entry[3][6] , \entry[3][7] , 
        \entry[3][8] , \entry[3][9] , \entry[3][10] , \entry[3][11] , 
        \entry[3][12] , \entry[3][13] , \entry[3][14] , \entry[3][15] , 
        \entry[3][16] , \entry[3][17] , \entry[3][18] , \entry[3][19] , 
        \entry[3][20] , \entry[3][21] , \entry[3][22] , \entry[3][23] , 
        \entry[3][24] , \entry[3][25] , \entry[3][26] , \entry[3][27] , 
        \entry[3][28] , \entry[3][29] , \entry[3][30] , \entry[3][31] , 
        \entry[3][32] , \entry[3][33] , \entry[3][34] , \entry[3][35] , 
        \entry[3][36] , \entry[3][37] , \entry[3][38] , \entry[3][39] , 
        \entry[3][40] }), .S0(N11), .S1(N12), .Z({N66, N65, N64, N63, N62, N61, 
        N60, N59, N58, N57, N56, N55, N54, N53, N52, N51, N50, N49, N48, N47, 
        N46, N45, N44, N43, N42, N41, N40, N39, N38, N37, N36, N35, N34, N33, 
        N32, N31, N30, N29, N28, N27, N26}) );
  GTECH_BUF B_7 ( .A(ptr_rd_plus_1[0]), .Z(N11) );
  GTECH_BUF B_8 ( .A(ptr_rd_plus_1[1]), .Z(N12) );
  MUX_OP C1257 ( .D0({\entry[0][0] , \entry[0][1] , \entry[0][2] , 
        \entry[0][3] , \entry[0][4] , \entry[0][5] , \entry[0][6] , 
        \entry[0][7] , \entry[0][8] , \entry[0][9] , \entry[0][10] , 
        \entry[0][11] , \entry[0][12] , \entry[0][13] , \entry[0][14] , 
        \entry[0][15] , \entry[0][16] , \entry[0][17] , \entry[0][18] , 
        \entry[0][19] , \entry[0][20] , \entry[0][21] , \entry[0][22] , 
        \entry[0][23] , \entry[0][24] , \entry[0][25] , \entry[0][26] , 
        \entry[0][27] , \entry[0][28] , \entry[0][29] , \entry[0][30] , 
        \entry[0][31] , \entry[0][32] , \entry[0][33] , \entry[0][34] , 
        \entry[0][35] , \entry[0][36] , \entry[0][37] , \entry[0][38] , 
        \entry[0][39] , \entry[0][40] }), .D1({\entry[1][0] , \entry[1][1] , 
        \entry[1][2] , \entry[1][3] , \entry[1][4] , \entry[1][5] , 
        \entry[1][6] , \entry[1][7] , \entry[1][8] , \entry[1][9] , 
        \entry[1][10] , \entry[1][11] , \entry[1][12] , \entry[1][13] , 
        \entry[1][14] , \entry[1][15] , \entry[1][16] , \entry[1][17] , 
        \entry[1][18] , \entry[1][19] , \entry[1][20] , \entry[1][21] , 
        \entry[1][22] , \entry[1][23] , \entry[1][24] , \entry[1][25] , 
        \entry[1][26] , \entry[1][27] , \entry[1][28] , \entry[1][29] , 
        \entry[1][30] , \entry[1][31] , \entry[1][32] , \entry[1][33] , 
        \entry[1][34] , \entry[1][35] , \entry[1][36] , \entry[1][37] , 
        \entry[1][38] , \entry[1][39] , \entry[1][40] }), .D2({\entry[2][0] , 
        \entry[2][1] , \entry[2][2] , \entry[2][3] , \entry[2][4] , 
        \entry[2][5] , \entry[2][6] , \entry[2][7] , \entry[2][8] , 
        \entry[2][9] , \entry[2][10] , \entry[2][11] , \entry[2][12] , 
        \entry[2][13] , \entry[2][14] , \entry[2][15] , \entry[2][16] , 
        \entry[2][17] , \entry[2][18] , \entry[2][19] , \entry[2][20] , 
        \entry[2][21] , \entry[2][22] , \entry[2][23] , \entry[2][24] , 
        \entry[2][25] , \entry[2][26] , \entry[2][27] , \entry[2][28] , 
        \entry[2][29] , \entry[2][30] , \entry[2][31] , \entry[2][32] , 
        \entry[2][33] , \entry[2][34] , \entry[2][35] , \entry[2][36] , 
        \entry[2][37] , \entry[2][38] , \entry[2][39] , \entry[2][40] }), .D3(
        {\entry[3][0] , \entry[3][1] , \entry[3][2] , \entry[3][3] , 
        \entry[3][4] , \entry[3][5] , \entry[3][6] , \entry[3][7] , 
        \entry[3][8] , \entry[3][9] , \entry[3][10] , \entry[3][11] , 
        \entry[3][12] , \entry[3][13] , \entry[3][14] , \entry[3][15] , 
        \entry[3][16] , \entry[3][17] , \entry[3][18] , \entry[3][19] , 
        \entry[3][20] , \entry[3][21] , \entry[3][22] , \entry[3][23] , 
        \entry[3][24] , \entry[3][25] , \entry[3][26] , \entry[3][27] , 
        \entry[3][28] , \entry[3][29] , \entry[3][30] , \entry[3][31] , 
        \entry[3][32] , \entry[3][33] , \entry[3][34] , \entry[3][35] , 
        \entry[3][36] , \entry[3][37] , \entry[3][38] , \entry[3][39] , 
        \entry[3][40] }), .S0(N13), .S1(N14), .Z({N107, N106, N105, N104, N103, 
        N102, N101, N100, N99, N98, N97, N96, N95, N94, N93, N92, N91, N90, 
        N89, N88, N87, N86, N85, N84, N83, N82, N81, N80, N79, N78, N77, N76, 
        N75, N74, N73, N72, N71, N70, N69, N68, N67}) );
  GTECH_BUF B_9 ( .A(ptr_rd[0]), .Z(N13) );
  GTECH_BUF B_10 ( .A(ptr_rd[1]), .Z(N14) );
  GTECH_NOT I_4 ( .A(rst), .Z(N15) );
  GTECH_BUF B_11 ( .A(N15), .Z(N16) );
  GTECH_NOT I_5 ( .A(wr_i), .Z(N17) );
  GTECH_AND2 C1266 ( .A(N16), .B(wr_i) );
  GTECH_OR2 C1272 ( .A(rd_i), .B(status_empty_o), .Z(N24) );
  GTECH_NOT I_6 ( .A(N24), .Z(N25) );
  GTECH_NOT I_7 ( .A(status_empty_o), .Z(N149) );
  GTECH_AND2 C1275 ( .A(rd_i), .B(N149), .Z(N150) );
  GTECH_AND2 C1277 ( .A(N16), .B(rd_i) );
  GTECH_AND2 C1278 ( .A(N153), .B(fifo_depth_decrease), .Z(N154) );
  GTECH_AND2 C1279 ( .A(N171), .B(fifo_depth_increase), .Z(N156) );
  GTECH_AND2 C1280 ( .A(N155), .B(status_empty_o), .Z(N171) );
  GTECH_OR2 C1283 ( .A(N156), .B(N154), .Z(N157) );
  GTECH_NOT I_8 ( .A(N157), .Z(N158) );
  GTECH_AND2 C1285 ( .A(N160), .B(fifo_depth_increase), .Z(N161) );
  GTECH_AND2 C1286 ( .A(N172), .B(fifo_depth_decrease), .Z(N163) );
  GTECH_AND2 C1287 ( .A(N162), .B(status_full_o), .Z(N172) );
  GTECH_OR2 C1290 ( .A(N163), .B(N161), .Z(N164) );
  GTECH_NOT I_9 ( .A(N164), .Z(N165) );
  GTECH_AND2 C1292 ( .A(wr_i), .B(N173), .Z(fifo_depth_increase) );
  GTECH_NOT I_10 ( .A(rd_i), .Z(N173) );
  GTECH_AND2 C1294 ( .A(N17), .B(rd_i), .Z(fifo_depth_decrease) );
endmodule



    module mesi_isc_broad_CBUS_CMD_WIDTH3_ADDR_WIDTH32_BROAD_TYPE_WIDTH2_BROAD_ID_WIDTH5_BROAD_REQ_FIFO_SIZE4_BROAD_REQ_FIFO_SIZE_LOG22 ( 
        clk, rst, cbus_ack_array_i, broad_fifo_wr_i, broad_addr_i, 
        broad_type_i, broad_cpu_id_i, broad_id_i, cbus_addr_o, 
        cbus_cmd_array_o, fifo_status_full_o );
  input [3:0] cbus_ack_array_i;
  input [31:0] broad_addr_i;
  input [1:0] broad_type_i;
  input [1:0] broad_cpu_id_i;
  input [4:0] broad_id_i;
  output [31:0] cbus_addr_o;
  output [11:0] cbus_cmd_array_o;
  input clk, rst, broad_fifo_wr_i;
  output fifo_status_full_o;
  wire   fifo_status_empty, broad_fifo_rd;
  wire   [1:0] broad_snoop_type;
  wire   [1:0] broad_snoop_cpu_id;
  wire   [4:0] broad_snoop_id;

  mesi_isc_broad_cntl_CBUS_CMD_WIDTH3_BROAD_TYPE_WIDTH2_BROAD_ID_WIDTH5 mesi_isc_broad_cntl ( 
        .clk(clk), .rst(rst), .cbus_ack_array_i(cbus_ack_array_i), 
        .fifo_status_empty_i(fifo_status_empty), .fifo_status_full_i(
        fifo_status_full_o), .broad_snoop_type_i(broad_snoop_type), 
        .broad_snoop_cpu_id_i(broad_snoop_cpu_id), .broad_snoop_id_i(
        broad_snoop_id), .cbus_cmd_array_o(cbus_cmd_array_o), 
        .broad_fifo_rd_o(broad_fifo_rd) );
  mesi_isc_basic_fifo_DATA_WIDTH41_FIFO_SIZE4_FIFO_SIZE_LOG22 broad_fifo ( 
        .clk(clk), .rst(rst), .wr_i(broad_fifo_wr_i), .rd_i(broad_fifo_rd), 
        .data_i({broad_addr_i, broad_type_i, broad_cpu_id_i, broad_id_i}), 
        .data_o({cbus_addr_o, broad_snoop_type, broad_snoop_cpu_id, 
        broad_snoop_id}), .status_empty_o(fifo_status_empty), .status_full_o(
        fifo_status_full_o) );
endmodule



    module mesi_isc_breq_fifos_cntl_MBUS_CMD_WIDTH3_ADDR_WIDTH32_BROAD_TYPE_WIDTH2_BROAD_ID_WIDTH5 ( 
        clk, rst, mbus_cmd_array_i, fifo_status_empty_array_i, 
        fifo_status_full_array_i, broad_fifo_status_full_i, broad_addr_array_i, 
        broad_type_array_i, broad_id_array_i, mbus_ack_array_o, 
        fifo_wr_array_o, fifo_rd_array_o, broad_fifo_wr_o, broad_addr_o, 
        broad_type_o, broad_cpu_id_o, broad_id_o, breq_type_array_o, 
        breq_cpu_id_array_o, breq_id_array_o );
  input [11:0] mbus_cmd_array_i;
  input [3:0] fifo_status_empty_array_i;
  input [3:0] fifo_status_full_array_i;
  input [127:0] broad_addr_array_i;
  input [7:0] broad_type_array_i;
  input [19:0] broad_id_array_i;
  output [3:0] mbus_ack_array_o;
  output [3:0] fifo_wr_array_o;
  output [3:0] fifo_rd_array_o;
  output [31:0] broad_addr_o;
  output [1:0] broad_type_o;
  output [1:0] broad_cpu_id_o;
  output [4:0] broad_id_o;
  output [7:0] breq_type_array_o;
  output [7:0] breq_cpu_id_array_o;
  output [19:0] breq_id_array_o;
  input clk, rst, broad_fifo_status_full_i;
  output broad_fifo_wr_o;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15,
         N16, N17, N18, N19, N20, N21, N22, N23, N24, N25, N26, N27, N28, N29,
         N30, N31, N32, N33, N34, N35, N36, N37, N38, N39, N40, N41, N42, N43,
         N44, N45, N46, N47, N48, N49, N50, N51, N52, N53, N54, N55, N56, N57,
         N58, N59, N60, N61, N62, N63, N64, N65, N66, N67, N68, N69, N70, N71,
         N72, N73, N74, N75, N76, N77, N78, N79, N80, N81, N82, N83, N84, N85,
         N86, N87, N88, N89, N90, N91, N92, N93, N94, N95, N96, N97, N98, N99,
         N100, N101, N102, N103, N104, N105, N106, N107, N108, N109, N110,
         N111, N112, N113, N114, N115, N116, N117, N118, N119, N120, N121,
         N122, N123, N124, N125, N126, N127, N128, N129, N130, N131, N132,
         N133, N134, N135, N136, N137, N138, N139, N140, N141, N142, N143,
         N144, N145, N146, N147, N148, N149, N150, N151, N152, N153, N154,
         N155, N156, N157, N158, N159, N160, N161, N162, N163, N164, N165,
         N166, N167, N168, N169, N170, N171, N172, N173, N174, N175, N176,
         N177, N178, N179, N180, N181, N182, N183, N184, N185, N186, N187,
         N188, N189, N190, N191, N192, N193, N194, N195, N196, N197, N198,
         N199, N200, N201, N202, N203, N204, N205, N206, N207, N208, N209,
         N210, N211, N212, N213, N214, N215, N216, N217, N218, N219, N220,
         N221, N222, N223, N224, N225, N226, N227, N228, N229, N230, N231,
         N232, N233, N234, N235, N236, N237, N238, N239, N240, N241, N242,
         N243, N244, N245, N246, N247, N248, N249, N250, N251, N252, N253,
         N254, N255, N256, N257, N258, N259, N260, N261, N262, N263, N264,
         N265, N266, N267, N268, N269, N270, N271, N272, N273, N274, N275,
         N276, N277, N278, N279, N280, N281, N282, N283, N284, N285, N286,
         N287, N288, N289, N290, N291, N292, N293, N294, N295, N296, N297,
         N298, N299, N300, N301, N302, N303, N304, N305, N306, N307, N308,
         N309, N310, N311, N312, N313, N314, N315, N316, N317, N318, N319,
         N320, N321, N322, N323, N324, N325, N326, N327, N328, N329, N330,
         N331, N332, N333, N334, N335, N336, N337, N338, N339, N340, N341,
         N342, N343, N344, N345, N346, N347, N348, N349, N350, N351, N352,
         N353, N354, N355, N356, N357, N358, N359, N360, N361, N362, N363,
         N364, N365, N366, N367, N368, N369;
  wire   [3:0] fifo_select_oh;
  wire   [3:0] fifos_priority;
  assign breq_id_array_o[0] = 1'b1;
  assign breq_id_array_o[1] = 1'b1;
  assign breq_id_array_o[6] = 1'b1;
  assign breq_id_array_o[10] = 1'b1;
  assign breq_cpu_id_array_o[2] = 1'b1;
  assign breq_cpu_id_array_o[5] = 1'b1;
  assign breq_cpu_id_array_o[6] = 1'b1;
  assign breq_cpu_id_array_o[7] = 1'b1;
  assign breq_id_array_o[5] = 1'b0;
  assign breq_id_array_o[11] = 1'b0;
  assign breq_id_array_o[15] = 1'b0;
  assign breq_id_array_o[16] = 1'b0;
  assign breq_cpu_id_array_o[0] = 1'b0;
  assign breq_cpu_id_array_o[1] = 1'b0;
  assign breq_cpu_id_array_o[3] = 1'b0;
  assign breq_cpu_id_array_o[4] = 1'b0;
  assign fifo_wr_array_o[3] = mbus_ack_array_o[3];
  assign fifo_wr_array_o[2] = mbus_ack_array_o[2];
  assign fifo_wr_array_o[1] = mbus_ack_array_o[1];
  assign fifo_wr_array_o[0] = mbus_ack_array_o[0];
  assign breq_id_array_o[4] = breq_id_array_o[19];
  assign breq_id_array_o[9] = breq_id_array_o[19];
  assign breq_id_array_o[14] = breq_id_array_o[19];
  assign breq_id_array_o[3] = breq_id_array_o[18];
  assign breq_id_array_o[8] = breq_id_array_o[18];
  assign breq_id_array_o[13] = breq_id_array_o[18];
  assign breq_id_array_o[2] = breq_id_array_o[17];
  assign breq_id_array_o[7] = breq_id_array_o[17];
  assign breq_id_array_o[12] = breq_id_array_o[17];

  \**SEQGEN**  \fifos_priority_reg[3]  ( .clear(rst), .preset(1'b0), 
        .next_state(fifos_priority[2]), .clocked_on(clk), .data_in(1'b0), 
        .enable(1'b0), .Q(fifos_priority[3]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        broad_fifo_wr_o) );
  \**SEQGEN**  \fifos_priority_reg[2]  ( .clear(rst), .preset(1'b0), 
        .next_state(fifos_priority[1]), .clocked_on(clk), .data_in(1'b0), 
        .enable(1'b0), .Q(fifos_priority[2]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        broad_fifo_wr_o) );
  \**SEQGEN**  \fifos_priority_reg[1]  ( .clear(rst), .preset(1'b0), 
        .next_state(fifos_priority[0]), .clocked_on(clk), .data_in(1'b0), 
        .enable(1'b0), .Q(fifos_priority[1]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        broad_fifo_wr_o) );
  \**SEQGEN**  \fifos_priority_reg[0]  ( .clear(1'b0), .preset(rst), 
        .next_state(fifos_priority[3]), .clocked_on(clk), .data_in(1'b0), 
        .enable(1'b0), .Q(fifos_priority[0]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        broad_fifo_wr_o) );
  \**SEQGEN**  \mbus_ack_array_reg[3]  ( .clear(rst), .preset(1'b0), 
        .next_state(N29), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mbus_ack_array_o[3]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \mbus_ack_array_reg[2]  ( .clear(rst), .preset(1'b0), 
        .next_state(N30), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mbus_ack_array_o[2]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \mbus_ack_array_reg[1]  ( .clear(rst), .preset(1'b0), 
        .next_state(N31), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mbus_ack_array_o[1]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \mbus_ack_array_reg[0]  ( .clear(rst), .preset(1'b0), 
        .next_state(N32), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mbus_ack_array_o[0]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \breq_type_array_o_reg[7]  ( .clear(rst), .preset(1'b0), 
        .next_state(N36), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        breq_type_array_o[7]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \breq_type_array_o_reg[6]  ( .clear(rst), .preset(1'b0), 
        .next_state(N35), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        breq_type_array_o[6]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \breq_type_array_o_reg[5]  ( .clear(rst), .preset(1'b0), 
        .next_state(N40), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        breq_type_array_o[5]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \breq_type_array_o_reg[4]  ( .clear(rst), .preset(1'b0), 
        .next_state(N39), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        breq_type_array_o[4]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \breq_type_array_o_reg[3]  ( .clear(rst), .preset(1'b0), 
        .next_state(N44), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        breq_type_array_o[3]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \breq_type_array_o_reg[2]  ( .clear(rst), .preset(1'b0), 
        .next_state(N43), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        breq_type_array_o[2]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \breq_type_array_o_reg[1]  ( .clear(rst), .preset(1'b0), 
        .next_state(N48), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        breq_type_array_o[1]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \breq_type_array_o_reg[0]  ( .clear(rst), .preset(1'b0), 
        .next_state(N47), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        breq_type_array_o[0]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \breq_id_base_reg[2]  ( .clear(rst), .preset(1'b0), 
        .next_state(N54), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        breq_id_array_o[19]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N51) );
  \**SEQGEN**  \breq_id_base_reg[1]  ( .clear(rst), .preset(1'b0), 
        .next_state(N53), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        breq_id_array_o[18]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N51) );
  \**SEQGEN**  \breq_id_base_reg[0]  ( .clear(rst), .preset(1'b0), 
        .next_state(N52), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        breq_id_array_o[17]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N51) );
  GTECH_NOT I_0 ( .A(mbus_cmd_array_i[10]), .Z(N55) );
  GTECH_NOT I_1 ( .A(mbus_cmd_array_i[9]), .Z(N56) );
  GTECH_OR2 C220 ( .A(N55), .B(mbus_cmd_array_i[11]), .Z(N57) );
  GTECH_OR2 C221 ( .A(N56), .B(N57), .Z(N58) );
  GTECH_NOT I_2 ( .A(N58), .Z(N59) );
  GTECH_NOT I_3 ( .A(mbus_cmd_array_i[11]), .Z(N60) );
  GTECH_OR2 C224 ( .A(mbus_cmd_array_i[10]), .B(N60), .Z(N61) );
  GTECH_OR2 C225 ( .A(mbus_cmd_array_i[9]), .B(N61), .Z(N62) );
  GTECH_NOT I_4 ( .A(N62), .Z(N63) );
  GTECH_NOT I_5 ( .A(fifo_status_full_array_i[3]), .Z(N64) );
  GTECH_NOT I_6 ( .A(mbus_cmd_array_i[7]), .Z(N65) );
  GTECH_NOT I_7 ( .A(mbus_cmd_array_i[6]), .Z(N66) );
  GTECH_OR2 C230 ( .A(N65), .B(mbus_cmd_array_i[8]), .Z(N67) );
  GTECH_OR2 C231 ( .A(N66), .B(N67), .Z(N68) );
  GTECH_NOT I_8 ( .A(N68), .Z(N69) );
  GTECH_NOT I_9 ( .A(mbus_cmd_array_i[8]), .Z(N70) );
  GTECH_OR2 C234 ( .A(mbus_cmd_array_i[7]), .B(N70), .Z(N71) );
  GTECH_OR2 C235 ( .A(mbus_cmd_array_i[6]), .B(N71), .Z(N72) );
  GTECH_NOT I_10 ( .A(N72), .Z(N73) );
  GTECH_NOT I_11 ( .A(fifo_status_full_array_i[2]), .Z(N74) );
  GTECH_NOT I_12 ( .A(mbus_cmd_array_i[4]), .Z(N75) );
  GTECH_NOT I_13 ( .A(mbus_cmd_array_i[3]), .Z(N76) );
  GTECH_OR2 C240 ( .A(N75), .B(mbus_cmd_array_i[5]), .Z(N77) );
  GTECH_OR2 C241 ( .A(N76), .B(N77), .Z(N78) );
  GTECH_NOT I_14 ( .A(N78), .Z(N79) );
  GTECH_NOT I_15 ( .A(mbus_cmd_array_i[5]), .Z(N80) );
  GTECH_OR2 C244 ( .A(mbus_cmd_array_i[4]), .B(N80), .Z(N81) );
  GTECH_OR2 C245 ( .A(mbus_cmd_array_i[3]), .B(N81), .Z(N82) );
  GTECH_NOT I_16 ( .A(N82), .Z(N83) );
  GTECH_NOT I_17 ( .A(fifo_status_full_array_i[1]), .Z(N84) );
  GTECH_NOT I_18 ( .A(mbus_cmd_array_i[1]), .Z(N85) );
  GTECH_NOT I_19 ( .A(mbus_cmd_array_i[0]), .Z(N86) );
  GTECH_OR2 C250 ( .A(N85), .B(mbus_cmd_array_i[2]), .Z(N87) );
  GTECH_OR2 C251 ( .A(N86), .B(N87), .Z(N88) );
  GTECH_NOT I_20 ( .A(N88), .Z(N89) );
  GTECH_NOT I_21 ( .A(mbus_cmd_array_i[2]), .Z(N90) );
  GTECH_OR2 C254 ( .A(mbus_cmd_array_i[1]), .B(N90), .Z(N91) );
  GTECH_OR2 C255 ( .A(mbus_cmd_array_i[0]), .B(N91), .Z(N92) );
  GTECH_NOT I_22 ( .A(N92), .Z(N93) );
  GTECH_NOT I_23 ( .A(fifo_status_full_array_i[0]), .Z(N94) );
  ADD_UNS_OP add_373 ( .A(breq_id_array_o[19:17]), .B(1'b1), .Z({N54, N53, N52}) );
  SELECT_OP C294 ( .DATA1(fifos_priority), .DATA2({fifos_priority[2:0], 
        fifos_priority[3]}), .DATA3({fifos_priority[1:0], fifos_priority[3:2]}), .DATA4({fifos_priority[0], fifos_priority[3:1]}), .DATA5({1'b0, 1'b0, 1'b0, 
        1'b0}), .CONTROL1(N0), .CONTROL2(N22), .CONTROL3(N25), .CONTROL4(N28), 
        .CONTROL5(N20), .Z(fifo_select_oh) );
  GTECH_BUF B_0 ( .A(N13), .Z(N0) );
  SELECT_OP C295 ( .DATA1({1'b0, 1'b1}), .DATA2({1'b1, 1'b0}), .DATA3({1'b0, 
        1'b0}), .CONTROL1(N1), .CONTROL2(N2), .CONTROL3(N34), .Z({N36, N35})
         );
  GTECH_BUF B_1 ( .A(N59), .Z(N1) );
  GTECH_BUF B_2 ( .A(N63), .Z(N2) );
  SELECT_OP C296 ( .DATA1({1'b0, 1'b1}), .DATA2({1'b1, 1'b0}), .DATA3({1'b0, 
        1'b0}), .CONTROL1(N3), .CONTROL2(N4), .CONTROL3(N38), .Z({N40, N39})
         );
  GTECH_BUF B_3 ( .A(N69), .Z(N3) );
  GTECH_BUF B_4 ( .A(N73), .Z(N4) );
  SELECT_OP C297 ( .DATA1({1'b0, 1'b1}), .DATA2({1'b1, 1'b0}), .DATA3({1'b0, 
        1'b0}), .CONTROL1(N5), .CONTROL2(N6), .CONTROL3(N42), .Z({N44, N43})
         );
  GTECH_BUF B_5 ( .A(N79), .Z(N5) );
  GTECH_BUF B_6 ( .A(N83), .Z(N6) );
  SELECT_OP C298 ( .DATA1({1'b0, 1'b1}), .DATA2({1'b1, 1'b0}), .DATA3({1'b0, 
        1'b0}), .CONTROL1(N7), .CONTROL2(N8), .CONTROL3(N46), .Z({N48, N47})
         );
  GTECH_BUF B_7 ( .A(N89), .Z(N7) );
  GTECH_BUF B_8 ( .A(N93), .Z(N8) );
  GTECH_AND2 C301 ( .A(N95), .B(fifo_select_oh[3]), .Z(fifo_rd_array_o[3]) );
  GTECH_NOT I_24 ( .A(broad_fifo_status_full_i), .Z(N95) );
  GTECH_AND2 C303 ( .A(N95), .B(fifo_select_oh[2]), .Z(fifo_rd_array_o[2]) );
  GTECH_AND2 C305 ( .A(N95), .B(fifo_select_oh[1]), .Z(fifo_rd_array_o[1]) );
  GTECH_AND2 C307 ( .A(N95), .B(fifo_select_oh[0]), .Z(fifo_rd_array_o[0]) );
  GTECH_OR2 C309 ( .A(N97), .B(fifo_rd_array_o[0]), .Z(broad_fifo_wr_o) );
  GTECH_OR2 C310 ( .A(N96), .B(fifo_rd_array_o[1]), .Z(N97) );
  GTECH_OR2 C311 ( .A(fifo_rd_array_o[3]), .B(fifo_rd_array_o[2]), .Z(N96) );
  GTECH_NOT I_25 ( .A(fifo_status_empty_array_i[3]), .Z(N9) );
  GTECH_NOT I_26 ( .A(fifo_status_empty_array_i[2]), .Z(N10) );
  GTECH_NOT I_27 ( .A(fifo_status_empty_array_i[1]), .Z(N11) );
  GTECH_NOT I_28 ( .A(fifo_status_empty_array_i[0]), .Z(N12) );
  GTECH_OR2 C317 ( .A(N102), .B(N103), .Z(N13) );
  GTECH_OR2 C318 ( .A(N100), .B(N101), .Z(N102) );
  GTECH_OR2 C319 ( .A(N98), .B(N99), .Z(N100) );
  GTECH_AND2 C320 ( .A(N9), .B(fifos_priority[3]), .Z(N98) );
  GTECH_AND2 C321 ( .A(N10), .B(fifos_priority[2]), .Z(N99) );
  GTECH_AND2 C322 ( .A(N11), .B(fifos_priority[1]), .Z(N101) );
  GTECH_AND2 C323 ( .A(N12), .B(fifos_priority[0]), .Z(N103) );
  GTECH_OR2 C324 ( .A(N108), .B(N109), .Z(N14) );
  GTECH_OR2 C325 ( .A(N106), .B(N107), .Z(N108) );
  GTECH_OR2 C326 ( .A(N104), .B(N105), .Z(N106) );
  GTECH_AND2 C327 ( .A(N9), .B(fifos_priority[2]), .Z(N104) );
  GTECH_AND2 C328 ( .A(N10), .B(fifos_priority[1]), .Z(N105) );
  GTECH_AND2 C329 ( .A(N11), .B(fifos_priority[0]), .Z(N107) );
  GTECH_AND2 C330 ( .A(N12), .B(fifos_priority[3]), .Z(N109) );
  GTECH_OR2 C331 ( .A(N114), .B(N115), .Z(N15) );
  GTECH_OR2 C332 ( .A(N112), .B(N113), .Z(N114) );
  GTECH_OR2 C333 ( .A(N110), .B(N111), .Z(N112) );
  GTECH_AND2 C334 ( .A(N9), .B(fifos_priority[1]), .Z(N110) );
  GTECH_AND2 C335 ( .A(N10), .B(fifos_priority[0]), .Z(N111) );
  GTECH_AND2 C336 ( .A(N11), .B(fifos_priority[3]), .Z(N113) );
  GTECH_AND2 C337 ( .A(N12), .B(fifos_priority[2]), .Z(N115) );
  GTECH_OR2 C338 ( .A(N120), .B(N121), .Z(N16) );
  GTECH_OR2 C339 ( .A(N118), .B(N119), .Z(N120) );
  GTECH_OR2 C340 ( .A(N116), .B(N117), .Z(N118) );
  GTECH_AND2 C341 ( .A(N9), .B(fifos_priority[0]), .Z(N116) );
  GTECH_AND2 C342 ( .A(N10), .B(fifos_priority[3]), .Z(N117) );
  GTECH_AND2 C343 ( .A(N11), .B(fifos_priority[2]), .Z(N119) );
  GTECH_AND2 C344 ( .A(N12), .B(fifos_priority[1]), .Z(N121) );
  GTECH_OR2 C349 ( .A(N14), .B(N13), .Z(N17) );
  GTECH_OR2 C350 ( .A(N15), .B(N17), .Z(N18) );
  GTECH_OR2 C351 ( .A(N16), .B(N18), .Z(N19) );
  GTECH_NOT I_29 ( .A(N19), .Z(N20) );
  GTECH_NOT I_30 ( .A(N13), .Z(N21) );
  GTECH_AND2 C354 ( .A(N14), .B(N21), .Z(N22) );
  GTECH_NOT I_31 ( .A(N14), .Z(N23) );
  GTECH_AND2 C356 ( .A(N21), .B(N23), .Z(N24) );
  GTECH_AND2 C357 ( .A(N15), .B(N24), .Z(N25) );
  GTECH_NOT I_32 ( .A(N15), .Z(N26) );
  GTECH_AND2 C359 ( .A(N24), .B(N26), .Z(N27) );
  GTECH_AND2 C360 ( .A(N16), .B(N27), .Z(N28) );
  GTECH_OR2 C361 ( .A(N126), .B(N127), .Z(broad_addr_o[31]) );
  GTECH_OR2 C362 ( .A(N124), .B(N125), .Z(N126) );
  GTECH_OR2 C363 ( .A(N122), .B(N123), .Z(N124) );
  GTECH_AND2 C364 ( .A(broad_addr_array_i[127]), .B(fifo_select_oh[3]), .Z(
        N122) );
  GTECH_AND2 C365 ( .A(broad_addr_array_i[95]), .B(fifo_select_oh[2]), .Z(N123) );
  GTECH_AND2 C366 ( .A(broad_addr_array_i[63]), .B(fifo_select_oh[1]), .Z(N125) );
  GTECH_AND2 C367 ( .A(broad_addr_array_i[31]), .B(fifo_select_oh[0]), .Z(N127) );
  GTECH_OR2 C368 ( .A(N132), .B(N133), .Z(broad_addr_o[30]) );
  GTECH_OR2 C369 ( .A(N130), .B(N131), .Z(N132) );
  GTECH_OR2 C370 ( .A(N128), .B(N129), .Z(N130) );
  GTECH_AND2 C371 ( .A(broad_addr_array_i[126]), .B(fifo_select_oh[3]), .Z(
        N128) );
  GTECH_AND2 C372 ( .A(broad_addr_array_i[94]), .B(fifo_select_oh[2]), .Z(N129) );
  GTECH_AND2 C373 ( .A(broad_addr_array_i[62]), .B(fifo_select_oh[1]), .Z(N131) );
  GTECH_AND2 C374 ( .A(broad_addr_array_i[30]), .B(fifo_select_oh[0]), .Z(N133) );
  GTECH_OR2 C375 ( .A(N138), .B(N139), .Z(broad_addr_o[29]) );
  GTECH_OR2 C376 ( .A(N136), .B(N137), .Z(N138) );
  GTECH_OR2 C377 ( .A(N134), .B(N135), .Z(N136) );
  GTECH_AND2 C378 ( .A(broad_addr_array_i[125]), .B(fifo_select_oh[3]), .Z(
        N134) );
  GTECH_AND2 C379 ( .A(broad_addr_array_i[93]), .B(fifo_select_oh[2]), .Z(N135) );
  GTECH_AND2 C380 ( .A(broad_addr_array_i[61]), .B(fifo_select_oh[1]), .Z(N137) );
  GTECH_AND2 C381 ( .A(broad_addr_array_i[29]), .B(fifo_select_oh[0]), .Z(N139) );
  GTECH_OR2 C382 ( .A(N144), .B(N145), .Z(broad_addr_o[28]) );
  GTECH_OR2 C383 ( .A(N142), .B(N143), .Z(N144) );
  GTECH_OR2 C384 ( .A(N140), .B(N141), .Z(N142) );
  GTECH_AND2 C385 ( .A(broad_addr_array_i[124]), .B(fifo_select_oh[3]), .Z(
        N140) );
  GTECH_AND2 C386 ( .A(broad_addr_array_i[92]), .B(fifo_select_oh[2]), .Z(N141) );
  GTECH_AND2 C387 ( .A(broad_addr_array_i[60]), .B(fifo_select_oh[1]), .Z(N143) );
  GTECH_AND2 C388 ( .A(broad_addr_array_i[28]), .B(fifo_select_oh[0]), .Z(N145) );
  GTECH_OR2 C389 ( .A(N150), .B(N151), .Z(broad_addr_o[27]) );
  GTECH_OR2 C390 ( .A(N148), .B(N149), .Z(N150) );
  GTECH_OR2 C391 ( .A(N146), .B(N147), .Z(N148) );
  GTECH_AND2 C392 ( .A(broad_addr_array_i[123]), .B(fifo_select_oh[3]), .Z(
        N146) );
  GTECH_AND2 C393 ( .A(broad_addr_array_i[91]), .B(fifo_select_oh[2]), .Z(N147) );
  GTECH_AND2 C394 ( .A(broad_addr_array_i[59]), .B(fifo_select_oh[1]), .Z(N149) );
  GTECH_AND2 C395 ( .A(broad_addr_array_i[27]), .B(fifo_select_oh[0]), .Z(N151) );
  GTECH_OR2 C396 ( .A(N156), .B(N157), .Z(broad_addr_o[26]) );
  GTECH_OR2 C397 ( .A(N154), .B(N155), .Z(N156) );
  GTECH_OR2 C398 ( .A(N152), .B(N153), .Z(N154) );
  GTECH_AND2 C399 ( .A(broad_addr_array_i[122]), .B(fifo_select_oh[3]), .Z(
        N152) );
  GTECH_AND2 C400 ( .A(broad_addr_array_i[90]), .B(fifo_select_oh[2]), .Z(N153) );
  GTECH_AND2 C401 ( .A(broad_addr_array_i[58]), .B(fifo_select_oh[1]), .Z(N155) );
  GTECH_AND2 C402 ( .A(broad_addr_array_i[26]), .B(fifo_select_oh[0]), .Z(N157) );
  GTECH_OR2 C403 ( .A(N162), .B(N163), .Z(broad_addr_o[25]) );
  GTECH_OR2 C404 ( .A(N160), .B(N161), .Z(N162) );
  GTECH_OR2 C405 ( .A(N158), .B(N159), .Z(N160) );
  GTECH_AND2 C406 ( .A(broad_addr_array_i[121]), .B(fifo_select_oh[3]), .Z(
        N158) );
  GTECH_AND2 C407 ( .A(broad_addr_array_i[89]), .B(fifo_select_oh[2]), .Z(N159) );
  GTECH_AND2 C408 ( .A(broad_addr_array_i[57]), .B(fifo_select_oh[1]), .Z(N161) );
  GTECH_AND2 C409 ( .A(broad_addr_array_i[25]), .B(fifo_select_oh[0]), .Z(N163) );
  GTECH_OR2 C410 ( .A(N168), .B(N169), .Z(broad_addr_o[24]) );
  GTECH_OR2 C411 ( .A(N166), .B(N167), .Z(N168) );
  GTECH_OR2 C412 ( .A(N164), .B(N165), .Z(N166) );
  GTECH_AND2 C413 ( .A(broad_addr_array_i[120]), .B(fifo_select_oh[3]), .Z(
        N164) );
  GTECH_AND2 C414 ( .A(broad_addr_array_i[88]), .B(fifo_select_oh[2]), .Z(N165) );
  GTECH_AND2 C415 ( .A(broad_addr_array_i[56]), .B(fifo_select_oh[1]), .Z(N167) );
  GTECH_AND2 C416 ( .A(broad_addr_array_i[24]), .B(fifo_select_oh[0]), .Z(N169) );
  GTECH_OR2 C417 ( .A(N174), .B(N175), .Z(broad_addr_o[23]) );
  GTECH_OR2 C418 ( .A(N172), .B(N173), .Z(N174) );
  GTECH_OR2 C419 ( .A(N170), .B(N171), .Z(N172) );
  GTECH_AND2 C420 ( .A(broad_addr_array_i[119]), .B(fifo_select_oh[3]), .Z(
        N170) );
  GTECH_AND2 C421 ( .A(broad_addr_array_i[87]), .B(fifo_select_oh[2]), .Z(N171) );
  GTECH_AND2 C422 ( .A(broad_addr_array_i[55]), .B(fifo_select_oh[1]), .Z(N173) );
  GTECH_AND2 C423 ( .A(broad_addr_array_i[23]), .B(fifo_select_oh[0]), .Z(N175) );
  GTECH_OR2 C424 ( .A(N180), .B(N181), .Z(broad_addr_o[22]) );
  GTECH_OR2 C425 ( .A(N178), .B(N179), .Z(N180) );
  GTECH_OR2 C426 ( .A(N176), .B(N177), .Z(N178) );
  GTECH_AND2 C427 ( .A(broad_addr_array_i[118]), .B(fifo_select_oh[3]), .Z(
        N176) );
  GTECH_AND2 C428 ( .A(broad_addr_array_i[86]), .B(fifo_select_oh[2]), .Z(N177) );
  GTECH_AND2 C429 ( .A(broad_addr_array_i[54]), .B(fifo_select_oh[1]), .Z(N179) );
  GTECH_AND2 C430 ( .A(broad_addr_array_i[22]), .B(fifo_select_oh[0]), .Z(N181) );
  GTECH_OR2 C431 ( .A(N186), .B(N187), .Z(broad_addr_o[21]) );
  GTECH_OR2 C432 ( .A(N184), .B(N185), .Z(N186) );
  GTECH_OR2 C433 ( .A(N182), .B(N183), .Z(N184) );
  GTECH_AND2 C434 ( .A(broad_addr_array_i[117]), .B(fifo_select_oh[3]), .Z(
        N182) );
  GTECH_AND2 C435 ( .A(broad_addr_array_i[85]), .B(fifo_select_oh[2]), .Z(N183) );
  GTECH_AND2 C436 ( .A(broad_addr_array_i[53]), .B(fifo_select_oh[1]), .Z(N185) );
  GTECH_AND2 C437 ( .A(broad_addr_array_i[21]), .B(fifo_select_oh[0]), .Z(N187) );
  GTECH_OR2 C438 ( .A(N192), .B(N193), .Z(broad_addr_o[20]) );
  GTECH_OR2 C439 ( .A(N190), .B(N191), .Z(N192) );
  GTECH_OR2 C440 ( .A(N188), .B(N189), .Z(N190) );
  GTECH_AND2 C441 ( .A(broad_addr_array_i[116]), .B(fifo_select_oh[3]), .Z(
        N188) );
  GTECH_AND2 C442 ( .A(broad_addr_array_i[84]), .B(fifo_select_oh[2]), .Z(N189) );
  GTECH_AND2 C443 ( .A(broad_addr_array_i[52]), .B(fifo_select_oh[1]), .Z(N191) );
  GTECH_AND2 C444 ( .A(broad_addr_array_i[20]), .B(fifo_select_oh[0]), .Z(N193) );
  GTECH_OR2 C445 ( .A(N198), .B(N199), .Z(broad_addr_o[19]) );
  GTECH_OR2 C446 ( .A(N196), .B(N197), .Z(N198) );
  GTECH_OR2 C447 ( .A(N194), .B(N195), .Z(N196) );
  GTECH_AND2 C448 ( .A(broad_addr_array_i[115]), .B(fifo_select_oh[3]), .Z(
        N194) );
  GTECH_AND2 C449 ( .A(broad_addr_array_i[83]), .B(fifo_select_oh[2]), .Z(N195) );
  GTECH_AND2 C450 ( .A(broad_addr_array_i[51]), .B(fifo_select_oh[1]), .Z(N197) );
  GTECH_AND2 C451 ( .A(broad_addr_array_i[19]), .B(fifo_select_oh[0]), .Z(N199) );
  GTECH_OR2 C452 ( .A(N204), .B(N205), .Z(broad_addr_o[18]) );
  GTECH_OR2 C453 ( .A(N202), .B(N203), .Z(N204) );
  GTECH_OR2 C454 ( .A(N200), .B(N201), .Z(N202) );
  GTECH_AND2 C455 ( .A(broad_addr_array_i[114]), .B(fifo_select_oh[3]), .Z(
        N200) );
  GTECH_AND2 C456 ( .A(broad_addr_array_i[82]), .B(fifo_select_oh[2]), .Z(N201) );
  GTECH_AND2 C457 ( .A(broad_addr_array_i[50]), .B(fifo_select_oh[1]), .Z(N203) );
  GTECH_AND2 C458 ( .A(broad_addr_array_i[18]), .B(fifo_select_oh[0]), .Z(N205) );
  GTECH_OR2 C459 ( .A(N210), .B(N211), .Z(broad_addr_o[17]) );
  GTECH_OR2 C460 ( .A(N208), .B(N209), .Z(N210) );
  GTECH_OR2 C461 ( .A(N206), .B(N207), .Z(N208) );
  GTECH_AND2 C462 ( .A(broad_addr_array_i[113]), .B(fifo_select_oh[3]), .Z(
        N206) );
  GTECH_AND2 C463 ( .A(broad_addr_array_i[81]), .B(fifo_select_oh[2]), .Z(N207) );
  GTECH_AND2 C464 ( .A(broad_addr_array_i[49]), .B(fifo_select_oh[1]), .Z(N209) );
  GTECH_AND2 C465 ( .A(broad_addr_array_i[17]), .B(fifo_select_oh[0]), .Z(N211) );
  GTECH_OR2 C466 ( .A(N216), .B(N217), .Z(broad_addr_o[16]) );
  GTECH_OR2 C467 ( .A(N214), .B(N215), .Z(N216) );
  GTECH_OR2 C468 ( .A(N212), .B(N213), .Z(N214) );
  GTECH_AND2 C469 ( .A(broad_addr_array_i[112]), .B(fifo_select_oh[3]), .Z(
        N212) );
  GTECH_AND2 C470 ( .A(broad_addr_array_i[80]), .B(fifo_select_oh[2]), .Z(N213) );
  GTECH_AND2 C471 ( .A(broad_addr_array_i[48]), .B(fifo_select_oh[1]), .Z(N215) );
  GTECH_AND2 C472 ( .A(broad_addr_array_i[16]), .B(fifo_select_oh[0]), .Z(N217) );
  GTECH_OR2 C473 ( .A(N222), .B(N223), .Z(broad_addr_o[15]) );
  GTECH_OR2 C474 ( .A(N220), .B(N221), .Z(N222) );
  GTECH_OR2 C475 ( .A(N218), .B(N219), .Z(N220) );
  GTECH_AND2 C476 ( .A(broad_addr_array_i[111]), .B(fifo_select_oh[3]), .Z(
        N218) );
  GTECH_AND2 C477 ( .A(broad_addr_array_i[79]), .B(fifo_select_oh[2]), .Z(N219) );
  GTECH_AND2 C478 ( .A(broad_addr_array_i[47]), .B(fifo_select_oh[1]), .Z(N221) );
  GTECH_AND2 C479 ( .A(broad_addr_array_i[15]), .B(fifo_select_oh[0]), .Z(N223) );
  GTECH_OR2 C480 ( .A(N228), .B(N229), .Z(broad_addr_o[14]) );
  GTECH_OR2 C481 ( .A(N226), .B(N227), .Z(N228) );
  GTECH_OR2 C482 ( .A(N224), .B(N225), .Z(N226) );
  GTECH_AND2 C483 ( .A(broad_addr_array_i[110]), .B(fifo_select_oh[3]), .Z(
        N224) );
  GTECH_AND2 C484 ( .A(broad_addr_array_i[78]), .B(fifo_select_oh[2]), .Z(N225) );
  GTECH_AND2 C485 ( .A(broad_addr_array_i[46]), .B(fifo_select_oh[1]), .Z(N227) );
  GTECH_AND2 C486 ( .A(broad_addr_array_i[14]), .B(fifo_select_oh[0]), .Z(N229) );
  GTECH_OR2 C487 ( .A(N234), .B(N235), .Z(broad_addr_o[13]) );
  GTECH_OR2 C488 ( .A(N232), .B(N233), .Z(N234) );
  GTECH_OR2 C489 ( .A(N230), .B(N231), .Z(N232) );
  GTECH_AND2 C490 ( .A(broad_addr_array_i[109]), .B(fifo_select_oh[3]), .Z(
        N230) );
  GTECH_AND2 C491 ( .A(broad_addr_array_i[77]), .B(fifo_select_oh[2]), .Z(N231) );
  GTECH_AND2 C492 ( .A(broad_addr_array_i[45]), .B(fifo_select_oh[1]), .Z(N233) );
  GTECH_AND2 C493 ( .A(broad_addr_array_i[13]), .B(fifo_select_oh[0]), .Z(N235) );
  GTECH_OR2 C494 ( .A(N240), .B(N241), .Z(broad_addr_o[12]) );
  GTECH_OR2 C495 ( .A(N238), .B(N239), .Z(N240) );
  GTECH_OR2 C496 ( .A(N236), .B(N237), .Z(N238) );
  GTECH_AND2 C497 ( .A(broad_addr_array_i[108]), .B(fifo_select_oh[3]), .Z(
        N236) );
  GTECH_AND2 C498 ( .A(broad_addr_array_i[76]), .B(fifo_select_oh[2]), .Z(N237) );
  GTECH_AND2 C499 ( .A(broad_addr_array_i[44]), .B(fifo_select_oh[1]), .Z(N239) );
  GTECH_AND2 C500 ( .A(broad_addr_array_i[12]), .B(fifo_select_oh[0]), .Z(N241) );
  GTECH_OR2 C501 ( .A(N246), .B(N247), .Z(broad_addr_o[11]) );
  GTECH_OR2 C502 ( .A(N244), .B(N245), .Z(N246) );
  GTECH_OR2 C503 ( .A(N242), .B(N243), .Z(N244) );
  GTECH_AND2 C504 ( .A(broad_addr_array_i[107]), .B(fifo_select_oh[3]), .Z(
        N242) );
  GTECH_AND2 C505 ( .A(broad_addr_array_i[75]), .B(fifo_select_oh[2]), .Z(N243) );
  GTECH_AND2 C506 ( .A(broad_addr_array_i[43]), .B(fifo_select_oh[1]), .Z(N245) );
  GTECH_AND2 C507 ( .A(broad_addr_array_i[11]), .B(fifo_select_oh[0]), .Z(N247) );
  GTECH_OR2 C508 ( .A(N252), .B(N253), .Z(broad_addr_o[10]) );
  GTECH_OR2 C509 ( .A(N250), .B(N251), .Z(N252) );
  GTECH_OR2 C510 ( .A(N248), .B(N249), .Z(N250) );
  GTECH_AND2 C511 ( .A(broad_addr_array_i[106]), .B(fifo_select_oh[3]), .Z(
        N248) );
  GTECH_AND2 C512 ( .A(broad_addr_array_i[74]), .B(fifo_select_oh[2]), .Z(N249) );
  GTECH_AND2 C513 ( .A(broad_addr_array_i[42]), .B(fifo_select_oh[1]), .Z(N251) );
  GTECH_AND2 C514 ( .A(broad_addr_array_i[10]), .B(fifo_select_oh[0]), .Z(N253) );
  GTECH_OR2 C515 ( .A(N258), .B(N259), .Z(broad_addr_o[9]) );
  GTECH_OR2 C516 ( .A(N256), .B(N257), .Z(N258) );
  GTECH_OR2 C517 ( .A(N254), .B(N255), .Z(N256) );
  GTECH_AND2 C518 ( .A(broad_addr_array_i[105]), .B(fifo_select_oh[3]), .Z(
        N254) );
  GTECH_AND2 C519 ( .A(broad_addr_array_i[73]), .B(fifo_select_oh[2]), .Z(N255) );
  GTECH_AND2 C520 ( .A(broad_addr_array_i[41]), .B(fifo_select_oh[1]), .Z(N257) );
  GTECH_AND2 C521 ( .A(broad_addr_array_i[9]), .B(fifo_select_oh[0]), .Z(N259)
         );
  GTECH_OR2 C522 ( .A(N264), .B(N265), .Z(broad_addr_o[8]) );
  GTECH_OR2 C523 ( .A(N262), .B(N263), .Z(N264) );
  GTECH_OR2 C524 ( .A(N260), .B(N261), .Z(N262) );
  GTECH_AND2 C525 ( .A(broad_addr_array_i[104]), .B(fifo_select_oh[3]), .Z(
        N260) );
  GTECH_AND2 C526 ( .A(broad_addr_array_i[72]), .B(fifo_select_oh[2]), .Z(N261) );
  GTECH_AND2 C527 ( .A(broad_addr_array_i[40]), .B(fifo_select_oh[1]), .Z(N263) );
  GTECH_AND2 C528 ( .A(broad_addr_array_i[8]), .B(fifo_select_oh[0]), .Z(N265)
         );
  GTECH_OR2 C529 ( .A(N270), .B(N271), .Z(broad_addr_o[7]) );
  GTECH_OR2 C530 ( .A(N268), .B(N269), .Z(N270) );
  GTECH_OR2 C531 ( .A(N266), .B(N267), .Z(N268) );
  GTECH_AND2 C532 ( .A(broad_addr_array_i[103]), .B(fifo_select_oh[3]), .Z(
        N266) );
  GTECH_AND2 C533 ( .A(broad_addr_array_i[71]), .B(fifo_select_oh[2]), .Z(N267) );
  GTECH_AND2 C534 ( .A(broad_addr_array_i[39]), .B(fifo_select_oh[1]), .Z(N269) );
  GTECH_AND2 C535 ( .A(broad_addr_array_i[7]), .B(fifo_select_oh[0]), .Z(N271)
         );
  GTECH_OR2 C536 ( .A(N276), .B(N277), .Z(broad_addr_o[6]) );
  GTECH_OR2 C537 ( .A(N274), .B(N275), .Z(N276) );
  GTECH_OR2 C538 ( .A(N272), .B(N273), .Z(N274) );
  GTECH_AND2 C539 ( .A(broad_addr_array_i[102]), .B(fifo_select_oh[3]), .Z(
        N272) );
  GTECH_AND2 C540 ( .A(broad_addr_array_i[70]), .B(fifo_select_oh[2]), .Z(N273) );
  GTECH_AND2 C541 ( .A(broad_addr_array_i[38]), .B(fifo_select_oh[1]), .Z(N275) );
  GTECH_AND2 C542 ( .A(broad_addr_array_i[6]), .B(fifo_select_oh[0]), .Z(N277)
         );
  GTECH_OR2 C543 ( .A(N282), .B(N283), .Z(broad_addr_o[5]) );
  GTECH_OR2 C544 ( .A(N280), .B(N281), .Z(N282) );
  GTECH_OR2 C545 ( .A(N278), .B(N279), .Z(N280) );
  GTECH_AND2 C546 ( .A(broad_addr_array_i[101]), .B(fifo_select_oh[3]), .Z(
        N278) );
  GTECH_AND2 C547 ( .A(broad_addr_array_i[69]), .B(fifo_select_oh[2]), .Z(N279) );
  GTECH_AND2 C548 ( .A(broad_addr_array_i[37]), .B(fifo_select_oh[1]), .Z(N281) );
  GTECH_AND2 C549 ( .A(broad_addr_array_i[5]), .B(fifo_select_oh[0]), .Z(N283)
         );
  GTECH_OR2 C550 ( .A(N288), .B(N289), .Z(broad_addr_o[4]) );
  GTECH_OR2 C551 ( .A(N286), .B(N287), .Z(N288) );
  GTECH_OR2 C552 ( .A(N284), .B(N285), .Z(N286) );
  GTECH_AND2 C553 ( .A(broad_addr_array_i[100]), .B(fifo_select_oh[3]), .Z(
        N284) );
  GTECH_AND2 C554 ( .A(broad_addr_array_i[68]), .B(fifo_select_oh[2]), .Z(N285) );
  GTECH_AND2 C555 ( .A(broad_addr_array_i[36]), .B(fifo_select_oh[1]), .Z(N287) );
  GTECH_AND2 C556 ( .A(broad_addr_array_i[4]), .B(fifo_select_oh[0]), .Z(N289)
         );
  GTECH_OR2 C557 ( .A(N294), .B(N295), .Z(broad_addr_o[3]) );
  GTECH_OR2 C558 ( .A(N292), .B(N293), .Z(N294) );
  GTECH_OR2 C559 ( .A(N290), .B(N291), .Z(N292) );
  GTECH_AND2 C560 ( .A(broad_addr_array_i[99]), .B(fifo_select_oh[3]), .Z(N290) );
  GTECH_AND2 C561 ( .A(broad_addr_array_i[67]), .B(fifo_select_oh[2]), .Z(N291) );
  GTECH_AND2 C562 ( .A(broad_addr_array_i[35]), .B(fifo_select_oh[1]), .Z(N293) );
  GTECH_AND2 C563 ( .A(broad_addr_array_i[3]), .B(fifo_select_oh[0]), .Z(N295)
         );
  GTECH_OR2 C564 ( .A(N300), .B(N301), .Z(broad_addr_o[2]) );
  GTECH_OR2 C565 ( .A(N298), .B(N299), .Z(N300) );
  GTECH_OR2 C566 ( .A(N296), .B(N297), .Z(N298) );
  GTECH_AND2 C567 ( .A(broad_addr_array_i[98]), .B(fifo_select_oh[3]), .Z(N296) );
  GTECH_AND2 C568 ( .A(broad_addr_array_i[66]), .B(fifo_select_oh[2]), .Z(N297) );
  GTECH_AND2 C569 ( .A(broad_addr_array_i[34]), .B(fifo_select_oh[1]), .Z(N299) );
  GTECH_AND2 C570 ( .A(broad_addr_array_i[2]), .B(fifo_select_oh[0]), .Z(N301)
         );
  GTECH_OR2 C571 ( .A(N306), .B(N307), .Z(broad_addr_o[1]) );
  GTECH_OR2 C572 ( .A(N304), .B(N305), .Z(N306) );
  GTECH_OR2 C573 ( .A(N302), .B(N303), .Z(N304) );
  GTECH_AND2 C574 ( .A(broad_addr_array_i[97]), .B(fifo_select_oh[3]), .Z(N302) );
  GTECH_AND2 C575 ( .A(broad_addr_array_i[65]), .B(fifo_select_oh[2]), .Z(N303) );
  GTECH_AND2 C576 ( .A(broad_addr_array_i[33]), .B(fifo_select_oh[1]), .Z(N305) );
  GTECH_AND2 C577 ( .A(broad_addr_array_i[1]), .B(fifo_select_oh[0]), .Z(N307)
         );
  GTECH_OR2 C578 ( .A(N312), .B(N313), .Z(broad_addr_o[0]) );
  GTECH_OR2 C579 ( .A(N310), .B(N311), .Z(N312) );
  GTECH_OR2 C580 ( .A(N308), .B(N309), .Z(N310) );
  GTECH_AND2 C581 ( .A(broad_addr_array_i[96]), .B(fifo_select_oh[3]), .Z(N308) );
  GTECH_AND2 C582 ( .A(broad_addr_array_i[64]), .B(fifo_select_oh[2]), .Z(N309) );
  GTECH_AND2 C583 ( .A(broad_addr_array_i[32]), .B(fifo_select_oh[1]), .Z(N311) );
  GTECH_AND2 C584 ( .A(broad_addr_array_i[0]), .B(fifo_select_oh[0]), .Z(N313)
         );
  GTECH_OR2 C585 ( .A(N318), .B(N319), .Z(broad_type_o[1]) );
  GTECH_OR2 C586 ( .A(N316), .B(N317), .Z(N318) );
  GTECH_OR2 C587 ( .A(N314), .B(N315), .Z(N316) );
  GTECH_AND2 C588 ( .A(broad_type_array_i[7]), .B(fifo_select_oh[3]), .Z(N314)
         );
  GTECH_AND2 C589 ( .A(broad_type_array_i[5]), .B(fifo_select_oh[2]), .Z(N315)
         );
  GTECH_AND2 C590 ( .A(broad_type_array_i[3]), .B(fifo_select_oh[1]), .Z(N317)
         );
  GTECH_AND2 C591 ( .A(broad_type_array_i[1]), .B(fifo_select_oh[0]), .Z(N319)
         );
  GTECH_OR2 C592 ( .A(N324), .B(N325), .Z(broad_type_o[0]) );
  GTECH_OR2 C593 ( .A(N322), .B(N323), .Z(N324) );
  GTECH_OR2 C594 ( .A(N320), .B(N321), .Z(N322) );
  GTECH_AND2 C595 ( .A(broad_type_array_i[6]), .B(fifo_select_oh[3]), .Z(N320)
         );
  GTECH_AND2 C596 ( .A(broad_type_array_i[4]), .B(fifo_select_oh[2]), .Z(N321)
         );
  GTECH_AND2 C597 ( .A(broad_type_array_i[2]), .B(fifo_select_oh[1]), .Z(N323)
         );
  GTECH_AND2 C598 ( .A(broad_type_array_i[0]), .B(fifo_select_oh[0]), .Z(N325)
         );
  GTECH_OR2 C599 ( .A(fifo_select_oh[3]), .B(fifo_select_oh[2]), .Z(
        broad_cpu_id_o[1]) );
  GTECH_OR2 C600 ( .A(fifo_select_oh[3]), .B(fifo_select_oh[1]), .Z(
        broad_cpu_id_o[0]) );
  GTECH_OR2 C601 ( .A(N330), .B(N331), .Z(broad_id_o[4]) );
  GTECH_OR2 C602 ( .A(N328), .B(N329), .Z(N330) );
  GTECH_OR2 C603 ( .A(N326), .B(N327), .Z(N328) );
  GTECH_AND2 C604 ( .A(broad_id_array_i[19]), .B(fifo_select_oh[3]), .Z(N326)
         );
  GTECH_AND2 C605 ( .A(broad_id_array_i[14]), .B(fifo_select_oh[2]), .Z(N327)
         );
  GTECH_AND2 C606 ( .A(broad_id_array_i[9]), .B(fifo_select_oh[1]), .Z(N329)
         );
  GTECH_AND2 C607 ( .A(broad_id_array_i[4]), .B(fifo_select_oh[0]), .Z(N331)
         );
  GTECH_OR2 C608 ( .A(N336), .B(N337), .Z(broad_id_o[3]) );
  GTECH_OR2 C609 ( .A(N334), .B(N335), .Z(N336) );
  GTECH_OR2 C610 ( .A(N332), .B(N333), .Z(N334) );
  GTECH_AND2 C611 ( .A(broad_id_array_i[18]), .B(fifo_select_oh[3]), .Z(N332)
         );
  GTECH_AND2 C612 ( .A(broad_id_array_i[13]), .B(fifo_select_oh[2]), .Z(N333)
         );
  GTECH_AND2 C613 ( .A(broad_id_array_i[8]), .B(fifo_select_oh[1]), .Z(N335)
         );
  GTECH_AND2 C614 ( .A(broad_id_array_i[3]), .B(fifo_select_oh[0]), .Z(N337)
         );
  GTECH_OR2 C615 ( .A(N342), .B(N343), .Z(broad_id_o[2]) );
  GTECH_OR2 C616 ( .A(N340), .B(N341), .Z(N342) );
  GTECH_OR2 C617 ( .A(N338), .B(N339), .Z(N340) );
  GTECH_AND2 C618 ( .A(broad_id_array_i[17]), .B(fifo_select_oh[3]), .Z(N338)
         );
  GTECH_AND2 C619 ( .A(broad_id_array_i[12]), .B(fifo_select_oh[2]), .Z(N339)
         );
  GTECH_AND2 C620 ( .A(broad_id_array_i[7]), .B(fifo_select_oh[1]), .Z(N341)
         );
  GTECH_AND2 C621 ( .A(broad_id_array_i[2]), .B(fifo_select_oh[0]), .Z(N343)
         );
  GTECH_OR2 C622 ( .A(N348), .B(N349), .Z(broad_id_o[1]) );
  GTECH_OR2 C623 ( .A(N346), .B(N347), .Z(N348) );
  GTECH_OR2 C624 ( .A(N344), .B(N345), .Z(N346) );
  GTECH_AND2 C625 ( .A(broad_id_array_i[16]), .B(fifo_select_oh[3]), .Z(N344)
         );
  GTECH_AND2 C626 ( .A(broad_id_array_i[11]), .B(fifo_select_oh[2]), .Z(N345)
         );
  GTECH_AND2 C627 ( .A(broad_id_array_i[6]), .B(fifo_select_oh[1]), .Z(N347)
         );
  GTECH_AND2 C628 ( .A(broad_id_array_i[1]), .B(fifo_select_oh[0]), .Z(N349)
         );
  GTECH_OR2 C629 ( .A(N354), .B(N355), .Z(broad_id_o[0]) );
  GTECH_OR2 C630 ( .A(N352), .B(N353), .Z(N354) );
  GTECH_OR2 C631 ( .A(N350), .B(N351), .Z(N352) );
  GTECH_AND2 C632 ( .A(broad_id_array_i[15]), .B(fifo_select_oh[3]), .Z(N350)
         );
  GTECH_AND2 C633 ( .A(broad_id_array_i[10]), .B(fifo_select_oh[2]), .Z(N351)
         );
  GTECH_AND2 C634 ( .A(broad_id_array_i[5]), .B(fifo_select_oh[1]), .Z(N353)
         );
  GTECH_AND2 C635 ( .A(broad_id_array_i[0]), .B(fifo_select_oh[0]), .Z(N355)
         );
  GTECH_AND2 C636 ( .A(N358), .B(N64), .Z(N29) );
  GTECH_AND2 C637 ( .A(N356), .B(N357), .Z(N358) );
  GTECH_NOT I_33 ( .A(mbus_ack_array_o[3]), .Z(N356) );
  GTECH_OR2 C639 ( .A(N59), .B(N63), .Z(N357) );
  GTECH_AND2 C640 ( .A(N361), .B(N74), .Z(N30) );
  GTECH_AND2 C641 ( .A(N359), .B(N360), .Z(N361) );
  GTECH_NOT I_34 ( .A(mbus_ack_array_o[2]), .Z(N359) );
  GTECH_OR2 C643 ( .A(N69), .B(N73), .Z(N360) );
  GTECH_AND2 C644 ( .A(N364), .B(N84), .Z(N31) );
  GTECH_AND2 C645 ( .A(N362), .B(N363), .Z(N364) );
  GTECH_NOT I_35 ( .A(mbus_ack_array_o[1]), .Z(N362) );
  GTECH_OR2 C647 ( .A(N79), .B(N83), .Z(N363) );
  GTECH_AND2 C648 ( .A(N367), .B(N94), .Z(N32) );
  GTECH_AND2 C649 ( .A(N365), .B(N366), .Z(N367) );
  GTECH_NOT I_36 ( .A(mbus_ack_array_o[0]), .Z(N365) );
  GTECH_OR2 C651 ( .A(N89), .B(N93), .Z(N366) );
  GTECH_OR2 C654 ( .A(N63), .B(N59), .Z(N33) );
  GTECH_NOT I_37 ( .A(N33), .Z(N34) );
  GTECH_OR2 C658 ( .A(N73), .B(N69), .Z(N37) );
  GTECH_NOT I_38 ( .A(N37), .Z(N38) );
  GTECH_OR2 C662 ( .A(N83), .B(N79), .Z(N41) );
  GTECH_NOT I_39 ( .A(N41), .Z(N42) );
  GTECH_OR2 C666 ( .A(N93), .B(N89), .Z(N45) );
  GTECH_NOT I_40 ( .A(N45), .Z(N46) );
  GTECH_NOT I_41 ( .A(rst), .Z(N49) );
  GTECH_BUF B_9 ( .A(N49), .Z(N50) );
  GTECH_OR2 C671 ( .A(N369), .B(mbus_ack_array_o[0]), .Z(N51) );
  GTECH_OR2 C672 ( .A(N368), .B(mbus_ack_array_o[1]), .Z(N369) );
  GTECH_OR2 C673 ( .A(mbus_ack_array_o[3]), .B(mbus_ack_array_o[2]), .Z(N368)
         );
  GTECH_AND2 C675 ( .A(N50), .B(N51) );
endmodule


module mesi_isc_basic_fifo_DATA_WIDTH41_FIFO_SIZE2_FIFO_SIZE_LOG21 ( clk, rst, 
        wr_i, rd_i, data_i, data_o, status_empty_o, status_full_o );
  input [40:0] data_i;
  output [40:0] data_o;
  input clk, rst, wr_i, rd_i;
  output status_empty_o, status_full_o;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, N8, \entry[1][40] , \entry[1][39] ,
         \entry[1][38] , \entry[1][37] , \entry[1][36] , \entry[1][35] ,
         \entry[1][34] , \entry[1][33] , \entry[1][32] , \entry[1][31] ,
         \entry[1][30] , \entry[1][29] , \entry[1][28] , \entry[1][27] ,
         \entry[1][26] , \entry[1][25] , \entry[1][24] , \entry[1][23] ,
         \entry[1][22] , \entry[1][21] , \entry[1][20] , \entry[1][19] ,
         \entry[1][18] , \entry[1][17] , \entry[1][16] , \entry[1][15] ,
         \entry[1][14] , \entry[1][13] , \entry[1][12] , \entry[1][11] ,
         \entry[1][10] , \entry[1][9] , \entry[1][8] , \entry[1][7] ,
         \entry[1][6] , \entry[1][5] , \entry[1][4] , \entry[1][3] ,
         \entry[1][2] , \entry[1][1] , \entry[1][0] , \entry[0][40] ,
         \entry[0][39] , \entry[0][38] , \entry[0][37] , \entry[0][36] ,
         \entry[0][35] , \entry[0][34] , \entry[0][33] , \entry[0][32] ,
         \entry[0][31] , \entry[0][30] , \entry[0][29] , \entry[0][28] ,
         \entry[0][27] , \entry[0][26] , \entry[0][25] , \entry[0][24] ,
         \entry[0][23] , \entry[0][22] , \entry[0][21] , \entry[0][20] ,
         \entry[0][19] , \entry[0][18] , \entry[0][17] , \entry[0][16] ,
         \entry[0][15] , \entry[0][14] , \entry[0][13] , \entry[0][12] ,
         \entry[0][11] , \entry[0][10] , \entry[0][9] , \entry[0][8] ,
         \entry[0][7] , \entry[0][6] , \entry[0][5] , \entry[0][4] ,
         \entry[0][3] , \entry[0][2] , \entry[0][1] , \entry[0][0] , N9, N10,
         N11, N12, N13, N14, N15, N16, N17, N18, N19, N20, N21, N22, N23, N24,
         N25, N26, N27, N28, N29, N30, N31, N32, N33, N34, N35, N36, N37, N38,
         N39, N40, N41, N42, N43, N44, N45, N46, N47, N48, N49, N50, N51, N52,
         N53, N54, N55, N56, N57, N58, N59, N60, N61, N62, N63, N64, N65, N66,
         N67, N68, N69, N70, N71, N72, N73, N74, N75, N76, N77, N78, N79, N80,
         N81, N82, N83, N84, N85, N86, N87, N88, N89, N90, N91, N92, N93, N94,
         N95, N96, N97, N98, N99, N100, N101, N102, N103, N104, N105, N106,
         N107, N108, N109, N110, N111, N112, N113, N114, N115, N116, N117,
         N118, N119, N120, N121, N122, N123, N124, N125, N126, N127, N128,
         N129, N130, N131, N132, N133, N134, N135, N136, N137, N138, N139,
         N140, N141, N142, fifo_depth_decrease, N143, fifo_depth_increase,
         N144, N145, N146, N147, N148, N149, N150, N151, N152, N153, N154,
         N155, N156, N157;
  wire   [0:0] ptr_wr;
  wire   [0:0] ptr_rd_plus_1;
  wire   [0:0] ptr_rd;
  wire   [0:0] fifo_depth;

  \**SEQGEN**  \ptr_wr_reg[0]  ( .clear(rst), .preset(1'b0), .next_state(N10), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(ptr_wr[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(wr_i) );
  \**SEQGEN**  \entry_reg[1][40]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[40]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][40] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][39]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[39]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][39] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][38]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[38]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][38] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][37]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[37]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][37] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][36]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[36]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][36] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][35]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[35]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][35] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][34]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[34]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][34] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][33]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[33]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][33] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][32]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[32]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][32] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][31]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[31]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][31] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][30]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[30]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][30] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][29]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[29]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][29] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][28]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[28]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][28] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][27]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[27]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][27] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][26]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[26]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][26] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][25]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[25]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][25] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][24]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[24]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][24] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][23]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[23]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][23] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][22]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[22]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][22] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][21]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[21]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][21] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][20]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[20]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][20] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][19]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[19]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][19] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][18]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[18]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][18] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][17]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[17]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][17] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][16]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[16]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][16] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][15]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[15]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][15] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][14]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[14]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][14] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][13]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[13]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][13] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][12]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[12]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][12] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][11]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[11]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][11] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][10]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[10]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][10] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][9]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[9]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][9] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][8]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[8]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][8] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][7]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[7]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][7] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][6]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[6]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][6] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][5]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[5]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][5] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][4]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[4]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][4] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][3]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[3]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][3] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][2]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[2]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][2] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][1]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[1]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][1] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[1][0]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[0]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[1][0] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N12) );
  \**SEQGEN**  \entry_reg[0][40]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[40]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][40] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][39]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[39]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][39] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][38]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[38]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][38] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][37]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[37]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][37] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][36]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[36]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][36] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][35]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[35]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][35] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][34]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[34]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][34] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][33]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[33]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][33] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][32]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[32]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][32] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][31]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[31]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][31] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][30]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[30]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][30] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][29]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[29]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][29] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][28]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[28]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][28] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][27]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[27]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][27] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][26]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[26]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][26] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][25]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[25]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][25] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][24]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[24]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][24] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][23]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[23]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][23] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][22]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[22]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][22] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][21]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[21]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][21] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][20]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[20]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][20] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][19]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[19]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][19] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][18]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[18]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][18] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][17]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[17]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][17] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][16]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[16]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][16] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][15]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[15]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][15] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][14]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[14]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][14] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][13]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[13]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][13] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][12]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[12]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][12] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][11]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[11]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][11] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][10]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[10]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][10] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][9]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[9]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][9] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][8]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[8]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][8] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][7]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[7]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][7] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][6]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[6]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][6] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][5]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[5]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][5] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][4]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[4]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][4] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][3]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[3]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][3] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][2]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[2]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][2] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][1]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[1]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][1] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  \**SEQGEN**  \entry_reg[0][0]  ( .clear(rst), .preset(1'b0), .next_state(
        data_i[0]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        \entry[0][0] ), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N11) );
  SELECT_OP C536 ( .DATA1(\entry[0][40] ), .DATA2(\entry[1][40] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N16) );
  GTECH_BUF B_0 ( .A(ptr_rd_plus_1[0]), .Z(N0) );
  SELECT_OP C537 ( .DATA1(\entry[0][39] ), .DATA2(\entry[1][39] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N17) );
  SELECT_OP C538 ( .DATA1(\entry[0][38] ), .DATA2(\entry[1][38] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N18) );
  SELECT_OP C539 ( .DATA1(\entry[0][37] ), .DATA2(\entry[1][37] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N19) );
  SELECT_OP C540 ( .DATA1(\entry[0][36] ), .DATA2(\entry[1][36] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N20) );
  SELECT_OP C541 ( .DATA1(\entry[0][35] ), .DATA2(\entry[1][35] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N21) );
  SELECT_OP C542 ( .DATA1(\entry[0][34] ), .DATA2(\entry[1][34] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N22) );
  SELECT_OP C543 ( .DATA1(\entry[0][33] ), .DATA2(\entry[1][33] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N23) );
  SELECT_OP C544 ( .DATA1(\entry[0][32] ), .DATA2(\entry[1][32] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N24) );
  SELECT_OP C545 ( .DATA1(\entry[0][31] ), .DATA2(\entry[1][31] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N25) );
  SELECT_OP C546 ( .DATA1(\entry[0][30] ), .DATA2(\entry[1][30] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N26) );
  SELECT_OP C547 ( .DATA1(\entry[0][29] ), .DATA2(\entry[1][29] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N27) );
  SELECT_OP C548 ( .DATA1(\entry[0][28] ), .DATA2(\entry[1][28] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N28) );
  SELECT_OP C549 ( .DATA1(\entry[0][27] ), .DATA2(\entry[1][27] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N29) );
  SELECT_OP C550 ( .DATA1(\entry[0][26] ), .DATA2(\entry[1][26] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N30) );
  SELECT_OP C551 ( .DATA1(\entry[0][25] ), .DATA2(\entry[1][25] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N31) );
  SELECT_OP C552 ( .DATA1(\entry[0][24] ), .DATA2(\entry[1][24] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N32) );
  SELECT_OP C553 ( .DATA1(\entry[0][23] ), .DATA2(\entry[1][23] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N33) );
  SELECT_OP C554 ( .DATA1(\entry[0][22] ), .DATA2(\entry[1][22] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N34) );
  SELECT_OP C555 ( .DATA1(\entry[0][21] ), .DATA2(\entry[1][21] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N35) );
  SELECT_OP C556 ( .DATA1(\entry[0][20] ), .DATA2(\entry[1][20] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N36) );
  SELECT_OP C557 ( .DATA1(\entry[0][19] ), .DATA2(\entry[1][19] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N37) );
  SELECT_OP C558 ( .DATA1(\entry[0][18] ), .DATA2(\entry[1][18] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N38) );
  SELECT_OP C559 ( .DATA1(\entry[0][17] ), .DATA2(\entry[1][17] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N39) );
  SELECT_OP C560 ( .DATA1(\entry[0][16] ), .DATA2(\entry[1][16] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N40) );
  SELECT_OP C561 ( .DATA1(\entry[0][15] ), .DATA2(\entry[1][15] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N41) );
  SELECT_OP C562 ( .DATA1(\entry[0][14] ), .DATA2(\entry[1][14] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N42) );
  SELECT_OP C563 ( .DATA1(\entry[0][13] ), .DATA2(\entry[1][13] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N43) );
  SELECT_OP C564 ( .DATA1(\entry[0][12] ), .DATA2(\entry[1][12] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N44) );
  SELECT_OP C565 ( .DATA1(\entry[0][11] ), .DATA2(\entry[1][11] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N45) );
  SELECT_OP C566 ( .DATA1(\entry[0][10] ), .DATA2(\entry[1][10] ), .CONTROL1(
        N15), .CONTROL2(N0), .Z(N46) );
  SELECT_OP C567 ( .DATA1(\entry[0][9] ), .DATA2(\entry[1][9] ), .CONTROL1(N15), .CONTROL2(N0), .Z(N47) );
  SELECT_OP C568 ( .DATA1(\entry[0][8] ), .DATA2(\entry[1][8] ), .CONTROL1(N15), .CONTROL2(N0), .Z(N48) );
  SELECT_OP C569 ( .DATA1(\entry[0][7] ), .DATA2(\entry[1][7] ), .CONTROL1(N15), .CONTROL2(N0), .Z(N49) );
  SELECT_OP C570 ( .DATA1(\entry[0][6] ), .DATA2(\entry[1][6] ), .CONTROL1(N15), .CONTROL2(N0), .Z(N50) );
  SELECT_OP C571 ( .DATA1(\entry[0][5] ), .DATA2(\entry[1][5] ), .CONTROL1(N15), .CONTROL2(N0), .Z(N51) );
  SELECT_OP C572 ( .DATA1(\entry[0][4] ), .DATA2(\entry[1][4] ), .CONTROL1(N15), .CONTROL2(N0), .Z(N52) );
  SELECT_OP C573 ( .DATA1(\entry[0][3] ), .DATA2(\entry[1][3] ), .CONTROL1(N15), .CONTROL2(N0), .Z(N53) );
  SELECT_OP C574 ( .DATA1(\entry[0][2] ), .DATA2(\entry[1][2] ), .CONTROL1(N15), .CONTROL2(N0), .Z(N54) );
  SELECT_OP C575 ( .DATA1(\entry[0][1] ), .DATA2(\entry[1][1] ), .CONTROL1(N15), .CONTROL2(N0), .Z(N55) );
  SELECT_OP C576 ( .DATA1(\entry[0][0] ), .DATA2(\entry[1][0] ), .CONTROL1(N15), .CONTROL2(N0), .Z(N56) );
  SELECT_OP C579 ( .DATA1(\entry[0][40] ), .DATA2(\entry[1][40] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N58) );
  GTECH_BUF B_1 ( .A(ptr_rd[0]), .Z(N1) );
  SELECT_OP C580 ( .DATA1(\entry[0][39] ), .DATA2(\entry[1][39] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N59) );
  SELECT_OP C581 ( .DATA1(\entry[0][38] ), .DATA2(\entry[1][38] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N60) );
  SELECT_OP C582 ( .DATA1(\entry[0][37] ), .DATA2(\entry[1][37] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N61) );
  SELECT_OP C583 ( .DATA1(\entry[0][36] ), .DATA2(\entry[1][36] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N62) );
  SELECT_OP C584 ( .DATA1(\entry[0][35] ), .DATA2(\entry[1][35] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N63) );
  SELECT_OP C585 ( .DATA1(\entry[0][34] ), .DATA2(\entry[1][34] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N64) );
  SELECT_OP C586 ( .DATA1(\entry[0][33] ), .DATA2(\entry[1][33] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N65) );
  SELECT_OP C587 ( .DATA1(\entry[0][32] ), .DATA2(\entry[1][32] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N66) );
  SELECT_OP C588 ( .DATA1(\entry[0][31] ), .DATA2(\entry[1][31] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N67) );
  SELECT_OP C589 ( .DATA1(\entry[0][30] ), .DATA2(\entry[1][30] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N68) );
  SELECT_OP C590 ( .DATA1(\entry[0][29] ), .DATA2(\entry[1][29] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N69) );
  SELECT_OP C591 ( .DATA1(\entry[0][28] ), .DATA2(\entry[1][28] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N70) );
  SELECT_OP C592 ( .DATA1(\entry[0][27] ), .DATA2(\entry[1][27] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N71) );
  SELECT_OP C593 ( .DATA1(\entry[0][26] ), .DATA2(\entry[1][26] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N72) );
  SELECT_OP C594 ( .DATA1(\entry[0][25] ), .DATA2(\entry[1][25] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N73) );
  SELECT_OP C595 ( .DATA1(\entry[0][24] ), .DATA2(\entry[1][24] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N74) );
  SELECT_OP C596 ( .DATA1(\entry[0][23] ), .DATA2(\entry[1][23] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N75) );
  SELECT_OP C597 ( .DATA1(\entry[0][22] ), .DATA2(\entry[1][22] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N76) );
  SELECT_OP C598 ( .DATA1(\entry[0][21] ), .DATA2(\entry[1][21] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N77) );
  SELECT_OP C599 ( .DATA1(\entry[0][20] ), .DATA2(\entry[1][20] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N78) );
  SELECT_OP C600 ( .DATA1(\entry[0][19] ), .DATA2(\entry[1][19] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N79) );
  SELECT_OP C601 ( .DATA1(\entry[0][18] ), .DATA2(\entry[1][18] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N80) );
  SELECT_OP C602 ( .DATA1(\entry[0][17] ), .DATA2(\entry[1][17] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N81) );
  SELECT_OP C603 ( .DATA1(\entry[0][16] ), .DATA2(\entry[1][16] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N82) );
  SELECT_OP C604 ( .DATA1(\entry[0][15] ), .DATA2(\entry[1][15] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N83) );
  SELECT_OP C605 ( .DATA1(\entry[0][14] ), .DATA2(\entry[1][14] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N84) );
  SELECT_OP C606 ( .DATA1(\entry[0][13] ), .DATA2(\entry[1][13] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N85) );
  SELECT_OP C607 ( .DATA1(\entry[0][12] ), .DATA2(\entry[1][12] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N86) );
  SELECT_OP C608 ( .DATA1(\entry[0][11] ), .DATA2(\entry[1][11] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N87) );
  SELECT_OP C609 ( .DATA1(\entry[0][10] ), .DATA2(\entry[1][10] ), .CONTROL1(
        N57), .CONTROL2(N1), .Z(N88) );
  SELECT_OP C610 ( .DATA1(\entry[0][9] ), .DATA2(\entry[1][9] ), .CONTROL1(N57), .CONTROL2(N1), .Z(N89) );
  SELECT_OP C611 ( .DATA1(\entry[0][8] ), .DATA2(\entry[1][8] ), .CONTROL1(N57), .CONTROL2(N1), .Z(N90) );
  SELECT_OP C612 ( .DATA1(\entry[0][7] ), .DATA2(\entry[1][7] ), .CONTROL1(N57), .CONTROL2(N1), .Z(N91) );
  SELECT_OP C613 ( .DATA1(\entry[0][6] ), .DATA2(\entry[1][6] ), .CONTROL1(N57), .CONTROL2(N1), .Z(N92) );
  SELECT_OP C614 ( .DATA1(\entry[0][5] ), .DATA2(\entry[1][5] ), .CONTROL1(N57), .CONTROL2(N1), .Z(N93) );
  SELECT_OP C615 ( .DATA1(\entry[0][4] ), .DATA2(\entry[1][4] ), .CONTROL1(N57), .CONTROL2(N1), .Z(N94) );
  SELECT_OP C616 ( .DATA1(\entry[0][3] ), .DATA2(\entry[1][3] ), .CONTROL1(N57), .CONTROL2(N1), .Z(N95) );
  SELECT_OP C617 ( .DATA1(\entry[0][2] ), .DATA2(\entry[1][2] ), .CONTROL1(N57), .CONTROL2(N1), .Z(N96) );
  SELECT_OP C618 ( .DATA1(\entry[0][1] ), .DATA2(\entry[1][1] ), .CONTROL1(N57), .CONTROL2(N1), .Z(N97) );
  SELECT_OP C619 ( .DATA1(\entry[0][0] ), .DATA2(\entry[1][0] ), .CONTROL1(N57), .CONTROL2(N1), .Z(N98) );
  \**SEQGEN**  \data_o_reg[40]  ( .clear(rst), .preset(1'b0), .next_state(N139), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[40]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[39]  ( .clear(rst), .preset(1'b0), .next_state(N138), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[39]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[38]  ( .clear(rst), .preset(1'b0), .next_state(N137), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[38]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[37]  ( .clear(rst), .preset(1'b0), .next_state(N136), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[37]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[36]  ( .clear(rst), .preset(1'b0), .next_state(N135), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[36]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[35]  ( .clear(rst), .preset(1'b0), .next_state(N134), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[35]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[34]  ( .clear(rst), .preset(1'b0), .next_state(N133), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[34]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[33]  ( .clear(rst), .preset(1'b0), .next_state(N132), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[33]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[32]  ( .clear(rst), .preset(1'b0), .next_state(N131), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[32]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[31]  ( .clear(rst), .preset(1'b0), .next_state(N130), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[31]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[30]  ( .clear(rst), .preset(1'b0), .next_state(N129), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[30]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[29]  ( .clear(rst), .preset(1'b0), .next_state(N128), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[29]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[28]  ( .clear(rst), .preset(1'b0), .next_state(N127), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[28]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[27]  ( .clear(rst), .preset(1'b0), .next_state(N126), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[27]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[26]  ( .clear(rst), .preset(1'b0), .next_state(N125), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[26]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[25]  ( .clear(rst), .preset(1'b0), .next_state(N124), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[25]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[24]  ( .clear(rst), .preset(1'b0), .next_state(N123), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[24]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[23]  ( .clear(rst), .preset(1'b0), .next_state(N122), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[23]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[22]  ( .clear(rst), .preset(1'b0), .next_state(N121), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[22]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[21]  ( .clear(rst), .preset(1'b0), .next_state(N120), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[21]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[20]  ( .clear(rst), .preset(1'b0), .next_state(N119), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[20]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[19]  ( .clear(rst), .preset(1'b0), .next_state(N118), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[19]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[18]  ( .clear(rst), .preset(1'b0), .next_state(N117), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[18]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[17]  ( .clear(rst), .preset(1'b0), .next_state(N116), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[17]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[16]  ( .clear(rst), .preset(1'b0), .next_state(N115), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[16]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[15]  ( .clear(rst), .preset(1'b0), .next_state(N114), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[15]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[14]  ( .clear(rst), .preset(1'b0), .next_state(N113), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[14]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[13]  ( .clear(rst), .preset(1'b0), .next_state(N112), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[13]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[12]  ( .clear(rst), .preset(1'b0), .next_state(N111), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[12]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[11]  ( .clear(rst), .preset(1'b0), .next_state(N110), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[11]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[10]  ( .clear(rst), .preset(1'b0), .next_state(N109), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[10]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[9]  ( .clear(rst), .preset(1'b0), .next_state(N108), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[8]  ( .clear(rst), .preset(1'b0), .next_state(N107), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[7]  ( .clear(rst), .preset(1'b0), .next_state(N106), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[6]  ( .clear(rst), .preset(1'b0), .next_state(N105), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[5]  ( .clear(rst), .preset(1'b0), .next_state(N104), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[4]  ( .clear(rst), .preset(1'b0), .next_state(N103), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[3]  ( .clear(rst), .preset(1'b0), .next_state(N102), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[2]  ( .clear(rst), .preset(1'b0), .next_state(N101), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[1]  ( .clear(rst), .preset(1'b0), .next_state(N100), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \data_o_reg[0]  ( .clear(rst), .preset(1'b0), .next_state(N99), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(data_o[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \ptr_rd_reg[0]  ( .clear(rst), .preset(1'b0), .next_state(N142), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(ptr_rd[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(rd_i) );
  \**SEQGEN**  status_empty_reg ( .clear(1'b0), .preset(rst), .next_state(N143), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(status_empty_o), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N147) );
  \**SEQGEN**  status_full_reg ( .clear(rst), .preset(1'b0), .next_state(N148), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(status_full_o), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N152) );
  GTECH_NOT I_0 ( .A(fifo_depth[0]), .Z(N153) );
  GTECH_XOR2 C816 ( .A(ptr_wr[0]), .B(ptr_rd[0]), .Z(fifo_depth[0]) );
  GTECH_XOR2 C817 ( .A(ptr_rd[0]), .B(1'b1), .Z(ptr_rd_plus_1[0]) );
  GTECH_XOR2 C818 ( .A(ptr_rd[0]), .B(1'b1), .Z(N142) );
  GTECH_XOR2 C819 ( .A(ptr_wr[0]), .B(1'b1), .Z(N10) );
  GTECH_NOT I_1 ( .A(ptr_wr[0]), .Z(N154) );
  SELECT_OP C821 ( .DATA1({ptr_wr[0], N154}), .DATA2({1'b0, 1'b0}), .CONTROL1(
        N2), .CONTROL2(N3), .Z({N12, N11}) );
  GTECH_BUF B_2 ( .A(wr_i), .Z(N2) );
  GTECH_BUF B_3 ( .A(N9), .Z(N3) );
  SELECT_OP C822 ( .DATA1(data_i), .DATA2({N16, N17, N18, N19, N20, N21, N22, 
        N23, N24, N25, N26, N27, N28, N29, N30, N31, N32, N33, N34, N35, N36, 
        N37, N38, N39, N40, N41, N42, N43, N44, N45, N46, N47, N48, N49, N50, 
        N51, N52, N53, N54, N55, N56}), .DATA3({N58, N59, N60, N61, N62, N63, 
        N64, N65, N66, N67, N68, N69, N70, N71, N72, N73, N74, N75, N76, N77, 
        N78, N79, N80, N81, N82, N83, N84, N85, N86, N87, N88, N89, N90, N91, 
        N92, N93, N94, N95, N96, N97, N98}), .CONTROL1(N4), .CONTROL2(N141), 
        .CONTROL3(N14), .Z({N139, N138, N137, N136, N135, N134, N133, N132, 
        N131, N130, N129, N128, N127, N126, N125, N124, N123, N122, N121, N120, 
        N119, N118, N117, N116, N115, N114, N113, N112, N111, N110, N109, N108, 
        N107, N106, N105, N104, N103, N102, N101, N100, N99}) );
  GTECH_BUF B_4 ( .A(status_empty_o), .Z(N4) );
  SELECT_OP C823 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N5), 
        .CONTROL2(N6), .CONTROL3(N146), .Z(N147) );
  GTECH_BUF B_5 ( .A(N143), .Z(N5) );
  GTECH_BUF B_6 ( .A(N144), .Z(N6) );
  SELECT_OP C824 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N7), 
        .CONTROL2(N8), .CONTROL3(N151), .Z(N152) );
  GTECH_BUF B_7 ( .A(N148), .Z(N7) );
  GTECH_BUF B_8 ( .A(N149), .Z(N8) );
  GTECH_NOT I_2 ( .A(wr_i), .Z(N9) );
  GTECH_OR2 C833 ( .A(rd_i), .B(status_empty_o), .Z(N13) );
  GTECH_NOT I_3 ( .A(N13), .Z(N14) );
  GTECH_NOT I_4 ( .A(ptr_rd_plus_1[0]), .Z(N15) );
  GTECH_NOT I_5 ( .A(ptr_rd[0]), .Z(N57) );
  GTECH_NOT I_6 ( .A(status_empty_o), .Z(N140) );
  GTECH_AND2 C838 ( .A(rd_i), .B(N140), .Z(N141) );
  GTECH_AND2 C840 ( .A(fifo_depth[0]), .B(fifo_depth_decrease), .Z(N143) );
  GTECH_AND2 C841 ( .A(N155), .B(fifo_depth_increase), .Z(N144) );
  GTECH_AND2 C842 ( .A(N153), .B(status_empty_o), .Z(N155) );
  GTECH_OR2 C845 ( .A(N144), .B(N143), .Z(N145) );
  GTECH_NOT I_7 ( .A(N145), .Z(N146) );
  GTECH_AND2 C847 ( .A(fifo_depth[0]), .B(fifo_depth_increase), .Z(N148) );
  GTECH_AND2 C848 ( .A(N156), .B(fifo_depth_decrease), .Z(N149) );
  GTECH_AND2 C849 ( .A(N153), .B(status_full_o), .Z(N156) );
  GTECH_OR2 C852 ( .A(N149), .B(N148), .Z(N150) );
  GTECH_NOT I_8 ( .A(N150), .Z(N151) );
  GTECH_AND2 C854 ( .A(wr_i), .B(N157), .Z(fifo_depth_increase) );
  GTECH_NOT I_9 ( .A(rd_i), .Z(N157) );
  GTECH_AND2 C856 ( .A(N9), .B(rd_i), .Z(fifo_depth_decrease) );
endmodule



    module mesi_isc_breq_fifos_MBUS_CMD_WIDTH3_ADDR_WIDTH32_BROAD_TYPE_WIDTH2_BROAD_ID_WIDTH5_BREQ_FIFO_SIZE2_BREQ_FIFO_SIZE_LOG21 ( 
        clk, rst, mbus_cmd_array_i, mbus_addr_array_i, 
        broad_fifo_status_full_i, mbus_ack_array_o, broad_fifo_wr_o, 
        broad_addr_o, broad_type_o, broad_cpu_id_o, broad_id_o );
  input [11:0] mbus_cmd_array_i;
  input [127:0] mbus_addr_array_i;
  output [3:0] mbus_ack_array_o;
  output [31:0] broad_addr_o;
  output [1:0] broad_type_o;
  output [1:0] broad_cpu_id_o;
  output [4:0] broad_id_o;
  input clk, rst, broad_fifo_status_full_i;
  output broad_fifo_wr_o;

  wire   [3:0] fifo_status_empty_array;
  wire   [3:0] fifo_status_full_array;
  wire   [127:0] broad_addr_array;
  wire   [7:0] broad_type_array;
  wire   [19:0] broad_id_array;
  wire   [3:0] fifo_wr_array;
  wire   [3:0] fifo_rd_array;
  wire   [7:0] breq_type_array;
  wire   [7:0] breq_cpu_id_array;
  wire   [19:0] breq_id_array;
  wire   [7:0] broad_cpu_id_array;

  mesi_isc_breq_fifos_cntl_MBUS_CMD_WIDTH3_ADDR_WIDTH32_BROAD_TYPE_WIDTH2_BROAD_ID_WIDTH5 mesi_isc_breq_fifos_cntl ( 
        .clk(clk), .rst(rst), .mbus_cmd_array_i(mbus_cmd_array_i), 
        .fifo_status_empty_array_i(fifo_status_empty_array), 
        .fifo_status_full_array_i(fifo_status_full_array), 
        .broad_fifo_status_full_i(broad_fifo_status_full_i), 
        .broad_addr_array_i(broad_addr_array), .broad_type_array_i(
        broad_type_array), .broad_id_array_i(broad_id_array), 
        .mbus_ack_array_o(mbus_ack_array_o), .fifo_wr_array_o(fifo_wr_array), 
        .fifo_rd_array_o(fifo_rd_array), .broad_fifo_wr_o(broad_fifo_wr_o), 
        .broad_addr_o(broad_addr_o), .broad_type_o(broad_type_o), 
        .broad_cpu_id_o(broad_cpu_id_o), .broad_id_o(broad_id_o), 
        .breq_type_array_o(breq_type_array), .breq_cpu_id_array_o(
        breq_cpu_id_array), .breq_id_array_o(breq_id_array) );
  mesi_isc_basic_fifo_DATA_WIDTH41_FIFO_SIZE2_FIFO_SIZE_LOG21 fifo_3 ( .clk(
        clk), .rst(rst), .wr_i(fifo_wr_array[3]), .rd_i(fifo_rd_array[3]), 
        .data_i({mbus_addr_array_i[127:96], breq_type_array[7:6], 
        breq_cpu_id_array[7:6], breq_id_array[19:15]}), .data_o({
        broad_addr_array[127:96], broad_type_array[7:6], 
        broad_cpu_id_array[7:6], broad_id_array[19:15]}), .status_empty_o(
        fifo_status_empty_array[3]), .status_full_o(fifo_status_full_array[3])
         );
  mesi_isc_basic_fifo_DATA_WIDTH41_FIFO_SIZE2_FIFO_SIZE_LOG21 fifo_2 ( .clk(
        clk), .rst(rst), .wr_i(fifo_wr_array[2]), .rd_i(fifo_rd_array[2]), 
        .data_i({mbus_addr_array_i[95:64], breq_type_array[5:4], 
        breq_cpu_id_array[5:4], breq_id_array[14:10]}), .data_o({
        broad_addr_array[95:64], broad_type_array[5:4], 
        broad_cpu_id_array[5:4], broad_id_array[14:10]}), .status_empty_o(
        fifo_status_empty_array[2]), .status_full_o(fifo_status_full_array[2])
         );
  mesi_isc_basic_fifo_DATA_WIDTH41_FIFO_SIZE2_FIFO_SIZE_LOG21 fifo_1 ( .clk(
        clk), .rst(rst), .wr_i(fifo_wr_array[1]), .rd_i(fifo_rd_array[1]), 
        .data_i({mbus_addr_array_i[63:32], breq_type_array[3:2], 
        breq_cpu_id_array[3:2], breq_id_array[9:5]}), .data_o({
        broad_addr_array[63:32], broad_type_array[3:2], 
        broad_cpu_id_array[3:2], broad_id_array[9:5]}), .status_empty_o(
        fifo_status_empty_array[1]), .status_full_o(fifo_status_full_array[1])
         );
  mesi_isc_basic_fifo_DATA_WIDTH41_FIFO_SIZE2_FIFO_SIZE_LOG21 fifo_0 ( .clk(
        clk), .rst(rst), .wr_i(fifo_wr_array[0]), .rd_i(fifo_rd_array[0]), 
        .data_i({mbus_addr_array_i[31:0], breq_type_array[1:0], 
        breq_cpu_id_array[1:0], breq_id_array[4:0]}), .data_o({
        broad_addr_array[31:0], broad_type_array[1:0], broad_cpu_id_array[1:0], 
        broad_id_array[4:0]}), .status_empty_o(fifo_status_empty_array[0]), 
        .status_full_o(fifo_status_full_array[0]) );
endmodule


module mesi_isc ( clk, rst, mbus_cmd3_i, mbus_cmd2_i, mbus_cmd1_i, mbus_cmd0_i, 
        mbus_addr3_i, mbus_addr2_i, mbus_addr1_i, mbus_addr0_i, cbus_ack3_i, 
        cbus_ack2_i, cbus_ack1_i, cbus_ack0_i, cbus_addr_o, cbus_cmd3_o, 
        cbus_cmd2_o, cbus_cmd1_o, cbus_cmd0_o, mbus_ack3_o, mbus_ack2_o, 
        mbus_ack1_o, mbus_ack0_o );
  input [2:0] mbus_cmd3_i;
  input [2:0] mbus_cmd2_i;
  input [2:0] mbus_cmd1_i;
  input [2:0] mbus_cmd0_i;
  input [31:0] mbus_addr3_i;
  input [31:0] mbus_addr2_i;
  input [31:0] mbus_addr1_i;
  input [31:0] mbus_addr0_i;
  output [31:0] cbus_addr_o;
  output [2:0] cbus_cmd3_o;
  output [2:0] cbus_cmd2_o;
  output [2:0] cbus_cmd1_o;
  output [2:0] cbus_cmd0_o;
  input clk, rst, cbus_ack3_i, cbus_ack2_i, cbus_ack1_i, cbus_ack0_i;
  output mbus_ack3_o, mbus_ack2_o, mbus_ack1_o, mbus_ack0_o;
  wire   broad_fifo_wr, broad_fifo_status_full;
  wire   [31:0] broad_addr;
  wire   [1:0] broad_type;
  wire   [1:0] broad_cpu_id;
  wire   [4:0] broad_id;

  mesi_isc_broad_CBUS_CMD_WIDTH3_ADDR_WIDTH32_BROAD_TYPE_WIDTH2_BROAD_ID_WIDTH5_BROAD_REQ_FIFO_SIZE4_BROAD_REQ_FIFO_SIZE_LOG22 mesi_isc_broad ( 
        .clk(clk), .rst(rst), .cbus_ack_array_i({cbus_ack3_i, cbus_ack2_i, 
        cbus_ack1_i, cbus_ack0_i}), .broad_fifo_wr_i(broad_fifo_wr), 
        .broad_addr_i(broad_addr), .broad_type_i(broad_type), .broad_cpu_id_i(
        broad_cpu_id), .broad_id_i(broad_id), .cbus_addr_o(cbus_addr_o), 
        .cbus_cmd_array_o({cbus_cmd3_o, cbus_cmd2_o, cbus_cmd1_o, cbus_cmd0_o}), .fifo_status_full_o(broad_fifo_status_full) );
  mesi_isc_breq_fifos_MBUS_CMD_WIDTH3_ADDR_WIDTH32_BROAD_TYPE_WIDTH2_BROAD_ID_WIDTH5_BREQ_FIFO_SIZE2_BREQ_FIFO_SIZE_LOG21 mesi_isc_breq_fifos ( 
        .clk(clk), .rst(rst), .mbus_cmd_array_i({mbus_cmd3_i, mbus_cmd2_i, 
        mbus_cmd1_i, mbus_cmd0_i}), .mbus_addr_array_i({mbus_addr3_i, 
        mbus_addr2_i, mbus_addr1_i, mbus_addr0_i}), .broad_fifo_status_full_i(
        broad_fifo_status_full), .mbus_ack_array_o({mbus_ack3_o, mbus_ack2_o, 
        mbus_ack1_o, mbus_ack0_o}), .broad_fifo_wr_o(broad_fifo_wr), 
        .broad_addr_o(broad_addr), .broad_type_o(broad_type), .broad_cpu_id_o(
        broad_cpu_id), .broad_id_o(broad_id) );
endmodule


set WORKDIR /home/debjit/Work/Post_Silicon/rtl
source ${WORKDIR}/.synopsys_dc.setup
analyze -library WORK -format verilog {/home/debjit/Work/Post_Silicon/rtl/mesi_isc_define.v /home/debjit/Work/Post_Silicon/rtl/mesi_isc_broad_cntl.v /home/debjit/Work/Post_Silicon/rtl/mesi_isc_broad.v /home/debjit/Work/Post_Silicon/rtl/mesi_isc_breq_fifos_cntl.v /home/debjit/Work/Post_Silicon/rtl/mesi_isc_breq_fifos.v /home/debjit/Work/Post_Silicon/rtl/mesi_isc_basic_fifo.v /home/debjit/Work/Post_Silicon/rtl/mesi_isc.v}
elaborate mesi_isc -architecture verilog -library DEFAULT
create_clock clk -period 10
ungroup -all -flatten
write_file -format verilog -output mesi_netlist.v
quit


module SLC ( Clk, Reset, Run, Continue, ContinueIR, S, CE, UB, LB, OE, WE, A, 
        LED, DIS3, DIS2, DIS1, DIS0, MEMdata );
  input [15:0] S;
  output [17:0] A;
  output [11:0] LED;
  output [6:0] DIS3;
  output [6:0] DIS2;
  output [6:0] DIS1;
  output [6:0] DIS0;
  inout [15:0] MEMdata;
  input Clk, Reset, Run, Continue, ContinueIR;
  output CE, UB, LB, OE, WE;
  wire   n122, n123, n124, n125, n126, n127, n128, n129, n130, n131, n132,
         n133, n134, n135, n136, n137, n138, n139, n140, n141, n142, n143,
         n144, n145, n146, n147, n148, n149, n150, n151, n152, n153, n154,
         n171, n172, n173, n174, n175, n176, n177, n178, n179, n180, n181,
         n182, n183, n184, n185, n186, n187, n188, n189, n190, n191, n192,
         n193, n194, n195, n196, n197, n198, n199, n200, n201, n202, n203,
         n204, n205, n206, n207, n208, n209, n210, n211, n212, n213, n214,
         n215, n216, n217, n218, n219, n220, n221, n222, n223, n224, n225,
         n226, n227, n228, n229, n230, n231, n232, n233, n234, n235, n236,
         n237, n238, n239, n240, n241, n242, n243, n244, n245, n246, n247,
         n248, n249, n250, n251, n252, n253, n254, n255, n256, n257, n258,
         n259, n260, n261, n262, n263, n264, n265, n266, n267, n268, n269,
         n270, n271, n272, n273, n274, n275, n276, n277;
  tri   Clk;
  tri   CE;
  tri   UB;
  tri   LB;
  tri   OE;
  tri   WE;
  tri   [17:0] A;
  tri   [11:0] LED;
  tri   [15:0] MEMdata;
  tri   _0_net_;
  tri   _1_net_;
  tri   _2_net_;
  tri   _3_net_;
  tri   \mem2io/N88 ;
  tri   \mem2io/N87 ;
  tri   \mem2io/N86 ;
  tri   \mem2io/N85 ;
  tri   \mem2io/N84 ;
  tri   \mem2io/N83 ;
  tri   \mem2io/N82 ;
  tri   \mem2io/N81 ;
  tri   \mem2io/N80 ;
  tri   \mem2io/N79 ;
  tri   \mem2io/N78 ;
  tri   \mem2io/N77 ;
  tri   \mem2io/N76 ;
  tri   \mem2io/N75 ;
  tri   \mem2io/N74 ;
  tri   \mem2io/N73 ;

  CPU cpu ( .Reset(_0_net_), .Run(_1_net_), .Continue(_2_net_), .ContinueIR(
        _3_net_), .Data({\mem2io/N88 , \mem2io/N87 , \mem2io/N86 , 
        \mem2io/N85 , \mem2io/N84 , \mem2io/N83 , \mem2io/N82 , \mem2io/N81 , 
        \mem2io/N80 , \mem2io/N79 , \mem2io/N78 , \mem2io/N77 , \mem2io/N76 , 
        \mem2io/N75 , \mem2io/N74 , \mem2io/N73 }), .ADDR(A), .LED(LED), 
        .Mem_WE(WE), .Mem_OE(OE), .Mem_LB(LB), .Mem_UB(UB), .Mem_CE(CE), .Clk(
        Clk) );
  INV_X4 I_3 ( .A(Reset), .ZN(_0_net_) );
  INV_X4 I_2 ( .A(Run), .ZN(_1_net_) );
  INV_X4 I_1 ( .A(Continue), .ZN(_2_net_) );
  INV_X4 I_0 ( .A(ContinueIR), .ZN(_3_net_) );
  DFFR_X1 \mem2io/hex_data_reg[0]  ( .D(n154), .CK(Clk), .RN(Reset), .Q(n274), 
        .QN(n182) );
  DFFR_X1 \mem2io/hex_data_reg[1]  ( .D(n153), .CK(Clk), .RN(Reset), .Q(n276)
         );
  DFFR_X1 \mem2io/hex_data_reg[2]  ( .D(n152), .CK(Clk), .RN(Reset), .Q(n275), 
        .QN(n174) );
  DFFR_X1 \mem2io/hex_data_reg[3]  ( .D(n151), .CK(Clk), .RN(Reset), .Q(n277), 
        .QN(n178) );
  DFFR_X1 \mem2io/hex_data_reg[4]  ( .D(n150), .CK(Clk), .RN(Reset), .Q(n270), 
        .QN(n181) );
  DFFR_X1 \mem2io/hex_data_reg[5]  ( .D(n149), .CK(Clk), .RN(Reset), .Q(n272)
         );
  DFFR_X1 \mem2io/hex_data_reg[6]  ( .D(n148), .CK(Clk), .RN(Reset), .Q(n271), 
        .QN(n173) );
  DFFR_X1 \mem2io/hex_data_reg[7]  ( .D(n147), .CK(Clk), .RN(Reset), .Q(n273), 
        .QN(n177) );
  DFFR_X1 \mem2io/hex_data_reg[8]  ( .D(n146), .CK(Clk), .RN(Reset), .Q(n266), 
        .QN(n180) );
  DFFR_X1 \mem2io/hex_data_reg[9]  ( .D(n145), .CK(Clk), .RN(Reset), .Q(n268)
         );
  DFFR_X1 \mem2io/hex_data_reg[10]  ( .D(n144), .CK(Clk), .RN(Reset), .Q(n267), 
        .QN(n172) );
  DFFR_X1 \mem2io/hex_data_reg[11]  ( .D(n143), .CK(Clk), .RN(Reset), .Q(n269), 
        .QN(n176) );
  DFFR_X1 \mem2io/hex_data_reg[12]  ( .D(n142), .CK(Clk), .RN(Reset), .Q(n262), 
        .QN(n179) );
  DFFR_X1 \mem2io/hex_data_reg[13]  ( .D(n141), .CK(Clk), .RN(Reset), .Q(n264)
         );
  DFFR_X1 \mem2io/hex_data_reg[14]  ( .D(n140), .CK(Clk), .RN(Reset), .Q(n263), 
        .QN(n171) );
  DFFR_X1 \mem2io/hex_data_reg[15]  ( .D(n139), .CK(Clk), .RN(Reset), .Q(n265), 
        .QN(n175) );
  TBUF_X2 \mem2io/Data_Mem_tri[15]  ( .A(\mem2io/N88 ), .EN(WE), .Z(
        MEMdata[15]) );
  TBUF_X2 \mem2io/Data_Mem_tri[14]  ( .A(\mem2io/N87 ), .EN(WE), .Z(
        MEMdata[14]) );
  TBUF_X2 \mem2io/Data_Mem_tri[13]  ( .A(\mem2io/N86 ), .EN(WE), .Z(
        MEMdata[13]) );
  TBUF_X2 \mem2io/Data_Mem_tri[12]  ( .A(\mem2io/N85 ), .EN(WE), .Z(
        MEMdata[12]) );
  TBUF_X2 \mem2io/Data_Mem_tri[11]  ( .A(\mem2io/N84 ), .EN(WE), .Z(
        MEMdata[11]) );
  TBUF_X2 \mem2io/Data_Mem_tri[10]  ( .A(\mem2io/N83 ), .EN(WE), .Z(
        MEMdata[10]) );
  TBUF_X2 \mem2io/Data_Mem_tri[9]  ( .A(\mem2io/N82 ), .EN(WE), .Z(MEMdata[9])
         );
  TBUF_X2 \mem2io/Data_Mem_tri[8]  ( .A(\mem2io/N81 ), .EN(WE), .Z(MEMdata[8])
         );
  TBUF_X2 \mem2io/Data_Mem_tri[7]  ( .A(\mem2io/N80 ), .EN(WE), .Z(MEMdata[7])
         );
  TBUF_X2 \mem2io/Data_Mem_tri[6]  ( .A(\mem2io/N79 ), .EN(WE), .Z(MEMdata[6])
         );
  TBUF_X2 \mem2io/Data_Mem_tri[5]  ( .A(\mem2io/N78 ), .EN(WE), .Z(MEMdata[5])
         );
  TBUF_X2 \mem2io/Data_Mem_tri[4]  ( .A(\mem2io/N77 ), .EN(WE), .Z(MEMdata[4])
         );
  TBUF_X2 \mem2io/Data_Mem_tri[3]  ( .A(\mem2io/N76 ), .EN(WE), .Z(MEMdata[3])
         );
  TBUF_X2 \mem2io/Data_Mem_tri[2]  ( .A(\mem2io/N75 ), .EN(WE), .Z(MEMdata[2])
         );
  TBUF_X2 \mem2io/Data_Mem_tri[1]  ( .A(\mem2io/N74 ), .EN(WE), .Z(MEMdata[1])
         );
  TBUF_X2 \mem2io/Data_Mem_tri[0]  ( .A(\mem2io/N73 ), .EN(WE), .Z(MEMdata[0])
         );
  TBUF_X1 \mem2io/Data_CPU_signal_tri[15]  ( .A(n138), .EN(n132), .Z(
        \mem2io/N88 ) );
  TBUF_X1 \mem2io/Data_CPU_signal_tri[14]  ( .A(n137), .EN(n132), .Z(
        \mem2io/N87 ) );
  TBUF_X1 \mem2io/Data_CPU_signal_tri[13]  ( .A(n136), .EN(n132), .Z(
        \mem2io/N86 ) );
  TBUF_X1 \mem2io/Data_CPU_signal_tri[12]  ( .A(n135), .EN(n132), .Z(
        \mem2io/N85 ) );
  TBUF_X1 \mem2io/Data_CPU_signal_tri[11]  ( .A(n134), .EN(n132), .Z(
        \mem2io/N84 ) );
  TBUF_X1 \mem2io/Data_CPU_signal_tri[10]  ( .A(n133), .EN(n132), .Z(
        \mem2io/N83 ) );
  TBUF_X1 \mem2io/Data_CPU_signal_tri[9]  ( .A(n131), .EN(n132), .Z(
        \mem2io/N82 ) );
  TBUF_X1 \mem2io/Data_CPU_signal_tri[8]  ( .A(n130), .EN(n132), .Z(
        \mem2io/N81 ) );
  TBUF_X1 \mem2io/Data_CPU_signal_tri[7]  ( .A(n129), .EN(n132), .Z(
        \mem2io/N80 ) );
  TBUF_X1 \mem2io/Data_CPU_signal_tri[6]  ( .A(n128), .EN(n132), .Z(
        \mem2io/N79 ) );
  TBUF_X1 \mem2io/Data_CPU_signal_tri[5]  ( .A(n127), .EN(n132), .Z(
        \mem2io/N78 ) );
  TBUF_X2 \mem2io/Data_CPU_signal_tri[4]  ( .A(n126), .EN(n132), .Z(
        \mem2io/N77 ) );
  TBUF_X2 \mem2io/Data_CPU_signal_tri[3]  ( .A(n125), .EN(n132), .Z(
        \mem2io/N76 ) );
  TBUF_X2 \mem2io/Data_CPU_signal_tri[2]  ( .A(n124), .EN(n132), .Z(
        \mem2io/N75 ) );
  TBUF_X2 \mem2io/Data_CPU_signal_tri[1]  ( .A(n123), .EN(n132), .Z(
        \mem2io/N74 ) );
  TBUF_X2 \mem2io/Data_CPU_signal_tri[0]  ( .A(n122), .EN(n132), .Z(
        \mem2io/N73 ) );
  AND2_X4 U183 ( .A1(n184), .A2(n185), .ZN(n183) );
  NOR4_X2 U184 ( .A1(n186), .A2(n187), .A3(n188), .A4(n189), .ZN(n184) );
  MUX2_X1 U185 ( .A(n274), .B(\mem2io/N73 ), .S(n183), .Z(n154) );
  MUX2_X1 U186 ( .A(n276), .B(\mem2io/N74 ), .S(n183), .Z(n153) );
  MUX2_X1 U187 ( .A(n275), .B(\mem2io/N75 ), .S(n183), .Z(n152) );
  MUX2_X1 U188 ( .A(n277), .B(\mem2io/N76 ), .S(n183), .Z(n151) );
  MUX2_X1 U189 ( .A(n270), .B(\mem2io/N77 ), .S(n183), .Z(n150) );
  MUX2_X1 U190 ( .A(n272), .B(\mem2io/N78 ), .S(n183), .Z(n149) );
  MUX2_X1 U191 ( .A(n271), .B(\mem2io/N79 ), .S(n183), .Z(n148) );
  MUX2_X1 U192 ( .A(n273), .B(\mem2io/N80 ), .S(n183), .Z(n147) );
  MUX2_X1 U193 ( .A(n266), .B(\mem2io/N81 ), .S(n183), .Z(n146) );
  MUX2_X1 U194 ( .A(n268), .B(\mem2io/N82 ), .S(n183), .Z(n145) );
  MUX2_X1 U195 ( .A(n267), .B(\mem2io/N83 ), .S(n183), .Z(n144) );
  MUX2_X1 U196 ( .A(n269), .B(\mem2io/N84 ), .S(n183), .Z(n143) );
  MUX2_X1 U197 ( .A(n262), .B(\mem2io/N85 ), .S(n183), .Z(n142) );
  MUX2_X1 U198 ( .A(n264), .B(\mem2io/N86 ), .S(n183), .Z(n141) );
  MUX2_X1 U199 ( .A(n263), .B(\mem2io/N87 ), .S(n183), .Z(n140) );
  MUX2_X1 U200 ( .A(n265), .B(\mem2io/N88 ), .S(n183), .Z(n139) );
  MUX2_X1 U201 ( .A(MEMdata[15]), .B(S[15]), .S(n184), .Z(n138) );
  MUX2_X1 U202 ( .A(MEMdata[14]), .B(S[14]), .S(n184), .Z(n137) );
  MUX2_X1 U203 ( .A(MEMdata[13]), .B(S[13]), .S(n184), .Z(n136) );
  MUX2_X1 U204 ( .A(MEMdata[12]), .B(S[12]), .S(n184), .Z(n135) );
  MUX2_X1 U205 ( .A(MEMdata[11]), .B(S[11]), .S(n184), .Z(n134) );
  MUX2_X1 U206 ( .A(MEMdata[10]), .B(S[10]), .S(n184), .Z(n133) );
  OR2_X1 U207 ( .A1(OE), .A2(n185), .ZN(n132) );
  INV_X1 U208 ( .A(WE), .ZN(n185) );
  MUX2_X1 U209 ( .A(MEMdata[9]), .B(S[9]), .S(n184), .Z(n131) );
  MUX2_X1 U210 ( .A(MEMdata[8]), .B(S[8]), .S(n184), .Z(n130) );
  MUX2_X1 U211 ( .A(MEMdata[7]), .B(S[7]), .S(n184), .Z(n129) );
  MUX2_X1 U212 ( .A(MEMdata[6]), .B(S[6]), .S(n184), .Z(n128) );
  MUX2_X1 U213 ( .A(MEMdata[5]), .B(S[5]), .S(n184), .Z(n127) );
  MUX2_X1 U214 ( .A(MEMdata[4]), .B(S[4]), .S(n184), .Z(n126) );
  MUX2_X1 U215 ( .A(MEMdata[3]), .B(S[3]), .S(n184), .Z(n125) );
  MUX2_X1 U216 ( .A(MEMdata[2]), .B(S[2]), .S(n184), .Z(n124) );
  MUX2_X1 U217 ( .A(MEMdata[1]), .B(S[1]), .S(n184), .Z(n123) );
  MUX2_X1 U218 ( .A(MEMdata[0]), .B(S[0]), .S(n184), .Z(n122) );
  NAND4_X1 U219 ( .A1(A[1]), .A2(A[15]), .A3(A[14]), .A4(A[13]), .ZN(n189) );
  NAND4_X1 U220 ( .A1(A[12]), .A2(A[11]), .A3(A[10]), .A4(A[0]), .ZN(n188) );
  NAND4_X1 U221 ( .A1(A[9]), .A2(A[8]), .A3(A[7]), .A4(A[6]), .ZN(n187) );
  NAND4_X1 U222 ( .A1(A[5]), .A2(A[4]), .A3(A[3]), .A4(A[2]), .ZN(n186) );
  OAI21_X1 U223 ( .B1(n264), .B2(n190), .A(n191), .ZN(DIS3[6]) );
  MUX2_X1 U224 ( .A(n192), .B(n193), .S(n171), .Z(n191) );
  INV_X1 U225 ( .A(n194), .ZN(n193) );
  NAND4_X1 U226 ( .A1(n195), .A2(n196), .A3(n192), .A4(n197), .ZN(DIS3[5]) );
  OR2_X1 U227 ( .A1(n198), .A2(n265), .ZN(n192) );
  NAND2_X1 U228 ( .A1(n199), .A2(n175), .ZN(n195) );
  OAI221_X1 U229 ( .B1(n264), .B2(n200), .C1(n265), .C2(n179), .A(n201), .ZN(
        DIS3[4]) );
  INV_X1 U230 ( .A(n199), .ZN(n200) );
  OAI221_X1 U231 ( .B1(n171), .B2(n198), .C1(n262), .C2(n202), .A(n203), .ZN(
        DIS3[3]) );
  OAI221_X1 U232 ( .B1(n175), .B2(n204), .C1(n262), .C2(n196), .A(n190), .ZN(
        DIS3[2]) );
  NAND3_X1 U233 ( .A1(n175), .A2(n171), .A3(n264), .ZN(n196) );
  NAND2_X1 U234 ( .A1(n263), .A2(n264), .ZN(n204) );
  OAI221_X1 U235 ( .B1(n175), .B2(n198), .C1(n179), .C2(n201), .A(n205), .ZN(
        DIS3[1]) );
  AOI21_X1 U236 ( .B1(n206), .B2(n264), .A(n207), .ZN(n205) );
  INV_X1 U237 ( .A(n190), .ZN(n207) );
  NAND2_X1 U238 ( .A1(n206), .A2(n265), .ZN(n190) );
  NAND2_X1 U239 ( .A1(n194), .A2(n263), .ZN(n201) );
  NAND2_X1 U240 ( .A1(n264), .A2(n262), .ZN(n198) );
  OAI211_X1 U241 ( .C1(n179), .C2(n202), .A(n203), .B(n197), .ZN(DIS3[0]) );
  OR4_X1 U242 ( .A1(n175), .A2(n171), .A3(n179), .A4(n264), .ZN(n197) );
  OAI21_X1 U243 ( .B1(n206), .B2(n199), .A(n194), .ZN(n203) );
  NOR2_X1 U244 ( .A1(n265), .A2(n264), .ZN(n194) );
  NOR2_X1 U245 ( .A1(n179), .A2(n263), .ZN(n199) );
  NOR2_X1 U246 ( .A1(n171), .A2(n262), .ZN(n206) );
  NAND3_X1 U247 ( .A1(n264), .A2(n171), .A3(n265), .ZN(n202) );
  OAI21_X1 U248 ( .B1(n268), .B2(n208), .A(n209), .ZN(DIS2[6]) );
  MUX2_X1 U249 ( .A(n210), .B(n211), .S(n172), .Z(n209) );
  INV_X1 U250 ( .A(n212), .ZN(n211) );
  NAND4_X1 U251 ( .A1(n213), .A2(n214), .A3(n210), .A4(n215), .ZN(DIS2[5]) );
  OR2_X1 U252 ( .A1(n216), .A2(n269), .ZN(n210) );
  NAND2_X1 U253 ( .A1(n217), .A2(n176), .ZN(n213) );
  OAI221_X1 U254 ( .B1(n268), .B2(n218), .C1(n269), .C2(n180), .A(n219), .ZN(
        DIS2[4]) );
  INV_X1 U255 ( .A(n217), .ZN(n218) );
  OAI221_X1 U256 ( .B1(n172), .B2(n216), .C1(n266), .C2(n220), .A(n221), .ZN(
        DIS2[3]) );
  OAI221_X1 U257 ( .B1(n176), .B2(n222), .C1(n266), .C2(n214), .A(n208), .ZN(
        DIS2[2]) );
  NAND3_X1 U258 ( .A1(n176), .A2(n172), .A3(n268), .ZN(n214) );
  NAND2_X1 U259 ( .A1(n267), .A2(n268), .ZN(n222) );
  OAI221_X1 U260 ( .B1(n176), .B2(n216), .C1(n180), .C2(n219), .A(n223), .ZN(
        DIS2[1]) );
  AOI21_X1 U261 ( .B1(n224), .B2(n268), .A(n225), .ZN(n223) );
  INV_X1 U262 ( .A(n208), .ZN(n225) );
  NAND2_X1 U263 ( .A1(n224), .A2(n269), .ZN(n208) );
  NAND2_X1 U264 ( .A1(n212), .A2(n267), .ZN(n219) );
  NAND2_X1 U265 ( .A1(n268), .A2(n266), .ZN(n216) );
  OAI211_X1 U266 ( .C1(n180), .C2(n220), .A(n221), .B(n215), .ZN(DIS2[0]) );
  OR4_X1 U267 ( .A1(n176), .A2(n172), .A3(n180), .A4(n268), .ZN(n215) );
  OAI21_X1 U268 ( .B1(n224), .B2(n217), .A(n212), .ZN(n221) );
  NOR2_X1 U269 ( .A1(n269), .A2(n268), .ZN(n212) );
  NOR2_X1 U270 ( .A1(n180), .A2(n267), .ZN(n217) );
  NOR2_X1 U271 ( .A1(n172), .A2(n266), .ZN(n224) );
  NAND3_X1 U272 ( .A1(n268), .A2(n172), .A3(n269), .ZN(n220) );
  OAI21_X1 U273 ( .B1(n272), .B2(n226), .A(n227), .ZN(DIS1[6]) );
  MUX2_X1 U274 ( .A(n228), .B(n229), .S(n173), .Z(n227) );
  INV_X1 U275 ( .A(n230), .ZN(n229) );
  NAND4_X1 U276 ( .A1(n231), .A2(n232), .A3(n228), .A4(n233), .ZN(DIS1[5]) );
  OR2_X1 U277 ( .A1(n234), .A2(n273), .ZN(n228) );
  NAND2_X1 U278 ( .A1(n235), .A2(n177), .ZN(n231) );
  OAI221_X1 U279 ( .B1(n272), .B2(n236), .C1(n273), .C2(n181), .A(n237), .ZN(
        DIS1[4]) );
  INV_X1 U280 ( .A(n235), .ZN(n236) );
  OAI221_X1 U281 ( .B1(n173), .B2(n234), .C1(n270), .C2(n238), .A(n239), .ZN(
        DIS1[3]) );
  OAI221_X1 U282 ( .B1(n177), .B2(n240), .C1(n270), .C2(n232), .A(n226), .ZN(
        DIS1[2]) );
  NAND3_X1 U283 ( .A1(n177), .A2(n173), .A3(n272), .ZN(n232) );
  NAND2_X1 U284 ( .A1(n271), .A2(n272), .ZN(n240) );
  OAI221_X1 U285 ( .B1(n177), .B2(n234), .C1(n181), .C2(n237), .A(n241), .ZN(
        DIS1[1]) );
  AOI21_X1 U286 ( .B1(n242), .B2(n272), .A(n243), .ZN(n241) );
  INV_X1 U287 ( .A(n226), .ZN(n243) );
  NAND2_X1 U288 ( .A1(n242), .A2(n273), .ZN(n226) );
  NAND2_X1 U289 ( .A1(n230), .A2(n271), .ZN(n237) );
  NAND2_X1 U290 ( .A1(n272), .A2(n270), .ZN(n234) );
  OAI211_X1 U291 ( .C1(n181), .C2(n238), .A(n239), .B(n233), .ZN(DIS1[0]) );
  OR4_X1 U292 ( .A1(n177), .A2(n173), .A3(n181), .A4(n272), .ZN(n233) );
  OAI21_X1 U293 ( .B1(n242), .B2(n235), .A(n230), .ZN(n239) );
  NOR2_X1 U294 ( .A1(n273), .A2(n272), .ZN(n230) );
  NOR2_X1 U295 ( .A1(n181), .A2(n271), .ZN(n235) );
  NOR2_X1 U296 ( .A1(n173), .A2(n270), .ZN(n242) );
  NAND3_X1 U297 ( .A1(n272), .A2(n173), .A3(n273), .ZN(n238) );
  OAI21_X1 U298 ( .B1(n276), .B2(n244), .A(n245), .ZN(DIS0[6]) );
  MUX2_X1 U299 ( .A(n246), .B(n247), .S(n174), .Z(n245) );
  INV_X1 U300 ( .A(n248), .ZN(n247) );
  NAND4_X1 U301 ( .A1(n249), .A2(n250), .A3(n246), .A4(n251), .ZN(DIS0[5]) );
  OR2_X1 U302 ( .A1(n252), .A2(n277), .ZN(n246) );
  NAND2_X1 U303 ( .A1(n253), .A2(n178), .ZN(n249) );
  OAI221_X1 U304 ( .B1(n276), .B2(n254), .C1(n277), .C2(n182), .A(n255), .ZN(
        DIS0[4]) );
  INV_X1 U305 ( .A(n253), .ZN(n254) );
  OAI221_X1 U306 ( .B1(n174), .B2(n252), .C1(n274), .C2(n256), .A(n257), .ZN(
        DIS0[3]) );
  OAI221_X1 U307 ( .B1(n178), .B2(n258), .C1(n274), .C2(n250), .A(n244), .ZN(
        DIS0[2]) );
  NAND3_X1 U308 ( .A1(n178), .A2(n174), .A3(n276), .ZN(n250) );
  NAND2_X1 U309 ( .A1(n275), .A2(n276), .ZN(n258) );
  OAI221_X1 U310 ( .B1(n178), .B2(n252), .C1(n182), .C2(n255), .A(n259), .ZN(
        DIS0[1]) );
  AOI21_X1 U311 ( .B1(n260), .B2(n276), .A(n261), .ZN(n259) );
  INV_X1 U312 ( .A(n244), .ZN(n261) );
  NAND2_X1 U313 ( .A1(n260), .A2(n277), .ZN(n244) );
  NAND2_X1 U314 ( .A1(n248), .A2(n275), .ZN(n255) );
  NAND2_X1 U315 ( .A1(n276), .A2(n274), .ZN(n252) );
  OAI211_X1 U316 ( .C1(n182), .C2(n256), .A(n257), .B(n251), .ZN(DIS0[0]) );
  OR4_X1 U317 ( .A1(n178), .A2(n174), .A3(n182), .A4(n276), .ZN(n251) );
  OAI21_X1 U318 ( .B1(n260), .B2(n253), .A(n248), .ZN(n257) );
  NOR2_X1 U319 ( .A1(n277), .A2(n276), .ZN(n248) );
  NOR2_X1 U320 ( .A1(n182), .A2(n275), .ZN(n253) );
  NOR2_X1 U321 ( .A1(n174), .A2(n274), .ZN(n260) );
  NAND3_X1 U322 ( .A1(n276), .A2(n174), .A3(n277), .ZN(n256) );
endmodule


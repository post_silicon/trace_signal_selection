analyze -library WORK -format sverilog {/home/debjit/Work/Post_Silicon/lc3/TSB.sv /home/debjit/Work/Post_Silicon/lc3/SLC.sv /home/debjit/Work/Post_Silicon/lc3/SEXT11.sv /home/debjit/Work/Post_Silicon/lc3/SEXT9.sv /home/debjit/Work/Post_Silicon/lc3/SEXT6.sv /home/debjit/Work/Post_Silicon/lc3/SEXT5.sv /home/debjit/Work/Post_Silicon/lc3/RegFile.sv /home/debjit/Work/Post_Silicon/lc3/RegAddrMux.sv /home/debjit/Work/Post_Silicon/lc3/Reg16.sv /home/debjit/Work/Post_Silicon/lc3/NZPreg.sv /home/debjit/Work/Post_Silicon/lc3/Mux4.sv /home/debjit/Work/Post_Silicon/lc3/Mux2.sv /home/debjit/Work/Post_Silicon/lc3/Mem2IO.sv /home/debjit/Work/Post_Silicon/lc3/ISDU.sv /home/debjit/Work/Post_Silicon/lc3/HexDriver.sv /home/debjit/Work/Post_Silicon/lc3/CPU.sv /home/debjit/Work/Post_Silicon/lc3/ALU.sv}

elaborate SLC -architecture verilog -library WORK
check_design -multiple_designs
write_file -format ddc -hierarchy -output ./lc3_netlist_gtech.ddc
write -hierarchy -format verilog -output /home/debjit/Work/Post_Silicon/lc3/lc3_netlist_gtech.v
create_clock -name "Clk" -period 10 -waveform {0 2} {Clk}
set_clock_uncertainty 0.1 Clk
set_clock_latency 0.1 Clk
set_clock_transition 0.1 Clk
set_dont_touch_network Clk
set_dont_touch Reset
set_ideal_network Reset
compile
write_file -format verilog -hierarchy -output ./lc3_netlist_tech_grouped.v
remove_design -all
read_file -format ddc ./lc3_netlist_gtech.ddc
set_ungroup SLC
set_flatten true -design SLC -effort high -minimize single_output -phase false
ungroup -all -flatten
compile
write_file -format verilog -hierarchy -output ./lc3_netlist_tech_ungrouped.v
quit

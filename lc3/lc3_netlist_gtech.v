
module Mem2IO ( Clk, Reset, A, CE, UB, LB, OE, WE, Switches, Data_CPU, 
        Data_Mem, HEX0, HEX1, HEX2, HEX3 );
  input [17:0] A;
  input [15:0] Switches;
  inout [15:0] Data_CPU;
  inout [15:0] Data_Mem;
  output [3:0] HEX0;
  output [3:0] HEX1;
  output [3:0] HEX2;
  output [3:0] HEX3;
  input Clk, Reset, CE, UB, LB, OE, WE;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15,
         N16, N17, N18, N19, N20, N21, N22, N23, N24, N25, N26, N27, N28, N29,
         N30, N31, N32, N33, N34, N35, N36, N37, N38, N39, N40, N41, N42, N43,
         N44, N45, N46, N47, N48, N49, N50, N51, N52, N53, N54, N55, N56, N57,
         N58, N59, N60, N61, N62, N63, N64, N65, N66, N67, N68, N69, N70, N71,
         N72, N73, N74, N75, N76, N77, N78, N79, N80, N81, N82, N83, N84, N85,
         N86, N87, N88, N89, N90, N91, N92, N93, N94, N95, N96, N97, N98, N99,
         N100, N101, N102, N103, N104, N105, N106;
  tri   Clk;
  tri   \A[15] ;
  tri   \A[14] ;
  tri   \A[13] ;
  tri   \A[12] ;
  tri   \A[11] ;
  tri   \A[10] ;
  tri   \A[9] ;
  tri   \A[8] ;
  tri   \A[7] ;
  tri   \A[6] ;
  tri   \A[5] ;
  tri   \A[4] ;
  tri   \A[3] ;
  tri   \A[2] ;
  tri   \A[1] ;
  tri   \A[0] ;
  tri   OE;
  tri   WE;
  tri   [15:0] Data_CPU;
  tri   [15:0] Data_Mem;

  \**TSGEN**  \Data_CPU_signal_tri[15]  ( .\function (N71), .three_state(N0), 
        .\output (Data_CPU[15]) );
  GTECH_NOT I_0 ( .A(N37), .Z(N0) );
  \**TSGEN**  \Data_CPU_signal_tri[14]  ( .\function (N70), .three_state(N1), 
        .\output (Data_CPU[14]) );
  GTECH_NOT I_1 ( .A(N37), .Z(N1) );
  \**TSGEN**  \Data_CPU_signal_tri[13]  ( .\function (N69), .three_state(N2), 
        .\output (Data_CPU[13]) );
  GTECH_NOT I_2 ( .A(N37), .Z(N2) );
  \**TSGEN**  \Data_CPU_signal_tri[12]  ( .\function (N68), .three_state(N3), 
        .\output (Data_CPU[12]) );
  GTECH_NOT I_3 ( .A(N37), .Z(N3) );
  \**TSGEN**  \Data_CPU_signal_tri[11]  ( .\function (N67), .three_state(N4), 
        .\output (Data_CPU[11]) );
  GTECH_NOT I_4 ( .A(N37), .Z(N4) );
  \**TSGEN**  \Data_CPU_signal_tri[10]  ( .\function (N66), .three_state(N5), 
        .\output (Data_CPU[10]) );
  GTECH_NOT I_5 ( .A(N37), .Z(N5) );
  \**TSGEN**  \Data_CPU_signal_tri[9]  ( .\function (N65), .three_state(N6), 
        .\output (Data_CPU[9]) );
  GTECH_NOT I_6 ( .A(N37), .Z(N6) );
  \**TSGEN**  \Data_CPU_signal_tri[8]  ( .\function (N64), .three_state(N7), 
        .\output (Data_CPU[8]) );
  GTECH_NOT I_7 ( .A(N37), .Z(N7) );
  \**TSGEN**  \Data_CPU_signal_tri[7]  ( .\function (N63), .three_state(N8), 
        .\output (Data_CPU[7]) );
  GTECH_NOT I_8 ( .A(N37), .Z(N8) );
  \**TSGEN**  \Data_CPU_signal_tri[6]  ( .\function (N62), .three_state(N9), 
        .\output (Data_CPU[6]) );
  GTECH_NOT I_9 ( .A(N37), .Z(N9) );
  \**TSGEN**  \Data_CPU_signal_tri[5]  ( .\function (N61), .three_state(N10), 
        .\output (Data_CPU[5]) );
  GTECH_NOT I_10 ( .A(N37), .Z(N10) );
  \**TSGEN**  \Data_CPU_signal_tri[4]  ( .\function (N60), .three_state(N11), 
        .\output (Data_CPU[4]) );
  GTECH_NOT I_11 ( .A(N37), .Z(N11) );
  \**TSGEN**  \Data_CPU_signal_tri[3]  ( .\function (N59), .three_state(N12), 
        .\output (Data_CPU[3]) );
  GTECH_NOT I_12 ( .A(N37), .Z(N12) );
  \**TSGEN**  \Data_CPU_signal_tri[2]  ( .\function (N58), .three_state(N13), 
        .\output (Data_CPU[2]) );
  GTECH_NOT I_13 ( .A(N37), .Z(N13) );
  \**TSGEN**  \Data_CPU_signal_tri[1]  ( .\function (N57), .three_state(N14), 
        .\output (Data_CPU[1]) );
  GTECH_NOT I_14 ( .A(N37), .Z(N14) );
  \**TSGEN**  \Data_CPU_signal_tri[0]  ( .\function (N56), .three_state(N15), 
        .\output (Data_CPU[0]) );
  GTECH_NOT I_15 ( .A(N37), .Z(N15) );
  \**TSGEN**  \Data_Mem_tri[15]  ( .\function (N88), .three_state(N16), 
        .\output (Data_Mem[15]) );
  GTECH_NOT I_16 ( .A(N89), .Z(N16) );
  \**TSGEN**  \Data_Mem_tri[14]  ( .\function (N87), .three_state(N17), 
        .\output (Data_Mem[14]) );
  GTECH_NOT I_17 ( .A(N89), .Z(N17) );
  \**TSGEN**  \Data_Mem_tri[13]  ( .\function (N86), .three_state(N18), 
        .\output (Data_Mem[13]) );
  GTECH_NOT I_18 ( .A(N89), .Z(N18) );
  \**TSGEN**  \Data_Mem_tri[12]  ( .\function (N85), .three_state(N19), 
        .\output (Data_Mem[12]) );
  GTECH_NOT I_19 ( .A(N89), .Z(N19) );
  \**TSGEN**  \Data_Mem_tri[11]  ( .\function (N84), .three_state(N20), 
        .\output (Data_Mem[11]) );
  GTECH_NOT I_20 ( .A(N89), .Z(N20) );
  \**TSGEN**  \Data_Mem_tri[10]  ( .\function (N83), .three_state(N21), 
        .\output (Data_Mem[10]) );
  GTECH_NOT I_21 ( .A(N89), .Z(N21) );
  \**TSGEN**  \Data_Mem_tri[9]  ( .\function (N82), .three_state(N22), 
        .\output (Data_Mem[9]) );
  GTECH_NOT I_22 ( .A(N89), .Z(N22) );
  \**TSGEN**  \Data_Mem_tri[8]  ( .\function (N81), .three_state(N23), 
        .\output (Data_Mem[8]) );
  GTECH_NOT I_23 ( .A(N89), .Z(N23) );
  \**TSGEN**  \Data_Mem_tri[7]  ( .\function (N80), .three_state(N24), 
        .\output (Data_Mem[7]) );
  GTECH_NOT I_24 ( .A(N89), .Z(N24) );
  \**TSGEN**  \Data_Mem_tri[6]  ( .\function (N79), .three_state(N25), 
        .\output (Data_Mem[6]) );
  GTECH_NOT I_25 ( .A(N89), .Z(N25) );
  \**TSGEN**  \Data_Mem_tri[5]  ( .\function (N78), .three_state(N26), 
        .\output (Data_Mem[5]) );
  GTECH_NOT I_26 ( .A(N89), .Z(N26) );
  \**TSGEN**  \Data_Mem_tri[4]  ( .\function (N77), .three_state(N27), 
        .\output (Data_Mem[4]) );
  GTECH_NOT I_27 ( .A(N89), .Z(N27) );
  \**TSGEN**  \Data_Mem_tri[3]  ( .\function (N76), .three_state(N28), 
        .\output (Data_Mem[3]) );
  GTECH_NOT I_28 ( .A(N89), .Z(N28) );
  \**TSGEN**  \Data_Mem_tri[2]  ( .\function (N75), .three_state(N29), 
        .\output (Data_Mem[2]) );
  GTECH_NOT I_29 ( .A(N89), .Z(N29) );
  \**TSGEN**  \Data_Mem_tri[1]  ( .\function (N74), .three_state(N30), 
        .\output (Data_Mem[1]) );
  GTECH_NOT I_30 ( .A(N89), .Z(N30) );
  \**TSGEN**  \Data_Mem_tri[0]  ( .\function (N73), .three_state(N31), 
        .\output (Data_Mem[0]) );
  GTECH_NOT I_31 ( .A(N89), .Z(N31) );
  \**SEQGEN**  \hex_data_reg[15]  ( .clear(Reset), .preset(1'b0), .next_state(
        Data_CPU[15]), .clocked_on(Clk), .data_in(1'b0), .enable(1'b0), .Q(
        HEX3[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N90) );
  \**SEQGEN**  \hex_data_reg[14]  ( .clear(Reset), .preset(1'b0), .next_state(
        Data_CPU[14]), .clocked_on(Clk), .data_in(1'b0), .enable(1'b0), .Q(
        HEX3[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N90) );
  \**SEQGEN**  \hex_data_reg[13]  ( .clear(Reset), .preset(1'b0), .next_state(
        Data_CPU[13]), .clocked_on(Clk), .data_in(1'b0), .enable(1'b0), .Q(
        HEX3[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N90) );
  \**SEQGEN**  \hex_data_reg[12]  ( .clear(Reset), .preset(1'b0), .next_state(
        Data_CPU[12]), .clocked_on(Clk), .data_in(1'b0), .enable(1'b0), .Q(
        HEX3[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N90) );
  \**SEQGEN**  \hex_data_reg[11]  ( .clear(Reset), .preset(1'b0), .next_state(
        Data_CPU[11]), .clocked_on(Clk), .data_in(1'b0), .enable(1'b0), .Q(
        HEX2[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N90) );
  \**SEQGEN**  \hex_data_reg[10]  ( .clear(Reset), .preset(1'b0), .next_state(
        Data_CPU[10]), .clocked_on(Clk), .data_in(1'b0), .enable(1'b0), .Q(
        HEX2[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N90) );
  \**SEQGEN**  \hex_data_reg[9]  ( .clear(Reset), .preset(1'b0), .next_state(
        Data_CPU[9]), .clocked_on(Clk), .data_in(1'b0), .enable(1'b0), .Q(
        HEX2[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N90) );
  \**SEQGEN**  \hex_data_reg[8]  ( .clear(Reset), .preset(1'b0), .next_state(
        Data_CPU[8]), .clocked_on(Clk), .data_in(1'b0), .enable(1'b0), .Q(
        HEX2[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N90) );
  \**SEQGEN**  \hex_data_reg[7]  ( .clear(Reset), .preset(1'b0), .next_state(
        Data_CPU[7]), .clocked_on(Clk), .data_in(1'b0), .enable(1'b0), .Q(
        HEX1[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N90) );
  \**SEQGEN**  \hex_data_reg[6]  ( .clear(Reset), .preset(1'b0), .next_state(
        Data_CPU[6]), .clocked_on(Clk), .data_in(1'b0), .enable(1'b0), .Q(
        HEX1[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N90) );
  \**SEQGEN**  \hex_data_reg[5]  ( .clear(Reset), .preset(1'b0), .next_state(
        Data_CPU[5]), .clocked_on(Clk), .data_in(1'b0), .enable(1'b0), .Q(
        HEX1[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N90) );
  \**SEQGEN**  \hex_data_reg[4]  ( .clear(Reset), .preset(1'b0), .next_state(
        Data_CPU[4]), .clocked_on(Clk), .data_in(1'b0), .enable(1'b0), .Q(
        HEX1[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N90) );
  \**SEQGEN**  \hex_data_reg[3]  ( .clear(Reset), .preset(1'b0), .next_state(
        Data_CPU[3]), .clocked_on(Clk), .data_in(1'b0), .enable(1'b0), .Q(
        HEX0[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N90) );
  \**SEQGEN**  \hex_data_reg[2]  ( .clear(Reset), .preset(1'b0), .next_state(
        Data_CPU[2]), .clocked_on(Clk), .data_in(1'b0), .enable(1'b0), .Q(
        HEX0[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N90) );
  \**SEQGEN**  \hex_data_reg[1]  ( .clear(Reset), .preset(1'b0), .next_state(
        Data_CPU[1]), .clocked_on(Clk), .data_in(1'b0), .enable(1'b0), .Q(
        HEX0[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N90) );
  \**SEQGEN**  \hex_data_reg[0]  ( .clear(Reset), .preset(1'b0), .next_state(
        Data_CPU[0]), .clocked_on(Clk), .data_in(1'b0), .enable(1'b0), .Q(
        HEX0[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N90) );
  GTECH_AND2 C224 ( .A(A[14]), .B(A[15]), .Z(N91) );
  GTECH_AND2 C225 ( .A(A[13]), .B(N91), .Z(N92) );
  GTECH_AND2 C226 ( .A(A[12]), .B(N92), .Z(N93) );
  GTECH_AND2 C227 ( .A(A[11]), .B(N93), .Z(N94) );
  GTECH_AND2 C228 ( .A(A[10]), .B(N94), .Z(N95) );
  GTECH_AND2 C229 ( .A(A[9]), .B(N95), .Z(N96) );
  GTECH_AND2 C230 ( .A(A[8]), .B(N96), .Z(N97) );
  GTECH_AND2 C231 ( .A(A[7]), .B(N97), .Z(N98) );
  GTECH_AND2 C232 ( .A(A[6]), .B(N98), .Z(N99) );
  GTECH_AND2 C233 ( .A(A[5]), .B(N99), .Z(N100) );
  GTECH_AND2 C234 ( .A(A[4]), .B(N100), .Z(N101) );
  GTECH_AND2 C235 ( .A(A[3]), .B(N101), .Z(N102) );
  GTECH_AND2 C236 ( .A(A[2]), .B(N102), .Z(N103) );
  GTECH_AND2 C237 ( .A(A[1]), .B(N103), .Z(N104) );
  GTECH_AND2 C238 ( .A(A[0]), .B(N104), .Z(N105) );
  SELECT_OP C254 ( .DATA1(Switches), .DATA2(Data_Mem), .CONTROL1(N32), 
        .CONTROL2(N39), .Z({N55, N54, N53, N52, N51, N50, N49, N48, N47, N46, 
        N45, N44, N43, N42, N41, N40}) );
  GTECH_BUF B_0 ( .A(N105), .Z(N32) );
  SELECT_OP C255 ( .DATA1({N55, N54, N53, N52, N51, N50, N49, N48, N47, N46, 
        N45, N44, N43, N42, N41, N40}), .DATA2({N36, N36, N36, N36, N36, N36, 
        N36, N36, N36, N36, N36, N36, N36, N36, N36, N36}), .CONTROL1(N33), 
        .CONTROL2(N38), .Z({N71, N70, N69, N68, N67, N66, N65, N64, N63, N62, 
        N61, N60, N59, N58, N57, N56}) );
  GTECH_BUF B_1 ( .A(N37), .Z(N33) );
  SELECT_OP C272 ( .DATA1(Data_CPU), .DATA2({N36, N36, N36, N36, N36, N36, N36, 
        N36, N36, N36, N36, N36, N36, N36, N36, N36}), .CONTROL1(N34), 
        .CONTROL2(N35), .Z({N88, N87, N86, N85, N84, N83, N82, N81, N80, N79, 
        N78, N77, N76, N75, N74, N73}) );
  GTECH_BUF B_2 ( .A(N72), .Z(N34) );
  GTECH_BUF B_3 ( .A(WE), .Z(N35) );  assign N36 = 1'b0;

  GTECH_AND2 C278 ( .A(WE), .B(N106), .Z(N37) );
  GTECH_NOT I_32 ( .A(OE), .Z(N106) );
  GTECH_NOT I_33 ( .A(N37), .Z(N38) );
  GTECH_NOT I_34 ( .A(N105), .Z(N39) );
  GTECH_NOT I_35 ( .A(WE), .Z(N72) );
  GTECH_NOT I_36 ( .A(WE), .Z(N89) );
  GTECH_AND2 C319 ( .A(N72), .B(N105), .Z(N90) );
endmodule


module HexDriver ( In0, Out0 );
  input [3:0] In0;
  output [6:0] Out0;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15,
         N16, N17, N18, N19, N20, N21, N22, N23, N24, N25, N26, N27, N28, N29,
         N30, N31, N32, N33, N34, N35, N36, N37, N38, N39, N40, N41, N42, N43,
         N44, N45, N46, N47, N48, N49, N50, N51, N52, N53, N54, N55, N56, N57,
         N58, N59, N60, N61;

  GTECH_AND2 C8 ( .A(N16), .B(N17), .Z(N20) );
  GTECH_AND2 C9 ( .A(N18), .B(N19), .Z(N21) );
  GTECH_AND2 C10 ( .A(N20), .B(N21), .Z(N22) );
  GTECH_OR2 C12 ( .A(In0[3]), .B(In0[2]), .Z(N23) );
  GTECH_OR2 C13 ( .A(In0[1]), .B(N19), .Z(N24) );
  GTECH_OR2 C14 ( .A(N23), .B(N24), .Z(N25) );
  GTECH_OR2 C18 ( .A(N18), .B(In0[0]), .Z(N27) );
  GTECH_OR2 C19 ( .A(N23), .B(N27), .Z(N28) );
  GTECH_OR2 C24 ( .A(N18), .B(N19), .Z(N30) );
  GTECH_OR2 C25 ( .A(N23), .B(N30), .Z(N31) );
  GTECH_OR2 C28 ( .A(In0[3]), .B(N17), .Z(N33) );
  GTECH_OR2 C29 ( .A(In0[1]), .B(In0[0]), .Z(N34) );
  GTECH_OR2 C30 ( .A(N33), .B(N34), .Z(N35) );
  GTECH_OR2 C36 ( .A(N33), .B(N24), .Z(N37) );
  GTECH_OR2 C42 ( .A(N33), .B(N27), .Z(N39) );
  GTECH_OR2 C49 ( .A(N33), .B(N30), .Z(N41) );
  GTECH_OR2 C52 ( .A(N16), .B(In0[2]), .Z(N43) );
  GTECH_OR2 C54 ( .A(N43), .B(N34), .Z(N44) );
  GTECH_OR2 C60 ( .A(N43), .B(N24), .Z(N46) );
  GTECH_OR2 C66 ( .A(N43), .B(N27), .Z(N48) );
  GTECH_OR2 C73 ( .A(N43), .B(N30), .Z(N50) );
  GTECH_OR2 C77 ( .A(N16), .B(N17), .Z(N52) );
  GTECH_OR2 C79 ( .A(N52), .B(N34), .Z(N53) );
  GTECH_OR2 C86 ( .A(N52), .B(N24), .Z(N55) );
  GTECH_OR2 C93 ( .A(N52), .B(N27), .Z(N57) );
  GTECH_AND2 C95 ( .A(In0[3]), .B(In0[2]), .Z(N59) );
  GTECH_AND2 C96 ( .A(In0[1]), .B(In0[0]), .Z(N60) );
  GTECH_AND2 C97 ( .A(N59), .B(N60), .Z(N61) );
  SELECT_OP C138 ( .DATA1({1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), .DATA2(
        {1'b1, 1'b1, 1'b1, 1'b1, 1'b0, 1'b0, 1'b1}), .DATA3({1'b0, 1'b1, 1'b0, 
        1'b0, 1'b1, 1'b0, 1'b0}), .DATA4({1'b0, 1'b1, 1'b1, 1'b0, 1'b0, 1'b0, 
        1'b0}), .DATA5({1'b0, 1'b0, 1'b1, 1'b1, 1'b0, 1'b0, 1'b1}), .DATA6({
        1'b0, 1'b0, 1'b1, 1'b0, 1'b0, 1'b1, 1'b0}), .DATA7({1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b1, 1'b0}), .DATA8({1'b1, 1'b1, 1'b1, 1'b1, 1'b0, 1'b0, 
        1'b0}), .DATA9({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), .DATA10({
        1'b0, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0}), .DATA11({1'b0, 1'b0, 1'b0, 
        1'b1, 1'b0, 1'b0, 1'b0}), .DATA12({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 
        1'b1}), .DATA13({1'b1, 1'b0, 1'b0, 1'b0, 1'b1, 1'b1, 1'b0}), .DATA14({
        1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1}), .DATA15({1'b0, 1'b0, 1'b0, 
        1'b0, 1'b1, 1'b1, 1'b0}), .DATA16({1'b0, 1'b0, 1'b0, 1'b1, 1'b1, 1'b1, 
        1'b0}), .CONTROL1(N0), .CONTROL2(N1), .CONTROL3(N2), .CONTROL4(N3), 
        .CONTROL5(N4), .CONTROL6(N5), .CONTROL7(N6), .CONTROL8(N7), .CONTROL9(
        N8), .CONTROL10(N9), .CONTROL11(N10), .CONTROL12(N11), .CONTROL13(N12), 
        .CONTROL14(N13), .CONTROL15(N14), .CONTROL16(N15), .Z(Out0) );
  GTECH_BUF B_0 ( .A(N22), .Z(N0) );
  GTECH_BUF B_1 ( .A(N26), .Z(N1) );
  GTECH_BUF B_2 ( .A(N29), .Z(N2) );
  GTECH_BUF B_3 ( .A(N32), .Z(N3) );
  GTECH_BUF B_4 ( .A(N36), .Z(N4) );
  GTECH_BUF B_5 ( .A(N38), .Z(N5) );
  GTECH_BUF B_6 ( .A(N40), .Z(N6) );
  GTECH_BUF B_7 ( .A(N42), .Z(N7) );
  GTECH_BUF B_8 ( .A(N45), .Z(N8) );
  GTECH_BUF B_9 ( .A(N47), .Z(N9) );
  GTECH_BUF B_10 ( .A(N49), .Z(N10) );
  GTECH_BUF B_11 ( .A(N51), .Z(N11) );
  GTECH_BUF B_12 ( .A(N54), .Z(N12) );
  GTECH_BUF B_13 ( .A(N56), .Z(N13) );
  GTECH_BUF B_14 ( .A(N58), .Z(N14) );
  GTECH_BUF B_15 ( .A(N61), .Z(N15) );
  GTECH_NOT I_0 ( .A(In0[3]), .Z(N16) );
  GTECH_NOT I_1 ( .A(In0[2]), .Z(N17) );
  GTECH_NOT I_2 ( .A(In0[1]), .Z(N18) );
  GTECH_NOT I_3 ( .A(In0[0]), .Z(N19) );
  GTECH_NOT I_4 ( .A(N25), .Z(N26) );
  GTECH_NOT I_5 ( .A(N28), .Z(N29) );
  GTECH_NOT I_6 ( .A(N31), .Z(N32) );
  GTECH_NOT I_7 ( .A(N35), .Z(N36) );
  GTECH_NOT I_8 ( .A(N37), .Z(N38) );
  GTECH_NOT I_9 ( .A(N39), .Z(N40) );
  GTECH_NOT I_10 ( .A(N41), .Z(N42) );
  GTECH_NOT I_11 ( .A(N44), .Z(N45) );
  GTECH_NOT I_12 ( .A(N46), .Z(N47) );
  GTECH_NOT I_13 ( .A(N48), .Z(N49) );
  GTECH_NOT I_14 ( .A(N50), .Z(N51) );
  GTECH_NOT I_15 ( .A(N53), .Z(N54) );
  GTECH_NOT I_16 ( .A(N55), .Z(N56) );
  GTECH_NOT I_17 ( .A(N57), .Z(N58) );
endmodule


module SLC ( Clk, Reset, Run, Continue, ContinueIR, S, CE, UB, LB, OE, WE, A, 
        LED, DIS3, DIS2, DIS1, DIS0, MEMdata );
  input [15:0] S;
  output [17:0] A;
  output [11:0] LED;
  output [6:0] DIS3;
  output [6:0] DIS2;
  output [6:0] DIS1;
  output [6:0] DIS0;
  inout [15:0] MEMdata;
  input Clk, Reset, Run, Continue, ContinueIR;
  output CE, UB, LB, OE, WE;
  wire   _4_net_;
  wire   [3:0] HEX3;
  wire   [3:0] HEX2;
  wire   [3:0] HEX1;
  wire   [3:0] HEX0;
  tri   Clk;
  tri   CE;
  tri   UB;
  tri   LB;
  tri   OE;
  tri   WE;
  tri   [17:0] A;
  tri   [11:0] LED;
  tri   [15:0] MEMdata;
  tri   _0_net_;
  tri   _1_net_;
  tri   _2_net_;
  tri   _3_net_;
  tri   \CPUdata[15] ;
  tri   \CPUdata[14] ;
  tri   \CPUdata[13] ;
  tri   \CPUdata[12] ;
  tri   \CPUdata[11] ;
  tri   \CPUdata[10] ;
  tri   \CPUdata[9] ;
  tri   \CPUdata[8] ;
  tri   \CPUdata[7] ;
  tri   \CPUdata[6] ;
  tri   \CPUdata[5] ;
  tri   \CPUdata[4] ;
  tri   \CPUdata[3] ;
  tri   \CPUdata[2] ;
  tri   \CPUdata[1] ;
  tri   \CPUdata[0] ;

  CPU cpu ( .Reset(_0_net_), .Run(_1_net_), .Continue(_2_net_), .ContinueIR(
        _3_net_), .Data({\CPUdata[15] , \CPUdata[14] , \CPUdata[13] , 
        \CPUdata[12] , \CPUdata[11] , \CPUdata[10] , \CPUdata[9] , 
        \CPUdata[8] , \CPUdata[7] , \CPUdata[6] , \CPUdata[5] , \CPUdata[4] , 
        \CPUdata[3] , \CPUdata[2] , \CPUdata[1] , \CPUdata[0] }), .ADDR(A), 
        .LED(LED), .Mem_WE(WE), .Mem_OE(OE), .Mem_LB(LB), .Mem_UB(UB), 
        .Mem_CE(CE), .Clk(Clk) );
  Mem2IO mem2io ( .Clk(Clk), .Reset(_4_net_), .A(A), .CE(CE), .UB(UB), .LB(LB), 
        .OE(OE), .WE(WE), .Switches(S), .Data_CPU({\CPUdata[15] , 
        \CPUdata[14] , \CPUdata[13] , \CPUdata[12] , \CPUdata[11] , 
        \CPUdata[10] , \CPUdata[9] , \CPUdata[8] , \CPUdata[7] , \CPUdata[6] , 
        \CPUdata[5] , \CPUdata[4] , \CPUdata[3] , \CPUdata[2] , \CPUdata[1] , 
        \CPUdata[0] }), .Data_Mem(MEMdata), .HEX0(HEX0), .HEX1(HEX1), .HEX2(
        HEX2), .HEX3(HEX3) );
  HexDriver H3 ( .In0(HEX3), .Out0(DIS3) );
  HexDriver H2 ( .In0(HEX2), .Out0(DIS2) );
  HexDriver H1 ( .In0(HEX1), .Out0(DIS1) );
  HexDriver H0 ( .In0(HEX0), .Out0(DIS0) );
  GTECH_NOT I_0 ( .A(ContinueIR), .Z(_3_net_) );
  GTECH_NOT I_1 ( .A(Continue), .Z(_2_net_) );
  GTECH_NOT I_2 ( .A(Run), .Z(_1_net_) );
  GTECH_NOT I_3 ( .A(Reset), .Z(_0_net_) );
  GTECH_NOT I_4 ( .A(Reset), .Z(_4_net_) );
endmodule


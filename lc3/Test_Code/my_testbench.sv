`timescale 1ns / 10ps
module my_testbench();


// Ask Joe about how we can use the supplied test program

logic Clk = 0;
logic Reset;

logic Mem_CE, Mem_UB, Mem_LB, Mem_OE, Mem_WE;
logic [19:0] ADDR;
tri [15:0] Data_CPU;
tri [15:0] Data_Mem;
logic [11:0] LED;

// PINS in CPU meant for FPGA board as per my understanding and should not be relevant to the simulation or I may be wrong. Ask Joe.
logic Run, Continue, ContinueIR;
logic [3:0] HEX0, HEX1, HEX2, HEX3;
logic [6:0] DIS0, DIS1, DIS2, DIS3;
logic [15:0] Switches;

initial begin: SIGNAL_DUMP
	$dumpfile("lc3_cpu.vcd");
	$dumpvars(1, my_testbench._CPU.isdu.State_out);
	$dumpvars(1, my_testbench._CPU.PC.Dout);
	$dumpvars(1, my_testbench._CPU.nzpreg.NZP);
	//$dumpvars(1, my_testbench._Mem2IO.hex_data);
	$dumpvars(1, my_testbench._CPU.IR.Dout);
	#250 $finish;	
end


initial begin: CLOCK_INIT
	Clk = 0;
end

always begin: CLOCK_GEN
	#1 Clk = ~Clk;
end

initial begin: TEST_VEC
	// Initial Setup
	Reset = 1;
	Switches = 16'h0000;
	// After initial clock cycles circuit is back to normal op mode
	#10 Reset = 0;
	#20  Run = 1;
//	#100 Switches = 4'h0001;
//	#100 Switches = 4'h0003;
//	#100 Switches = 4'h0005;
end

always begin
	#8;
	Continue = $random;
	ContinueIR = $random;
end

always begin
	#18;
	Switches = $random;
end

CPU _CPU ( 	.Clk(Clk),
		.Reset(Reset),
		.Run(Run),
		.Continue(Continue),
		.ContinueIR(ContinueIR),
		.Mem_CE(Mem_CE),
		.Mem_UB(Mem_UB),
		.Mem_LB(Mem_LB),
		.Mem_OE(Mem_OE),
		.Mem_WE(Mem_WE),
		.LED(LED),
		.ADDR(ADDR),
		.Data(Data_CPU)
		);

Mem2IO _Mem2IO (	.Clk(Clk),
			.Reset(Reset),
			// Ask Joe, both in Mem2IO and in test_memory they are Input
			// In Mem2IO.sv, A is [19:0], In CPU it is [17:0] and in SLC they are mapped 
			.A(ADDR),
			.CE(Mem_CE),
			.UB(Mem_UB),
			.LB(Mem_LB),
			.OE(Mem_OE),
			.WE(Mem_WE),
			// Ask Joe about the. Looks like it is related to the board
			.Switches(Switches),
			.Data_CPU(Data_CPU),
			.Data_Mem(Data_Mem),
			.HEX0(HEX0),
			.HEX1(HEX1),
			.HEX2(HEX2),
			.HEX3(HEX3)
			);

HexDriver _H3(	.In0(HEX3), 
		.Out0(DIS3)
		);
HexDriver _H2(	.In0(HEX2), 
		.Out0(DIS2)
		);
HexDriver _H1(	.In0(HEX1),
		.Out0(DIS1)
		);
HexDriver _H0(	.In0(HEX0),
		.Out0(DIS0)
		);


test_memory _test_memory (	.Clk(Clk),
				.Reset(Reset),
				.I_O(Data_Mem),
				// Ask Joe about this
				// I believe it is the same memory signal from CPU which is also 
				// connected to Mem2IO
				.A(ADDR),
				.CE(Mem_CE),
				.UB(Mem_UB),
				.LB(Mem_LB),
				.OE(Mem_OE),
				.WE(Mem_WE)
				);


endmodule


module Mem2IO ( Clk, Reset, A, CE, UB, LB, OE, WE, Switches, Data_CPU, 
        Data_Mem, HEX0, HEX1, HEX2, HEX3 );
  input [17:0] A;
  input [15:0] Switches;
  inout [15:0] Data_CPU;
  inout [15:0] Data_Mem;
  output [3:0] HEX0;
  output [3:0] HEX1;
  output [3:0] HEX2;
  output [3:0] HEX3;
  input Clk, Reset, CE, UB, LB, OE, WE;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31,
         n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45,
         n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59,
         n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73,
         n74, n75, n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87,
         n88, n89, n90, n91, n19;
  tri   Clk;
  tri   \A[15] ;
  tri   \A[14] ;
  tri   \A[13] ;
  tri   \A[12] ;
  tri   \A[11] ;
  tri   \A[10] ;
  tri   \A[9] ;
  tri   \A[8] ;
  tri   \A[7] ;
  tri   \A[6] ;
  tri   \A[5] ;
  tri   \A[4] ;
  tri   \A[3] ;
  tri   \A[2] ;
  tri   \A[1] ;
  tri   \A[0] ;
  tri   OE;
  tri   WE;
  tri   [15:0] Data_CPU;
  tri   [15:0] Data_Mem;

  DFFR_X1 \hex_data_reg[15]  ( .D(n91), .CK(Clk), .RN(n90), .Q(HEX3[3]), .QN(
        n57) );
  DFFR_X1 \hex_data_reg[14]  ( .D(n89), .CK(Clk), .RN(n90), .Q(HEX3[2]), .QN(
        n56) );
  DFFR_X1 \hex_data_reg[13]  ( .D(n88), .CK(Clk), .RN(n90), .Q(HEX3[1]), .QN(
        n55) );
  DFFR_X1 \hex_data_reg[12]  ( .D(n87), .CK(Clk), .RN(n90), .Q(HEX3[0]), .QN(
        n54) );
  DFFR_X1 \hex_data_reg[11]  ( .D(n86), .CK(Clk), .RN(n90), .Q(HEX2[3]), .QN(
        n53) );
  DFFR_X1 \hex_data_reg[10]  ( .D(n85), .CK(Clk), .RN(n90), .Q(HEX2[2]), .QN(
        n52) );
  DFFR_X1 \hex_data_reg[9]  ( .D(n84), .CK(Clk), .RN(n90), .Q(HEX2[1]), .QN(
        n51) );
  DFFR_X1 \hex_data_reg[8]  ( .D(n83), .CK(Clk), .RN(n90), .Q(HEX2[0]), .QN(
        n50) );
  DFFR_X1 \hex_data_reg[7]  ( .D(n82), .CK(Clk), .RN(n90), .Q(HEX1[3]), .QN(
        n49) );
  DFFR_X1 \hex_data_reg[6]  ( .D(n81), .CK(Clk), .RN(n90), .Q(HEX1[2]), .QN(
        n48) );
  DFFR_X1 \hex_data_reg[5]  ( .D(n80), .CK(Clk), .RN(n90), .Q(HEX1[1]), .QN(
        n47) );
  DFFR_X1 \hex_data_reg[4]  ( .D(n79), .CK(Clk), .RN(n90), .Q(HEX1[0]), .QN(
        n46) );
  DFFR_X1 \hex_data_reg[3]  ( .D(n78), .CK(Clk), .RN(n90), .Q(HEX0[3]), .QN(
        n45) );
  DFFR_X1 \hex_data_reg[2]  ( .D(n77), .CK(Clk), .RN(n90), .Q(HEX0[2]), .QN(
        n44) );
  DFFR_X1 \hex_data_reg[1]  ( .D(n76), .CK(Clk), .RN(n90), .Q(HEX0[1]), .QN(
        n43) );
  DFFR_X1 \hex_data_reg[0]  ( .D(n75), .CK(Clk), .RN(n90), .Q(HEX0[0]), .QN(
        n42) );
  OAI22_X1 U4 ( .A1(n42), .A2(n1), .B1(n2), .B2(n3), .ZN(n75) );
  INV_X1 U5 ( .A(Data_CPU[0]), .ZN(n3) );
  OAI22_X1 U6 ( .A1(n43), .A2(n1), .B1(n2), .B2(n4), .ZN(n76) );
  INV_X1 U7 ( .A(Data_CPU[1]), .ZN(n4) );
  OAI22_X1 U8 ( .A1(n44), .A2(n1), .B1(n2), .B2(n5), .ZN(n77) );
  INV_X1 U9 ( .A(Data_CPU[2]), .ZN(n5) );
  OAI22_X1 U10 ( .A1(n45), .A2(n1), .B1(n2), .B2(n6), .ZN(n78) );
  INV_X1 U11 ( .A(Data_CPU[3]), .ZN(n6) );
  OAI22_X1 U12 ( .A1(n46), .A2(n1), .B1(n2), .B2(n7), .ZN(n79) );
  INV_X1 U13 ( .A(Data_CPU[4]), .ZN(n7) );
  OAI22_X1 U14 ( .A1(n47), .A2(n1), .B1(n2), .B2(n8), .ZN(n80) );
  INV_X1 U15 ( .A(Data_CPU[5]), .ZN(n8) );
  OAI22_X1 U16 ( .A1(n48), .A2(n1), .B1(n2), .B2(n9), .ZN(n81) );
  INV_X1 U17 ( .A(Data_CPU[6]), .ZN(n9) );
  OAI22_X1 U18 ( .A1(n49), .A2(n1), .B1(n2), .B2(n10), .ZN(n82) );
  INV_X1 U19 ( .A(Data_CPU[7]), .ZN(n10) );
  OAI22_X1 U20 ( .A1(n50), .A2(n1), .B1(n2), .B2(n11), .ZN(n83) );
  INV_X1 U21 ( .A(Data_CPU[8]), .ZN(n11) );
  OAI22_X1 U22 ( .A1(n51), .A2(n1), .B1(n2), .B2(n12), .ZN(n84) );
  INV_X1 U23 ( .A(Data_CPU[9]), .ZN(n12) );
  OAI22_X1 U24 ( .A1(n52), .A2(n1), .B1(n2), .B2(n13), .ZN(n85) );
  INV_X1 U25 ( .A(Data_CPU[10]), .ZN(n13) );
  OAI22_X1 U26 ( .A1(n53), .A2(n1), .B1(n2), .B2(n14), .ZN(n86) );
  INV_X1 U27 ( .A(Data_CPU[11]), .ZN(n14) );
  OAI22_X1 U28 ( .A1(n54), .A2(n1), .B1(n2), .B2(n15), .ZN(n87) );
  INV_X1 U29 ( .A(Data_CPU[12]), .ZN(n15) );
  OAI22_X1 U30 ( .A1(n55), .A2(n1), .B1(n2), .B2(n16), .ZN(n88) );
  INV_X1 U31 ( .A(Data_CPU[13]), .ZN(n16) );
  OAI22_X1 U32 ( .A1(n56), .A2(n1), .B1(n2), .B2(n17), .ZN(n89) );
  INV_X1 U33 ( .A(Data_CPU[14]), .ZN(n17) );
  INV_X1 U34 ( .A(Reset), .ZN(n90) );
  OAI22_X1 U35 ( .A1(n57), .A2(n1), .B1(n2), .B2(n18), .ZN(n91) );
  INV_X1 U36 ( .A(Data_CPU[15]), .ZN(n18) );
  INV_X1 U39 ( .A(n20), .ZN(n64) );
  AOI22_X1 U40 ( .A1(Data_Mem[9]), .A2(n19), .B1(Switches[9]), .B2(n21), .ZN(
        n20) );
  INV_X1 U41 ( .A(n22), .ZN(n66) );
  AOI22_X1 U42 ( .A1(Data_Mem[8]), .A2(n19), .B1(Switches[8]), .B2(n21), .ZN(
        n22) );
  INV_X1 U43 ( .A(n23), .ZN(n67) );
  AOI22_X1 U44 ( .A1(Data_Mem[7]), .A2(n19), .B1(Switches[7]), .B2(n21), .ZN(
        n23) );
  INV_X1 U45 ( .A(n24), .ZN(n68) );
  AOI22_X1 U46 ( .A1(Data_Mem[6]), .A2(n19), .B1(Switches[6]), .B2(n21), .ZN(
        n24) );
  INV_X1 U47 ( .A(n25), .ZN(n69) );
  AOI22_X1 U48 ( .A1(Data_Mem[5]), .A2(n19), .B1(Switches[5]), .B2(n21), .ZN(
        n25) );
  INV_X1 U49 ( .A(n26), .ZN(n70) );
  AOI22_X1 U50 ( .A1(Data_Mem[4]), .A2(n19), .B1(Switches[4]), .B2(n21), .ZN(
        n26) );
  INV_X1 U51 ( .A(n27), .ZN(n71) );
  AOI22_X1 U52 ( .A1(Data_Mem[3]), .A2(n19), .B1(Switches[3]), .B2(n21), .ZN(
        n27) );
  INV_X1 U53 ( .A(n28), .ZN(n72) );
  AOI22_X1 U54 ( .A1(Data_Mem[2]), .A2(n19), .B1(Switches[2]), .B2(n21), .ZN(
        n28) );
  INV_X1 U55 ( .A(n29), .ZN(n73) );
  AOI22_X1 U56 ( .A1(Data_Mem[1]), .A2(n19), .B1(Switches[1]), .B2(n21), .ZN(
        n29) );
  INV_X1 U57 ( .A(n30), .ZN(n58) );
  AOI22_X1 U58 ( .A1(Data_Mem[15]), .A2(n19), .B1(Switches[15]), .B2(n21), 
        .ZN(n30) );
  INV_X1 U59 ( .A(n31), .ZN(n59) );
  AOI22_X1 U60 ( .A1(Data_Mem[14]), .A2(n19), .B1(Switches[14]), .B2(n21), 
        .ZN(n31) );
  INV_X1 U61 ( .A(n32), .ZN(n60) );
  AOI22_X1 U62 ( .A1(Data_Mem[13]), .A2(n19), .B1(Switches[13]), .B2(n21), 
        .ZN(n32) );
  INV_X1 U63 ( .A(n33), .ZN(n61) );
  AOI22_X1 U64 ( .A1(Data_Mem[12]), .A2(n19), .B1(Switches[12]), .B2(n21), 
        .ZN(n33) );
  INV_X1 U65 ( .A(n34), .ZN(n62) );
  AOI22_X1 U66 ( .A1(Data_Mem[11]), .A2(n19), .B1(Switches[11]), .B2(n21), 
        .ZN(n34) );
  INV_X1 U67 ( .A(n35), .ZN(n63) );
  AOI22_X1 U68 ( .A1(Data_Mem[10]), .A2(n19), .B1(Switches[10]), .B2(n21), 
        .ZN(n35) );
  INV_X1 U70 ( .A(OE), .ZN(n36) );
  INV_X1 U71 ( .A(n37), .ZN(n74) );
  AOI22_X1 U72 ( .A1(Data_Mem[0]), .A2(n19), .B1(Switches[0]), .B2(n21), .ZN(
        n37) );
  NAND4_X1 U75 ( .A1(A[1]), .A2(A[15]), .A3(A[14]), .A4(A[13]), .ZN(n41) );
  NAND4_X1 U76 ( .A1(A[12]), .A2(A[11]), .A3(A[10]), .A4(A[0]), .ZN(n40) );
  NAND4_X1 U77 ( .A1(A[9]), .A2(A[8]), .A3(A[7]), .A4(A[6]), .ZN(n39) );
  NAND4_X1 U78 ( .A1(A[5]), .A2(A[4]), .A3(A[3]), .A4(A[2]), .ZN(n38) );
  TBUF_X2 \Data_Mem_tri[0]  ( .A(Data_CPU[0]), .EN(WE), .Z(Data_Mem[0]) );
  TBUF_X2 \Data_Mem_tri[1]  ( .A(Data_CPU[1]), .EN(WE), .Z(Data_Mem[1]) );
  TBUF_X2 \Data_Mem_tri[2]  ( .A(Data_CPU[2]), .EN(WE), .Z(Data_Mem[2]) );
  TBUF_X2 \Data_Mem_tri[3]  ( .A(Data_CPU[3]), .EN(WE), .Z(Data_Mem[3]) );
  TBUF_X2 \Data_Mem_tri[4]  ( .A(Data_CPU[4]), .EN(WE), .Z(Data_Mem[4]) );
  TBUF_X2 \Data_Mem_tri[5]  ( .A(Data_CPU[5]), .EN(WE), .Z(Data_Mem[5]) );
  TBUF_X2 \Data_Mem_tri[6]  ( .A(Data_CPU[6]), .EN(WE), .Z(Data_Mem[6]) );
  TBUF_X2 \Data_Mem_tri[7]  ( .A(Data_CPU[7]), .EN(WE), .Z(Data_Mem[7]) );
  TBUF_X2 \Data_Mem_tri[8]  ( .A(Data_CPU[8]), .EN(WE), .Z(Data_Mem[8]) );
  TBUF_X2 \Data_Mem_tri[9]  ( .A(Data_CPU[9]), .EN(WE), .Z(Data_Mem[9]) );
  TBUF_X2 \Data_Mem_tri[10]  ( .A(Data_CPU[10]), .EN(WE), .Z(Data_Mem[10]) );
  TBUF_X2 \Data_Mem_tri[11]  ( .A(Data_CPU[11]), .EN(WE), .Z(Data_Mem[11]) );
  TBUF_X2 \Data_Mem_tri[12]  ( .A(Data_CPU[12]), .EN(WE), .Z(Data_Mem[12]) );
  TBUF_X2 \Data_Mem_tri[13]  ( .A(Data_CPU[13]), .EN(WE), .Z(Data_Mem[13]) );
  TBUF_X2 \Data_Mem_tri[14]  ( .A(Data_CPU[14]), .EN(WE), .Z(Data_Mem[14]) );
  TBUF_X2 \Data_Mem_tri[15]  ( .A(Data_CPU[15]), .EN(WE), .Z(Data_Mem[15]) );
  TBUF_X2 \Data_CPU_signal_tri[0]  ( .A(n74), .EN(n65), .Z(Data_CPU[0]) );
  TBUF_X2 \Data_CPU_signal_tri[1]  ( .A(n73), .EN(n65), .Z(Data_CPU[1]) );
  TBUF_X2 \Data_CPU_signal_tri[2]  ( .A(n72), .EN(n65), .Z(Data_CPU[2]) );
  TBUF_X2 \Data_CPU_signal_tri[3]  ( .A(n71), .EN(n65), .Z(Data_CPU[3]) );
  TBUF_X2 \Data_CPU_signal_tri[4]  ( .A(n70), .EN(n65), .Z(Data_CPU[4]) );
  TBUF_X2 \Data_CPU_signal_tri[5]  ( .A(n69), .EN(n65), .Z(Data_CPU[5]) );
  TBUF_X2 \Data_CPU_signal_tri[6]  ( .A(n68), .EN(n65), .Z(Data_CPU[6]) );
  TBUF_X2 \Data_CPU_signal_tri[7]  ( .A(n67), .EN(n65), .Z(Data_CPU[7]) );
  TBUF_X2 \Data_CPU_signal_tri[8]  ( .A(n66), .EN(n65), .Z(Data_CPU[8]) );
  TBUF_X2 \Data_CPU_signal_tri[9]  ( .A(n64), .EN(n65), .Z(Data_CPU[9]) );
  TBUF_X2 \Data_CPU_signal_tri[10]  ( .A(n63), .EN(n65), .Z(Data_CPU[10]) );
  TBUF_X2 \Data_CPU_signal_tri[11]  ( .A(n62), .EN(n65), .Z(Data_CPU[11]) );
  TBUF_X2 \Data_CPU_signal_tri[12]  ( .A(n61), .EN(n65), .Z(Data_CPU[12]) );
  TBUF_X2 \Data_CPU_signal_tri[13]  ( .A(n60), .EN(n65), .Z(Data_CPU[13]) );
  TBUF_X2 \Data_CPU_signal_tri[14]  ( .A(n59), .EN(n65), .Z(Data_CPU[14]) );
  TBUF_X2 \Data_CPU_signal_tri[15]  ( .A(n58), .EN(n65), .Z(Data_CPU[15]) );
  INV_X4 U37 ( .A(n1), .ZN(n2) );
  NOR2_X2 U38 ( .A1(n19), .A2(WE), .ZN(n1) );
  NAND2_X2 U69 ( .A1(n36), .A2(WE), .ZN(n65) );
  INV_X4 U73 ( .A(n21), .ZN(n19) );
  NOR4_X2 U74 ( .A1(n38), .A2(n39), .A3(n40), .A4(n41), .ZN(n21) );
endmodule


module HexDriver_0 ( In0, Out0 );
  input [3:0] In0;
  output [6:0] Out0;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25;

  OAI211_X2 U3 ( .C1(In0[1]), .C2(n12), .A(n13), .B(n14), .ZN(Out0[6]) );
  OAI211_X2 U4 ( .C1(n10), .C2(n12), .A(n14), .B(n4), .ZN(Out0[5]) );
  OAI21_X2 U6 ( .B1(n10), .B2(n5), .A(n7), .ZN(n15) );
  OAI21_X2 U9 ( .B1(In0[0]), .B2(n9), .A(n16), .ZN(n18) );
  OAI211_X2 U10 ( .C1(n9), .C2(n3), .A(n20), .B(n13), .ZN(Out0[2]) );
  OR3_X2 U11 ( .A1(n10), .A2(In0[0]), .A3(n12), .ZN(n20) );
  OAI211_X2 U12 ( .C1(n21), .C2(n11), .A(n22), .B(n13), .ZN(Out0[1]) );
  NAND4_X2 U13 ( .A1(In0[3]), .A2(In0[2]), .A3(n11), .A4(n10), .ZN(n13) );
  OAI221_X2 U16 ( .B1(n16), .B2(n3), .C1(In0[0]), .C2(n2), .A(n4), .ZN(Out0[0]) );
  NAND2_X2 U18 ( .A1(n9), .A2(n5), .ZN(n12) );
  NAND2_X2 U21 ( .A1(In0[0]), .A2(n9), .ZN(n16) );
  INV_X4 U22 ( .A(n19), .ZN(n1) );
  INV_X4 U23 ( .A(n24), .ZN(n2) );
  INV_X4 U24 ( .A(n23), .ZN(n3) );
  INV_X4 U25 ( .A(n25), .ZN(n4) );
  INV_X4 U26 ( .A(In0[3]), .ZN(n5) );
  INV_X4 U27 ( .A(n18), .ZN(n6) );
  INV_X4 U28 ( .A(n16), .ZN(n7) );
  INV_X4 U29 ( .A(n17), .ZN(n8) );
  INV_X4 U30 ( .A(In0[2]), .ZN(n9) );
  INV_X4 U31 ( .A(In0[1]), .ZN(n10) );
  INV_X4 U32 ( .A(In0[0]), .ZN(n11) );
  OAI33_X1 U33 ( .A1(n18), .A2(n19), .A3(n10), .B1(n1), .B2(In0[1]), .B3(n6), 
        .ZN(Out0[3]) );
  OAI33_X1 U34 ( .A1(n12), .A2(In0[1]), .A3(n11), .B1(n5), .B2(In0[1]), .B3(n8), .ZN(n25) );
  NOR2_X2 U5 ( .A1(n9), .A2(n11), .ZN(n17) );
  NOR2_X2 U7 ( .A1(n5), .A2(n10), .ZN(n23) );
  NAND3_X2 U8 ( .A1(n14), .A2(n2), .A3(n15), .ZN(Out0[4]) );
  NOR2_X2 U14 ( .A1(In0[3]), .A2(n17), .ZN(n19) );
  NOR3_X2 U15 ( .A1(In0[1]), .A2(In0[3]), .A3(n9), .ZN(n24) );
  NAND3_X2 U17 ( .A1(n17), .A2(n5), .A3(In0[1]), .ZN(n14) );
  NOR2_X2 U19 ( .A1(n23), .A2(n24), .ZN(n21) );
  NAND3_X2 U20 ( .A1(In0[2]), .A2(n11), .A3(In0[1]), .ZN(n22) );
endmodule


module HexDriver_1 ( In0, Out0 );
  input [3:0] In0;
  output [6:0] Out0;
  wire   n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39,
         n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50;

  OAI211_X2 U3 ( .C1(In0[1]), .C2(n39), .A(n38), .B(n37), .ZN(Out0[6]) );
  OAI211_X2 U4 ( .C1(n41), .C2(n39), .A(n37), .B(n47), .ZN(Out0[5]) );
  OAI21_X2 U6 ( .B1(n41), .B2(n46), .A(n44), .ZN(n36) );
  OAI21_X2 U9 ( .B1(In0[0]), .B2(n42), .A(n35), .ZN(n33) );
  OAI211_X2 U10 ( .C1(n42), .C2(n48), .A(n31), .B(n38), .ZN(Out0[2]) );
  OR3_X2 U11 ( .A1(n41), .A2(In0[0]), .A3(n39), .ZN(n31) );
  OAI211_X2 U12 ( .C1(n30), .C2(n40), .A(n29), .B(n38), .ZN(Out0[1]) );
  NAND4_X2 U13 ( .A1(In0[3]), .A2(In0[2]), .A3(n40), .A4(n41), .ZN(n38) );
  OAI221_X2 U16 ( .B1(n35), .B2(n48), .C1(In0[0]), .C2(n49), .A(n47), .ZN(
        Out0[0]) );
  NAND2_X2 U18 ( .A1(n42), .A2(n46), .ZN(n39) );
  NAND2_X2 U21 ( .A1(In0[0]), .A2(n42), .ZN(n35) );
  INV_X4 U22 ( .A(n32), .ZN(n50) );
  INV_X4 U23 ( .A(n27), .ZN(n49) );
  INV_X4 U24 ( .A(n28), .ZN(n48) );
  INV_X4 U25 ( .A(n26), .ZN(n47) );
  INV_X4 U26 ( .A(In0[3]), .ZN(n46) );
  INV_X4 U27 ( .A(n33), .ZN(n45) );
  INV_X4 U28 ( .A(n35), .ZN(n44) );
  INV_X4 U29 ( .A(n34), .ZN(n43) );
  INV_X4 U30 ( .A(In0[2]), .ZN(n42) );
  INV_X4 U31 ( .A(In0[1]), .ZN(n41) );
  INV_X4 U32 ( .A(In0[0]), .ZN(n40) );
  OAI33_X1 U33 ( .A1(n33), .A2(n32), .A3(n41), .B1(n50), .B2(In0[1]), .B3(n45), 
        .ZN(Out0[3]) );
  OAI33_X1 U34 ( .A1(n39), .A2(In0[1]), .A3(n40), .B1(n46), .B2(In0[1]), .B3(
        n43), .ZN(n26) );
  NOR2_X2 U5 ( .A1(n42), .A2(n40), .ZN(n34) );
  NOR2_X2 U7 ( .A1(n46), .A2(n41), .ZN(n28) );
  NAND3_X2 U8 ( .A1(n37), .A2(n49), .A3(n36), .ZN(Out0[4]) );
  NOR2_X2 U14 ( .A1(In0[3]), .A2(n34), .ZN(n32) );
  NOR3_X2 U15 ( .A1(In0[1]), .A2(In0[3]), .A3(n42), .ZN(n27) );
  NAND3_X2 U17 ( .A1(n34), .A2(n46), .A3(In0[1]), .ZN(n37) );
  NOR2_X2 U19 ( .A1(n28), .A2(n27), .ZN(n30) );
  NAND3_X2 U20 ( .A1(In0[2]), .A2(n40), .A3(In0[1]), .ZN(n29) );
endmodule


module HexDriver_2 ( In0, Out0 );
  input [3:0] In0;
  output [6:0] Out0;
  wire   n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39,
         n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50;

  OAI211_X2 U3 ( .C1(In0[1]), .C2(n39), .A(n38), .B(n37), .ZN(Out0[6]) );
  OAI211_X2 U4 ( .C1(n41), .C2(n39), .A(n37), .B(n47), .ZN(Out0[5]) );
  OAI21_X2 U6 ( .B1(n41), .B2(n46), .A(n44), .ZN(n36) );
  OAI21_X2 U9 ( .B1(In0[0]), .B2(n42), .A(n35), .ZN(n33) );
  OAI211_X2 U10 ( .C1(n42), .C2(n48), .A(n31), .B(n38), .ZN(Out0[2]) );
  OR3_X2 U11 ( .A1(n41), .A2(In0[0]), .A3(n39), .ZN(n31) );
  OAI211_X2 U12 ( .C1(n30), .C2(n40), .A(n29), .B(n38), .ZN(Out0[1]) );
  NAND4_X2 U13 ( .A1(In0[3]), .A2(In0[2]), .A3(n40), .A4(n41), .ZN(n38) );
  OAI221_X2 U16 ( .B1(n35), .B2(n48), .C1(In0[0]), .C2(n49), .A(n47), .ZN(
        Out0[0]) );
  NAND2_X2 U18 ( .A1(n42), .A2(n46), .ZN(n39) );
  NAND2_X2 U21 ( .A1(In0[0]), .A2(n42), .ZN(n35) );
  INV_X4 U22 ( .A(n32), .ZN(n50) );
  INV_X4 U23 ( .A(n27), .ZN(n49) );
  INV_X4 U24 ( .A(n28), .ZN(n48) );
  INV_X4 U25 ( .A(n26), .ZN(n47) );
  INV_X4 U26 ( .A(In0[3]), .ZN(n46) );
  INV_X4 U27 ( .A(n33), .ZN(n45) );
  INV_X4 U28 ( .A(n35), .ZN(n44) );
  INV_X4 U29 ( .A(n34), .ZN(n43) );
  INV_X4 U30 ( .A(In0[2]), .ZN(n42) );
  INV_X4 U31 ( .A(In0[1]), .ZN(n41) );
  INV_X4 U32 ( .A(In0[0]), .ZN(n40) );
  OAI33_X1 U33 ( .A1(n33), .A2(n32), .A3(n41), .B1(n50), .B2(In0[1]), .B3(n45), 
        .ZN(Out0[3]) );
  OAI33_X1 U34 ( .A1(n39), .A2(In0[1]), .A3(n40), .B1(n46), .B2(In0[1]), .B3(
        n43), .ZN(n26) );
  NOR2_X2 U5 ( .A1(n42), .A2(n40), .ZN(n34) );
  NOR2_X2 U7 ( .A1(n46), .A2(n41), .ZN(n28) );
  NAND3_X2 U8 ( .A1(n37), .A2(n49), .A3(n36), .ZN(Out0[4]) );
  NOR2_X2 U14 ( .A1(In0[3]), .A2(n34), .ZN(n32) );
  NOR3_X2 U15 ( .A1(In0[1]), .A2(In0[3]), .A3(n42), .ZN(n27) );
  NAND3_X2 U17 ( .A1(n34), .A2(n46), .A3(In0[1]), .ZN(n37) );
  NOR2_X2 U19 ( .A1(n28), .A2(n27), .ZN(n30) );
  NAND3_X2 U20 ( .A1(In0[2]), .A2(n40), .A3(In0[1]), .ZN(n29) );
endmodule


module HexDriver_3 ( In0, Out0 );
  input [3:0] In0;
  output [6:0] Out0;
  wire   n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39,
         n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50;

  OAI211_X2 U3 ( .C1(In0[1]), .C2(n39), .A(n38), .B(n37), .ZN(Out0[6]) );
  OAI211_X2 U4 ( .C1(n41), .C2(n39), .A(n37), .B(n47), .ZN(Out0[5]) );
  OAI21_X2 U6 ( .B1(n41), .B2(n46), .A(n44), .ZN(n36) );
  OAI21_X2 U9 ( .B1(In0[0]), .B2(n42), .A(n35), .ZN(n33) );
  OAI211_X2 U10 ( .C1(n42), .C2(n48), .A(n31), .B(n38), .ZN(Out0[2]) );
  OR3_X2 U11 ( .A1(n41), .A2(In0[0]), .A3(n39), .ZN(n31) );
  OAI211_X2 U12 ( .C1(n30), .C2(n40), .A(n29), .B(n38), .ZN(Out0[1]) );
  NAND4_X2 U13 ( .A1(In0[3]), .A2(In0[2]), .A3(n40), .A4(n41), .ZN(n38) );
  OAI221_X2 U16 ( .B1(n35), .B2(n48), .C1(In0[0]), .C2(n49), .A(n47), .ZN(
        Out0[0]) );
  NAND2_X2 U18 ( .A1(n42), .A2(n46), .ZN(n39) );
  NAND2_X2 U21 ( .A1(In0[0]), .A2(n42), .ZN(n35) );
  INV_X4 U22 ( .A(n32), .ZN(n50) );
  INV_X4 U23 ( .A(n27), .ZN(n49) );
  INV_X4 U24 ( .A(n28), .ZN(n48) );
  INV_X4 U25 ( .A(n26), .ZN(n47) );
  INV_X4 U26 ( .A(In0[3]), .ZN(n46) );
  INV_X4 U27 ( .A(n33), .ZN(n45) );
  INV_X4 U28 ( .A(n35), .ZN(n44) );
  INV_X4 U29 ( .A(n34), .ZN(n43) );
  INV_X4 U30 ( .A(In0[2]), .ZN(n42) );
  INV_X4 U31 ( .A(In0[1]), .ZN(n41) );
  INV_X4 U32 ( .A(In0[0]), .ZN(n40) );
  OAI33_X1 U33 ( .A1(n33), .A2(n32), .A3(n41), .B1(n50), .B2(In0[1]), .B3(n45), 
        .ZN(Out0[3]) );
  OAI33_X1 U34 ( .A1(n39), .A2(In0[1]), .A3(n40), .B1(n46), .B2(In0[1]), .B3(
        n43), .ZN(n26) );
  NOR2_X2 U5 ( .A1(n42), .A2(n40), .ZN(n34) );
  NOR2_X2 U7 ( .A1(n46), .A2(n41), .ZN(n28) );
  NAND3_X2 U8 ( .A1(n37), .A2(n49), .A3(n36), .ZN(Out0[4]) );
  NOR2_X2 U14 ( .A1(In0[3]), .A2(n34), .ZN(n32) );
  NOR3_X2 U15 ( .A1(In0[1]), .A2(In0[3]), .A3(n42), .ZN(n27) );
  NAND3_X2 U17 ( .A1(n34), .A2(n46), .A3(In0[1]), .ZN(n37) );
  NOR2_X2 U19 ( .A1(n28), .A2(n27), .ZN(n30) );
  NAND3_X2 U20 ( .A1(In0[2]), .A2(n40), .A3(In0[1]), .ZN(n29) );
endmodule


module SLC ( Clk, Reset, Run, Continue, ContinueIR, S, CE, UB, LB, OE, WE, A, 
        LED, DIS3, DIS2, DIS1, DIS0, MEMdata );
  input [15:0] S;
  output [17:0] A;
  output [11:0] LED;
  output [6:0] DIS3;
  output [6:0] DIS2;
  output [6:0] DIS1;
  output [6:0] DIS0;
  inout [15:0] MEMdata;
  input Clk, Reset, Run, Continue, ContinueIR;
  output CE, UB, LB, OE, WE;
  wire   _4_net_;
  wire   [3:0] HEX3;
  wire   [3:0] HEX2;
  wire   [3:0] HEX1;
  wire   [3:0] HEX0;
  tri   Clk;
  tri   CE;
  tri   UB;
  tri   LB;
  tri   OE;
  tri   WE;
  tri   [17:0] A;
  tri   [11:0] LED;
  tri   [15:0] MEMdata;
  tri   _0_net_;
  tri   _1_net_;
  tri   _2_net_;
  tri   _3_net_;
  tri   \CPUdata[15] ;
  tri   \CPUdata[14] ;
  tri   \CPUdata[13] ;
  tri   \CPUdata[12] ;
  tri   \CPUdata[11] ;
  tri   \CPUdata[10] ;
  tri   \CPUdata[9] ;
  tri   \CPUdata[8] ;
  tri   \CPUdata[7] ;
  tri   \CPUdata[6] ;
  tri   \CPUdata[5] ;
  tri   \CPUdata[4] ;
  tri   \CPUdata[3] ;
  tri   \CPUdata[2] ;
  tri   \CPUdata[1] ;
  tri   \CPUdata[0] ;

  CPU cpu ( .Reset(_0_net_), .Run(_1_net_), .Continue(_2_net_), .ContinueIR(
        _3_net_), .Data({\CPUdata[15] , \CPUdata[14] , \CPUdata[13] , 
        \CPUdata[12] , \CPUdata[11] , \CPUdata[10] , \CPUdata[9] , 
        \CPUdata[8] , \CPUdata[7] , \CPUdata[6] , \CPUdata[5] , \CPUdata[4] , 
        \CPUdata[3] , \CPUdata[2] , \CPUdata[1] , \CPUdata[0] }), .ADDR(A), 
        .LED(LED), .Mem_WE(WE), .Mem_OE(OE), .Mem_LB(LB), .Mem_UB(UB), 
        .Mem_CE(CE), .Clk(Clk) );
  Mem2IO mem2io ( .Clk(Clk), .Reset(_4_net_), .A(A), .CE(CE), .UB(UB), .LB(LB), 
        .OE(OE), .WE(WE), .Switches(S), .Data_CPU({\CPUdata[15] , 
        \CPUdata[14] , \CPUdata[13] , \CPUdata[12] , \CPUdata[11] , 
        \CPUdata[10] , \CPUdata[9] , \CPUdata[8] , \CPUdata[7] , \CPUdata[6] , 
        \CPUdata[5] , \CPUdata[4] , \CPUdata[3] , \CPUdata[2] , \CPUdata[1] , 
        \CPUdata[0] }), .Data_Mem(MEMdata), .HEX0(HEX0), .HEX1(HEX1), .HEX2(
        HEX2), .HEX3(HEX3) );
  HexDriver_0 H3 ( .In0(HEX3), .Out0(DIS3) );
  HexDriver_3 H2 ( .In0(HEX2), .Out0(DIS2) );
  HexDriver_2 H1 ( .In0(HEX1), .Out0(DIS1) );
  HexDriver_1 H0 ( .In0(HEX0), .Out0(DIS0) );
  INV_X4 I_3 ( .A(Reset), .ZN(_0_net_) );
  INV_X4 I_2 ( .A(Run), .ZN(_1_net_) );
  INV_X4 I_1 ( .A(Continue), .ZN(_2_net_) );
  INV_X4 I_0 ( .A(ContinueIR), .ZN(_3_net_) );
  INV_X4 U1 ( .A(Reset), .ZN(_4_net_) );
endmodule


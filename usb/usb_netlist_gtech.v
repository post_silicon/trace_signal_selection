
module usbf_utmi_ls ( clk, rst, resume_req, rx_active, tx_ready, drive_k, 
        XcvSelect, TermSel, SuspendM, LineState, OpMode, usb_vbus, mode_hs, 
        usb_reset, usb_suspend, usb_attached, suspend_clr );
  input [1:0] LineState;
  output [1:0] OpMode;
  input clk, rst, resume_req, rx_active, tx_ready, usb_vbus;
  output drive_k, XcvSelect, TermSel, SuspendM, mode_hs, usb_reset,
         usb_suspend, usb_attached, suspend_clr;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15,
         N16, N17, N18, N19, N20, N21, N22, N23, N24, N25, N26, N27, N28, N29,
         N30, N31, N32, N33, N34, N35, N36, N37, drive_k_d, resume_req_s,
         resume_req_s1, mode_set_fs, mode_set_hs, N38, N39, N40, N41, N42,
         suspend_set, N43, N44, N45, N46, N47, attached_clr, attached_set, N48,
         N49, N50, N51, N52, fs_term_off, fs_term_on, N53, N54, N55, N56, N57,
         xcv_set_fs, xcv_set_hs, N58, N59, N60, N61, N62, bit_stuff_off,
         bit_stuff_on, N63, N64, N65, N66, N67, usb_reset_d, N68, ls_idle,
         ls_idle_r, idle_long_set, idle_long_clr, N69, N70, N71, N72,
         idle_long, N73, N74, N75, N76, N77, N78, ls_k_r, ls_j_r, ls_se0_r,
         k_long, j_long, se0_long, idle_cnt_clr, ps_cnt_clr, N79, N80, N81,
         N82, N83, N84, N85, N86, N87, N88, idle_cnt1_clr, N89, T1_gt_5_0_mS,
         N90, N91, N92, N93, N94, N95, N96, N97, N98, N99, N100, N101, N102,
         N103, N104, N105, N106, N107, N108, N109, N110, N111, N112, N113,
         T1_gt_2_5_uS, N114, N115, T1_st_3_0_mS, N116, N117, T1_gt_3_0_mS,
         N118, N119, me_cnt_clr, me_ps_2_5_us, N120, N121, N122, N123, N124,
         N125, N126, N127, N128, N129, N130, N131, N132, N133, N134, N135,
         N136, N137, me_ps2_0_5_ms, N138, N139, N140, N141, N142, N143, N144,
         N145, N146, N147, N148, N149, N150, N151, N152, N153, N154, N155,
         N156, N157, N158, N159, N160, N161, me_cnt_100_ms, N162, N163, N164,
         N165, N166, N167, N168, N169, N170, N171, N172, N173, N174, N175,
         N176, N177, N178, N179, N180, N181, N182, N183, N184, N185, N186,
         T2_gt_100_uS, N187, N188, T2_wakeup, N189, N190, T2_gt_1_0_mS, N191,
         N192, T2_gt_1_2_mS, N193, chirp_cnt_clr, chirp_cnt_inc, N194, N195,
         N196, N197, N198, N199, N200, N201, N202, N203, N204, N205,
         chirp_cnt_is_6, N206, N207, N208, N209, N210, N211, N212, N213, N214,
         N215, N216, N217, N218, N219, N220, N221, N222, N223, N224, N225,
         N226, N227, N228, N229, N230, N231, N232, N233, N234, N235, N236,
         N237, N238, N239, N240, N241, N242, N243, N244, N245, N246, N247,
         N248, N249, N250, N251, N252, N253, N254, N255, N256, N257, N258,
         N259, N260, N261, N262, N263, N264, N265, N266, N267, N268, N269,
         N270, N271, N272, N273, N274, N275, N276, N277, N278, N279, N280,
         N281, N282, N283, N284, N285, N286, N287, N288, N289, N290, N291,
         N292, N293, N294, N295, N296, N297, N298, N299, N300, N301, N302,
         N303, N304, N305, N306, N307, N308, N309, N310, N311, N312, N313,
         N314, N315, N316, N317, N318, N319, N320, N321, N322, N323, N324,
         N325, N326, N327, N328, N329, N330, N331, N332, N333, N334, N335,
         N336, N337, N338, N339, N340, N341, N342, N343, N344, N345, N346,
         N347, N348, N349, N350, N351, N352, N353, N354, N355, N356, N357,
         N358, N359, N360, N361, N362, N363, N364, N365, N366, N367, N368,
         N369, N370, N371, N372, N373, N374, N375, N376, N377, N378, N379,
         N380, N381, N382, N383, N384, N385, N386, N387, N388, N389, N390,
         N391, N392, N393, N394, N395, N396, N397, N398, N399, N400, N401,
         N402, N403, N404, N405, N406, N407, N408, N409, N410, N411, N412,
         N413, N414, N415, N416, N417, N418, N419, N420, N421, N422, N423,
         N424, N425, N426, N427, N428, N429, N430, N431, N432, N433, N434,
         N435, N436, N437, N438, N439, N440, N441, N442, N443, N444, N445,
         N446, N447, N448, N449, N450, N451, N452, N453, N454, N455, N456,
         N457, N458, N459, N460, N461, N462, N463, N464, N465, N466, N467,
         N468, N469, N470, N471, N472, N473, N474, N475, N476, N477, N478,
         N479, N480, N481, N482, N483, N484, N485, N486, N487, N488, N489,
         N490, N491, N492, N493, N494, N495, N496, N497, N498, N499, N500,
         N501, N502, N503, N504, N505, N506, N507, N508, N509, N510, N511,
         N512, N513, N514, N515, N516, N517, N518, N519, N520, N521, N522,
         N523, N524, N525, N526, N527, N528, N529, N530, N531, N532, N533,
         N534, N535, N536, N537, N538, N539, N540, N541, N542, N543, N544,
         N545, N546, N547, N548, N549, N550, N551, N552, N553, N554, N555,
         N556, N557, N558, N559, N560, N561, N562, N563, N564, N565, N566,
         N567, N568, N569, N570, N571, N572, N573, N574, N575, N576, N577,
         N578, N579, N580, N581, N582, N583, N584, N585, N586, N587, N588,
         N589, N590, N591, N592, N593, N594, N595, N596, N597, N598, N599,
         N600, N601, N602, N603, N604, N605, N606, N607, N608, N609, N610,
         N611, N612, N613, N614, N615, N616, N617, N618, N619, N620, N621,
         N622, N623, N624, N625, N626, N627, N628, N629, N630, N631, N632,
         N633, N634, N635, N636, N637, N638, N639, N640, N641, N642, N643,
         N644, N645, N646, N647, N648, N649, N650, N651, N652, N653, N654,
         N655, N656, N657, N658, N659, N660, N661, N662;
  wire   [1:0] line_state_r;
  wire   [3:0] ps_cnt;
  wire   [7:0] idle_cnt1;
  wire   [7:0] idle_cnt1_next;
  wire   [7:0] me_ps;
  wire   [7:0] me_ps2;
  wire   [7:0] me_cnt;
  wire   [2:0] chirp_cnt;
  wire   [14:0] state;
  wire   [14:0] next_state;

  \**SEQGEN**  drive_k_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        drive_k_d), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        drive_k), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  resume_req_s1_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        resume_req), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        resume_req_s1), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  resume_req_s_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        resume_req_s1), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        resume_req_s), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  mode_hs_reg ( .clear(1'b0), .preset(1'b0), .next_state(N41), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(mode_hs), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N40) );
  \**SEQGEN**  usb_suspend_reg ( .clear(1'b0), .preset(1'b0), .next_state(N46), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(usb_suspend), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N45) );
  \**SEQGEN**  usb_attached_reg ( .clear(1'b0), .preset(1'b0), .next_state(N51), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(usb_attached), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N50) );
  \**SEQGEN**  TermSel_reg ( .clear(1'b0), .preset(1'b0), .next_state(N56), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(TermSel), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N55) );
  \**SEQGEN**  XcvSelect_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        xcv_set_fs), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        XcvSelect), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N60) );
  \**SEQGEN**  \OpMode_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        bit_stuff_off), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        OpMode[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N65) );
  \**SEQGEN**  \OpMode_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(OpMode[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N65) );
  \**SEQGEN**  usb_reset_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        usb_reset_d), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        usb_reset), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \line_state_r_reg[1]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(LineState[1]), .clocked_on(clk), .data_in(1'b0), .enable(
        1'b0), .Q(line_state_r[1]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \line_state_r_reg[0]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(LineState[0]), .clocked_on(clk), .data_in(1'b0), .enable(
        1'b0), .Q(line_state_r[0]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  ls_idle_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        ls_idle), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        ls_idle_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  idle_long_reg ( .clear(1'b0), .preset(1'b0), .next_state(N74), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idle_long), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N73) );
  \**SEQGEN**  ls_k_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(N643), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(ls_k_r), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  ls_j_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(N646), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(ls_j_r), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  ls_se0_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(N648), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(ls_se0_r), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \ps_cnt_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N88), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(ps_cnt[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \ps_cnt_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N87), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(ps_cnt[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \ps_cnt_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N86), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(ps_cnt[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \ps_cnt_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N85), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(ps_cnt[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  ps_cnt_clr_reg ( .clear(1'b0), .preset(1'b0), .next_state(N588), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(ps_cnt_clr), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idle_cnt1_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N101), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        idle_cnt1[7]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N93) );
  \**SEQGEN**  \idle_cnt1_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N100), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        idle_cnt1[6]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N93) );
  \**SEQGEN**  \idle_cnt1_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N99), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idle_cnt1[5]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        N93) );
  \**SEQGEN**  \idle_cnt1_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N98), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idle_cnt1[4]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        N93) );
  \**SEQGEN**  \idle_cnt1_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N97), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idle_cnt1[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        N93) );
  \**SEQGEN**  \idle_cnt1_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N96), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idle_cnt1[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        N93) );
  \**SEQGEN**  \idle_cnt1_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N95), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idle_cnt1[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        N93) );
  \**SEQGEN**  \idle_cnt1_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N94), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idle_cnt1[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        N93) );
  \**SEQGEN**  \idle_cnt1_next_reg[7]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N111), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(idle_cnt1_next[7]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \idle_cnt1_next_reg[6]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N110), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(idle_cnt1_next[6]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \idle_cnt1_next_reg[5]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N109), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(idle_cnt1_next[5]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \idle_cnt1_next_reg[4]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N108), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(idle_cnt1_next[4]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \idle_cnt1_next_reg[3]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N107), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(idle_cnt1_next[3]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \idle_cnt1_next_reg[2]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N106), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(idle_cnt1_next[2]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \idle_cnt1_next_reg[1]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N105), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(idle_cnt1_next[1]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \idle_cnt1_next_reg[0]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N104), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(idle_cnt1_next[0]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  idle_cnt1_clr_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N602), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        idle_cnt1_clr), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  GT_UNS_OP gt_1352 ( .A(idle_cnt1), .B({1'b1, 1'b0, 1'b1, 1'b0}), .Z(N112) );
  \**SEQGEN**  T1_gt_2_5_uS_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N113), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        T1_gt_2_5_uS), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  LT_UNS_OP lt_1355 ( .A(idle_cnt1), .B({1'b1, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0}), 
        .Z(N114) );
  \**SEQGEN**  T1_st_3_0_mS_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N115), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        T1_st_3_0_mS), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  GT_UNS_OP gt_1358 ( .A(idle_cnt1), .B({1'b1, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0}), 
        .Z(N116) );
  \**SEQGEN**  T1_gt_3_0_mS_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N117), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        T1_gt_3_0_mS), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  GT_UNS_OP gt_1364 ( .A(idle_cnt1), .B({1'b1, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 
        1'b0}), .Z(N118) );
  \**SEQGEN**  T1_gt_5_0_mS_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N119), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        T1_gt_5_0_mS), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \me_ps_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N137), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_ps[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \me_ps_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N136), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_ps[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \me_ps_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N135), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_ps[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \me_ps_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N134), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_ps[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \me_ps_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N133), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_ps[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \me_ps_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N132), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_ps[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \me_ps_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N131), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_ps[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \me_ps_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N130), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_ps[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  me_ps_2_5_us_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N613), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        me_ps_2_5_us), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \me_ps2_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N158), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_ps2[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N150) );
  \**SEQGEN**  \me_ps2_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N157), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_ps2[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N150) );
  \**SEQGEN**  \me_ps2_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N156), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_ps2[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N150) );
  \**SEQGEN**  \me_ps2_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N155), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_ps2[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N150) );
  \**SEQGEN**  \me_ps2_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N154), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_ps2[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N150) );
  \**SEQGEN**  \me_ps2_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N153), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_ps2[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N150) );
  \**SEQGEN**  \me_ps2_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N152), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_ps2[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N150) );
  \**SEQGEN**  \me_ps2_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N151), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_ps2[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N150) );
  \**SEQGEN**  me_ps2_0_5_ms_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N161), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        me_ps2_0_5_ms), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \me_cnt_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N182), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_cnt[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N174) );
  \**SEQGEN**  \me_cnt_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N181), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_cnt[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N174) );
  \**SEQGEN**  \me_cnt_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N180), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_cnt[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N174) );
  \**SEQGEN**  \me_cnt_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N179), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_cnt[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N174) );
  \**SEQGEN**  \me_cnt_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N178), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_cnt[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N174) );
  \**SEQGEN**  \me_cnt_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N177), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_cnt[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N174) );
  \**SEQGEN**  \me_cnt_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N176), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_cnt[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N174) );
  \**SEQGEN**  \me_cnt_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N175), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(me_cnt[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N174) );
  GT_UNS_OP gt_1393 ( .A(me_ps2), .B({1'b1, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0}), 
        .Z(N185) );
  \**SEQGEN**  T2_gt_100_uS_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N186), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        T2_gt_100_uS), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  GT_UNS_OP gt_1396 ( .A(me_cnt), .B({1'b1, 1'b0, 1'b1, 1'b0}), .Z(N187) );
  \**SEQGEN**  T2_wakeup_reg ( .clear(1'b0), .preset(1'b0), .next_state(N188), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(T2_wakeup), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  GT_UNS_OP gt_1399 ( .A(me_cnt), .B({1'b1, 1'b0}), .Z(N189) );
  \**SEQGEN**  T2_gt_1_0_mS_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N190), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        T2_gt_1_0_mS), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  GT_UNS_OP gt_1402 ( .A(me_cnt), .B({1'b1, 1'b0}), .Z(N191) );
  \**SEQGEN**  T2_gt_1_2_mS_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N192), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        T2_gt_1_2_mS), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  me_cnt_100_ms_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N193), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        me_cnt_100_ms), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \chirp_cnt_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N203), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        chirp_cnt[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N200) );
  \**SEQGEN**  \chirp_cnt_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N202), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        chirp_cnt[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N200) );
  \**SEQGEN**  \chirp_cnt_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N201), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        chirp_cnt[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N200) );
  \**SEQGEN**  chirp_cnt_is_6_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N618), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        chirp_cnt_is_6), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \state_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(N222), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[14]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N578) );
  \**SEQGEN**  \state_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(N221), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[13]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N578) );
  \**SEQGEN**  \state_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(N220), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[12]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N578) );
  \**SEQGEN**  \state_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(N219), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[11]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N578) );
  \**SEQGEN**  \state_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(N218), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[10]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N578) );
  \**SEQGEN**  \state_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(N217), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N578) );
  \**SEQGEN**  \state_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(N216), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N578) );
  \**SEQGEN**  \state_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N215), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N578) );
  \**SEQGEN**  \state_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N214), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N578) );
  \**SEQGEN**  \state_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N213), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N578) );
  \**SEQGEN**  \state_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N212), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N578) );
  \**SEQGEN**  \state_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N211), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N578) );
  \**SEQGEN**  \state_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N210), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N578) );
  \**SEQGEN**  \state_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N209), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N578) );
  \**SEQGEN**  \state_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N208), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N578) );
  GTECH_OR2 C295 ( .A(state[14]), .B(state[13]), .Z(N225) );
  GTECH_OR2 C296 ( .A(state[12]), .B(state[11]), .Z(N226) );
  GTECH_OR2 C297 ( .A(state[10]), .B(state[9]), .Z(N227) );
  GTECH_OR2 C298 ( .A(state[8]), .B(state[7]), .Z(N228) );
  GTECH_OR2 C299 ( .A(state[6]), .B(state[5]), .Z(N229) );
  GTECH_OR2 C300 ( .A(state[4]), .B(state[3]), .Z(N230) );
  GTECH_OR2 C301 ( .A(state[2]), .B(state[1]), .Z(N231) );
  GTECH_OR2 C302 ( .A(N225), .B(N226), .Z(N232) );
  GTECH_OR2 C303 ( .A(N227), .B(N228), .Z(N233) );
  GTECH_OR2 C304 ( .A(N229), .B(N230), .Z(N234) );
  GTECH_OR2 C305 ( .A(N231), .B(N224), .Z(N235) );
  GTECH_OR2 C306 ( .A(N232), .B(N233), .Z(N236) );
  GTECH_OR2 C307 ( .A(N234), .B(N235), .Z(N237) );
  GTECH_OR2 C308 ( .A(N236), .B(N237), .Z(N238) );
  GTECH_OR2 C311 ( .A(state[14]), .B(state[13]), .Z(N241) );
  GTECH_OR2 C312 ( .A(state[12]), .B(state[11]), .Z(N242) );
  GTECH_OR2 C313 ( .A(state[10]), .B(state[9]), .Z(N243) );
  GTECH_OR2 C314 ( .A(state[8]), .B(state[7]), .Z(N244) );
  GTECH_OR2 C315 ( .A(state[6]), .B(state[5]), .Z(N245) );
  GTECH_OR2 C316 ( .A(state[4]), .B(state[3]), .Z(N246) );
  GTECH_OR2 C317 ( .A(state[2]), .B(N240), .Z(N247) );
  GTECH_OR2 C318 ( .A(N241), .B(N242), .Z(N248) );
  GTECH_OR2 C319 ( .A(N243), .B(N244), .Z(N249) );
  GTECH_OR2 C320 ( .A(N245), .B(N246), .Z(N250) );
  GTECH_OR2 C321 ( .A(N247), .B(state[0]), .Z(N251) );
  GTECH_OR2 C322 ( .A(N248), .B(N249), .Z(N252) );
  GTECH_OR2 C323 ( .A(N250), .B(N251), .Z(N253) );
  GTECH_OR2 C324 ( .A(N252), .B(N253), .Z(N254) );
  GTECH_OR2 C327 ( .A(state[14]), .B(state[13]), .Z(N257) );
  GTECH_OR2 C328 ( .A(state[12]), .B(state[11]), .Z(N258) );
  GTECH_OR2 C329 ( .A(state[10]), .B(state[9]), .Z(N259) );
  GTECH_OR2 C330 ( .A(state[8]), .B(state[7]), .Z(N260) );
  GTECH_OR2 C331 ( .A(state[6]), .B(state[5]), .Z(N261) );
  GTECH_OR2 C332 ( .A(state[4]), .B(state[3]), .Z(N262) );
  GTECH_OR2 C333 ( .A(N256), .B(state[1]), .Z(N263) );
  GTECH_OR2 C334 ( .A(N257), .B(N258), .Z(N264) );
  GTECH_OR2 C335 ( .A(N259), .B(N260), .Z(N265) );
  GTECH_OR2 C336 ( .A(N261), .B(N262), .Z(N266) );
  GTECH_OR2 C337 ( .A(N263), .B(state[0]), .Z(N267) );
  GTECH_OR2 C338 ( .A(N264), .B(N265), .Z(N268) );
  GTECH_OR2 C339 ( .A(N266), .B(N267), .Z(N269) );
  GTECH_OR2 C340 ( .A(N268), .B(N269), .Z(N270) );
  GTECH_OR2 C343 ( .A(state[14]), .B(state[13]), .Z(N273) );
  GTECH_OR2 C344 ( .A(state[12]), .B(state[11]), .Z(N274) );
  GTECH_OR2 C345 ( .A(state[10]), .B(state[9]), .Z(N275) );
  GTECH_OR2 C346 ( .A(state[8]), .B(state[7]), .Z(N276) );
  GTECH_OR2 C347 ( .A(state[6]), .B(state[5]), .Z(N277) );
  GTECH_OR2 C348 ( .A(state[4]), .B(N272), .Z(N278) );
  GTECH_OR2 C349 ( .A(state[2]), .B(state[1]), .Z(N279) );
  GTECH_OR2 C350 ( .A(N273), .B(N274), .Z(N280) );
  GTECH_OR2 C351 ( .A(N275), .B(N276), .Z(N281) );
  GTECH_OR2 C352 ( .A(N277), .B(N278), .Z(N282) );
  GTECH_OR2 C353 ( .A(N279), .B(state[0]), .Z(N283) );
  GTECH_OR2 C354 ( .A(N280), .B(N281), .Z(N284) );
  GTECH_OR2 C355 ( .A(N282), .B(N283), .Z(N285) );
  GTECH_OR2 C356 ( .A(N284), .B(N285), .Z(N286) );
  GTECH_OR2 C359 ( .A(state[14]), .B(state[13]), .Z(N289) );
  GTECH_OR2 C360 ( .A(state[12]), .B(state[11]), .Z(N290) );
  GTECH_OR2 C361 ( .A(state[10]), .B(state[9]), .Z(N291) );
  GTECH_OR2 C362 ( .A(state[8]), .B(state[7]), .Z(N292) );
  GTECH_OR2 C363 ( .A(state[6]), .B(state[5]), .Z(N293) );
  GTECH_OR2 C364 ( .A(N288), .B(state[3]), .Z(N294) );
  GTECH_OR2 C365 ( .A(state[2]), .B(state[1]), .Z(N295) );
  GTECH_OR2 C366 ( .A(N289), .B(N290), .Z(N296) );
  GTECH_OR2 C367 ( .A(N291), .B(N292), .Z(N297) );
  GTECH_OR2 C368 ( .A(N293), .B(N294), .Z(N298) );
  GTECH_OR2 C369 ( .A(N295), .B(state[0]), .Z(N299) );
  GTECH_OR2 C370 ( .A(N296), .B(N297), .Z(N300) );
  GTECH_OR2 C371 ( .A(N298), .B(N299), .Z(N301) );
  GTECH_OR2 C372 ( .A(N300), .B(N301), .Z(N302) );
  GTECH_OR2 C375 ( .A(state[14]), .B(state[13]), .Z(N305) );
  GTECH_OR2 C376 ( .A(state[12]), .B(state[11]), .Z(N306) );
  GTECH_OR2 C377 ( .A(state[10]), .B(state[9]), .Z(N307) );
  GTECH_OR2 C378 ( .A(state[8]), .B(state[7]), .Z(N308) );
  GTECH_OR2 C379 ( .A(N304), .B(state[5]), .Z(N309) );
  GTECH_OR2 C380 ( .A(state[4]), .B(state[3]), .Z(N310) );
  GTECH_OR2 C381 ( .A(state[2]), .B(state[1]), .Z(N311) );
  GTECH_OR2 C382 ( .A(N305), .B(N306), .Z(N312) );
  GTECH_OR2 C383 ( .A(N307), .B(N308), .Z(N313) );
  GTECH_OR2 C384 ( .A(N309), .B(N310), .Z(N314) );
  GTECH_OR2 C385 ( .A(N311), .B(state[0]), .Z(N315) );
  GTECH_OR2 C386 ( .A(N312), .B(N313), .Z(N316) );
  GTECH_OR2 C387 ( .A(N314), .B(N315), .Z(N317) );
  GTECH_OR2 C388 ( .A(N316), .B(N317), .Z(N318) );
  GTECH_OR2 C391 ( .A(state[14]), .B(state[13]), .Z(N321) );
  GTECH_OR2 C392 ( .A(state[12]), .B(state[11]), .Z(N322) );
  GTECH_OR2 C393 ( .A(state[10]), .B(state[9]), .Z(N323) );
  GTECH_OR2 C394 ( .A(state[8]), .B(state[7]), .Z(N324) );
  GTECH_OR2 C395 ( .A(state[6]), .B(N320), .Z(N325) );
  GTECH_OR2 C396 ( .A(state[4]), .B(state[3]), .Z(N326) );
  GTECH_OR2 C397 ( .A(state[2]), .B(state[1]), .Z(N327) );
  GTECH_OR2 C398 ( .A(N321), .B(N322), .Z(N328) );
  GTECH_OR2 C399 ( .A(N323), .B(N324), .Z(N329) );
  GTECH_OR2 C400 ( .A(N325), .B(N326), .Z(N330) );
  GTECH_OR2 C401 ( .A(N327), .B(state[0]), .Z(N331) );
  GTECH_OR2 C402 ( .A(N328), .B(N329), .Z(N332) );
  GTECH_OR2 C403 ( .A(N330), .B(N331), .Z(N333) );
  GTECH_OR2 C404 ( .A(N332), .B(N333), .Z(N334) );
  GTECH_OR2 C407 ( .A(state[14]), .B(state[13]), .Z(N337) );
  GTECH_OR2 C408 ( .A(state[12]), .B(state[11]), .Z(N338) );
  GTECH_OR2 C409 ( .A(state[10]), .B(state[9]), .Z(N339) );
  GTECH_OR2 C410 ( .A(state[8]), .B(N336), .Z(N340) );
  GTECH_OR2 C411 ( .A(state[6]), .B(state[5]), .Z(N341) );
  GTECH_OR2 C412 ( .A(state[4]), .B(state[3]), .Z(N342) );
  GTECH_OR2 C413 ( .A(state[2]), .B(state[1]), .Z(N343) );
  GTECH_OR2 C414 ( .A(N337), .B(N338), .Z(N344) );
  GTECH_OR2 C415 ( .A(N339), .B(N340), .Z(N345) );
  GTECH_OR2 C416 ( .A(N341), .B(N342), .Z(N346) );
  GTECH_OR2 C417 ( .A(N343), .B(state[0]), .Z(N347) );
  GTECH_OR2 C418 ( .A(N344), .B(N345), .Z(N348) );
  GTECH_OR2 C419 ( .A(N346), .B(N347), .Z(N349) );
  GTECH_OR2 C420 ( .A(N348), .B(N349), .Z(N350) );
  GTECH_OR2 C423 ( .A(state[14]), .B(state[13]), .Z(N353) );
  GTECH_OR2 C424 ( .A(state[12]), .B(state[11]), .Z(N354) );
  GTECH_OR2 C425 ( .A(state[10]), .B(state[9]), .Z(N355) );
  GTECH_OR2 C426 ( .A(N352), .B(state[7]), .Z(N356) );
  GTECH_OR2 C427 ( .A(state[6]), .B(state[5]), .Z(N357) );
  GTECH_OR2 C428 ( .A(state[4]), .B(state[3]), .Z(N358) );
  GTECH_OR2 C429 ( .A(state[2]), .B(state[1]), .Z(N359) );
  GTECH_OR2 C430 ( .A(N353), .B(N354), .Z(N360) );
  GTECH_OR2 C431 ( .A(N355), .B(N356), .Z(N361) );
  GTECH_OR2 C432 ( .A(N357), .B(N358), .Z(N362) );
  GTECH_OR2 C433 ( .A(N359), .B(state[0]), .Z(N363) );
  GTECH_OR2 C434 ( .A(N360), .B(N361), .Z(N364) );
  GTECH_OR2 C435 ( .A(N362), .B(N363), .Z(N365) );
  GTECH_OR2 C436 ( .A(N364), .B(N365), .Z(N366) );
  GTECH_OR2 C439 ( .A(state[14]), .B(state[13]), .Z(N369) );
  GTECH_OR2 C440 ( .A(state[12]), .B(state[11]), .Z(N370) );
  GTECH_OR2 C441 ( .A(state[10]), .B(N368), .Z(N371) );
  GTECH_OR2 C442 ( .A(state[8]), .B(state[7]), .Z(N372) );
  GTECH_OR2 C443 ( .A(state[6]), .B(state[5]), .Z(N373) );
  GTECH_OR2 C444 ( .A(state[4]), .B(state[3]), .Z(N374) );
  GTECH_OR2 C445 ( .A(state[2]), .B(state[1]), .Z(N375) );
  GTECH_OR2 C446 ( .A(N369), .B(N370), .Z(N376) );
  GTECH_OR2 C447 ( .A(N371), .B(N372), .Z(N377) );
  GTECH_OR2 C448 ( .A(N373), .B(N374), .Z(N378) );
  GTECH_OR2 C449 ( .A(N375), .B(state[0]), .Z(N379) );
  GTECH_OR2 C450 ( .A(N376), .B(N377), .Z(N380) );
  GTECH_OR2 C451 ( .A(N378), .B(N379), .Z(N381) );
  GTECH_OR2 C452 ( .A(N380), .B(N381), .Z(N382) );
  GTECH_OR2 C455 ( .A(state[14]), .B(state[13]), .Z(N385) );
  GTECH_OR2 C456 ( .A(state[12]), .B(state[11]), .Z(N386) );
  GTECH_OR2 C457 ( .A(N384), .B(state[9]), .Z(N387) );
  GTECH_OR2 C458 ( .A(state[8]), .B(state[7]), .Z(N388) );
  GTECH_OR2 C459 ( .A(state[6]), .B(state[5]), .Z(N389) );
  GTECH_OR2 C460 ( .A(state[4]), .B(state[3]), .Z(N390) );
  GTECH_OR2 C461 ( .A(state[2]), .B(state[1]), .Z(N391) );
  GTECH_OR2 C462 ( .A(N385), .B(N386), .Z(N392) );
  GTECH_OR2 C463 ( .A(N387), .B(N388), .Z(N393) );
  GTECH_OR2 C464 ( .A(N389), .B(N390), .Z(N394) );
  GTECH_OR2 C465 ( .A(N391), .B(state[0]), .Z(N395) );
  GTECH_OR2 C466 ( .A(N392), .B(N393), .Z(N396) );
  GTECH_OR2 C467 ( .A(N394), .B(N395), .Z(N397) );
  GTECH_OR2 C468 ( .A(N396), .B(N397), .Z(N398) );
  GTECH_OR2 C471 ( .A(state[14]), .B(state[13]), .Z(N401) );
  GTECH_OR2 C472 ( .A(state[12]), .B(N400), .Z(N402) );
  GTECH_OR2 C473 ( .A(state[10]), .B(state[9]), .Z(N403) );
  GTECH_OR2 C474 ( .A(state[8]), .B(state[7]), .Z(N404) );
  GTECH_OR2 C475 ( .A(state[6]), .B(state[5]), .Z(N405) );
  GTECH_OR2 C476 ( .A(state[4]), .B(state[3]), .Z(N406) );
  GTECH_OR2 C477 ( .A(state[2]), .B(state[1]), .Z(N407) );
  GTECH_OR2 C478 ( .A(N401), .B(N402), .Z(N408) );
  GTECH_OR2 C479 ( .A(N403), .B(N404), .Z(N409) );
  GTECH_OR2 C480 ( .A(N405), .B(N406), .Z(N410) );
  GTECH_OR2 C481 ( .A(N407), .B(state[0]), .Z(N411) );
  GTECH_OR2 C482 ( .A(N408), .B(N409), .Z(N412) );
  GTECH_OR2 C483 ( .A(N410), .B(N411), .Z(N413) );
  GTECH_OR2 C484 ( .A(N412), .B(N413), .Z(N414) );
  GTECH_OR2 C487 ( .A(state[14]), .B(state[13]), .Z(N417) );
  GTECH_OR2 C488 ( .A(N416), .B(state[11]), .Z(N418) );
  GTECH_OR2 C489 ( .A(state[10]), .B(state[9]), .Z(N419) );
  GTECH_OR2 C490 ( .A(state[8]), .B(state[7]), .Z(N420) );
  GTECH_OR2 C491 ( .A(state[6]), .B(state[5]), .Z(N421) );
  GTECH_OR2 C492 ( .A(state[4]), .B(state[3]), .Z(N422) );
  GTECH_OR2 C493 ( .A(state[2]), .B(state[1]), .Z(N423) );
  GTECH_OR2 C494 ( .A(N417), .B(N418), .Z(N424) );
  GTECH_OR2 C495 ( .A(N419), .B(N420), .Z(N425) );
  GTECH_OR2 C496 ( .A(N421), .B(N422), .Z(N426) );
  GTECH_OR2 C497 ( .A(N423), .B(state[0]), .Z(N427) );
  GTECH_OR2 C498 ( .A(N424), .B(N425), .Z(N428) );
  GTECH_OR2 C499 ( .A(N426), .B(N427), .Z(N429) );
  GTECH_OR2 C500 ( .A(N428), .B(N429), .Z(N430) );
  GTECH_OR2 C503 ( .A(state[14]), .B(N432), .Z(N433) );
  GTECH_OR2 C504 ( .A(state[12]), .B(state[11]), .Z(N434) );
  GTECH_OR2 C505 ( .A(state[10]), .B(state[9]), .Z(N435) );
  GTECH_OR2 C506 ( .A(state[8]), .B(state[7]), .Z(N436) );
  GTECH_OR2 C507 ( .A(state[6]), .B(state[5]), .Z(N437) );
  GTECH_OR2 C508 ( .A(state[4]), .B(state[3]), .Z(N438) );
  GTECH_OR2 C509 ( .A(state[2]), .B(state[1]), .Z(N439) );
  GTECH_OR2 C510 ( .A(N433), .B(N434), .Z(N440) );
  GTECH_OR2 C511 ( .A(N435), .B(N436), .Z(N441) );
  GTECH_OR2 C512 ( .A(N437), .B(N438), .Z(N442) );
  GTECH_OR2 C513 ( .A(N439), .B(state[0]), .Z(N443) );
  GTECH_OR2 C514 ( .A(N440), .B(N441), .Z(N444) );
  GTECH_OR2 C515 ( .A(N442), .B(N443), .Z(N445) );
  GTECH_OR2 C516 ( .A(N444), .B(N445), .Z(N446) );
  GTECH_OR2 C519 ( .A(N448), .B(state[13]), .Z(N449) );
  GTECH_OR2 C520 ( .A(state[12]), .B(state[11]), .Z(N450) );
  GTECH_OR2 C521 ( .A(state[10]), .B(state[9]), .Z(N451) );
  GTECH_OR2 C522 ( .A(state[8]), .B(state[7]), .Z(N452) );
  GTECH_OR2 C523 ( .A(state[6]), .B(state[5]), .Z(N453) );
  GTECH_OR2 C524 ( .A(state[4]), .B(state[3]), .Z(N454) );
  GTECH_OR2 C525 ( .A(state[2]), .B(state[1]), .Z(N455) );
  GTECH_OR2 C526 ( .A(N449), .B(N450), .Z(N456) );
  GTECH_OR2 C527 ( .A(N451), .B(N452), .Z(N457) );
  GTECH_OR2 C528 ( .A(N453), .B(N454), .Z(N458) );
  GTECH_OR2 C529 ( .A(N455), .B(state[0]), .Z(N459) );
  GTECH_OR2 C530 ( .A(N456), .B(N457), .Z(N460) );
  GTECH_OR2 C531 ( .A(N458), .B(N459), .Z(N461) );
  GTECH_OR2 C532 ( .A(N460), .B(N461), .Z(N462) );
  GTECH_NOT I_0 ( .A(LineState[1]), .Z(N579) );
  GTECH_OR2 C1672 ( .A(LineState[0]), .B(N579), .Z(N580) );
  GTECH_NOT I_1 ( .A(N580), .Z(N581) );
  GTECH_NOT I_2 ( .A(ps_cnt[3]), .Z(N582) );
  GTECH_NOT I_3 ( .A(ps_cnt[2]), .Z(N583) );
  GTECH_NOT I_4 ( .A(ps_cnt[0]), .Z(N584) );
  GTECH_OR2 C1677 ( .A(N583), .B(N582), .Z(N585) );
  GTECH_OR2 C1678 ( .A(ps_cnt[1]), .B(N585), .Z(N586) );
  GTECH_OR2 C1679 ( .A(N584), .B(N586), .Z(N587) );
  GTECH_NOT I_5 ( .A(N587), .Z(N588) );
  GTECH_NOT I_6 ( .A(idle_cnt1[7]), .Z(N589) );
  GTECH_NOT I_7 ( .A(idle_cnt1[6]), .Z(N590) );
  GTECH_NOT I_8 ( .A(idle_cnt1[5]), .Z(N591) );
  GTECH_NOT I_9 ( .A(idle_cnt1[4]), .Z(N592) );
  GTECH_NOT I_10 ( .A(idle_cnt1[3]), .Z(N593) );
  GTECH_NOT I_11 ( .A(idle_cnt1[1]), .Z(N594) );
  GTECH_OR2 C1687 ( .A(N590), .B(N589), .Z(N595) );
  GTECH_OR2 C1688 ( .A(N591), .B(N595), .Z(N596) );
  GTECH_OR2 C1689 ( .A(N592), .B(N596), .Z(N597) );
  GTECH_OR2 C1690 ( .A(N593), .B(N597), .Z(N598) );
  GTECH_OR2 C1691 ( .A(idle_cnt1[2]), .B(N598), .Z(N599) );
  GTECH_OR2 C1692 ( .A(N594), .B(N599), .Z(N600) );
  GTECH_OR2 C1693 ( .A(idle_cnt1[0]), .B(N600), .Z(N601) );
  GTECH_NOT I_12 ( .A(N601), .Z(N602) );
  GTECH_NOT I_13 ( .A(me_ps[7]), .Z(N603) );
  GTECH_NOT I_14 ( .A(me_ps[4]), .Z(N604) );
  GTECH_NOT I_15 ( .A(me_ps[2]), .Z(N605) );
  GTECH_OR2 C1698 ( .A(me_ps[6]), .B(N603), .Z(N606) );
  GTECH_OR2 C1699 ( .A(me_ps[5]), .B(N606), .Z(N607) );
  GTECH_OR2 C1700 ( .A(N604), .B(N607), .Z(N608) );
  GTECH_OR2 C1701 ( .A(me_ps[3]), .B(N608), .Z(N609) );
  GTECH_OR2 C1702 ( .A(N605), .B(N609), .Z(N610) );
  GTECH_OR2 C1703 ( .A(me_ps[1]), .B(N610), .Z(N611) );
  GTECH_OR2 C1704 ( .A(me_ps[0]), .B(N611), .Z(N612) );
  GTECH_NOT I_16 ( .A(N612), .Z(N613) );
  GTECH_NOT I_17 ( .A(chirp_cnt[2]), .Z(N614) );
  GTECH_NOT I_18 ( .A(chirp_cnt[1]), .Z(N615) );
  GTECH_OR2 C1708 ( .A(N615), .B(N614), .Z(N616) );
  GTECH_OR2 C1709 ( .A(chirp_cnt[0]), .B(N616), .Z(N617) );
  GTECH_NOT I_19 ( .A(N617), .Z(N618) );
  GTECH_NOT I_20 ( .A(me_ps2[7]), .Z(N619) );
  GTECH_NOT I_21 ( .A(me_ps2[6]), .Z(N620) );
  GTECH_NOT I_22 ( .A(me_ps2[3]), .Z(N621) );
  GTECH_OR2 C1714 ( .A(N620), .B(N619), .Z(N622) );
  GTECH_OR2 C1715 ( .A(me_ps2[5]), .B(N622), .Z(N623) );
  GTECH_OR2 C1716 ( .A(me_ps2[4]), .B(N623), .Z(N624) );
  GTECH_OR2 C1717 ( .A(N621), .B(N624), .Z(N625) );
  GTECH_OR2 C1718 ( .A(me_ps2[2]), .B(N625), .Z(N626) );
  GTECH_OR2 C1719 ( .A(me_ps2[1]), .B(N626), .Z(N627) );
  GTECH_OR2 C1720 ( .A(me_ps2[0]), .B(N627), .Z(N628) );
  GTECH_NOT I_23 ( .A(N628), .Z(N629) );
  GTECH_NOT I_24 ( .A(me_cnt[7]), .Z(N630) );
  GTECH_NOT I_25 ( .A(me_cnt[6]), .Z(N631) );
  GTECH_NOT I_26 ( .A(me_cnt[3]), .Z(N632) );
  GTECH_OR2 C1725 ( .A(N631), .B(N630), .Z(N633) );
  GTECH_OR2 C1726 ( .A(me_cnt[5]), .B(N633), .Z(N634) );
  GTECH_OR2 C1727 ( .A(me_cnt[4]), .B(N634), .Z(N635) );
  GTECH_OR2 C1728 ( .A(N632), .B(N635), .Z(N636) );
  GTECH_OR2 C1729 ( .A(me_cnt[2]), .B(N636), .Z(N637) );
  GTECH_OR2 C1730 ( .A(me_cnt[1]), .B(N637), .Z(N638) );
  GTECH_OR2 C1731 ( .A(me_cnt[0]), .B(N638), .Z(N639) );
  GTECH_NOT I_27 ( .A(N639), .Z(N640) );
  GTECH_NOT I_28 ( .A(line_state_r[1]), .Z(N641) );
  GTECH_OR2 C1734 ( .A(line_state_r[0]), .B(N641), .Z(N642) );
  GTECH_NOT I_29 ( .A(N642), .Z(N643) );
  GTECH_NOT I_30 ( .A(line_state_r[0]), .Z(N644) );
  GTECH_OR2 C1737 ( .A(N644), .B(line_state_r[1]), .Z(N645) );
  GTECH_NOT I_31 ( .A(N645), .Z(N646) );
  GTECH_OR2 C1739 ( .A(line_state_r[0]), .B(line_state_r[1]), .Z(N647) );
  GTECH_NOT I_32 ( .A(N647), .Z(N648) );
  ADD_UNS_OP add_1346 ( .A(idle_cnt1), .B(1'b1), .Z({N111, N110, N109, N108, 
        N107, N106, N105, N104}) );
  ADD_UNS_OP add_1372 ( .A(me_ps), .B(1'b1), .Z({N129, N128, N127, N126, N125, 
        N124, N123, N122}) );
  ADD_UNS_OP add_1334 ( .A(ps_cnt), .B(1'b1), .Z({N84, N83, N82, N81}) );
  ADD_UNS_OP add_1390_S2 ( .A(me_cnt), .B(1'b1), .Z({N173, N172, N171, N170, 
        N169, N168, N167, N166}) );
  ADD_UNS_OP add_1413_S2 ( .A(chirp_cnt), .B(1'b1), .Z({N199, N198, N197}) );
  ADD_UNS_OP add_1381_S2 ( .A(me_ps2), .B(1'b1), .Z({N149, N148, N147, N146, 
        N145, N144, N143, N142}) );
  SELECT_OP C1749 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N0), 
        .CONTROL2(N41), .CONTROL3(N39), .Z(N40) );
  GTECH_BUF B_0 ( .A(mode_set_fs), .Z(N0) );
  SELECT_OP C1750 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N1), 
        .CONTROL2(N46), .CONTROL3(N44), .Z(N45) );
  GTECH_BUF B_1 ( .A(suspend_clr), .Z(N1) );
  SELECT_OP C1751 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N2), 
        .CONTROL2(N51), .CONTROL3(N49), .Z(N50) );
  GTECH_BUF B_2 ( .A(attached_clr), .Z(N2) );
  SELECT_OP C1752 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N3), 
        .CONTROL2(N56), .CONTROL3(N54), .Z(N55) );
  GTECH_BUF B_3 ( .A(fs_term_off), .Z(N3) );
  SELECT_OP C1753 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N4), 
        .CONTROL2(N62), .CONTROL3(N59), .Z(N60) );
  GTECH_BUF B_4 ( .A(xcv_set_fs), .Z(N4) );
  SELECT_OP C1754 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N5), 
        .CONTROL2(N67), .CONTROL3(N64), .Z(N65) );
  GTECH_BUF B_5 ( .A(bit_stuff_off), .Z(N5) );
  SELECT_OP C1755 ( .DATA1(N648), .DATA2(N646), .CONTROL1(N6), .CONTROL2(N7), 
        .Z(ls_idle) );
  GTECH_BUF B_6 ( .A(mode_hs), .Z(N6) );
  GTECH_BUF B_7 ( .A(N68), .Z(N7) );
  SELECT_OP C1756 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N8), .CONTROL2(N75), .CONTROL3(N78), .CONTROL4(N72), .Z(N73)
         );
  GTECH_BUF B_8 ( .A(N69), .Z(N8) );
  SELECT_OP C1757 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b1), .CONTROL1(N8), 
        .CONTROL2(N75), .CONTROL3(N78), .Z(N74) );
  SELECT_OP C1758 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0}), .DATA2({N84, N83, N82, 
        N81}), .CONTROL1(N9), .CONTROL2(N80), .Z({N88, N87, N86, N85}) );
  GTECH_BUF B_9 ( .A(N79), .Z(N9) );
  SELECT_OP C1759 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N10), 
        .CONTROL2(N103), .CONTROL3(N92), .Z(N93) );
  GTECH_BUF B_10 ( .A(N89), .Z(N10) );
  SELECT_OP C1760 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), 
        .DATA2(idle_cnt1_next), .CONTROL1(N10), .CONTROL2(N103), .Z({N101, 
        N100, N99, N98, N97, N96, N95, N94}) );
  SELECT_OP C1761 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), 
        .DATA2({N129, N128, N127, N126, N125, N124, N123, N122}), .CONTROL1(
        N11), .CONTROL2(N121), .Z({N137, N136, N135, N134, N133, N132, N131, 
        N130}) );
  GTECH_BUF B_11 ( .A(N120), .Z(N11) );
  SELECT_OP C1762 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N12), 
        .CONTROL2(N160), .CONTROL3(N140), .Z(N150) );
  GTECH_BUF B_12 ( .A(N138), .Z(N12) );
  SELECT_OP C1763 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), 
        .DATA2({N149, N148, N147, N146, N145, N144, N143, N142}), .CONTROL1(
        N12), .CONTROL2(N160), .Z({N158, N157, N156, N155, N154, N153, N152, 
        N151}) );
  SELECT_OP C1764 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N13), 
        .CONTROL2(N184), .CONTROL3(N164), .Z(N174) );
  GTECH_BUF B_13 ( .A(me_cnt_clr), .Z(N13) );
  SELECT_OP C1765 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), 
        .DATA2({N173, N172, N171, N170, N169, N168, N167, N166}), .CONTROL1(
        N13), .CONTROL2(N184), .Z({N182, N181, N180, N179, N178, N177, N176, 
        N175}) );
  SELECT_OP C1766 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N14), 
        .CONTROL2(N205), .CONTROL3(N195), .Z(N200) );
  GTECH_BUF B_14 ( .A(chirp_cnt_clr), .Z(N14) );
  SELECT_OP C1767 ( .DATA1({1'b0, 1'b0, 1'b0}), .DATA2({N199, N198, N197}), 
        .CONTROL1(N14), .CONTROL2(N205), .Z({N203, N202, N201}) );
  SELECT_OP C1768 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1}), .DATA2({1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1}), .DATA3(next_state), .CONTROL1(N8), .CONTROL2(N223), .CONTROL3(N207), .Z({
        N222, N221, N220, N219, N218, N217, N216, N215, N214, N213, N212, N211, 
        N210, N209, N208}) );
  SELECT_OP C1769 ( .DATA1(1'b1), .DATA2(1'b0), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N15), .CONTROL2(N527), .CONTROL3(N530), .CONTROL4(N469), .Z(
        N470) );
  GTECH_BUF B_15 ( .A(N464), .Z(N15) );
  SELECT_OP C1770 ( .DATA1({1'b1, 1'b0, 1'b0}), .DATA2({1'b0, 1'b1, 1'b0}), 
        .DATA3({1'b0, 1'b0, 1'b1}), .CONTROL1(N15), .CONTROL2(N527), 
        .CONTROL3(N530), .Z({N473, N472, N471}) );
  SELECT_OP C1771 ( .DATA1(1'b0), .DATA2(1'b1), .DATA3(1'b0), .DATA4(1'b0), 
        .CONTROL1(N15), .CONTROL2(N527), .CONTROL3(N474), .CONTROL4(N16), .Z(
        N475) );
  GTECH_BUF B_16 ( .A(1'b0), .Z(N16) );
  SELECT_OP C1772 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N15), .CONTROL2(N527), .CONTROL3(N530), .CONTROL4(N469), .Z(
        N476) );
  SELECT_OP C1773 ( .DATA1(1'b1), .DATA2(1'b0), .DATA3(1'b0), .CONTROL1(N17), 
        .CONTROL2(N481), .CONTROL3(N16), .Z(N482) );
  GTECH_BUF B_17 ( .A(N477), .Z(N17) );
  SELECT_OP C1774 ( .DATA1({1'b1, 1'b0}), .DATA2({1'b0, 1'b1}), .CONTROL1(N17), 
        .CONTROL2(N531), .Z({N484, N483}) );
  SELECT_OP C1775 ( .DATA1(1'b0), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N17), 
        .CONTROL2(N531), .CONTROL3(N480), .Z(N485) );
  SELECT_OP C1776 ( .DATA1(1'b1), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .CONTROL1(N18), .CONTROL2(N491), .CONTROL3(N16), .CONTROL4(N16), .Z(
        N492) );
  GTECH_BUF B_18 ( .A(N486), .Z(N18) );
  SELECT_OP C1777 ( .DATA1({1'b1, 1'b0, 1'b0}), .DATA2({1'b0, 1'b0, 1'b1}), 
        .DATA3({1'b0, 1'b1, 1'b0}), .CONTROL1(N18), .CONTROL2(N532), 
        .CONTROL3(N535), .Z({N495, N494, N493}) );
  SELECT_OP C1778 ( .DATA1(mode_hs), .DATA2(1'b0), .CONTROL1(N19), .CONTROL2(
        N20), .Z(N496) );
  GTECH_BUF B_19 ( .A(N648), .Z(N19) );
  GTECH_BUF B_20 ( .A(N647), .Z(N20) );
  SELECT_OP C1779 ( .DATA1({1'b0, 1'b0}), .DATA2({se0_long, N503}), .CONTROL1(
        N21), .CONTROL2(N22), .Z({N505, N504}) );
  GTECH_BUF B_21 ( .A(chirp_cnt_is_6), .Z(N21) );
  GTECH_BUF B_22 ( .A(N501), .Z(N22) );
  SELECT_OP C1780 ( .DATA1(1'b0), .DATA2(k_long), .CONTROL1(N21), .CONTROL2(
        N22), .Z(N506) );
  SELECT_OP C1781 ( .DATA1({1'b0, 1'b0}), .DATA2({se0_long, N503}), .CONTROL1(
        N21), .CONTROL2(N22), .Z({N509, N508}) );
  SELECT_OP C1782 ( .DATA1(1'b0), .DATA2(j_long), .CONTROL1(N21), .CONTROL2(
        N22), .Z(N510) );
  SELECT_OP C1783 ( .DATA1(1'b1), .DATA2(N470), .DATA3(N482), .DATA4(N492), 
        .DATA5(N648), .DATA6(1'b0), .DATA7(T2_wakeup), .DATA8(1'b0), .DATA9(
        1'b0), .DATA10(T2_gt_1_0_mS), .DATA11(1'b0), .DATA12(1'b0), .DATA13(
        1'b0), .DATA14(1'b0), .DATA15(1'b0), .DATA16(1'b0), .CONTROL1(N23), 
        .CONTROL2(N24), .CONTROL3(N25), .CONTROL4(N26), .CONTROL5(N27), 
        .CONTROL6(N28), .CONTROL7(N29), .CONTROL8(N30), .CONTROL9(N31), 
        .CONTROL10(N32), .CONTROL11(N33), .CONTROL12(N34), .CONTROL13(N35), 
        .CONTROL14(N36), .CONTROL15(N37), .CONTROL16(N525), .Z(me_cnt_clr) );
  GTECH_BUF B_23 ( .A(N239), .Z(N23) );
  GTECH_BUF B_24 ( .A(N255), .Z(N24) );
  GTECH_BUF B_25 ( .A(N271), .Z(N25) );
  GTECH_BUF B_26 ( .A(N287), .Z(N26) );
  GTECH_BUF B_27 ( .A(N303), .Z(N27) );
  GTECH_BUF B_28 ( .A(N319), .Z(N28) );
  GTECH_BUF B_29 ( .A(N335), .Z(N29) );
  GTECH_BUF B_30 ( .A(N351), .Z(N30) );
  GTECH_BUF B_31 ( .A(N367), .Z(N31) );
  GTECH_BUF B_32 ( .A(N383), .Z(N32) );
  GTECH_BUF B_33 ( .A(N399), .Z(N33) );
  GTECH_BUF B_34 ( .A(N415), .Z(N34) );
  GTECH_BUF B_35 ( .A(N431), .Z(N35) );
  GTECH_BUF B_36 ( .A(N447), .Z(N36) );
  GTECH_BUF B_37 ( .A(N463), .Z(N37) );
  SELECT_OP C1784 ( .DATA1(1'b1), .DATA2(N476), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .DATA10(1'b0), .DATA11(1'b0), .DATA12(1'b0), .DATA13(1'b0), .DATA14(
        1'b0), .DATA15(1'b1), .DATA16(1'b0), .CONTROL1(N23), .CONTROL2(N24), 
        .CONTROL3(N25), .CONTROL4(N26), .CONTROL5(N27), .CONTROL6(N28), 
        .CONTROL7(N29), .CONTROL8(N30), .CONTROL9(N31), .CONTROL10(N32), 
        .CONTROL11(N33), .CONTROL12(N34), .CONTROL13(N35), .CONTROL14(N36), 
        .CONTROL15(N37), .CONTROL16(N525), .Z(xcv_set_fs) );
  SELECT_OP C1785 ( .DATA1(1'b1), .DATA2(N476), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(T2_wakeup), .DATA8(1'b0), .DATA9(
        1'b0), .DATA10(1'b1), .DATA11(1'b0), .DATA12(1'b0), .DATA13(1'b0), 
        .DATA14(1'b0), .DATA15(1'b1), .DATA16(1'b0), .CONTROL1(N23), 
        .CONTROL2(N24), .CONTROL3(N25), .CONTROL4(N26), .CONTROL5(N27), 
        .CONTROL6(N28), .CONTROL7(N29), .CONTROL8(N30), .CONTROL9(N31), 
        .CONTROL10(N32), .CONTROL11(N33), .CONTROL12(N34), .CONTROL13(N35), 
        .CONTROL14(N36), .CONTROL15(N37), .CONTROL16(N525), .Z(fs_term_on) );
  SELECT_OP C1786 ( .DATA1(1'b1), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .DATA10(1'b1), .DATA11(1'b0), .DATA12(1'b0), .DATA13(1'b0), .DATA14(
        1'b0), .DATA15(1'b1), .DATA16(1'b0), .CONTROL1(N23), .CONTROL2(N24), 
        .CONTROL3(N25), .CONTROL4(N26), .CONTROL5(N27), .CONTROL6(N28), 
        .CONTROL7(N29), .CONTROL8(N30), .CONTROL9(N31), .CONTROL10(N32), 
        .CONTROL11(N33), .CONTROL12(N34), .CONTROL13(N35), .CONTROL14(N36), 
        .CONTROL15(N37), .CONTROL16(N525), .Z(mode_set_fs) );
  SELECT_OP C1787 ( .DATA1(1'b1), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .DATA10(1'b0), .DATA11(1'b0), .DATA12(1'b0), .DATA13(1'b0), .DATA14(
        1'b0), .DATA15(1'b0), .DATA16(1'b0), .CONTROL1(N23), .CONTROL2(N24), 
        .CONTROL3(N25), .CONTROL4(N26), .CONTROL5(N27), .CONTROL6(N28), 
        .CONTROL7(N29), .CONTROL8(N30), .CONTROL9(N31), .CONTROL10(N32), 
        .CONTROL11(N33), .CONTROL12(N34), .CONTROL13(N35), .CONTROL14(N36), 
        .CONTROL15(N37), .CONTROL16(N525), .Z(attached_clr) );
  SELECT_OP C1788 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(N648), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .DATA10(1'b0), .DATA11(1'b0), .DATA12(1'b0), .DATA13(1'b0), .DATA14(
        1'b1), .DATA15(1'b1), .DATA16(1'b0), .CONTROL1(N23), .CONTROL2(N24), 
        .CONTROL3(N25), .CONTROL4(N26), .CONTROL5(N27), .CONTROL6(N28), 
        .CONTROL7(N29), .CONTROL8(N30), .CONTROL9(N31), .CONTROL10(N32), 
        .CONTROL11(N33), .CONTROL12(N34), .CONTROL13(N35), .CONTROL14(N36), 
        .CONTROL15(N37), .CONTROL16(N525), .Z(bit_stuff_on) );
  SELECT_OP C1789 ( .DATA1(1'b1), .DATA2(1'b0), .DATA3(1'b0), .DATA4(N492), 
        .DATA5(1'b1), .DATA6(1'b0), .DATA7(1'b1), .DATA8(1'b0), .DATA9(1'b0), 
        .DATA10(1'b0), .DATA11(1'b0), .DATA12(1'b0), .DATA13(1'b0), .DATA14(
        1'b0), .DATA15(1'b0), .DATA16(1'b0), .CONTROL1(N23), .CONTROL2(N24), 
        .CONTROL3(N25), .CONTROL4(N26), .CONTROL5(N27), .CONTROL6(N28), 
        .CONTROL7(N29), .CONTROL8(N30), .CONTROL9(N31), .CONTROL10(N32), 
        .CONTROL11(N33), .CONTROL12(N34), .CONTROL13(N35), .CONTROL14(N36), 
        .CONTROL15(N37), .CONTROL16(N525), .Z(suspend_clr) );
  SELECT_OP C1790 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), .DATA2({1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, N473, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, N472, N471, 1'b0, 1'b0}), .DATA3({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, N484, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        N483, 1'b0, 1'b0, 1'b0}), .DATA4({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, N495, 
        1'b0, 1'b0, 1'b0, N494, N493, 1'b0, 1'b0, 1'b0, 1'b0}), .DATA5({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0}), .DATA6({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0}), .DATA7({1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), .DATA8({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 
        1'b0, 1'b0, 1'b0, 1'b0}), .DATA9({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0}), .DATA10({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0}), .DATA11({1'b0, 1'b0, 1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), .DATA12({N505, 
        chirp_cnt_is_6, N504, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0}), .DATA13({N509, chirp_cnt_is_6, 1'b0, N508, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), 
        .DATA14({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b1, 1'b0}), .DATA15({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0}), .DATA16(
        state), .CONTROL1(N23), .CONTROL2(N24), .CONTROL3(N25), .CONTROL4(N26), 
        .CONTROL5(N27), .CONTROL6(N28), .CONTROL7(N29), .CONTROL8(N30), 
        .CONTROL9(N31), .CONTROL10(N32), .CONTROL11(N33), .CONTROL12(N34), 
        .CONTROL13(N35), .CONTROL14(N36), .CONTROL15(N37), .CONTROL16(N525), 
        .Z(next_state) );
  SELECT_OP C1791 ( .DATA1(1'b0), .DATA2(N475), .DATA3(N485), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b1), 
        .DATA10(1'b0), .DATA11(1'b0), .DATA12(1'b0), .DATA13(1'b0), .DATA14(
        1'b0), .DATA15(1'b0), .DATA16(1'b0), .CONTROL1(N23), .CONTROL2(N24), 
        .CONTROL3(N25), .CONTROL4(N26), .CONTROL5(N27), .CONTROL6(N28), 
        .CONTROL7(N29), .CONTROL8(N30), .CONTROL9(N31), .CONTROL10(N32), 
        .CONTROL11(N33), .CONTROL12(N34), .CONTROL13(N35), .CONTROL14(N36), 
        .CONTROL15(N37), .CONTROL16(N525), .Z(idle_cnt_clr) );
  SELECT_OP C1792 ( .DATA1(1'b0), .DATA2(N475), .DATA3(N485), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .DATA10(1'b0), .DATA11(1'b0), .DATA12(1'b0), .DATA13(1'b0), .DATA14(
        1'b0), .DATA15(1'b0), .DATA16(1'b0), .CONTROL1(N23), .CONTROL2(N24), 
        .CONTROL3(N25), .CONTROL4(N26), .CONTROL5(N27), .CONTROL6(N28), 
        .CONTROL7(N29), .CONTROL8(N30), .CONTROL9(N31), .CONTROL10(N32), 
        .CONTROL11(N33), .CONTROL12(N34), .CONTROL13(N35), .CONTROL14(N36), 
        .CONTROL15(N37), .CONTROL16(N525), .Z(suspend_set) );
  SELECT_OP C1793 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(N496), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .DATA10(1'b0), .DATA11(1'b0), .DATA12(1'b0), .DATA13(1'b0), .DATA14(
        1'b1), .DATA15(1'b0), .DATA16(1'b0), .CONTROL1(N23), .CONTROL2(N24), 
        .CONTROL3(N25), .CONTROL4(N26), .CONTROL5(N27), .CONTROL6(N28), 
        .CONTROL7(N29), .CONTROL8(N30), .CONTROL9(N31), .CONTROL10(N32), 
        .CONTROL11(N33), .CONTROL12(N34), .CONTROL13(N35), .CONTROL14(N36), 
        .CONTROL15(N37), .CONTROL16(N525), .Z(fs_term_off) );
  SELECT_OP C1794 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(N496), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .DATA10(1'b1), .DATA11(1'b0), .DATA12(1'b0), .DATA13(1'b0), .DATA14(
        1'b1), .DATA15(1'b0), .DATA16(1'b0), .CONTROL1(N23), .CONTROL2(N24), 
        .CONTROL3(N25), .CONTROL4(N26), .CONTROL5(N27), .CONTROL6(N28), 
        .CONTROL7(N29), .CONTROL8(N30), .CONTROL9(N31), .CONTROL10(N32), 
        .CONTROL11(N33), .CONTROL12(N34), .CONTROL13(N35), .CONTROL14(N36), 
        .CONTROL15(N37), .CONTROL16(N525), .Z(xcv_set_hs) );
  SELECT_OP C1795 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(T2_wakeup), .DATA8(1'b0), .DATA9(
        1'b0), .DATA10(1'b1), .DATA11(1'b0), .DATA12(1'b0), .DATA13(1'b0), 
        .DATA14(1'b0), .DATA15(1'b0), .DATA16(1'b0), .CONTROL1(N23), 
        .CONTROL2(N24), .CONTROL3(N25), .CONTROL4(N26), .CONTROL5(N27), 
        .CONTROL6(N28), .CONTROL7(N29), .CONTROL8(N30), .CONTROL9(N31), 
        .CONTROL10(N32), .CONTROL11(N33), .CONTROL12(N34), .CONTROL13(N35), 
        .CONTROL14(N36), .CONTROL15(N37), .CONTROL16(N525), .Z(bit_stuff_off)
         );
  SELECT_OP C1796 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b1), .DATA9(1'b0), 
        .DATA10(1'b0), .DATA11(1'b1), .DATA12(1'b0), .DATA13(1'b0), .DATA14(
        1'b0), .DATA15(1'b0), .DATA16(1'b0), .CONTROL1(N23), .CONTROL2(N24), 
        .CONTROL3(N25), .CONTROL4(N26), .CONTROL5(N27), .CONTROL6(N28), 
        .CONTROL7(N29), .CONTROL8(N30), .CONTROL9(N31), .CONTROL10(N32), 
        .CONTROL11(N33), .CONTROL12(N34), .CONTROL13(N35), .CONTROL14(N36), 
        .CONTROL15(N37), .CONTROL16(N525), .Z(drive_k_d) );
  SELECT_OP C1797 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(
        me_cnt_100_ms), .DATA10(1'b0), .DATA11(1'b0), .DATA12(1'b0), .DATA13(
        1'b0), .DATA14(1'b0), .DATA15(1'b0), .DATA16(1'b0), .CONTROL1(N23), 
        .CONTROL2(N24), .CONTROL3(N25), .CONTROL4(N26), .CONTROL5(N27), 
        .CONTROL6(N28), .CONTROL7(N29), .CONTROL8(N30), .CONTROL9(N31), 
        .CONTROL10(N32), .CONTROL11(N33), .CONTROL12(N34), .CONTROL13(N35), 
        .CONTROL14(N36), .CONTROL15(N37), .CONTROL16(N525), .Z(attached_set)
         );
  SELECT_OP C1798 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .DATA10(1'b1), .DATA11(1'b0), .DATA12(1'b0), .DATA13(1'b0), .DATA14(
        1'b0), .DATA15(1'b0), .DATA16(1'b0), .CONTROL1(N23), .CONTROL2(N24), 
        .CONTROL3(N25), .CONTROL4(N26), .CONTROL5(N27), .CONTROL6(N28), 
        .CONTROL7(N29), .CONTROL8(N30), .CONTROL9(N31), .CONTROL10(N32), 
        .CONTROL11(N33), .CONTROL12(N34), .CONTROL13(N35), .CONTROL14(N36), 
        .CONTROL15(N37), .CONTROL16(N525), .Z(usb_reset_d) );
  SELECT_OP C1799 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .DATA10(1'b0), .DATA11(1'b1), .DATA12(1'b0), .DATA13(1'b0), .DATA14(
        1'b0), .DATA15(1'b0), .DATA16(1'b0), .CONTROL1(N23), .CONTROL2(N24), 
        .CONTROL3(N25), .CONTROL4(N26), .CONTROL5(N27), .CONTROL6(N28), 
        .CONTROL7(N29), .CONTROL8(N30), .CONTROL9(N31), .CONTROL10(N32), 
        .CONTROL11(N33), .CONTROL12(N34), .CONTROL13(N35), .CONTROL14(N36), 
        .CONTROL15(N37), .CONTROL16(N525), .Z(chirp_cnt_clr) );
  SELECT_OP C1800 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .DATA10(1'b0), .DATA11(1'b0), .DATA12(N506), .DATA13(N510), .DATA14(
        1'b0), .DATA15(1'b0), .DATA16(1'b0), .CONTROL1(N23), .CONTROL2(N24), 
        .CONTROL3(N25), .CONTROL4(N26), .CONTROL5(N27), .CONTROL6(N28), 
        .CONTROL7(N29), .CONTROL8(N30), .CONTROL9(N31), .CONTROL10(N32), 
        .CONTROL11(N33), .CONTROL12(N34), .CONTROL13(N35), .CONTROL14(N36), 
        .CONTROL15(N37), .CONTROL16(N525), .Z(chirp_cnt_inc) );
  SELECT_OP C1801 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .DATA10(1'b0), .DATA11(1'b0), .DATA12(1'b0), .DATA13(1'b0), .DATA14(
        1'b1), .DATA15(1'b0), .DATA16(1'b0), .CONTROL1(N23), .CONTROL2(N24), 
        .CONTROL3(N25), .CONTROL4(N26), .CONTROL5(N27), .CONTROL6(N28), 
        .CONTROL7(N29), .CONTROL8(N30), .CONTROL9(N31), .CONTROL10(N32), 
        .CONTROL11(N33), .CONTROL12(N34), .CONTROL13(N35), .CONTROL14(N36), 
        .CONTROL15(N37), .CONTROL16(N525), .Z(mode_set_hs) );
  GTECH_OR2 C1804 ( .A(N650), .B(N581), .Z(SuspendM) );
  GTECH_AND2 C1805 ( .A(usb_suspend), .B(N649), .Z(N650) );
  GTECH_NOT I_33 ( .A(resume_req_s), .Z(N649) );
  GTECH_OR2 C1809 ( .A(mode_set_hs), .B(mode_set_fs), .Z(N38) );
  GTECH_NOT I_34 ( .A(N38), .Z(N39) );
  GTECH_NOT I_35 ( .A(mode_set_fs), .Z(N42) );
  GTECH_AND2 C1812 ( .A(mode_set_hs), .B(N42), .Z(N41) );
  GTECH_OR2 C1815 ( .A(suspend_set), .B(suspend_clr), .Z(N43) );
  GTECH_NOT I_36 ( .A(N43), .Z(N44) );
  GTECH_NOT I_37 ( .A(suspend_clr), .Z(N47) );
  GTECH_AND2 C1818 ( .A(suspend_set), .B(N47), .Z(N46) );
  GTECH_OR2 C1821 ( .A(attached_set), .B(attached_clr), .Z(N48) );
  GTECH_NOT I_38 ( .A(N48), .Z(N49) );
  GTECH_NOT I_39 ( .A(attached_clr), .Z(N52) );
  GTECH_AND2 C1824 ( .A(attached_set), .B(N52), .Z(N51) );
  GTECH_OR2 C1827 ( .A(fs_term_on), .B(fs_term_off), .Z(N53) );
  GTECH_NOT I_40 ( .A(N53), .Z(N54) );
  GTECH_NOT I_41 ( .A(fs_term_off), .Z(N57) );
  GTECH_AND2 C1830 ( .A(fs_term_on), .B(N57), .Z(N56) );
  GTECH_OR2 C1833 ( .A(xcv_set_hs), .B(xcv_set_fs), .Z(N58) );
  GTECH_NOT I_42 ( .A(N58), .Z(N59) );
  GTECH_NOT I_43 ( .A(xcv_set_fs), .Z(N61) );
  GTECH_AND2 C1836 ( .A(xcv_set_hs), .B(N61), .Z(N62) );
  GTECH_OR2 C1839 ( .A(bit_stuff_on), .B(bit_stuff_off), .Z(N63) );
  GTECH_NOT I_44 ( .A(N63), .Z(N64) );
  GTECH_NOT I_45 ( .A(bit_stuff_off), .Z(N66) );
  GTECH_AND2 C1842 ( .A(bit_stuff_on), .B(N66), .Z(N67) );
  GTECH_NOT I_46 ( .A(mode_hs), .Z(N68) );
  GTECH_AND2 C1846 ( .A(ls_idle), .B(ls_idle_r), .Z(idle_long_set) );
  GTECH_AND2 C1847 ( .A(N651), .B(N652), .Z(idle_long_clr) );
  GTECH_NOT I_47 ( .A(ls_idle), .Z(N651) );
  GTECH_NOT I_48 ( .A(ls_idle_r), .Z(N652) );
  GTECH_NOT I_49 ( .A(rst), .Z(N69) );
  GTECH_OR2 C1854 ( .A(idle_long_clr), .B(N69), .Z(N70) );
  GTECH_OR2 C1855 ( .A(idle_long_set), .B(N70), .Z(N71) );
  GTECH_NOT I_50 ( .A(N71), .Z(N72) );
  GTECH_AND2 C1858 ( .A(idle_long_clr), .B(rst), .Z(N75) );
  GTECH_NOT I_51 ( .A(idle_long_clr), .Z(N76) );
  GTECH_AND2 C1860 ( .A(rst), .B(N76), .Z(N77) );
  GTECH_AND2 C1861 ( .A(idle_long_set), .B(N77), .Z(N78) );
  GTECH_AND2 C1862 ( .A(N643), .B(ls_k_r), .Z(k_long) );
  GTECH_AND2 C1863 ( .A(N646), .B(ls_j_r), .Z(j_long) );
  GTECH_AND2 C1864 ( .A(N648), .B(ls_se0_r), .Z(se0_long) );
  GTECH_OR2 C1865 ( .A(N654), .B(ps_cnt_clr), .Z(N79) );
  GTECH_OR2 C1866 ( .A(N653), .B(idle_cnt_clr), .Z(N654) );
  GTECH_NOT I_52 ( .A(idle_long), .Z(N653) );
  GTECH_NOT I_53 ( .A(N79), .Z(N80) );
  GTECH_BUF B_38 ( .A(N80) );
  GTECH_OR2 C1871 ( .A(N655), .B(idle_cnt_clr), .Z(N89) );
  GTECH_OR2 C1872 ( .A(N653), .B(idle_cnt1_clr), .Z(N655) );
  GTECH_AND2 C1874 ( .A(N656), .B(ps_cnt_clr), .Z(N90) );
  GTECH_NOT I_54 ( .A(T1_gt_5_0_mS), .Z(N656) );
  GTECH_OR2 C1878 ( .A(N90), .B(N89), .Z(N91) );
  GTECH_NOT I_55 ( .A(N91), .Z(N92) );
  GTECH_NOT I_56 ( .A(N89), .Z(N102) );
  GTECH_AND2 C1881 ( .A(N90), .B(N102), .Z(N103) );
  GTECH_AND2 C1882 ( .A(N657), .B(N112), .Z(N113) );
  GTECH_NOT I_57 ( .A(idle_cnt_clr), .Z(N657) );
  GTECH_AND2 C1884 ( .A(N657), .B(N114), .Z(N115) );
  GTECH_AND2 C1886 ( .A(N657), .B(N116), .Z(N117) );
  GTECH_AND2 C1888 ( .A(N657), .B(N118), .Z(N119) );
  GTECH_OR2 C1890 ( .A(me_cnt_clr), .B(me_ps_2_5_us), .Z(N120) );
  GTECH_NOT I_58 ( .A(N120), .Z(N121) );
  GTECH_BUF B_39 ( .A(N121) );
  GTECH_OR2 C1894 ( .A(me_cnt_clr), .B(me_ps2_0_5_ms), .Z(N138) );
  GTECH_OR2 C1897 ( .A(me_ps_2_5_us), .B(N138), .Z(N139) );
  GTECH_NOT I_59 ( .A(N139), .Z(N140) );
  GTECH_BUF B_40 ( .A(N160), .Z(N141) );
  GTECH_NOT I_60 ( .A(N138), .Z(N159) );
  GTECH_AND2 C1901 ( .A(me_ps_2_5_us), .B(N159), .Z(N160) );
  GTECH_AND2 C1902 ( .A(N141), .B(N159) );
  GTECH_AND2 C1903 ( .A(N629), .B(N658), .Z(N161) );
  GTECH_NOT I_61 ( .A(me_ps2_0_5_ms), .Z(N658) );
  GTECH_AND2 C1905 ( .A(N659), .B(me_ps2_0_5_ms), .Z(N162) );
  GTECH_NOT I_62 ( .A(me_cnt_100_ms), .Z(N659) );
  GTECH_OR2 C1909 ( .A(N162), .B(me_cnt_clr), .Z(N163) );
  GTECH_NOT I_63 ( .A(N163), .Z(N164) );
  GTECH_BUF B_41 ( .A(N184), .Z(N165) );
  GTECH_NOT I_64 ( .A(me_cnt_clr), .Z(N183) );
  GTECH_AND2 C1913 ( .A(N162), .B(N183), .Z(N184) );
  GTECH_AND2 C1914 ( .A(N165), .B(N183) );
  GTECH_AND2 C1915 ( .A(N660), .B(N185), .Z(N186) );
  GTECH_NOT I_65 ( .A(me_cnt_clr), .Z(N660) );
  GTECH_AND2 C1917 ( .A(N660), .B(N187), .Z(N188) );
  GTECH_AND2 C1919 ( .A(N660), .B(N189), .Z(N190) );
  GTECH_AND2 C1921 ( .A(N660), .B(N191), .Z(N192) );
  GTECH_AND2 C1923 ( .A(N660), .B(N640), .Z(N193) );
  GTECH_OR2 C1927 ( .A(chirp_cnt_inc), .B(chirp_cnt_clr), .Z(N194) );
  GTECH_NOT I_66 ( .A(N194), .Z(N195) );
  GTECH_BUF B_42 ( .A(N205), .Z(N196) );
  GTECH_NOT I_67 ( .A(chirp_cnt_clr), .Z(N204) );
  GTECH_AND2 C1931 ( .A(chirp_cnt_inc), .B(N204), .Z(N205) );
  GTECH_AND2 C1932 ( .A(N196), .B(N204) );
  GTECH_OR2 C1934 ( .A(usb_vbus), .B(N69), .Z(N206) );
  GTECH_NOT I_68 ( .A(N206), .Z(N207) );
  GTECH_AND2 C1936 ( .A(usb_vbus), .B(rst), .Z(N223) );
  GTECH_NOT I_69 ( .A(state[0]), .Z(N224) );
  GTECH_NOT I_70 ( .A(N238), .Z(N239) );
  GTECH_NOT I_71 ( .A(state[1]), .Z(N240) );
  GTECH_NOT I_72 ( .A(N254), .Z(N255) );
  GTECH_NOT I_73 ( .A(state[2]), .Z(N256) );
  GTECH_NOT I_74 ( .A(N270), .Z(N271) );
  GTECH_NOT I_75 ( .A(state[3]), .Z(N272) );
  GTECH_NOT I_76 ( .A(N286), .Z(N287) );
  GTECH_NOT I_77 ( .A(state[4]), .Z(N288) );
  GTECH_NOT I_78 ( .A(N302), .Z(N303) );
  GTECH_NOT I_79 ( .A(state[6]), .Z(N304) );
  GTECH_NOT I_80 ( .A(N318), .Z(N319) );
  GTECH_NOT I_81 ( .A(state[5]), .Z(N320) );
  GTECH_NOT I_82 ( .A(N334), .Z(N335) );
  GTECH_NOT I_83 ( .A(state[7]), .Z(N336) );
  GTECH_NOT I_84 ( .A(N350), .Z(N351) );
  GTECH_NOT I_85 ( .A(state[8]), .Z(N352) );
  GTECH_NOT I_86 ( .A(N366), .Z(N367) );
  GTECH_NOT I_87 ( .A(state[9]), .Z(N368) );
  GTECH_NOT I_88 ( .A(N382), .Z(N383) );
  GTECH_NOT I_89 ( .A(state[10]), .Z(N384) );
  GTECH_NOT I_90 ( .A(N398), .Z(N399) );
  GTECH_NOT I_91 ( .A(state[11]), .Z(N400) );
  GTECH_NOT I_92 ( .A(N414), .Z(N415) );
  GTECH_NOT I_93 ( .A(state[12]), .Z(N416) );
  GTECH_NOT I_94 ( .A(N430), .Z(N431) );
  GTECH_NOT I_95 ( .A(state[13]), .Z(N432) );
  GTECH_NOT I_96 ( .A(N446), .Z(N447) );
  GTECH_NOT I_97 ( .A(state[14]), .Z(N448) );
  GTECH_NOT I_98 ( .A(N462), .Z(N463) );
  GTECH_AND2 C1982 ( .A(N662), .B(N653), .Z(N464) );
  GTECH_AND2 C1983 ( .A(N661), .B(T1_st_3_0_mS), .Z(N662) );
  GTECH_AND2 C1984 ( .A(N68), .B(T1_gt_2_5_uS), .Z(N661) );
  GTECH_AND2 C1987 ( .A(N68), .B(T1_gt_3_0_mS), .Z(N465) );
  GTECH_AND2 C1989 ( .A(mode_hs), .B(T1_gt_3_0_mS), .Z(N466) );
  GTECH_OR2 C1993 ( .A(N465), .B(N464), .Z(N467) );
  GTECH_OR2 C1994 ( .A(N466), .B(N467), .Z(N468) );
  GTECH_NOT I_99 ( .A(N468), .Z(N469) );
  GTECH_NOT I_100 ( .A(N467), .Z(N474) );
  GTECH_AND2 C1997 ( .A(T2_gt_100_uS), .B(se0_long), .Z(N477) );
  GTECH_AND2 C1998 ( .A(T2_gt_100_uS), .B(j_long), .Z(N478) );
  GTECH_OR2 C2001 ( .A(N478), .B(N477), .Z(N479) );
  GTECH_NOT I_101 ( .A(N479), .Z(N480) );
  GTECH_NOT I_102 ( .A(N477), .Z(N481) );
  GTECH_AND2 C2004 ( .A(T1_gt_2_5_uS), .B(se0_long), .Z(N486) );
  GTECH_AND2 C2005 ( .A(T1_gt_5_0_mS), .B(resume_req_s), .Z(N487) );
  GTECH_OR2 C2009 ( .A(k_long), .B(N486), .Z(N488) );
  GTECH_OR2 C2010 ( .A(N487), .B(N488), .Z(N489) );
  GTECH_NOT I_103 ( .A(N489), .Z(N490) );
  GTECH_NOT I_104 ( .A(N486), .Z(N491) );
  GTECH_NOT I_105 ( .A(T2_gt_100_uS), .Z(N497) );
  GTECH_NOT I_106 ( .A(T2_wakeup), .Z(N498) );
  GTECH_NOT I_107 ( .A(T2_gt_1_0_mS), .Z(N499) );
  GTECH_NOT I_108 ( .A(T2_gt_1_2_mS), .Z(N500) );
  GTECH_NOT I_109 ( .A(chirp_cnt_is_6), .Z(N501) );
  GTECH_NOT I_110 ( .A(k_long), .Z(N502) );
  GTECH_NOT I_111 ( .A(se0_long), .Z(N503) );
  GTECH_NOT I_112 ( .A(j_long), .Z(N507) );
  GTECH_OR2 C2052 ( .A(N255), .B(N239), .Z(N511) );
  GTECH_OR2 C2053 ( .A(N271), .B(N511), .Z(N512) );
  GTECH_OR2 C2054 ( .A(N287), .B(N512), .Z(N513) );
  GTECH_OR2 C2055 ( .A(N303), .B(N513), .Z(N514) );
  GTECH_OR2 C2056 ( .A(N319), .B(N514), .Z(N515) );
  GTECH_OR2 C2057 ( .A(N335), .B(N515), .Z(N516) );
  GTECH_OR2 C2058 ( .A(N351), .B(N516), .Z(N517) );
  GTECH_OR2 C2059 ( .A(N367), .B(N517), .Z(N518) );
  GTECH_OR2 C2060 ( .A(N383), .B(N518), .Z(N519) );
  GTECH_OR2 C2061 ( .A(N399), .B(N519), .Z(N520) );
  GTECH_OR2 C2062 ( .A(N415), .B(N520), .Z(N521) );
  GTECH_OR2 C2063 ( .A(N431), .B(N521), .Z(N522) );
  GTECH_OR2 C2064 ( .A(N447), .B(N522), .Z(N523) );
  GTECH_OR2 C2065 ( .A(N463), .B(N523), .Z(N524) );
  GTECH_NOT I_113 ( .A(N524), .Z(N525) );
  GTECH_NOT I_114 ( .A(N464), .Z(N526) );
  GTECH_AND2 C2068 ( .A(N465), .B(N526), .Z(N527) );
  GTECH_NOT I_115 ( .A(N465), .Z(N528) );
  GTECH_AND2 C2070 ( .A(N526), .B(N528), .Z(N529) );
  GTECH_AND2 C2071 ( .A(N466), .B(N529), .Z(N530) );
  GTECH_AND2 C2072 ( .A(N478), .B(N481), .Z(N531) );
  GTECH_AND2 C2073 ( .A(k_long), .B(N491), .Z(N532) );
  GTECH_NOT I_116 ( .A(k_long), .Z(N533) );
  GTECH_AND2 C2075 ( .A(N491), .B(N533), .Z(N534) );
  GTECH_AND2 C2076 ( .A(N487), .B(N534), .Z(N535) );
  GTECH_AND2 C2077 ( .A(N255), .B(N207), .Z(N536) );
  GTECH_AND2 C2078 ( .A(N469), .B(N536), .Z(N537) );
  GTECH_AND2 C2079 ( .A(N271), .B(N207), .Z(N538) );
  GTECH_AND2 C2080 ( .A(N480), .B(N538), .Z(N539) );
  GTECH_OR2 C2081 ( .A(N537), .B(N539), .Z(N540) );
  GTECH_AND2 C2082 ( .A(N287), .B(N207), .Z(N541) );
  GTECH_AND2 C2083 ( .A(N490), .B(N541), .Z(N542) );
  GTECH_OR2 C2084 ( .A(N540), .B(N542), .Z(N543) );
  GTECH_AND2 C2085 ( .A(N303), .B(N207), .Z(N544) );
  GTECH_AND2 C2086 ( .A(N647), .B(N544), .Z(N545) );
  GTECH_OR2 C2087 ( .A(N543), .B(N545), .Z(N546) );
  GTECH_AND2 C2088 ( .A(N319), .B(N207), .Z(N547) );
  GTECH_AND2 C2089 ( .A(N497), .B(N547), .Z(N548) );
  GTECH_OR2 C2090 ( .A(N546), .B(N548), .Z(N549) );
  GTECH_AND2 C2091 ( .A(N335), .B(N207), .Z(N550) );
  GTECH_AND2 C2092 ( .A(N498), .B(N550), .Z(N551) );
  GTECH_OR2 C2093 ( .A(N549), .B(N551), .Z(N552) );
  GTECH_AND2 C2094 ( .A(N351), .B(N207), .Z(N553) );
  GTECH_AND2 C2095 ( .A(N499), .B(N553), .Z(N554) );
  GTECH_OR2 C2096 ( .A(N552), .B(N554), .Z(N555) );
  GTECH_AND2 C2097 ( .A(N367), .B(N207), .Z(N556) );
  GTECH_AND2 C2098 ( .A(N659), .B(N556), .Z(N557) );
  GTECH_OR2 C2099 ( .A(N555), .B(N557), .Z(N558) );
  GTECH_AND2 C2100 ( .A(N383), .B(N207), .Z(N559) );
  GTECH_AND2 C2101 ( .A(N499), .B(N559), .Z(N560) );
  GTECH_OR2 C2102 ( .A(N558), .B(N560), .Z(N561) );
  GTECH_AND2 C2103 ( .A(N399), .B(N207), .Z(N562) );
  GTECH_AND2 C2104 ( .A(N500), .B(N562), .Z(N563) );
  GTECH_OR2 C2105 ( .A(N561), .B(N563), .Z(N564) );
  GTECH_AND2 C2106 ( .A(N415), .B(N207), .Z(N565) );
  GTECH_AND2 C2107 ( .A(N501), .B(N565), .Z(N566) );
  GTECH_AND2 C2108 ( .A(N503), .B(N566), .Z(N567) );
  GTECH_AND2 C2109 ( .A(N502), .B(N567), .Z(N568) );
  GTECH_OR2 C2110 ( .A(N564), .B(N568), .Z(N569) );
  GTECH_AND2 C2111 ( .A(N431), .B(N207), .Z(N570) );
  GTECH_AND2 C2112 ( .A(N501), .B(N570), .Z(N571) );
  GTECH_AND2 C2113 ( .A(N503), .B(N571), .Z(N572) );
  GTECH_AND2 C2114 ( .A(N507), .B(N572), .Z(N573) );
  GTECH_OR2 C2115 ( .A(N569), .B(N573), .Z(N574) );
  GTECH_AND2 C2116 ( .A(N447), .B(N207), .Z(N575) );
  GTECH_AND2 C2117 ( .A(N503), .B(N575), .Z(N576) );
  GTECH_OR2 C2118 ( .A(N574), .B(N576), .Z(N577) );
  GTECH_NOT I_117 ( .A(N577), .Z(N578) );
endmodule


module usbf_utmi_if ( phy_clk, rst, DataOut, TxValid, TxReady, RxValid, 
        RxActive, RxError, DataIn, XcvSelect, TermSel, SuspendM, LineState, 
        OpMode, usb_vbus, rx_data, rx_valid, rx_active, rx_err, tx_data, 
        tx_valid, tx_valid_last, tx_ready, tx_first, mode_hs, usb_reset, 
        usb_suspend, usb_attached, resume_req, suspend_clr );
  output [7:0] DataOut;
  input [7:0] DataIn;
  input [1:0] LineState;
  output [1:0] OpMode;
  output [7:0] rx_data;
  input [7:0] tx_data;
  input phy_clk, rst, TxReady, RxValid, RxActive, RxError, usb_vbus, tx_valid,
         tx_valid_last, tx_first, resume_req;
  output TxValid, XcvSelect, TermSel, SuspendM, rx_valid, rx_active, rx_err,
         tx_ready, mode_hs, usb_reset, usb_suspend, usb_attached, suspend_clr;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15,
         N16, N17, N18, drive_k, N19, N20, N21, N22, N23, N24, N25, N26, N27,
         N28, N29, N30, N31, drive_k_r, N32, N33, N34, N35, N36, N37, N38, N39,
         N40;

  \**SEQGEN**  rx_valid_reg ( .clear(1'b0), .preset(1'b0), .next_state(N11), 
        .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(rx_valid), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  rx_active_reg ( .clear(1'b0), .preset(1'b0), .next_state(N14), 
        .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(rx_active), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  rx_err_reg ( .clear(1'b0), .preset(1'b0), .next_state(N17), 
        .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(rx_err), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \rx_data_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        DataIn[7]), .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(
        rx_data[7]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_data_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        DataIn[6]), .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(
        rx_data[6]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_data_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        DataIn[5]), .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(
        rx_data[5]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_data_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        DataIn[4]), .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(
        rx_data[4]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_data_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        DataIn[3]), .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(
        rx_data[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_data_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        DataIn[2]), .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(
        rx_data[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_data_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        DataIn[1]), .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(
        rx_data[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_data_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        DataIn[0]), .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(
        rx_data[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \DataOut_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N29), .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(DataOut[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N21) );
  \**SEQGEN**  \DataOut_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N28), .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(DataOut[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N21) );
  \**SEQGEN**  \DataOut_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N27), .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(DataOut[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N21) );
  \**SEQGEN**  \DataOut_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N26), .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(DataOut[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N21) );
  \**SEQGEN**  \DataOut_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N25), .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(DataOut[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N21) );
  \**SEQGEN**  \DataOut_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N24), .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(DataOut[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N21) );
  \**SEQGEN**  \DataOut_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N23), .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(DataOut[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N21) );
  \**SEQGEN**  \DataOut_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N22), .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(DataOut[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N21) );
  \**SEQGEN**  tx_ready_reg ( .clear(1'b0), .preset(1'b0), .next_state(TxReady), .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(tx_ready), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  drive_k_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        drive_k), .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(
        drive_k_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  TxValid_reg ( .clear(1'b0), .preset(1'b0), .next_state(N35), 
        .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(TxValid), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  usbf_utmi_ls u0 ( .clk(phy_clk), .rst(rst), .resume_req(resume_req), 
        .rx_active(rx_active), .tx_ready(tx_ready), .drive_k(drive_k), 
        .XcvSelect(XcvSelect), .TermSel(TermSel), .SuspendM(SuspendM), 
        .LineState(LineState), .OpMode(OpMode), .usb_vbus(usb_vbus), .mode_hs(
        mode_hs), .usb_reset(usb_reset), .usb_suspend(usb_suspend), 
        .usb_attached(usb_attached), .suspend_clr(suspend_clr) );
  SELECT_OP C59 ( .DATA1(1'b0), .DATA2(RxValid), .CONTROL1(N0), .CONTROL2(N1), 
        .Z(N11) );
  GTECH_BUF B_0 ( .A(N9), .Z(N0) );
  GTECH_BUF B_1 ( .A(N10), .Z(N1) );
  SELECT_OP C60 ( .DATA1(1'b0), .DATA2(RxActive), .CONTROL1(N2), .CONTROL2(N3), 
        .Z(N14) );
  GTECH_BUF B_2 ( .A(N12), .Z(N2) );
  GTECH_BUF B_3 ( .A(N13), .Z(N3) );
  SELECT_OP C61 ( .DATA1(1'b0), .DATA2(RxError), .CONTROL1(N4), .CONTROL2(N5), 
        .Z(N17) );
  GTECH_BUF B_4 ( .A(N15), .Z(N4) );
  GTECH_BUF B_5 ( .A(N16), .Z(N5) );
  SELECT_OP C62 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N6), 
        .CONTROL2(N31), .CONTROL3(N20), .Z(N21) );
  GTECH_BUF B_6 ( .A(N18), .Z(N6) );
  SELECT_OP C63 ( .DATA1(tx_data), .DATA2({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0}), .CONTROL1(N6), .CONTROL2(N31), .Z({N29, N28, N27, N26, 
        N25, N24, N23, N22}) );
  SELECT_OP C64 ( .DATA1(1'b0), .DATA2(N34), .CONTROL1(N7), .CONTROL2(N8), .Z(
        N35) );
  GTECH_BUF B_7 ( .A(N32), .Z(N7) );
  GTECH_BUF B_8 ( .A(N33), .Z(N8) );
  GTECH_NOT I_0 ( .A(rst), .Z(N9) );
  GTECH_BUF B_9 ( .A(rst), .Z(N10) );
  GTECH_NOT I_1 ( .A(rst), .Z(N12) );
  GTECH_BUF B_10 ( .A(rst), .Z(N13) );
  GTECH_NOT I_2 ( .A(rst), .Z(N15) );
  GTECH_BUF B_11 ( .A(rst), .Z(N16) );
  GTECH_OR2 C76 ( .A(TxReady), .B(tx_first), .Z(N18) );
  GTECH_OR2 C79 ( .A(drive_k), .B(N18), .Z(N19) );
  GTECH_NOT I_3 ( .A(N19), .Z(N20) );
  GTECH_NOT I_4 ( .A(N18), .Z(N30) );
  GTECH_AND2 C82 ( .A(drive_k), .B(N30), .Z(N31) );
  GTECH_NOT I_5 ( .A(rst), .Z(N32) );
  GTECH_BUF B_12 ( .A(rst), .Z(N33) );
  GTECH_OR2 C86 ( .A(N37), .B(N40), .Z(N34) );
  GTECH_OR2 C87 ( .A(N36), .B(tx_valid_last), .Z(N37) );
  GTECH_OR2 C88 ( .A(tx_valid), .B(drive_k), .Z(N36) );
  GTECH_AND2 C89 ( .A(TxValid), .B(N39), .Z(N40) );
  GTECH_NOT I_6 ( .A(N38), .Z(N39) );
  GTECH_OR2 C91 ( .A(TxReady), .B(drive_k_r), .Z(N38) );
endmodule


module usbf_crc5 ( crc_in, din, crc_out );
  input [4:0] crc_in;
  input [10:0] din;
  output [4:0] crc_out;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15,
         N16, N17, N18, N19, N20, N21, N22, N23, N24, N25, N26, N27, N28, N29,
         N30, N31, N32, N33, N34, N35, N36, N37, N38, N39;

  GTECH_XOR2 C11 ( .A(N6), .B(crc_in[4]), .Z(crc_out[0]) );
  GTECH_XOR2 C12 ( .A(N5), .B(crc_in[3]), .Z(N6) );
  GTECH_XOR2 C13 ( .A(N4), .B(crc_in[0]), .Z(N5) );
  GTECH_XOR2 C14 ( .A(N3), .B(din[0]), .Z(N4) );
  GTECH_XOR2 C15 ( .A(N2), .B(din[3]), .Z(N3) );
  GTECH_XOR2 C16 ( .A(N1), .B(din[5]), .Z(N2) );
  GTECH_XOR2 C17 ( .A(N0), .B(din[6]), .Z(N1) );
  GTECH_XOR2 C18 ( .A(din[10]), .B(din[9]), .Z(N0) );
  GTECH_XOR2 C19 ( .A(N12), .B(crc_in[4]), .Z(crc_out[1]) );
  GTECH_XOR2 C20 ( .A(N11), .B(crc_in[1]), .Z(N12) );
  GTECH_XOR2 C21 ( .A(N10), .B(crc_in[0]), .Z(N11) );
  GTECH_XOR2 C22 ( .A(N9), .B(din[1]), .Z(N10) );
  GTECH_XOR2 C23 ( .A(N8), .B(din[4]), .Z(N9) );
  GTECH_XOR2 C24 ( .A(N7), .B(din[6]), .Z(N8) );
  GTECH_XOR2 C25 ( .A(din[10]), .B(din[7]), .Z(N7) );
  GTECH_XOR2 C26 ( .A(N23), .B(crc_in[4]), .Z(crc_out[2]) );
  GTECH_XOR2 C27 ( .A(N22), .B(crc_in[3]), .Z(N23) );
  GTECH_XOR2 C28 ( .A(N21), .B(crc_in[2]), .Z(N22) );
  GTECH_XOR2 C29 ( .A(N20), .B(crc_in[1]), .Z(N21) );
  GTECH_XOR2 C30 ( .A(N19), .B(crc_in[0]), .Z(N20) );
  GTECH_XOR2 C31 ( .A(N18), .B(din[0]), .Z(N19) );
  GTECH_XOR2 C32 ( .A(N17), .B(din[2]), .Z(N18) );
  GTECH_XOR2 C33 ( .A(N16), .B(din[3]), .Z(N17) );
  GTECH_XOR2 C34 ( .A(N15), .B(din[6]), .Z(N16) );
  GTECH_XOR2 C35 ( .A(N14), .B(din[7]), .Z(N15) );
  GTECH_XOR2 C36 ( .A(N13), .B(din[8]), .Z(N14) );
  GTECH_XOR2 C37 ( .A(din[10]), .B(din[9]), .Z(N13) );
  GTECH_XOR2 C38 ( .A(N32), .B(crc_in[4]), .Z(crc_out[3]) );
  GTECH_XOR2 C39 ( .A(N31), .B(crc_in[3]), .Z(N32) );
  GTECH_XOR2 C40 ( .A(N30), .B(crc_in[2]), .Z(N31) );
  GTECH_XOR2 C41 ( .A(N29), .B(crc_in[1]), .Z(N30) );
  GTECH_XOR2 C42 ( .A(N28), .B(din[1]), .Z(N29) );
  GTECH_XOR2 C43 ( .A(N27), .B(din[3]), .Z(N28) );
  GTECH_XOR2 C44 ( .A(N26), .B(din[4]), .Z(N27) );
  GTECH_XOR2 C45 ( .A(N25), .B(din[7]), .Z(N26) );
  GTECH_XOR2 C46 ( .A(N24), .B(din[8]), .Z(N25) );
  GTECH_XOR2 C47 ( .A(din[10]), .B(din[9]), .Z(N24) );
  GTECH_XOR2 C48 ( .A(N39), .B(crc_in[4]), .Z(crc_out[4]) );
  GTECH_XOR2 C49 ( .A(N38), .B(crc_in[3]), .Z(N39) );
  GTECH_XOR2 C50 ( .A(N37), .B(crc_in[2]), .Z(N38) );
  GTECH_XOR2 C51 ( .A(N36), .B(din[2]), .Z(N37) );
  GTECH_XOR2 C52 ( .A(N35), .B(din[4]), .Z(N36) );
  GTECH_XOR2 C53 ( .A(N34), .B(din[5]), .Z(N35) );
  GTECH_XOR2 C54 ( .A(N33), .B(din[8]), .Z(N34) );
  GTECH_XOR2 C55 ( .A(din[10]), .B(din[9]), .Z(N33) );
endmodule


module usbf_crc16 ( crc_in, din, crc_out );
  input [15:0] crc_in;
  input [7:0] din;
  output [15:0] crc_out;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15,
         N16, N17, N18, N19, N20, N21, N22, N23, N24, N25, N26, N27, N28, N29,
         N30, N31, N32, N33, N34, N35, N36, N37, N38, N39, N40, N41, N42, N43,
         N44, N45, N46, N47, N48, N49, N50, N51, N52, N53, N54, N55, N56;
  assign crc_out[14] = crc_in[6];
  assign crc_out[13] = crc_in[5];
  assign crc_out[12] = crc_in[4];
  assign crc_out[11] = crc_in[3];
  assign crc_out[10] = crc_in[2];

  GTECH_XOR2 C17 ( .A(N13), .B(crc_in[15]), .Z(crc_out[0]) );
  GTECH_XOR2 C18 ( .A(N12), .B(crc_in[14]), .Z(N13) );
  GTECH_XOR2 C19 ( .A(N11), .B(crc_in[13]), .Z(N12) );
  GTECH_XOR2 C20 ( .A(N10), .B(crc_in[12]), .Z(N11) );
  GTECH_XOR2 C21 ( .A(N9), .B(crc_in[11]), .Z(N10) );
  GTECH_XOR2 C22 ( .A(N8), .B(crc_in[10]), .Z(N9) );
  GTECH_XOR2 C23 ( .A(N7), .B(crc_in[9]), .Z(N8) );
  GTECH_XOR2 C24 ( .A(N6), .B(crc_in[8]), .Z(N7) );
  GTECH_XOR2 C25 ( .A(N5), .B(din[0]), .Z(N6) );
  GTECH_XOR2 C26 ( .A(N4), .B(din[1]), .Z(N5) );
  GTECH_XOR2 C27 ( .A(N3), .B(din[2]), .Z(N4) );
  GTECH_XOR2 C28 ( .A(N2), .B(din[3]), .Z(N3) );
  GTECH_XOR2 C29 ( .A(N1), .B(din[4]), .Z(N2) );
  GTECH_XOR2 C30 ( .A(N0), .B(din[5]), .Z(N1) );
  GTECH_XOR2 C31 ( .A(din[7]), .B(din[6]), .Z(N0) );
  GTECH_XOR2 C32 ( .A(N25), .B(crc_in[15]), .Z(crc_out[1]) );
  GTECH_XOR2 C33 ( .A(N24), .B(crc_in[14]), .Z(N25) );
  GTECH_XOR2 C34 ( .A(N23), .B(crc_in[13]), .Z(N24) );
  GTECH_XOR2 C35 ( .A(N22), .B(crc_in[12]), .Z(N23) );
  GTECH_XOR2 C36 ( .A(N21), .B(crc_in[11]), .Z(N22) );
  GTECH_XOR2 C37 ( .A(N20), .B(crc_in[10]), .Z(N21) );
  GTECH_XOR2 C38 ( .A(N19), .B(crc_in[9]), .Z(N20) );
  GTECH_XOR2 C39 ( .A(N18), .B(din[1]), .Z(N19) );
  GTECH_XOR2 C40 ( .A(N17), .B(din[2]), .Z(N18) );
  GTECH_XOR2 C41 ( .A(N16), .B(din[3]), .Z(N17) );
  GTECH_XOR2 C42 ( .A(N15), .B(din[4]), .Z(N16) );
  GTECH_XOR2 C43 ( .A(N14), .B(din[5]), .Z(N15) );
  GTECH_XOR2 C44 ( .A(din[7]), .B(din[6]), .Z(N14) );
  GTECH_XOR2 C45 ( .A(N27), .B(crc_in[9]), .Z(crc_out[2]) );
  GTECH_XOR2 C46 ( .A(N26), .B(crc_in[8]), .Z(N27) );
  GTECH_XOR2 C47 ( .A(din[1]), .B(din[0]), .Z(N26) );
  GTECH_XOR2 C48 ( .A(N29), .B(crc_in[10]), .Z(crc_out[3]) );
  GTECH_XOR2 C49 ( .A(N28), .B(crc_in[9]), .Z(N29) );
  GTECH_XOR2 C50 ( .A(din[2]), .B(din[1]), .Z(N28) );
  GTECH_XOR2 C51 ( .A(N31), .B(crc_in[11]), .Z(crc_out[4]) );
  GTECH_XOR2 C52 ( .A(N30), .B(crc_in[10]), .Z(N31) );
  GTECH_XOR2 C53 ( .A(din[3]), .B(din[2]), .Z(N30) );
  GTECH_XOR2 C54 ( .A(N33), .B(crc_in[12]), .Z(crc_out[5]) );
  GTECH_XOR2 C55 ( .A(N32), .B(crc_in[11]), .Z(N33) );
  GTECH_XOR2 C56 ( .A(din[4]), .B(din[3]), .Z(N32) );
  GTECH_XOR2 C57 ( .A(N35), .B(crc_in[13]), .Z(crc_out[6]) );
  GTECH_XOR2 C58 ( .A(N34), .B(crc_in[12]), .Z(N35) );
  GTECH_XOR2 C59 ( .A(din[5]), .B(din[4]), .Z(N34) );
  GTECH_XOR2 C60 ( .A(N37), .B(crc_in[14]), .Z(crc_out[7]) );
  GTECH_XOR2 C61 ( .A(N36), .B(crc_in[13]), .Z(N37) );
  GTECH_XOR2 C62 ( .A(din[6]), .B(din[5]), .Z(N36) );
  GTECH_XOR2 C63 ( .A(N40), .B(crc_in[15]), .Z(crc_out[8]) );
  GTECH_XOR2 C64 ( .A(N39), .B(crc_in[14]), .Z(N40) );
  GTECH_XOR2 C65 ( .A(N38), .B(crc_in[0]), .Z(N39) );
  GTECH_XOR2 C66 ( .A(din[7]), .B(din[6]), .Z(N38) );
  GTECH_XOR2 C67 ( .A(N41), .B(crc_in[15]), .Z(crc_out[9]) );
  GTECH_XOR2 C68 ( .A(din[7]), .B(crc_in[1]), .Z(N41) );
  GTECH_XOR2 C69 ( .A(N56), .B(crc_in[15]), .Z(crc_out[15]) );
  GTECH_XOR2 C70 ( .A(N55), .B(crc_in[14]), .Z(N56) );
  GTECH_XOR2 C71 ( .A(N54), .B(crc_in[13]), .Z(N55) );
  GTECH_XOR2 C72 ( .A(N53), .B(crc_in[12]), .Z(N54) );
  GTECH_XOR2 C73 ( .A(N52), .B(crc_in[11]), .Z(N53) );
  GTECH_XOR2 C74 ( .A(N51), .B(crc_in[10]), .Z(N52) );
  GTECH_XOR2 C75 ( .A(N50), .B(crc_in[9]), .Z(N51) );
  GTECH_XOR2 C76 ( .A(N49), .B(crc_in[8]), .Z(N50) );
  GTECH_XOR2 C77 ( .A(N48), .B(crc_in[7]), .Z(N49) );
  GTECH_XOR2 C78 ( .A(N47), .B(din[0]), .Z(N48) );
  GTECH_XOR2 C79 ( .A(N46), .B(din[1]), .Z(N47) );
  GTECH_XOR2 C80 ( .A(N45), .B(din[2]), .Z(N46) );
  GTECH_XOR2 C81 ( .A(N44), .B(din[3]), .Z(N45) );
  GTECH_XOR2 C82 ( .A(N43), .B(din[4]), .Z(N44) );
  GTECH_XOR2 C83 ( .A(N42), .B(din[5]), .Z(N43) );
  GTECH_XOR2 C84 ( .A(din[7]), .B(din[6]), .Z(N42) );
endmodule


module usbf_pd ( clk, rst, rx_data, rx_valid, rx_active, rx_err, pid_OUT, 
        pid_IN, pid_SOF, pid_SETUP, pid_DATA0, pid_DATA1, pid_DATA2, pid_MDATA, 
        pid_ACK, pid_NACK, pid_STALL, pid_NYET, pid_PRE, pid_ERR, pid_SPLIT, 
        pid_PING, pid_cks_err, token_fadr, token_endp, token_valid, crc5_err, 
        frame_no, rx_data_st, rx_data_valid, rx_data_done, crc16_err, seq_err
 );
  input [7:0] rx_data;
  output [6:0] token_fadr;
  output [3:0] token_endp;
  output [10:0] frame_no;
  output [7:0] rx_data_st;
  input clk, rst, rx_valid, rx_active, rx_err;
  output pid_OUT, pid_IN, pid_SOF, pid_SETUP, pid_DATA0, pid_DATA1, pid_DATA2,
         pid_MDATA, pid_ACK, pid_NACK, pid_STALL, pid_NYET, pid_PRE, pid_ERR,
         pid_SPLIT, pid_PING, pid_cks_err, token_valid, crc5_err,
         rx_data_valid, rx_data_done, crc16_err, seq_err;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, N8, N9, N10, N11, N12, pid_le_sm,
         pid_ld_en, N13, N14, N15, N16, N17, N18, N19, N20, N21, N22, N23, N24,
         N25, N26, N27, N28, pid_TOKEN, pid_DATA, token_le_1, token_le_2,
         token_valid_r1, got_pid_ack, N29, N30, data_valid_d, N31, N32, N33,
         rxv1, N34, N35, N36, N37, N38, N39, N40, N41, N42, N43, rxv2, N44,
         N45, N46, N47, N48, N49, N50, rx_active_r, crc16_clr, N51, N52, N53,
         N54, N55, N56, N57, N58, N59, N60, N61, N62, N63, N64, N65, N66, N67,
         N68, N69, N70, N71, N72, N73, N74, N75, N76, N77, N78, N79, N80, N81,
         N82, N83, N84, N85, N86, N87, N88, N89, N90, N91, N92, N93, N94, N95,
         N96, N97, N98, N99, N100, N101, N102, N103, N104, N105, N106, N107,
         N108, N109, N110, N111, N112, N113, N114, N115, N116, N117, N118,
         N119, N120, N121, N122, N123, N124, N125, N126, N127, N128, N129,
         N130, N131, N132, N133, N134, N135, N136, N137, N138, N139, N140,
         N141, N142, N143, N144, N145, N146, N147, N148, N149, N150, N151,
         N152, N153, N154, N155, N156, N157, N158, N159, N160, N161, N162,
         N163, N164, N165, N166, N167, N168, N169, N170, N171, N172, N173,
         N174, N175, N176, N177, N178, N179, N180, N181, N182, N183, N184,
         N185, N186, N187, N188, N189, N190, N191, N192, N193, N194, N195,
         N196, N197, N198, N199, N200, N201, N202, N203, N204, N205, N206,
         N207, N208, N209, N210, N211, N212, N213, N214, N215, N216, N217,
         N218, N219, N220, N221, N222, N223, N224, N225, N226, N227, N228,
         N229, N230, N231, N232, N233, N234, N235, N236, N237, N238, N239,
         N240, N241, N242, N243, N244, N245, N246, N247, N248, N249, N250,
         N251, N252, N253, N254, N255, N256, N257, N258, N259, N260, N261,
         N262, N263;
  wire   [7:0] pid;
  wire   [7:3] token1;
  wire   [4:0] crc5_out2;
  wire   [4:0] crc5_out;
  wire   [7:0] d0;
  wire   [7:0] d1;
  wire   [15:0] crc16_sum;
  wire   [15:0] crc16_out;
  wire   [3:0] state;
  wire   [3:0] next_state;
  assign token_endp[3] = frame_no[10];
  assign token_endp[2] = frame_no[9];
  assign token_endp[1] = frame_no[8];
  assign token_endp[0] = frame_no[7];
  assign token_fadr[6] = frame_no[6];
  assign token_fadr[5] = frame_no[5];
  assign token_fadr[4] = frame_no[4];
  assign token_fadr[3] = frame_no[3];
  assign token_fadr[2] = frame_no[2];
  assign token_fadr[1] = frame_no[1];
  assign token_fadr[0] = frame_no[0];
  assign pid_NACK = N165;
  assign pid_STALL = N170;
  assign pid_NYET = N174;
  assign pid_PRE = N178;
  assign pid_ERR = N182;
  assign pid_SPLIT = N186;
  assign pid_DATA0 = N210;
  assign pid_DATA1 = N214;
  assign pid_DATA2 = N218;
  assign pid_MDATA = N221;
  assign pid_ACK = N225;
  assign pid_OUT = N229;
  assign pid_IN = N233;
  assign pid_SOF = N237;
  assign pid_SETUP = N241;
  assign pid_PING = N245;

  \**SEQGEN**  \pid_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N23), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(pid[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N15) );
  \**SEQGEN**  \pid_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N22), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(pid[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N15) );
  \**SEQGEN**  \pid_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N21), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(pid[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N15) );
  \**SEQGEN**  \pid_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N20), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(pid[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N15) );
  \**SEQGEN**  \pid_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N19), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(pid[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N15) );
  \**SEQGEN**  \pid_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N18), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(pid[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N15) );
  \**SEQGEN**  \pid_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N17), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(pid[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N15) );
  \**SEQGEN**  \pid_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N16), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(pid[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N15) );
  NE_UNS_OP ne_2357 ( .A(pid[3:0]), .B({N25, N26, N27, N28}), .Z(pid_cks_err)
         );
  \**SEQGEN**  \token0_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[7]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        frame_no[7]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(token_le_1) );
  \**SEQGEN**  \token0_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[6]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        frame_no[6]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(token_le_1) );
  \**SEQGEN**  \token0_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[5]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        frame_no[5]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(token_le_1) );
  \**SEQGEN**  \token0_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[4]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        frame_no[4]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(token_le_1) );
  \**SEQGEN**  \token0_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[3]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        frame_no[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(token_le_1) );
  \**SEQGEN**  \token0_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[2]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        frame_no[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(token_le_1) );
  \**SEQGEN**  \token0_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[1]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        frame_no[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(token_le_1) );
  \**SEQGEN**  \token0_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[0]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        frame_no[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(token_le_1) );
  \**SEQGEN**  \token1_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[7]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        token1[7]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(token_le_2) );
  \**SEQGEN**  \token1_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[6]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        token1[6]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(token_le_2) );
  \**SEQGEN**  \token1_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[5]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        token1[5]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(token_le_2) );
  \**SEQGEN**  \token1_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[4]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        token1[4]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(token_le_2) );
  \**SEQGEN**  \token1_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[3]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        token1[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(token_le_2) );
  \**SEQGEN**  \token1_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[2]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        frame_no[10]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(token_le_2) );
  \**SEQGEN**  \token1_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[1]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        frame_no[9]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(token_le_2) );
  \**SEQGEN**  \token1_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[0]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        frame_no[8]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(token_le_2) );
  \**SEQGEN**  token_valid_r1_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        token_le_2), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        token_valid_r1), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  token_valid_str1_reg ( .clear(1'b0), .preset(1'b0), 
        .next_state(N29), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        token_valid), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  NE_UNS_OP ne_2397 ( .A(crc5_out2), .B(token1), .Z(N30) );
  usbf_crc5 u0 ( .crc_in({1'b1, 1'b1, 1'b1, 1'b1, 1'b1}), .din({frame_no[0], 
        frame_no[1], frame_no[2], frame_no[3], frame_no[4], frame_no[5], 
        frame_no[6], frame_no[7], frame_no[8], frame_no[9], frame_no[10]}), 
        .crc_out(crc5_out) );
  \**SEQGEN**  rxv1_reg ( .clear(1'b0), .preset(1'b0), .next_state(N35), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(rxv1), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N34) );
  \**SEQGEN**  rxv2_reg ( .clear(1'b0), .preset(1'b0), .next_state(N45), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(rxv2), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N44) );
  \**SEQGEN**  data_valid0_reg ( .clear(1'b0), .preset(1'b0), .next_state(N50), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(rx_data_valid), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \d0_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[7]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(d0[7]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        data_valid_d) );
  \**SEQGEN**  \d0_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[6]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(d0[6]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        data_valid_d) );
  \**SEQGEN**  \d0_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[5]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(d0[5]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        data_valid_d) );
  \**SEQGEN**  \d0_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[4]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(d0[4]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        data_valid_d) );
  \**SEQGEN**  \d0_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[3]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(d0[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        data_valid_d) );
  \**SEQGEN**  \d0_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[2]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(d0[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        data_valid_d) );
  \**SEQGEN**  \d0_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[1]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(d0[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        data_valid_d) );
  \**SEQGEN**  \d0_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data[0]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(d0[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        data_valid_d) );
  \**SEQGEN**  \d1_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(d0[7]), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(d1[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(data_valid_d) );
  \**SEQGEN**  \d1_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(d0[6]), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(d1[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(data_valid_d) );
  \**SEQGEN**  \d1_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(d0[5]), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(d1[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(data_valid_d) );
  \**SEQGEN**  \d1_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(d0[4]), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(d1[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(data_valid_d) );
  \**SEQGEN**  \d1_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(d0[3]), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(d1[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(data_valid_d) );
  \**SEQGEN**  \d1_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(d0[2]), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(d1[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(data_valid_d) );
  \**SEQGEN**  \d1_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(d0[1]), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(d1[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(data_valid_d) );
  \**SEQGEN**  \d1_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(d0[0]), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(d1[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(data_valid_d) );
  \**SEQGEN**  \d2_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(d1[7]), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(rx_data_st[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(data_valid_d) );
  \**SEQGEN**  \d2_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(d1[6]), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(rx_data_st[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(data_valid_d) );
  \**SEQGEN**  \d2_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(d1[5]), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(rx_data_st[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(data_valid_d) );
  \**SEQGEN**  \d2_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(d1[4]), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(rx_data_st[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(data_valid_d) );
  \**SEQGEN**  \d2_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(d1[3]), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(rx_data_st[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(data_valid_d) );
  \**SEQGEN**  \d2_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(d1[2]), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(rx_data_st[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(data_valid_d) );
  \**SEQGEN**  \d2_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(d1[1]), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(rx_data_st[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(data_valid_d) );
  \**SEQGEN**  \d2_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(d1[0]), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(rx_data_st[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(data_valid_d) );
  \**SEQGEN**  rx_active_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_active), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rx_active_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \crc16_sum_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N69), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        crc16_sum[15]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N53) );
  \**SEQGEN**  \crc16_sum_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N68), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        crc16_sum[14]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N53) );
  \**SEQGEN**  \crc16_sum_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N67), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        crc16_sum[13]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N53) );
  \**SEQGEN**  \crc16_sum_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N66), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        crc16_sum[12]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N53) );
  \**SEQGEN**  \crc16_sum_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N65), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        crc16_sum[11]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N53) );
  \**SEQGEN**  \crc16_sum_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N64), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        crc16_sum[10]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N53) );
  \**SEQGEN**  \crc16_sum_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N63), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16_sum[9]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        N53) );
  \**SEQGEN**  \crc16_sum_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N62), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16_sum[8]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        N53) );
  \**SEQGEN**  \crc16_sum_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N61), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16_sum[7]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        N53) );
  \**SEQGEN**  \crc16_sum_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N60), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16_sum[6]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        N53) );
  \**SEQGEN**  \crc16_sum_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N59), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16_sum[5]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        N53) );
  \**SEQGEN**  \crc16_sum_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N58), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16_sum[4]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        N53) );
  \**SEQGEN**  \crc16_sum_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N57), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16_sum[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        N53) );
  \**SEQGEN**  \crc16_sum_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N56), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16_sum[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        N53) );
  \**SEQGEN**  \crc16_sum_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N55), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16_sum[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        N53) );
  \**SEQGEN**  \crc16_sum_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N54), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16_sum[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        N53) );
  usbf_crc16 u1 ( .crc_in(crc16_sum), .din({rx_data[0], rx_data[1], rx_data[2], 
        rx_data[3], rx_data[4], rx_data[5], rx_data[6], rx_data[7]}), 
        .crc_out(crc16_out) );
  \**SEQGEN**  \state_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N76), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N159) );
  \**SEQGEN**  \state_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N75), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N159) );
  \**SEQGEN**  \state_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N74), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N159) );
  \**SEQGEN**  \state_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N73), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N159) );
  GTECH_OR2 C252 ( .A(state[3]), .B(state[2]), .Z(N78) );
  GTECH_OR2 C253 ( .A(state[1]), .B(N77), .Z(N79) );
  GTECH_OR2 C254 ( .A(N78), .B(N79), .Z(N80) );
  GTECH_OR2 C257 ( .A(state[3]), .B(state[2]), .Z(N83) );
  GTECH_OR2 C258 ( .A(N82), .B(state[0]), .Z(N84) );
  GTECH_OR2 C259 ( .A(N83), .B(N84), .Z(N85) );
  GTECH_OR2 C262 ( .A(state[3]), .B(N87), .Z(N88) );
  GTECH_OR2 C263 ( .A(state[1]), .B(state[0]), .Z(N89) );
  GTECH_OR2 C264 ( .A(N88), .B(N89), .Z(N90) );
  GTECH_OR2 C267 ( .A(N92), .B(state[2]), .Z(N93) );
  GTECH_OR2 C268 ( .A(state[1]), .B(state[0]), .Z(N94) );
  GTECH_OR2 C269 ( .A(N93), .B(N94), .Z(N95) );
  GTECH_NOT I_0 ( .A(pid[3]), .Z(N160) );
  GTECH_NOT I_1 ( .A(pid[1]), .Z(N161) );
  GTECH_OR2 C503 ( .A(pid[2]), .B(N160), .Z(N162) );
  GTECH_OR2 C504 ( .A(N161), .B(N162), .Z(N163) );
  GTECH_OR2 C505 ( .A(pid[0]), .B(N163), .Z(N164) );
  GTECH_NOT I_2 ( .A(N164), .Z(N165) );
  GTECH_NOT I_3 ( .A(pid[2]), .Z(N166) );
  GTECH_OR2 C510 ( .A(N166), .B(N160), .Z(N167) );
  GTECH_OR2 C511 ( .A(N161), .B(N167), .Z(N168) );
  GTECH_OR2 C512 ( .A(pid[0]), .B(N168), .Z(N169) );
  GTECH_NOT I_4 ( .A(N169), .Z(N170) );
  GTECH_OR2 C516 ( .A(N166), .B(pid[3]), .Z(N171) );
  GTECH_OR2 C517 ( .A(N161), .B(N171), .Z(N172) );
  GTECH_OR2 C518 ( .A(pid[0]), .B(N172), .Z(N173) );
  GTECH_NOT I_5 ( .A(N173), .Z(N174) );
  GTECH_OR2 C522 ( .A(N166), .B(N160), .Z(N175) );
  GTECH_OR2 C523 ( .A(pid[1]), .B(N175), .Z(N176) );
  GTECH_OR2 C524 ( .A(pid[0]), .B(N176), .Z(N177) );
  GTECH_NOT I_6 ( .A(N177), .Z(N178) );
  GTECH_OR2 C528 ( .A(N166), .B(N160), .Z(N179) );
  GTECH_OR2 C529 ( .A(pid[1]), .B(N179), .Z(N180) );
  GTECH_OR2 C530 ( .A(pid[0]), .B(N180), .Z(N181) );
  GTECH_NOT I_7 ( .A(N181), .Z(N182) );
  GTECH_OR2 C533 ( .A(pid[2]), .B(N160), .Z(N183) );
  GTECH_OR2 C534 ( .A(pid[1]), .B(N183), .Z(N184) );
  GTECH_OR2 C535 ( .A(pid[0]), .B(N184), .Z(N185) );
  GTECH_NOT I_8 ( .A(N185), .Z(N186) );
  GTECH_NOT I_9 ( .A(crc16_sum[15]), .Z(N187) );
  GTECH_NOT I_10 ( .A(crc16_sum[3]), .Z(N188) );
  GTECH_NOT I_11 ( .A(crc16_sum[2]), .Z(N189) );
  GTECH_NOT I_12 ( .A(crc16_sum[0]), .Z(N190) );
  GTECH_OR2 C541 ( .A(crc16_sum[14]), .B(N187), .Z(N191) );
  GTECH_OR2 C542 ( .A(crc16_sum[13]), .B(N191), .Z(N192) );
  GTECH_OR2 C543 ( .A(crc16_sum[12]), .B(N192), .Z(N193) );
  GTECH_OR2 C544 ( .A(crc16_sum[11]), .B(N193), .Z(N194) );
  GTECH_OR2 C545 ( .A(crc16_sum[10]), .B(N194), .Z(N195) );
  GTECH_OR2 C546 ( .A(crc16_sum[9]), .B(N195), .Z(N196) );
  GTECH_OR2 C547 ( .A(crc16_sum[8]), .B(N196), .Z(N197) );
  GTECH_OR2 C548 ( .A(crc16_sum[7]), .B(N197), .Z(N198) );
  GTECH_OR2 C549 ( .A(crc16_sum[6]), .B(N198), .Z(N199) );
  GTECH_OR2 C550 ( .A(crc16_sum[5]), .B(N199), .Z(N200) );
  GTECH_OR2 C551 ( .A(crc16_sum[4]), .B(N200), .Z(N201) );
  GTECH_OR2 C552 ( .A(N188), .B(N201), .Z(N202) );
  GTECH_OR2 C553 ( .A(N189), .B(N202), .Z(N203) );
  GTECH_OR2 C554 ( .A(crc16_sum[1]), .B(N203), .Z(N204) );
  GTECH_OR2 C555 ( .A(N190), .B(N204), .Z(N205) );
  GTECH_NOT I_13 ( .A(pid[0]), .Z(N206) );
  GTECH_OR2 C560 ( .A(pid[2]), .B(pid[3]), .Z(N207) );
  GTECH_OR2 C561 ( .A(N161), .B(N207), .Z(N208) );
  GTECH_OR2 C562 ( .A(N206), .B(N208), .Z(N209) );
  GTECH_NOT I_14 ( .A(N209), .Z(N210) );
  GTECH_OR2 C567 ( .A(pid[2]), .B(N160), .Z(N211) );
  GTECH_OR2 C568 ( .A(N161), .B(N211), .Z(N212) );
  GTECH_OR2 C569 ( .A(N206), .B(N212), .Z(N213) );
  GTECH_NOT I_15 ( .A(N213), .Z(N214) );
  GTECH_OR2 C574 ( .A(N166), .B(pid[3]), .Z(N215) );
  GTECH_OR2 C575 ( .A(N161), .B(N215), .Z(N216) );
  GTECH_OR2 C576 ( .A(N206), .B(N216), .Z(N217) );
  GTECH_NOT I_16 ( .A(N217), .Z(N218) );
  GTECH_AND2 C578 ( .A(pid[2]), .B(pid[3]), .Z(N219) );
  GTECH_AND2 C579 ( .A(pid[1]), .B(N219), .Z(N220) );
  GTECH_AND2 C580 ( .A(pid[0]), .B(N220), .Z(N221) );
  GTECH_OR2 C582 ( .A(pid[2]), .B(pid[3]), .Z(N222) );
  GTECH_OR2 C583 ( .A(N161), .B(N222), .Z(N223) );
  GTECH_OR2 C584 ( .A(pid[0]), .B(N223), .Z(N224) );
  GTECH_NOT I_17 ( .A(N224), .Z(N225) );
  GTECH_OR2 C587 ( .A(pid[2]), .B(pid[3]), .Z(N226) );
  GTECH_OR2 C588 ( .A(pid[1]), .B(N226), .Z(N227) );
  GTECH_OR2 C589 ( .A(N206), .B(N227), .Z(N228) );
  GTECH_NOT I_18 ( .A(N228), .Z(N229) );
  GTECH_OR2 C593 ( .A(pid[2]), .B(N160), .Z(N230) );
  GTECH_OR2 C594 ( .A(pid[1]), .B(N230), .Z(N231) );
  GTECH_OR2 C595 ( .A(N206), .B(N231), .Z(N232) );
  GTECH_NOT I_19 ( .A(N232), .Z(N233) );
  GTECH_OR2 C599 ( .A(N166), .B(pid[3]), .Z(N234) );
  GTECH_OR2 C600 ( .A(pid[1]), .B(N234), .Z(N235) );
  GTECH_OR2 C601 ( .A(N206), .B(N235), .Z(N236) );
  GTECH_NOT I_20 ( .A(N236), .Z(N237) );
  GTECH_OR2 C606 ( .A(N166), .B(N160), .Z(N238) );
  GTECH_OR2 C607 ( .A(pid[1]), .B(N238), .Z(N239) );
  GTECH_OR2 C608 ( .A(N206), .B(N239), .Z(N240) );
  GTECH_NOT I_21 ( .A(N240), .Z(N241) );
  GTECH_OR2 C611 ( .A(N166), .B(pid[3]), .Z(N242) );
  GTECH_OR2 C612 ( .A(pid[1]), .B(N242), .Z(N243) );
  GTECH_OR2 C613 ( .A(pid[0]), .B(N243), .Z(N244) );
  GTECH_NOT I_22 ( .A(N244), .Z(N245) );
  SELECT_OP C615 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N0), 
        .CONTROL2(N24), .CONTROL3(N14), .Z(N15) );
  GTECH_BUF B_0 ( .A(N72), .Z(N0) );
  SELECT_OP C616 ( .DATA1({1'b1, 1'b1, 1'b1, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0}), 
        .DATA2(rx_data), .CONTROL1(N0), .CONTROL2(N24), .Z({N23, N22, N21, N20, 
        N19, N18, N17, N16}) );
  SELECT_OP C617 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N0), .CONTROL2(N36), .CONTROL3(N39), .CONTROL4(N33), .Z(N34)
         );
  SELECT_OP C618 ( .DATA1(1'b0), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N0), 
        .CONTROL2(N36), .CONTROL3(N39), .Z(N35) );
  SELECT_OP C619 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N0), .CONTROL2(N46), .CONTROL3(N49), .CONTROL4(N43), .Z(N44)
         );
  SELECT_OP C620 ( .DATA1(1'b0), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N0), 
        .CONTROL2(N46), .CONTROL3(N49), .Z(N45) );
  SELECT_OP C621 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N1), 
        .CONTROL2(N71), .CONTROL3(N52), .Z(N53) );
  GTECH_BUF B_1 ( .A(crc16_clr), .Z(N1) );
  SELECT_OP C622 ( .DATA1({1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 
        1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1}), .DATA2(crc16_out), 
        .CONTROL1(N1), .CONTROL2(N71), .Z({N69, N68, N67, N66, N65, N64, N63, 
        N62, N61, N60, N59, N58, N57, N56, N55, N54}) );
  SELECT_OP C623 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b1}), .DATA2(next_state), 
        .CONTROL1(N0), .CONTROL2(N2), .Z({N76, N75, N74, N73}) );
  GTECH_BUF B_2 ( .A(rst), .Z(N2) );
  SELECT_OP C624 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b1}), .DATA2(state), .CONTROL1(
        N3), .CONTROL2(N4), .Z({N110, N109, N108, N107}) );
  GTECH_BUF B_3 ( .A(N258), .Z(N3) );
  GTECH_BUF B_4 ( .A(rx_active), .Z(N4) );
  SELECT_OP C625 ( .DATA1(1'b1), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .CONTROL1(N5), .CONTROL2(N111), .CONTROL3(N6), 
        .CONTROL4(N6), .CONTROL5(N6), .Z(N112) );
  GTECH_BUF B_5 ( .A(N99), .Z(N5) );
  GTECH_BUF B_6 ( .A(1'b0), .Z(N6) );
  SELECT_OP C626 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b1}), .DATA2({1'b0, 1'b1, 1'b0, 
        1'b0}), .DATA3({1'b1, 1'b0, 1'b0, 1'b0}), .DATA4({N110, N109, N108, 
        N107}), .DATA5(state), .CONTROL1(N5), .CONTROL2(N134), .CONTROL3(N137), 
        .CONTROL4(N140), .CONTROL5(N106), .Z({N116, N115, N114, N113}) );
  SELECT_OP C627 ( .DATA1(1'b0), .DATA2(1'b1), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .CONTROL1(N5), .CONTROL2(N134), .CONTROL3(N117), 
        .CONTROL4(N6), .CONTROL5(N6), .Z(N118) );
  SELECT_OP C628 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b1), .DATA4(1'b0), 
        .DATA5(1'b0), .CONTROL1(N5), .CONTROL2(N134), .CONTROL3(N137), 
        .CONTROL4(N119), .CONTROL5(N6), .Z(N120) );
  SELECT_OP C629 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(N253), 
        .DATA5(1'b0), .CONTROL1(N5), .CONTROL2(N134), .CONTROL3(N137), 
        .CONTROL4(N140), .CONTROL5(N106), .Z(N121) );
  SELECT_OP C630 ( .DATA1(1'b1), .DATA2(1'b0), .DATA3(1'b0), .CONTROL1(N7), 
        .CONTROL2(N125), .CONTROL3(N6), .Z(N126) );
  GTECH_BUF B_7 ( .A(N122), .Z(N7) );
  SELECT_OP C631 ( .DATA1(1'b0), .DATA2(N253), .DATA3(1'b0), .CONTROL1(N7), 
        .CONTROL2(N8), .CONTROL3(N124), .Z(N127) );
  GTECH_BUF B_8 ( .A(N128), .Z(N8) );
  SELECT_OP C632 ( .DATA1(1'b1), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .CONTROL1(N9), .CONTROL2(N10), .CONTROL3(N11), 
        .CONTROL4(N12), .CONTROL5(N133), .Z(pid_le_sm) );
  GTECH_BUF B_9 ( .A(N81), .Z(N9) );
  GTECH_BUF B_10 ( .A(N86), .Z(N10) );
  GTECH_BUF B_11 ( .A(N91), .Z(N11) );
  GTECH_BUF B_12 ( .A(N96), .Z(N12) );
  SELECT_OP C633 ( .DATA1({1'b0, 1'b0, 1'b1, 1'b0}), .DATA2({N116, N115, N114, 
        N113}), .DATA3({1'b0, 1'b0, 1'b0, 1'b1}), .DATA4({1'b0, 1'b0, 1'b0, 
        1'b1}), .DATA5(state), .CONTROL1(N9), .CONTROL2(N10), .CONTROL3(N11), 
        .CONTROL4(N12), .CONTROL5(N133), .Z(next_state) );
  SELECT_OP C634 ( .DATA1(1'b0), .DATA2(N121), .DATA3(N127), .DATA4(1'b0), 
        .DATA5(1'b0), .CONTROL1(N9), .CONTROL2(N10), .CONTROL3(N11), 
        .CONTROL4(N12), .CONTROL5(N133), .Z(seq_err) );
  SELECT_OP C635 ( .DATA1(1'b0), .DATA2(N112), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .CONTROL1(N9), .CONTROL2(N10), .CONTROL3(N11), 
        .CONTROL4(N12), .CONTROL5(N133), .Z(got_pid_ack) );
  SELECT_OP C636 ( .DATA1(1'b0), .DATA2(N118), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .CONTROL1(N9), .CONTROL2(N10), .CONTROL3(N11), 
        .CONTROL4(N12), .CONTROL5(N133), .Z(token_le_1) );
  SELECT_OP C637 ( .DATA1(1'b0), .DATA2(N120), .DATA3(1'b0), .DATA4(N122), 
        .DATA5(1'b0), .CONTROL1(N9), .CONTROL2(N10), .CONTROL3(N11), 
        .CONTROL4(N12), .CONTROL5(N133), .Z(data_valid_d) );
  SELECT_OP C638 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(N126), .DATA4(1'b0), 
        .DATA5(1'b0), .CONTROL1(N9), .CONTROL2(N10), .CONTROL3(N11), 
        .CONTROL4(N12), .CONTROL5(N133), .Z(token_le_2) );
  SELECT_OP C639 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(N128), 
        .DATA5(1'b0), .CONTROL1(N9), .CONTROL2(N10), .CONTROL3(N11), 
        .CONTROL4(N12), .CONTROL5(N133), .Z(rx_data_done) );
  GTECH_OR2 C642 ( .A(N246), .B(rx_valid), .Z(pid_ld_en) );
  GTECH_AND2 C643 ( .A(pid_le_sm), .B(rx_active), .Z(N246) );
  GTECH_OR2 C645 ( .A(pid_ld_en), .B(N72), .Z(N13) );
  GTECH_NOT I_23 ( .A(N13), .Z(N14) );
  GTECH_AND2 C648 ( .A(pid_ld_en), .B(rst), .Z(N24) );
  GTECH_NOT I_24 ( .A(pid[7]), .Z(N25) );
  GTECH_NOT I_25 ( .A(pid[6]), .Z(N26) );
  GTECH_NOT I_26 ( .A(pid[5]), .Z(N27) );
  GTECH_NOT I_27 ( .A(pid[4]), .Z(N28) );
  GTECH_OR2 C653 ( .A(N249), .B(N245), .Z(pid_TOKEN) );
  GTECH_OR2 C654 ( .A(N248), .B(N241), .Z(N249) );
  GTECH_OR2 C655 ( .A(N247), .B(N237), .Z(N248) );
  GTECH_OR2 C656 ( .A(N229), .B(N233), .Z(N247) );
  GTECH_OR2 C657 ( .A(N250), .B(N251), .Z(pid_DATA) );
  GTECH_OR2 C658 ( .A(N210), .B(N214), .Z(N250) );
  GTECH_AND2 C659 ( .A(N218), .B(N221), .Z(N251) );
  GTECH_OR2 C662 ( .A(token_valid_r1), .B(got_pid_ack), .Z(N29) );
  GTECH_AND2 C663 ( .A(token_valid), .B(N30), .Z(crc5_err) );
  GTECH_NOT I_28 ( .A(crc5_out[0]), .Z(crc5_out2[4]) );
  GTECH_NOT I_29 ( .A(crc5_out[1]), .Z(crc5_out2[3]) );
  GTECH_NOT I_30 ( .A(crc5_out[2]), .Z(crc5_out2[2]) );
  GTECH_NOT I_31 ( .A(crc5_out[3]), .Z(crc5_out2[1]) );
  GTECH_NOT I_32 ( .A(crc5_out[4]), .Z(crc5_out2[0]) );
  GTECH_OR2 C671 ( .A(data_valid_d), .B(N72), .Z(N31) );
  GTECH_OR2 C672 ( .A(rx_data_done), .B(N31), .Z(N32) );
  GTECH_NOT I_33 ( .A(N32), .Z(N33) );
  GTECH_AND2 C674 ( .A(data_valid_d), .B(rst), .Z(N36) );
  GTECH_NOT I_34 ( .A(data_valid_d), .Z(N37) );
  GTECH_AND2 C676 ( .A(rst), .B(N37), .Z(N38) );
  GTECH_AND2 C677 ( .A(rx_data_done), .B(N38), .Z(N39) );
  GTECH_AND2 C678 ( .A(rxv1), .B(data_valid_d), .Z(N40) );
  GTECH_OR2 C681 ( .A(N40), .B(N72), .Z(N41) );
  GTECH_OR2 C682 ( .A(rx_data_done), .B(N41), .Z(N42) );
  GTECH_NOT I_35 ( .A(N42), .Z(N43) );
  GTECH_AND2 C684 ( .A(N40), .B(rst), .Z(N46) );
  GTECH_NOT I_36 ( .A(N40), .Z(N47) );
  GTECH_AND2 C686 ( .A(rst), .B(N47), .Z(N48) );
  GTECH_AND2 C687 ( .A(rx_data_done), .B(N48), .Z(N49) );
  GTECH_AND2 C688 ( .A(rxv2), .B(data_valid_d), .Z(N50) );
  GTECH_AND2 C692 ( .A(rx_active), .B(N252), .Z(crc16_clr) );
  GTECH_NOT I_37 ( .A(rx_active_r), .Z(N252) );
  GTECH_OR2 C696 ( .A(data_valid_d), .B(crc16_clr), .Z(N51) );
  GTECH_NOT I_38 ( .A(N51), .Z(N52) );
  GTECH_NOT I_39 ( .A(crc16_clr), .Z(N70) );
  GTECH_AND2 C699 ( .A(data_valid_d), .B(N70), .Z(N71) );
  GTECH_AND2 C700 ( .A(rx_data_done), .B(N205), .Z(crc16_err) );
  GTECH_NOT I_40 ( .A(rst), .Z(N72) );
  GTECH_NOT I_41 ( .A(state[0]), .Z(N77) );
  GTECH_NOT I_42 ( .A(N80), .Z(N81) );
  GTECH_NOT I_43 ( .A(state[1]), .Z(N82) );
  GTECH_NOT I_44 ( .A(N85), .Z(N86) );
  GTECH_NOT I_45 ( .A(state[2]), .Z(N87) );
  GTECH_NOT I_46 ( .A(N90), .Z(N91) );
  GTECH_NOT I_47 ( .A(state[3]), .Z(N92) );
  GTECH_NOT I_48 ( .A(N95), .Z(N96) );
  GTECH_AND2 C716 ( .A(rx_valid), .B(rx_active), .Z(N97) );
  GTECH_NOT I_49 ( .A(N97), .Z(N98) );
  GTECH_AND2 C719 ( .A(N225), .B(N253), .Z(N99) );
  GTECH_NOT I_50 ( .A(rx_err), .Z(N253) );
  GTECH_AND2 C721 ( .A(N255), .B(N253), .Z(N100) );
  GTECH_AND2 C722 ( .A(N254), .B(rx_active), .Z(N255) );
  GTECH_AND2 C723 ( .A(pid_TOKEN), .B(rx_valid), .Z(N254) );
  GTECH_AND2 C725 ( .A(N257), .B(N253), .Z(N101) );
  GTECH_AND2 C726 ( .A(N256), .B(rx_active), .Z(N257) );
  GTECH_AND2 C727 ( .A(pid_DATA), .B(rx_valid), .Z(N256) );
  GTECH_OR2 C729 ( .A(N259), .B(N262), .Z(N102) );
  GTECH_OR2 C730 ( .A(N258), .B(rx_err), .Z(N259) );
  GTECH_NOT I_51 ( .A(rx_active), .Z(N258) );
  GTECH_AND2 C732 ( .A(rx_valid), .B(N261), .Z(N262) );
  GTECH_NOT I_52 ( .A(N260), .Z(N261) );
  GTECH_OR2 C734 ( .A(pid_TOKEN), .B(pid_DATA), .Z(N260) );
  GTECH_OR2 C739 ( .A(N100), .B(N99), .Z(N103) );
  GTECH_OR2 C740 ( .A(N101), .B(N103), .Z(N104) );
  GTECH_OR2 C741 ( .A(N102), .B(N104), .Z(N105) );
  GTECH_NOT I_53 ( .A(N105), .Z(N106) );
  GTECH_NOT I_54 ( .A(N99), .Z(N111) );
  GTECH_NOT I_55 ( .A(N103), .Z(N117) );
  GTECH_NOT I_56 ( .A(N104), .Z(N119) );
  GTECH_AND2 C749 ( .A(N263), .B(N253), .Z(N122) );
  GTECH_AND2 C750 ( .A(rx_valid), .B(rx_active), .Z(N263) );
  GTECH_OR2 C753 ( .A(N128), .B(N122), .Z(N123) );
  GTECH_NOT I_57 ( .A(N123), .Z(N124) );
  GTECH_NOT I_58 ( .A(N122), .Z(N125) );
  GTECH_OR2 C756 ( .A(N258), .B(rx_err), .Z(N128) );
  GTECH_NOT I_59 ( .A(N128), .Z(N129) );
  GTECH_OR2 C761 ( .A(N86), .B(N81), .Z(N130) );
  GTECH_OR2 C762 ( .A(N91), .B(N130), .Z(N131) );
  GTECH_OR2 C763 ( .A(N96), .B(N131), .Z(N132) );
  GTECH_NOT I_60 ( .A(N132), .Z(N133) );
  GTECH_AND2 C765 ( .A(N100), .B(N111), .Z(N134) );
  GTECH_NOT I_61 ( .A(N100), .Z(N135) );
  GTECH_AND2 C767 ( .A(N111), .B(N135), .Z(N136) );
  GTECH_AND2 C768 ( .A(N101), .B(N136), .Z(N137) );
  GTECH_NOT I_62 ( .A(N101), .Z(N138) );
  GTECH_AND2 C770 ( .A(N136), .B(N138), .Z(N139) );
  GTECH_AND2 C771 ( .A(N102), .B(N139), .Z(N140) );
  GTECH_AND2 C772 ( .A(N81), .B(rst), .Z(N141) );
  GTECH_AND2 C773 ( .A(N98), .B(N141), .Z(N142) );
  GTECH_AND2 C774 ( .A(N86), .B(rst), .Z(N143) );
  GTECH_AND2 C775 ( .A(N99), .B(N143), .Z(N144) );
  GTECH_AND2 C776 ( .A(rx_active), .B(N144), .Z(N145) );
  GTECH_OR2 C777 ( .A(N142), .B(N145), .Z(N146) );
  GTECH_AND2 C778 ( .A(N91), .B(rst), .Z(N147) );
  GTECH_AND2 C779 ( .A(N128), .B(N147), .Z(N148) );
  GTECH_AND2 C780 ( .A(rx_active), .B(N148), .Z(N149) );
  GTECH_OR2 C781 ( .A(N146), .B(N149), .Z(N150) );
  GTECH_AND2 C782 ( .A(N124), .B(N147), .Z(N151) );
  GTECH_OR2 C783 ( .A(N150), .B(N151), .Z(N152) );
  GTECH_AND2 C784 ( .A(N96), .B(rst), .Z(N153) );
  GTECH_AND2 C785 ( .A(N128), .B(N153), .Z(N154) );
  GTECH_AND2 C786 ( .A(rx_active), .B(N154), .Z(N155) );
  GTECH_OR2 C787 ( .A(N152), .B(N155), .Z(N156) );
  GTECH_AND2 C788 ( .A(N129), .B(N153), .Z(N157) );
  GTECH_OR2 C789 ( .A(N156), .B(N157), .Z(N158) );
  GTECH_NOT I_63 ( .A(N158), .Z(N159) );
endmodule


module usbf_pa ( clk, rst, tx_data, tx_valid, tx_valid_last, tx_ready, 
        tx_first, send_token, token_pid_sel, send_data, data_pid_sel, 
        send_zero_length, tx_data_st, rd_next );
  output [7:0] tx_data;
  input [1:0] token_pid_sel;
  input [1:0] data_pid_sel;
  input [7:0] tx_data_st;
  input clk, rst, tx_ready, send_token, send_data, send_zero_length;
  output tx_valid, tx_valid_last, tx_first, rd_next;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15,
         N16, N17, N18, N19, N20, N21, send_zero_length_r, last, crc16_clr,
         N22, N23, N24, zero_length_r, N25, N26, N27, N28, N29, N30,
         tx_valid_r1, tx_valid_r, N31, N32, N33, send_token_r, N34, N35, N36,
         N37, N38, N39, N40, N41, N42, N43, N44, N45, N46, N47, token_pid_3,
         token_pid_2, N48, N49, N50, N51, N52, N53, N54, N55, data_pid_3,
         data_pid_2, N56, N57, dsel, N58, crc_sel1, crc_sel2, N59, N60, N61,
         N62, tx_first_r, send_data_r, send_data_r2, crc16_add, N63, N64, N65,
         N66, N67, N68, N69, N70, N71, N72, N73, N74, N75, N76, N77, N78, N79,
         N80, N81, N82, N83, N84, N85, N86, N87, N88, N89, N90, N91, N92, N93,
         N94, N95, N96, N97, N98, N99, N100, N101, N102, N103, N104, N105,
         N106, N107, N108, N109, N110, N111, N112, N113, N114, N115, N116,
         N117, N118, N119, N120, N121, N122, N123, N124, N125, N126, N127,
         N128, N129, N130, N131, N132, N133, N134, N135, N136, N137, N138,
         N139, N140, N141, N142, N143, N144, N145, N146, N147, N148, N149,
         N150, N151, N152, N153, N154, N155, N156, N157, N158, N159;
  wire   [7:6] token_pid;
  wire   [7:6] data_pid;
  wire   [7:0] tx_data_data;
  wire   [7:0] tx_spec_data;
  wire   [15:0] crc16_rev;
  wire   [15:0] crc16;
  wire   [15:0] crc16_next;
  wire   [4:0] state;
  wire   [4:0] next_state;

  \**SEQGEN**  send_zero_length_r_reg ( .clear(1'b0), .preset(1'b0), 
        .next_state(send_zero_length), .clocked_on(clk), .data_in(1'b0), 
        .enable(1'b0), .Q(send_zero_length_r), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  zero_length_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N26), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        zero_length_r), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N25) );
  \**SEQGEN**  tx_valid_r1_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        tx_valid), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        tx_valid_r1), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  tx_valid_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        tx_valid_r1), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        tx_valid_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  send_token_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(N35), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(send_token_r), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N34) );
  GTECH_AND2 C48 ( .A(N40), .B(N41), .Z(N42) );
  GTECH_OR2 C50 ( .A(token_pid_sel[1]), .B(N41), .Z(N43) );
  GTECH_OR2 C53 ( .A(N40), .B(token_pid_sel[0]), .Z(N45) );
  GTECH_AND2 C55 ( .A(token_pid_sel[1]), .B(token_pid_sel[0]), .Z(N47) );
  GTECH_AND2 C70 ( .A(N48), .B(N49), .Z(N50) );
  GTECH_OR2 C72 ( .A(data_pid_sel[1]), .B(N49), .Z(N51) );
  GTECH_OR2 C75 ( .A(N48), .B(data_pid_sel[0]), .Z(N53) );
  GTECH_AND2 C77 ( .A(data_pid_sel[1]), .B(data_pid_sel[0]), .Z(N55) );
  \**SEQGEN**  tx_first_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(N62), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(tx_first_r), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  send_data_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        send_data), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        send_data_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  send_data_r2_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        send_data_r), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        send_data_r2), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \crc16_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(N81), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16[15]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N65) );
  \**SEQGEN**  \crc16_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(N80), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16[14]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N65) );
  \**SEQGEN**  \crc16_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(N79), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16[13]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N65) );
  \**SEQGEN**  \crc16_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(N78), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16[12]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N65) );
  \**SEQGEN**  \crc16_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(N77), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16[11]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N65) );
  \**SEQGEN**  \crc16_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(N76), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16[10]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N65) );
  \**SEQGEN**  \crc16_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(N75), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N65) );
  \**SEQGEN**  \crc16_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(N74), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N65) );
  \**SEQGEN**  \crc16_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N73), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N65) );
  \**SEQGEN**  \crc16_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N72), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N65) );
  \**SEQGEN**  \crc16_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N71), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N65) );
  \**SEQGEN**  \crc16_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N70), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N65) );
  \**SEQGEN**  \crc16_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N69), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N65) );
  \**SEQGEN**  \crc16_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N68), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N65) );
  \**SEQGEN**  \crc16_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N67), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N65) );
  \**SEQGEN**  \crc16_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N66), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(crc16[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N65) );
  usbf_crc16 u1 ( .crc_in(crc16), .din({tx_data_st[0], tx_data_st[1], 
        tx_data_st[2], tx_data_st[3], tx_data_st[4], tx_data_st[5], 
        tx_data_st[6], tx_data_st[7]}), .crc_out(crc16_next) );
  \**SEQGEN**  \state_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N89), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N148) );
  \**SEQGEN**  \state_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N88), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N148) );
  \**SEQGEN**  \state_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N87), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N148) );
  \**SEQGEN**  \state_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N86), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N148) );
  \**SEQGEN**  \state_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N85), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N148) );
  GTECH_OR2 C210 ( .A(state[4]), .B(state[3]), .Z(N91) );
  GTECH_OR2 C211 ( .A(state[2]), .B(state[1]), .Z(N92) );
  GTECH_OR2 C212 ( .A(N91), .B(N92), .Z(N93) );
  GTECH_OR2 C213 ( .A(N93), .B(N90), .Z(N94) );
  GTECH_OR2 C216 ( .A(state[4]), .B(state[3]), .Z(N97) );
  GTECH_OR2 C217 ( .A(state[2]), .B(N96), .Z(N98) );
  GTECH_OR2 C218 ( .A(N97), .B(N98), .Z(N99) );
  GTECH_OR2 C219 ( .A(N99), .B(state[0]), .Z(N100) );
  GTECH_OR2 C222 ( .A(N102), .B(state[3]), .Z(N103) );
  GTECH_OR2 C223 ( .A(state[2]), .B(state[1]), .Z(N104) );
  GTECH_OR2 C224 ( .A(N103), .B(N104), .Z(N105) );
  GTECH_OR2 C225 ( .A(N105), .B(state[0]), .Z(N106) );
  GTECH_OR2 C228 ( .A(state[4]), .B(N108), .Z(N109) );
  GTECH_OR2 C229 ( .A(state[2]), .B(state[1]), .Z(N110) );
  GTECH_OR2 C230 ( .A(N109), .B(N110), .Z(N111) );
  GTECH_OR2 C231 ( .A(N111), .B(state[0]), .Z(N112) );
  GTECH_OR2 C234 ( .A(state[4]), .B(state[3]), .Z(N115) );
  GTECH_OR2 C235 ( .A(N114), .B(state[1]), .Z(N116) );
  GTECH_OR2 C236 ( .A(N115), .B(N116), .Z(N117) );
  GTECH_OR2 C237 ( .A(N117), .B(state[0]), .Z(N118) );
  SELECT_OP C392 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N0), .CONTROL2(N27), .CONTROL3(N30), .CONTROL4(N24), .Z(N25)
         );
  GTECH_BUF B_0 ( .A(N84), .Z(N0) );
  SELECT_OP C393 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(send_zero_length_r), 
        .CONTROL1(N0), .CONTROL2(N27), .CONTROL3(N30), .Z(N26) );
  SELECT_OP C394 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N0), .CONTROL2(N36), .CONTROL3(N39), .CONTROL4(N33), .Z(N34)
         );
  SELECT_OP C395 ( .DATA1(1'b0), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N0), 
        .CONTROL2(N36), .CONTROL3(N39), .Z(N35) );
  SELECT_OP C396 ( .DATA1({1'b1, 1'b1, 1'b0, 1'b0}), .DATA2({1'b0, 1'b1, 1'b1, 
        1'b0}), .DATA3({1'b0, 1'b0, 1'b1, 1'b1}), .DATA4({1'b1, 1'b0, 1'b0, 
        1'b1}), .CONTROL1(N1), .CONTROL2(N2), .CONTROL3(N3), .CONTROL4(N4), 
        .Z({token_pid, token_pid_3, token_pid_2}) );
  GTECH_BUF B_1 ( .A(N42), .Z(N1) );
  GTECH_BUF B_2 ( .A(N44), .Z(N2) );
  GTECH_BUF B_3 ( .A(N46), .Z(N3) );
  GTECH_BUF B_4 ( .A(N47), .Z(N4) );
  SELECT_OP C397 ( .DATA1({1'b1, 1'b1, 1'b0, 1'b0}), .DATA2({1'b0, 1'b1, 1'b1, 
        1'b0}), .DATA3({1'b1, 1'b0, 1'b0, 1'b1}), .DATA4({1'b0, 1'b0, 1'b1, 
        1'b1}), .CONTROL1(N5), .CONTROL2(N6), .CONTROL3(N7), .CONTROL4(N8), 
        .Z({data_pid, data_pid_3, data_pid_2}) );
  GTECH_BUF B_5 ( .A(N50), .Z(N5) );
  GTECH_BUF B_6 ( .A(N52), .Z(N6) );
  GTECH_BUF B_7 ( .A(N54), .Z(N7) );
  GTECH_BUF B_8 ( .A(N55), .Z(N8) );
  SELECT_OP C398 ( .DATA1({token_pid, 1'b0, 1'b1, token_pid_3, token_pid_2, 
        1'b1, 1'b0}), .DATA2(tx_data_data), .CONTROL1(N9), .CONTROL2(N57), .Z(
        tx_data) );
  GTECH_BUF B_9 ( .A(N56), .Z(N9) );
  SELECT_OP C399 ( .DATA1(tx_spec_data), .DATA2(tx_data_st), .CONTROL1(N10), 
        .CONTROL2(N11), .Z(tx_data_data) );
  GTECH_BUF B_10 ( .A(dsel), .Z(N10) );
  GTECH_BUF B_11 ( .A(N58), .Z(N11) );
  SELECT_OP C400 ( .DATA1({data_pid, 1'b0, 1'b0, data_pid_3, data_pid_2, 1'b1, 
        1'b1}), .DATA2(crc16_rev[15:8]), .DATA3(crc16_rev[7:0]), .CONTROL1(N12), .CONTROL2(N13), .CONTROL3(N61), .Z(tx_spec_data) );
  GTECH_BUF B_12 ( .A(N59), .Z(N12) );
  GTECH_BUF B_13 ( .A(crc_sel1), .Z(N13) );
  SELECT_OP C401 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N14), 
        .CONTROL2(N83), .CONTROL3(N64), .Z(N65) );
  GTECH_BUF B_14 ( .A(crc16_clr), .Z(N14) );
  SELECT_OP C402 ( .DATA1({1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 
        1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1}), .DATA2(crc16_next), 
        .CONTROL1(N14), .CONTROL2(N83), .Z({N81, N80, N79, N78, N77, N76, N75, 
        N74, N73, N72, N71, N70, N69, N68, N67, N66}) );
  SELECT_OP C403 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b1}), .DATA2(next_state), 
        .CONTROL1(N0), .CONTROL2(N15), .Z({N89, N88, N87, N86, N85}) );
  GTECH_BUF B_15 ( .A(rst), .Z(N15) );
  SELECT_OP C404 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N16), 
        .CONTROL2(N136), .CONTROL3(N122), .Z(N123) );
  GTECH_BUF B_16 ( .A(N120), .Z(N16) );
  SELECT_OP C405 ( .DATA1({1'b1, 1'b0}), .DATA2({1'b0, 1'b1}), .CONTROL1(N16), 
        .CONTROL2(N136), .Z({N125, N124}) );
  SELECT_OP C406 ( .DATA1(N123), .DATA2(N127), .DATA3(1'b1), .DATA4(1'b1), 
        .DATA5(1'b1), .DATA6(1'b0), .CONTROL1(N17), .CONTROL2(N18), .CONTROL3(
        N19), .CONTROL4(N20), .CONTROL5(N21), .CONTROL6(N134), .Z(dsel) );
  GTECH_BUF B_17 ( .A(N95), .Z(N17) );
  GTECH_BUF B_18 ( .A(N101), .Z(N18) );
  GTECH_BUF B_19 ( .A(N107), .Z(N19) );
  GTECH_BUF B_20 ( .A(N113), .Z(N20) );
  GTECH_BUF B_21 ( .A(N119), .Z(N21) );
  SELECT_OP C407 ( .DATA1(N123), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b1), 
        .DATA5(1'b0), .DATA6(1'b0), .CONTROL1(N17), .CONTROL2(N18), .CONTROL3(
        N19), .CONTROL4(N20), .CONTROL5(N21), .CONTROL6(N134), .Z(tx_valid) );
  SELECT_OP C408 ( .DATA1({N125, 1'b0, 1'b0, N124, 1'b0}), .DATA2({1'b0, 1'b0, 
        1'b1, 1'b0, 1'b0}), .DATA3({1'b0, 1'b0, 1'b1, 1'b0, 1'b0}), .DATA4({
        1'b0, 1'b1, 1'b0, 1'b0, 1'b0}), .DATA5({1'b0, 1'b0, 1'b0, 1'b0, 1'b1}), 
        .DATA6(state), .CONTROL1(N17), .CONTROL2(N18), .CONTROL3(N19), 
        .CONTROL4(N20), .CONTROL5(N21), .CONTROL6(N134), .Z(next_state) );
  SELECT_OP C409 ( .DATA1(1'b0), .DATA2(N126), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .CONTROL1(N17), .CONTROL2(N18), .CONTROL3(
        N19), .CONTROL4(N20), .CONTROL5(N21), .CONTROL6(N134), .Z(rd_next) );
  SELECT_OP C410 ( .DATA1(1'b0), .DATA2(N127), .DATA3(1'b1), .DATA4(N129), 
        .DATA5(1'b0), .DATA6(1'b0), .CONTROL1(N17), .CONTROL2(N18), .CONTROL3(
        N19), .CONTROL4(N20), .CONTROL5(N21), .CONTROL6(N134), .Z(crc_sel1) );
  SELECT_OP C411 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(tx_ready), 
        .DATA5(N129), .DATA6(1'b0), .CONTROL1(N17), .CONTROL2(N18), .CONTROL3(
        N19), .CONTROL4(N20), .CONTROL5(N21), .CONTROL6(N134), .Z(last) );
  SELECT_OP C412 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(tx_ready), 
        .DATA5(1'b1), .DATA6(1'b0), .CONTROL1(N17), .CONTROL2(N18), .CONTROL3(
        N19), .CONTROL4(N20), .CONTROL5(N21), .CONTROL6(N134), .Z(crc_sel2) );
  GTECH_OR2 C417 ( .A(last), .B(N84), .Z(N22) );
  GTECH_OR2 C418 ( .A(crc16_clr), .B(N22), .Z(N23) );
  GTECH_NOT I_0 ( .A(N23), .Z(N24) );
  GTECH_AND2 C421 ( .A(last), .B(rst), .Z(N27) );
  GTECH_NOT I_1 ( .A(last), .Z(N28) );
  GTECH_AND2 C423 ( .A(rst), .B(N28), .Z(N29) );
  GTECH_AND2 C424 ( .A(crc16_clr), .B(N29), .Z(N30) );
  GTECH_OR2 C427 ( .A(send_token), .B(N84), .Z(N31) );
  GTECH_OR2 C428 ( .A(tx_ready), .B(N31), .Z(N32) );
  GTECH_NOT I_2 ( .A(N32), .Z(N33) );
  GTECH_AND2 C430 ( .A(send_token), .B(rst), .Z(N36) );
  GTECH_NOT I_3 ( .A(send_token), .Z(N37) );
  GTECH_AND2 C432 ( .A(rst), .B(N37), .Z(N38) );
  GTECH_AND2 C433 ( .A(tx_ready), .B(N38), .Z(N39) );
  GTECH_NOT I_4 ( .A(token_pid_sel[1]), .Z(N40) );
  GTECH_NOT I_5 ( .A(token_pid_sel[0]), .Z(N41) );
  GTECH_NOT I_6 ( .A(N43), .Z(N44) );
  GTECH_NOT I_7 ( .A(N45), .Z(N46) );
  GTECH_NOT I_8 ( .A(data_pid_sel[1]), .Z(N48) );
  GTECH_NOT I_9 ( .A(data_pid_sel[0]), .Z(N49) );
  GTECH_NOT I_10 ( .A(N51), .Z(N52) );
  GTECH_NOT I_11 ( .A(N53), .Z(N54) );
  GTECH_OR2 C450 ( .A(send_token), .B(send_token_r), .Z(N56) );
  GTECH_NOT I_12 ( .A(N56), .Z(N57) );
  GTECH_NOT I_13 ( .A(dsel), .Z(N58) );
  GTECH_AND2 C456 ( .A(N149), .B(N150), .Z(N59) );
  GTECH_NOT I_14 ( .A(crc_sel1), .Z(N149) );
  GTECH_NOT I_15 ( .A(crc_sel2), .Z(N150) );
  GTECH_OR2 C461 ( .A(crc_sel1), .B(N59), .Z(N60) );
  GTECH_NOT I_16 ( .A(N60), .Z(N61) );
  GTECH_OR2 C463 ( .A(send_token), .B(last), .Z(tx_valid_last) );
  GTECH_OR2 C464 ( .A(send_token), .B(send_data), .Z(N62) );
  GTECH_AND2 C465 ( .A(N151), .B(N152), .Z(tx_first) );
  GTECH_OR2 C466 ( .A(send_token), .B(send_data), .Z(N151) );
  GTECH_NOT I_17 ( .A(tx_first_r), .Z(N152) );
  GTECH_OR2 C468 ( .A(send_data), .B(N153), .Z(crc16_clr) );
  GTECH_NOT I_18 ( .A(send_data_r), .Z(N153) );
  GTECH_OR2 C470 ( .A(N157), .B(rd_next), .Z(crc16_add) );
  GTECH_AND2 C471 ( .A(N154), .B(N156), .Z(N157) );
  GTECH_NOT I_19 ( .A(zero_length_r), .Z(N154) );
  GTECH_AND2 C473 ( .A(send_data_r), .B(N155), .Z(N156) );
  GTECH_NOT I_20 ( .A(send_data_r2), .Z(N155) );
  GTECH_OR2 C477 ( .A(crc16_add), .B(crc16_clr), .Z(N63) );
  GTECH_NOT I_21 ( .A(N63), .Z(N64) );
  GTECH_NOT I_22 ( .A(crc16_clr), .Z(N82) );
  GTECH_AND2 C480 ( .A(crc16_add), .B(N82), .Z(N83) );
  GTECH_NOT I_23 ( .A(crc16[8]), .Z(crc16_rev[15]) );
  GTECH_NOT I_24 ( .A(crc16[9]), .Z(crc16_rev[14]) );
  GTECH_NOT I_25 ( .A(crc16[10]), .Z(crc16_rev[13]) );
  GTECH_NOT I_26 ( .A(crc16[11]), .Z(crc16_rev[12]) );
  GTECH_NOT I_27 ( .A(crc16[12]), .Z(crc16_rev[11]) );
  GTECH_NOT I_28 ( .A(crc16[13]), .Z(crc16_rev[10]) );
  GTECH_NOT I_29 ( .A(crc16[14]), .Z(crc16_rev[9]) );
  GTECH_NOT I_30 ( .A(crc16[15]), .Z(crc16_rev[8]) );
  GTECH_NOT I_31 ( .A(crc16[0]), .Z(crc16_rev[7]) );
  GTECH_NOT I_32 ( .A(crc16[1]), .Z(crc16_rev[6]) );
  GTECH_NOT I_33 ( .A(crc16[2]), .Z(crc16_rev[5]) );
  GTECH_NOT I_34 ( .A(crc16[3]), .Z(crc16_rev[4]) );
  GTECH_NOT I_35 ( .A(crc16[4]), .Z(crc16_rev[3]) );
  GTECH_NOT I_36 ( .A(crc16[5]), .Z(crc16_rev[2]) );
  GTECH_NOT I_37 ( .A(crc16[6]), .Z(crc16_rev[1]) );
  GTECH_NOT I_38 ( .A(crc16[7]), .Z(crc16_rev[0]) );
  GTECH_NOT I_39 ( .A(rst), .Z(N84) );
  GTECH_NOT I_40 ( .A(state[0]), .Z(N90) );
  GTECH_NOT I_41 ( .A(N94), .Z(N95) );
  GTECH_NOT I_42 ( .A(state[1]), .Z(N96) );
  GTECH_NOT I_43 ( .A(N100), .Z(N101) );
  GTECH_NOT I_44 ( .A(state[4]), .Z(N102) );
  GTECH_NOT I_45 ( .A(N106), .Z(N107) );
  GTECH_NOT I_46 ( .A(state[3]), .Z(N108) );
  GTECH_NOT I_47 ( .A(N112), .Z(N113) );
  GTECH_NOT I_48 ( .A(state[2]), .Z(N114) );
  GTECH_NOT I_49 ( .A(N118), .Z(N119) );
  GTECH_AND2 C515 ( .A(send_zero_length_r), .B(send_data), .Z(N120) );
  GTECH_OR2 C518 ( .A(send_data), .B(N120), .Z(N121) );
  GTECH_NOT I_50 ( .A(N121), .Z(N122) );
  GTECH_AND2 C520 ( .A(tx_ready), .B(tx_valid_r), .Z(N126) );
  GTECH_AND2 C522 ( .A(N159), .B(tx_valid_r), .Z(N127) );
  GTECH_AND2 C523 ( .A(N158), .B(tx_ready), .Z(N159) );
  GTECH_NOT I_51 ( .A(send_data), .Z(N158) );
  GTECH_NOT I_52 ( .A(N127), .Z(N128) );
  GTECH_NOT I_53 ( .A(tx_ready), .Z(N129) );
  GTECH_OR2 C529 ( .A(N101), .B(N95), .Z(N130) );
  GTECH_OR2 C530 ( .A(N107), .B(N130), .Z(N131) );
  GTECH_OR2 C531 ( .A(N113), .B(N131), .Z(N132) );
  GTECH_OR2 C532 ( .A(N119), .B(N132), .Z(N133) );
  GTECH_NOT I_54 ( .A(N133), .Z(N134) );
  GTECH_NOT I_55 ( .A(N120), .Z(N135) );
  GTECH_AND2 C535 ( .A(send_data), .B(N135), .Z(N136) );
  GTECH_AND2 C536 ( .A(N95), .B(rst), .Z(N137) );
  GTECH_AND2 C537 ( .A(N122), .B(N137), .Z(N138) );
  GTECH_AND2 C538 ( .A(N101), .B(rst), .Z(N139) );
  GTECH_AND2 C539 ( .A(N128), .B(N139), .Z(N140) );
  GTECH_OR2 C540 ( .A(N138), .B(N140), .Z(N141) );
  GTECH_AND2 C541 ( .A(N113), .B(rst), .Z(N142) );
  GTECH_AND2 C542 ( .A(N129), .B(N142), .Z(N143) );
  GTECH_OR2 C543 ( .A(N141), .B(N143), .Z(N144) );
  GTECH_AND2 C544 ( .A(N119), .B(rst), .Z(N145) );
  GTECH_AND2 C545 ( .A(N129), .B(N145), .Z(N146) );
  GTECH_OR2 C546 ( .A(N144), .B(N146), .Z(N147) );
  GTECH_NOT I_56 ( .A(N147), .Z(N148) );
endmodule


module usbf_idma_SSRAM_HADR14 ( clk, rst, rx_data_st, rx_data_valid, 
        rx_data_done, send_data, tx_data_st, rd_next, rx_dma_en, tx_dma_en, 
        abort, idma_done, buf_size, dma_en, send_zero_length, adr, size, 
        sizu_c, madr, mdout, mdin, mwe, mreq, mack );
  input [7:0] rx_data_st;
  output [7:0] tx_data_st;
  input [13:0] buf_size;
  input [16:0] adr;
  input [13:0] size;
  output [10:0] sizu_c;
  output [14:0] madr;
  output [31:0] mdout;
  input [31:0] mdin;
  input clk, rst, rx_data_valid, rx_data_done, rd_next, rx_dma_en, tx_dma_en,
         abort, dma_en, send_zero_length, mack;
  output send_data, idma_done, mwe, mreq;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15,
         N16, N17, N18, N19, N20, N21, N22, N23, N24, N25, N26, mreq_d, mack_r,
         word_done_r, mwe_d, N27, rx_data_valid_r, rx_data_done_r,
         rx_data_done_r2, tx_dma_en_r, rx_dma_en_r, send_zero_length_r, N28,
         N29, N30, N31, N32, N33, N34, N35, N36, N37, N38, N39, N40, N41, N42,
         N43, N44, N45, N46, N47, N48, N49, N50, N51, N52, N53, N54, N55, N56,
         N57, N58, N59, N60, N61, N62, adr_incw, N63, N64, N65, N66, N67, N68,
         N69, N70, N71, N72, N73, N74, N75, N76, N77, N78, N79, N80, N81, N82,
         N83, N84, N85, adr_incb, N86, N87, N88, N89, dtmp_sel_r, N90, siz_dec,
         N91, N92, N93, N94, N95, N96, N97, N98, N99, N100, N101, N102, N103,
         N104, N105, N106, N107, N108, N109, N110, N111, N112, N113, N114,
         N115, N116, N117, N118, N119, N120, N121, N122, N123, N124, N125,
         N126, N127, rd_first, sizd_is_zero, N128, N129, N130, N131, N132,
         N133, N134, N135, N136, N137, N138, N139, N140, N141, N142, N143,
         N144, N145, N146, N147, N148, N149, N150, N151, N152, N153, N154,
         N155, N156, N157, N158, N159, dtmp_sel, N160, N161, N162, N163, N164,
         N165, N166, N167, N168, N169, N170, N171, N172, N173, N174, N175,
         N176, N177, N178, N179, N180, N181, N182, N183, N184, N185, N186,
         N187, N188, N189, N190, N191, N192, N193, N194, N195, N196, N197,
         N198, N199, wr_last, N200, word_done, N201, wr_last_en, N202,
         wr_done_r, wr_done, fill_buf0, fill_buf1, N203, N204, N205, N206,
         N207, N208, N209, N210, N211, N212, N213, N214, N215, N216, N217,
         N218, N219, N220, N221, N222, N223, N224, N225, N226, N227, N228,
         N229, N230, N231, send_data_r, N232, N233, N234, N235, N236, N237,
         N238, N239, N240, N241, N242, N243, N244, N245, N246, N247, N248,
         N249, N250, N251, N252, N253, N254, N255, N256, N257, N258, N259,
         N260, N261, N262, N263, N264, N265, N266, N267, N268, N269, N270,
         N271, N272, N273, N274, N275, N276, N277, N278, N279, N280, N281,
         N282, N283, N284, N285, N286, N287, N288, N289, N290, N291, N292,
         N293, N294, N295, N296, N297, N298, N299, N300, N301, N302, N303,
         N304, N305, N306, N307, N308, N309, N310, N311, N312, N313, N314,
         N315, N316, N317, N318, N319, N320, N321, N322, N323, N324, N325,
         N326, N327, N328, N329, N330, N331, N332, N333, N334, N335, N336,
         N337, N338, N339, N340, N341, N342, N343, N344, N345, N346, N347,
         N348, N349, N350, N351, N352, N353, N354, N355, N356, N357, N358,
         N359, N360, N361, N362, N363, N364, N365, N366, N367, N368, N369,
         N370, N371, N372, N373, N374, N375, N376, N377, N378, N379, N380,
         N381, N382, N383, N384, N385, N386, N387, N388, N389, N390, N391,
         N392, N393, N394, N395, N396, N397, N398, N399, N400, N401, N402,
         N403, N404, N405, N406, N407, N408, N409, N410, N411, N412, N413,
         N414, N415, N416, N417, N418, N419, N420, N421, N422, N423, N424,
         N425, N426, N427, N428, N429, N430, N431, N432, N433, N434, N435,
         N436, N437, N438, N439, N440, N441, N442, N443, N444, N445, N446,
         N447, N448, N449, N450, N451, N452, N453, N454, N455, N456, N457,
         N458, N459, N460, N461, N462, N463, N464;
  wire   [7:0] rx_data_st_r;
  wire   [14:0] adrw_next1;
  wire   [14:0] last_buf_adr;
  wire   [14:0] adrw_next;
  wire   [2:0] adr_cb;
  wire   [2:0] adrb_next;
  wire   [13:0] sizd_c;
  wire   [31:0] dtmp_r;
  wire   [31:0] rd_buf0;
  wire   [31:0] rd_buf1;
  wire   [7:0] state;
  wire   [7:0] next_state;

  \**SEQGEN**  mwe_reg ( .clear(1'b0), .preset(1'b0), .next_state(mwe_d), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(mwe), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  mack_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(N27), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(mack_r), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  rx_data_valid_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data_valid), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rx_data_valid_r), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_data_st_r_reg[7]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(rx_data_st[7]), .clocked_on(clk), .data_in(1'b0), .enable(
        1'b0), .Q(rx_data_st_r[7]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_data_st_r_reg[6]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(rx_data_st[6]), .clocked_on(clk), .data_in(1'b0), .enable(
        1'b0), .Q(rx_data_st_r[6]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_data_st_r_reg[5]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(rx_data_st[5]), .clocked_on(clk), .data_in(1'b0), .enable(
        1'b0), .Q(rx_data_st_r[5]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_data_st_r_reg[4]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(rx_data_st[4]), .clocked_on(clk), .data_in(1'b0), .enable(
        1'b0), .Q(rx_data_st_r[4]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_data_st_r_reg[3]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(rx_data_st[3]), .clocked_on(clk), .data_in(1'b0), .enable(
        1'b0), .Q(rx_data_st_r[3]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_data_st_r_reg[2]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(rx_data_st[2]), .clocked_on(clk), .data_in(1'b0), .enable(
        1'b0), .Q(rx_data_st_r[2]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_data_st_r_reg[1]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(rx_data_st[1]), .clocked_on(clk), .data_in(1'b0), .enable(
        1'b0), .Q(rx_data_st_r[1]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_data_st_r_reg[0]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(rx_data_st[0]), .clocked_on(clk), .data_in(1'b0), .enable(
        1'b0), .Q(rx_data_st_r[0]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  rx_data_done_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data_done), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rx_data_done_r), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  rx_data_done_r2_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data_done_r), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rx_data_done_r2), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  tx_dma_en_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        tx_dma_en), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        tx_dma_en_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  rx_dma_en_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_dma_en), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rx_dma_en_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  send_zero_length_r_reg ( .clear(1'b0), .preset(1'b0), 
        .next_state(send_zero_length), .clocked_on(clk), .data_in(1'b0), 
        .enable(1'b0), .Q(send_zero_length_r), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \adr_cw_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(N44), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(madr[14]), .synch_clear(
        1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1)
         );
  \**SEQGEN**  \adr_cw_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(N43), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(madr[13]), .synch_clear(
        1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1)
         );
  \**SEQGEN**  \adr_cw_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(N42), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(madr[12]), .synch_clear(
        1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1)
         );
  \**SEQGEN**  \adr_cw_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(N41), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(madr[11]), .synch_clear(
        1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1)
         );
  \**SEQGEN**  \adr_cw_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(N40), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(madr[10]), .synch_clear(
        1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1)
         );
  \**SEQGEN**  \adr_cw_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(N39), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(madr[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_cw_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(N38), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(madr[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_cw_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N37), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(madr[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_cw_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N36), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(madr[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_cw_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N35), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(madr[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_cw_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N34), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(madr[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_cw_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N33), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(madr[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_cw_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N32), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(madr[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_cw_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N31), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(madr[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_cw_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N30), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(madr[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \last_buf_adr_reg[14]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N59), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        last_buf_adr[14]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \last_buf_adr_reg[13]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N58), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        last_buf_adr[13]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \last_buf_adr_reg[12]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N57), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        last_buf_adr[12]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \last_buf_adr_reg[11]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N56), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        last_buf_adr[11]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \last_buf_adr_reg[10]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N55), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        last_buf_adr[10]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \last_buf_adr_reg[9]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N54), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        last_buf_adr[9]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \last_buf_adr_reg[8]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N53), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        last_buf_adr[8]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \last_buf_adr_reg[7]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N52), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        last_buf_adr[7]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \last_buf_adr_reg[6]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N51), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        last_buf_adr[6]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \last_buf_adr_reg[5]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N50), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        last_buf_adr[5]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \last_buf_adr_reg[4]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N49), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        last_buf_adr[4]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \last_buf_adr_reg[3]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N48), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        last_buf_adr[3]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \last_buf_adr_reg[2]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N47), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        last_buf_adr[2]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \last_buf_adr_reg[1]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N46), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        last_buf_adr[1]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \last_buf_adr_reg[0]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N45), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        last_buf_adr[0]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  EQ_UNS_OP eq_3430 ( .A(adrw_next), .B(last_buf_adr), .Z(N60) );
  \**SEQGEN**  \adr_cb_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N84), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr_cb[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_cb_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N83), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr_cb[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_cb_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N82), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr_cb[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \sizd_c_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N123), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizd_c[13]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N109) );
  \**SEQGEN**  \sizd_c_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N122), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizd_c[12]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N109) );
  \**SEQGEN**  \sizd_c_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N121), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizd_c[11]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N109) );
  \**SEQGEN**  \sizd_c_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N120), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizd_c[10]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N109) );
  \**SEQGEN**  \sizd_c_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(N119), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizd_c[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N109) );
  \**SEQGEN**  \sizd_c_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(N118), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizd_c[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N109) );
  \**SEQGEN**  \sizd_c_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N117), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizd_c[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N109) );
  \**SEQGEN**  \sizd_c_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N116), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizd_c[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N109) );
  \**SEQGEN**  \sizd_c_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N115), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizd_c[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N109) );
  \**SEQGEN**  \sizd_c_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N114), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizd_c[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N109) );
  \**SEQGEN**  \sizd_c_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N113), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizd_c[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N109) );
  \**SEQGEN**  \sizd_c_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N112), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizd_c[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N109) );
  \**SEQGEN**  \sizd_c_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N111), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizd_c[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N109) );
  \**SEQGEN**  \sizd_c_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N110), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizd_c[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N109) );
  \**SEQGEN**  sizd_is_zero_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N436), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        sizd_is_zero), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \sizu_c_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N154), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizu_c[10]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N143) );
  \**SEQGEN**  \sizu_c_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(N153), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizu_c[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N143) );
  \**SEQGEN**  \sizu_c_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(N152), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizu_c[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N143) );
  \**SEQGEN**  \sizu_c_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N151), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizu_c[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N143) );
  \**SEQGEN**  \sizu_c_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N150), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizu_c[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N143) );
  \**SEQGEN**  \sizu_c_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N149), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizu_c[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N143) );
  \**SEQGEN**  \sizu_c_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N148), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizu_c[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N143) );
  \**SEQGEN**  \sizu_c_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N147), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizu_c[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N143) );
  \**SEQGEN**  \sizu_c_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N146), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizu_c[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N143) );
  \**SEQGEN**  \sizu_c_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N145), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizu_c[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N143) );
  \**SEQGEN**  \sizu_c_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N144), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(sizu_c[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N143) );
  \**SEQGEN**  idma_done_reg ( .clear(1'b0), .preset(1'b0), .next_state(N159), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idma_done), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  dtmp_sel_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_sel), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        dtmp_sel_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \dtmp_r_reg[31]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N197), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[31]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N189) );
  \**SEQGEN**  \dtmp_r_reg[30]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N196), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[30]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N189) );
  \**SEQGEN**  \dtmp_r_reg[29]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N195), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[29]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N189) );
  \**SEQGEN**  \dtmp_r_reg[28]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N194), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[28]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N189) );
  \**SEQGEN**  \dtmp_r_reg[27]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N193), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[27]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N189) );
  \**SEQGEN**  \dtmp_r_reg[26]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N192), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[26]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N189) );
  \**SEQGEN**  \dtmp_r_reg[25]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N191), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[25]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N189) );
  \**SEQGEN**  \dtmp_r_reg[24]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N190), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[24]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N189) );
  \**SEQGEN**  \dtmp_r_reg[23]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N188), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[23]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N180) );
  \**SEQGEN**  \dtmp_r_reg[22]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N187), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[22]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N180) );
  \**SEQGEN**  \dtmp_r_reg[21]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N186), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[21]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N180) );
  \**SEQGEN**  \dtmp_r_reg[20]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N185), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[20]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N180) );
  \**SEQGEN**  \dtmp_r_reg[19]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N184), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[19]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N180) );
  \**SEQGEN**  \dtmp_r_reg[18]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N183), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[18]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N180) );
  \**SEQGEN**  \dtmp_r_reg[17]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N182), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[17]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N180) );
  \**SEQGEN**  \dtmp_r_reg[16]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N181), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[16]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N180) );
  \**SEQGEN**  \dtmp_r_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N179), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[15]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N171) );
  \**SEQGEN**  \dtmp_r_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N178), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[14]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N171) );
  \**SEQGEN**  \dtmp_r_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N177), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[13]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N171) );
  \**SEQGEN**  \dtmp_r_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N176), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[12]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N171) );
  \**SEQGEN**  \dtmp_r_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N175), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[11]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N171) );
  \**SEQGEN**  \dtmp_r_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N174), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[10]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N171) );
  \**SEQGEN**  \dtmp_r_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(N173), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N171) );
  \**SEQGEN**  \dtmp_r_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(N172), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N171) );
  \**SEQGEN**  \dtmp_r_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N170), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N162) );
  \**SEQGEN**  \dtmp_r_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N169), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N162) );
  \**SEQGEN**  \dtmp_r_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N168), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N162) );
  \**SEQGEN**  \dtmp_r_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N167), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N162) );
  \**SEQGEN**  \dtmp_r_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N166), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N162) );
  \**SEQGEN**  \dtmp_r_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N165), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N162) );
  \**SEQGEN**  \dtmp_r_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N164), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N162) );
  \**SEQGEN**  \dtmp_r_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N163), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(dtmp_r[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N162) );
  \**SEQGEN**  word_done_reg ( .clear(1'b0), .preset(1'b0), .next_state(N200), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(word_done), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  word_done_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(N201), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(word_done_r), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \dout_r_reg[31]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[31]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[31]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[30]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[30]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[30]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[29]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[29]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[29]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[28]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[28]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[28]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[27]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[27]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[27]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[26]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[26]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[26]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[25]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[25]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[25]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[24]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[24]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[24]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[23]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[23]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[23]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[22]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[22]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[22]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[21]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[21]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[21]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[20]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[20]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[20]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[19]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[19]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[19]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[18]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[18]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[18]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[17]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[17]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[17]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[16]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[16]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[16]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[15]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[15]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[14]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[14]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[13]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[13]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[12]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[12]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[11]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[11]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[10]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[10]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[9]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[9]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[8]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[8]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[7]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[7]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[6]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[6]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[5]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[5]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[4]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[4]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[3]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[2]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[1]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(word_done) );
  \**SEQGEN**  \dout_r_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        dtmp_r[0]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        mdout[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(word_done) );
  \**SEQGEN**  wr_last_reg ( .clear(1'b0), .preset(1'b0), .next_state(N202), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(wr_last), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  wr_done_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        rx_data_done_r), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        wr_done_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  wr_done_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        wr_done_r), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        wr_done), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \rd_buf0_reg[31]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[31]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[31]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[30]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[30]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[30]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[29]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[29]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[29]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[28]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[28]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[28]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[27]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[27]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[27]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[26]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[26]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[26]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[25]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[25]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[25]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[24]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[24]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[24]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[23]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[23]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[23]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[22]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[22]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[22]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[21]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[21]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[21]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[20]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[20]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[20]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[19]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[19]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[19]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[18]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[18]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[18]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[17]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[17]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[17]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[16]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[16]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[16]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[15]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[15]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[14]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[14]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[13]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[13]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[12]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[12]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[11]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[11]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[10]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[10]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[9]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[9]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[8]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[8]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[7]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[7]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[6]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[6]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[5]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[5]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[4]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[4]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[3]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[2]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[1]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf0_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[0]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf0[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf0) );
  \**SEQGEN**  \rd_buf1_reg[31]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[31]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[31]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[30]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[30]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[30]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[29]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[29]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[29]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[28]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[28]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[28]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[27]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[27]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[27]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[26]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[26]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[26]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[25]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[25]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[25]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[24]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[24]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[24]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[23]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[23]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[23]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[22]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[22]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[22]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[21]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[21]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[21]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[20]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[20]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[20]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[19]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[19]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[19]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[18]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[18]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[18]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[17]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[17]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[17]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[16]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[16]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[16]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[15]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[15]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[14]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[14]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[13]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[13]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[12]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[12]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[11]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[11]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[10]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[10]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[9]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[9]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[8]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[8]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[7]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[7]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[6]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[6]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[5]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[5]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[4]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[4]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[3]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[2]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[1]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  \**SEQGEN**  \rd_buf1_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        mdin[0]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rd_buf1[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(fill_buf1) );
  GTECH_AND2 C562 ( .A(N203), .B(N204), .Z(N206) );
  GTECH_AND2 C563 ( .A(N206), .B(N205), .Z(N207) );
  GTECH_OR2 C565 ( .A(adrb_next[2]), .B(adrb_next[1]), .Z(N208) );
  GTECH_OR2 C566 ( .A(N208), .B(N205), .Z(N209) );
  GTECH_OR2 C569 ( .A(adrb_next[2]), .B(N204), .Z(N211) );
  GTECH_OR2 C570 ( .A(N211), .B(adrb_next[0]), .Z(N212) );
  GTECH_OR2 C574 ( .A(adrb_next[2]), .B(N204), .Z(N214) );
  GTECH_OR2 C575 ( .A(N214), .B(N205), .Z(N215) );
  GTECH_OR2 C578 ( .A(N203), .B(adrb_next[1]), .Z(N217) );
  GTECH_OR2 C579 ( .A(N217), .B(adrb_next[0]), .Z(N218) );
  GTECH_OR2 C583 ( .A(N203), .B(adrb_next[1]), .Z(N220) );
  GTECH_OR2 C584 ( .A(N220), .B(N205), .Z(N221) );
  GTECH_OR2 C588 ( .A(N203), .B(N204), .Z(N223) );
  GTECH_OR2 C589 ( .A(N223), .B(adrb_next[0]), .Z(N224) );
  GTECH_AND2 C591 ( .A(adrb_next[2]), .B(adrb_next[1]), .Z(N226) );
  GTECH_AND2 C592 ( .A(N226), .B(adrb_next[0]), .Z(N227) );
  \**SEQGEN**  send_data_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(N233), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(send_data_r), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N232) );
  \**SEQGEN**  \state_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N246), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N396) );
  \**SEQGEN**  \state_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N245), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N396) );
  \**SEQGEN**  \state_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N244), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N396) );
  \**SEQGEN**  \state_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N243), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N396) );
  \**SEQGEN**  \state_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N242), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N396) );
  \**SEQGEN**  \state_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N241), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N396) );
  \**SEQGEN**  \state_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N240), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N396) );
  \**SEQGEN**  \state_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N239), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N396) );
  GTECH_OR2 C656 ( .A(state[7]), .B(state[6]), .Z(N248) );
  GTECH_OR2 C657 ( .A(state[5]), .B(state[4]), .Z(N249) );
  GTECH_OR2 C658 ( .A(state[3]), .B(state[2]), .Z(N250) );
  GTECH_OR2 C659 ( .A(state[1]), .B(N247), .Z(N251) );
  GTECH_OR2 C660 ( .A(N248), .B(N249), .Z(N252) );
  GTECH_OR2 C661 ( .A(N250), .B(N251), .Z(N253) );
  GTECH_OR2 C662 ( .A(N252), .B(N253), .Z(N254) );
  GTECH_OR2 C665 ( .A(state[7]), .B(state[6]), .Z(N257) );
  GTECH_OR2 C666 ( .A(state[5]), .B(state[4]), .Z(N258) );
  GTECH_OR2 C667 ( .A(state[3]), .B(state[2]), .Z(N259) );
  GTECH_OR2 C668 ( .A(N256), .B(state[0]), .Z(N260) );
  GTECH_OR2 C669 ( .A(N257), .B(N258), .Z(N261) );
  GTECH_OR2 C670 ( .A(N259), .B(N260), .Z(N262) );
  GTECH_OR2 C671 ( .A(N261), .B(N262), .Z(N263) );
  GTECH_OR2 C674 ( .A(state[7]), .B(state[6]), .Z(N266) );
  GTECH_OR2 C675 ( .A(state[5]), .B(state[4]), .Z(N267) );
  GTECH_OR2 C676 ( .A(state[3]), .B(N265), .Z(N268) );
  GTECH_OR2 C677 ( .A(state[1]), .B(state[0]), .Z(N269) );
  GTECH_OR2 C678 ( .A(N266), .B(N267), .Z(N270) );
  GTECH_OR2 C679 ( .A(N268), .B(N269), .Z(N271) );
  GTECH_OR2 C680 ( .A(N270), .B(N271), .Z(N272) );
  GTECH_OR2 C683 ( .A(state[7]), .B(state[6]), .Z(N275) );
  GTECH_OR2 C684 ( .A(state[5]), .B(state[4]), .Z(N276) );
  GTECH_OR2 C685 ( .A(N274), .B(state[2]), .Z(N277) );
  GTECH_OR2 C686 ( .A(state[1]), .B(state[0]), .Z(N278) );
  GTECH_OR2 C687 ( .A(N275), .B(N276), .Z(N279) );
  GTECH_OR2 C688 ( .A(N277), .B(N278), .Z(N280) );
  GTECH_OR2 C689 ( .A(N279), .B(N280), .Z(N281) );
  GTECH_OR2 C692 ( .A(state[7]), .B(state[6]), .Z(N284) );
  GTECH_OR2 C693 ( .A(state[5]), .B(N283), .Z(N285) );
  GTECH_OR2 C694 ( .A(state[3]), .B(state[2]), .Z(N286) );
  GTECH_OR2 C695 ( .A(state[1]), .B(state[0]), .Z(N287) );
  GTECH_OR2 C696 ( .A(N284), .B(N285), .Z(N288) );
  GTECH_OR2 C697 ( .A(N286), .B(N287), .Z(N289) );
  GTECH_OR2 C698 ( .A(N288), .B(N289), .Z(N290) );
  GTECH_OR2 C701 ( .A(state[7]), .B(state[6]), .Z(N293) );
  GTECH_OR2 C702 ( .A(N292), .B(state[4]), .Z(N294) );
  GTECH_OR2 C703 ( .A(state[3]), .B(state[2]), .Z(N295) );
  GTECH_OR2 C704 ( .A(state[1]), .B(state[0]), .Z(N296) );
  GTECH_OR2 C705 ( .A(N293), .B(N294), .Z(N297) );
  GTECH_OR2 C706 ( .A(N295), .B(N296), .Z(N298) );
  GTECH_OR2 C707 ( .A(N297), .B(N298), .Z(N299) );
  GTECH_OR2 C710 ( .A(state[7]), .B(N301), .Z(N302) );
  GTECH_OR2 C711 ( .A(state[5]), .B(state[4]), .Z(N303) );
  GTECH_OR2 C712 ( .A(state[3]), .B(state[2]), .Z(N304) );
  GTECH_OR2 C713 ( .A(state[1]), .B(state[0]), .Z(N305) );
  GTECH_OR2 C714 ( .A(N302), .B(N303), .Z(N306) );
  GTECH_OR2 C715 ( .A(N304), .B(N305), .Z(N307) );
  GTECH_OR2 C716 ( .A(N306), .B(N307), .Z(N308) );
  GTECH_OR2 C719 ( .A(N310), .B(state[6]), .Z(N311) );
  GTECH_OR2 C720 ( .A(state[5]), .B(state[4]), .Z(N312) );
  GTECH_OR2 C721 ( .A(state[3]), .B(state[2]), .Z(N313) );
  GTECH_OR2 C722 ( .A(state[1]), .B(state[0]), .Z(N314) );
  GTECH_OR2 C723 ( .A(N311), .B(N312), .Z(N315) );
  GTECH_OR2 C724 ( .A(N313), .B(N314), .Z(N316) );
  GTECH_OR2 C725 ( .A(N315), .B(N316), .Z(N317) );
  GTECH_AND2 C1143 ( .A(adr_cb[0]), .B(adr_cb[1]), .Z(N397) );
  GTECH_OR2 C1144 ( .A(adr_cb[0]), .B(adr_cb[1]), .Z(N398) );
  GTECH_AND2 C1147 ( .A(adr_cb[0]), .B(adr_cb[1]), .Z(N399) );
  GTECH_NOT I_0 ( .A(adr_cb[1]), .Z(N400) );
  GTECH_OR2 C1149 ( .A(adr_cb[0]), .B(N400), .Z(N401) );
  GTECH_NOT I_1 ( .A(N401), .Z(N402) );
  GTECH_NOT I_2 ( .A(adr_cb[0]), .Z(N403) );
  GTECH_OR2 C1152 ( .A(N403), .B(adr_cb[1]), .Z(N404) );
  GTECH_NOT I_3 ( .A(N404), .Z(N405) );
  GTECH_OR2 C1154 ( .A(adr_cb[0]), .B(adr_cb[1]), .Z(N406) );
  GTECH_NOT I_4 ( .A(N406), .Z(N407) );
  GTECH_NOT I_5 ( .A(sizd_c[0]), .Z(N408) );
  GTECH_OR2 C1157 ( .A(sizd_c[12]), .B(sizd_c[13]), .Z(N409) );
  GTECH_OR2 C1158 ( .A(sizd_c[11]), .B(N409), .Z(N410) );
  GTECH_OR2 C1159 ( .A(sizd_c[10]), .B(N410), .Z(N411) );
  GTECH_OR2 C1160 ( .A(sizd_c[9]), .B(N411), .Z(N412) );
  GTECH_OR2 C1161 ( .A(sizd_c[8]), .B(N412), .Z(N413) );
  GTECH_OR2 C1162 ( .A(sizd_c[7]), .B(N413), .Z(N414) );
  GTECH_OR2 C1163 ( .A(sizd_c[6]), .B(N414), .Z(N415) );
  GTECH_OR2 C1164 ( .A(sizd_c[5]), .B(N415), .Z(N416) );
  GTECH_OR2 C1165 ( .A(sizd_c[4]), .B(N416), .Z(N417) );
  GTECH_OR2 C1166 ( .A(sizd_c[3]), .B(N417), .Z(N418) );
  GTECH_OR2 C1167 ( .A(sizd_c[2]), .B(N418), .Z(N419) );
  GTECH_OR2 C1168 ( .A(sizd_c[1]), .B(N419), .Z(N420) );
  GTECH_OR2 C1169 ( .A(N408), .B(N420), .Z(N421) );
  GTECH_NOT I_6 ( .A(N421), .Z(N422) );
  GTECH_OR2 C1171 ( .A(sizd_c[12]), .B(sizd_c[13]), .Z(N423) );
  GTECH_OR2 C1172 ( .A(sizd_c[11]), .B(N423), .Z(N424) );
  GTECH_OR2 C1173 ( .A(sizd_c[10]), .B(N424), .Z(N425) );
  GTECH_OR2 C1174 ( .A(sizd_c[9]), .B(N425), .Z(N426) );
  GTECH_OR2 C1175 ( .A(sizd_c[8]), .B(N426), .Z(N427) );
  GTECH_OR2 C1176 ( .A(sizd_c[7]), .B(N427), .Z(N428) );
  GTECH_OR2 C1177 ( .A(sizd_c[6]), .B(N428), .Z(N429) );
  GTECH_OR2 C1178 ( .A(sizd_c[5]), .B(N429), .Z(N430) );
  GTECH_OR2 C1179 ( .A(sizd_c[4]), .B(N430), .Z(N431) );
  GTECH_OR2 C1180 ( .A(sizd_c[3]), .B(N431), .Z(N432) );
  GTECH_OR2 C1181 ( .A(sizd_c[2]), .B(N432), .Z(N433) );
  GTECH_OR2 C1182 ( .A(sizd_c[1]), .B(N433), .Z(N434) );
  GTECH_OR2 C1183 ( .A(sizd_c[0]), .B(N434), .Z(N435) );
  GTECH_NOT I_7 ( .A(N435), .Z(N436) );
  GTECH_OR2 C1185 ( .A(sizd_c[12]), .B(sizd_c[13]), .Z(N437) );
  GTECH_OR2 C1186 ( .A(sizd_c[11]), .B(N437), .Z(N438) );
  GTECH_OR2 C1187 ( .A(sizd_c[10]), .B(N438), .Z(N439) );
  GTECH_OR2 C1188 ( .A(sizd_c[9]), .B(N439), .Z(N440) );
  GTECH_OR2 C1189 ( .A(sizd_c[8]), .B(N440), .Z(N441) );
  GTECH_OR2 C1190 ( .A(sizd_c[7]), .B(N441), .Z(N442) );
  GTECH_OR2 C1191 ( .A(sizd_c[6]), .B(N442), .Z(N443) );
  GTECH_OR2 C1192 ( .A(sizd_c[5]), .B(N443), .Z(N444) );
  GTECH_OR2 C1193 ( .A(sizd_c[4]), .B(N444), .Z(N445) );
  GTECH_OR2 C1194 ( .A(sizd_c[3]), .B(N445), .Z(N446) );
  GTECH_OR2 C1195 ( .A(sizd_c[2]), .B(N446), .Z(N447) );
  GTECH_OR2 C1196 ( .A(sizd_c[1]), .B(N447), .Z(N448) );
  GTECH_OR2 C1197 ( .A(sizd_c[0]), .B(N448), .Z(N449) );
  GTECH_AND2 C1200 ( .A(adr_cb[0]), .B(adr_cb[1]), .Z(N450) );
  ADD_UNS_OP add_3427 ( .A(adr[14:0]), .B(buf_size), .Z({N59, N58, N57, N56, 
        N55, N54, N53, N52, N51, N50, N49, N48, N47, N46, N45}) );
  ADD_UNS_OP add_3448_S2 ( .A(adr_cb), .B(1'b1), .Z({N89, N88, N87}) );
  ADD_UNS_OP add_3434_S2 ( .A(madr), .B(1'b1), .Z({N78, N77, N76, N75, N74, 
        N73, N72, N71, N70, N69, N68, N67, N66, N65, N64}) );
  ADD_UNS_OP add_3484_S2 ( .A(sizu_c), .B(1'b1), .Z({N142, N141, N140, N139, 
        N138, N137, N136, N135, N134, N133, N132}) );
  SUB_UNS_OP sub_3464_S2 ( .A(sizd_c), .B(1'b1), .Z({N108, N107, N106, N105, 
        N104, N103, N102, N101, N100, N99, N98, N97, N96, N95}) );
  SELECT_OP C1213 ( .DATA1(adr[16:2]), .DATA2(adrw_next1), .CONTROL1(N0), 
        .CONTROL2(N29), .Z({N44, N43, N42, N41, N40, N39, N38, N37, N36, N35, 
        N34, N33, N32, N31, N30}) );
  GTECH_BUF B_0 ( .A(N28), .Z(N0) );
  SELECT_OP C1214 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), .DATA2(adrw_next), 
        .CONTROL1(N1), .CONTROL2(N62), .Z(adrw_next1) );
  GTECH_BUF B_1 ( .A(N61), .Z(N1) );
  SELECT_OP C1215 ( .DATA1({N78, N77, N76, N75, N74, N73, N72, N71, N70, N69, 
        N68, N67, N66, N65, N64}), .DATA2(madr), .CONTROL1(N2), .CONTROL2(N3), 
        .Z(adrw_next) );
  GTECH_BUF B_2 ( .A(adr_incw), .Z(N2) );
  GTECH_BUF B_3 ( .A(N63), .Z(N3) );
  SELECT_OP C1216 ( .DATA1({1'b0, 1'b0, 1'b0}), .DATA2(adr[2:0]), .DATA3(
        adrb_next), .CONTROL1(N4), .CONTROL2(N85), .CONTROL3(N81), .Z({N84, 
        N83, N82}) );
  GTECH_BUF B_4 ( .A(N238), .Z(N4) );
  SELECT_OP C1217 ( .DATA1({N89, N88, N87}), .DATA2(adr_cb), .CONTROL1(N5), 
        .CONTROL2(N6), .Z(adrb_next) );
  GTECH_BUF B_5 ( .A(adr_incb), .Z(N5) );
  GTECH_BUF B_6 ( .A(N86), .Z(N6) );
  SELECT_OP C1218 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N4), .CONTROL2(N124), .CONTROL3(N127), .CONTROL4(N93), .Z(
        N109) );
  SELECT_OP C1219 ( .DATA1({1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 
        1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1}), .DATA2(size), .DATA3({N108, N107, 
        N106, N105, N104, N103, N102, N101, N100, N99, N98, N97, N96, N95}), 
        .CONTROL1(N4), .CONTROL2(N124), .CONTROL3(N127), .Z({N123, N122, N121, 
        N120, N119, N118, N117, N116, N115, N114, N113, N112, N111, N110}) );
  SELECT_OP C1220 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N4), .CONTROL2(N155), .CONTROL3(N158), .CONTROL4(N130), .Z(
        N143) );
  SELECT_OP C1221 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0}), .DATA2({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0}), .DATA3({N142, N141, N140, N139, N138, N137, 
        N136, N135, N134, N133, N132}), .CONTROL1(N4), .CONTROL2(N155), 
        .CONTROL3(N158), .Z({N154, N153, N152, N151, N150, N149, N148, N147, 
        N146, N145, N144}) );
  SELECT_OP C1222 ( .DATA1({1'b1, 1'b1, 1'b1, 1'b1}), .DATA2({N399, N402, N405, 
        N407}), .DATA3({1'b0, 1'b0, 1'b0, 1'b0}), .CONTROL1(N7), .CONTROL2(
        N199), .CONTROL3(N161), .Z({N189, N180, N171, N162}) );
  GTECH_BUF B_7 ( .A(dtmp_sel_r), .Z(N7) );
  SELECT_OP C1223 ( .DATA1(mdin), .DATA2({rx_data_st_r, rx_data_st_r, 
        rx_data_st_r, rx_data_st_r}), .CONTROL1(N7), .CONTROL2(N199), .Z({N197, 
        N196, N195, N194, N193, N192, N191, N190, N188, N187, N186, N185, N184, 
        N183, N182, N181, N179, N178, N177, N176, N175, N174, N173, N172, N170, 
        N169, N168, N167, N166, N165, N164, N163}) );
  SELECT_OP C1224 ( .DATA1(rd_buf0[7:0]), .DATA2(rd_buf0[15:8]), .DATA3(
        rd_buf0[23:16]), .DATA4(rd_buf0[31:24]), .DATA5(rd_buf1[7:0]), .DATA6(
        rd_buf1[15:8]), .DATA7(rd_buf1[23:16]), .DATA8(rd_buf1[31:24]), 
        .CONTROL1(N8), .CONTROL2(N9), .CONTROL3(N10), .CONTROL4(N11), 
        .CONTROL5(N12), .CONTROL6(N13), .CONTROL7(N14), .CONTROL8(N15), .Z(
        tx_data_st) );
  GTECH_BUF B_8 ( .A(N207), .Z(N8) );
  GTECH_BUF B_9 ( .A(N210), .Z(N9) );
  GTECH_BUF B_10 ( .A(N213), .Z(N10) );
  GTECH_BUF B_11 ( .A(N216), .Z(N11) );
  GTECH_BUF B_12 ( .A(N219), .Z(N12) );
  GTECH_BUF B_13 ( .A(N222), .Z(N13) );
  GTECH_BUF B_14 ( .A(N225), .Z(N14) );
  GTECH_BUF B_15 ( .A(N227), .Z(N15) );
  SELECT_OP C1225 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N4), .CONTROL2(N234), .CONTROL3(N237), .CONTROL4(N231), .Z(
        N232) );
  SELECT_OP C1226 ( .DATA1(1'b0), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N4), 
        .CONTROL2(N234), .CONTROL3(N237), .Z(N233) );
  SELECT_OP C1227 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1}), 
        .DATA2(next_state), .CONTROL1(N4), .CONTROL2(N16), .Z({N246, N245, 
        N244, N243, N242, N241, N240, N239}) );
  GTECH_BUF B_16 ( .A(rst), .Z(N16) );
  GTECH_NOT I_8 ( .A(N321), .Z(N323) );
  SELECT_OP C1229 ( .DATA1({1'b0, 1'b1}), .DATA2({1'b1, 1'b0}), .CONTROL1(N17), 
        .CONTROL2(N361), .Z({N327, N326}) );
  GTECH_BUF B_17 ( .A(abort), .Z(N17) );
  SELECT_OP C1230 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b1), .CONTROL1(N17), 
        .CONTROL2(N361), .CONTROL3(N325), .Z(N328) );
  SELECT_OP C1231 ( .DATA1({1'b0, 1'b1}), .DATA2({1'b1, 1'b0}), .CONTROL1(N17), 
        .CONTROL2(N362), .Z({N332, N331}) );
  SELECT_OP C1232 ( .DATA1(1'b0), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N17), 
        .CONTROL2(N362), .CONTROL3(N330), .Z(N333) );
  SELECT_OP C1233 ( .DATA1({1'b0, 1'b1}), .DATA2({1'b1, 1'b0}), .DATA3({1'b0, 
        1'b1}), .CONTROL1(N17), .CONTROL2(N363), .CONTROL3(N366), .Z({N338, 
        N337}) );
  SELECT_OP C1234 ( .DATA1({1'b0, 1'b1}), .DATA2({1'b1, 1'b0}), .CONTROL1(N17), 
        .CONTROL2(N367), .Z({N342, N341}) );
  SELECT_OP C1235 ( .DATA1({1'b0, 1'b1}), .DATA2({1'b1, 1'b0}), .CONTROL1(N17), 
        .CONTROL2(N369), .Z({N346, N345}) );
  SELECT_OP C1236 ( .DATA1({1'b0, 1'b1}), .DATA2({1'b1, 1'b0}), .CONTROL1(N18), 
        .CONTROL2(N371), .Z({N352, N351}) );
  GTECH_BUF B_18 ( .A(N347), .Z(N18) );
  SELECT_OP C1237 ( .DATA1({1'b0, 1'b0, N321, 1'b0, 1'b0, 1'b0, N323, 1'b0}), 
        .DATA2({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, N327, 1'b0, N326}), .DATA3({1'b0, 
        1'b0, 1'b0, 1'b0, N332, 1'b0, 1'b0, N331}), .DATA4({1'b0, 1'b0, 1'b0, 
        N338, 1'b0, 1'b0, 1'b0, N337}), .DATA5({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b1}), .DATA6({1'b0, N342, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        N341}), .DATA7({N346, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, N345}), 
        .DATA8({1'b0, N352, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, N351}), .DATA9(state), .CONTROL1(N19), .CONTROL2(N20), .CONTROL3(N21), .CONTROL4(N22), .CONTROL5(
        N23), .CONTROL6(N24), .CONTROL7(N25), .CONTROL8(N26), .CONTROL9(N360), 
        .Z(next_state) );
  GTECH_BUF B_19 ( .A(N255), .Z(N19) );
  GTECH_BUF B_20 ( .A(N264), .Z(N20) );
  GTECH_BUF B_21 ( .A(N273), .Z(N21) );
  GTECH_BUF B_22 ( .A(N282), .Z(N22) );
  GTECH_BUF B_23 ( .A(N291), .Z(N23) );
  GTECH_BUF B_24 ( .A(N300), .Z(N24) );
  GTECH_BUF B_25 ( .A(N309), .Z(N25) );
  GTECH_BUF B_26 ( .A(N318), .Z(N26) );
  SELECT_OP C1238 ( .DATA1(1'b0), .DATA2(N328), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b1), .DATA7(1'b1), .DATA8(1'b0), .DATA9(1'b0), 
        .CONTROL1(N19), .CONTROL2(N20), .CONTROL3(N21), .CONTROL4(N22), 
        .CONTROL5(N23), .CONTROL6(N24), .CONTROL7(N25), .CONTROL8(N26), 
        .CONTROL9(N360), .Z(mreq_d) );
  SELECT_OP C1239 ( .DATA1(1'b0), .DATA2(N328), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .CONTROL1(N19), .CONTROL2(N20), .CONTROL3(N21), .CONTROL4(N22), 
        .CONTROL5(N23), .CONTROL6(N24), .CONTROL7(N25), .CONTROL8(N26), 
        .CONTROL9(N360), .Z(dtmp_sel) );
  SELECT_OP C1240 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b1), .DATA4(1'b1), 
        .DATA5(1'b1), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .CONTROL1(N19), .CONTROL2(N20), .CONTROL3(N21), .CONTROL4(N22), 
        .CONTROL5(N23), .CONTROL6(N24), .CONTROL7(N25), .CONTROL8(N26), 
        .CONTROL9(N360), .Z(mwe_d) );
  SELECT_OP C1241 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(N333), .DATA4(1'b1), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .CONTROL1(N19), .CONTROL2(N20), .CONTROL3(N21), .CONTROL4(N22), 
        .CONTROL5(N23), .CONTROL6(N24), .CONTROL7(N25), .CONTROL8(N26), 
        .CONTROL9(N360), .Z(wr_last_en) );
  SELECT_OP C1242 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(mack_r), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .CONTROL1(N19), .CONTROL2(N20), .CONTROL3(N21), .CONTROL4(N22), 
        .CONTROL5(N23), .CONTROL6(N24), .CONTROL7(N25), .CONTROL8(N26), 
        .CONTROL9(N360), .Z(rd_first) );
  GTECH_OR2 C1245 ( .A(N452), .B(word_done_r), .Z(mreq) );
  GTECH_AND2 C1246 ( .A(mreq_d), .B(N451), .Z(N452) );
  GTECH_NOT I_9 ( .A(mack_r), .Z(N451) );
  GTECH_AND2 C1248 ( .A(mreq), .B(mack), .Z(N27) );
  GTECH_OR2 C1249 ( .A(rx_dma_en_r), .B(tx_dma_en_r), .Z(N28) );
  GTECH_NOT I_10 ( .A(N28), .Z(N29) );
  GTECH_AND2 C1252 ( .A(N60), .B(dma_en), .Z(N61) );
  GTECH_NOT I_11 ( .A(N61), .Z(N62) );
  GTECH_NOT I_12 ( .A(adr_incw), .Z(N63) );
  GTECH_BUF B_27 ( .A(adr_incw) );
  GTECH_OR2 C1259 ( .A(rx_dma_en_r), .B(tx_dma_en_r), .Z(N79) );
  GTECH_OR2 C1261 ( .A(N79), .B(N238), .Z(N80) );
  GTECH_NOT I_13 ( .A(N80), .Z(N81) );
  GTECH_AND2 C1264 ( .A(N79), .B(rst), .Z(N85) );
  GTECH_NOT I_14 ( .A(adr_incb), .Z(N86) );
  GTECH_BUF B_28 ( .A(adr_incb) );
  GTECH_OR2 C1269 ( .A(rx_data_valid_r), .B(rd_next), .Z(adr_incb) );
  GTECH_AND2 C1270 ( .A(N453), .B(mack_r), .Z(adr_incw) );
  GTECH_NOT I_15 ( .A(dtmp_sel_r), .Z(N453) );
  GTECH_OR2 C1272 ( .A(tx_dma_en), .B(tx_dma_en_r), .Z(N90) );
  GTECH_OR2 C1275 ( .A(N90), .B(N238), .Z(N91) );
  GTECH_OR2 C1276 ( .A(siz_dec), .B(N91), .Z(N92) );
  GTECH_NOT I_16 ( .A(N92), .Z(N93) );
  GTECH_BUF B_29 ( .A(N127), .Z(N94) );
  GTECH_AND2 C1279 ( .A(N90), .B(rst), .Z(N124) );
  GTECH_NOT I_17 ( .A(N90), .Z(N125) );
  GTECH_AND2 C1281 ( .A(rst), .B(N125), .Z(N126) );
  GTECH_AND2 C1282 ( .A(siz_dec), .B(N126), .Z(N127) );
  GTECH_AND2 C1283 ( .A(N94), .B(N126) );
  GTECH_OR2 C1284 ( .A(N454), .B(N455), .Z(siz_dec) );
  GTECH_AND2 C1285 ( .A(rd_first), .B(mack_r), .Z(N454) );
  GTECH_AND2 C1286 ( .A(rd_next), .B(N449), .Z(N455) );
  GTECH_OR2 C1289 ( .A(rx_dma_en_r), .B(N238), .Z(N128) );
  GTECH_OR2 C1290 ( .A(rx_data_valid_r), .B(N128), .Z(N129) );
  GTECH_NOT I_18 ( .A(N129), .Z(N130) );
  GTECH_BUF B_30 ( .A(N158), .Z(N131) );
  GTECH_AND2 C1293 ( .A(rx_dma_en_r), .B(rst), .Z(N155) );
  GTECH_NOT I_19 ( .A(rx_dma_en_r), .Z(N156) );
  GTECH_AND2 C1295 ( .A(rst), .B(N156), .Z(N157) );
  GTECH_AND2 C1296 ( .A(rx_data_valid_r), .B(N157), .Z(N158) );
  GTECH_AND2 C1297 ( .A(N131), .B(N157) );
  GTECH_OR2 C1298 ( .A(rx_data_done_r), .B(N436), .Z(N159) );
  GTECH_OR2 C1301 ( .A(rx_data_valid_r), .B(dtmp_sel_r), .Z(N160) );
  GTECH_NOT I_20 ( .A(N160), .Z(N161) );
  GTECH_NOT I_21 ( .A(dtmp_sel_r), .Z(N198) );
  GTECH_AND2 C1308 ( .A(rx_data_valid_r), .B(N198), .Z(N199) );
  GTECH_OR2 C1309 ( .A(N456), .B(wr_last), .Z(N200) );
  GTECH_AND2 C1310 ( .A(N397), .B(rx_data_valid_r), .Z(N456) );
  GTECH_AND2 C1311 ( .A(word_done), .B(N457), .Z(N201) );
  GTECH_NOT I_22 ( .A(word_done_r), .Z(N457) );
  GTECH_AND2 C1314 ( .A(N459), .B(wr_last_en), .Z(N202) );
  GTECH_AND2 C1315 ( .A(N398), .B(N458), .Z(N459) );
  GTECH_NOT I_23 ( .A(rx_data_valid_r), .Z(N458) );
  GTECH_NOT I_24 ( .A(adrb_next[2]), .Z(N203) );
  GTECH_NOT I_25 ( .A(adrb_next[1]), .Z(N204) );
  GTECH_NOT I_26 ( .A(adrb_next[0]), .Z(N205) );
  GTECH_NOT I_27 ( .A(N209), .Z(N210) );
  GTECH_NOT I_28 ( .A(N212), .Z(N213) );
  GTECH_NOT I_29 ( .A(N215), .Z(N216) );
  GTECH_NOT I_30 ( .A(N218), .Z(N219) );
  GTECH_NOT I_31 ( .A(N221), .Z(N222) );
  GTECH_NOT I_32 ( .A(N224), .Z(N225) );
  GTECH_AND2 C1345 ( .A(N460), .B(mack_r), .Z(fill_buf0) );
  GTECH_NOT I_33 ( .A(madr[0]), .Z(N460) );
  GTECH_AND2 C1347 ( .A(madr[0]), .B(mack_r), .Z(fill_buf1) );
  GTECH_OR2 C1348 ( .A(N461), .B(N436), .Z(N228) );
  GTECH_AND2 C1349 ( .A(N422), .B(rd_next), .Z(N461) );
  GTECH_OR2 C1352 ( .A(rd_first), .B(N238), .Z(N229) );
  GTECH_OR2 C1353 ( .A(N228), .B(N229), .Z(N230) );
  GTECH_NOT I_34 ( .A(N230), .Z(N231) );
  GTECH_AND2 C1355 ( .A(rd_first), .B(rst), .Z(N234) );
  GTECH_NOT I_35 ( .A(rd_first), .Z(N235) );
  GTECH_AND2 C1357 ( .A(rst), .B(N235), .Z(N236) );
  GTECH_AND2 C1358 ( .A(N228), .B(N236), .Z(N237) );
  GTECH_OR2 C1359 ( .A(send_data_r), .B(send_zero_length_r), .Z(send_data) );
  GTECH_NOT I_36 ( .A(rst), .Z(N238) );
  GTECH_NOT I_37 ( .A(state[0]), .Z(N247) );
  GTECH_NOT I_38 ( .A(N254), .Z(N255) );
  GTECH_NOT I_39 ( .A(state[1]), .Z(N256) );
  GTECH_NOT I_40 ( .A(N263), .Z(N264) );
  GTECH_NOT I_41 ( .A(state[2]), .Z(N265) );
  GTECH_NOT I_42 ( .A(N272), .Z(N273) );
  GTECH_NOT I_43 ( .A(state[3]), .Z(N274) );
  GTECH_NOT I_44 ( .A(N281), .Z(N282) );
  GTECH_NOT I_45 ( .A(state[4]), .Z(N283) );
  GTECH_NOT I_46 ( .A(N290), .Z(N291) );
  GTECH_NOT I_47 ( .A(state[5]), .Z(N292) );
  GTECH_NOT I_48 ( .A(N299), .Z(N300) );
  GTECH_NOT I_49 ( .A(state[6]), .Z(N301) );
  GTECH_NOT I_50 ( .A(N308), .Z(N309) );
  GTECH_NOT I_51 ( .A(state[7]), .Z(N310) );
  GTECH_NOT I_52 ( .A(N317), .Z(N318) );
  GTECH_AND2 C1387 ( .A(rx_dma_en_r), .B(N462), .Z(N319) );
  GTECH_NOT I_53 ( .A(abort), .Z(N462) );
  GTECH_NOT I_54 ( .A(N319), .Z(N320) );
  GTECH_AND2 C1391 ( .A(N463), .B(N464), .Z(N321) );
  GTECH_AND2 C1392 ( .A(tx_dma_en_r), .B(N462), .Z(N463) );
  GTECH_NOT I_55 ( .A(send_zero_length_r), .Z(N464) );
  GTECH_NOT I_56 ( .A(N321), .Z(N322) );
  GTECH_OR2 C1398 ( .A(mack_r), .B(abort), .Z(N324) );
  GTECH_NOT I_57 ( .A(N324), .Z(N325) );
  GTECH_OR2 C1401 ( .A(rx_data_done_r2), .B(abort), .Z(N329) );
  GTECH_NOT I_58 ( .A(N329), .Z(N330) );
  GTECH_OR2 C1405 ( .A(wr_last), .B(abort), .Z(N334) );
  GTECH_OR2 C1406 ( .A(wr_done), .B(N334), .Z(N335) );
  GTECH_NOT I_59 ( .A(N335), .Z(N336) );
  GTECH_OR2 C1412 ( .A(mack_r), .B(abort), .Z(N339) );
  GTECH_NOT I_60 ( .A(N339), .Z(N340) );
  GTECH_OR2 C1416 ( .A(mack_r), .B(abort), .Z(N343) );
  GTECH_NOT I_61 ( .A(N343), .Z(N344) );
  GTECH_OR2 C1418 ( .A(sizd_is_zero), .B(abort), .Z(N347) );
  GTECH_AND2 C1419 ( .A(N450), .B(rd_next), .Z(N348) );
  GTECH_OR2 C1422 ( .A(N348), .B(N347), .Z(N349) );
  GTECH_NOT I_62 ( .A(N349), .Z(N350) );
  GTECH_OR2 C1424 ( .A(N264), .B(N255), .Z(N353) );
  GTECH_OR2 C1425 ( .A(N273), .B(N353), .Z(N354) );
  GTECH_OR2 C1426 ( .A(N282), .B(N354), .Z(N355) );
  GTECH_OR2 C1427 ( .A(N291), .B(N355), .Z(N356) );
  GTECH_OR2 C1428 ( .A(N300), .B(N356), .Z(N357) );
  GTECH_OR2 C1429 ( .A(N309), .B(N357), .Z(N358) );
  GTECH_OR2 C1430 ( .A(N318), .B(N358), .Z(N359) );
  GTECH_NOT I_63 ( .A(N359), .Z(N360) );
  GTECH_AND2 C1432 ( .A(mack_r), .B(N368), .Z(N361) );
  GTECH_AND2 C1433 ( .A(rx_data_done_r2), .B(N368), .Z(N362) );
  GTECH_AND2 C1434 ( .A(wr_last), .B(N368), .Z(N363) );
  GTECH_NOT I_64 ( .A(wr_last), .Z(N364) );
  GTECH_AND2 C1436 ( .A(N368), .B(N364), .Z(N365) );
  GTECH_AND2 C1437 ( .A(wr_done), .B(N365), .Z(N366) );
  GTECH_AND2 C1438 ( .A(mack_r), .B(N368), .Z(N367) );
  GTECH_NOT I_65 ( .A(abort), .Z(N368) );
  GTECH_AND2 C1440 ( .A(mack_r), .B(N368), .Z(N369) );
  GTECH_NOT I_66 ( .A(N347), .Z(N370) );
  GTECH_AND2 C1442 ( .A(N348), .B(N370), .Z(N371) );
  GTECH_AND2 C1443 ( .A(N255), .B(rst), .Z(N372) );
  GTECH_AND2 C1444 ( .A(N322), .B(N372), .Z(N373) );
  GTECH_AND2 C1445 ( .A(N320), .B(N373), .Z(N374) );
  GTECH_AND2 C1446 ( .A(N264), .B(rst), .Z(N375) );
  GTECH_AND2 C1447 ( .A(N325), .B(N375), .Z(N376) );
  GTECH_OR2 C1448 ( .A(N374), .B(N376), .Z(N377) );
  GTECH_AND2 C1449 ( .A(N273), .B(rst), .Z(N378) );
  GTECH_AND2 C1450 ( .A(N330), .B(N378), .Z(N379) );
  GTECH_OR2 C1451 ( .A(N377), .B(N379), .Z(N380) );
  GTECH_AND2 C1452 ( .A(N282), .B(rst), .Z(N381) );
  GTECH_AND2 C1453 ( .A(N336), .B(N381), .Z(N382) );
  GTECH_OR2 C1454 ( .A(N380), .B(N382), .Z(N383) );
  GTECH_AND2 C1455 ( .A(N291), .B(rst), .Z(N384) );
  GTECH_AND2 C1456 ( .A(N451), .B(N384), .Z(N385) );
  GTECH_OR2 C1457 ( .A(N383), .B(N385), .Z(N386) );
  GTECH_AND2 C1458 ( .A(N300), .B(rst), .Z(N387) );
  GTECH_AND2 C1459 ( .A(N340), .B(N387), .Z(N388) );
  GTECH_OR2 C1460 ( .A(N386), .B(N388), .Z(N389) );
  GTECH_AND2 C1461 ( .A(N309), .B(rst), .Z(N390) );
  GTECH_AND2 C1462 ( .A(N344), .B(N390), .Z(N391) );
  GTECH_OR2 C1463 ( .A(N389), .B(N391), .Z(N392) );
  GTECH_AND2 C1464 ( .A(N318), .B(rst), .Z(N393) );
  GTECH_AND2 C1465 ( .A(N350), .B(N393), .Z(N394) );
  GTECH_OR2 C1466 ( .A(N392), .B(N394), .Z(N395) );
  GTECH_NOT I_67 ( .A(N395), .Z(N396) );
endmodule


module usbf_pe_SSRAM_HADR14 ( clk, rst, tx_valid, rx_active, pid_OUT, pid_IN, 
        pid_SOF, pid_SETUP, pid_DATA0, pid_DATA1, pid_DATA2, pid_MDATA, 
        pid_ACK, pid_NACK, pid_STALL, pid_NYET, pid_PRE, pid_ERR, pid_SPLIT, 
        pid_PING, mode_hs, token_valid, crc5_err, rx_data_valid, rx_data_done, 
        crc16_err, send_token, token_pid_sel, data_pid_sel, send_zero_length, 
        rx_dma_en, tx_dma_en, abort, idma_done, adr, size, buf_size, sizu_c, 
        dma_en, fsel, idin, dma_in_buf_sz1, dma_out_buf_avail, ep_sel, match, 
        nse_err, buf0_rl, buf0_set, buf1_set, uc_bsel_set, uc_dpd_set, 
        int_buf1_set, int_buf0_set, int_upid_set, int_crc16_set, int_to_set, 
        int_seqerr_set, out_to_small, csr, buf0, buf1 );
  output [1:0] token_pid_sel;
  output [1:0] data_pid_sel;
  output [16:0] adr;
  output [13:0] size;
  output [13:0] buf_size;
  input [10:0] sizu_c;
  output [31:0] idin;
  input [3:0] ep_sel;
  input [31:0] csr;
  input [31:0] buf0;
  input [31:0] buf1;
  input clk, rst, tx_valid, rx_active, pid_OUT, pid_IN, pid_SOF, pid_SETUP,
         pid_DATA0, pid_DATA1, pid_DATA2, pid_MDATA, pid_ACK, pid_NACK,
         pid_STALL, pid_NYET, pid_PRE, pid_ERR, pid_SPLIT, pid_PING, mode_hs,
         token_valid, crc5_err, rx_data_valid, rx_data_done, crc16_err,
         idma_done, fsel, dma_in_buf_sz1, dma_out_buf_avail, match;
  output send_token, send_zero_length, rx_dma_en, tx_dma_en, abort, dma_en,
         nse_err, buf0_rl, buf0_set, buf1_set, uc_bsel_set, uc_dpd_set,
         int_buf1_set, int_buf0_set, int_upid_set, int_crc16_set, int_to_set,
         int_seqerr_set, out_to_small;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15,
         N16, N17, N18, N19, N20, N21, N22, N23, N24, N25, N26, N27, N28, N29,
         N30, N31, N32, N33, N34, N35, N36, N37, N38, N39, N40, N41, N42, N43,
         N44, N45, N46, N47, N48, N49, N50, N51, N52, N53, N54, N55, N56, N57,
         N58, N59, N60, N61, N62, N63, N64, N65, N66, N67, N68, N69, N70, N71,
         N72, N73, N74, N75, N76, N77, N78, N79, N80, N81, N82, N83, N84, N85,
         N86, N87, buf0_na, N88, buf1_na, N89, buf0_not_aloc, N90,
         buf1_not_aloc, match_r, N91, send_token_d, N92, N93, N94, N95, N96,
         N97, N98, N99, N100, N101, N102, N103, N104, N105, N106, N107, N108,
         N109, N110, N111, N112, N113, N114, N115, N116, N117, N118, N119,
         N120, N121, N122, N123, N124, N125, N126, N127, N128, N129, N130,
         N131, N132, N133, N134, N135, N136, N137, N138, N139, N140, N141,
         N142, N143, N144, N145, N146, N147, N148, N149, N150, N151, N152,
         N153, N154, N155, N156, N157, N158, N159, N160, N161, N162, N163,
         N164, N165, N166, N167, N168, N169, N170, N171, N172, N173, N174,
         N175, N176, N177, N178, N179, N180, N181, N182, N183, setup_token,
         in_op, out_op, N184, N185, N186, N187, N188, N189, N190, N191, N192,
         N193, N194, N195, N196, N197, N198, N199, N200, N201, N202, N203,
         N204, N205, N206, N207, N208, N209, N210, N211, N212, N213, N214,
         N215, N216, N217, N218, N219, N220, N221, N222, N223, N224, N225,
         N226, N227, N228, N229, N230, N231, N232, N233, N234, N235, N236,
         N237, N238, N239, N240, N241, N242, N243, N244, N245, N246, N247,
         N248, N249, N250, N251, N252, N253, N254, N255, N256, N257, N258,
         N259, N260, N261, N262, N263, N264, N265, N266, N267, N268, N269,
         N270, N271, N272, N273, N274, N275, N276, N277, N278, N279, N280,
         N281, N282, N283, N284, N285, N286, N287, N288, N289, N290, N291,
         N292, N293, N294, N295, N296, N297, N298, N299, N300, N301, N302,
         N303, N304, N305, N306, N307, N308, N309, N310, N311, N312, N313,
         N314, N315, N316, N317, N318, N319, N320, N321, N322, N323, N324,
         N325, N326, N327, N328, N329, N330, N331, N332, N333, N334, N335,
         N336, N337, N338, N339, N340, N341, N342, N343, pid_seq_err, N344,
         N345, N346, N347, N348, in_token, N349, N350, N351, N352, N353, N354,
         N355, N356, N357, out_token, N358, N359, N360, N361, N362, N363, N364,
         N365, N366, N367, N368, N369, N370, N371, N372, N373, N374, buf_sel,
         N375, N376, N377, N378, N379, N380, N381, N382, N383, N384, N385,
         N386, N387, N388, N389, N390, N391, N392, N393, N394, buf_smaller,
         N395, N396, buffer_full, buffer_empty, buffer_done, N397, no_buf0_dma,
         N398, buf0_st_max, N399, buf1_st_max, N400, N401, N402, no_bufs0,
         N403, N404, no_bufs1, no_bufs, N405, N406, N407, N408, N409, N410,
         N411, N412, N413, N414, N415, N416, N417, N418, N419, N420, N421,
         N422, N423, N424, N425, N426, N427, N428, N429, N430, N431, N432,
         N433, N434, N435, N436, N437, N438, N439, N440, N441, N442, N443,
         N444, N445, N446, N447, N448, N449, N450, N451, N452, N453, N454,
         N455, N456, N457, N458, N459, N460, N461, N462, N463, N464, N465,
         N466, N467, N468, N469, buffer_overflow, uc_stat_set_d, N470, N471,
         out_to_small_r, N472, N473, to_small, N474, N475, to_large, N476,
         N477, N478, N479, N480, N481, N482, N483, N484, N485, N486, N487,
         N488, N489, N490, N491, N492, N493, N494, N495, N496, N497, N498,
         N499, N500, N501, N502, N503, N504, N505, N506, N507, N508, N509,
         N510, N511, buf_set_d, N512, N513, N514, N515, N516, N517, N518, N519,
         N520, N521, buf0_rl_d, N522, rx_ack_to_clr_d, N523, rx_ack_to_clr,
         N524, N525, N526, N527, N528, N529, N530, N531, N532, N533, N534,
         N535, N536, N537, N538, N539, N540, N541, rx_ack_to, N542, N543, N544,
         N545, N546, N547, N548, N549, N550, N551, N552, N553, N554, N555,
         N556, N557, N558, N559, N560, tx_data_to, int_set_en, pid_OUT_r,
         pid_IN_r, pid_PING_r, pid_SETUP_r, N561, int_seqerr_set_d, N562, N563,
         N564, N565, N566, N567, N568, N569, N570, N571, N572, N573, N574,
         N575, N576, N577, N578, N579, N580, N581, N582, N583, N584, N585,
         N586, N587, N588, N589, N590, N591, N592, N593, N594, N595, N596,
         N597, N598, N599, N600, N601, N602, N603, N604, N605, N606, N607,
         N608, N609, N610, N611, N612, N613, N614, N615, N616, N617, N618,
         N619, N620, N621, N622, N623, N624, N625, N626, N627, N628, N629,
         N630, N631, N632, N633, N634, N635, N636, N637, N638, N639, N640,
         N641, N642, N643, N644, N645, N646, N647, N648, N649, N650, N651,
         N652, N653, N654, N655, N656, N657, N658, N659, N660, N661, N662,
         N663, N664, N665, N666, N667, N668, N669, N670, N671, N672, N673,
         N674, N675, N676, N677, N678, N679, N680, N681, N682, N683, N684,
         N685, N686, N687, N688, N689, N690, N691, N692, N693, N694, N695,
         N696, N697, N698, N699, N700, N701, N702, N703, N704, N705, N706,
         N707, N708, N709, N710, N711, N712, N713, N714, N715, N716, N717,
         N718, N719, N720, N721, N722, N723, N724, N725, N726, N727, N728,
         N729, N730, N731, N732, N733, N734, N735, N736, N737, N738, N739,
         N740, N741, N742, N743, N744, N745, N746, N747, N748, N749, N750,
         N751, N752, N753, N754, N755, N756, N757, N758, N759, N760, N761,
         N762, N763, N764, N765, N766, N767, N768, N769, N770, N771, N772,
         N773, N774, N775, N776, N777, N778, N779, N780, N781, N782, N783,
         N784, N785, N786, N787, N788, N789, N790, N791, N792, N793, N794,
         N795, N796, N797, N798, N799, N800, N801, N802, N803, N804, N805,
         N806, N807, N808, N809, N810, N811, N812, N813, N814, N815, N816,
         N817, N818, N819, N820, N821, N822, N823, N824, N825, N826, N827,
         N828, N829, N830, N831, N832, N833, N834, N835, N836, N837, N838,
         N839, N840, N841, N842, N843, N844, N845, N846, N847, N848, N849,
         N850, N851, N852, N853, N854, N855, N856, N857, N858, N859, N860,
         N861, N862, N863, N864, N865, N866, N867, N868, N869, N870, N871,
         N872, N873, N874, N875, N876, N877, N878, N879, N880, N881, N882,
         N883, N884, N885, N886, N887, N888, N889, N890, N891, N892, N893,
         N894, N895, N896, N897, N898, N899, N900, N901, N902, N903, N904,
         N905, N906, N907, N908, N909, N910, N911, N912, N913, N914, N915,
         N916, N917, N918, N919, N920, N921, N922, N923, N924, N925, N926,
         N927, N928, N929, N930, N931, N932, N933, N934, N935, N936, N937,
         N938, N939, N940, N941, N942, N943, N944, N945, N946, N947, N948,
         N949, N950, N951, N952, N953, N954, N955, N956, N957, N958, N959,
         N960, N961, N962, N963, N964, N965, N966, N967, N968, N969, N970,
         N971, N972, N973, N974, N975, N976, N977, N978, N979, N980, N981,
         N982, N983, N984, N985, N986, N987, N988, N989, N990, N991, N992,
         N993;
  wire   [1:0] token_pid_sel_d;
  wire   [1:0] tr_fr_d;
  wire   [1:0] next_dpid;
  wire   [1:0] allow_pid;
  wire   [13:0] new_size;
  wire   [13:0] new_sizeb;
  wire   [16:0] adr_r;
  wire   [13:0] size_next_r;
  wire   [16:0] new_adr;
  wire   [1:0] next_bsel;
  wire   [9:0] state;
  wire   [7:0] rx_ack_to_cnt;
  wire   [7:0] tx_data_to_cnt;
  wire   [5:4] tx_data_to_val;

  \**SEQGEN**  buf0_na_reg ( .clear(1'b0), .preset(1'b0), .next_state(N87), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0_na), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  buf1_na_reg ( .clear(1'b0), .preset(1'b0), .next_state(N88), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1_na), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  buf0_not_aloc_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N89), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_not_aloc), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  buf1_not_aloc_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N90), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf1_not_aloc), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  match_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(match), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(match_r), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  nse_err_reg ( .clear(1'b0), .preset(1'b0), .next_state(N91), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(nse_err), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  send_token_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        send_token_d), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        send_token), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \token_pid_sel_reg[1]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(token_pid_sel_d[1]), .clocked_on(clk), .data_in(1'b0), 
        .enable(1'b0), .Q(token_pid_sel[1]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \token_pid_sel_reg[0]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(token_pid_sel_d[0]), .clocked_on(clk), .data_in(1'b0), 
        .enable(1'b0), .Q(token_pid_sel[0]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  GTECH_AND2 C20 ( .A(N92), .B(N865), .Z(N93) );
  GTECH_AND2 C21 ( .A(csr[26]), .B(N148), .Z(N94) );
  GTECH_AND2 C22 ( .A(N93), .B(N94), .Z(N95) );
  GTECH_AND2 C23 ( .A(N95), .B(csr[24]), .Z(N96) );
  GTECH_AND2 C28 ( .A(tr_fr_d[1]), .B(N97), .Z(N98) );
  GTECH_AND2 C31 ( .A(N98), .B(N158), .Z(N99) );
  GTECH_AND2 C33 ( .A(N99), .B(N282), .Z(N100) );
  GTECH_AND2 C37 ( .A(tr_fr_d[1]), .B(N97), .Z(N101) );
  GTECH_AND2 C40 ( .A(N101), .B(N158), .Z(N102) );
  GTECH_AND2 C42 ( .A(N102), .B(N287), .Z(N103) );
  GTECH_OR2 C47 ( .A(N92), .B(N97), .Z(N104) );
  GTECH_OR2 C51 ( .A(N104), .B(N113), .Z(N105) );
  GTECH_OR2 C53 ( .A(N105), .B(N128), .Z(N106) );
  GTECH_OR2 C60 ( .A(N92), .B(N97), .Z(N108) );
  GTECH_OR2 C64 ( .A(N108), .B(N113), .Z(N109) );
  GTECH_OR2 C66 ( .A(N109), .B(N134), .Z(N110) );
  GTECH_OR2 C73 ( .A(N92), .B(N97), .Z(N112) );
  GTECH_OR2 C74 ( .A(csr[27]), .B(N862), .Z(N113) );
  GTECH_OR2 C77 ( .A(N112), .B(N113), .Z(N114) );
  GTECH_OR2 C79 ( .A(N114), .B(N142), .Z(N115) );
  GTECH_AND2 C84 ( .A(N92), .B(csr[27]), .Z(N117) );
  GTECH_AND2 C85 ( .A(N862), .B(N148), .Z(N118) );
  GTECH_AND2 C86 ( .A(N117), .B(N118), .Z(N119) );
  GTECH_AND2 C87 ( .A(N119), .B(csr[24]), .Z(N120) );
  GTECH_OR2 C91 ( .A(N92), .B(tr_fr_d[0]), .Z(N121) );
  GTECH_OR2 C94 ( .A(N121), .B(N138), .Z(N122) );
  GTECH_OR2 C95 ( .A(N122), .B(N139), .Z(N123) );
  GTECH_OR2 C101 ( .A(N92), .B(N97), .Z(N125) );
  GTECH_OR2 C104 ( .A(csr[29]), .B(csr[28]), .Z(N126) );
  GTECH_OR2 C105 ( .A(N125), .B(N138), .Z(N127) );
  GTECH_OR2 C106 ( .A(N139), .B(N126), .Z(N128) );
  GTECH_OR2 C107 ( .A(N127), .B(N128), .Z(N129) );
  GTECH_OR2 C114 ( .A(N92), .B(N97), .Z(N131) );
  GTECH_OR2 C117 ( .A(csr[29]), .B(N195), .Z(N132) );
  GTECH_OR2 C118 ( .A(N131), .B(N138), .Z(N133) );
  GTECH_OR2 C119 ( .A(N139), .B(N132), .Z(N134) );
  GTECH_OR2 C120 ( .A(N133), .B(N134), .Z(N135) );
  GTECH_OR2 C127 ( .A(N92), .B(N97), .Z(N137) );
  GTECH_OR2 C128 ( .A(N865), .B(csr[26]), .Z(N138) );
  GTECH_OR2 C129 ( .A(csr[25]), .B(N854), .Z(N139) );
  GTECH_OR2 C130 ( .A(N189), .B(csr[28]), .Z(N140) );
  GTECH_OR2 C131 ( .A(N137), .B(N138), .Z(N141) );
  GTECH_OR2 C132 ( .A(N139), .B(N140), .Z(N142) );
  GTECH_OR2 C133 ( .A(N141), .B(N142), .Z(N143) );
  GTECH_AND2 C142 ( .A(N150), .B(N195), .Z(N145) );
  GTECH_AND2 C150 ( .A(N152), .B(N195), .Z(N146) );
  GTECH_AND2 C156 ( .A(N148), .B(N854), .Z(N149) );
  GTECH_AND2 C157 ( .A(N158), .B(N149), .Z(N150) );
  GTECH_AND2 C158 ( .A(N150), .B(csr[28]), .Z(N151) );
  GTECH_AND2 C164 ( .A(N162), .B(N149), .Z(N152) );
  GTECH_AND2 C165 ( .A(N152), .B(csr[28]), .Z(N153) );
  GTECH_AND2 C173 ( .A(N160), .B(N195), .Z(N155) );
  GTECH_AND2 C180 ( .A(N163), .B(N195), .Z(N156) );
  GTECH_AND2 C184 ( .A(N865), .B(csr[26]), .Z(N158) );
  GTECH_AND2 C185 ( .A(csr[25]), .B(N854), .Z(N159) );
  GTECH_AND2 C186 ( .A(N158), .B(N159), .Z(N160) );
  GTECH_AND2 C187 ( .A(N160), .B(csr[28]), .Z(N161) );
  GTECH_AND2 C190 ( .A(csr[27]), .B(N862), .Z(N162) );
  GTECH_AND2 C192 ( .A(N162), .B(N159), .Z(N163) );
  GTECH_AND2 C193 ( .A(N163), .B(csr[28]), .Z(N164) );
  GTECH_AND2 C197 ( .A(N865), .B(N862), .Z(N166) );
  GTECH_OR2 C223 ( .A(N167), .B(pid_DATA1), .Z(N168) );
  GTECH_OR2 C226 ( .A(pid_MDATA), .B(N170), .Z(N171) );
  GTECH_OR2 C240 ( .A(N167), .B(pid_DATA2), .Z(N176) );
  GTECH_OR2 C243 ( .A(pid_MDATA), .B(N178), .Z(N179) );
  GTECH_OR2 C295 ( .A(setup_token), .B(N184), .Z(N185) );
  GTECH_OR2 C296 ( .A(out_op), .B(csr[29]), .Z(N186) );
  GTECH_OR2 C297 ( .A(N185), .B(N186), .Z(N187) );
  GTECH_OR2 C301 ( .A(setup_token), .B(N184), .Z(N190) );
  GTECH_OR2 C302 ( .A(out_op), .B(N189), .Z(N191) );
  GTECH_OR2 C303 ( .A(N190), .B(N191), .Z(N192) );
  GTECH_AND2 C308 ( .A(N194), .B(N184), .Z(N196) );
  GTECH_AND2 C309 ( .A(out_op), .B(N195), .Z(N197) );
  GTECH_AND2 C310 ( .A(N196), .B(N197), .Z(N198) );
  GTECH_AND2 C313 ( .A(N194), .B(N184), .Z(N199) );
  GTECH_AND2 C314 ( .A(out_op), .B(csr[28]), .Z(N200) );
  GTECH_AND2 C315 ( .A(N199), .B(N200), .Z(N201) );
  \**SEQGEN**  \next_dpid_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N228), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        next_dpid[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N226) );
  \**SEQGEN**  \next_dpid_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N227), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        next_dpid[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N226) );
  GTECH_OR2 C356 ( .A(N229), .B(pid_DATA1), .Z(N230) );
  GTECH_OR2 C358 ( .A(N230), .B(N234), .Z(N231) );
  GTECH_OR2 C361 ( .A(pid_DATA0), .B(N170), .Z(N233) );
  GTECH_OR2 C362 ( .A(pid_DATA2), .B(pid_MDATA), .Z(N234) );
  GTECH_OR2 C363 ( .A(N233), .B(N234), .Z(N235) );
  GTECH_OR2 C367 ( .A(N178), .B(pid_MDATA), .Z(N237) );
  GTECH_OR2 C368 ( .A(N240), .B(N237), .Z(N238) );
  GTECH_OR2 C371 ( .A(pid_DATA0), .B(pid_DATA1), .Z(N240) );
  GTECH_OR2 C372 ( .A(pid_DATA2), .B(N167), .Z(N241) );
  GTECH_OR2 C373 ( .A(N240), .B(N241), .Z(N242) );
  \**SEQGEN**  \allow_pid_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        1'b0), .clocked_on(1'b0), .data_in(N250), .enable(N248), .Q(
        allow_pid[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b0) );
  \**SEQGEN**  \allow_pid_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        1'b0), .clocked_on(1'b0), .data_in(N249), .enable(N248), .Q(
        allow_pid[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b0) );
  GTECH_AND2 C394 ( .A(N92), .B(N865), .Z(N251) );
  GTECH_AND2 C396 ( .A(N251), .B(N94), .Z(N252) );
  GTECH_AND2 C397 ( .A(N252), .B(csr[24]), .Z(N253) );
  GTECH_AND2 C402 ( .A(tr_fr_d[1]), .B(N97), .Z(N254) );
  GTECH_AND2 C405 ( .A(N254), .B(N158), .Z(N255) );
  GTECH_AND2 C407 ( .A(N255), .B(N282), .Z(N256) );
  GTECH_AND2 C411 ( .A(tr_fr_d[1]), .B(N97), .Z(N257) );
  GTECH_AND2 C414 ( .A(N257), .B(N158), .Z(N258) );
  GTECH_AND2 C416 ( .A(N258), .B(N287), .Z(N259) );
  GTECH_OR2 C421 ( .A(N92), .B(N97), .Z(N260) );
  GTECH_OR2 C425 ( .A(N260), .B(N113), .Z(N261) );
  GTECH_OR2 C427 ( .A(N261), .B(N128), .Z(N262) );
  GTECH_OR2 C434 ( .A(N92), .B(N97), .Z(N264) );
  GTECH_OR2 C438 ( .A(N264), .B(N113), .Z(N265) );
  GTECH_OR2 C440 ( .A(N265), .B(N134), .Z(N266) );
  GTECH_OR2 C447 ( .A(N92), .B(N97), .Z(N268) );
  GTECH_OR2 C451 ( .A(N268), .B(N113), .Z(N269) );
  GTECH_OR2 C453 ( .A(N269), .B(N142), .Z(N270) );
  GTECH_OR2 C457 ( .A(tr_fr_d[1]), .B(tr_fr_d[0]), .Z(N272) );
  GTECH_OR2 C460 ( .A(N272), .B(N138), .Z(N273) );
  GTECH_OR2 C461 ( .A(N273), .B(N139), .Z(N274) );
  GTECH_OR2 C466 ( .A(tr_fr_d[1]), .B(N97), .Z(N276) );
  GTECH_OR2 C469 ( .A(N276), .B(N138), .Z(N277) );
  GTECH_OR2 C470 ( .A(N277), .B(N139), .Z(N278) );
  GTECH_AND2 C476 ( .A(tr_fr_d[1]), .B(N97), .Z(N280) );
  GTECH_AND2 C479 ( .A(N280), .B(N162), .Z(N281) );
  GTECH_AND2 C480 ( .A(N285), .B(N195), .Z(N282) );
  GTECH_AND2 C481 ( .A(N281), .B(N282), .Z(N283) );
  GTECH_AND2 C485 ( .A(tr_fr_d[1]), .B(N97), .Z(N284) );
  GTECH_AND2 C487 ( .A(N148), .B(csr[24]), .Z(N285) );
  GTECH_AND2 C488 ( .A(N284), .B(N162), .Z(N286) );
  GTECH_AND2 C489 ( .A(N285), .B(csr[28]), .Z(N287) );
  GTECH_AND2 C490 ( .A(N286), .B(N287), .Z(N288) );
  GTECH_OR2 C495 ( .A(N92), .B(N97), .Z(N289) );
  GTECH_OR2 C499 ( .A(N289), .B(N138), .Z(N290) );
  GTECH_OR2 C501 ( .A(N290), .B(N128), .Z(N291) );
  GTECH_OR2 C508 ( .A(N92), .B(N97), .Z(N293) );
  GTECH_OR2 C512 ( .A(N293), .B(N138), .Z(N294) );
  GTECH_OR2 C514 ( .A(N294), .B(N134), .Z(N295) );
  GTECH_OR2 C521 ( .A(N92), .B(N97), .Z(N297) );
  GTECH_OR2 C525 ( .A(N297), .B(N138), .Z(N298) );
  GTECH_OR2 C527 ( .A(N298), .B(N142), .Z(N299) );
  GTECH_OR2 C629 ( .A(setup_token), .B(N184), .Z(N301) );
  GTECH_OR2 C630 ( .A(out_op), .B(csr[29]), .Z(N302) );
  GTECH_OR2 C631 ( .A(N301), .B(N302), .Z(N303) );
  GTECH_OR2 C635 ( .A(setup_token), .B(N184), .Z(N305) );
  GTECH_OR2 C636 ( .A(out_op), .B(N189), .Z(N306) );
  GTECH_OR2 C637 ( .A(N305), .B(N306), .Z(N307) );
  GTECH_AND2 C642 ( .A(N194), .B(N184), .Z(N309) );
  GTECH_AND2 C643 ( .A(out_op), .B(N195), .Z(N310) );
  GTECH_AND2 C644 ( .A(N309), .B(N310), .Z(N311) );
  GTECH_AND2 C647 ( .A(N194), .B(N184), .Z(N312) );
  GTECH_AND2 C648 ( .A(out_op), .B(csr[28]), .Z(N313) );
  GTECH_AND2 C649 ( .A(N312), .B(N313), .Z(N314) );
  \**SEQGEN**  \this_dpid_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N342), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        data_pid_sel[1]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N340) );
  \**SEQGEN**  \this_dpid_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N341), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        data_pid_sel[0]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N340) );
  \**SEQGEN**  pid_seq_err_reg ( .clear(1'b0), .preset(1'b0), .next_state(N343), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(pid_seq_err), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  in_token_reg ( .clear(1'b0), .preset(1'b0), .next_state(N350), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(in_token), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N349) );
  \**SEQGEN**  out_token_reg ( .clear(1'b0), .preset(1'b0), .next_state(N359), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(out_token), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N358) );
  \**SEQGEN**  setup_token_reg ( .clear(1'b0), .preset(1'b0), .next_state(N369), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(setup_token), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N368) );
  \**SEQGEN**  \adr_reg[16]  ( .clear(1'b0), .preset(1'b0), .next_state(N394), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr[16]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(N393), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr[15]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(N392), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr[14]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(N391), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr[13]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(N390), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr[12]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(N389), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr[11]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(N388), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr[10]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(N387), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(N386), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N385), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N384), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N383), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N382), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N381), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N380), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N379), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N378), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  LT_UNS_OP lt_4320 ( .A(buf_size), .B(csr[10:0]), .Z(buf_smaller) );
  LT_UNS_OP lt_4331 ( .A(new_size), .B(csr[10:0]), .Z(N396) );
  \**SEQGEN**  buffer_full_reg ( .clear(1'b0), .preset(1'b0), .next_state(N396), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buffer_full), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  buffer_empty_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N823), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buffer_empty), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  buffer_done_reg ( .clear(1'b0), .preset(1'b0), .next_state(N397), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buffer_done), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  LT_UNS_OP lt_4347 ( .A(buf0[30:17]), .B(csr[10:0]), .Z(N398) );
  \**SEQGEN**  buf0_st_max_reg ( .clear(1'b0), .preset(1'b0), .next_state(N398), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0_st_max), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  LT_UNS_OP lt_4350 ( .A(buf1[30:17]), .B(csr[10:0]), .Z(N399) );
  \**SEQGEN**  buf1_st_max_reg ( .clear(1'b0), .preset(1'b0), .next_state(N399), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1_st_max), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  no_bufs0_reg ( .clear(1'b0), .preset(1'b0), .next_state(N402), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(no_bufs0), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  no_bufs1_reg ( .clear(1'b0), .preset(1'b0), .next_state(N404), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(no_bufs1), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \new_sizeb_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N434), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        new_sizeb[13]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \new_sizeb_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N433), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        new_sizeb[12]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \new_sizeb_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N432), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        new_sizeb[11]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \new_sizeb_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N431), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        new_sizeb[10]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \new_sizeb_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N430), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        new_sizeb[9]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \new_sizeb_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N429), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        new_sizeb[8]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \new_sizeb_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N428), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        new_sizeb[7]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \new_sizeb_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N427), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        new_sizeb[6]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \new_sizeb_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N426), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        new_sizeb[5]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \new_sizeb_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N425), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        new_sizeb[4]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \new_sizeb_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N424), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        new_sizeb[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \new_sizeb_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N423), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        new_sizeb[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \new_sizeb_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N422), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        new_sizeb[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \new_sizeb_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N421), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        new_sizeb[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \new_size_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N448), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        new_size[13]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \new_size_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N447), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        new_size[12]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \new_size_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N446), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        new_size[11]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \new_size_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N445), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        new_size[10]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \new_size_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N444), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(new_size[9]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        1'b1) );
  \**SEQGEN**  \new_size_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N443), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(new_size[8]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        1'b1) );
  \**SEQGEN**  \new_size_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N442), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(new_size[7]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        1'b1) );
  \**SEQGEN**  \new_size_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N441), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(new_size[6]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        1'b1) );
  \**SEQGEN**  \new_size_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N440), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(new_size[5]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        1'b1) );
  \**SEQGEN**  \new_size_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N439), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(new_size[4]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        1'b1) );
  \**SEQGEN**  \new_size_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N438), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(new_size[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        1'b1) );
  \**SEQGEN**  \new_size_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N437), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(new_size[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        1'b1) );
  \**SEQGEN**  \new_size_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N436), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(new_size[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        1'b1) );
  \**SEQGEN**  \new_size_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N435), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(new_size[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(
        1'b1) );
  \**SEQGEN**  \adr_r_reg[16]  ( .clear(1'b0), .preset(1'b0), .next_state(
        adr[16]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        adr_r[16]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \adr_r_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(
        adr[15]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        adr_r[15]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \adr_r_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(
        adr[14]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        adr_r[14]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \adr_r_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(
        adr[13]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        adr_r[13]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \adr_r_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(
        adr[12]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        adr_r[12]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \adr_r_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(
        adr[11]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        adr_r[11]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \adr_r_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(
        adr[10]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        adr_r[10]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \adr_r_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(
        adr[9]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr_r[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_r_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(
        adr[8]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr_r[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_r_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        adr[7]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr_r[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_r_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        adr[6]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr_r[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_r_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        adr[5]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr_r[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_r_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        adr[4]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr_r[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_r_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        adr[3]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr_r[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_r_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        adr[2]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr_r[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_r_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        adr[1]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr_r[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \adr_r_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        adr[0]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(adr_r[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \size_next_r_reg[13]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(size[13]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(size_next_r[13]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \size_next_r_reg[12]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(size[12]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(size_next_r[12]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \size_next_r_reg[11]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(size[11]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(size_next_r[11]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \size_next_r_reg[10]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(size[10]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(size_next_r[10]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \size_next_r_reg[9]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(size[9]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(size_next_r[9]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \size_next_r_reg[8]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(size[8]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(size_next_r[8]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \size_next_r_reg[7]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(size[7]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(size_next_r[7]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \size_next_r_reg[6]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(size[6]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(size_next_r[6]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \size_next_r_reg[5]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(size[5]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(size_next_r[5]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \size_next_r_reg[4]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(size[4]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(size_next_r[4]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \size_next_r_reg[3]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(size[3]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(size_next_r[3]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \size_next_r_reg[2]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(size[2]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(size_next_r[2]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \size_next_r_reg[1]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(size[1]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(size_next_r[1]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \size_next_r_reg[0]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(size[0]), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(size_next_r[0]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  GT_UNS_OP gt_4383 ( .A(sizu_c), .B(buf_size), .Z(N468) );
  \**SEQGEN**  buffer_overflow_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N469), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buffer_overflow), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  NE_UNS_OP ne_4388 ( .A(sizu_c), .B(csr[10:0]), .Z(N470) );
  \**SEQGEN**  out_to_small_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N471), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        out_to_small_r), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  out_to_small_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        out_to_small_r), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        out_to_small), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  LT_UNS_OP lt_4400 ( .A(sizu_c), .B(csr[10:0]), .Z(N472) );
  \**SEQGEN**  to_small_reg ( .clear(1'b0), .preset(1'b0), .next_state(N473), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(to_small), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  GT_UNS_OP gt_4403 ( .A(sizu_c), .B(csr[10:0]), .Z(N474) );
  \**SEQGEN**  to_large_reg ( .clear(1'b0), .preset(1'b0), .next_state(N475), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(to_large), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[31]  ( .clear(1'b0), .preset(1'b0), .next_state(N498), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[31]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[30]  ( .clear(1'b0), .preset(1'b0), .next_state(N497), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[30]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[29]  ( .clear(1'b0), .preset(1'b0), .next_state(N496), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[29]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[28]  ( .clear(1'b0), .preset(1'b0), .next_state(N495), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[28]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[27]  ( .clear(1'b0), .preset(1'b0), .next_state(N494), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[27]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[26]  ( .clear(1'b0), .preset(1'b0), .next_state(N493), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[26]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[25]  ( .clear(1'b0), .preset(1'b0), .next_state(N492), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[25]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[24]  ( .clear(1'b0), .preset(1'b0), .next_state(N491), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[24]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[23]  ( .clear(1'b0), .preset(1'b0), .next_state(N490), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[23]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[22]  ( .clear(1'b0), .preset(1'b0), .next_state(N489), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[22]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[21]  ( .clear(1'b0), .preset(1'b0), .next_state(N488), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[21]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[20]  ( .clear(1'b0), .preset(1'b0), .next_state(N487), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[20]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[19]  ( .clear(1'b0), .preset(1'b0), .next_state(N486), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[19]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[18]  ( .clear(1'b0), .preset(1'b0), .next_state(N485), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[18]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[17]  ( .clear(1'b0), .preset(1'b0), .next_state(N484), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[17]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[16]  ( .clear(1'b0), .preset(1'b0), .next_state(N511), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[16]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(N510), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[15]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(N509), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[14]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(N508), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[13]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(N507), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[12]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(N506), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[11]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(N505), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[10]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(N504), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(N503), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N502), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N501), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N500), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N499), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N517), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N516), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N515), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \idin_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N514), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(idin[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  buf0_set_reg ( .clear(1'b0), .preset(1'b0), .next_state(N520), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0_set), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  buf1_set_reg ( .clear(1'b0), .preset(1'b0), .next_state(N521), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1_set), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  uc_bsel_set_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        uc_stat_set_d), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        uc_bsel_set), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  uc_dpd_set_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        uc_stat_set_d), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        uc_dpd_set), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  buf0_rl_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        buf0_rl_d), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_rl), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  abort_reg ( .clear(1'b0), .preset(1'b0), .next_state(N522), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(abort), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  rx_ack_to_clr_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N523), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rx_ack_to_clr), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_ack_to_cnt_reg[7]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N540), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(rx_ack_to_cnt[7]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_ack_to_cnt_reg[6]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N539), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(rx_ack_to_cnt[6]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_ack_to_cnt_reg[5]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N538), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(rx_ack_to_cnt[5]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_ack_to_cnt_reg[4]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N537), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(rx_ack_to_cnt[4]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_ack_to_cnt_reg[3]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N536), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(rx_ack_to_cnt[3]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_ack_to_cnt_reg[2]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N535), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(rx_ack_to_cnt[2]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_ack_to_cnt_reg[1]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N534), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(rx_ack_to_cnt[1]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \rx_ack_to_cnt_reg[0]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N533), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(rx_ack_to_cnt[0]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  EQ_UNS_OP eq_4463 ( .A(rx_ack_to_cnt), .B({1'b0, 1'b0, tx_data_to_val, 1'b0, 
        1'b1, tx_data_to_val[4], 1'b0}), .Z(N541) );
  \**SEQGEN**  rx_ack_to_reg ( .clear(1'b0), .preset(1'b0), .next_state(N541), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(rx_ack_to), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \tx_data_to_cnt_reg[7]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N559), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(tx_data_to_cnt[7]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \tx_data_to_cnt_reg[6]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N558), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(tx_data_to_cnt[6]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \tx_data_to_cnt_reg[5]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N557), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(tx_data_to_cnt[5]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \tx_data_to_cnt_reg[4]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N556), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(tx_data_to_cnt[4]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \tx_data_to_cnt_reg[3]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N555), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(tx_data_to_cnt[3]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \tx_data_to_cnt_reg[2]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N554), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(tx_data_to_cnt[2]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \tx_data_to_cnt_reg[1]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N553), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(tx_data_to_cnt[1]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \tx_data_to_cnt_reg[0]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N552), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(tx_data_to_cnt[0]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  EQ_UNS_OP eq_4480 ( .A(tx_data_to_cnt), .B({1'b0, 1'b0, tx_data_to_val, 1'b0, 
        1'b1, tx_data_to_val[4], 1'b0}), .Z(N560) );
  \**SEQGEN**  tx_data_to_reg ( .clear(1'b0), .preset(1'b0), .next_state(N560), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(tx_data_to), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  pid_OUT_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        pid_OUT), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        pid_OUT_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  pid_IN_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(pid_IN), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(pid_IN_r), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  pid_PING_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        pid_PING), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        pid_PING_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  pid_SETUP_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        pid_SETUP), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        pid_SETUP_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  int_upid_set_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N561), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        int_upid_set), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  int_seqerr_set_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        int_seqerr_set_d), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(int_seqerr_set), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \state_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(N573), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N787) );
  \**SEQGEN**  \state_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(N572), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N787) );
  \**SEQGEN**  \state_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N571), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N787) );
  \**SEQGEN**  \state_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N570), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N787) );
  \**SEQGEN**  \state_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N569), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N787) );
  \**SEQGEN**  \state_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N568), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N787) );
  \**SEQGEN**  \state_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N567), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N787) );
  \**SEQGEN**  \state_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N566), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N787) );
  \**SEQGEN**  \state_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N565), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N787) );
  \**SEQGEN**  \state_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N564), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(state[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N787) );
  GTECH_OR2 C1073 ( .A(state[9]), .B(state[8]), .Z(N576) );
  GTECH_OR2 C1074 ( .A(state[7]), .B(state[5]), .Z(N577) );
  GTECH_OR2 C1075 ( .A(state[4]), .B(state[3]), .Z(N578) );
  GTECH_OR2 C1076 ( .A(state[2]), .B(state[1]), .Z(N579) );
  GTECH_OR2 C1077 ( .A(N576), .B(N577), .Z(N580) );
  GTECH_OR2 C1078 ( .A(N578), .B(N579), .Z(N581) );
  GTECH_OR2 C1079 ( .A(N580), .B(N581), .Z(N582) );
  GTECH_OR2 C1080 ( .A(N582), .B(N833), .Z(N583) );
  GTECH_OR2 C1083 ( .A(state[9]), .B(state[8]), .Z(N586) );
  GTECH_OR2 C1084 ( .A(state[7]), .B(state[5]), .Z(N587) );
  GTECH_OR2 C1085 ( .A(state[4]), .B(state[3]), .Z(N588) );
  GTECH_OR2 C1086 ( .A(state[2]), .B(N585), .Z(N589) );
  GTECH_OR2 C1087 ( .A(N586), .B(N587), .Z(N590) );
  GTECH_OR2 C1088 ( .A(N588), .B(N589), .Z(N591) );
  GTECH_OR2 C1089 ( .A(N590), .B(N591), .Z(N592) );
  GTECH_OR2 C1090 ( .A(N592), .B(state[0]), .Z(N593) );
  GTECH_OR2 C1093 ( .A(state[9]), .B(state[8]), .Z(N596) );
  GTECH_OR2 C1094 ( .A(state[7]), .B(state[5]), .Z(N597) );
  GTECH_OR2 C1095 ( .A(state[4]), .B(state[3]), .Z(N598) );
  GTECH_OR2 C1096 ( .A(N595), .B(state[1]), .Z(N599) );
  GTECH_OR2 C1097 ( .A(N596), .B(N597), .Z(N600) );
  GTECH_OR2 C1098 ( .A(N598), .B(N599), .Z(N601) );
  GTECH_OR2 C1099 ( .A(N600), .B(N601), .Z(N602) );
  GTECH_OR2 C1100 ( .A(N602), .B(state[0]), .Z(N603) );
  GTECH_OR2 C1103 ( .A(state[9]), .B(state[8]), .Z(N605) );
  GTECH_OR2 C1104 ( .A(state[7]), .B(state[5]), .Z(N606) );
  GTECH_OR2 C1105 ( .A(state[4]), .B(N788), .Z(N607) );
  GTECH_OR2 C1106 ( .A(state[2]), .B(state[1]), .Z(N608) );
  GTECH_OR2 C1107 ( .A(N605), .B(N606), .Z(N609) );
  GTECH_OR2 C1108 ( .A(N607), .B(N608), .Z(N610) );
  GTECH_OR2 C1109 ( .A(N609), .B(N610), .Z(N611) );
  GTECH_OR2 C1110 ( .A(N611), .B(state[0]), .Z(N612) );
  GTECH_OR2 C1113 ( .A(state[9]), .B(state[8]), .Z(N614) );
  GTECH_OR2 C1114 ( .A(state[7]), .B(state[5]), .Z(N615) );
  GTECH_OR2 C1115 ( .A(N799), .B(state[3]), .Z(N616) );
  GTECH_OR2 C1116 ( .A(state[2]), .B(state[1]), .Z(N617) );
  GTECH_OR2 C1117 ( .A(N614), .B(N615), .Z(N618) );
  GTECH_OR2 C1118 ( .A(N616), .B(N617), .Z(N619) );
  GTECH_OR2 C1119 ( .A(N618), .B(N619), .Z(N620) );
  GTECH_OR2 C1120 ( .A(N620), .B(state[0]), .Z(N621) );
  GTECH_OR2 C1123 ( .A(state[9]), .B(state[8]), .Z(N624) );
  GTECH_OR2 C1124 ( .A(state[7]), .B(N623), .Z(N625) );
  GTECH_OR2 C1125 ( .A(state[4]), .B(state[3]), .Z(N626) );
  GTECH_OR2 C1126 ( .A(state[2]), .B(state[1]), .Z(N627) );
  GTECH_OR2 C1127 ( .A(N624), .B(N625), .Z(N628) );
  GTECH_OR2 C1128 ( .A(N626), .B(N627), .Z(N629) );
  GTECH_OR2 C1129 ( .A(N628), .B(N629), .Z(N630) );
  GTECH_OR2 C1130 ( .A(N630), .B(state[0]), .Z(N631) );
  GTECH_OR2 C1133 ( .A(state[9]), .B(state[8]), .Z(N634) );
  GTECH_OR2 C1134 ( .A(N633), .B(state[5]), .Z(N635) );
  GTECH_OR2 C1135 ( .A(state[4]), .B(state[3]), .Z(N636) );
  GTECH_OR2 C1136 ( .A(state[2]), .B(state[1]), .Z(N637) );
  GTECH_OR2 C1137 ( .A(N634), .B(N635), .Z(N638) );
  GTECH_OR2 C1138 ( .A(N636), .B(N637), .Z(N639) );
  GTECH_OR2 C1139 ( .A(N638), .B(N639), .Z(N640) );
  GTECH_OR2 C1140 ( .A(N640), .B(state[0]), .Z(N641) );
  GTECH_OR2 C1143 ( .A(state[9]), .B(N643), .Z(N644) );
  GTECH_OR2 C1144 ( .A(state[7]), .B(state[5]), .Z(N645) );
  GTECH_OR2 C1145 ( .A(state[4]), .B(state[3]), .Z(N646) );
  GTECH_OR2 C1146 ( .A(state[2]), .B(state[1]), .Z(N647) );
  GTECH_OR2 C1147 ( .A(N644), .B(N645), .Z(N648) );
  GTECH_OR2 C1148 ( .A(N646), .B(N647), .Z(N649) );
  GTECH_OR2 C1149 ( .A(N648), .B(N649), .Z(N650) );
  GTECH_OR2 C1150 ( .A(N650), .B(state[0]), .Z(N651) );
  GTECH_OR2 C1153 ( .A(N653), .B(state[8]), .Z(N654) );
  GTECH_OR2 C1154 ( .A(state[7]), .B(state[5]), .Z(N655) );
  GTECH_OR2 C1155 ( .A(state[4]), .B(state[3]), .Z(N656) );
  GTECH_OR2 C1156 ( .A(state[2]), .B(state[1]), .Z(N657) );
  GTECH_OR2 C1157 ( .A(N654), .B(N655), .Z(N658) );
  GTECH_OR2 C1158 ( .A(N656), .B(N657), .Z(N659) );
  GTECH_OR2 C1159 ( .A(N658), .B(N659), .Z(N660) );
  GTECH_OR2 C1160 ( .A(N660), .B(state[0]), .Z(N661) );
  GTECH_NOT I_0 ( .A(state[3]), .Z(N788) );
  GTECH_OR2 C1635 ( .A(state[8]), .B(state[9]), .Z(N789) );
  GTECH_OR2 C1636 ( .A(state[7]), .B(N789), .Z(N790) );
  GTECH_OR2 C1637 ( .A(state[6]), .B(N790), .Z(N791) );
  GTECH_OR2 C1638 ( .A(state[5]), .B(N791), .Z(N792) );
  GTECH_OR2 C1639 ( .A(state[4]), .B(N792), .Z(N793) );
  GTECH_OR2 C1640 ( .A(N788), .B(N793), .Z(N794) );
  GTECH_OR2 C1641 ( .A(state[2]), .B(N794), .Z(N795) );
  GTECH_OR2 C1642 ( .A(state[1]), .B(N795), .Z(N796) );
  GTECH_OR2 C1643 ( .A(state[0]), .B(N796), .Z(N797) );
  GTECH_NOT I_1 ( .A(N797), .Z(N798) );
  GTECH_NOT I_2 ( .A(state[4]), .Z(N799) );
  GTECH_OR2 C1646 ( .A(state[8]), .B(state[9]), .Z(N800) );
  GTECH_OR2 C1647 ( .A(state[7]), .B(N800), .Z(N801) );
  GTECH_OR2 C1648 ( .A(state[6]), .B(N801), .Z(N802) );
  GTECH_OR2 C1649 ( .A(state[5]), .B(N802), .Z(N803) );
  GTECH_OR2 C1650 ( .A(N799), .B(N803), .Z(N804) );
  GTECH_OR2 C1651 ( .A(state[3]), .B(N804), .Z(N805) );
  GTECH_OR2 C1652 ( .A(state[2]), .B(N805), .Z(N806) );
  GTECH_OR2 C1653 ( .A(state[1]), .B(N806), .Z(N807) );
  GTECH_OR2 C1654 ( .A(state[0]), .B(N807), .Z(N808) );
  GTECH_NOT I_3 ( .A(N808), .Z(N809) );
  GTECH_OR2 C1656 ( .A(new_size[12]), .B(new_size[13]), .Z(N810) );
  GTECH_OR2 C1657 ( .A(new_size[11]), .B(N810), .Z(N811) );
  GTECH_OR2 C1658 ( .A(new_size[10]), .B(N811), .Z(N812) );
  GTECH_OR2 C1659 ( .A(new_size[9]), .B(N812), .Z(N813) );
  GTECH_OR2 C1660 ( .A(new_size[8]), .B(N813), .Z(N814) );
  GTECH_OR2 C1661 ( .A(new_size[7]), .B(N814), .Z(N815) );
  GTECH_OR2 C1662 ( .A(new_size[6]), .B(N815), .Z(N816) );
  GTECH_OR2 C1663 ( .A(new_size[5]), .B(N816), .Z(N817) );
  GTECH_OR2 C1664 ( .A(new_size[4]), .B(N817), .Z(N818) );
  GTECH_OR2 C1665 ( .A(new_size[3]), .B(N818), .Z(N819) );
  GTECH_OR2 C1666 ( .A(new_size[2]), .B(N819), .Z(N820) );
  GTECH_OR2 C1667 ( .A(new_size[1]), .B(N820), .Z(N821) );
  GTECH_OR2 C1668 ( .A(new_size[0]), .B(N821), .Z(N822) );
  GTECH_NOT I_4 ( .A(N822), .Z(N823) );
  GTECH_OR2 C1670 ( .A(data_pid_sel[0]), .B(data_pid_sel[1]), .Z(N824) );
  GTECH_NOT I_5 ( .A(N824), .Z(N825) );
  GTECH_NOT I_6 ( .A(data_pid_sel[0]), .Z(N826) );
  GTECH_OR2 C1673 ( .A(N826), .B(data_pid_sel[1]), .Z(N827) );
  GTECH_NOT I_7 ( .A(N827), .Z(N828) );
  GTECH_NOT I_8 ( .A(data_pid_sel[1]), .Z(N829) );
  GTECH_OR2 C1676 ( .A(data_pid_sel[0]), .B(N829), .Z(N830) );
  GTECH_NOT I_9 ( .A(N830), .Z(N831) );
  GTECH_AND2 C1678 ( .A(data_pid_sel[0]), .B(data_pid_sel[1]), .Z(N832) );
  GTECH_NOT I_10 ( .A(state[0]), .Z(N833) );
  GTECH_OR2 C1680 ( .A(state[8]), .B(state[9]), .Z(N834) );
  GTECH_OR2 C1681 ( .A(state[7]), .B(N834), .Z(N835) );
  GTECH_OR2 C1682 ( .A(state[6]), .B(N835), .Z(N836) );
  GTECH_OR2 C1683 ( .A(state[5]), .B(N836), .Z(N837) );
  GTECH_OR2 C1684 ( .A(state[4]), .B(N837), .Z(N838) );
  GTECH_OR2 C1685 ( .A(state[3]), .B(N838), .Z(N839) );
  GTECH_OR2 C1686 ( .A(state[2]), .B(N839), .Z(N840) );
  GTECH_OR2 C1687 ( .A(state[1]), .B(N840), .Z(N841) );
  GTECH_OR2 C1688 ( .A(N833), .B(N841), .Z(N842) );
  GTECH_OR2 C1691 ( .A(csr[9]), .B(csr[10]), .Z(N843) );
  GTECH_OR2 C1692 ( .A(csr[8]), .B(N843), .Z(N844) );
  GTECH_OR2 C1693 ( .A(csr[7]), .B(N844), .Z(N845) );
  GTECH_OR2 C1694 ( .A(csr[6]), .B(N845), .Z(N846) );
  GTECH_OR2 C1695 ( .A(csr[5]), .B(N846), .Z(N847) );
  GTECH_OR2 C1696 ( .A(csr[4]), .B(N847), .Z(N848) );
  GTECH_OR2 C1697 ( .A(csr[3]), .B(N848), .Z(N849) );
  GTECH_OR2 C1698 ( .A(csr[2]), .B(N849), .Z(N850) );
  GTECH_OR2 C1699 ( .A(csr[1]), .B(N850), .Z(N851) );
  GTECH_OR2 C1700 ( .A(csr[0]), .B(N851), .Z(N852) );
  GTECH_NOT I_11 ( .A(N852), .Z(N853) );
  GTECH_NOT I_12 ( .A(csr[24]), .Z(N854) );
  GTECH_OR2 C1703 ( .A(N854), .B(csr[25]), .Z(N855) );
  GTECH_NOT I_13 ( .A(N855), .Z(N856) );
  GTECH_NOT I_14 ( .A(csr[22]), .Z(N857) );
  GTECH_OR2 C1706 ( .A(N857), .B(csr[23]), .Z(N858) );
  GTECH_NOT I_15 ( .A(csr[23]), .Z(N859) );
  GTECH_OR2 C1709 ( .A(csr[22]), .B(N859), .Z(N860) );
  GTECH_NOT I_16 ( .A(N860), .Z(N861) );
  GTECH_NOT I_17 ( .A(csr[26]), .Z(N862) );
  GTECH_OR2 C1712 ( .A(N862), .B(csr[27]), .Z(N863) );
  GTECH_NOT I_18 ( .A(N863), .Z(N864) );
  GTECH_NOT I_19 ( .A(csr[27]), .Z(N865) );
  GTECH_OR2 C1715 ( .A(csr[26]), .B(N865), .Z(N866) );
  GTECH_NOT I_20 ( .A(N866), .Z(N867) );
  GTECH_OR2 C1717 ( .A(csr[26]), .B(csr[27]), .Z(N868) );
  GTECH_NOT I_21 ( .A(N868), .Z(N869) );
  ADD_UNS_OP add_4460 ( .A(rx_ack_to_cnt), .B(1'b1), .Z({N532, N531, N530, 
        N529, N528, N527, N526, N525}) );
  ADD_UNS_OP add_4477 ( .A(tx_data_to_cnt), .B(1'b1), .Z({N551, N550, N549, 
        N548, N547, N546, N545, N544}) );
  ADD_UNS_OP add_4410 ( .A(csr[31:30]), .B(1'b1), .Z({N480, N479}) );
  SUB_UNS_OP sub_4366 ( .A(buf_size), .B(new_sizeb), .Z({N448, N447, N446, 
        N445, N444, N443, N442, N441, N440, N439, N438, N437, N436, N435}) );
  ADD_UNS_OP add_4376 ( .A(adr_r), .B({N465, N464, N463, N462, N461, N460, 
        N459, N458, N457, N456, N455, N454, N453, N452}), .Z(new_adr) );
  SELECT_OP C1724 ( .DATA1(csr[12:11]), .DATA2({1'b0, 1'b0}), .CONTROL1(N0), 
        .CONTROL2(N1), .Z(tr_fr_d) );
  GTECH_BUF B_0 ( .A(tx_data_to_val[4]), .Z(N0) );
  GTECH_BUF B_1 ( .A(tx_data_to_val[5]), .Z(N1) );
  SELECT_OP C1725 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N2), 
        .CONTROL2(N3), .CONTROL3(N174), .Z(N175) );
  GTECH_BUF B_2 ( .A(N169), .Z(N2) );
  GTECH_BUF B_3 ( .A(N172), .Z(N3) );
  SELECT_OP C1726 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N4), 
        .CONTROL2(N5), .CONTROL3(N182), .Z(N183) );
  GTECH_BUF B_4 ( .A(N177), .Z(N4) );
  GTECH_BUF B_5 ( .A(N180), .Z(N5) );
  SELECT_OP C1727 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b1), 
        .DATA5(1'b1), .DATA6(1'b0), .CONTROL1(N6), .CONTROL2(N7), .CONTROL3(N8), .CONTROL4(N9), .CONTROL5(N10), .CONTROL6(N206), .Z(N207) );
  GTECH_BUF B_6 ( .A(setup_token), .Z(N6) );
  GTECH_BUF B_7 ( .A(N188), .Z(N7) );
  GTECH_BUF B_8 ( .A(N193), .Z(N8) );
  GTECH_BUF B_9 ( .A(N198), .Z(N9) );
  GTECH_BUF B_10 ( .A(N201), .Z(N10) );
  SELECT_OP C1728 ( .DATA1({1'b1, 1'b1}), .DATA2({1'b1, 1'b1}), .DATA3({1'b0, 
        1'b1}), .DATA4({1'b1, 1'b1}), .DATA5({1'b1, 1'b0}), .CONTROL1(N6), 
        .CONTROL2(N7), .CONTROL3(N8), .CONTROL4(N9), .CONTROL5(N10), .Z({N209, 
        N208}) );
  SELECT_OP C1729 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b1), 
        .DATA5(1'b1), .DATA6(1'b1), .DATA7(1'b1), .DATA8(N175), .DATA9(N183), 
        .DATA10(N183), .DATA11(N183), .DATA12(1'b1), .DATA13(1'b1), .DATA14(
        1'b1), .DATA15(1'b1), .DATA16(N207), .DATA17(1'b0), .CONTROL1(N11), 
        .CONTROL2(N12), .CONTROL3(N13), .CONTROL4(N14), .CONTROL5(N15), 
        .CONTROL6(N16), .CONTROL7(N17), .CONTROL8(N18), .CONTROL9(N19), 
        .CONTROL10(N20), .CONTROL11(N21), .CONTROL12(N22), .CONTROL13(N23), 
        .CONTROL14(N24), .CONTROL15(N25), .CONTROL16(N26), .CONTROL17(N225), 
        .Z(N226) );
  GTECH_BUF B_11 ( .A(N96), .Z(N11) );
  GTECH_BUF B_12 ( .A(N100), .Z(N12) );
  GTECH_BUF B_13 ( .A(N103), .Z(N13) );
  GTECH_BUF B_14 ( .A(N107), .Z(N14) );
  GTECH_BUF B_15 ( .A(N111), .Z(N15) );
  GTECH_BUF B_16 ( .A(N116), .Z(N16) );
  GTECH_BUF B_17 ( .A(N120), .Z(N17) );
  GTECH_BUF B_18 ( .A(N124), .Z(N18) );
  GTECH_BUF B_19 ( .A(N130), .Z(N19) );
  GTECH_BUF B_20 ( .A(N136), .Z(N20) );
  GTECH_BUF B_21 ( .A(N144), .Z(N21) );
  GTECH_BUF B_22 ( .A(N147), .Z(N22) );
  GTECH_BUF B_23 ( .A(N154), .Z(N23) );
  GTECH_BUF B_24 ( .A(N157), .Z(N24) );
  GTECH_BUF B_25 ( .A(N165), .Z(N25) );
  GTECH_BUF B_26 ( .A(N166), .Z(N26) );
  SELECT_OP C1730 ( .DATA1({1'b0, 1'b0}), .DATA2({1'b0, 1'b1}), .DATA3({1'b0, 
        1'b0}), .DATA4({1'b0, 1'b1}), .DATA5({1'b1, 1'b0}), .DATA6({1'b0, 1'b0}), .DATA7({1'b0, 1'b0}), .DATA8({1'b0, N169}), .DATA9({1'b0, N177}), .DATA10({
        N177, 1'b0}), .DATA11({1'b0, N177}), .DATA12({1'b0, 1'b1}), .DATA13({
        1'b0, 1'b0}), .DATA14({1'b0, 1'b1}), .DATA15({1'b0, 1'b0}), .DATA16({
        N209, N208}), .CONTROL1(N11), .CONTROL2(N12), .CONTROL3(N13), 
        .CONTROL4(N14), .CONTROL5(N15), .CONTROL6(N16), .CONTROL7(N17), 
        .CONTROL8(N18), .CONTROL9(N19), .CONTROL10(N20), .CONTROL11(N21), 
        .CONTROL12(N22), .CONTROL13(N23), .CONTROL14(N24), .CONTROL15(N25), 
        .CONTROL16(N26), .Z({N228, N227}) );
  SELECT_OP C1731 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b1), 
        .DATA5(1'b0), .CONTROL1(N27), .CONTROL2(N28), .CONTROL3(N29), 
        .CONTROL4(N30), .CONTROL5(N247), .Z(N248) );
  GTECH_BUF B_27 ( .A(N232), .Z(N27) );
  GTECH_BUF B_28 ( .A(N236), .Z(N28) );
  GTECH_BUF B_29 ( .A(N239), .Z(N29) );
  GTECH_BUF B_30 ( .A(N243), .Z(N30) );
  SELECT_OP C1732 ( .DATA1({1'b0, 1'b0}), .DATA2({1'b0, 1'b1}), .DATA3({1'b1, 
        1'b0}), .DATA4({1'b1, 1'b1}), .CONTROL1(N27), .CONTROL2(N28), 
        .CONTROL3(N29), .CONTROL4(N30), .Z({N250, N249}) );
  SELECT_OP C1733 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b1), 
        .DATA5(1'b1), .DATA6(1'b0), .CONTROL1(N6), .CONTROL2(N31), .CONTROL3(
        N32), .CONTROL4(N33), .CONTROL5(N34), .CONTROL6(N319), .Z(N320) );
  GTECH_BUF B_31 ( .A(N304), .Z(N31) );
  GTECH_BUF B_32 ( .A(N308), .Z(N32) );
  GTECH_BUF B_33 ( .A(N311), .Z(N33) );
  GTECH_BUF B_34 ( .A(N314), .Z(N34) );
  SELECT_OP C1734 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b1), .DATA4(1'b0), 
        .DATA5(1'b1), .CONTROL1(N6), .CONTROL2(N31), .CONTROL3(N32), 
        .CONTROL4(N33), .CONTROL5(N34), .Z(N321) );
  SELECT_OP C1735 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b1), 
        .DATA5(1'b1), .DATA6(1'b1), .DATA7(1'b1), .DATA8(1'b1), .DATA9(1'b1), 
        .DATA10(1'b1), .DATA11(1'b1), .DATA12(1'b1), .DATA13(1'b1), .DATA14(
        1'b1), .DATA15(1'b1), .DATA16(1'b1), .DATA17(1'b1), .DATA18(N320), 
        .DATA19(1'b0), .CONTROL1(N35), .CONTROL2(N36), .CONTROL3(N37), 
        .CONTROL4(N38), .CONTROL5(N39), .CONTROL6(N40), .CONTROL7(N41), 
        .CONTROL8(N42), .CONTROL9(N43), .CONTROL10(N44), .CONTROL11(N45), 
        .CONTROL12(N46), .CONTROL13(N47), .CONTROL14(N22), .CONTROL15(N23), 
        .CONTROL16(N24), .CONTROL17(N25), .CONTROL18(N26), .CONTROL19(N339), 
        .Z(N340) );
  GTECH_BUF B_35 ( .A(N253), .Z(N35) );
  GTECH_BUF B_36 ( .A(N256), .Z(N36) );
  GTECH_BUF B_37 ( .A(N259), .Z(N37) );
  GTECH_BUF B_38 ( .A(N263), .Z(N38) );
  GTECH_BUF B_39 ( .A(N267), .Z(N39) );
  GTECH_BUF B_40 ( .A(N271), .Z(N40) );
  GTECH_BUF B_41 ( .A(N275), .Z(N41) );
  GTECH_BUF B_42 ( .A(N279), .Z(N42) );
  GTECH_BUF B_43 ( .A(N283), .Z(N43) );
  GTECH_BUF B_44 ( .A(N288), .Z(N44) );
  GTECH_BUF B_45 ( .A(N292), .Z(N45) );
  GTECH_BUF B_46 ( .A(N296), .Z(N46) );
  GTECH_BUF B_47 ( .A(N300), .Z(N47) );
  SELECT_OP C1736 ( .DATA1({1'b0, 1'b0}), .DATA2({1'b0, 1'b1}), .DATA3({1'b0, 
        1'b0}), .DATA4({1'b1, 1'b0}), .DATA5({1'b0, 1'b1}), .DATA6({1'b0, 1'b0}), .DATA7(allow_pid), .DATA8({1'b0, 1'b0}), .DATA9({1'b1, 1'b1}), .DATA10({
        1'b0, 1'b1}), .DATA11({1'b1, 1'b1}), .DATA12({1'b1, 1'b1}), .DATA13({
        1'b1, 1'b0}), .DATA14({1'b0, 1'b0}), .DATA15({1'b0, 1'b1}), .DATA16({
        1'b0, 1'b0}), .DATA17({1'b0, 1'b1}), .DATA18({1'b0, N321}), .CONTROL1(
        N35), .CONTROL2(N36), .CONTROL3(N37), .CONTROL4(N38), .CONTROL5(N39), 
        .CONTROL6(N40), .CONTROL7(N41), .CONTROL8(N42), .CONTROL9(N43), 
        .CONTROL10(N44), .CONTROL11(N45), .CONTROL12(N46), .CONTROL13(N47), 
        .CONTROL14(N22), .CONTROL15(N23), .CONTROL16(N24), .CONTROL17(N25), 
        .CONTROL18(N26), .Z({N342, N341}) );
  SELECT_OP C1737 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N48), .CONTROL2(N351), .CONTROL3(N354), .CONTROL4(N348), .Z(
        N349) );
  GTECH_BUF B_48 ( .A(N344), .Z(N48) );
  SELECT_OP C1738 ( .DATA1(1'b0), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N48), 
        .CONTROL2(N351), .CONTROL3(N354), .Z(N350) );
  SELECT_OP C1739 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N48), .CONTROL2(N360), .CONTROL3(N363), .CONTROL4(N357), .Z(
        N358) );
  SELECT_OP C1740 ( .DATA1(1'b0), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N48), 
        .CONTROL2(N360), .CONTROL3(N363), .Z(N359) );
  SELECT_OP C1741 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N48), .CONTROL2(N370), .CONTROL3(N373), .CONTROL4(N367), .Z(
        N368) );
  SELECT_OP C1742 ( .DATA1(1'b0), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N48), 
        .CONTROL2(N370), .CONTROL3(N373), .Z(N369) );
  SELECT_OP C1743 ( .DATA1(in_token), .DATA2(N375), .CONTROL1(N49), .CONTROL2(
        N50), .Z(N376) );
  GTECH_BUF B_49 ( .A(N869), .Z(N49) );
  GTECH_BUF B_50 ( .A(N868), .Z(N50) );
  SELECT_OP C1744 ( .DATA1(1'b0), .DATA2(N376), .CONTROL1(N51), .CONTROL2(N52), 
        .Z(buf_sel) );
  GTECH_BUF B_51 ( .A(dma_en), .Z(N51) );
  GTECH_BUF B_52 ( .A(N374), .Z(N52) );
  SELECT_OP C1745 ( .DATA1(buf1[16:0]), .DATA2(buf0[16:0]), .CONTROL1(N53), 
        .CONTROL2(N54), .Z({N394, N393, N392, N391, N390, N389, N388, N387, 
        N386, N385, N384, N383, N382, N381, N380, N379, N378}) );
  GTECH_BUF B_53 ( .A(buf_sel), .Z(N53) );
  GTECH_BUF B_54 ( .A(N377), .Z(N54) );
  SELECT_OP C1746 ( .DATA1(buf1[30:17]), .DATA2(buf0[30:17]), .CONTROL1(N53), 
        .CONTROL2(N54), .Z(buf_size) );
  SELECT_OP C1747 ( .DATA1(buf_size), .DATA2({1'b0, 1'b0, 1'b0, csr[10:0]}), 
        .CONTROL1(N55), .CONTROL2(N56), .Z(size) );
  GTECH_BUF B_55 ( .A(buf_smaller), .Z(N55) );
  GTECH_BUF B_56 ( .A(N395), .Z(N56) );
  SELECT_OP C1748 ( .DATA1(buffer_empty), .DATA2(buffer_full), .CONTROL1(N57), 
        .CONTROL2(N58), .Z(N397) );
  GTECH_BUF B_57 ( .A(in_op), .Z(N57) );
  GTECH_BUF B_58 ( .A(N184), .Z(N58) );
  SELECT_OP C1749 ( .DATA1(buf0_st_max), .DATA2(N400), .CONTROL1(N53), 
        .CONTROL2(N54), .Z(N401) );
  SELECT_OP C1750 ( .DATA1(buffer_full), .DATA2(buf1_st_max), .CONTROL1(N53), 
        .CONTROL2(N54), .Z(N403) );
  SELECT_OP C1751 ( .DATA1(size), .DATA2({1'b0, 1'b0, 1'b0, sizu_c}), 
        .CONTROL1(N57), .CONTROL2(N58), .Z({N420, N419, N418, N417, N416, N415, 
        N414, N413, N412, N411, N410, N409, N408, N407}) );
  SELECT_OP C1752 ( .DATA1({1'b0, 1'b0, 1'b0, csr[10:0]}), .DATA2({N420, N419, 
        N418, N417, N416, N415, N414, N413, N412, N411, N410, N409, N408, N407}), .CONTROL1(N59), .CONTROL2(N406), .Z({N434, N433, N432, N431, N430, N429, 
        N428, N427, N426, N425, N424, N423, N422, N421}) );
  GTECH_BUF B_59 ( .A(N405), .Z(N59) );
  SELECT_OP C1753 ( .DATA1({1'b0, 1'b0, 1'b0, csr[10:0]}), .DATA2(size_next_r), 
        .DATA3({1'b0, 1'b0, 1'b0, sizu_c}), .CONTROL1(N60), .CONTROL2(N467), 
        .CONTROL3(N451), .Z({N465, N464, N463, N462, N461, N460, N459, N458, 
        N457, N456, N455, N454, N453, N452}) );
  GTECH_BUF B_60 ( .A(N449), .Z(N60) );
  SELECT_OP C1754 ( .DATA1({1'b0, 1'b0}), .DATA2({N480, N479}), .DATA3(
        csr[31:30]), .CONTROL1(N51), .CONTROL2(N482), .CONTROL3(N477), .Z(
        next_bsel) );
  SELECT_OP C1755 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, sizu_c}), .DATA2({
        buffer_done, new_size}), .CONTROL1(N61), .CONTROL2(N62), .Z({N498, 
        N497, N496, N495, N494, N493, N492, N491, N490, N489, N488, N487, N486, 
        N485, N484}) );
  GTECH_BUF B_61 ( .A(out_to_small_r), .Z(N61) );
  GTECH_BUF B_62 ( .A(N483), .Z(N62) );
  SELECT_OP C1756 ( .DATA1(buf0[16:4]), .DATA2(new_adr[16:4]), .CONTROL1(N61), 
        .CONTROL2(N62), .Z({N511, N510, N509, N508, N507, N506, N505, N504, 
        N503, N502, N501, N500, N499}) );
  SELECT_OP C1757 ( .DATA1(new_adr[3:0]), .DATA2(buf0[3:0]), .DATA3({next_dpid, 
        next_bsel}), .CONTROL1(N63), .CONTROL2(N519), .CONTROL3(N513), .Z({
        N517, N516, N515, N514}) );
  GTECH_BUF B_63 ( .A(buf_set_d), .Z(N63) );
  SELECT_OP C1758 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), 
        .DATA2({N532, N531, N530, N529, N528, N527, N526, N525}), .CONTROL1(
        N64), .CONTROL2(N65), .Z({N540, N539, N538, N537, N536, N535, N534, 
        N533}) );
  GTECH_BUF B_64 ( .A(rx_ack_to_clr), .Z(N64) );
  GTECH_BUF B_65 ( .A(N524), .Z(N65) );
  SELECT_OP C1759 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), 
        .DATA2({N551, N550, N549, N548, N547, N546, N545, N544}), .CONTROL1(
        N66), .CONTROL2(N67), .Z({N559, N558, N557, N556, N555, N554, N553, 
        N552}) );
  GTECH_BUF B_66 ( .A(rx_active), .Z(N66) );
  GTECH_BUF B_67 ( .A(N543), .Z(N67) );
  SELECT_OP C1760 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b1}), .DATA2({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b1}), .DATA3({N747, N746, N745, N744, N743, N742, N741, N740, 
        N739, N738}), .CONTROL1(N48), .CONTROL2(N574), .CONTROL3(N563), .Z({
        N573, N572, N571, N570, N569, N568, N567, N566, N565, N564}) );
  SELECT_OP C1761 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .CONTROL1(N68), .CONTROL2(N674), 
        .CONTROL3(N757), .CONTROL4(N675), .CONTROL5(N69), .CONTROL6(N69), .Z(
        N676) );
  GTECH_BUF B_68 ( .A(N861), .Z(N68) );
  GTECH_BUF B_69 ( .A(1'b0), .Z(N69) );
  SELECT_OP C1762 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b1, 1'b0}), .DATA2({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b1, 1'b0}), .DATA3({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b1, 1'b0}), .DATA4({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 
        1'b0, 1'b0}), .DATA5({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1, 1'b0, 1'b0, 
        1'b0, 1'b0}), .DATA6(state), .CONTROL1(N68), .CONTROL2(N674), 
        .CONTROL3(N757), .CONTROL4(N760), .CONTROL5(N763), .CONTROL6(N673), 
        .Z({N686, N685, N684, N683, N682, N681, N680, N679, N678, N677}) );
  SELECT_OP C1763 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(N853), 
        .DATA5(1'b0), .DATA6(1'b0), .CONTROL1(N68), .CONTROL2(N674), 
        .CONTROL3(N757), .CONTROL4(N760), .CONTROL5(N687), .CONTROL6(N69), .Z(
        N688) );
  SELECT_OP C1764 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b1), 
        .DATA5(1'b0), .DATA6(1'b0), .CONTROL1(N68), .CONTROL2(N674), 
        .CONTROL3(N757), .CONTROL4(N760), .CONTROL5(N687), .CONTROL6(N69), .Z(
        N689) );
  SELECT_OP C1765 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b1), .DATA6(1'b0), .CONTROL1(N68), .CONTROL2(N674), 
        .CONTROL3(N757), .CONTROL4(N760), .CONTROL5(N763), .CONTROL6(N673), 
        .Z(N690) );
  SELECT_OP C1766 ( .DATA1(N690), .DATA2(1'b0), .CONTROL1(N70), .CONTROL2(N664), .Z(N691) );
  GTECH_BUF B_70 ( .A(N663), .Z(N70) );
  SELECT_OP C1767 ( .DATA1({N861, N674}), .DATA2({1'b0, 1'b0}), .CONTROL1(N70), 
        .CONTROL2(N664), .Z({N693, N692}) );
  SELECT_OP C1768 ( .DATA1(N676), .DATA2(1'b0), .CONTROL1(N70), .CONTROL2(N664), .Z(N694) );
  SELECT_OP C1769 ( .DATA1(N688), .DATA2(1'b0), .CONTROL1(N70), .CONTROL2(N664), .Z(N695) );
  SELECT_OP C1770 ( .DATA1(N689), .DATA2(1'b0), .CONTROL1(N70), .CONTROL2(N664), .Z(N696) );
  SELECT_OP C1771 ( .DATA1({1'b0, 1'b1}), .DATA2({1'b1, 1'b0}), .CONTROL1(N71), 
        .CONTROL2(N765), .Z({N702, N701}) );
  GTECH_BUF B_71 ( .A(rx_ack_to), .Z(N71) );
  SELECT_OP C1772 ( .DATA1(pid_seq_err), .DATA2(1'b0), .CONTROL1(N72), 
        .CONTROL2(N73), .Z(N706) );
  GTECH_BUF B_72 ( .A(N856), .Z(N72) );
  GTECH_BUF B_73 ( .A(N855), .Z(N73) );
  SELECT_OP C1773 ( .DATA1({1'b0, 1'b0, 1'b1}), .DATA2({N856, N855, 1'b0}), 
        .CONTROL1(N74), .CONTROL2(N767), .Z({N709, N708, N707}) );
  GTECH_BUF B_74 ( .A(N703), .Z(N74) );
  SELECT_OP C1774 ( .DATA1(1'b0), .DATA2(N706), .DATA3(1'b0), .CONTROL1(N74), 
        .CONTROL2(N767), .CONTROL3(N705), .Z(N710) );
  SELECT_OP C1775 ( .DATA1({1'b0, 1'b1}), .DATA2({1'b0, 1'b1}), .DATA3({1'b0, 
        1'b1}), .DATA4({1'b1, 1'b0}), .CONTROL1(N75), .CONTROL2(N769), 
        .CONTROL3(N772), .CONTROL4(N714), .Z({N717, N716}) );
  GTECH_BUF B_75 ( .A(abort), .Z(N75) );
  SELECT_OP C1776 ( .DATA1(1'b0), .DATA2(1'b1), .DATA3(1'b0), .DATA4(N715), 
        .CONTROL1(N75), .CONTROL2(N769), .CONTROL3(N772), .CONTROL4(N714), .Z(
        N718) );
  SELECT_OP C1777 ( .DATA1(N715), .DATA2(1'b0), .CONTROL1(N714), .CONTROL2(
        N719), .Z(N720) );
  SELECT_OP C1778 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b1), .DATA4(1'b1), 
        .CONTROL1(N75), .CONTROL2(N769), .CONTROL3(N772), .CONTROL4(N714), .Z(
        N721) );
  GTECH_NOT I_22 ( .A(N722), .Z(N723) );
  SELECT_OP C1780 ( .DATA1(N696), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .DATA10(1'b0), .CONTROL1(N76), .CONTROL2(N77), .CONTROL3(N78), 
        .CONTROL4(N79), .CONTROL5(N80), .CONTROL6(N81), .CONTROL7(N82), 
        .CONTROL8(N83), .CONTROL9(N84), .CONTROL10(N732), .Z(N733) );
  GTECH_BUF B_76 ( .A(N584), .Z(N76) );
  GTECH_BUF B_77 ( .A(N594), .Z(N77) );
  GTECH_BUF B_78 ( .A(N604), .Z(N78) );
  GTECH_BUF B_79 ( .A(N613), .Z(N79) );
  GTECH_BUF B_80 ( .A(N622), .Z(N80) );
  GTECH_BUF B_81 ( .A(N632), .Z(N81) );
  GTECH_BUF B_82 ( .A(N642), .Z(N82) );
  GTECH_BUF B_83 ( .A(N652), .Z(N83) );
  GTECH_BUF B_84 ( .A(N662), .Z(N84) );
  SELECT_OP C1781 ( .DATA1(N691), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .DATA10(1'b0), .CONTROL1(N76), .CONTROL2(N77), .CONTROL3(N78), 
        .CONTROL4(N79), .CONTROL5(N80), .CONTROL6(N81), .CONTROL7(N82), 
        .CONTROL8(N83), .CONTROL9(N84), .CONTROL10(N732), .Z(N734) );
  SELECT_OP C1782 ( .DATA1({N693, N692}), .DATA2({1'b0, 1'b0}), .DATA3({1'b0, 
        1'b0}), .DATA4({1'b0, 1'b0}), .DATA5({1'b0, 1'b0}), .DATA6({N720, N718}), .DATA7({1'b0, 1'b0}), .DATA8({1'b0, 1'b0}), .DATA9({1'b0, 1'b0}), .DATA10({
        1'b0, 1'b0}), .CONTROL1(N76), .CONTROL2(N77), .CONTROL3(N78), 
        .CONTROL4(N79), .CONTROL5(N80), .CONTROL6(N81), .CONTROL7(N82), 
        .CONTROL8(N83), .CONTROL9(N84), .CONTROL10(N732), .Z({N736, N735}) );
  SELECT_OP C1783 ( .DATA1(N694), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(N721), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .DATA10(1'b0), .CONTROL1(N76), .CONTROL2(N77), .CONTROL3(N78), 
        .CONTROL4(N79), .CONTROL5(N80), .CONTROL6(N81), .CONTROL7(N82), 
        .CONTROL8(N83), .CONTROL9(N84), .CONTROL10(N732), .Z(N737) );
  SELECT_OP C1784 ( .DATA1({N686, N685, N684, N683, N682, N681, N680, N679, 
        N678, N677}), .DATA2({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b1}), .DATA3({1'b0, N856, 1'b0, 1'b0, 1'b0, 1'b0, N855, 1'b0, 
        1'b0, 1'b0}), .DATA4({1'b0, N702, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, N701}), .DATA5({1'b0, 1'b0, N709, 1'b0, N708, 1'b0, 1'b0, 1'b0, 
        1'b0, N707}), .DATA6({1'b0, N717, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, N716}), .DATA7({1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0}), .DATA8({1'b1, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0}), .DATA9({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b1}), .DATA10(state), .CONTROL1(N76), .CONTROL2(N77), 
        .CONTROL3(N78), .CONTROL4(N79), .CONTROL5(N80), .CONTROL6(N81), 
        .CONTROL7(N82), .CONTROL8(N83), .CONTROL9(N84), .CONTROL10(N732), .Z({
        N747, N746, N745, N744, N743, N742, N741, N740, N739, N738}) );
  SELECT_OP C1785 ( .DATA1(N695), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .DATA10(1'b0), .CONTROL1(N76), .CONTROL2(N77), .CONTROL3(N78), 
        .CONTROL4(N79), .CONTROL5(N80), .CONTROL6(N81), .CONTROL7(N82), 
        .CONTROL8(N83), .CONTROL9(N84), .CONTROL10(N732), .Z(N748) );
  SELECT_OP C1786 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b1), .DATA6(1'b1), .DATA7(1'b1), .DATA8(1'b1), .DATA9(1'b1), 
        .DATA10(1'b1), .CONTROL1(N76), .CONTROL2(N77), .CONTROL3(N78), 
        .CONTROL4(N79), .CONTROL5(N80), .CONTROL6(N81), .CONTROL7(N82), 
        .CONTROL8(N83), .CONTROL9(N84), .CONTROL10(N732), .Z(N749) );
  SELECT_OP C1787 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(N710), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b0), 
        .DATA10(1'b0), .CONTROL1(N76), .CONTROL2(N77), .CONTROL3(N78), 
        .CONTROL4(N79), .CONTROL5(N80), .CONTROL6(N81), .CONTROL7(N82), 
        .CONTROL8(N83), .CONTROL9(N84), .CONTROL10(N732), .Z(N750) );
  SELECT_OP C1788 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b1), .DATA9(1'b0), 
        .DATA10(1'b0), .CONTROL1(N76), .CONTROL2(N77), .CONTROL3(N78), 
        .CONTROL4(N79), .CONTROL5(N80), .CONTROL6(N81), .CONTROL7(N82), 
        .CONTROL8(N83), .CONTROL9(N84), .CONTROL10(N732), .Z(N751) );
  SELECT_OP C1789 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(N723), .DATA9(1'b0), 
        .DATA10(1'b0), .CONTROL1(N76), .CONTROL2(N77), .CONTROL3(N78), 
        .CONTROL4(N79), .CONTROL5(N80), .CONTROL6(N81), .CONTROL7(N82), 
        .CONTROL8(N83), .CONTROL9(N84), .CONTROL10(N732), .Z(N752) );
  SELECT_OP C1790 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(N722), .DATA9(1'b0), 
        .DATA10(1'b0), .CONTROL1(N76), .CONTROL2(N77), .CONTROL3(N78), 
        .CONTROL4(N79), .CONTROL5(N80), .CONTROL6(N81), .CONTROL7(N82), 
        .CONTROL8(N83), .CONTROL9(N84), .CONTROL10(N732), .Z(N753) );
  SELECT_OP C1791 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .DATA8(1'b0), .DATA9(1'b1), 
        .DATA10(1'b0), .CONTROL1(N76), .CONTROL2(N77), .CONTROL3(N78), 
        .CONTROL4(N79), .CONTROL5(N80), .CONTROL6(N81), .CONTROL7(N82), 
        .CONTROL8(N83), .CONTROL9(N84), .CONTROL10(N732), .Z(N754) );
  SELECT_OP C1792 ( .DATA1(N753), .DATA2(1'b0), .CONTROL1(N85), .CONTROL2(N86), 
        .Z(buf0_rl_d) );
  GTECH_BUF B_85 ( .A(N575), .Z(N85) );
  GTECH_BUF B_86 ( .A(state[6]), .Z(N86) );
  SELECT_OP C1793 ( .DATA1(N754), .DATA2(1'b0), .CONTROL1(N85), .CONTROL2(N86), 
        .Z(uc_stat_set_d) );
  SELECT_OP C1794 ( .DATA1(N733), .DATA2(1'b0), .CONTROL1(N85), .CONTROL2(N86), 
        .Z(tx_dma_en) );
  SELECT_OP C1795 ( .DATA1(N734), .DATA2(1'b0), .CONTROL1(N85), .CONTROL2(N86), 
        .Z(rx_dma_en) );
  SELECT_OP C1796 ( .DATA1({N736, N735}), .DATA2({1'b0, 1'b0}), .CONTROL1(N85), 
        .CONTROL2(N86), .Z(token_pid_sel_d) );
  SELECT_OP C1797 ( .DATA1(N737), .DATA2(1'b0), .CONTROL1(N85), .CONTROL2(N86), 
        .Z(send_token_d) );
  SELECT_OP C1798 ( .DATA1(N748), .DATA2(1'b0), .CONTROL1(N85), .CONTROL2(N86), 
        .Z(send_zero_length) );
  SELECT_OP C1799 ( .DATA1(N749), .DATA2(1'b1), .CONTROL1(N85), .CONTROL2(N86), 
        .Z(rx_ack_to_clr_d) );
  SELECT_OP C1800 ( .DATA1(N750), .DATA2(1'b0), .CONTROL1(N85), .CONTROL2(N86), 
        .Z(int_seqerr_set_d) );
  SELECT_OP C1801 ( .DATA1(N751), .DATA2(1'b0), .CONTROL1(N85), .CONTROL2(N86), 
        .Z(int_set_en) );
  SELECT_OP C1802 ( .DATA1(N752), .DATA2(1'b0), .CONTROL1(N85), .CONTROL2(N86), 
        .Z(buf_set_d) );
  GTECH_AND2 C1805 ( .A(csr[15]), .B(N868), .Z(dma_en) );
  GTECH_OR2 C1807 ( .A(buf0[31]), .B(N885), .Z(N87) );
  GTECH_AND2 C1808 ( .A(N884), .B(buf0[0]), .Z(N885) );
  GTECH_AND2 C1809 ( .A(N883), .B(buf0[1]), .Z(N884) );
  GTECH_AND2 C1810 ( .A(N882), .B(buf0[2]), .Z(N883) );
  GTECH_AND2 C1811 ( .A(N881), .B(buf0[3]), .Z(N882) );
  GTECH_AND2 C1812 ( .A(N880), .B(buf0[4]), .Z(N881) );
  GTECH_AND2 C1813 ( .A(N879), .B(buf0[5]), .Z(N880) );
  GTECH_AND2 C1814 ( .A(N878), .B(buf0[6]), .Z(N879) );
  GTECH_AND2 C1815 ( .A(N877), .B(buf0[7]), .Z(N878) );
  GTECH_AND2 C1816 ( .A(N876), .B(buf0[8]), .Z(N877) );
  GTECH_AND2 C1817 ( .A(N875), .B(buf0[9]), .Z(N876) );
  GTECH_AND2 C1818 ( .A(N874), .B(buf0[10]), .Z(N875) );
  GTECH_AND2 C1819 ( .A(N873), .B(buf0[11]), .Z(N874) );
  GTECH_AND2 C1820 ( .A(N872), .B(buf0[12]), .Z(N873) );
  GTECH_AND2 C1821 ( .A(N871), .B(buf0[13]), .Z(N872) );
  GTECH_AND2 C1822 ( .A(N870), .B(buf0[14]), .Z(N871) );
  GTECH_AND2 C1823 ( .A(buf0[16]), .B(buf0[15]), .Z(N870) );
  GTECH_OR2 C1824 ( .A(buf1[31]), .B(N901), .Z(N88) );
  GTECH_AND2 C1825 ( .A(N900), .B(buf1[0]), .Z(N901) );
  GTECH_AND2 C1826 ( .A(N899), .B(buf1[1]), .Z(N900) );
  GTECH_AND2 C1827 ( .A(N898), .B(buf1[2]), .Z(N899) );
  GTECH_AND2 C1828 ( .A(N897), .B(buf1[3]), .Z(N898) );
  GTECH_AND2 C1829 ( .A(N896), .B(buf1[4]), .Z(N897) );
  GTECH_AND2 C1830 ( .A(N895), .B(buf1[5]), .Z(N896) );
  GTECH_AND2 C1831 ( .A(N894), .B(buf1[6]), .Z(N895) );
  GTECH_AND2 C1832 ( .A(N893), .B(buf1[7]), .Z(N894) );
  GTECH_AND2 C1833 ( .A(N892), .B(buf1[8]), .Z(N893) );
  GTECH_AND2 C1834 ( .A(N891), .B(buf1[9]), .Z(N892) );
  GTECH_AND2 C1835 ( .A(N890), .B(buf1[10]), .Z(N891) );
  GTECH_AND2 C1836 ( .A(N889), .B(buf1[11]), .Z(N890) );
  GTECH_AND2 C1837 ( .A(N888), .B(buf1[12]), .Z(N889) );
  GTECH_AND2 C1838 ( .A(N887), .B(buf1[13]), .Z(N888) );
  GTECH_AND2 C1839 ( .A(N886), .B(buf1[14]), .Z(N887) );
  GTECH_AND2 C1840 ( .A(buf1[16]), .B(buf1[15]), .Z(N886) );
  GTECH_AND2 C1841 ( .A(N916), .B(buf0[0]), .Z(N89) );
  GTECH_AND2 C1842 ( .A(N915), .B(buf0[1]), .Z(N916) );
  GTECH_AND2 C1843 ( .A(N914), .B(buf0[2]), .Z(N915) );
  GTECH_AND2 C1844 ( .A(N913), .B(buf0[3]), .Z(N914) );
  GTECH_AND2 C1845 ( .A(N912), .B(buf0[4]), .Z(N913) );
  GTECH_AND2 C1846 ( .A(N911), .B(buf0[5]), .Z(N912) );
  GTECH_AND2 C1847 ( .A(N910), .B(buf0[6]), .Z(N911) );
  GTECH_AND2 C1848 ( .A(N909), .B(buf0[7]), .Z(N910) );
  GTECH_AND2 C1849 ( .A(N908), .B(buf0[8]), .Z(N909) );
  GTECH_AND2 C1850 ( .A(N907), .B(buf0[9]), .Z(N908) );
  GTECH_AND2 C1851 ( .A(N906), .B(buf0[10]), .Z(N907) );
  GTECH_AND2 C1852 ( .A(N905), .B(buf0[11]), .Z(N906) );
  GTECH_AND2 C1853 ( .A(N904), .B(buf0[12]), .Z(N905) );
  GTECH_AND2 C1854 ( .A(N903), .B(buf0[13]), .Z(N904) );
  GTECH_AND2 C1855 ( .A(N902), .B(buf0[14]), .Z(N903) );
  GTECH_AND2 C1856 ( .A(buf0[16]), .B(buf0[15]), .Z(N902) );
  GTECH_AND2 C1857 ( .A(N931), .B(buf1[0]), .Z(N90) );
  GTECH_AND2 C1858 ( .A(N930), .B(buf1[1]), .Z(N931) );
  GTECH_AND2 C1859 ( .A(N929), .B(buf1[2]), .Z(N930) );
  GTECH_AND2 C1860 ( .A(N928), .B(buf1[3]), .Z(N929) );
  GTECH_AND2 C1861 ( .A(N927), .B(buf1[4]), .Z(N928) );
  GTECH_AND2 C1862 ( .A(N926), .B(buf1[5]), .Z(N927) );
  GTECH_AND2 C1863 ( .A(N925), .B(buf1[6]), .Z(N926) );
  GTECH_AND2 C1864 ( .A(N924), .B(buf1[7]), .Z(N925) );
  GTECH_AND2 C1865 ( .A(N923), .B(buf1[8]), .Z(N924) );
  GTECH_AND2 C1866 ( .A(N922), .B(buf1[9]), .Z(N923) );
  GTECH_AND2 C1867 ( .A(N921), .B(buf1[10]), .Z(N922) );
  GTECH_AND2 C1868 ( .A(N920), .B(buf1[11]), .Z(N921) );
  GTECH_AND2 C1869 ( .A(N919), .B(buf1[12]), .Z(N920) );
  GTECH_AND2 C1870 ( .A(N918), .B(buf1[13]), .Z(N919) );
  GTECH_AND2 C1871 ( .A(N917), .B(buf1[14]), .Z(N918) );
  GTECH_AND2 C1872 ( .A(buf1[16]), .B(buf1[15]), .Z(N917) );
  GTECH_AND2 C1873 ( .A(N934), .B(N935), .Z(N91) );
  GTECH_AND2 C1874 ( .A(token_valid), .B(N933), .Z(N934) );
  GTECH_OR2 C1875 ( .A(N932), .B(pid_SETUP), .Z(N933) );
  GTECH_OR2 C1876 ( .A(pid_OUT), .B(pid_IN), .Z(N932) );
  GTECH_NOT I_23 ( .A(match), .Z(N935) );
  GTECH_NOT I_24 ( .A(tr_fr_d[1]), .Z(N92) );
  GTECH_NOT I_25 ( .A(tr_fr_d[0]), .Z(N97) );
  GTECH_NOT I_26 ( .A(N106), .Z(N107) );
  GTECH_NOT I_27 ( .A(N110), .Z(N111) );
  GTECH_NOT I_28 ( .A(N115), .Z(N116) );
  GTECH_NOT I_29 ( .A(N123), .Z(N124) );
  GTECH_NOT I_30 ( .A(N129), .Z(N130) );
  GTECH_NOT I_31 ( .A(N135), .Z(N136) );
  GTECH_NOT I_32 ( .A(N143), .Z(N144) );
  GTECH_OR2 C1902 ( .A(N145), .B(N146), .Z(N147) );
  GTECH_NOT I_33 ( .A(csr[25]), .Z(N148) );
  GTECH_OR2 C1904 ( .A(N151), .B(N153), .Z(N154) );
  GTECH_OR2 C1905 ( .A(N155), .B(N156), .Z(N157) );
  GTECH_OR2 C1907 ( .A(N161), .B(N164), .Z(N165) );
  GTECH_NOT I_34 ( .A(pid_MDATA), .Z(N167) );
  GTECH_NOT I_35 ( .A(N168), .Z(N169) );
  GTECH_NOT I_36 ( .A(pid_DATA1), .Z(N170) );
  GTECH_NOT I_37 ( .A(N171), .Z(N172) );
  GTECH_OR2 C1932 ( .A(N172), .B(N169), .Z(N173) );
  GTECH_NOT I_38 ( .A(N173), .Z(N174) );
  GTECH_NOT I_39 ( .A(N176), .Z(N177) );
  GTECH_NOT I_40 ( .A(pid_DATA2), .Z(N178) );
  GTECH_NOT I_41 ( .A(N179), .Z(N180) );
  GTECH_OR2 C1939 ( .A(N180), .B(N177), .Z(N181) );
  GTECH_NOT I_42 ( .A(N181), .Z(N182) );
  GTECH_NOT I_43 ( .A(in_op), .Z(N184) );
  GTECH_NOT I_44 ( .A(N187), .Z(N188) );
  GTECH_NOT I_45 ( .A(csr[29]), .Z(N189) );
  GTECH_NOT I_46 ( .A(N192), .Z(N193) );
  GTECH_NOT I_47 ( .A(setup_token), .Z(N194) );
  GTECH_NOT I_48 ( .A(csr[28]), .Z(N195) );
  GTECH_OR2 C1956 ( .A(N188), .B(setup_token), .Z(N202) );
  GTECH_OR2 C1957 ( .A(N193), .B(N202), .Z(N203) );
  GTECH_OR2 C1958 ( .A(N198), .B(N203), .Z(N204) );
  GTECH_OR2 C1959 ( .A(N201), .B(N204), .Z(N205) );
  GTECH_NOT I_49 ( .A(N205), .Z(N206) );
  GTECH_OR2 C1961 ( .A(N100), .B(N96), .Z(N210) );
  GTECH_OR2 C1962 ( .A(N103), .B(N210), .Z(N211) );
  GTECH_OR2 C1963 ( .A(N107), .B(N211), .Z(N212) );
  GTECH_OR2 C1964 ( .A(N111), .B(N212), .Z(N213) );
  GTECH_OR2 C1965 ( .A(N116), .B(N213), .Z(N214) );
  GTECH_OR2 C1966 ( .A(N120), .B(N214), .Z(N215) );
  GTECH_OR2 C1967 ( .A(N124), .B(N215), .Z(N216) );
  GTECH_OR2 C1968 ( .A(N130), .B(N216), .Z(N217) );
  GTECH_OR2 C1969 ( .A(N136), .B(N217), .Z(N218) );
  GTECH_OR2 C1970 ( .A(N144), .B(N218), .Z(N219) );
  GTECH_OR2 C1971 ( .A(N147), .B(N219), .Z(N220) );
  GTECH_OR2 C1972 ( .A(N154), .B(N220), .Z(N221) );
  GTECH_OR2 C1973 ( .A(N157), .B(N221), .Z(N222) );
  GTECH_OR2 C1974 ( .A(N165), .B(N222), .Z(N223) );
  GTECH_OR2 C1975 ( .A(N166), .B(N223), .Z(N224) );
  GTECH_NOT I_50 ( .A(N224), .Z(N225) );
  GTECH_NOT I_51 ( .A(pid_DATA0), .Z(N229) );
  GTECH_NOT I_52 ( .A(N231), .Z(N232) );
  GTECH_NOT I_53 ( .A(N235), .Z(N236) );
  GTECH_NOT I_54 ( .A(N238), .Z(N239) );
  GTECH_NOT I_55 ( .A(N242), .Z(N243) );
  GTECH_OR2 C1986 ( .A(N236), .B(N232), .Z(N244) );
  GTECH_OR2 C1987 ( .A(N239), .B(N244), .Z(N245) );
  GTECH_OR2 C1988 ( .A(N243), .B(N245), .Z(N246) );
  GTECH_NOT I_56 ( .A(N246), .Z(N247) );
  GTECH_NOT I_57 ( .A(N262), .Z(N263) );
  GTECH_NOT I_58 ( .A(N266), .Z(N267) );
  GTECH_NOT I_59 ( .A(N270), .Z(N271) );
  GTECH_NOT I_60 ( .A(N274), .Z(N275) );
  GTECH_NOT I_61 ( .A(N278), .Z(N279) );
  GTECH_NOT I_62 ( .A(N291), .Z(N292) );
  GTECH_NOT I_63 ( .A(N295), .Z(N296) );
  GTECH_NOT I_64 ( .A(N299), .Z(N300) );
  GTECH_NOT I_65 ( .A(N303), .Z(N304) );
  GTECH_NOT I_66 ( .A(N307), .Z(N308) );
  GTECH_OR2 C2042 ( .A(N304), .B(setup_token), .Z(N315) );
  GTECH_OR2 C2043 ( .A(N308), .B(N315), .Z(N316) );
  GTECH_OR2 C2044 ( .A(N311), .B(N316), .Z(N317) );
  GTECH_OR2 C2045 ( .A(N314), .B(N317), .Z(N318) );
  GTECH_NOT I_67 ( .A(N318), .Z(N319) );
  GTECH_OR2 C2047 ( .A(N256), .B(N253), .Z(N322) );
  GTECH_OR2 C2048 ( .A(N259), .B(N322), .Z(N323) );
  GTECH_OR2 C2049 ( .A(N263), .B(N323), .Z(N324) );
  GTECH_OR2 C2050 ( .A(N267), .B(N324), .Z(N325) );
  GTECH_OR2 C2051 ( .A(N271), .B(N325), .Z(N326) );
  GTECH_OR2 C2052 ( .A(N275), .B(N326), .Z(N327) );
  GTECH_OR2 C2053 ( .A(N279), .B(N327), .Z(N328) );
  GTECH_OR2 C2054 ( .A(N283), .B(N328), .Z(N329) );
  GTECH_OR2 C2055 ( .A(N288), .B(N329), .Z(N330) );
  GTECH_OR2 C2056 ( .A(N292), .B(N330), .Z(N331) );
  GTECH_OR2 C2057 ( .A(N296), .B(N331), .Z(N332) );
  GTECH_OR2 C2058 ( .A(N300), .B(N332), .Z(N333) );
  GTECH_OR2 C2059 ( .A(N147), .B(N333), .Z(N334) );
  GTECH_OR2 C2060 ( .A(N154), .B(N334), .Z(N335) );
  GTECH_OR2 C2061 ( .A(N157), .B(N335), .Z(N336) );
  GTECH_OR2 C2062 ( .A(N165), .B(N336), .Z(N337) );
  GTECH_OR2 C2063 ( .A(N166), .B(N337), .Z(N338) );
  GTECH_NOT I_68 ( .A(N338), .Z(N339) );
  GTECH_NOT I_69 ( .A(N942), .Z(N343) );
  GTECH_OR2 C2066 ( .A(N940), .B(N941), .Z(N942) );
  GTECH_OR2 C2067 ( .A(N938), .B(N939), .Z(N940) );
  GTECH_OR2 C2068 ( .A(N936), .B(N937), .Z(N938) );
  GTECH_AND2 C2069 ( .A(N825), .B(pid_DATA0), .Z(N936) );
  GTECH_AND2 C2070 ( .A(N828), .B(pid_DATA1), .Z(N937) );
  GTECH_AND2 C2071 ( .A(N831), .B(pid_DATA2), .Z(N939) );
  GTECH_AND2 C2072 ( .A(N832), .B(pid_MDATA), .Z(N941) );
  GTECH_NOT I_70 ( .A(rst), .Z(N344) );
  GTECH_OR2 C2074 ( .A(pid_OUT), .B(pid_SETUP), .Z(N345) );
  GTECH_OR2 C2077 ( .A(pid_IN), .B(N344), .Z(N346) );
  GTECH_OR2 C2078 ( .A(N345), .B(N346), .Z(N347) );
  GTECH_NOT I_71 ( .A(N347), .Z(N348) );
  GTECH_AND2 C2081 ( .A(pid_IN), .B(rst), .Z(N351) );
  GTECH_NOT I_72 ( .A(pid_IN), .Z(N352) );
  GTECH_AND2 C2083 ( .A(rst), .B(N352), .Z(N353) );
  GTECH_AND2 C2084 ( .A(N345), .B(N353), .Z(N354) );
  GTECH_OR2 C2086 ( .A(N345), .B(N344), .Z(N355) );
  GTECH_OR2 C2087 ( .A(pid_IN), .B(N355), .Z(N356) );
  GTECH_NOT I_73 ( .A(N356), .Z(N357) );
  GTECH_AND2 C2089 ( .A(N345), .B(rst), .Z(N360) );
  GTECH_NOT I_74 ( .A(N345), .Z(N361) );
  GTECH_AND2 C2091 ( .A(rst), .B(N361), .Z(N362) );
  GTECH_AND2 C2092 ( .A(pid_IN), .B(N362), .Z(N363) );
  GTECH_OR2 C2093 ( .A(pid_OUT), .B(pid_IN), .Z(N364) );
  GTECH_OR2 C2096 ( .A(pid_SETUP), .B(N344), .Z(N365) );
  GTECH_OR2 C2097 ( .A(N364), .B(N365), .Z(N366) );
  GTECH_NOT I_75 ( .A(N366), .Z(N367) );
  GTECH_AND2 C2099 ( .A(pid_SETUP), .B(rst), .Z(N370) );
  GTECH_NOT I_76 ( .A(pid_SETUP), .Z(N371) );
  GTECH_AND2 C2101 ( .A(rst), .B(N371), .Z(N372) );
  GTECH_AND2 C2102 ( .A(N364), .B(N372), .Z(N373) );
  GTECH_OR2 C2103 ( .A(N864), .B(N943), .Z(in_op) );
  GTECH_AND2 C2104 ( .A(N869), .B(in_token), .Z(N943) );
  GTECH_OR2 C2105 ( .A(N867), .B(N944), .Z(out_op) );
  GTECH_AND2 C2106 ( .A(N869), .B(out_token), .Z(N944) );
  GTECH_NOT I_77 ( .A(dma_en), .Z(N374) );
  GTECH_AND2 C2113 ( .A(N945), .B(N946), .Z(N375) );
  GTECH_OR2 C2114 ( .A(csr[30]), .B(buf0_na), .Z(N945) );
  GTECH_NOT I_78 ( .A(buf1_na), .Z(N946) );
  GTECH_NOT I_79 ( .A(buf_sel), .Z(N377) );
  GTECH_NOT I_80 ( .A(buf_smaller), .Z(N395) );
  GTECH_AND2 C2128 ( .A(dma_en), .B(N951), .Z(no_buf0_dma) );
  GTECH_OR2 C2129 ( .A(N948), .B(N950), .Z(N951) );
  GTECH_AND2 C2130 ( .A(N864), .B(N947), .Z(N948) );
  GTECH_NOT I_81 ( .A(dma_in_buf_sz1), .Z(N947) );
  GTECH_AND2 C2132 ( .A(N867), .B(N949), .Z(N950) );
  GTECH_NOT I_82 ( .A(dma_out_buf_avail), .Z(N949) );
  GTECH_AND2 C2137 ( .A(buffer_full), .B(N374), .Z(N400) );
  GTECH_OR2 C2139 ( .A(N952), .B(N401), .Z(N402) );
  GTECH_OR2 C2140 ( .A(buf0_na), .B(no_buf0_dma), .Z(N952) );
  GTECH_OR2 C2144 ( .A(buf1_na), .B(N403), .Z(N404) );
  GTECH_AND2 C2145 ( .A(no_bufs0), .B(no_bufs1), .Z(no_bufs) );
  GTECH_AND2 C2146 ( .A(out_op), .B(dma_en), .Z(N405) );
  GTECH_NOT I_83 ( .A(N405), .Z(N406) );
  GTECH_AND2 C2152 ( .A(out_op), .B(dma_en), .Z(N449) );
  GTECH_OR2 C2155 ( .A(in_op), .B(N449), .Z(N450) );
  GTECH_NOT I_84 ( .A(N450), .Z(N451) );
  GTECH_NOT I_85 ( .A(N449), .Z(N466) );
  GTECH_AND2 C2158 ( .A(in_op), .B(N466), .Z(N467) );
  GTECH_AND2 C2159 ( .A(N468), .B(rx_data_valid), .Z(N469) );
  GTECH_AND2 C2160 ( .A(N954), .B(N470), .Z(N471) );
  GTECH_AND2 C2161 ( .A(N953), .B(dma_en), .Z(N954) );
  GTECH_AND2 C2162 ( .A(uc_stat_set_d), .B(out_op), .Z(N953) );
  GTECH_AND2 C2163 ( .A(N955), .B(N472), .Z(N473) );
  GTECH_NOT I_86 ( .A(csr[16]), .Z(N955) );
  GTECH_AND2 C2165 ( .A(N956), .B(N474), .Z(N475) );
  GTECH_NOT I_87 ( .A(csr[17]), .Z(N956) );
  GTECH_OR2 C2169 ( .A(buffer_done), .B(dma_en), .Z(N476) );
  GTECH_NOT I_88 ( .A(N476), .Z(N477) );
  GTECH_BUF B_87 ( .A(N482), .Z(N478) );
  GTECH_NOT I_89 ( .A(dma_en), .Z(N481) );
  GTECH_AND2 C2173 ( .A(buffer_done), .B(N481), .Z(N482) );
  GTECH_AND2 C2174 ( .A(N478), .B(N481) );
  GTECH_NOT I_90 ( .A(out_to_small_r), .Z(N483) );
  GTECH_OR2 C2183 ( .A(out_to_small_r), .B(buf_set_d), .Z(N512) );
  GTECH_NOT I_91 ( .A(N512), .Z(N513) );
  GTECH_NOT I_92 ( .A(buf_set_d), .Z(N518) );
  GTECH_AND2 C2186 ( .A(out_to_small_r), .B(N518), .Z(N519) );
  GTECH_AND2 C2187 ( .A(N377), .B(buf_set_d), .Z(N520) );
  GTECH_AND2 C2189 ( .A(buf_sel), .B(buf_set_d), .Z(N521) );
  GTECH_OR2 C2190 ( .A(N958), .B(N959), .Z(N522) );
  GTECH_OR2 C2191 ( .A(buffer_overflow), .B(N957), .Z(N958) );
  GTECH_AND2 C2192 ( .A(match), .B(N842), .Z(N957) );
  GTECH_AND2 C2193 ( .A(match_r), .B(to_large), .Z(N959) );
  GTECH_OR2 C2194 ( .A(tx_valid), .B(rx_ack_to_clr_d), .Z(N523) );
  GTECH_NOT I_93 ( .A(rx_ack_to_clr), .Z(N524) );
  GTECH_BUF B_88 ( .A(N524) );
  GTECH_NOT I_94 ( .A(mode_hs), .Z(N542) );
  GTECH_BUF B_89 ( .A(mode_hs), .Z(tx_data_to_val[4]) );
  GTECH_BUF B_90 ( .A(N542), .Z(tx_data_to_val[5]) );
  GTECH_NOT I_95 ( .A(rx_active), .Z(N543) );
  GTECH_BUF B_91 ( .A(N543) );
  GTECH_AND2 C2206 ( .A(N961), .B(N962), .Z(int_buf1_set) );
  GTECH_AND2 C2207 ( .A(N960), .B(int_set_en), .Z(N961) );
  GTECH_AND2 C2208 ( .A(N377), .B(buffer_done), .Z(N960) );
  GTECH_NOT I_96 ( .A(buf1_not_aloc), .Z(N962) );
  GTECH_AND2 C2211 ( .A(N964), .B(N965), .Z(int_buf0_set) );
  GTECH_AND2 C2212 ( .A(N963), .B(int_set_en), .Z(N964) );
  GTECH_AND2 C2213 ( .A(buf_sel), .B(buffer_done), .Z(N963) );
  GTECH_NOT I_97 ( .A(buf0_not_aloc), .Z(N965) );
  GTECH_AND2 C2215 ( .A(N967), .B(N979), .Z(N561) );
  GTECH_AND2 C2216 ( .A(match_r), .B(N966), .Z(N967) );
  GTECH_NOT I_98 ( .A(pid_SOF), .Z(N966) );
  GTECH_OR2 C2218 ( .A(N973), .B(N978), .Z(N979) );
  GTECH_OR2 C2219 ( .A(N970), .B(N972), .Z(N973) );
  GTECH_AND2 C2220 ( .A(N867), .B(N969), .Z(N970) );
  GTECH_NOT I_99 ( .A(N968), .Z(N969) );
  GTECH_OR2 C2222 ( .A(pid_OUT_r), .B(pid_PING_r), .Z(N968) );
  GTECH_AND2 C2223 ( .A(N864), .B(N971), .Z(N972) );
  GTECH_NOT I_100 ( .A(pid_IN_r), .Z(N971) );
  GTECH_AND2 C2225 ( .A(N869), .B(N977), .Z(N978) );
  GTECH_NOT I_101 ( .A(N976), .Z(N977) );
  GTECH_OR2 C2227 ( .A(N975), .B(pid_SETUP_r), .Z(N976) );
  GTECH_OR2 C2228 ( .A(N974), .B(pid_PING_r), .Z(N975) );
  GTECH_OR2 C2229 ( .A(pid_IN_r), .B(pid_OUT_r), .Z(N974) );
  GTECH_OR2 C2230 ( .A(N980), .B(N981), .Z(int_to_set) );
  GTECH_AND2 C2231 ( .A(N798), .B(rx_ack_to), .Z(N980) );
  GTECH_AND2 C2232 ( .A(N809), .B(tx_data_to), .Z(N981) );
  GTECH_AND2 C2233 ( .A(rx_data_done), .B(crc16_err), .Z(int_crc16_set) );
  GTECH_OR2 C2235 ( .A(match), .B(N344), .Z(N562) );
  GTECH_NOT I_102 ( .A(N562), .Z(N563) );
  GTECH_AND2 C2237 ( .A(match), .B(rst), .Z(N574) );
  GTECH_NOT I_103 ( .A(state[6]), .Z(N575) );
  GTECH_NOT I_104 ( .A(N583), .Z(N584) );
  GTECH_NOT I_105 ( .A(state[1]), .Z(N585) );
  GTECH_NOT I_106 ( .A(N593), .Z(N594) );
  GTECH_NOT I_107 ( .A(state[2]), .Z(N595) );
  GTECH_NOT I_108 ( .A(N603), .Z(N604) );
  GTECH_NOT I_109 ( .A(N612), .Z(N613) );
  GTECH_NOT I_110 ( .A(N621), .Z(N622) );
  GTECH_NOT I_111 ( .A(state[5]), .Z(N623) );
  GTECH_NOT I_112 ( .A(N631), .Z(N632) );
  GTECH_NOT I_113 ( .A(state[7]), .Z(N633) );
  GTECH_NOT I_114 ( .A(N641), .Z(N642) );
  GTECH_NOT I_115 ( .A(state[8]), .Z(N643) );
  GTECH_NOT I_116 ( .A(N651), .Z(N652) );
  GTECH_NOT I_117 ( .A(state[9]), .Z(N653) );
  GTECH_NOT I_118 ( .A(N661), .Z(N662) );
  GTECH_AND2 C2267 ( .A(N982), .B(N966), .Z(N663) );
  GTECH_AND2 C2268 ( .A(match_r), .B(N858), .Z(N982) );
  GTECH_NOT I_119 ( .A(N663), .Z(N664) );
  GTECH_OR2 C2273 ( .A(N987), .B(N989), .Z(N665) );
  GTECH_OR2 C2274 ( .A(N984), .B(N986), .Z(N987) );
  GTECH_OR2 C2275 ( .A(N983), .B(no_buf0_dma), .Z(N984) );
  GTECH_AND2 C2276 ( .A(buf0_na), .B(buf1_na), .Z(N983) );
  GTECH_AND2 C2277 ( .A(N985), .B(buf1_na), .Z(N986) );
  GTECH_AND2 C2278 ( .A(N869), .B(pid_IN), .Z(N985) );
  GTECH_AND2 C2279 ( .A(N988), .B(buf0_na), .Z(N989) );
  GTECH_AND2 C2280 ( .A(N869), .B(pid_OUT), .Z(N988) );
  GTECH_AND2 C2281 ( .A(pid_PING), .B(mode_hs), .Z(N666) );
  GTECH_OR2 C2282 ( .A(N864), .B(N990), .Z(N667) );
  GTECH_AND2 C2283 ( .A(N869), .B(pid_IN), .Z(N990) );
  GTECH_OR2 C2284 ( .A(N867), .B(N992), .Z(N668) );
  GTECH_AND2 C2285 ( .A(N869), .B(N991), .Z(N992) );
  GTECH_OR2 C2286 ( .A(pid_OUT), .B(pid_SETUP), .Z(N991) );
  GTECH_OR2 C2292 ( .A(N665), .B(N861), .Z(N669) );
  GTECH_OR2 C2293 ( .A(N666), .B(N669), .Z(N670) );
  GTECH_OR2 C2294 ( .A(N667), .B(N670), .Z(N671) );
  GTECH_OR2 C2295 ( .A(N668), .B(N671), .Z(N672) );
  GTECH_NOT I_120 ( .A(N672), .Z(N673) );
  GTECH_NOT I_121 ( .A(N670), .Z(N675) );
  GTECH_NOT I_122 ( .A(N671), .Z(N687) );
  GTECH_NOT I_123 ( .A(idma_done), .Z(N697) );
  GTECH_AND2 C2305 ( .A(token_valid), .B(pid_ACK), .Z(N698) );
  GTECH_OR2 C2308 ( .A(N698), .B(rx_ack_to), .Z(N699) );
  GTECH_NOT I_124 ( .A(N699), .Z(N700) );
  GTECH_OR2 C2310 ( .A(N993), .B(abort), .Z(N703) );
  GTECH_OR2 C2311 ( .A(tx_data_to), .B(crc16_err), .Z(N993) );
  GTECH_OR2 C2314 ( .A(rx_data_done), .B(N703), .Z(N704) );
  GTECH_NOT I_125 ( .A(N704), .Z(N705) );
  GTECH_OR2 C2317 ( .A(to_small), .B(to_large), .Z(N711) );
  GTECH_OR2 C2321 ( .A(N711), .B(abort), .Z(N712) );
  GTECH_OR2 C2322 ( .A(pid_seq_err), .B(N712), .Z(N713) );
  GTECH_NOT I_126 ( .A(N713), .Z(N714) );
  GTECH_AND2 C2324 ( .A(mode_hs), .B(no_bufs), .Z(N715) );
  GTECH_BUF B_92 ( .A(N713), .Z(N719) );
  GTECH_AND2 C2328 ( .A(buffer_done), .B(dma_en), .Z(N722) );
  GTECH_OR2 C2330 ( .A(N594), .B(N584), .Z(N724) );
  GTECH_OR2 C2331 ( .A(N604), .B(N724), .Z(N725) );
  GTECH_OR2 C2332 ( .A(N613), .B(N725), .Z(N726) );
  GTECH_OR2 C2333 ( .A(N622), .B(N726), .Z(N727) );
  GTECH_OR2 C2334 ( .A(N632), .B(N727), .Z(N728) );
  GTECH_OR2 C2335 ( .A(N642), .B(N728), .Z(N729) );
  GTECH_OR2 C2336 ( .A(N652), .B(N729), .Z(N730) );
  GTECH_OR2 C2337 ( .A(N662), .B(N730), .Z(N731) );
  GTECH_NOT I_127 ( .A(N731), .Z(N732) );
  GTECH_AND2 C2341 ( .A(N665), .B(N860), .Z(N674) );
  GTECH_NOT I_128 ( .A(N665), .Z(N755) );
  GTECH_AND2 C2343 ( .A(N860), .B(N755), .Z(N756) );
  GTECH_AND2 C2344 ( .A(N666), .B(N756), .Z(N757) );
  GTECH_NOT I_129 ( .A(N666), .Z(N758) );
  GTECH_AND2 C2346 ( .A(N756), .B(N758), .Z(N759) );
  GTECH_AND2 C2347 ( .A(N667), .B(N759), .Z(N760) );
  GTECH_NOT I_130 ( .A(N667), .Z(N761) );
  GTECH_AND2 C2349 ( .A(N759), .B(N761), .Z(N762) );
  GTECH_AND2 C2350 ( .A(N668), .B(N762), .Z(N763) );
  GTECH_NOT I_131 ( .A(rx_ack_to), .Z(N764) );
  GTECH_AND2 C2352 ( .A(N698), .B(N764), .Z(N765) );
  GTECH_NOT I_132 ( .A(N703), .Z(N766) );
  GTECH_AND2 C2354 ( .A(rx_data_done), .B(N766), .Z(N767) );
  GTECH_NOT I_133 ( .A(abort), .Z(N768) );
  GTECH_AND2 C2356 ( .A(N711), .B(N768), .Z(N769) );
  GTECH_NOT I_134 ( .A(N711), .Z(N770) );
  GTECH_AND2 C2358 ( .A(N768), .B(N770), .Z(N771) );
  GTECH_AND2 C2359 ( .A(pid_seq_err), .B(N771), .Z(N772) );
  GTECH_AND2 C2360 ( .A(N575), .B(N563), .Z(N773) );
  GTECH_AND2 C2361 ( .A(N584), .B(N773), .Z(N774) );
  GTECH_AND2 C2362 ( .A(N664), .B(N774), .Z(N775) );
  GTECH_AND2 C2363 ( .A(N604), .B(N773), .Z(N776) );
  GTECH_AND2 C2364 ( .A(N697), .B(N776), .Z(N777) );
  GTECH_OR2 C2365 ( .A(N775), .B(N777), .Z(N778) );
  GTECH_AND2 C2366 ( .A(N613), .B(N773), .Z(N779) );
  GTECH_AND2 C2367 ( .A(N700), .B(N779), .Z(N780) );
  GTECH_OR2 C2368 ( .A(N778), .B(N780), .Z(N781) );
  GTECH_AND2 C2369 ( .A(N622), .B(N773), .Z(N782) );
  GTECH_AND2 C2370 ( .A(N705), .B(N782), .Z(N783) );
  GTECH_OR2 C2371 ( .A(N781), .B(N783), .Z(N784) );
  GTECH_AND2 C2372 ( .A(state[6]), .B(N563), .Z(N785) );
  GTECH_OR2 C2373 ( .A(N784), .B(N785), .Z(N786) );
  GTECH_NOT I_135 ( .A(N786), .Z(N787) );
endmodule


module usbf_pl_SSRAM_HADR14 ( clk, rst, rx_data, rx_valid, rx_active, rx_err, 
        tx_data, tx_valid, tx_valid_last, tx_ready, tx_first, tx_valid_out, 
        mode_hs, usb_reset, usb_suspend, usb_attached, madr, mdout, mdin, mwe, 
        mreq, mack, fa, idin, ep_sel, match, dma_in_buf_sz1, dma_out_buf_avail, 
        buf0_rl, buf0_set, buf1_set, uc_bsel_set, uc_dpd_set, int_buf1_set, 
        int_buf0_set, int_upid_set, int_crc16_set, int_to_set, int_seqerr_set, 
        out_to_small, csr, buf0, buf1, frm_nat, pid_cs_err, nse_err, crc5_err
 );
  input [7:0] rx_data;
  output [7:0] tx_data;
  output [14:0] madr;
  output [31:0] mdout;
  input [31:0] mdin;
  input [6:0] fa;
  output [31:0] idin;
  output [3:0] ep_sel;
  input [31:0] csr;
  input [31:0] buf0;
  input [31:0] buf1;
  output [31:0] frm_nat;
  input clk, rst, rx_valid, rx_active, rx_err, tx_ready, tx_valid_out, mode_hs,
         usb_reset, usb_suspend, usb_attached, mack, match, dma_in_buf_sz1,
         dma_out_buf_avail;
  output tx_valid, tx_valid_last, tx_first, mwe, mreq, buf0_rl, buf0_set,
         buf1_set, uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set,
         int_upid_set, int_crc16_set, int_to_set, int_seqerr_set, out_to_small,
         pid_cs_err, nse_err, crc5_err;
  wire   N0, N1, N2, N3, pid_ACK, pid_NACK, pid_STALL, pid_NYET, pid_PRE,
         pid_ERR, pid_SPLIT, pid_bad1, pid_PING, pid_bad2, pid_bad, fsel,
         token_valid, match_o, pid_SOF, frame_no_we, frame_no_we_r, N4, N5, N6,
         N7, N8, N9, N10, N11, N12, N13, N14, N15, N16, N17, N18, N19, N20,
         N21, N22, frame_no_same, N23, N24, N25, N26, N27, N28, N29, N30, N31,
         N32, N33, N34, N35, N36, N37, N38, N39, N40, N41, N42, clr_sof_time,
         hms_clk, N43, N44, N45, N46, N47, N48, N49, N50, N51, N52, N53, N54,
         N55, N56, N57, N58, N59, N60, N61, N62, N63, N64, N65, N66, N67, N68,
         N69, N70, N71, N72, N73, N74, N75, N76, N77, N78, N79, N80, N81, N82,
         N83, N84, N85, N86, N87, N88, pid_OUT, pid_IN, pid_SETUP, pid_DATA0,
         pid_DATA1, pid_DATA2, pid_MDATA, rx_data_valid, rx_data_done,
         crc16_err, rx_seq_err, send_token, send_data, send_zero_length,
         rd_next, rx_dma_en, tx_dma_en, abort, idma_done, dma_en, N89, N90,
         N91, N92, N93, N94, N95, N96, N97, N98, N99, N100, N101, N102, N103,
         N104, N105, N106, N107, N108, N109, N110;
  wire   [10:0] frame_no;
  wire   [4:0] hms_cnt;
  wire   [6:0] token_fadr;
  wire   [7:0] rx_data_st;
  wire   [1:0] token_pid_sel;
  wire   [1:0] data_pid_sel;
  wire   [7:0] tx_data_st;
  wire   [16:0] adr;
  wire   [13:0] size;
  wire   [13:0] buf_size;
  wire   [10:0] sizu_c;
  assign frm_nat[12] = 1'b0;
  assign frm_nat[13] = 1'b0;
  assign frm_nat[14] = 1'b0;
  assign frm_nat[15] = 1'b0;
  assign frm_nat[27] = 1'b0;

  \**SEQGEN**  frame_no_we_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        frame_no_we), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        frame_no_we_r), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \frame_no_r_reg[10]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N18), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        frm_nat[26]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N7) );
  \**SEQGEN**  \frame_no_r_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N17), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[25]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N7) );
  \**SEQGEN**  \frame_no_r_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N16), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[24]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N7) );
  \**SEQGEN**  \frame_no_r_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N15), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[23]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N7) );
  \**SEQGEN**  \frame_no_r_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N14), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[22]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N7) );
  \**SEQGEN**  \frame_no_r_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N13), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[21]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N7) );
  \**SEQGEN**  \frame_no_r_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N12), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[20]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N7) );
  \**SEQGEN**  \frame_no_r_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N11), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[19]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N7) );
  \**SEQGEN**  \frame_no_r_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N10), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[18]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N7) );
  \**SEQGEN**  \frame_no_r_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N9), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[17]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N7) );
  \**SEQGEN**  \frame_no_r_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N8), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[16]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N7) );
  EQ_UNS_OP eq_1948 ( .A(frm_nat[26:16]), .B(frame_no), .Z(N21) );
  \**SEQGEN**  frame_no_same_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N22), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        frame_no_same), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \mfm_cnt_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N37), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[31]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N33) );
  \**SEQGEN**  \mfm_cnt_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N36), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[30]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N33) );
  \**SEQGEN**  \mfm_cnt_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N35), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[29]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N33) );
  \**SEQGEN**  \mfm_cnt_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N34), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[28]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N33) );
  \**SEQGEN**  clr_sof_time_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        frame_no_we), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        clr_sof_time), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \sof_time_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N70), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[11]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N58) );
  \**SEQGEN**  \sof_time_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N69), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[10]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N58) );
  \**SEQGEN**  \sof_time_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N68), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N58) );
  \**SEQGEN**  \sof_time_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N67), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N58) );
  \**SEQGEN**  \sof_time_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N66), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N58) );
  \**SEQGEN**  \sof_time_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N65), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N58) );
  \**SEQGEN**  \sof_time_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N64), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N58) );
  \**SEQGEN**  \sof_time_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N63), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N58) );
  \**SEQGEN**  \sof_time_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N62), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N58) );
  \**SEQGEN**  \sof_time_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N61), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N58) );
  \**SEQGEN**  \sof_time_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N60), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N58) );
  \**SEQGEN**  \sof_time_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N59), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(frm_nat[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N58) );
  \**SEQGEN**  \hms_cnt_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N86), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(hms_cnt[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \hms_cnt_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N85), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(hms_cnt[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \hms_cnt_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N84), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(hms_cnt[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \hms_cnt_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N83), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(hms_cnt[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \hms_cnt_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N82), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(hms_cnt[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  hms_clk_reg ( .clear(1'b0), .preset(1'b0), .next_state(N96), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(hms_clk), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  EQ_UNS_OP eq_1990 ( .A(token_fadr), .B(fa), .Z(fsel) );
  usbf_pd u0 ( .clk(clk), .rst(rst), .rx_data(rx_data), .rx_valid(rx_valid), 
        .rx_active(rx_active), .rx_err(rx_err), .pid_OUT(pid_OUT), .pid_IN(
        pid_IN), .pid_SOF(pid_SOF), .pid_SETUP(pid_SETUP), .pid_DATA0(
        pid_DATA0), .pid_DATA1(pid_DATA1), .pid_DATA2(pid_DATA2), .pid_MDATA(
        pid_MDATA), .pid_ACK(pid_ACK), .pid_NACK(pid_NACK), .pid_STALL(
        pid_STALL), .pid_NYET(pid_NYET), .pid_PRE(pid_PRE), .pid_ERR(pid_ERR), 
        .pid_SPLIT(pid_SPLIT), .pid_PING(pid_PING), .pid_cks_err(pid_cs_err), 
        .token_fadr(token_fadr), .token_endp(ep_sel), .token_valid(token_valid), .crc5_err(crc5_err), .frame_no(frame_no), .rx_data_st(rx_data_st), 
        .rx_data_valid(rx_data_valid), .rx_data_done(rx_data_done), 
        .crc16_err(crc16_err), .seq_err(rx_seq_err) );
  usbf_pa u1 ( .clk(clk), .rst(rst), .tx_data(tx_data), .tx_valid(tx_valid), 
        .tx_valid_last(tx_valid_last), .tx_ready(tx_ready), .tx_first(tx_first), .send_token(send_token), .token_pid_sel(token_pid_sel), .send_data(send_data), .data_pid_sel(data_pid_sel), .send_zero_length(send_zero_length), 
        .tx_data_st(tx_data_st), .rd_next(rd_next) );
  usbf_idma_SSRAM_HADR14 u2 ( .clk(clk), .rst(rst), .rx_data_st(rx_data_st), 
        .rx_data_valid(rx_data_valid), .rx_data_done(rx_data_done), 
        .send_data(send_data), .tx_data_st(tx_data_st), .rd_next(rd_next), 
        .rx_dma_en(rx_dma_en), .tx_dma_en(tx_dma_en), .abort(abort), 
        .idma_done(idma_done), .buf_size(buf_size), .dma_en(dma_en), 
        .send_zero_length(send_zero_length), .adr(adr), .size(size), .sizu_c(
        sizu_c), .madr(madr), .mdout(mdout), .mdin(mdin), .mwe(mwe), .mreq(
        mreq), .mack(mack) );
  usbf_pe_SSRAM_HADR14 u3 ( .clk(clk), .rst(rst), .tx_valid(tx_valid_out), 
        .rx_active(rx_active), .pid_OUT(pid_OUT), .pid_IN(pid_IN), .pid_SOF(
        pid_SOF), .pid_SETUP(pid_SETUP), .pid_DATA0(pid_DATA0), .pid_DATA1(
        pid_DATA1), .pid_DATA2(pid_DATA2), .pid_MDATA(pid_MDATA), .pid_ACK(
        pid_ACK), .pid_NACK(pid_NACK), .pid_STALL(pid_STALL), .pid_NYET(
        pid_NYET), .pid_PRE(pid_PRE), .pid_ERR(pid_ERR), .pid_SPLIT(pid_SPLIT), 
        .pid_PING(pid_PING), .mode_hs(mode_hs), .token_valid(token_valid), 
        .crc5_err(crc5_err), .rx_data_valid(rx_data_valid), .rx_data_done(
        rx_data_done), .crc16_err(crc16_err), .send_token(send_token), 
        .token_pid_sel(token_pid_sel), .data_pid_sel(data_pid_sel), 
        .send_zero_length(send_zero_length), .rx_dma_en(rx_dma_en), 
        .tx_dma_en(tx_dma_en), .abort(abort), .idma_done(idma_done), .adr(adr), 
        .size(size), .buf_size(buf_size), .sizu_c(sizu_c), .dma_en(dma_en), 
        .fsel(fsel), .idin(idin), .dma_in_buf_sz1(dma_in_buf_sz1), 
        .dma_out_buf_avail(dma_out_buf_avail), .ep_sel(ep_sel), .match(match_o), .nse_err(nse_err), .buf0_rl(buf0_rl), .buf0_set(buf0_set), .buf1_set(
        buf1_set), .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), 
        .int_buf1_set(int_buf1_set), .int_buf0_set(int_buf0_set), 
        .int_upid_set(int_upid_set), .int_crc16_set(int_crc16_set), 
        .int_to_set(int_to_set), .int_seqerr_set(int_seqerr_set), 
        .out_to_small(out_to_small), .csr(csr), .buf0(buf0), .buf1(buf1) );
  GTECH_NOT I_0 ( .A(hms_cnt[4]), .Z(N89) );
  GTECH_NOT I_1 ( .A(hms_cnt[3]), .Z(N90) );
  GTECH_NOT I_2 ( .A(hms_cnt[2]), .Z(N91) );
  GTECH_OR2 C134 ( .A(N90), .B(N89), .Z(N92) );
  GTECH_OR2 C135 ( .A(N91), .B(N92), .Z(N93) );
  GTECH_OR2 C136 ( .A(hms_cnt[1]), .B(N93), .Z(N94) );
  GTECH_OR2 C137 ( .A(hms_cnt[0]), .B(N94), .Z(N95) );
  GTECH_NOT I_3 ( .A(N95), .Z(N96) );
  ADD_UNS_OP add_1969_S2 ( .A(frm_nat[11:0]), .B(1'b1), .Z({N57, N56, N55, N54, 
        N53, N52, N51, N50, N49, N48, N47, N46}) );
  ADD_UNS_OP add_1982 ( .A(hms_cnt), .B(1'b1), .Z({N81, N80, N79, N78, N77})
         );
  ADD_UNS_OP add_1960_S2 ( .A(frm_nat[31:28]), .B(1'b1), .Z({N32, N31, N30, 
        N29}) );
  SELECT_OP C139 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N0), 
        .CONTROL2(N20), .CONTROL3(N6), .Z(N7) );
  GTECH_BUF B_0 ( .A(N4), .Z(N0) );
  SELECT_OP C140 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0}), .DATA2(frame_no), .CONTROL1(N0), .CONTROL2(N20), 
        .Z({N18, N17, N16, N15, N14, N13, N12, N11, N10, N9, N8}) );
  SELECT_OP C141 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N1), .CONTROL2(N39), .CONTROL3(N42), .CONTROL4(N27), .Z(N33)
         );
  GTECH_BUF B_1 ( .A(N23), .Z(N1) );
  SELECT_OP C142 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0}), .DATA2({1'b0, 1'b0, 1'b0, 
        1'b0}), .DATA3({N32, N31, N30, N29}), .CONTROL1(N1), .CONTROL2(N39), 
        .CONTROL3(N42), .Z({N37, N36, N35, N34}) );
  SELECT_OP C143 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N2), 
        .CONTROL2(N72), .CONTROL3(N44), .Z(N58) );
  GTECH_BUF B_2 ( .A(clr_sof_time), .Z(N2) );
  SELECT_OP C144 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0}), .DATA2({N57, N56, N55, N54, N53, N52, N51, 
        N50, N49, N48, N47, N46}), .CONTROL1(N2), .CONTROL2(N72), .Z({N70, N69, 
        N68, N67, N66, N65, N64, N63, N62, N61, N60, N59}) );
  SELECT_OP C145 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), .DATA2({1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0}), .DATA3({N81, N80, N79, N78, N77}), .CONTROL1(N3), 
        .CONTROL2(N88), .CONTROL3(N76), .Z({N86, N85, N84, N83, N82}) );
  GTECH_BUF B_3 ( .A(N73), .Z(N3) );
  GTECH_OR2 C148 ( .A(N101), .B(pid_SPLIT), .Z(pid_bad1) );
  GTECH_OR2 C149 ( .A(N100), .B(pid_ERR), .Z(N101) );
  GTECH_OR2 C150 ( .A(N99), .B(pid_PRE), .Z(N100) );
  GTECH_OR2 C151 ( .A(N98), .B(pid_NYET), .Z(N99) );
  GTECH_OR2 C152 ( .A(N97), .B(pid_STALL), .Z(N98) );
  GTECH_OR2 C153 ( .A(pid_ACK), .B(pid_NACK), .Z(N97) );
  GTECH_AND2 C154 ( .A(N102), .B(pid_PING), .Z(pid_bad2) );
  GTECH_NOT I_4 ( .A(mode_hs), .Z(N102) );
  GTECH_OR2 C156 ( .A(pid_bad1), .B(pid_bad2), .Z(pid_bad) );
  GTECH_AND2 C157 ( .A(N106), .B(N107), .Z(match_o) );
  GTECH_AND2 C158 ( .A(N105), .B(token_valid), .Z(N106) );
  GTECH_AND2 C159 ( .A(N104), .B(match), .Z(N105) );
  GTECH_AND2 C160 ( .A(N103), .B(fsel), .Z(N104) );
  GTECH_NOT I_5 ( .A(pid_bad), .Z(N103) );
  GTECH_NOT I_6 ( .A(crc5_err), .Z(N107) );
  GTECH_AND2 C163 ( .A(N109), .B(pid_SOF), .Z(frame_no_we) );
  GTECH_AND2 C164 ( .A(token_valid), .B(N108), .Z(N109) );
  GTECH_NOT I_7 ( .A(crc5_err), .Z(N108) );
  GTECH_NOT I_8 ( .A(rst), .Z(N4) );
  GTECH_OR2 C169 ( .A(frame_no_we_r), .B(N4), .Z(N5) );
  GTECH_NOT I_9 ( .A(N5), .Z(N6) );
  GTECH_NOT I_10 ( .A(N4), .Z(N19) );
  GTECH_AND2 C172 ( .A(frame_no_we_r), .B(N19), .Z(N20) );
  GTECH_AND2 C173 ( .A(frame_no_we), .B(N21), .Z(N22) );
  GTECH_NOT I_11 ( .A(rst), .Z(N23) );
  GTECH_AND2 C175 ( .A(frame_no_we_r), .B(N110), .Z(N24) );
  GTECH_NOT I_12 ( .A(frame_no_same), .Z(N110) );
  GTECH_OR2 C180 ( .A(N24), .B(N23), .Z(N25) );
  GTECH_OR2 C181 ( .A(frame_no_same), .B(N25), .Z(N26) );
  GTECH_NOT I_13 ( .A(N26), .Z(N27) );
  GTECH_BUF B_4 ( .A(N42), .Z(N28) );
  GTECH_NOT I_14 ( .A(N23), .Z(N38) );
  GTECH_AND2 C185 ( .A(N24), .B(N38), .Z(N39) );
  GTECH_NOT I_15 ( .A(N24), .Z(N40) );
  GTECH_AND2 C187 ( .A(N38), .B(N40), .Z(N41) );
  GTECH_AND2 C188 ( .A(frame_no_same), .B(N41), .Z(N42) );
  GTECH_AND2 C189 ( .A(N28), .B(N41) );
  GTECH_OR2 C192 ( .A(hms_clk), .B(clr_sof_time), .Z(N43) );
  GTECH_NOT I_16 ( .A(N43), .Z(N44) );
  GTECH_BUF B_5 ( .A(N72), .Z(N45) );
  GTECH_NOT I_17 ( .A(clr_sof_time), .Z(N71) );
  GTECH_AND2 C196 ( .A(hms_clk), .B(N71), .Z(N72) );
  GTECH_AND2 C197 ( .A(N45), .B(N71) );
  GTECH_NOT I_18 ( .A(rst), .Z(N73) );
  GTECH_OR2 C199 ( .A(hms_clk), .B(frame_no_we_r), .Z(N74) );
  GTECH_OR2 C202 ( .A(N74), .B(N73), .Z(N75) );
  GTECH_NOT I_19 ( .A(N75), .Z(N76) );
  GTECH_BUF B_6 ( .A(N76) );
  GTECH_NOT I_20 ( .A(N73), .Z(N87) );
  GTECH_AND2 C206 ( .A(N74), .B(N87), .Z(N88) );
endmodule


module usbf_mem_arb_SSRAM_HADR14 ( phy_clk, wclk, rst, sram_adr, sram_din, 
        sram_dout, sram_re, sram_we, madr, mdout, mdin, mwe, mreq, mack, wadr, 
        wdout, wdin, wwe, wreq, wack );
  output [14:0] sram_adr;
  input [31:0] sram_din;
  output [31:0] sram_dout;
  input [14:0] madr;
  output [31:0] mdout;
  input [31:0] mdin;
  input [14:0] wadr;
  output [31:0] wdout;
  input [31:0] wdin;
  input phy_clk, wclk, rst, mwe, mreq, wwe, wreq;
  output sram_re, sram_we, mack, wack;
  wire   N0, N1, N2, N3, mack, wsel, N4, N5, N6, wack_r, N7, N8, N9, N10, N11,
         N12, N13;
  assign sram_re = 1'b1;
  assign wdout[31] = mdout[31];
  assign mdout[31] = sram_din[31];
  assign wdout[30] = mdout[30];
  assign mdout[30] = sram_din[30];
  assign wdout[29] = mdout[29];
  assign mdout[29] = sram_din[29];
  assign wdout[28] = mdout[28];
  assign mdout[28] = sram_din[28];
  assign wdout[27] = mdout[27];
  assign mdout[27] = sram_din[27];
  assign wdout[26] = mdout[26];
  assign mdout[26] = sram_din[26];
  assign wdout[25] = mdout[25];
  assign mdout[25] = sram_din[25];
  assign wdout[24] = mdout[24];
  assign mdout[24] = sram_din[24];
  assign wdout[23] = mdout[23];
  assign mdout[23] = sram_din[23];
  assign wdout[22] = mdout[22];
  assign mdout[22] = sram_din[22];
  assign wdout[21] = mdout[21];
  assign mdout[21] = sram_din[21];
  assign wdout[20] = mdout[20];
  assign mdout[20] = sram_din[20];
  assign wdout[19] = mdout[19];
  assign mdout[19] = sram_din[19];
  assign wdout[18] = mdout[18];
  assign mdout[18] = sram_din[18];
  assign wdout[17] = mdout[17];
  assign mdout[17] = sram_din[17];
  assign wdout[16] = mdout[16];
  assign mdout[16] = sram_din[16];
  assign wdout[15] = mdout[15];
  assign mdout[15] = sram_din[15];
  assign wdout[14] = mdout[14];
  assign mdout[14] = sram_din[14];
  assign wdout[13] = mdout[13];
  assign mdout[13] = sram_din[13];
  assign wdout[12] = mdout[12];
  assign mdout[12] = sram_din[12];
  assign wdout[11] = mdout[11];
  assign mdout[11] = sram_din[11];
  assign wdout[10] = mdout[10];
  assign mdout[10] = sram_din[10];
  assign wdout[9] = mdout[9];
  assign mdout[9] = sram_din[9];
  assign wdout[8] = mdout[8];
  assign mdout[8] = sram_din[8];
  assign wdout[7] = mdout[7];
  assign mdout[7] = sram_din[7];
  assign wdout[6] = mdout[6];
  assign mdout[6] = sram_din[6];
  assign wdout[5] = mdout[5];
  assign mdout[5] = sram_din[5];
  assign wdout[4] = mdout[4];
  assign mdout[4] = sram_din[4];
  assign wdout[3] = mdout[3];
  assign mdout[3] = sram_din[3];
  assign wdout[2] = mdout[2];
  assign mdout[2] = sram_din[2];
  assign wdout[1] = mdout[1];
  assign mdout[1] = sram_din[1];
  assign wdout[0] = mdout[0];
  assign mdout[0] = sram_din[0];
  assign mack = mreq;

  \**SEQGEN**  wack_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(N9), 
        .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(wack_r), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  SELECT_OP C79 ( .DATA1(wdin), .DATA2(mdin), .CONTROL1(N0), .CONTROL2(N1), 
        .Z(sram_dout) );
  GTECH_BUF B_0 ( .A(wsel), .Z(N0) );
  GTECH_BUF B_1 ( .A(N4), .Z(N1) );
  SELECT_OP C80 ( .DATA1(wadr), .DATA2(madr), .CONTROL1(N0), .CONTROL2(N1), 
        .Z(sram_adr) );
  SELECT_OP C81 ( .DATA1(N5), .DATA2(N6), .CONTROL1(N0), .CONTROL2(N1), .Z(
        sram_we) );
  SELECT_OP C82 ( .DATA1(1'b0), .DATA2(N8), .CONTROL1(N2), .CONTROL2(N3), .Z(
        N9) );
  GTECH_BUF B_2 ( .A(N7), .Z(N2) );
  GTECH_BUF B_3 ( .A(rst), .Z(N3) );
  GTECH_AND2 C85 ( .A(N10), .B(N11), .Z(wsel) );
  GTECH_OR2 C86 ( .A(wreq), .B(wack), .Z(N10) );
  GTECH_NOT I_0 ( .A(mack), .Z(N11) );
  GTECH_NOT I_1 ( .A(wsel), .Z(N4) );
  GTECH_AND2 C97 ( .A(wreq), .B(wwe), .Z(N5) );
  GTECH_AND2 C98 ( .A(mwe), .B(mack), .Z(N6) );
  GTECH_AND2 C99 ( .A(wack_r), .B(N11), .Z(wack) );
  GTECH_NOT I_2 ( .A(rst), .Z(N7) );
  GTECH_AND2 C104 ( .A(N12), .B(N13), .Z(N8) );
  GTECH_AND2 C105 ( .A(wreq), .B(N11), .Z(N12) );
  GTECH_NOT I_3 ( .A(wack), .Z(N13) );
endmodule


module usbf_ep_rf ( clk, wclk, rst, adr, re, we, din, dout, inta, intb, 
        dma_req, dma_ack, idin, ep_sel, ep_match, buf0_rl, buf0_set, buf1_set, 
        uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, 
        int_crc16_set, int_to_set, int_seqerr_set, out_to_small, csr, buf0, 
        buf1, dma_in_buf_sz1, dma_out_buf_avail );
  input [1:0] adr;
  input [31:0] din;
  output [31:0] dout;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  input clk, wclk, rst, re, we, dma_ack, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small;
  output inta, intb, dma_req, ep_match, dma_in_buf_sz1, dma_out_buf_avail;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, int__29, int__28, int__27, int__26,
         int__25, int__24, int__21, int__20, int__19, int__18, int__17,
         int__16, N8, N9, N10, N11, N12, N13, we0, we1, we2, we3, N14, N15,
         N16, N17, N18, N19, N20, N21, N22, N23, N24, N25, N26, N27, N28, N29,
         N30, N31, N32, N33, N34, N35, N36, N37, N38, N39, N40, N41, N42, N43,
         N44, N45, N46, N47, N48, N49, N50, N51, N52, N53, N54, N55, N56, N57,
         N58, N59, N60, N61, N62, N63, N64, N65, N66, N67, ep_match_r, N68,
         N69, N70, N71, N72, N73, N74, N75, N76, N77, N78, N79, N80, N81, N82,
         N83, N84, N85, N86, N87, N88, N89, N90, N91, N92, N93, N94, N95, N96,
         N97, N98, N99, N100, N101, N102, N103, N104, N105, N106, N107, N108,
         N109, N110, N111, N112, N113, N114, N115, N116, N117, N118, N119,
         N120, N121, N122, N123, N124, N125, N126, N127, N128, N129, N130,
         N131, N132, N133, N134, N135, N136, N137, N138, N139, N140, N141,
         N142, N143, N144, N145, N146, N147, N148, N149, N150, N151, N152,
         N153, N154, N155, N156, N157, N158, N159, N160, N161, N162, N163,
         N164, N165, N166, N167, N168, N169, N170, N171, N172, N173, N174,
         N175, N176, N177, N178, N179, N180, N181, N182, N183, N184, N185,
         N186, N187, N188, N189, N190, N191, int_re, N192, N193, N194, N195,
         N196, N197, N198, N199, N200, N201, N202, N203, N204, N205, N206,
         N207, N208, N209, N210, N211, N212, N213, N214, N215, N216, N217,
         N218, N219, N220, N221, N222, N223, set_r, N224, N225, N226, N227,
         N228, N229, N230, N231, N232, N233, N234, N235, N236, N237, N238,
         N239, N240, N241, N242, N243, N244, N245, N246, N247, N248, N249,
         N250, N251, N252, N253, N254, N255, N256, N257, N258, N259, N260,
         N261, N262, N263, N264, N265, N266, N267, N268, N269, N270, N271,
         N272, dma_req_out_hold, N273, N274, N275, N276, N277, N278, N279,
         N280, N281, N282, N283, N284, N285, N286, N287, N288, N289, N290,
         N291, N292, N293, N294, N295, N296, N297, N298, N299, N300, N301,
         N302, N303, N304, N305, N306, N307, N308, N309, N310, N311, N312,
         N313, N314, N315, N316, N317, N318, N319, N320, N321, N322, N323,
         N324, N325, N326, N327, N328, N329, N330, N331, N332, N333,
         dma_req_in_d, dma_req_out_d, dma_req_d, N334, N335, N336, N337, N338,
         N339, N340, N341, N342, N343, N344, N345, N346, N347,
         dma_req_in_hold2, N348, dma_req_in_hold, dma_req_hold, N349, r1, r2,
         N350, N351, N352, N353, N354, N355, N356, N357, N358, N359, N360, r4,
         r5, N361, N362, N363, N364, N365, N366, N367, N368, N369, N370,
         dma_ack_clr1, N371, N372, N373, dma_ack_wr1, N374, N375, N376, N377,
         N378, N379, N380, N381, N382, N383, N384, N385, N386, N387, N388,
         N389, N390, N391, N392, N393, N394, N395, N396, N397, N398, N399,
         N400, N401, N402, N403, N404, N405, N406, N407, N408, N409, N410,
         N411, N412, N413, N414, N415, N416, N417, N418, N419, N420, N421,
         N422, N423, N424, N425, N426, N427, N428, N429, N430, N431, N432,
         N433, N434, N435, N436, N437, N438, N439, N440, N441, N442, N443,
         N444, N445, N446, N447, N448, N449, N450, N451, N452, N453, N454,
         N455, N456, N457, N458, N459, N460, N461, N462, N463, N464, N465,
         N466, N467, N468, N469, N470;
  wire   [6:0] int_;
  wire   [31:0] buf0_orig;
  wire   [11:0] dma_out_cnt;
  wire   [11:0] dma_in_cnt;
  wire   [11:0] dma_out_left;
  wire   [11:0] buf0_orig_m3;
  assign csr[14] = 1'b0;

  GTECH_AND2 C6 ( .A(N410), .B(N380), .Z(N8) );
  GTECH_OR2 C8 ( .A(adr[1]), .B(N380), .Z(N9) );
  GTECH_OR2 C11 ( .A(N410), .B(adr[0]), .Z(N11) );
  GTECH_AND2 C13 ( .A(adr[1]), .B(adr[0]), .Z(N13) );
  \**SEQGEN**  ots_stop_reg ( .clear(1'b0), .preset(1'b0), .next_state(N46), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[13]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr0_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(N31), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[12]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr0_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(N30), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[11]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr0_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(N29), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[10]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr0_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(N28), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr0_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(N27), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr0_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N26), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr0_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N25), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr0_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N24), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr0_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N23), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr0_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N22), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr0_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N21), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr0_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N20), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr0_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N19), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr1_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(N45), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[27]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr1_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(N44), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[26]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr1_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(N43), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[25]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr1_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(N42), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[24]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr1_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(N41), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[23]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N39) );
  \**SEQGEN**  \csr1_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N40), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[22]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N39) );
  \**SEQGEN**  \csr1_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N38), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[21]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr1_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N37), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[20]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr1_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N36), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[19]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr1_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N35), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[18]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr1_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N34), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[17]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr1_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N33), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[16]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \csr1_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N32), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[15]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N18) );
  \**SEQGEN**  \iena_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N66), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int__29), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N54) );
  \**SEQGEN**  \iena_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N65), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int__28), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N54) );
  \**SEQGEN**  \iena_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N64), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int__27), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N54) );
  \**SEQGEN**  \iena_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N63), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int__26), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N54) );
  \**SEQGEN**  \iena_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N62), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int__25), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N54) );
  \**SEQGEN**  \iena_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N61), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int__24), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N54) );
  \**SEQGEN**  \ienb_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N60), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int__21), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N54) );
  \**SEQGEN**  \ienb_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N59), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int__20), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N54) );
  \**SEQGEN**  \ienb_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N58), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int__19), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N54) );
  \**SEQGEN**  \ienb_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N57), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int__18), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N54) );
  \**SEQGEN**  \ienb_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N56), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int__17), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N54) );
  \**SEQGEN**  \ienb_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N55), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int__16), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N54) );
  \**SEQGEN**  \buf0_reg[31]  ( .clear(1'b0), .preset(1'b0), .next_state(N106), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[31]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[30]  ( .clear(1'b0), .preset(1'b0), .next_state(N105), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[30]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[29]  ( .clear(1'b0), .preset(1'b0), .next_state(N104), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[29]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[28]  ( .clear(1'b0), .preset(1'b0), .next_state(N103), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[28]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[27]  ( .clear(1'b0), .preset(1'b0), .next_state(N102), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[27]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[26]  ( .clear(1'b0), .preset(1'b0), .next_state(N101), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[26]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[25]  ( .clear(1'b0), .preset(1'b0), .next_state(N100), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[25]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[24]  ( .clear(1'b0), .preset(1'b0), .next_state(N99), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[24]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[23]  ( .clear(1'b0), .preset(1'b0), .next_state(N98), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[23]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[22]  ( .clear(1'b0), .preset(1'b0), .next_state(N97), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[22]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[21]  ( .clear(1'b0), .preset(1'b0), .next_state(N96), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[21]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[20]  ( .clear(1'b0), .preset(1'b0), .next_state(N95), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[20]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[19]  ( .clear(1'b0), .preset(1'b0), .next_state(N94), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[19]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[18]  ( .clear(1'b0), .preset(1'b0), .next_state(N93), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[18]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[17]  ( .clear(1'b0), .preset(1'b0), .next_state(N92), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[17]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[16]  ( .clear(1'b0), .preset(1'b0), .next_state(N91), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[16]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(N90), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[15]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(N89), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[14]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(N88), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[13]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(N87), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[12]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(N86), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[11]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(N85), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[10]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(N84), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(N83), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N82), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N81), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N80), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N79), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N78), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N77), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N76), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf0_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N75), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N74) );
  \**SEQGEN**  \buf1_reg[31]  ( .clear(1'b0), .preset(1'b0), .next_state(N150), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[31]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[30]  ( .clear(1'b0), .preset(1'b0), .next_state(N149), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[30]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[29]  ( .clear(1'b0), .preset(1'b0), .next_state(N148), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[29]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[28]  ( .clear(1'b0), .preset(1'b0), .next_state(N147), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[28]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[27]  ( .clear(1'b0), .preset(1'b0), .next_state(N146), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[27]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[26]  ( .clear(1'b0), .preset(1'b0), .next_state(N145), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[26]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[25]  ( .clear(1'b0), .preset(1'b0), .next_state(N144), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[25]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[24]  ( .clear(1'b0), .preset(1'b0), .next_state(N143), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[24]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[23]  ( .clear(1'b0), .preset(1'b0), .next_state(N142), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[23]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[22]  ( .clear(1'b0), .preset(1'b0), .next_state(N141), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[22]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[21]  ( .clear(1'b0), .preset(1'b0), .next_state(N140), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[21]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[20]  ( .clear(1'b0), .preset(1'b0), .next_state(N139), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[20]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[19]  ( .clear(1'b0), .preset(1'b0), .next_state(N138), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[19]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[18]  ( .clear(1'b0), .preset(1'b0), .next_state(N137), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[18]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[17]  ( .clear(1'b0), .preset(1'b0), .next_state(N136), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[17]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[16]  ( .clear(1'b0), .preset(1'b0), .next_state(N135), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[16]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(N134), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[15]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(N133), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[14]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(N132), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[13]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(N131), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[12]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(N130), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[11]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(N129), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[10]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(N128), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(N127), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N126), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N125), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N124), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N123), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N122), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N121), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N120), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf1_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N119), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N118) );
  \**SEQGEN**  \buf0_orig_reg[31]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N189), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[31]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[30]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N188), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[30]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[29]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N187), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[29]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[28]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N186), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[28]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[27]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N185), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[27]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[26]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N184), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[26]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[25]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N183), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[25]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[24]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N182), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[24]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[23]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N181), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[23]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[22]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N180), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[22]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[21]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N179), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[21]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[20]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N178), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[20]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[19]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N177), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[19]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[18]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N176), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[18]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[17]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N175), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[17]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[16]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N174), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[16]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N173), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[15]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N172), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[14]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N171), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[13]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N170), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[12]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N169), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[11]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N168), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[10]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N167), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[9]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N166), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[8]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N165), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[7]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N164), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[6]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N163), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[5]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N162), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[4]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N161), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N160), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N159), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N157) );
  \**SEQGEN**  \buf0_orig_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N158), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        buf0_orig[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N157) );
  EQ_UNS_OP eq_7084 ( .A(ep_sel), .B(csr[21:18]), .Z(ep_match) );
  \**SEQGEN**  ep_match_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        ep_match), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        ep_match_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  int_re_reg ( .clear(1'b0), .preset(1'b0), .next_state(N191), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int_re), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \int_stat_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N196), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int_[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N202) );
  \**SEQGEN**  \int_stat_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N196), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int_[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N201) );
  \**SEQGEN**  \int_stat_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N196), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int_[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N200) );
  \**SEQGEN**  \int_stat_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N196), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int_[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N199) );
  \**SEQGEN**  \int_stat_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N196), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int_[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N198) );
  \**SEQGEN**  \int_stat_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N196), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int_[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N197) );
  \**SEQGEN**  \int_stat_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N196), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(int_[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N195) );
  \**SEQGEN**  \uc_dpd_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N212), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[29]), .synch_clear(
        1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(N210)
         );
  \**SEQGEN**  \uc_dpd_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N211), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[28]), .synch_clear(
        1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(N210)
         );
  \**SEQGEN**  \uc_bsel_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N219), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[31]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N217) );
  \**SEQGEN**  \uc_bsel_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N218), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[30]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N217) );
  \**SEQGEN**  inta_reg ( .clear(1'b0), .preset(1'b0), .next_state(N221), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(inta), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  intb_reg ( .clear(1'b0), .preset(1'b0), .next_state(N222), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(intb), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \dma_out_cnt_reg[11]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N266), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_cnt[11]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N254) );
  \**SEQGEN**  \dma_out_cnt_reg[10]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N265), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_cnt[10]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N254) );
  \**SEQGEN**  \dma_out_cnt_reg[9]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N264), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_cnt[9]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N254) );
  \**SEQGEN**  \dma_out_cnt_reg[8]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N263), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_cnt[8]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N254) );
  \**SEQGEN**  \dma_out_cnt_reg[7]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N262), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_cnt[7]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N254) );
  \**SEQGEN**  \dma_out_cnt_reg[6]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N261), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_cnt[6]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N254) );
  \**SEQGEN**  \dma_out_cnt_reg[5]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N260), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_cnt[5]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N254) );
  \**SEQGEN**  \dma_out_cnt_reg[4]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N259), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_cnt[4]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N254) );
  \**SEQGEN**  \dma_out_cnt_reg[3]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N258), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_cnt[3]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N254) );
  \**SEQGEN**  \dma_out_cnt_reg[2]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N257), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_cnt[2]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N254) );
  \**SEQGEN**  \dma_out_cnt_reg[1]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N256), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_cnt[1]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N254) );
  \**SEQGEN**  \dma_out_cnt_reg[0]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N255), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_cnt[0]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N254) );
  \**SEQGEN**  set_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(N271), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(set_r), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  dma_req_out_hold_reg ( .clear(1'b0), .preset(1'b0), 
        .next_state(N272), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_req_out_hold), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \dma_in_cnt_reg[11]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N315), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_in_cnt[11]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N303) );
  \**SEQGEN**  \dma_in_cnt_reg[10]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N314), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_in_cnt[10]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N303) );
  \**SEQGEN**  \dma_in_cnt_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N313), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        dma_in_cnt[9]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N303) );
  \**SEQGEN**  \dma_in_cnt_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N312), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        dma_in_cnt[8]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N303) );
  \**SEQGEN**  \dma_in_cnt_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N311), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        dma_in_cnt[7]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N303) );
  \**SEQGEN**  \dma_in_cnt_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N310), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        dma_in_cnt[6]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N303) );
  \**SEQGEN**  \dma_in_cnt_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N309), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        dma_in_cnt[5]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N303) );
  \**SEQGEN**  \dma_in_cnt_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N308), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        dma_in_cnt[4]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N303) );
  \**SEQGEN**  \dma_in_cnt_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N307), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        dma_in_cnt[3]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N303) );
  \**SEQGEN**  \dma_in_cnt_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N306), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        dma_in_cnt[2]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N303) );
  \**SEQGEN**  \dma_in_cnt_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N305), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        dma_in_cnt[1]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N303) );
  \**SEQGEN**  \dma_in_cnt_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N304), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        dma_in_cnt[0]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N303) );
  GEQ_UNS_OP gte_7198 ( .A(dma_in_cnt), .B(csr[10:2]), .Z(N319) );
  \**SEQGEN**  dma_in_buf_sz1_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N320), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        dma_in_buf_sz1), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \dma_out_left_reg[11]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N332), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_left[11]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \dma_out_left_reg[10]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N331), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_left[10]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \dma_out_left_reg[9]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N330), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_left[9]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \dma_out_left_reg[8]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N329), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_left[8]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \dma_out_left_reg[7]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N328), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_left[7]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \dma_out_left_reg[6]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N327), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_left[6]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \dma_out_left_reg[5]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N326), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_left[5]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \dma_out_left_reg[4]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N325), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_left[4]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \dma_out_left_reg[3]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N324), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_left[3]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \dma_out_left_reg[2]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N323), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_left[2]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \dma_out_left_reg[1]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N322), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_left[1]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \dma_out_left_reg[0]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N321), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_left[0]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  GEQ_UNS_OP gte_7207 ( .A(dma_out_left), .B(csr[10:2]), .Z(N333) );
  \**SEQGEN**  dma_out_buf_avail_reg ( .clear(1'b0), .preset(1'b0), 
        .next_state(N333), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_buf_avail), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  LT_UNS_OP lt_7216 ( .A(dma_in_cnt), .B(buf0_orig[30:19]), .Z(N334) );
  \**SEQGEN**  \buf0_orig_m3_reg[11]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N346), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), 
        .Q(buf0_orig_m3[11]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \buf0_orig_m3_reg[10]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N345), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), 
        .Q(buf0_orig_m3[10]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \buf0_orig_m3_reg[9]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N344), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), 
        .Q(buf0_orig_m3[9]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \buf0_orig_m3_reg[8]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N343), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), 
        .Q(buf0_orig_m3[8]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \buf0_orig_m3_reg[7]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N342), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), 
        .Q(buf0_orig_m3[7]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \buf0_orig_m3_reg[6]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N341), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), 
        .Q(buf0_orig_m3[6]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \buf0_orig_m3_reg[5]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N340), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), 
        .Q(buf0_orig_m3[5]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \buf0_orig_m3_reg[4]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N339), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), 
        .Q(buf0_orig_m3[4]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \buf0_orig_m3_reg[3]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N338), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), 
        .Q(buf0_orig_m3[3]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \buf0_orig_m3_reg[2]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N337), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), 
        .Q(buf0_orig_m3[2]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \buf0_orig_m3_reg[1]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N336), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), 
        .Q(buf0_orig_m3[1]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \buf0_orig_m3_reg[0]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(N335), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), 
        .Q(buf0_orig_m3[0]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  LT_UNS_OP lt_7225 ( .A(dma_in_cnt), .B(buf0_orig_m3), .Z(N347) );
  \**SEQGEN**  dma_req_in_hold2_reg ( .clear(1'b0), .preset(1'b0), 
        .next_state(N347), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_req_in_hold2), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  dma_req_in_hold_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N348), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        dma_req_in_hold), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  dma_req_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(N356), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dma_req), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N355) );
  \**SEQGEN**  r1_reg ( .clear(1'b0), .preset(1'b0), .next_state(N361), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(r1), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  r2_reg ( .clear(1'b0), .preset(1'b0), .next_state(N366), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(r2), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N365) );
  \**SEQGEN**  dma_ack_wr1_reg ( .clear(1'b0), .preset(1'b0), .next_state(N375), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dma_ack_wr1), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N374) );
  \**SEQGEN**  dma_ack_clr1_reg ( .clear(1'b0), .preset(1'b0), .next_state(r4), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dma_ack_clr1), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  r4_reg ( .clear(1'b0), .preset(1'b0), .next_state(dma_ack_wr1), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(r4), .synch_clear(
        1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1)
         );
  \**SEQGEN**  r5_reg ( .clear(1'b0), .preset(1'b0), .next_state(r4), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(r5), .synch_clear(
        1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1)
         );
  GTECH_NOT I_0 ( .A(adr[0]), .Z(N380) );
  GTECH_OR2 C716 ( .A(N380), .B(adr[1]), .Z(N381) );
  GTECH_NOT I_1 ( .A(N381), .Z(N382) );
  GTECH_OR2 C718 ( .A(csr[9]), .B(csr[10]), .Z(N383) );
  GTECH_OR2 C719 ( .A(csr[8]), .B(N383), .Z(N384) );
  GTECH_OR2 C720 ( .A(csr[7]), .B(N384), .Z(N385) );
  GTECH_OR2 C721 ( .A(csr[6]), .B(N385), .Z(N386) );
  GTECH_OR2 C722 ( .A(csr[5]), .B(N386), .Z(N387) );
  GTECH_OR2 C723 ( .A(csr[4]), .B(N387), .Z(N388) );
  GTECH_OR2 C724 ( .A(csr[3]), .B(N388), .Z(N389) );
  GTECH_OR2 C725 ( .A(csr[2]), .B(N389), .Z(N390) );
  GTECH_OR2 C726 ( .A(csr[1]), .B(N390), .Z(N391) );
  GTECH_OR2 C727 ( .A(csr[0]), .B(N391), .Z(N392) );
  GTECH_NOT I_2 ( .A(csr[26]), .Z(N393) );
  GTECH_OR2 C731 ( .A(N393), .B(csr[27]), .Z(N394) );
  GTECH_NOT I_3 ( .A(N394), .Z(N395) );
  GTECH_OR2 C733 ( .A(dma_out_cnt[10]), .B(dma_out_cnt[11]), .Z(N396) );
  GTECH_OR2 C734 ( .A(dma_out_cnt[9]), .B(N396), .Z(N397) );
  GTECH_OR2 C735 ( .A(dma_out_cnt[8]), .B(N397), .Z(N398) );
  GTECH_OR2 C736 ( .A(dma_out_cnt[7]), .B(N398), .Z(N399) );
  GTECH_OR2 C737 ( .A(dma_out_cnt[6]), .B(N399), .Z(N400) );
  GTECH_OR2 C738 ( .A(dma_out_cnt[5]), .B(N400), .Z(N401) );
  GTECH_OR2 C739 ( .A(dma_out_cnt[4]), .B(N401), .Z(N402) );
  GTECH_OR2 C740 ( .A(dma_out_cnt[3]), .B(N402), .Z(N403) );
  GTECH_OR2 C741 ( .A(dma_out_cnt[2]), .B(N403), .Z(N404) );
  GTECH_OR2 C742 ( .A(dma_out_cnt[1]), .B(N404), .Z(N405) );
  GTECH_OR2 C743 ( .A(dma_out_cnt[0]), .B(N405), .Z(N406) );
  GTECH_OR2 C748 ( .A(adr[0]), .B(adr[1]), .Z(N407) );
  GTECH_NOT I_4 ( .A(N407), .Z(N408) );
  GTECH_AND2 C750 ( .A(adr[0]), .B(adr[1]), .Z(N409) );
  GTECH_NOT I_5 ( .A(adr[1]), .Z(N410) );
  GTECH_OR2 C752 ( .A(adr[0]), .B(N410), .Z(N411) );
  GTECH_NOT I_6 ( .A(N411), .Z(N412) );
  GTECH_NOT I_7 ( .A(csr[27]), .Z(N413) );
  GTECH_OR2 C755 ( .A(csr[26]), .B(N413), .Z(N414) );
  GTECH_NOT I_8 ( .A(N414), .Z(N415) );
  SUB_UNS_OP sub_7204 ( .A(buf0_orig[30:19]), .B(dma_out_cnt), .Z({N332, N331, 
        N330, N329, N328, N327, N326, N325, N324, N323, N322, N321}) );
  SUB_UNS_OP sub_7220 ( .A(buf0_orig[30:19]), .B({1'b1, 1'b1}), .Z({N346, N345, 
        N344, N343, N342, N341, N340, N339, N338, N337, N336, N335}) );
  SUB_UNS_OP sub_7165_S2 ( .A(dma_out_cnt), .B(1'b1), .Z({N240, N239, N238, 
        N237, N236, N235, N234, N233, N232, N231, N230, N229}) );
  ADD_UNS_OP add_7190_S2 ( .A(dma_in_cnt), .B(1'b1), .Z({N289, N288, N287, 
        N286, N285, N284, N283, N282, N281, N280, N279, N278}) );
  ADD_UNS_OP add_7168 ( .A(dma_out_cnt), .B(csr[10:2]), .Z({N253, N252, N251, 
        N250, N249, N248, N247, N246, N245, N244, N243, N242}) );
  SUB_UNS_OP sub_7193 ( .A(dma_in_cnt), .B(csr[10:2]), .Z({N302, N301, N300, 
        N299, N298, N297, N296, N295, N294, N293, N292, N291}) );
  SELECT_OP C757 ( .DATA1({csr[31:15], 1'b0, csr[13:0]}), .DATA2({1'b0, 1'b0, 
        int__29, int__28, int__27, int__26, int__25, int__24, 1'b0, 1'b0, 
        int__21, int__20, int__19, int__18, int__17, int__16, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, int_}), .DATA3(buf0), .DATA4(buf1), 
        .CONTROL1(N0), .CONTROL2(N1), .CONTROL3(N2), .CONTROL4(N3), .Z(dout)
         );
  GTECH_BUF B_0 ( .A(N8), .Z(N0) );
  GTECH_BUF B_1 ( .A(N10), .Z(N1) );
  GTECH_BUF B_2 ( .A(N12), .Z(N2) );
  GTECH_BUF B_3 ( .A(N13), .Z(N3) );
  SELECT_OP C758 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .DATA4(1'b0), 
        .CONTROL1(N4), .CONTROL2(N47), .CONTROL3(N50), .CONTROL4(N17), .Z(N18)
         );
  GTECH_BUF B_4 ( .A(N51), .Z(N4) );
  SELECT_OP C759 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), .DATA2(din[12:0]), .CONTROL1(N4), 
        .CONTROL2(N47), .Z({N31, N30, N29, N28, N27, N26, N25, N24, N23, N22, 
        N21, N20, N19}) );
  SELECT_OP C760 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N4), .CONTROL2(N47), .CONTROL3(N50), .CONTROL4(N17), .Z(N39)
         );
  SELECT_OP C761 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0}), .DATA2({din[27:24], din[21:15]}), .CONTROL1(N4), 
        .CONTROL2(N47), .Z({N45, N44, N43, N42, N38, N37, N36, N35, N34, N33, 
        N32}) );
  SELECT_OP C762 ( .DATA1({1'b0, 1'b0}), .DATA2(din[23:22]), .DATA3({1'b0, 
        1'b1}), .CONTROL1(N4), .CONTROL2(N47), .CONTROL3(N50), .Z({N41, N40})
         );
  SELECT_OP C763 ( .DATA1(1'b0), .DATA2(din[13]), .CONTROL1(N4), .CONTROL2(N47), .Z(N46) );
  SELECT_OP C764 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N4), 
        .CONTROL2(N67), .CONTROL3(N53), .Z(N54) );
  SELECT_OP C765 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), .DATA2(
        din[21:16]), .CONTROL1(N4), .CONTROL2(N67), .Z({N60, N59, N58, N57, 
        N56, N55}) );
  SELECT_OP C766 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), .DATA2(
        din[29:24]), .CONTROL1(N4), .CONTROL2(N67), .Z({N66, N65, N64, N63, 
        N62, N61}) );
  SELECT_OP C767 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b1), 
        .DATA5(1'b0), .CONTROL1(N4), .CONTROL2(N107), .CONTROL3(N110), 
        .CONTROL4(N113), .CONTROL5(N73), .Z(N74) );
  SELECT_OP C768 ( .DATA1({1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 
        1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 
        1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1}), .DATA2(din), .DATA3(buf0_orig), .DATA4(idin), .CONTROL1(N4), .CONTROL2(N107), .CONTROL3(N110), .CONTROL4(N113), .Z({N106, N105, N104, N103, N102, N101, 
        N100, N99, N98, N97, N96, N95, N94, N93, N92, N91, N90, N89, N88, N87, 
        N86, N85, N84, N83, N82, N81, N80, N79, N78, N77, N76, N75}) );
  SELECT_OP C769 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N4), .CONTROL2(N151), .CONTROL3(N154), .CONTROL4(N117), .Z(
        N118) );
  SELECT_OP C770 ( .DATA1({1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 
        1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 
        1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1}), .DATA2(din), .DATA3(idin), .CONTROL1(N4), .CONTROL2(N151), .CONTROL3(N154), 
        .Z({N150, N149, N148, N147, N146, N145, N144, N143, N142, N141, N140, 
        N139, N138, N137, N136, N135, N134, N133, N132, N131, N130, N129, N128, 
        N127, N126, N125, N124, N123, N122, N121, N120, N119}) );
  SELECT_OP C771 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N4), 
        .CONTROL2(N190), .CONTROL3(N156), .Z(N157) );
  SELECT_OP C772 ( .DATA1({1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 
        1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 
        1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1}), .DATA2(din), .CONTROL1(N4), .CONTROL2(N190), .Z({N189, N188, N187, N186, 
        N185, N184, N183, N182, N181, N180, N179, N178, N177, N176, N175, N174, 
        N173, N172, N171, N170, N169, N168, N167, N166, N165, N164, N163, N162, 
        N161, N160, N159, N158}) );
  SELECT_OP C773 ( .DATA1({1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1}), .DATA2(
        {1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1, 1'b1}), .DATA3({out_to_small, 
        int_seqerr_set, int_buf1_set, int_buf0_set, int_upid_set, 
        int_crc16_set, int_to_set}), .DATA4({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0}), .CONTROL1(N4), .CONTROL2(N203), .CONTROL3(N206), 
        .CONTROL4(N194), .Z({N202, N201, N200, N199, N198, N197, N195}) );
  SELECT_OP C774 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b1), .CONTROL1(N4), 
        .CONTROL2(N203), .CONTROL3(N206), .Z(N196) );
  SELECT_OP C775 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N4), 
        .CONTROL2(N213), .CONTROL3(N209), .Z(N210) );
  SELECT_OP C776 ( .DATA1({1'b0, 1'b0}), .DATA2(idin[3:2]), .CONTROL1(N4), 
        .CONTROL2(N213), .Z({N212, N211}) );
  SELECT_OP C777 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N4), 
        .CONTROL2(N220), .CONTROL3(N216), .Z(N217) );
  SELECT_OP C778 ( .DATA1({1'b0, 1'b0}), .DATA2(idin[1:0]), .CONTROL1(N4), 
        .CONTROL2(N220), .Z({N219, N218}) );
  SELECT_OP C779 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N5), .CONTROL2(N267), .CONTROL3(N270), .CONTROL4(N227), .Z(
        N254) );
  GTECH_BUF B_5 ( .A(N223), .Z(N5) );
  SELECT_OP C780 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0}), .DATA2({N240, N239, N238, N237, N236, N235, 
        N234, N233, N232, N231, N230, N229}), .DATA3({N253, N252, N251, N250, 
        N249, N248, N247, N246, N245, N244, N243, N242}), .CONTROL1(N5), 
        .CONTROL2(N267), .CONTROL3(N270), .Z({N266, N265, N264, N263, N262, 
        N261, N260, N259, N258, N257, N256, N255}) );
  SELECT_OP C781 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N5), .CONTROL2(N316), .CONTROL3(N318), .CONTROL4(N276), .Z(
        N303) );
  SELECT_OP C782 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0}), .DATA2({N289, N288, N287, N286, N285, N284, 
        N283, N282, N281, N280, N279, N278}), .DATA3({N302, N301, N300, N299, 
        N298, N297, N296, N295, N294, N293, N292, N291}), .CONTROL1(N5), 
        .CONTROL2(N316), .CONTROL3(N318), .Z({N315, N314, N313, N312, N311, 
        N310, N309, N308, N307, N306, N305, N304}) );
  SELECT_OP C783 ( .DATA1(dma_req_out_hold), .DATA2(N349), .CONTROL1(N6), 
        .CONTROL2(N7), .Z(dma_req_hold) );
  GTECH_BUF B_6 ( .A(N415), .Z(N6) );
  GTECH_BUF B_7 ( .A(N414), .Z(N7) );
  SELECT_OP C784 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N4), .CONTROL2(N357), .CONTROL3(N360), .CONTROL4(N354), .Z(
        N355) );
  SELECT_OP C785 ( .DATA1(1'b0), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N4), 
        .CONTROL2(N357), .CONTROL3(N360), .Z(N356) );
  SELECT_OP C786 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N4), .CONTROL2(N367), .CONTROL3(N370), .CONTROL4(N364), .Z(
        N365) );
  SELECT_OP C787 ( .DATA1(1'b0), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N4), 
        .CONTROL2(N367), .CONTROL3(N370), .Z(N366) );
  SELECT_OP C788 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N4), .CONTROL2(N376), .CONTROL3(N379), .CONTROL4(N373), .Z(
        N374) );
  SELECT_OP C789 ( .DATA1(1'b0), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N4), 
        .CONTROL2(N376), .CONTROL3(N379), .Z(N375) );
  GTECH_NOT I_9 ( .A(N9), .Z(N10) );
  GTECH_NOT I_10 ( .A(N11), .Z(N12) );
  GTECH_AND2 C800 ( .A(N408), .B(we), .Z(we0) );
  GTECH_AND2 C801 ( .A(N382), .B(we), .Z(we1) );
  GTECH_AND2 C802 ( .A(N412), .B(we), .Z(we2) );
  GTECH_AND2 C803 ( .A(N409), .B(we), .Z(we3) );
  GTECH_AND2 C804 ( .A(csr[13]), .B(out_to_small), .Z(N14) );
  GTECH_OR2 C807 ( .A(we0), .B(N51), .Z(N15) );
  GTECH_OR2 C808 ( .A(N14), .B(N15), .Z(N16) );
  GTECH_NOT I_11 ( .A(N16), .Z(N17) );
  GTECH_AND2 C810 ( .A(we0), .B(rst), .Z(N47) );
  GTECH_NOT I_12 ( .A(we0), .Z(N48) );
  GTECH_AND2 C812 ( .A(rst), .B(N48), .Z(N49) );
  GTECH_AND2 C813 ( .A(N14), .B(N49), .Z(N50) );
  GTECH_NOT I_13 ( .A(rst), .Z(N51) );
  GTECH_OR2 C817 ( .A(we1), .B(N51), .Z(N52) );
  GTECH_NOT I_14 ( .A(N52), .Z(N53) );
  GTECH_AND2 C820 ( .A(we1), .B(rst), .Z(N67) );
  GTECH_AND2 C821 ( .A(ep_match_r), .B(buf0_rl), .Z(N68) );
  GTECH_AND2 C822 ( .A(ep_match_r), .B(buf0_set), .Z(N69) );
  GTECH_OR2 C826 ( .A(we2), .B(N51), .Z(N70) );
  GTECH_OR2 C827 ( .A(N68), .B(N70), .Z(N71) );
  GTECH_OR2 C828 ( .A(N69), .B(N71), .Z(N72) );
  GTECH_NOT I_15 ( .A(N72), .Z(N73) );
  GTECH_AND2 C830 ( .A(we2), .B(rst), .Z(N107) );
  GTECH_NOT I_16 ( .A(we2), .Z(N108) );
  GTECH_AND2 C832 ( .A(rst), .B(N108), .Z(N109) );
  GTECH_AND2 C833 ( .A(N68), .B(N109), .Z(N110) );
  GTECH_NOT I_17 ( .A(N68), .Z(N111) );
  GTECH_AND2 C835 ( .A(N109), .B(N111), .Z(N112) );
  GTECH_AND2 C836 ( .A(N69), .B(N112), .Z(N113) );
  GTECH_AND2 C837 ( .A(ep_match_r), .B(N416), .Z(N114) );
  GTECH_OR2 C838 ( .A(buf1_set), .B(out_to_small), .Z(N416) );
  GTECH_OR2 C842 ( .A(we3), .B(N51), .Z(N115) );
  GTECH_OR2 C843 ( .A(N114), .B(N115), .Z(N116) );
  GTECH_NOT I_18 ( .A(N116), .Z(N117) );
  GTECH_AND2 C846 ( .A(we3), .B(rst), .Z(N151) );
  GTECH_NOT I_19 ( .A(we3), .Z(N152) );
  GTECH_AND2 C848 ( .A(rst), .B(N152), .Z(N153) );
  GTECH_AND2 C849 ( .A(N114), .B(N153), .Z(N154) );
  GTECH_OR2 C852 ( .A(we2), .B(N51), .Z(N155) );
  GTECH_NOT I_20 ( .A(N155), .Z(N156) );
  GTECH_AND2 C855 ( .A(we2), .B(rst), .Z(N190) );
  GTECH_AND2 C856 ( .A(re), .B(N382), .Z(N191) );
  GTECH_OR2 C860 ( .A(int_re), .B(N51), .Z(N192) );
  GTECH_OR2 C861 ( .A(ep_match_r), .B(N192), .Z(N193) );
  GTECH_NOT I_21 ( .A(N193), .Z(N194) );
  GTECH_AND2 C871 ( .A(int_re), .B(rst), .Z(N203) );
  GTECH_NOT I_22 ( .A(int_re), .Z(N204) );
  GTECH_AND2 C873 ( .A(rst), .B(N204), .Z(N205) );
  GTECH_AND2 C874 ( .A(ep_match_r), .B(N205), .Z(N206) );
  GTECH_AND2 C875 ( .A(ep_match_r), .B(uc_dpd_set), .Z(N207) );
  GTECH_OR2 C878 ( .A(N207), .B(N51), .Z(N208) );
  GTECH_NOT I_23 ( .A(N208), .Z(N209) );
  GTECH_AND2 C881 ( .A(N207), .B(rst), .Z(N213) );
  GTECH_AND2 C882 ( .A(ep_match_r), .B(uc_bsel_set), .Z(N214) );
  GTECH_OR2 C885 ( .A(N214), .B(N51), .Z(N215) );
  GTECH_NOT I_24 ( .A(N215), .Z(N216) );
  GTECH_AND2 C888 ( .A(N214), .B(rst), .Z(N220) );
  GTECH_OR2 C889 ( .A(N427), .B(N428), .Z(N221) );
  GTECH_OR2 C890 ( .A(N425), .B(N426), .Z(N427) );
  GTECH_OR2 C891 ( .A(N423), .B(N424), .Z(N425) );
  GTECH_OR2 C892 ( .A(N421), .B(N422), .Z(N423) );
  GTECH_OR2 C893 ( .A(N419), .B(N420), .Z(N421) );
  GTECH_OR2 C894 ( .A(N417), .B(N418), .Z(N419) );
  GTECH_AND2 C895 ( .A(int_[0]), .B(int__24), .Z(N417) );
  GTECH_AND2 C896 ( .A(int_[1]), .B(int__25), .Z(N418) );
  GTECH_AND2 C897 ( .A(int_[2]), .B(int__26), .Z(N420) );
  GTECH_AND2 C898 ( .A(int_[3]), .B(int__27), .Z(N422) );
  GTECH_AND2 C899 ( .A(int_[4]), .B(int__27), .Z(N424) );
  GTECH_AND2 C900 ( .A(int_[5]), .B(int__28), .Z(N426) );
  GTECH_AND2 C901 ( .A(int_[6]), .B(int__29), .Z(N428) );
  GTECH_OR2 C902 ( .A(N439), .B(N440), .Z(N222) );
  GTECH_OR2 C903 ( .A(N437), .B(N438), .Z(N439) );
  GTECH_OR2 C904 ( .A(N435), .B(N436), .Z(N437) );
  GTECH_OR2 C905 ( .A(N433), .B(N434), .Z(N435) );
  GTECH_OR2 C906 ( .A(N431), .B(N432), .Z(N433) );
  GTECH_OR2 C907 ( .A(N429), .B(N430), .Z(N431) );
  GTECH_AND2 C908 ( .A(int_[0]), .B(int__16), .Z(N429) );
  GTECH_AND2 C909 ( .A(int_[1]), .B(int__17), .Z(N430) );
  GTECH_AND2 C910 ( .A(int_[2]), .B(int__18), .Z(N432) );
  GTECH_AND2 C911 ( .A(int_[3]), .B(int__19), .Z(N434) );
  GTECH_AND2 C912 ( .A(int_[4]), .B(int__19), .Z(N436) );
  GTECH_AND2 C913 ( .A(int_[5]), .B(int__20), .Z(N438) );
  GTECH_AND2 C914 ( .A(int_[6]), .B(int__21), .Z(N440) );
  GTECH_NOT I_25 ( .A(csr[15]), .Z(N223) );
  GTECH_AND2 C916 ( .A(ep_match_r), .B(N442), .Z(N224) );
  GTECH_OR2 C917 ( .A(N441), .B(buf0_rl), .Z(N442) );
  GTECH_OR2 C918 ( .A(set_r), .B(buf0_set), .Z(N441) );
  GTECH_OR2 C922 ( .A(r5), .B(N223), .Z(N225) );
  GTECH_OR2 C923 ( .A(N224), .B(N225), .Z(N226) );
  GTECH_NOT I_26 ( .A(N226), .Z(N227) );
  GTECH_BUF B_8 ( .A(N267), .Z(N228) );
  GTECH_BUF B_9 ( .A(N270), .Z(N241) );
  GTECH_AND2 C928 ( .A(r5), .B(csr[15]), .Z(N267) );
  GTECH_AND2 C929 ( .A(N228), .B(csr[15]) );
  GTECH_NOT I_27 ( .A(r5), .Z(N268) );
  GTECH_AND2 C931 ( .A(csr[15]), .B(N268), .Z(N269) );
  GTECH_AND2 C932 ( .A(N224), .B(N269), .Z(N270) );
  GTECH_AND2 C933 ( .A(N241), .B(N269) );
  GTECH_AND2 C934 ( .A(r5), .B(N443), .Z(N271) );
  GTECH_OR2 C935 ( .A(buf0_set), .B(buf0_rl), .Z(N443) );
  GTECH_AND2 C936 ( .A(N452), .B(N415), .Z(N272) );
  GTECH_OR2 C937 ( .A(N451), .B(dma_out_cnt[2]), .Z(N452) );
  GTECH_OR2 C938 ( .A(N450), .B(dma_out_cnt[3]), .Z(N451) );
  GTECH_OR2 C939 ( .A(N449), .B(dma_out_cnt[4]), .Z(N450) );
  GTECH_OR2 C940 ( .A(N448), .B(dma_out_cnt[5]), .Z(N449) );
  GTECH_OR2 C941 ( .A(N447), .B(dma_out_cnt[6]), .Z(N448) );
  GTECH_OR2 C942 ( .A(N446), .B(dma_out_cnt[7]), .Z(N447) );
  GTECH_OR2 C943 ( .A(N445), .B(dma_out_cnt[8]), .Z(N446) );
  GTECH_OR2 C944 ( .A(N444), .B(dma_out_cnt[9]), .Z(N445) );
  GTECH_OR2 C945 ( .A(dma_out_cnt[11]), .B(dma_out_cnt[10]), .Z(N444) );
  GTECH_AND2 C947 ( .A(ep_match_r), .B(N454), .Z(N273) );
  GTECH_OR2 C948 ( .A(N453), .B(buf0_rl), .Z(N454) );
  GTECH_OR2 C949 ( .A(set_r), .B(buf0_set), .Z(N453) );
  GTECH_OR2 C953 ( .A(r5), .B(N223), .Z(N274) );
  GTECH_OR2 C954 ( .A(N273), .B(N274), .Z(N275) );
  GTECH_NOT I_28 ( .A(N275), .Z(N276) );
  GTECH_BUF B_10 ( .A(N316), .Z(N277) );
  GTECH_BUF B_11 ( .A(N318), .Z(N290) );
  GTECH_AND2 C959 ( .A(r5), .B(csr[15]), .Z(N316) );
  GTECH_AND2 C960 ( .A(N277), .B(csr[15]) );
  GTECH_AND2 C962 ( .A(csr[15]), .B(N268), .Z(N317) );
  GTECH_AND2 C963 ( .A(N273), .B(N317), .Z(N318) );
  GTECH_AND2 C964 ( .A(N290), .B(N317) );
  GTECH_AND2 C965 ( .A(N319), .B(N392), .Z(N320) );
  GTECH_AND2 C966 ( .A(csr[15]), .B(N455), .Z(dma_req_d) );
  GTECH_OR2 C967 ( .A(dma_req_in_d), .B(dma_req_out_d), .Z(N455) );
  GTECH_AND2 C968 ( .A(N415), .B(N406), .Z(dma_req_out_d) );
  GTECH_AND2 C970 ( .A(N395), .B(N334), .Z(dma_req_in_d) );
  GTECH_AND2 C971 ( .A(N395), .B(N464), .Z(N348) );
  GTECH_OR2 C972 ( .A(N463), .B(buf0_orig[21]), .Z(N464) );
  GTECH_OR2 C973 ( .A(N462), .B(buf0_orig[22]), .Z(N463) );
  GTECH_OR2 C974 ( .A(N461), .B(buf0_orig[23]), .Z(N462) );
  GTECH_OR2 C975 ( .A(N460), .B(buf0_orig[24]), .Z(N461) );
  GTECH_OR2 C976 ( .A(N459), .B(buf0_orig[25]), .Z(N460) );
  GTECH_OR2 C977 ( .A(N458), .B(buf0_orig[26]), .Z(N459) );
  GTECH_OR2 C978 ( .A(N457), .B(buf0_orig[27]), .Z(N458) );
  GTECH_OR2 C979 ( .A(N456), .B(buf0_orig[28]), .Z(N457) );
  GTECH_OR2 C980 ( .A(buf0_orig[30]), .B(buf0_orig[29]), .Z(N456) );
  GTECH_AND2 C984 ( .A(dma_req_in_hold), .B(dma_req_in_hold2), .Z(N349) );
  GTECH_AND2 C985 ( .A(r1), .B(N465), .Z(N350) );
  GTECH_NOT I_29 ( .A(r2), .Z(N465) );
  GTECH_AND2 C987 ( .A(dma_ack), .B(N466), .Z(N351) );
  GTECH_NOT I_30 ( .A(dma_req_hold), .Z(N466) );
  GTECH_OR2 C991 ( .A(N350), .B(N51), .Z(N352) );
  GTECH_OR2 C992 ( .A(N351), .B(N352), .Z(N353) );
  GTECH_NOT I_31 ( .A(N353), .Z(N354) );
  GTECH_AND2 C994 ( .A(N350), .B(rst), .Z(N357) );
  GTECH_NOT I_32 ( .A(N350), .Z(N358) );
  GTECH_AND2 C996 ( .A(rst), .B(N358), .Z(N359) );
  GTECH_AND2 C997 ( .A(N351), .B(N359), .Z(N360) );
  GTECH_AND2 C998 ( .A(N469), .B(N470), .Z(N361) );
  GTECH_AND2 C999 ( .A(N467), .B(N468), .Z(N469) );
  GTECH_AND2 C1000 ( .A(dma_req_d), .B(N465), .Z(N467) );
  GTECH_NOT I_33 ( .A(r4), .Z(N468) );
  GTECH_NOT I_34 ( .A(r5), .Z(N470) );
  GTECH_OR2 C1006 ( .A(r1), .B(N51), .Z(N362) );
  GTECH_OR2 C1007 ( .A(r4), .B(N362), .Z(N363) );
  GTECH_NOT I_35 ( .A(N363), .Z(N364) );
  GTECH_AND2 C1009 ( .A(r1), .B(rst), .Z(N367) );
  GTECH_NOT I_36 ( .A(r1), .Z(N368) );
  GTECH_AND2 C1011 ( .A(rst), .B(N368), .Z(N369) );
  GTECH_AND2 C1012 ( .A(r4), .B(N369), .Z(N370) );
  GTECH_OR2 C1015 ( .A(dma_ack), .B(N51), .Z(N371) );
  GTECH_OR2 C1016 ( .A(dma_ack_clr1), .B(N371), .Z(N372) );
  GTECH_NOT I_37 ( .A(N372), .Z(N373) );
  GTECH_AND2 C1018 ( .A(dma_ack), .B(rst), .Z(N376) );
  GTECH_NOT I_38 ( .A(dma_ack), .Z(N377) );
  GTECH_AND2 C1020 ( .A(rst), .B(N377), .Z(N378) );
  GTECH_AND2 C1021 ( .A(dma_ack_clr1), .B(N378), .Z(N379) );
endmodule


module usbf_ep_rf_dummy ( clk, wclk, rst, adr, re, we, din, dout, inta, intb, 
        dma_req, dma_ack, idin, ep_sel, ep_match, buf0_rl, buf0_set, buf1_set, 
        uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, 
        int_crc16_set, int_to_set, int_seqerr_set, out_to_small, csr, buf0, 
        buf1, dma_in_buf_sz1, dma_out_buf_avail );
  input [1:0] adr;
  input [31:0] din;
  output [31:0] dout;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  input clk, wclk, rst, re, we, dma_ack, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small;
  output inta, intb, dma_req, ep_match, dma_in_buf_sz1, dma_out_buf_avail;

  assign buf1[0] = 1'b1;
  assign buf1[1] = 1'b1;
  assign buf1[2] = 1'b1;
  assign buf1[3] = 1'b1;
  assign buf1[4] = 1'b1;
  assign buf1[5] = 1'b1;
  assign buf1[6] = 1'b1;
  assign buf1[7] = 1'b1;
  assign buf1[8] = 1'b1;
  assign buf1[9] = 1'b1;
  assign buf1[10] = 1'b1;
  assign buf1[11] = 1'b1;
  assign buf1[12] = 1'b1;
  assign buf1[13] = 1'b1;
  assign buf1[14] = 1'b1;
  assign buf1[15] = 1'b1;
  assign buf1[16] = 1'b1;
  assign buf1[17] = 1'b1;
  assign buf1[18] = 1'b1;
  assign buf1[19] = 1'b1;
  assign buf1[20] = 1'b1;
  assign buf1[21] = 1'b1;
  assign buf1[22] = 1'b1;
  assign buf1[23] = 1'b1;
  assign buf1[24] = 1'b1;
  assign buf1[25] = 1'b1;
  assign buf1[26] = 1'b1;
  assign buf1[27] = 1'b1;
  assign buf1[28] = 1'b1;
  assign buf1[29] = 1'b1;
  assign buf1[30] = 1'b1;
  assign buf1[31] = 1'b1;
  assign buf0[0] = 1'b1;
  assign buf0[1] = 1'b1;
  assign buf0[2] = 1'b1;
  assign buf0[3] = 1'b1;
  assign buf0[4] = 1'b1;
  assign buf0[5] = 1'b1;
  assign buf0[6] = 1'b1;
  assign buf0[7] = 1'b1;
  assign buf0[8] = 1'b1;
  assign buf0[9] = 1'b1;
  assign buf0[10] = 1'b1;
  assign buf0[11] = 1'b1;
  assign buf0[12] = 1'b1;
  assign buf0[13] = 1'b1;
  assign buf0[14] = 1'b1;
  assign buf0[15] = 1'b1;
  assign buf0[16] = 1'b1;
  assign buf0[17] = 1'b1;
  assign buf0[18] = 1'b1;
  assign buf0[19] = 1'b1;
  assign buf0[20] = 1'b1;
  assign buf0[21] = 1'b1;
  assign buf0[22] = 1'b1;
  assign buf0[23] = 1'b1;
  assign buf0[24] = 1'b1;
  assign buf0[25] = 1'b1;
  assign buf0[26] = 1'b1;
  assign buf0[27] = 1'b1;
  assign buf0[28] = 1'b1;
  assign buf0[29] = 1'b1;
  assign buf0[30] = 1'b1;
  assign buf0[31] = 1'b1;
  assign dma_out_buf_avail = 1'b0;
  assign dma_in_buf_sz1 = 1'b0;
  assign csr[0] = 1'b0;
  assign csr[1] = 1'b0;
  assign csr[2] = 1'b0;
  assign csr[3] = 1'b0;
  assign csr[4] = 1'b0;
  assign csr[5] = 1'b0;
  assign csr[6] = 1'b0;
  assign csr[7] = 1'b0;
  assign csr[8] = 1'b0;
  assign csr[9] = 1'b0;
  assign csr[10] = 1'b0;
  assign csr[11] = 1'b0;
  assign csr[12] = 1'b0;
  assign csr[13] = 1'b0;
  assign csr[14] = 1'b0;
  assign csr[15] = 1'b0;
  assign csr[16] = 1'b0;
  assign csr[17] = 1'b0;
  assign csr[18] = 1'b0;
  assign csr[19] = 1'b0;
  assign csr[20] = 1'b0;
  assign csr[21] = 1'b0;
  assign csr[22] = 1'b0;
  assign csr[23] = 1'b0;
  assign csr[24] = 1'b0;
  assign csr[25] = 1'b0;
  assign csr[26] = 1'b0;
  assign csr[27] = 1'b0;
  assign csr[28] = 1'b0;
  assign csr[29] = 1'b0;
  assign csr[30] = 1'b0;
  assign csr[31] = 1'b0;
  assign ep_match = 1'b0;
  assign dma_req = 1'b0;
  assign intb = 1'b0;
  assign inta = 1'b0;
  assign dout[0] = 1'b0;
  assign dout[1] = 1'b0;
  assign dout[2] = 1'b0;
  assign dout[3] = 1'b0;
  assign dout[4] = 1'b0;
  assign dout[5] = 1'b0;
  assign dout[6] = 1'b0;
  assign dout[7] = 1'b0;
  assign dout[8] = 1'b0;
  assign dout[9] = 1'b0;
  assign dout[10] = 1'b0;
  assign dout[11] = 1'b0;
  assign dout[12] = 1'b0;
  assign dout[13] = 1'b0;
  assign dout[14] = 1'b0;
  assign dout[15] = 1'b0;
  assign dout[16] = 1'b0;
  assign dout[17] = 1'b0;
  assign dout[18] = 1'b0;
  assign dout[19] = 1'b0;
  assign dout[20] = 1'b0;
  assign dout[21] = 1'b0;
  assign dout[22] = 1'b0;
  assign dout[23] = 1'b0;
  assign dout[24] = 1'b0;
  assign dout[25] = 1'b0;
  assign dout[26] = 1'b0;
  assign dout[27] = 1'b0;
  assign dout[28] = 1'b0;
  assign dout[29] = 1'b0;
  assign dout[30] = 1'b0;
  assign dout[31] = 1'b0;

endmodule


module usbf_rf ( clk, wclk, rst, adr, re, we, din, dout, inta, intb, dma_req, 
        dma_ack, idin, ep_sel, match, buf0_rl, buf0_set, buf1_set, uc_bsel_set, 
        uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, int_crc16_set, 
        int_to_set, int_seqerr_set, out_to_small, csr, buf0, buf1, funct_adr, 
        dma_in_buf_sz1, dma_out_buf_avail, frm_nat, utmi_vend_stat, 
        utmi_vend_ctrl, utmi_vend_wr, line_stat, usb_attached, mode_hs, 
        suspend, attached, usb_reset, pid_cs_err, nse_err, crc5_err, rx_err, 
        rf_resume_req );
  input [6:0] adr;
  input [31:0] din;
  output [31:0] dout;
  output [15:0] dma_req;
  input [15:0] dma_ack;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  output [6:0] funct_adr;
  input [31:0] frm_nat;
  input [7:0] utmi_vend_stat;
  output [3:0] utmi_vend_ctrl;
  input [1:0] line_stat;
  input clk, wclk, rst, re, we, buf0_rl, buf0_set, buf1_set, uc_bsel_set,
         uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, int_crc16_set,
         int_to_set, int_seqerr_set, out_to_small, usb_attached, mode_hs,
         suspend, attached, usb_reset, pid_cs_err, nse_err, crc5_err, rx_err;
  output inta, intb, match, dma_in_buf_sz1, dma_out_buf_avail, utmi_vend_wr,
         rf_resume_req;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15,
         N16, N17, N18, N19, N20, N21, N22, N23, N24, N25, N26, N27, N28, N29,
         N30, N31, N32, N33, N34, N35, N36, N37, N38, N39, N40, N41, N42, N43,
         N44, N45, N46, N47, N48, N49, N50, N51, N52, N53, N54, N55, N56, N57,
         N58, N59, N60, N61, N62, N63, N64, N65, N66, N67, N68, N69, N70, N71,
         N72, N73, N74, N75, N76, N77, N78, N79, N80, N81, N82, N83, N84, N85,
         N86, N87, N88, N89, N90, N91, N92, N93, N94, N95, N96, N97, N98, N99,
         N100, N101, N102, N103, int_src_re, N104, N105, N106, N107, N108,
         utmi_vend_wr_r, N109, N110, N111, N112, N113, N114, N115, N116, N117,
         N118, N119, N120, N121, rf_resume_req_r, N122, N123, N124, N125, N126,
         N127, N128, N129, N130, N131, N132, N133, N134, N135, N136, N137,
         N138, N139, N140, N141, N142, N143, N144, N145, N146, N147, N148,
         N149, N150, N151, N152, N153, N154, N155, N156, N157, N158, N159,
         N160, N161, N162, N163, N164, N165, N166, N167, N168, N169, N170,
         N171, N172, N173, N174, N175, N176, N177, N178, N179, N180, N181,
         N182, N183, N184, N185, N186, N187, N188, N189, N190, N191, N192,
         N193, N194, N195, N196, N197, N198, N199, N200, N201, N202, N203,
         N204, N205, N206, N207, N208, N209, N210, N211, N212, N213, N214,
         N215, N216, N217, N218, N219, N220, N221, N222, N223, N224, N225,
         N226, N227, N228, N229, N230, N231, N232, N233, N234, N235, N236,
         N237, N238, N239, N240, N241, N242, N243, N244, N245, N246, N247,
         N248, N249, N250, N251, N252, N253, N254, N255, N256, N257, N258,
         N259, N260, N261, N262, N263, N264, N265, N266, N267, N268, N269,
         N270, N271, N272, N273, N274, N275, N276, N277, N278, N279, ep0_re,
         ep1_re, ep2_re, ep3_re, ep4_re, ep5_re, ep6_re, ep7_re, ep8_re,
         ep9_re, ep10_re, ep11_re, ep12_re, ep13_re, ep14_re, ep15_re, ep0_we,
         ep1_we, ep2_we, ep3_we, ep4_we, ep5_we, ep6_we, ep7_we, ep8_we,
         ep9_we, ep10_we, ep11_we, ep12_we, ep13_we, ep14_we, ep15_we,
         ep0_match, ep1_match, ep2_match, ep3_match, ep4_match, ep5_match,
         ep6_match, ep7_match, ep8_match, ep9_match, ep10_match, ep11_match,
         ep12_match, ep13_match, ep14_match, ep15_match, N280, N281, N282,
         N283, N284, N285, N286, N287, N288, N289, N290, N291, N292, N293,
         N294, N295, N296, N297, N298, N299, N300, N301, N302, N303, N304,
         N305, N306, N307, N308, N309, N310, N311, N312, N313, N314, N315,
         N316, N317, N318, N319, N320, N321, N322, N323, N324, N325, N326,
         N327, N328, N329, N330, N331, N332, N333, N334, N335, N336, N337,
         N338, N339, N340, N341, N342, N343, N344, N345, N346, N347, N348,
         N349, N350, N351, N352, N353, N354, N355, N356, N357, N358, N359,
         N360, N361, N362, N363, N364, N365, N366, N367, N368, N369, N370,
         N371, N372, N373, N374, N375, N376, N377, N378, N379, N380, N381,
         N382, N383, N384, N385, N386, N387, N388, N389, N390, N391, N392,
         N393, N394, N395, N396, N397, N398, N399, N400, N401, N402, N403,
         N404, N405, N406, N407, N408, N409, N410, N411, N412, N413, N414,
         N415, N416, N417, N418, N419, N420, N421, N422, N423, N424, N425,
         N426, N427, N428, N429, N430, N431, N432, N433, N434, N435, N436,
         N437, N438, N439, N440, N441, N442, N443, N444, N445, N446, N447,
         N448, N449, N450, N451, N452, N453, N454, N455, N456, N457, N458,
         N459, N460, N461, N462, N463, N464, N465, N466, N467, N468, N469,
         N470, N471, N472, N473, N474, N475, N476, N477, N478, N479, N480,
         N481, N482, N483, N484, N485, N486, N487, N488, N489, N490, N491,
         N492, N493, N494, N495, N496, N497, N498, N499, N500, N501, N502,
         N503, N504, N505, N506, N507, N508, N509, N510, N511, N512, N513,
         N514, N515, N516, N517, N518, N519, N520, N521, N522, N523, N524,
         N525, N526, N527, N528, N529, N530, N531, N532, N533, N534, N535,
         N536, N537, N538, N539, N540, N541, N542, N543, N544, N545,
         ep0_dma_in_buf_sz1, ep1_dma_in_buf_sz1, ep2_dma_in_buf_sz1,
         ep3_dma_in_buf_sz1, ep4_dma_in_buf_sz1, ep5_dma_in_buf_sz1,
         ep6_dma_in_buf_sz1, ep7_dma_in_buf_sz1, ep8_dma_in_buf_sz1,
         ep9_dma_in_buf_sz1, ep10_dma_in_buf_sz1, ep11_dma_in_buf_sz1,
         ep12_dma_in_buf_sz1, ep13_dma_in_buf_sz1, ep14_dma_in_buf_sz1,
         ep15_dma_in_buf_sz1, N546, N547, N548, N549, N550, N551, N552, N553,
         N554, N555, N556, N557, N558, N559, N560, N561, N562, N563, N564,
         N565, N566, N567, N568, N569, N570, N571, N572, N573, N574, N575,
         N576, N577, N578, N579, N580, N581, N582, N583, N584, N585, N586,
         N587, N588, N589, N590, N591, N592, ep0_dma_out_buf_avail,
         ep1_dma_out_buf_avail, ep2_dma_out_buf_avail, ep3_dma_out_buf_avail,
         ep4_dma_out_buf_avail, ep5_dma_out_buf_avail, ep6_dma_out_buf_avail,
         ep7_dma_out_buf_avail, ep8_dma_out_buf_avail, ep9_dma_out_buf_avail,
         ep10_dma_out_buf_avail, ep11_dma_out_buf_avail,
         ep12_dma_out_buf_avail, ep13_dma_out_buf_avail,
         ep14_dma_out_buf_avail, ep15_dma_out_buf_avail, N593, N594, N595,
         N596, N597, N598, N599, N600, N601, N602, N603, N604, N605, N606,
         N607, N608, N609, N610, N611, N612, N613, N614, N615, N616, N617,
         N618, N619, N620, N621, N622, N623, attach_r, attach_r1, suspend_r,
         suspend_r1, usb_reset_r, rx_err_r, nse_err_r, pid_cs_err_r,
         crc5_err_r, attach, deattach, suspend_start, suspend_end, N624, N625,
         N626, N627, N628, N629, N630, N631, N632, N633, N634, N635, N636,
         N637, N638, N639, N640, N641, N642, N643, N644, N645, N646, N647,
         N648, N649, N650, N651, N652, N653, N654, N655, N656, N657, N658,
         N659, N660, N661, N662, N663, N664, N665, N666, N667, N668, N669,
         N670, N671, N672, N673, N674, N675, N676, N677, N678, N679, N680,
         N681, N682, N683, N684, N685, N686, N687, N688, N689, N690, N691,
         N692, N693, N694, N695, N696, N697, N698, N699, N700, N701, N702,
         N703, N704, N705, N706, N707, N708, N709, N710, N711, N712, N713,
         N714, ep15_inta, ep15_intb, N715, ep14_inta, ep14_intb, N716,
         ep13_inta, ep13_intb, N717, ep12_inta, ep12_intb, N718, ep11_inta,
         ep11_intb, N719, ep10_inta, ep10_intb, N720, ep9_inta, ep9_intb, N721,
         ep8_inta, ep8_intb, N722, ep7_inta, ep7_intb, N723, ep6_inta,
         ep6_intb, N724, ep5_inta, ep5_intb, N725, ep4_inta, ep4_intb, N726,
         ep3_inta, ep3_intb, N727, ep2_inta, ep2_intb, N728, ep1_inta,
         ep1_intb, N729, ep0_inta, ep0_intb, N730, inta_ep, intb_ep, inta_rf,
         intb_rf, N731, N732, N733, N734, N735, N736, N737, N738, N739, N740,
         N741, N742, N743, N744, N745, N746, N747, N748, N749, N750, N751,
         N752, N753, N754, N755, N756, N757, N758, N759, N760, N761, N762,
         N763, N764, N765, N766, N767, N768, N769, N770, N771, N772, N773,
         N774, N775, N776, N777, N778, N779, N780, N781, N782, N783, N784,
         N785, N786, N787, N788, N789, N790, N791, N792, N793, N794, N795,
         N796, N797, N798, N799, N800, N801, N802, N803, N804, N805, N806,
         N807, N808, N809, N810, N811, N812, N813, N814, N815, N816, N817,
         N818, N819, N820, N821, N822, N823, N824, N825, N826, N827, N828,
         N829, N830, N831, N832, N833, N834, N835, N836, N837, N838, N839,
         N840, N841, N842, N843, N844, N845, N846, N847, N848, N849, N850,
         N851, N852, N853, N854, N855, N856, N857, N858, N859, N860, N861,
         N862, N863, N864, N865, N866, N867, N868, N869, N870, N871, N872,
         N873, N874, N875, N876, N877, N878, N879, N880, N881, N882, N883,
         N884, N885, N886, N887, N888, N889, N890, N891, N892, N893, N894,
         N895, N896, N897, N898, N899, N900, N901, N902, N903, N904, N905,
         N906, N907, N908, N909, N910, N911, N912, N913, N914, N915, N916,
         N917, N918, N919, N920, N921, N922, N923, N924, N925, N926, N927,
         N928, N929, N930, N931, N932, N933, N934, N935, N936, N937, N938,
         N939, N940, N941, N942, N943, N944, N945, N946, N947, N948, N949,
         N950, N951, N952, N953, N954, N955, N956, N957, N958, N959, N960,
         N961, N962, N963, N964, N965, N966, N967, N968, N969, N970, N971,
         N972, N973, N974, N975, N976, N977, N978, N979, N980, N981, N982,
         N983, N984, N985, N986, N987, N988, N989, N990, N991, N992, N993,
         N994, N995, N996;
  wire   [31:0] dtmp;
  wire   [8:0] intb_msk;
  wire   [8:0] inta_msk;
  wire   [8:0] int_srcb;
  wire   [15:0] int_srca;
  wire   [7:0] utmi_vend_stat_r;
  wire   [3:0] utmi_vend_ctrl_r;
  wire   [31:0] ep0_dout;
  wire   [31:0] ep1_dout;
  wire   [31:0] ep2_dout;
  wire   [31:0] ep3_dout;
  wire   [31:0] ep4_dout;
  wire   [31:0] ep5_dout;
  wire   [31:0] ep6_dout;
  wire   [31:0] ep7_dout;
  wire   [31:0] ep8_dout;
  wire   [31:0] ep9_dout;
  wire   [31:0] ep10_dout;
  wire   [31:0] ep11_dout;
  wire   [31:0] ep12_dout;
  wire   [31:0] ep13_dout;
  wire   [31:0] ep14_dout;
  wire   [31:0] ep15_dout;
  wire   [31:0] ep0_csr;
  wire   [31:0] ep1_csr;
  wire   [31:0] ep2_csr;
  wire   [31:0] ep3_csr;
  wire   [31:0] ep4_csr;
  wire   [31:0] ep5_csr;
  wire   [31:0] ep6_csr;
  wire   [31:0] ep7_csr;
  wire   [31:0] ep8_csr;
  wire   [31:0] ep9_csr;
  wire   [31:0] ep10_csr;
  wire   [31:0] ep11_csr;
  wire   [31:0] ep12_csr;
  wire   [31:0] ep13_csr;
  wire   [31:0] ep14_csr;
  wire   [31:0] ep15_csr;
  wire   [31:0] ep0_buf0;
  wire   [31:0] ep1_buf0;
  wire   [31:0] ep2_buf0;
  wire   [31:0] ep3_buf0;
  wire   [31:0] ep4_buf0;
  wire   [31:0] ep5_buf0;
  wire   [31:0] ep6_buf0;
  wire   [31:0] ep7_buf0;
  wire   [31:0] ep8_buf0;
  wire   [31:0] ep9_buf0;
  wire   [31:0] ep10_buf0;
  wire   [31:0] ep11_buf0;
  wire   [31:0] ep12_buf0;
  wire   [31:0] ep13_buf0;
  wire   [31:0] ep14_buf0;
  wire   [31:0] ep15_buf0;
  wire   [31:0] ep0_buf1;
  wire   [31:0] ep1_buf1;
  wire   [31:0] ep2_buf1;
  wire   [31:0] ep3_buf1;
  wire   [31:0] ep4_buf1;
  wire   [31:0] ep5_buf1;
  wire   [31:0] ep6_buf1;
  wire   [31:0] ep7_buf1;
  wire   [31:0] ep8_buf1;
  wire   [31:0] ep9_buf1;
  wire   [31:0] ep10_buf1;
  wire   [31:0] ep11_buf1;
  wire   [31:0] ep12_buf1;
  wire   [31:0] ep13_buf1;
  wire   [31:0] ep14_buf1;
  wire   [31:0] ep15_buf1;

  GTECH_AND2 C7 ( .A(N898), .B(N40), .Z(N42) );
  GTECH_AND2 C8 ( .A(N42), .B(N41), .Z(N43) );
  GTECH_OR2 C10 ( .A(adr[2]), .B(adr[1]), .Z(N45) );
  GTECH_OR2 C11 ( .A(N45), .B(N44), .Z(N46) );
  GTECH_OR2 C14 ( .A(adr[2]), .B(N48), .Z(N49) );
  GTECH_OR2 C15 ( .A(N49), .B(adr[0]), .Z(N50) );
  GTECH_OR2 C19 ( .A(adr[2]), .B(N52), .Z(N54) );
  GTECH_OR2 C20 ( .A(N54), .B(N53), .Z(N55) );
  GTECH_OR2 C23 ( .A(N898), .B(adr[1]), .Z(N57) );
  GTECH_OR2 C24 ( .A(N57), .B(adr[0]), .Z(N58) );
  GTECH_OR2 C28 ( .A(N898), .B(adr[1]), .Z(N61) );
  GTECH_OR2 C29 ( .A(N61), .B(N60), .Z(N62) );
  \**SEQGEN**  \dtmp_reg[31]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N102), .enable(N70), .Q(dtmp[31]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[30]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N101), .enable(N70), .Q(dtmp[30]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[29]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N100), .enable(N70), .Q(dtmp[29]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[28]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N99), .enable(N70), .Q(dtmp[28]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[27]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N98), .enable(N70), .Q(dtmp[27]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[26]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N97), .enable(N70), .Q(dtmp[26]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[25]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N96), .enable(N70), .Q(dtmp[25]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[24]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N95), .enable(N70), .Q(dtmp[24]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[23]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N94), .enable(N70), .Q(dtmp[23]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[22]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N93), .enable(N70), .Q(dtmp[22]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[21]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N92), .enable(N70), .Q(dtmp[21]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[20]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N91), .enable(N70), .Q(dtmp[20]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[19]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N90), .enable(N70), .Q(dtmp[19]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[18]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N89), .enable(N70), .Q(dtmp[18]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[17]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N88), .enable(N70), .Q(dtmp[17]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[16]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N87), .enable(N70), .Q(dtmp[16]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N86), .enable(N70), .Q(dtmp[15]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N85), .enable(N70), .Q(dtmp[14]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N84), .enable(N70), .Q(dtmp[13]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N83), .enable(N70), .Q(dtmp[12]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N82), .enable(N70), .Q(dtmp[11]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N81), .enable(N70), .Q(dtmp[10]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N80), .enable(N70), .Q(dtmp[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N79), .enable(N70), .Q(dtmp[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N78), .enable(N70), .Q(dtmp[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N77), .enable(N70), .Q(dtmp[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N76), .enable(N70), .Q(dtmp[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N75), .enable(N70), .Q(dtmp[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N74), .enable(N70), .Q(dtmp[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N73), .enable(N70), .Q(dtmp[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N72), .enable(N70), .Q(dtmp[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  \dtmp_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(1'b0), 
        .clocked_on(1'b0), .data_in(N71), .enable(N70), .Q(dtmp[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b0) );
  \**SEQGEN**  int_src_re_reg ( .clear(1'b0), .preset(1'b0), .next_state(N103), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(int_src_re), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \utmi_vend_stat_r_reg[7]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(utmi_vend_stat[7]), .clocked_on(wclk), .data_in(1'b0), 
        .enable(1'b0), .Q(utmi_vend_stat_r[7]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \utmi_vend_stat_r_reg[6]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(utmi_vend_stat[6]), .clocked_on(wclk), .data_in(1'b0), 
        .enable(1'b0), .Q(utmi_vend_stat_r[6]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \utmi_vend_stat_r_reg[5]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(utmi_vend_stat[5]), .clocked_on(wclk), .data_in(1'b0), 
        .enable(1'b0), .Q(utmi_vend_stat_r[5]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \utmi_vend_stat_r_reg[4]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(utmi_vend_stat[4]), .clocked_on(wclk), .data_in(1'b0), 
        .enable(1'b0), .Q(utmi_vend_stat_r[4]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \utmi_vend_stat_r_reg[3]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(utmi_vend_stat[3]), .clocked_on(wclk), .data_in(1'b0), 
        .enable(1'b0), .Q(utmi_vend_stat_r[3]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \utmi_vend_stat_r_reg[2]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(utmi_vend_stat[2]), .clocked_on(wclk), .data_in(1'b0), 
        .enable(1'b0), .Q(utmi_vend_stat_r[2]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \utmi_vend_stat_r_reg[1]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(utmi_vend_stat[1]), .clocked_on(wclk), .data_in(1'b0), 
        .enable(1'b0), .Q(utmi_vend_stat_r[1]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \utmi_vend_stat_r_reg[0]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(utmi_vend_stat[0]), .clocked_on(wclk), .data_in(1'b0), 
        .enable(1'b0), .Q(utmi_vend_stat_r[0]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  utmi_vend_wr_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N110), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        utmi_vend_wr_r), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N109) );
  \**SEQGEN**  utmi_vend_wr_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        utmi_vend_wr_r), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        utmi_vend_wr), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \utmi_vend_ctrl_r_reg[3]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(din[3]), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), 
        .Q(utmi_vend_ctrl_r[3]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N116) );
  \**SEQGEN**  \utmi_vend_ctrl_r_reg[2]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(din[2]), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), 
        .Q(utmi_vend_ctrl_r[2]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N116) );
  \**SEQGEN**  \utmi_vend_ctrl_r_reg[1]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(din[1]), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), 
        .Q(utmi_vend_ctrl_r[1]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N116) );
  \**SEQGEN**  \utmi_vend_ctrl_r_reg[0]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(din[0]), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), 
        .Q(utmi_vend_ctrl_r[0]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N116) );
  \**SEQGEN**  \utmi_vend_ctrl_reg[3]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(utmi_vend_ctrl_r[3]), .clocked_on(clk), .data_in(1'b0), 
        .enable(1'b0), .Q(utmi_vend_ctrl[3]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \utmi_vend_ctrl_reg[2]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(utmi_vend_ctrl_r[2]), .clocked_on(clk), .data_in(1'b0), 
        .enable(1'b0), .Q(utmi_vend_ctrl[2]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \utmi_vend_ctrl_reg[1]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(utmi_vend_ctrl_r[1]), .clocked_on(clk), .data_in(1'b0), 
        .enable(1'b0), .Q(utmi_vend_ctrl[1]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \utmi_vend_ctrl_reg[0]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(utmi_vend_ctrl_r[0]), .clocked_on(clk), .data_in(1'b0), 
        .enable(1'b0), .Q(utmi_vend_ctrl[0]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  rf_resume_req_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N123), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        rf_resume_req_r), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N122) );
  \**SEQGEN**  rf_resume_req_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        rf_resume_req_r), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        rf_resume_req), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \funct_adr_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N140), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        funct_adr[6]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N133) );
  \**SEQGEN**  \funct_adr_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N139), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        funct_adr[5]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N133) );
  \**SEQGEN**  \funct_adr_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N138), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        funct_adr[4]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N133) );
  \**SEQGEN**  \funct_adr_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N137), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        funct_adr[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N133) );
  \**SEQGEN**  \funct_adr_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N136), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        funct_adr[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N133) );
  \**SEQGEN**  \funct_adr_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N135), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        funct_adr[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N133) );
  \**SEQGEN**  \funct_adr_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N134), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        funct_adr[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N133) );
  \**SEQGEN**  \intb_msk_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N165), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        intb_msk[8]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N147) );
  \**SEQGEN**  \intb_msk_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N164), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        intb_msk[7]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N147) );
  \**SEQGEN**  \intb_msk_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N163), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        intb_msk[6]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N147) );
  \**SEQGEN**  \intb_msk_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N162), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        intb_msk[5]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N147) );
  \**SEQGEN**  \intb_msk_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N161), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        intb_msk[4]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N147) );
  \**SEQGEN**  \intb_msk_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N160), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        intb_msk[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N147) );
  \**SEQGEN**  \intb_msk_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N159), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        intb_msk[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N147) );
  \**SEQGEN**  \intb_msk_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N158), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        intb_msk[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N147) );
  \**SEQGEN**  \intb_msk_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N157), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        intb_msk[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N147) );
  \**SEQGEN**  \inta_msk_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N156), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        inta_msk[8]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N147) );
  \**SEQGEN**  \inta_msk_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N155), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        inta_msk[7]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N147) );
  \**SEQGEN**  \inta_msk_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N154), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        inta_msk[6]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N147) );
  \**SEQGEN**  \inta_msk_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N153), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        inta_msk[5]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N147) );
  \**SEQGEN**  \inta_msk_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N152), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        inta_msk[4]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N147) );
  \**SEQGEN**  \inta_msk_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N151), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        inta_msk[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N147) );
  \**SEQGEN**  \inta_msk_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N150), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        inta_msk[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N147) );
  \**SEQGEN**  \inta_msk_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N149), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        inta_msk[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N147) );
  \**SEQGEN**  \inta_msk_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N148), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        inta_msk[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N147) );
  GTECH_AND2 C255 ( .A(N848), .B(N767), .Z(N168) );
  GTECH_AND2 C256 ( .A(N733), .B(N750), .Z(N169) );
  GTECH_AND2 C257 ( .A(N168), .B(N169), .Z(N170) );
  GTECH_AND2 C258 ( .A(N170), .B(N898), .Z(N171) );
  GTECH_OR2 C262 ( .A(N187), .B(N216), .Z(N172) );
  GTECH_OR2 C263 ( .A(N172), .B(N898), .Z(N173) );
  GTECH_OR2 C269 ( .A(N177), .B(adr[2]), .Z(N175) );
  GTECH_OR2 C275 ( .A(N187), .B(N223), .Z(N177) );
  GTECH_OR2 C276 ( .A(N177), .B(N898), .Z(N178) );
  GTECH_OR2 C282 ( .A(N182), .B(adr[2]), .Z(N180) );
  GTECH_OR2 C288 ( .A(N187), .B(N203), .Z(N182) );
  GTECH_OR2 C289 ( .A(N182), .B(N898), .Z(N183) );
  GTECH_OR2 C296 ( .A(N188), .B(adr[2]), .Z(N185) );
  GTECH_OR2 C301 ( .A(adr[6]), .B(adr[5]), .Z(N187) );
  GTECH_OR2 C303 ( .A(N187), .B(N210), .Z(N188) );
  GTECH_OR2 C304 ( .A(N188), .B(N898), .Z(N189) );
  GTECH_OR2 C310 ( .A(N193), .B(adr[2]), .Z(N191) );
  GTECH_OR2 C316 ( .A(N209), .B(N216), .Z(N193) );
  GTECH_OR2 C317 ( .A(N193), .B(N898), .Z(N194) );
  GTECH_OR2 C324 ( .A(N198), .B(adr[2]), .Z(N196) );
  GTECH_OR2 C331 ( .A(N209), .B(N223), .Z(N198) );
  GTECH_OR2 C332 ( .A(N198), .B(N898), .Z(N199) );
  GTECH_OR2 C339 ( .A(N204), .B(adr[2]), .Z(N201) );
  GTECH_OR2 C345 ( .A(N733), .B(adr[3]), .Z(N203) );
  GTECH_OR2 C346 ( .A(N209), .B(N203), .Z(N204) );
  GTECH_OR2 C347 ( .A(N204), .B(N898), .Z(N205) );
  GTECH_OR2 C355 ( .A(N211), .B(adr[2]), .Z(N207) );
  GTECH_OR2 C361 ( .A(adr[6]), .B(N767), .Z(N209) );
  GTECH_OR2 C362 ( .A(N733), .B(N750), .Z(N210) );
  GTECH_OR2 C363 ( .A(N209), .B(N210), .Z(N211) );
  GTECH_OR2 C364 ( .A(N211), .B(N898), .Z(N212) );
  GTECH_OR2 C370 ( .A(N217), .B(adr[2]), .Z(N214) );
  GTECH_OR2 C375 ( .A(adr[4]), .B(adr[3]), .Z(N216) );
  GTECH_OR2 C376 ( .A(N222), .B(N216), .Z(N217) );
  GTECH_OR2 C377 ( .A(N217), .B(N898), .Z(N218) );
  GTECH_OR2 C384 ( .A(N224), .B(adr[2]), .Z(N220) );
  GTECH_OR2 C389 ( .A(N848), .B(adr[5]), .Z(N222) );
  GTECH_OR2 C390 ( .A(adr[4]), .B(N750), .Z(N223) );
  GTECH_OR2 C391 ( .A(N222), .B(N223), .Z(N224) );
  GTECH_OR2 C392 ( .A(N224), .B(N898), .Z(N225) );
  \**SEQGEN**  \dout_reg[31]  ( .clear(1'b0), .preset(1'b0), .next_state(N279), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[31]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[30]  ( .clear(1'b0), .preset(1'b0), .next_state(N278), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[30]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[29]  ( .clear(1'b0), .preset(1'b0), .next_state(N277), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[29]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[28]  ( .clear(1'b0), .preset(1'b0), .next_state(N276), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[28]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[27]  ( .clear(1'b0), .preset(1'b0), .next_state(N275), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[27]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[26]  ( .clear(1'b0), .preset(1'b0), .next_state(N274), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[26]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[25]  ( .clear(1'b0), .preset(1'b0), .next_state(N273), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[25]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[24]  ( .clear(1'b0), .preset(1'b0), .next_state(N272), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[24]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[23]  ( .clear(1'b0), .preset(1'b0), .next_state(N271), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[23]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[22]  ( .clear(1'b0), .preset(1'b0), .next_state(N270), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[22]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[21]  ( .clear(1'b0), .preset(1'b0), .next_state(N269), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[21]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[20]  ( .clear(1'b0), .preset(1'b0), .next_state(N268), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[20]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[19]  ( .clear(1'b0), .preset(1'b0), .next_state(N267), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[19]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[18]  ( .clear(1'b0), .preset(1'b0), .next_state(N266), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[18]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[17]  ( .clear(1'b0), .preset(1'b0), .next_state(N265), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[17]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[16]  ( .clear(1'b0), .preset(1'b0), .next_state(N264), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[16]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(N263), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[15]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(N262), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[14]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(N261), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[13]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(N260), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[12]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(N259), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[11]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(N258), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[10]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(N257), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(N256), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N255), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N254), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N253), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N252), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N251), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N250), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N249), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  \dout_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N248), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(dout[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N247) );
  \**SEQGEN**  match_r1_reg ( .clear(1'b0), .preset(1'b0), .next_state(N280), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(match), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \csr_reg[31]  ( .clear(1'b0), .preset(1'b0), .next_state(N329), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[31]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[30]  ( .clear(1'b0), .preset(1'b0), .next_state(N328), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[30]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[29]  ( .clear(1'b0), .preset(1'b0), .next_state(N327), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[29]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[28]  ( .clear(1'b0), .preset(1'b0), .next_state(N326), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[28]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[27]  ( .clear(1'b0), .preset(1'b0), .next_state(N325), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[27]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[26]  ( .clear(1'b0), .preset(1'b0), .next_state(N324), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[26]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[25]  ( .clear(1'b0), .preset(1'b0), .next_state(N323), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[25]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[24]  ( .clear(1'b0), .preset(1'b0), .next_state(N322), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[24]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[23]  ( .clear(1'b0), .preset(1'b0), .next_state(N321), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[23]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[22]  ( .clear(1'b0), .preset(1'b0), .next_state(N320), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[22]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[21]  ( .clear(1'b0), .preset(1'b0), .next_state(N319), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[21]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[20]  ( .clear(1'b0), .preset(1'b0), .next_state(N318), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[20]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[19]  ( .clear(1'b0), .preset(1'b0), .next_state(N317), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[19]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[18]  ( .clear(1'b0), .preset(1'b0), .next_state(N316), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[18]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[17]  ( .clear(1'b0), .preset(1'b0), .next_state(N315), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[17]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[16]  ( .clear(1'b0), .preset(1'b0), .next_state(N314), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[16]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(N313), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[15]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(N312), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[14]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(N311), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[13]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(N310), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[12]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(N309), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[11]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(N308), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[10]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(N307), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(N306), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N305), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N304), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N303), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N302), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N301), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N300), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N299), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \csr_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N298), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(csr[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N297) );
  \**SEQGEN**  \buf0_reg[31]  ( .clear(1'b0), .preset(1'b0), .next_state(N422), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[31]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[30]  ( .clear(1'b0), .preset(1'b0), .next_state(N421), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[30]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[29]  ( .clear(1'b0), .preset(1'b0), .next_state(N420), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[29]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[28]  ( .clear(1'b0), .preset(1'b0), .next_state(N419), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[28]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[27]  ( .clear(1'b0), .preset(1'b0), .next_state(N418), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[27]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[26]  ( .clear(1'b0), .preset(1'b0), .next_state(N417), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[26]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[25]  ( .clear(1'b0), .preset(1'b0), .next_state(N416), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[25]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[24]  ( .clear(1'b0), .preset(1'b0), .next_state(N415), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[24]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[23]  ( .clear(1'b0), .preset(1'b0), .next_state(N414), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[23]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[22]  ( .clear(1'b0), .preset(1'b0), .next_state(N413), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[22]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[21]  ( .clear(1'b0), .preset(1'b0), .next_state(N412), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[21]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[20]  ( .clear(1'b0), .preset(1'b0), .next_state(N411), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[20]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[19]  ( .clear(1'b0), .preset(1'b0), .next_state(N410), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[19]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[18]  ( .clear(1'b0), .preset(1'b0), .next_state(N409), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[18]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[17]  ( .clear(1'b0), .preset(1'b0), .next_state(N408), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[17]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[16]  ( .clear(1'b0), .preset(1'b0), .next_state(N407), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[16]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(N406), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[15]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(N405), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[14]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(N404), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[13]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(N403), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[12]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(N402), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[11]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(N401), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[10]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(N400), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(N399), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N398), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N397), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N396), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N395), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N394), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N393), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N392), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf0_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N391), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf0[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N390) );
  \**SEQGEN**  \buf1_reg[31]  ( .clear(1'b0), .preset(1'b0), .next_state(N500), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[31]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[30]  ( .clear(1'b0), .preset(1'b0), .next_state(N499), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[30]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[29]  ( .clear(1'b0), .preset(1'b0), .next_state(N498), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[29]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[28]  ( .clear(1'b0), .preset(1'b0), .next_state(N497), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[28]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[27]  ( .clear(1'b0), .preset(1'b0), .next_state(N496), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[27]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[26]  ( .clear(1'b0), .preset(1'b0), .next_state(N495), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[26]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[25]  ( .clear(1'b0), .preset(1'b0), .next_state(N494), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[25]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[24]  ( .clear(1'b0), .preset(1'b0), .next_state(N493), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[24]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[23]  ( .clear(1'b0), .preset(1'b0), .next_state(N492), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[23]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[22]  ( .clear(1'b0), .preset(1'b0), .next_state(N491), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[22]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[21]  ( .clear(1'b0), .preset(1'b0), .next_state(N490), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[21]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[20]  ( .clear(1'b0), .preset(1'b0), .next_state(N489), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[20]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[19]  ( .clear(1'b0), .preset(1'b0), .next_state(N488), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[19]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[18]  ( .clear(1'b0), .preset(1'b0), .next_state(N487), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[18]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[17]  ( .clear(1'b0), .preset(1'b0), .next_state(N486), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[17]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[16]  ( .clear(1'b0), .preset(1'b0), .next_state(N485), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[16]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(N484), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[15]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(N483), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[14]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(N482), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[13]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(N481), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[12]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(N480), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[11]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(N479), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[10]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(N478), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[9]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(N477), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[8]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(N476), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[7]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(N475), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[6]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N474), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N473), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N472), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N471), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N470), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  \buf1_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N469), 
        .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(buf1[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N468) );
  \**SEQGEN**  dma_in_buf_sz1_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        N547), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), .Q(
        dma_in_buf_sz1), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N546) );
  \**SEQGEN**  dma_out_buf_avail_reg ( .clear(1'b0), .preset(1'b0), 
        .next_state(N594), .clocked_on(clk), .data_in(1'b0), .enable(1'b0), 
        .Q(dma_out_buf_avail), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(N593) );
  \**SEQGEN**  attach_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        usb_attached), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        attach_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  attach_r1_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        attach_r), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        attach_r1), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  suspend_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        suspend), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        suspend_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  suspend_r1_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        suspend_r), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        suspend_r1), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  usb_reset_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        usb_reset), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        usb_reset_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  rx_err_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(rx_err), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(rx_err_r), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  nse_err_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        nse_err), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        nse_err_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  pid_cs_err_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        pid_cs_err), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        pid_cs_err_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  crc5_err_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        crc5_err), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        crc5_err_r), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \int_srcb_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N629), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srcb[8]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N628) );
  \**SEQGEN**  \int_srcb_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N640), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srcb[7]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N639) );
  \**SEQGEN**  \int_srcb_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N650), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srcb[6]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N649) );
  \**SEQGEN**  \int_srcb_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N660), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srcb[5]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N659) );
  \**SEQGEN**  \int_srcb_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N670), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srcb[4]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N669) );
  \**SEQGEN**  \int_srcb_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N680), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srcb[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N679) );
  \**SEQGEN**  \int_srcb_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N690), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srcb[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N689) );
  \**SEQGEN**  \int_srcb_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N700), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srcb[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N699) );
  \**SEQGEN**  \int_srcb_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N710), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srcb[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(N709) );
  \**SEQGEN**  \int_srca_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N715), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srca[15]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \int_srca_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N716), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srca[14]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \int_srca_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N717), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srca[13]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \int_srca_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N718), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srca[12]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \int_srca_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N719), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srca[11]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \int_srca_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N720), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srca[10]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \int_srca_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N721), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srca[9]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \int_srca_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N722), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srca[8]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \int_srca_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N723), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srca[7]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \int_srca_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N724), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srca[6]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \int_srca_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N725), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srca[5]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \int_srca_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N726), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srca[4]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \int_srca_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N727), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srca[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \int_srca_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N728), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srca[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \int_srca_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N729), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srca[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \int_srca_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N730), .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(
        int_srca[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  inta_reg ( .clear(1'b0), .preset(1'b0), .next_state(N731), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(inta), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  intb_reg ( .clear(1'b0), .preset(1'b0), .next_state(N732), 
        .clocked_on(wclk), .data_in(1'b0), .enable(1'b0), .Q(intb), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  usbf_ep_rf u0 ( .clk(clk), .wclk(wclk), .rst(rst), .adr(adr[1:0]), .re(
        ep0_re), .we(ep0_we), .din(din), .dout(ep0_dout), .inta(ep0_inta), 
        .intb(ep0_intb), .dma_req(dma_req[0]), .dma_ack(dma_ack[0]), .idin(
        idin), .ep_sel(ep_sel), .ep_match(ep0_match), .buf0_rl(buf0_rl), 
        .buf0_set(buf0_set), .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), 
        .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(
        int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(
        int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(ep0_csr), .buf0(
        ep0_buf0), .buf1(ep0_buf1), .dma_in_buf_sz1(ep0_dma_in_buf_sz1), 
        .dma_out_buf_avail(ep0_dma_out_buf_avail) );
  usbf_ep_rf u1 ( .clk(clk), .wclk(wclk), .rst(rst), .adr(adr[1:0]), .re(
        ep1_re), .we(ep1_we), .din(din), .dout(ep1_dout), .inta(ep1_inta), 
        .intb(ep1_intb), .dma_req(dma_req[1]), .dma_ack(dma_ack[1]), .idin(
        idin), .ep_sel(ep_sel), .ep_match(ep1_match), .buf0_rl(buf0_rl), 
        .buf0_set(buf0_set), .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), 
        .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(
        int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(
        int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(ep1_csr), .buf0(
        ep1_buf0), .buf1(ep1_buf1), .dma_in_buf_sz1(ep1_dma_in_buf_sz1), 
        .dma_out_buf_avail(ep1_dma_out_buf_avail) );
  usbf_ep_rf u2 ( .clk(clk), .wclk(wclk), .rst(rst), .adr(adr[1:0]), .re(
        ep2_re), .we(ep2_we), .din(din), .dout(ep2_dout), .inta(ep2_inta), 
        .intb(ep2_intb), .dma_req(dma_req[2]), .dma_ack(dma_ack[2]), .idin(
        idin), .ep_sel(ep_sel), .ep_match(ep2_match), .buf0_rl(buf0_rl), 
        .buf0_set(buf0_set), .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), 
        .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(
        int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(
        int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(ep2_csr), .buf0(
        ep2_buf0), .buf1(ep2_buf1), .dma_in_buf_sz1(ep2_dma_in_buf_sz1), 
        .dma_out_buf_avail(ep2_dma_out_buf_avail) );
  usbf_ep_rf u3 ( .clk(clk), .wclk(wclk), .rst(rst), .adr(adr[1:0]), .re(
        ep3_re), .we(ep3_we), .din(din), .dout(ep3_dout), .inta(ep3_inta), 
        .intb(ep3_intb), .dma_req(dma_req[3]), .dma_ack(dma_ack[3]), .idin(
        idin), .ep_sel(ep_sel), .ep_match(ep3_match), .buf0_rl(buf0_rl), 
        .buf0_set(buf0_set), .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), 
        .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(
        int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(
        int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(ep3_csr), .buf0(
        ep3_buf0), .buf1(ep3_buf1), .dma_in_buf_sz1(ep3_dma_in_buf_sz1), 
        .dma_out_buf_avail(ep3_dma_out_buf_avail) );
  usbf_ep_rf_dummy u4 ( .clk(clk), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep4_re), .we(ep4_we), .din(din), .dout(ep4_dout), .inta(ep4_inta), 
        .intb(ep4_intb), .dma_req(dma_req[4]), .dma_ack(dma_ack[4]), .idin(
        idin), .ep_sel(ep_sel), .ep_match(ep4_match), .buf0_rl(buf0_rl), 
        .buf0_set(buf0_set), .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), 
        .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(
        int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(
        int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(ep4_csr), .buf0(
        ep4_buf0), .buf1(ep4_buf1), .dma_in_buf_sz1(ep4_dma_in_buf_sz1), 
        .dma_out_buf_avail(ep4_dma_out_buf_avail) );
  usbf_ep_rf_dummy u5 ( .clk(clk), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep5_re), .we(ep5_we), .din(din), .dout(ep5_dout), .inta(ep5_inta), 
        .intb(ep5_intb), .dma_req(dma_req[5]), .dma_ack(dma_ack[5]), .idin(
        idin), .ep_sel(ep_sel), .ep_match(ep5_match), .buf0_rl(buf0_rl), 
        .buf0_set(buf0_set), .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), 
        .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(
        int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(
        int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(ep5_csr), .buf0(
        ep5_buf0), .buf1(ep5_buf1), .dma_in_buf_sz1(ep5_dma_in_buf_sz1), 
        .dma_out_buf_avail(ep5_dma_out_buf_avail) );
  usbf_ep_rf_dummy u6 ( .clk(clk), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep6_re), .we(ep6_we), .din(din), .dout(ep6_dout), .inta(ep6_inta), 
        .intb(ep6_intb), .dma_req(dma_req[6]), .dma_ack(dma_ack[6]), .idin(
        idin), .ep_sel(ep_sel), .ep_match(ep6_match), .buf0_rl(buf0_rl), 
        .buf0_set(buf0_set), .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), 
        .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(
        int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(
        int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(ep6_csr), .buf0(
        ep6_buf0), .buf1(ep6_buf1), .dma_in_buf_sz1(ep6_dma_in_buf_sz1), 
        .dma_out_buf_avail(ep6_dma_out_buf_avail) );
  usbf_ep_rf_dummy u7 ( .clk(clk), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep7_re), .we(ep7_we), .din(din), .dout(ep7_dout), .inta(ep7_inta), 
        .intb(ep7_intb), .dma_req(dma_req[7]), .dma_ack(dma_ack[7]), .idin(
        idin), .ep_sel(ep_sel), .ep_match(ep7_match), .buf0_rl(buf0_rl), 
        .buf0_set(buf0_set), .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), 
        .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(
        int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(
        int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(ep7_csr), .buf0(
        ep7_buf0), .buf1(ep7_buf1), .dma_in_buf_sz1(ep7_dma_in_buf_sz1), 
        .dma_out_buf_avail(ep7_dma_out_buf_avail) );
  usbf_ep_rf_dummy u8 ( .clk(clk), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep8_re), .we(ep8_we), .din(din), .dout(ep8_dout), .inta(ep8_inta), 
        .intb(ep8_intb), .dma_req(dma_req[8]), .dma_ack(dma_ack[8]), .idin(
        idin), .ep_sel(ep_sel), .ep_match(ep8_match), .buf0_rl(buf0_rl), 
        .buf0_set(buf0_set), .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), 
        .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(
        int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(
        int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(ep8_csr), .buf0(
        ep8_buf0), .buf1(ep8_buf1), .dma_in_buf_sz1(ep8_dma_in_buf_sz1), 
        .dma_out_buf_avail(ep8_dma_out_buf_avail) );
  usbf_ep_rf_dummy u9 ( .clk(clk), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep9_re), .we(ep9_we), .din(din), .dout(ep9_dout), .inta(ep9_inta), 
        .intb(ep9_intb), .dma_req(dma_req[9]), .dma_ack(dma_ack[9]), .idin(
        idin), .ep_sel(ep_sel), .ep_match(ep9_match), .buf0_rl(buf0_rl), 
        .buf0_set(buf0_set), .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), 
        .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(
        int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(
        int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(ep9_csr), .buf0(
        ep9_buf0), .buf1(ep9_buf1), .dma_in_buf_sz1(ep9_dma_in_buf_sz1), 
        .dma_out_buf_avail(ep9_dma_out_buf_avail) );
  usbf_ep_rf_dummy u10 ( .clk(clk), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep10_re), .we(ep10_we), .din(din), .dout(ep10_dout), .inta(
        ep10_inta), .intb(ep10_intb), .dma_req(dma_req[10]), .dma_ack(
        dma_ack[10]), .idin(idin), .ep_sel(ep_sel), .ep_match(ep10_match), 
        .buf0_rl(buf0_rl), .buf0_set(buf0_set), .buf1_set(buf1_set), 
        .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), .int_buf1_set(
        int_buf1_set), .int_buf0_set(int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(ep10_csr), .buf0(
        ep10_buf0), .buf1(ep10_buf1), .dma_in_buf_sz1(ep10_dma_in_buf_sz1), 
        .dma_out_buf_avail(ep10_dma_out_buf_avail) );
  usbf_ep_rf_dummy u11 ( .clk(clk), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep11_re), .we(ep11_we), .din(din), .dout(ep11_dout), .inta(
        ep11_inta), .intb(ep11_intb), .dma_req(dma_req[11]), .dma_ack(
        dma_ack[11]), .idin(idin), .ep_sel(ep_sel), .ep_match(ep11_match), 
        .buf0_rl(buf0_rl), .buf0_set(buf0_set), .buf1_set(buf1_set), 
        .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), .int_buf1_set(
        int_buf1_set), .int_buf0_set(int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(ep11_csr), .buf0(
        ep11_buf0), .buf1(ep11_buf1), .dma_in_buf_sz1(ep11_dma_in_buf_sz1), 
        .dma_out_buf_avail(ep11_dma_out_buf_avail) );
  usbf_ep_rf_dummy u12 ( .clk(clk), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep12_re), .we(ep12_we), .din(din), .dout(ep12_dout), .inta(
        ep12_inta), .intb(ep12_intb), .dma_req(dma_req[12]), .dma_ack(
        dma_ack[12]), .idin(idin), .ep_sel(ep_sel), .ep_match(ep12_match), 
        .buf0_rl(buf0_rl), .buf0_set(buf0_set), .buf1_set(buf1_set), 
        .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), .int_buf1_set(
        int_buf1_set), .int_buf0_set(int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(ep12_csr), .buf0(
        ep12_buf0), .buf1(ep12_buf1), .dma_in_buf_sz1(ep12_dma_in_buf_sz1), 
        .dma_out_buf_avail(ep12_dma_out_buf_avail) );
  usbf_ep_rf_dummy u13 ( .clk(clk), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep13_re), .we(ep13_we), .din(din), .dout(ep13_dout), .inta(
        ep13_inta), .intb(ep13_intb), .dma_req(dma_req[13]), .dma_ack(
        dma_ack[13]), .idin(idin), .ep_sel(ep_sel), .ep_match(ep13_match), 
        .buf0_rl(buf0_rl), .buf0_set(buf0_set), .buf1_set(buf1_set), 
        .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), .int_buf1_set(
        int_buf1_set), .int_buf0_set(int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(ep13_csr), .buf0(
        ep13_buf0), .buf1(ep13_buf1), .dma_in_buf_sz1(ep13_dma_in_buf_sz1), 
        .dma_out_buf_avail(ep13_dma_out_buf_avail) );
  usbf_ep_rf_dummy u14 ( .clk(clk), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep14_re), .we(ep14_we), .din(din), .dout(ep14_dout), .inta(
        ep14_inta), .intb(ep14_intb), .dma_req(dma_req[14]), .dma_ack(
        dma_ack[14]), .idin(idin), .ep_sel(ep_sel), .ep_match(ep14_match), 
        .buf0_rl(buf0_rl), .buf0_set(buf0_set), .buf1_set(buf1_set), 
        .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), .int_buf1_set(
        int_buf1_set), .int_buf0_set(int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(ep14_csr), .buf0(
        ep14_buf0), .buf1(ep14_buf1), .dma_in_buf_sz1(ep14_dma_in_buf_sz1), 
        .dma_out_buf_avail(ep14_dma_out_buf_avail) );
  usbf_ep_rf_dummy u15 ( .clk(clk), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep15_re), .we(ep15_we), .din(din), .dout(ep15_dout), .inta(
        ep15_inta), .intb(ep15_intb), .dma_req(dma_req[15]), .dma_ack(
        dma_ack[15]), .idin(idin), .ep_sel(ep_sel), .ep_match(ep15_match), 
        .buf0_rl(buf0_rl), .buf0_set(buf0_set), .buf1_set(buf1_set), 
        .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), .int_buf1_set(
        int_buf1_set), .int_buf0_set(int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(ep15_csr), .buf0(
        ep15_buf0), .buf1(ep15_buf1), .dma_in_buf_sz1(ep15_dma_in_buf_sz1), 
        .dma_out_buf_avail(ep15_dma_out_buf_avail) );
  GTECH_NOT I_0 ( .A(adr[4]), .Z(N733) );
  GTECH_OR2 C1445 ( .A(N733), .B(N891), .Z(N734) );
  GTECH_OR2 C1446 ( .A(adr[3]), .B(N734), .Z(N735) );
  GTECH_OR2 C1447 ( .A(adr[2]), .B(N735), .Z(N736) );
  GTECH_NOT I_1 ( .A(N736), .Z(N737) );
  GTECH_OR2 C1451 ( .A(N733), .B(N891), .Z(N738) );
  GTECH_OR2 C1452 ( .A(adr[3]), .B(N738), .Z(N739) );
  GTECH_OR2 C1453 ( .A(adr[2]), .B(N739), .Z(N740) );
  GTECH_NOT I_2 ( .A(N740), .Z(N741) );
  GTECH_OR2 C1458 ( .A(N733), .B(N891), .Z(N742) );
  GTECH_OR2 C1459 ( .A(adr[3]), .B(N742), .Z(N743) );
  GTECH_OR2 C1460 ( .A(N898), .B(N743), .Z(N744) );
  GTECH_NOT I_3 ( .A(N744), .Z(N745) );
  GTECH_OR2 C1465 ( .A(N733), .B(N891), .Z(N746) );
  GTECH_OR2 C1466 ( .A(adr[3]), .B(N746), .Z(N747) );
  GTECH_OR2 C1467 ( .A(N898), .B(N747), .Z(N748) );
  GTECH_NOT I_4 ( .A(N748), .Z(N749) );
  GTECH_NOT I_5 ( .A(adr[3]), .Z(N750) );
  GTECH_OR2 C1472 ( .A(N733), .B(N891), .Z(N751) );
  GTECH_OR2 C1473 ( .A(N750), .B(N751), .Z(N752) );
  GTECH_OR2 C1474 ( .A(adr[2]), .B(N752), .Z(N753) );
  GTECH_NOT I_6 ( .A(N753), .Z(N754) );
  GTECH_OR2 C1479 ( .A(N733), .B(N891), .Z(N755) );
  GTECH_OR2 C1480 ( .A(N750), .B(N755), .Z(N756) );
  GTECH_OR2 C1481 ( .A(adr[2]), .B(N756), .Z(N757) );
  GTECH_NOT I_7 ( .A(N757), .Z(N758) );
  GTECH_OR2 C1487 ( .A(N733), .B(N891), .Z(N759) );
  GTECH_OR2 C1488 ( .A(N750), .B(N759), .Z(N760) );
  GTECH_OR2 C1489 ( .A(N898), .B(N760), .Z(N761) );
  GTECH_NOT I_8 ( .A(N761), .Z(N762) );
  GTECH_OR2 C1495 ( .A(N733), .B(N891), .Z(N763) );
  GTECH_OR2 C1496 ( .A(N750), .B(N763), .Z(N764) );
  GTECH_OR2 C1497 ( .A(N898), .B(N764), .Z(N765) );
  GTECH_NOT I_9 ( .A(N765), .Z(N766) );
  GTECH_NOT I_10 ( .A(adr[5]), .Z(N767) );
  GTECH_OR2 C1500 ( .A(N767), .B(adr[6]), .Z(N768) );
  GTECH_OR2 C1501 ( .A(adr[4]), .B(N768), .Z(N769) );
  GTECH_OR2 C1502 ( .A(adr[3]), .B(N769), .Z(N770) );
  GTECH_OR2 C1503 ( .A(adr[2]), .B(N770), .Z(N771) );
  GTECH_NOT I_11 ( .A(N771), .Z(N772) );
  GTECH_OR2 C1506 ( .A(N767), .B(adr[6]), .Z(N773) );
  GTECH_OR2 C1507 ( .A(adr[4]), .B(N773), .Z(N774) );
  GTECH_OR2 C1508 ( .A(adr[3]), .B(N774), .Z(N775) );
  GTECH_OR2 C1509 ( .A(adr[2]), .B(N775), .Z(N776) );
  GTECH_NOT I_12 ( .A(N776), .Z(N777) );
  GTECH_OR2 C1513 ( .A(N767), .B(adr[6]), .Z(N778) );
  GTECH_OR2 C1514 ( .A(adr[4]), .B(N778), .Z(N779) );
  GTECH_OR2 C1515 ( .A(adr[3]), .B(N779), .Z(N780) );
  GTECH_OR2 C1516 ( .A(N898), .B(N780), .Z(N781) );
  GTECH_NOT I_13 ( .A(N781), .Z(N782) );
  GTECH_OR2 C1520 ( .A(N767), .B(adr[6]), .Z(N783) );
  GTECH_OR2 C1521 ( .A(adr[4]), .B(N783), .Z(N784) );
  GTECH_OR2 C1522 ( .A(adr[3]), .B(N784), .Z(N785) );
  GTECH_OR2 C1523 ( .A(N898), .B(N785), .Z(N786) );
  GTECH_NOT I_14 ( .A(N786), .Z(N787) );
  GTECH_OR2 C1527 ( .A(N767), .B(adr[6]), .Z(N788) );
  GTECH_OR2 C1528 ( .A(adr[4]), .B(N788), .Z(N789) );
  GTECH_OR2 C1529 ( .A(N750), .B(N789), .Z(N790) );
  GTECH_OR2 C1530 ( .A(adr[2]), .B(N790), .Z(N791) );
  GTECH_NOT I_15 ( .A(N791), .Z(N792) );
  GTECH_OR2 C1534 ( .A(N767), .B(adr[6]), .Z(N793) );
  GTECH_OR2 C1535 ( .A(adr[4]), .B(N793), .Z(N794) );
  GTECH_OR2 C1536 ( .A(N750), .B(N794), .Z(N795) );
  GTECH_OR2 C1537 ( .A(adr[2]), .B(N795), .Z(N796) );
  GTECH_NOT I_16 ( .A(N796), .Z(N797) );
  GTECH_OR2 C1542 ( .A(N767), .B(adr[6]), .Z(N798) );
  GTECH_OR2 C1543 ( .A(adr[4]), .B(N798), .Z(N799) );
  GTECH_OR2 C1544 ( .A(N750), .B(N799), .Z(N800) );
  GTECH_OR2 C1545 ( .A(N898), .B(N800), .Z(N801) );
  GTECH_NOT I_17 ( .A(N801), .Z(N802) );
  GTECH_OR2 C1550 ( .A(N767), .B(adr[6]), .Z(N803) );
  GTECH_OR2 C1551 ( .A(adr[4]), .B(N803), .Z(N804) );
  GTECH_OR2 C1552 ( .A(N750), .B(N804), .Z(N805) );
  GTECH_OR2 C1553 ( .A(N898), .B(N805), .Z(N806) );
  GTECH_NOT I_18 ( .A(N806), .Z(N807) );
  GTECH_OR2 C1557 ( .A(N767), .B(adr[6]), .Z(N808) );
  GTECH_OR2 C1558 ( .A(N733), .B(N808), .Z(N809) );
  GTECH_OR2 C1559 ( .A(adr[3]), .B(N809), .Z(N810) );
  GTECH_OR2 C1560 ( .A(adr[2]), .B(N810), .Z(N811) );
  GTECH_NOT I_19 ( .A(N811), .Z(N812) );
  GTECH_OR2 C1564 ( .A(N767), .B(adr[6]), .Z(N813) );
  GTECH_OR2 C1565 ( .A(N733), .B(N813), .Z(N814) );
  GTECH_OR2 C1566 ( .A(adr[3]), .B(N814), .Z(N815) );
  GTECH_OR2 C1567 ( .A(adr[2]), .B(N815), .Z(N816) );
  GTECH_NOT I_20 ( .A(N816), .Z(N817) );
  GTECH_OR2 C1572 ( .A(N767), .B(adr[6]), .Z(N818) );
  GTECH_OR2 C1573 ( .A(N733), .B(N818), .Z(N819) );
  GTECH_OR2 C1574 ( .A(adr[3]), .B(N819), .Z(N820) );
  GTECH_OR2 C1575 ( .A(N898), .B(N820), .Z(N821) );
  GTECH_NOT I_21 ( .A(N821), .Z(N822) );
  GTECH_OR2 C1580 ( .A(N767), .B(adr[6]), .Z(N823) );
  GTECH_OR2 C1581 ( .A(N733), .B(N823), .Z(N824) );
  GTECH_OR2 C1582 ( .A(adr[3]), .B(N824), .Z(N825) );
  GTECH_OR2 C1583 ( .A(N898), .B(N825), .Z(N826) );
  GTECH_NOT I_22 ( .A(N826), .Z(N827) );
  GTECH_OR2 C1588 ( .A(N767), .B(adr[6]), .Z(N828) );
  GTECH_OR2 C1589 ( .A(N733), .B(N828), .Z(N829) );
  GTECH_OR2 C1590 ( .A(N750), .B(N829), .Z(N830) );
  GTECH_OR2 C1591 ( .A(adr[2]), .B(N830), .Z(N831) );
  GTECH_NOT I_23 ( .A(N831), .Z(N832) );
  GTECH_OR2 C1596 ( .A(N767), .B(adr[6]), .Z(N833) );
  GTECH_OR2 C1597 ( .A(N733), .B(N833), .Z(N834) );
  GTECH_OR2 C1598 ( .A(N750), .B(N834), .Z(N835) );
  GTECH_OR2 C1599 ( .A(adr[2]), .B(N835), .Z(N836) );
  GTECH_NOT I_24 ( .A(N836), .Z(N837) );
  GTECH_OR2 C1605 ( .A(N767), .B(adr[6]), .Z(N838) );
  GTECH_OR2 C1606 ( .A(N733), .B(N838), .Z(N839) );
  GTECH_OR2 C1607 ( .A(N750), .B(N839), .Z(N840) );
  GTECH_OR2 C1608 ( .A(N898), .B(N840), .Z(N841) );
  GTECH_NOT I_25 ( .A(N841), .Z(N842) );
  GTECH_OR2 C1614 ( .A(N767), .B(adr[6]), .Z(N843) );
  GTECH_OR2 C1615 ( .A(N733), .B(N843), .Z(N844) );
  GTECH_OR2 C1616 ( .A(N750), .B(N844), .Z(N845) );
  GTECH_OR2 C1617 ( .A(N898), .B(N845), .Z(N846) );
  GTECH_NOT I_26 ( .A(N846), .Z(N847) );
  GTECH_NOT I_27 ( .A(adr[6]), .Z(N848) );
  GTECH_OR2 C1620 ( .A(adr[5]), .B(N848), .Z(N849) );
  GTECH_OR2 C1621 ( .A(adr[4]), .B(N849), .Z(N850) );
  GTECH_OR2 C1622 ( .A(adr[3]), .B(N850), .Z(N851) );
  GTECH_OR2 C1623 ( .A(adr[2]), .B(N851), .Z(N852) );
  GTECH_NOT I_28 ( .A(N852), .Z(N853) );
  GTECH_OR2 C1626 ( .A(adr[5]), .B(N848), .Z(N854) );
  GTECH_OR2 C1627 ( .A(adr[4]), .B(N854), .Z(N855) );
  GTECH_OR2 C1628 ( .A(adr[3]), .B(N855), .Z(N856) );
  GTECH_OR2 C1629 ( .A(adr[2]), .B(N856), .Z(N857) );
  GTECH_NOT I_29 ( .A(N857), .Z(N858) );
  GTECH_OR2 C1633 ( .A(adr[5]), .B(N848), .Z(N859) );
  GTECH_OR2 C1634 ( .A(adr[4]), .B(N859), .Z(N860) );
  GTECH_OR2 C1635 ( .A(adr[3]), .B(N860), .Z(N861) );
  GTECH_OR2 C1636 ( .A(N898), .B(N861), .Z(N862) );
  GTECH_NOT I_30 ( .A(N862), .Z(N863) );
  GTECH_OR2 C1640 ( .A(adr[5]), .B(N848), .Z(N864) );
  GTECH_OR2 C1641 ( .A(adr[4]), .B(N864), .Z(N865) );
  GTECH_OR2 C1642 ( .A(adr[3]), .B(N865), .Z(N866) );
  GTECH_OR2 C1643 ( .A(N898), .B(N866), .Z(N867) );
  GTECH_NOT I_31 ( .A(N867), .Z(N868) );
  GTECH_OR2 C1647 ( .A(adr[5]), .B(N848), .Z(N869) );
  GTECH_OR2 C1648 ( .A(adr[4]), .B(N869), .Z(N870) );
  GTECH_OR2 C1649 ( .A(N750), .B(N870), .Z(N871) );
  GTECH_OR2 C1650 ( .A(adr[2]), .B(N871), .Z(N872) );
  GTECH_NOT I_32 ( .A(N872), .Z(N873) );
  GTECH_OR2 C1654 ( .A(adr[5]), .B(N848), .Z(N874) );
  GTECH_OR2 C1655 ( .A(adr[4]), .B(N874), .Z(N875) );
  GTECH_OR2 C1656 ( .A(N750), .B(N875), .Z(N876) );
  GTECH_OR2 C1657 ( .A(adr[2]), .B(N876), .Z(N877) );
  GTECH_NOT I_33 ( .A(N877), .Z(N878) );
  GTECH_OR2 C1662 ( .A(adr[5]), .B(N848), .Z(N879) );
  GTECH_OR2 C1663 ( .A(adr[4]), .B(N879), .Z(N880) );
  GTECH_OR2 C1664 ( .A(N750), .B(N880), .Z(N881) );
  GTECH_OR2 C1665 ( .A(N898), .B(N881), .Z(N882) );
  GTECH_NOT I_34 ( .A(N882), .Z(N883) );
  GTECH_OR2 C1670 ( .A(adr[5]), .B(N848), .Z(N884) );
  GTECH_OR2 C1671 ( .A(adr[4]), .B(N884), .Z(N885) );
  GTECH_OR2 C1672 ( .A(N750), .B(N885), .Z(N886) );
  GTECH_OR2 C1673 ( .A(N898), .B(N886), .Z(N887) );
  GTECH_NOT I_35 ( .A(N887), .Z(N888) );
  GTECH_NOT I_36 ( .A(adr[1]), .Z(N889) );
  GTECH_NOT I_37 ( .A(adr[0]), .Z(N890) );
  GTECH_OR2 C1677 ( .A(adr[5]), .B(adr[6]), .Z(N891) );
  GTECH_OR2 C1678 ( .A(adr[4]), .B(N891), .Z(N892) );
  GTECH_OR2 C1679 ( .A(adr[3]), .B(N892), .Z(N893) );
  GTECH_OR2 C1680 ( .A(adr[2]), .B(N893), .Z(N894) );
  GTECH_OR2 C1681 ( .A(N889), .B(N894), .Z(N895) );
  GTECH_OR2 C1682 ( .A(N890), .B(N895), .Z(N896) );
  GTECH_NOT I_38 ( .A(N896), .Z(N897) );
  GTECH_NOT I_39 ( .A(adr[2]), .Z(N898) );
  GTECH_NOT I_40 ( .A(adr[0]), .Z(N899) );
  GTECH_OR2 C1689 ( .A(N898), .B(N893), .Z(N900) );
  GTECH_OR2 C1690 ( .A(adr[1]), .B(N900), .Z(N901) );
  GTECH_OR2 C1691 ( .A(N899), .B(N901), .Z(N902) );
  GTECH_NOT I_41 ( .A(N902), .Z(N903) );
  GTECH_NOT I_42 ( .A(adr[0]), .Z(N904) );
  GTECH_OR2 C1698 ( .A(adr[1]), .B(N894), .Z(N905) );
  GTECH_OR2 C1699 ( .A(N904), .B(N905), .Z(N906) );
  GTECH_NOT I_43 ( .A(N906), .Z(N907) );
  GTECH_NOT I_44 ( .A(adr[1]), .Z(N908) );
  GTECH_OR2 C1706 ( .A(N908), .B(N894), .Z(N909) );
  GTECH_OR2 C1707 ( .A(adr[0]), .B(N909), .Z(N910) );
  GTECH_NOT I_45 ( .A(N910), .Z(N911) );
  GTECH_NOT I_46 ( .A(adr[0]), .Z(N912) );
  GTECH_OR2 C1715 ( .A(adr[1]), .B(N900), .Z(N913) );
  GTECH_OR2 C1716 ( .A(N912), .B(N913), .Z(N914) );
  GTECH_NOT I_47 ( .A(N914), .Z(N915) );
  GTECH_OR2 C1722 ( .A(adr[1]), .B(N894), .Z(N916) );
  GTECH_OR2 C1723 ( .A(adr[0]), .B(N916), .Z(N917) );
  GTECH_NOT I_48 ( .A(N917), .Z(N918) );
  SELECT_OP C1725 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b1), 
        .DATA5(1'b1), .DATA6(1'b1), .DATA7(1'b0), .CONTROL1(N0), .CONTROL2(N1), 
        .CONTROL3(N2), .CONTROL4(N3), .CONTROL5(N4), .CONTROL6(N5), .CONTROL7(
        N69), .Z(N70) );
  GTECH_BUF B_0 ( .A(N43), .Z(N0) );
  GTECH_BUF B_1 ( .A(N47), .Z(N1) );
  GTECH_BUF B_2 ( .A(N51), .Z(N2) );
  GTECH_BUF B_3 ( .A(N56), .Z(N3) );
  GTECH_BUF B_4 ( .A(N59), .Z(N4) );
  GTECH_BUF B_5 ( .A(N63), .Z(N5) );
  SELECT_OP C1726 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, line_stat, usb_attached, 
        mode_hs, suspend}), .DATA2({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, funct_adr}), .DATA3({1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, intb_msk, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, inta_msk}), .DATA4({1'b0, 1'b0, 1'b0, int_srcb, 1'b0, 1'b0, 
        1'b0, 1'b0, int_srca}), .DATA5(frm_nat), .DATA6({1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, utmi_vend_stat_r}), .CONTROL1(N0), .CONTROL2(N1), .CONTROL3(N2), .CONTROL4(N3), .CONTROL5(N4), 
        .CONTROL6(N5), .Z({N102, N101, N100, N99, N98, N97, N96, N95, N94, N93, 
        N92, N91, N90, N89, N88, N87, N86, N85, N84, N83, N82, N81, N80, N79, 
        N78, N77, N76, N75, N74, N73, N72, N71}) );
  SELECT_OP C1727 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N6), .CONTROL2(N112), .CONTROL3(N115), .CONTROL4(N108), .Z(
        N109) );
  GTECH_BUF B_6 ( .A(N104), .Z(N6) );
  SELECT_OP C1728 ( .DATA1(1'b0), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N6), 
        .CONTROL2(N112), .CONTROL3(N115), .Z(N110) );
  SELECT_OP C1729 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N7), .CONTROL2(N125), .CONTROL3(N128), .CONTROL4(N121), .Z(
        N122) );
  GTECH_BUF B_7 ( .A(N117), .Z(N7) );
  SELECT_OP C1730 ( .DATA1(1'b0), .DATA2(din[5]), .DATA3(1'b0), .CONTROL1(N7), 
        .CONTROL2(N125), .CONTROL3(N128), .Z(N123) );
  SELECT_OP C1731 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N8), 
        .CONTROL2(N142), .CONTROL3(N132), .Z(N133) );
  GTECH_BUF B_8 ( .A(N129), .Z(N8) );
  SELECT_OP C1732 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), 
        .DATA2(din[6:0]), .CONTROL1(N8), .CONTROL2(N142), .Z({N140, N139, N138, 
        N137, N136, N135, N134}) );
  SELECT_OP C1733 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b0), .CONTROL1(N9), 
        .CONTROL2(N167), .CONTROL3(N146), .Z(N147) );
  GTECH_BUF B_9 ( .A(N143), .Z(N9) );
  SELECT_OP C1734 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0}), .DATA2(din[8:0]), .CONTROL1(N9), .CONTROL2(N167), .Z({N156, 
        N155, N154, N153, N152, N151, N150, N149, N148}) );
  SELECT_OP C1735 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0}), .DATA2(din[24:16]), .CONTROL1(N9), .CONTROL2(N167), .Z({N165, 
        N164, N163, N162, N161, N160, N159, N158, N157}) );
  SELECT_OP C1736 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b1), 
        .DATA5(1'b1), .DATA6(1'b1), .DATA7(1'b1), .DATA8(1'b1), .DATA9(1'b1), 
        .DATA10(1'b1), .DATA11(1'b1), .DATA12(1'b1), .DATA13(1'b1), .DATA14(
        1'b1), .DATA15(1'b1), .DATA16(1'b1), .DATA17(1'b1), .DATA18(1'b1), 
        .DATA19(1'b1), .DATA20(1'b1), .DATA21(1'b0), .CONTROL1(N10), 
        .CONTROL2(N11), .CONTROL3(N12), .CONTROL4(N13), .CONTROL5(N14), 
        .CONTROL6(N15), .CONTROL7(N16), .CONTROL8(N17), .CONTROL9(N18), 
        .CONTROL10(N19), .CONTROL11(N20), .CONTROL12(N21), .CONTROL13(N22), 
        .CONTROL14(N23), .CONTROL15(N24), .CONTROL16(N25), .CONTROL17(N26), 
        .CONTROL18(N27), .CONTROL19(N28), .CONTROL20(N29), .CONTROL21(N246), 
        .Z(N247) );
  GTECH_BUF B_10 ( .A(N171), .Z(N10) );
  GTECH_BUF B_11 ( .A(N174), .Z(N11) );
  GTECH_BUF B_12 ( .A(N176), .Z(N12) );
  GTECH_BUF B_13 ( .A(N179), .Z(N13) );
  GTECH_BUF B_14 ( .A(N181), .Z(N14) );
  GTECH_BUF B_15 ( .A(N184), .Z(N15) );
  GTECH_BUF B_16 ( .A(N186), .Z(N16) );
  GTECH_BUF B_17 ( .A(N190), .Z(N17) );
  GTECH_BUF B_18 ( .A(N192), .Z(N18) );
  GTECH_BUF B_19 ( .A(N195), .Z(N19) );
  GTECH_BUF B_20 ( .A(N197), .Z(N20) );
  GTECH_BUF B_21 ( .A(N200), .Z(N21) );
  GTECH_BUF B_22 ( .A(N202), .Z(N22) );
  GTECH_BUF B_23 ( .A(N206), .Z(N23) );
  GTECH_BUF B_24 ( .A(N208), .Z(N24) );
  GTECH_BUF B_25 ( .A(N213), .Z(N25) );
  GTECH_BUF B_26 ( .A(N215), .Z(N26) );
  GTECH_BUF B_27 ( .A(N219), .Z(N27) );
  GTECH_BUF B_28 ( .A(N221), .Z(N28) );
  GTECH_BUF B_29 ( .A(N226), .Z(N29) );
  SELECT_OP C1737 ( .DATA1(dtmp), .DATA2(dtmp), .DATA3({1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0}), .DATA4({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0}), .DATA5(ep0_dout), .DATA6(ep1_dout), .DATA7(ep2_dout), 
        .DATA8(ep3_dout), .DATA9(ep4_dout), .DATA10(ep5_dout), .DATA11(
        ep6_dout), .DATA12(ep7_dout), .DATA13(ep8_dout), .DATA14(ep9_dout), 
        .DATA15(ep10_dout), .DATA16(ep11_dout), .DATA17(ep12_dout), .DATA18(
        ep13_dout), .DATA19(ep14_dout), .DATA20(ep15_dout), .CONTROL1(N10), 
        .CONTROL2(N11), .CONTROL3(N12), .CONTROL4(N13), .CONTROL5(N14), 
        .CONTROL6(N15), .CONTROL7(N16), .CONTROL8(N17), .CONTROL9(N18), 
        .CONTROL10(N19), .CONTROL11(N20), .CONTROL12(N21), .CONTROL13(N22), 
        .CONTROL14(N23), .CONTROL15(N24), .CONTROL16(N25), .CONTROL17(N26), 
        .CONTROL18(N27), .CONTROL19(N28), .CONTROL20(N29), .Z({N279, N278, 
        N277, N276, N275, N274, N273, N272, N271, N270, N269, N268, N267, N266, 
        N265, N264, N263, N262, N261, N260, N259, N258, N257, N256, N255, N254, 
        N253, N252, N251, N250, N249, N248}) );
  SELECT_OP C1738 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b1), 
        .DATA5(1'b1), .DATA6(1'b1), .DATA7(1'b1), .DATA8(1'b1), .DATA9(1'b1), 
        .DATA10(1'b1), .DATA11(1'b1), .DATA12(1'b1), .DATA13(1'b1), .DATA14(
        1'b1), .DATA15(1'b1), .DATA16(1'b1), .DATA17(1'b0), .CONTROL1(N30), 
        .CONTROL2(N331), .CONTROL3(N334), .CONTROL4(N337), .CONTROL5(N340), 
        .CONTROL6(N343), .CONTROL7(N346), .CONTROL8(N349), .CONTROL9(N352), 
        .CONTROL10(N355), .CONTROL11(N358), .CONTROL12(N361), .CONTROL13(N364), 
        .CONTROL14(N367), .CONTROL15(N370), .CONTROL16(N373), .CONTROL17(N296), 
        .Z(N297) );
  GTECH_BUF B_30 ( .A(ep0_match), .Z(N30) );
  SELECT_OP C1739 ( .DATA1(ep0_csr), .DATA2(ep1_csr), .DATA3(ep2_csr), .DATA4(
        ep3_csr), .DATA5(ep4_csr), .DATA6(ep5_csr), .DATA7(ep6_csr), .DATA8(
        ep7_csr), .DATA9(ep8_csr), .DATA10(ep9_csr), .DATA11(ep10_csr), 
        .DATA12(ep11_csr), .DATA13(ep12_csr), .DATA14(ep13_csr), .DATA15(
        ep14_csr), .DATA16(ep15_csr), .CONTROL1(N30), .CONTROL2(N331), 
        .CONTROL3(N334), .CONTROL4(N337), .CONTROL5(N340), .CONTROL6(N343), 
        .CONTROL7(N346), .CONTROL8(N349), .CONTROL9(N352), .CONTROL10(N355), 
        .CONTROL11(N358), .CONTROL12(N361), .CONTROL13(N364), .CONTROL14(N367), 
        .CONTROL15(N370), .CONTROL16(N373), .Z({N329, N328, N327, N326, N325, 
        N324, N323, N322, N321, N320, N319, N318, N317, N316, N315, N314, N313, 
        N312, N311, N310, N309, N308, N307, N306, N305, N304, N303, N302, N301, 
        N300, N299, N298}) );
  SELECT_OP C1740 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b1), 
        .DATA5(1'b1), .DATA6(1'b1), .DATA7(1'b1), .DATA8(1'b1), .DATA9(1'b1), 
        .DATA10(1'b1), .DATA11(1'b1), .DATA12(1'b1), .DATA13(1'b1), .DATA14(
        1'b1), .DATA15(1'b1), .DATA16(1'b1), .DATA17(1'b0), .CONTROL1(N30), 
        .CONTROL2(N423), .CONTROL3(N425), .CONTROL4(N427), .CONTROL5(N429), 
        .CONTROL6(N431), .CONTROL7(N433), .CONTROL8(N435), .CONTROL9(N437), 
        .CONTROL10(N439), .CONTROL11(N441), .CONTROL12(N443), .CONTROL13(N445), 
        .CONTROL14(N447), .CONTROL15(N449), .CONTROL16(N451), .CONTROL17(N389), 
        .Z(N390) );
  SELECT_OP C1741 ( .DATA1(ep0_buf0), .DATA2(ep1_buf0), .DATA3(ep2_buf0), 
        .DATA4(ep3_buf0), .DATA5(ep4_buf0), .DATA6(ep5_buf0), .DATA7(ep6_buf0), 
        .DATA8(ep7_buf0), .DATA9(ep8_buf0), .DATA10(ep9_buf0), .DATA11(
        ep10_buf0), .DATA12(ep11_buf0), .DATA13(ep12_buf0), .DATA14(ep13_buf0), 
        .DATA15(ep14_buf0), .DATA16(ep15_buf0), .CONTROL1(N30), .CONTROL2(N423), .CONTROL3(N425), .CONTROL4(N427), .CONTROL5(N429), .CONTROL6(N431), 
        .CONTROL7(N433), .CONTROL8(N435), .CONTROL9(N437), .CONTROL10(N439), 
        .CONTROL11(N441), .CONTROL12(N443), .CONTROL13(N445), .CONTROL14(N447), 
        .CONTROL15(N449), .CONTROL16(N451), .Z({N422, N421, N420, N419, N418, 
        N417, N416, N415, N414, N413, N412, N411, N410, N409, N408, N407, N406, 
        N405, N404, N403, N402, N401, N400, N399, N398, N397, N396, N395, N394, 
        N393, N392, N391}) );
  SELECT_OP C1742 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b1), 
        .DATA5(1'b1), .DATA6(1'b1), .DATA7(1'b1), .DATA8(1'b1), .DATA9(1'b1), 
        .DATA10(1'b1), .DATA11(1'b1), .DATA12(1'b1), .DATA13(1'b1), .DATA14(
        1'b1), .DATA15(1'b1), .DATA16(1'b1), .DATA17(1'b0), .CONTROL1(N30), 
        .CONTROL2(N501), .CONTROL3(N503), .CONTROL4(N505), .CONTROL5(N507), 
        .CONTROL6(N509), .CONTROL7(N511), .CONTROL8(N513), .CONTROL9(N515), 
        .CONTROL10(N517), .CONTROL11(N519), .CONTROL12(N521), .CONTROL13(N523), 
        .CONTROL14(N525), .CONTROL15(N527), .CONTROL16(N529), .CONTROL17(N467), 
        .Z(N468) );
  SELECT_OP C1743 ( .DATA1(ep0_buf1), .DATA2(ep1_buf1), .DATA3(ep2_buf1), 
        .DATA4(ep3_buf1), .DATA5(ep4_buf1), .DATA6(ep5_buf1), .DATA7(ep6_buf1), 
        .DATA8(ep7_buf1), .DATA9(ep8_buf1), .DATA10(ep9_buf1), .DATA11(
        ep10_buf1), .DATA12(ep11_buf1), .DATA13(ep12_buf1), .DATA14(ep13_buf1), 
        .DATA15(ep14_buf1), .DATA16(ep15_buf1), .CONTROL1(N30), .CONTROL2(N501), .CONTROL3(N503), .CONTROL4(N505), .CONTROL5(N507), .CONTROL6(N509), 
        .CONTROL7(N511), .CONTROL8(N513), .CONTROL9(N515), .CONTROL10(N517), 
        .CONTROL11(N519), .CONTROL12(N521), .CONTROL13(N523), .CONTROL14(N525), 
        .CONTROL15(N527), .CONTROL16(N529), .Z({N500, N499, N498, N497, N496, 
        N495, N494, N493, N492, N491, N490, N489, N488, N487, N486, N485, N484, 
        N483, N482, N481, N480, N479, N478, N477, N476, N475, N474, N473, N472, 
        N471, N470, N469}) );
  SELECT_OP C1744 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b1), 
        .DATA5(1'b1), .DATA6(1'b1), .DATA7(1'b1), .DATA8(1'b1), .DATA9(1'b1), 
        .DATA10(1'b1), .DATA11(1'b1), .DATA12(1'b1), .DATA13(1'b1), .DATA14(
        1'b1), .DATA15(1'b1), .DATA16(1'b1), .DATA17(1'b0), .CONTROL1(N30), 
        .CONTROL2(N548), .CONTROL3(N550), .CONTROL4(N552), .CONTROL5(N554), 
        .CONTROL6(N556), .CONTROL7(N558), .CONTROL8(N560), .CONTROL9(N562), 
        .CONTROL10(N564), .CONTROL11(N566), .CONTROL12(N568), .CONTROL13(N570), 
        .CONTROL14(N572), .CONTROL15(N574), .CONTROL16(N576), .CONTROL17(N545), 
        .Z(N546) );
  SELECT_OP C1745 ( .DATA1(ep0_dma_in_buf_sz1), .DATA2(ep1_dma_in_buf_sz1), 
        .DATA3(ep2_dma_in_buf_sz1), .DATA4(ep3_dma_in_buf_sz1), .DATA5(
        ep4_dma_in_buf_sz1), .DATA6(ep5_dma_in_buf_sz1), .DATA7(
        ep6_dma_in_buf_sz1), .DATA8(ep7_dma_in_buf_sz1), .DATA9(
        ep8_dma_in_buf_sz1), .DATA10(ep9_dma_in_buf_sz1), .DATA11(
        ep10_dma_in_buf_sz1), .DATA12(ep11_dma_in_buf_sz1), .DATA13(
        ep12_dma_in_buf_sz1), .DATA14(ep13_dma_in_buf_sz1), .DATA15(
        ep14_dma_in_buf_sz1), .DATA16(ep15_dma_in_buf_sz1), .CONTROL1(N30), 
        .CONTROL2(N548), .CONTROL3(N550), .CONTROL4(N552), .CONTROL5(N554), 
        .CONTROL6(N556), .CONTROL7(N558), .CONTROL8(N560), .CONTROL9(N562), 
        .CONTROL10(N564), .CONTROL11(N566), .CONTROL12(N568), .CONTROL13(N570), 
        .CONTROL14(N572), .CONTROL15(N574), .CONTROL16(N576), .Z(N547) );
  SELECT_OP C1746 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b1), 
        .DATA5(1'b1), .DATA6(1'b1), .DATA7(1'b1), .DATA8(1'b1), .DATA9(1'b1), 
        .DATA10(1'b1), .DATA11(1'b1), .DATA12(1'b1), .DATA13(1'b1), .DATA14(
        1'b1), .DATA15(1'b1), .DATA16(1'b1), .DATA17(1'b0), .CONTROL1(N30), 
        .CONTROL2(N595), .CONTROL3(N597), .CONTROL4(N599), .CONTROL5(N601), 
        .CONTROL6(N603), .CONTROL7(N605), .CONTROL8(N607), .CONTROL9(N609), 
        .CONTROL10(N611), .CONTROL11(N613), .CONTROL12(N615), .CONTROL13(N617), 
        .CONTROL14(N619), .CONTROL15(N621), .CONTROL16(N623), .CONTROL17(N592), 
        .Z(N593) );
  SELECT_OP C1747 ( .DATA1(ep0_dma_out_buf_avail), .DATA2(
        ep1_dma_out_buf_avail), .DATA3(ep2_dma_out_buf_avail), .DATA4(
        ep3_dma_out_buf_avail), .DATA5(ep4_dma_out_buf_avail), .DATA6(
        ep5_dma_out_buf_avail), .DATA7(ep6_dma_out_buf_avail), .DATA8(
        ep7_dma_out_buf_avail), .DATA9(ep8_dma_out_buf_avail), .DATA10(
        ep9_dma_out_buf_avail), .DATA11(ep10_dma_out_buf_avail), .DATA12(
        ep11_dma_out_buf_avail), .DATA13(ep12_dma_out_buf_avail), .DATA14(
        ep13_dma_out_buf_avail), .DATA15(ep14_dma_out_buf_avail), .DATA16(
        ep15_dma_out_buf_avail), .CONTROL1(N30), .CONTROL2(N595), .CONTROL3(
        N597), .CONTROL4(N599), .CONTROL5(N601), .CONTROL6(N603), .CONTROL7(
        N605), .CONTROL8(N607), .CONTROL9(N609), .CONTROL10(N611), .CONTROL11(
        N613), .CONTROL12(N615), .CONTROL13(N617), .CONTROL14(N619), 
        .CONTROL15(N621), .CONTROL16(N623), .Z(N594) );
  SELECT_OP C1748 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N31), .CONTROL2(N631), .CONTROL3(N634), .CONTROL4(N627), .Z(
        N628) );
  GTECH_BUF B_31 ( .A(N624), .Z(N31) );
  SELECT_OP C1749 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b1), .CONTROL1(N31), 
        .CONTROL2(N631), .CONTROL3(N634), .Z(N629) );
  SELECT_OP C1750 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N32), .CONTROL2(N642), .CONTROL3(N644), .CONTROL4(N638), .Z(
        N639) );
  GTECH_BUF B_32 ( .A(N635), .Z(N32) );
  SELECT_OP C1751 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b1), .CONTROL1(N32), 
        .CONTROL2(N642), .CONTROL3(N644), .Z(N640) );
  SELECT_OP C1752 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N33), .CONTROL2(N652), .CONTROL3(N654), .CONTROL4(N648), .Z(
        N649) );
  GTECH_BUF B_33 ( .A(N645), .Z(N33) );
  SELECT_OP C1753 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b1), .CONTROL1(N33), 
        .CONTROL2(N652), .CONTROL3(N654), .Z(N650) );
  SELECT_OP C1754 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N34), .CONTROL2(N662), .CONTROL3(N664), .CONTROL4(N658), .Z(
        N659) );
  GTECH_BUF B_34 ( .A(N655), .Z(N34) );
  SELECT_OP C1755 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b1), .CONTROL1(N34), 
        .CONTROL2(N662), .CONTROL3(N664), .Z(N660) );
  SELECT_OP C1756 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N35), .CONTROL2(N672), .CONTROL3(N674), .CONTROL4(N668), .Z(
        N669) );
  GTECH_BUF B_35 ( .A(N665), .Z(N35) );
  SELECT_OP C1757 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b1), .CONTROL1(N35), 
        .CONTROL2(N672), .CONTROL3(N674), .Z(N670) );
  SELECT_OP C1758 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N36), .CONTROL2(N682), .CONTROL3(N684), .CONTROL4(N678), .Z(
        N679) );
  GTECH_BUF B_36 ( .A(N675), .Z(N36) );
  SELECT_OP C1759 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b1), .CONTROL1(N36), 
        .CONTROL2(N682), .CONTROL3(N684), .Z(N680) );
  SELECT_OP C1760 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N37), .CONTROL2(N692), .CONTROL3(N694), .CONTROL4(N688), .Z(
        N689) );
  GTECH_BUF B_37 ( .A(N685), .Z(N37) );
  SELECT_OP C1761 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b1), .CONTROL1(N37), 
        .CONTROL2(N692), .CONTROL3(N694), .Z(N690) );
  SELECT_OP C1762 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N38), .CONTROL2(N702), .CONTROL3(N704), .CONTROL4(N698), .Z(
        N699) );
  GTECH_BUF B_38 ( .A(N695), .Z(N38) );
  SELECT_OP C1763 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b1), .CONTROL1(N38), 
        .CONTROL2(N702), .CONTROL3(N704), .Z(N700) );
  SELECT_OP C1764 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N39), .CONTROL2(N712), .CONTROL3(N714), .CONTROL4(N708), .Z(
        N709) );
  GTECH_BUF B_39 ( .A(N705), .Z(N39) );
  SELECT_OP C1765 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b1), .CONTROL1(N39), 
        .CONTROL2(N712), .CONTROL3(N714), .Z(N710) );
  GTECH_NOT I_49 ( .A(adr[1]), .Z(N40) );
  GTECH_NOT I_50 ( .A(adr[0]), .Z(N41) );
  GTECH_NOT I_51 ( .A(adr[0]), .Z(N44) );
  GTECH_NOT I_52 ( .A(N46), .Z(N47) );
  GTECH_NOT I_53 ( .A(adr[1]), .Z(N48) );
  GTECH_NOT I_54 ( .A(N50), .Z(N51) );
  GTECH_NOT I_55 ( .A(adr[1]), .Z(N52) );
  GTECH_NOT I_56 ( .A(adr[0]), .Z(N53) );
  GTECH_NOT I_57 ( .A(N55), .Z(N56) );
  GTECH_NOT I_58 ( .A(N58), .Z(N59) );
  GTECH_NOT I_59 ( .A(adr[0]), .Z(N60) );
  GTECH_NOT I_60 ( .A(N62), .Z(N63) );
  GTECH_OR2 C1786 ( .A(N47), .B(N43), .Z(N64) );
  GTECH_OR2 C1787 ( .A(N51), .B(N64), .Z(N65) );
  GTECH_OR2 C1788 ( .A(N56), .B(N65), .Z(N66) );
  GTECH_OR2 C1789 ( .A(N59), .B(N66), .Z(N67) );
  GTECH_OR2 C1790 ( .A(N63), .B(N67), .Z(N68) );
  GTECH_NOT I_61 ( .A(N68), .Z(N69) );
  GTECH_AND2 C1792 ( .A(N897), .B(re), .Z(N103) );
  GTECH_NOT I_62 ( .A(rst), .Z(N104) );
  GTECH_AND2 C1794 ( .A(N915), .B(we), .Z(N105) );
  GTECH_OR2 C1798 ( .A(N105), .B(N104), .Z(N106) );
  GTECH_OR2 C1799 ( .A(utmi_vend_wr), .B(N106), .Z(N107) );
  GTECH_NOT I_63 ( .A(N107), .Z(N108) );
  GTECH_NOT I_64 ( .A(N104), .Z(N111) );
  GTECH_AND2 C1802 ( .A(N105), .B(N111), .Z(N112) );
  GTECH_NOT I_65 ( .A(N105), .Z(N113) );
  GTECH_AND2 C1804 ( .A(N111), .B(N113), .Z(N114) );
  GTECH_AND2 C1805 ( .A(utmi_vend_wr), .B(N114), .Z(N115) );
  GTECH_AND2 C1806 ( .A(N903), .B(we), .Z(N116) );
  GTECH_NOT I_66 ( .A(rst), .Z(N117) );
  GTECH_AND2 C1809 ( .A(N918), .B(we), .Z(N118) );
  GTECH_OR2 C1813 ( .A(N118), .B(N117), .Z(N119) );
  GTECH_OR2 C1814 ( .A(rf_resume_req), .B(N119), .Z(N120) );
  GTECH_NOT I_67 ( .A(N120), .Z(N121) );
  GTECH_NOT I_68 ( .A(N117), .Z(N124) );
  GTECH_AND2 C1817 ( .A(N118), .B(N124), .Z(N125) );
  GTECH_NOT I_69 ( .A(N118), .Z(N126) );
  GTECH_AND2 C1819 ( .A(N124), .B(N126), .Z(N127) );
  GTECH_AND2 C1820 ( .A(rf_resume_req), .B(N127), .Z(N128) );
  GTECH_NOT I_70 ( .A(rst), .Z(N129) );
  GTECH_AND2 C1822 ( .A(N907), .B(we), .Z(N130) );
  GTECH_OR2 C1825 ( .A(N130), .B(N129), .Z(N131) );
  GTECH_NOT I_71 ( .A(N131), .Z(N132) );
  GTECH_NOT I_72 ( .A(N129), .Z(N141) );
  GTECH_AND2 C1828 ( .A(N130), .B(N141), .Z(N142) );
  GTECH_NOT I_73 ( .A(rst), .Z(N143) );
  GTECH_AND2 C1830 ( .A(N911), .B(we), .Z(N144) );
  GTECH_OR2 C1833 ( .A(N144), .B(N143), .Z(N145) );
  GTECH_NOT I_74 ( .A(N145), .Z(N146) );
  GTECH_NOT I_75 ( .A(N143), .Z(N166) );
  GTECH_AND2 C1836 ( .A(N144), .B(N166), .Z(N167) );
  GTECH_NOT I_76 ( .A(N173), .Z(N174) );
  GTECH_NOT I_77 ( .A(N175), .Z(N176) );
  GTECH_NOT I_78 ( .A(N178), .Z(N179) );
  GTECH_NOT I_79 ( .A(N180), .Z(N181) );
  GTECH_NOT I_80 ( .A(N183), .Z(N184) );
  GTECH_NOT I_81 ( .A(N185), .Z(N186) );
  GTECH_NOT I_82 ( .A(N189), .Z(N190) );
  GTECH_NOT I_83 ( .A(N191), .Z(N192) );
  GTECH_NOT I_84 ( .A(N194), .Z(N195) );
  GTECH_NOT I_85 ( .A(N196), .Z(N197) );
  GTECH_NOT I_86 ( .A(N199), .Z(N200) );
  GTECH_NOT I_87 ( .A(N201), .Z(N202) );
  GTECH_NOT I_88 ( .A(N205), .Z(N206) );
  GTECH_NOT I_89 ( .A(N207), .Z(N208) );
  GTECH_NOT I_90 ( .A(N212), .Z(N213) );
  GTECH_NOT I_91 ( .A(N214), .Z(N215) );
  GTECH_NOT I_92 ( .A(N218), .Z(N219) );
  GTECH_NOT I_93 ( .A(N220), .Z(N221) );
  GTECH_NOT I_94 ( .A(N225), .Z(N226) );
  GTECH_OR2 C1881 ( .A(N174), .B(N171), .Z(N227) );
  GTECH_OR2 C1882 ( .A(N176), .B(N227), .Z(N228) );
  GTECH_OR2 C1883 ( .A(N179), .B(N228), .Z(N229) );
  GTECH_OR2 C1884 ( .A(N181), .B(N229), .Z(N230) );
  GTECH_OR2 C1885 ( .A(N184), .B(N230), .Z(N231) );
  GTECH_OR2 C1886 ( .A(N186), .B(N231), .Z(N232) );
  GTECH_OR2 C1887 ( .A(N190), .B(N232), .Z(N233) );
  GTECH_OR2 C1888 ( .A(N192), .B(N233), .Z(N234) );
  GTECH_OR2 C1889 ( .A(N195), .B(N234), .Z(N235) );
  GTECH_OR2 C1890 ( .A(N197), .B(N235), .Z(N236) );
  GTECH_OR2 C1891 ( .A(N200), .B(N236), .Z(N237) );
  GTECH_OR2 C1892 ( .A(N202), .B(N237), .Z(N238) );
  GTECH_OR2 C1893 ( .A(N206), .B(N238), .Z(N239) );
  GTECH_OR2 C1894 ( .A(N208), .B(N239), .Z(N240) );
  GTECH_OR2 C1895 ( .A(N213), .B(N240), .Z(N241) );
  GTECH_OR2 C1896 ( .A(N215), .B(N241), .Z(N242) );
  GTECH_OR2 C1897 ( .A(N219), .B(N242), .Z(N243) );
  GTECH_OR2 C1898 ( .A(N221), .B(N243), .Z(N244) );
  GTECH_OR2 C1899 ( .A(N226), .B(N244), .Z(N245) );
  GTECH_NOT I_95 ( .A(N245), .Z(N246) );
  GTECH_AND2 C1901 ( .A(N737), .B(re), .Z(ep0_re) );
  GTECH_AND2 C1902 ( .A(N745), .B(re), .Z(ep1_re) );
  GTECH_AND2 C1903 ( .A(N754), .B(re), .Z(ep2_re) );
  GTECH_AND2 C1904 ( .A(N762), .B(re), .Z(ep3_re) );
  GTECH_AND2 C1905 ( .A(N772), .B(re), .Z(ep4_re) );
  GTECH_AND2 C1906 ( .A(N782), .B(re), .Z(ep5_re) );
  GTECH_AND2 C1907 ( .A(N792), .B(re), .Z(ep6_re) );
  GTECH_AND2 C1908 ( .A(N802), .B(re), .Z(ep7_re) );
  GTECH_AND2 C1909 ( .A(N812), .B(re), .Z(ep8_re) );
  GTECH_AND2 C1910 ( .A(N822), .B(re), .Z(ep9_re) );
  GTECH_AND2 C1911 ( .A(N832), .B(re), .Z(ep10_re) );
  GTECH_AND2 C1912 ( .A(N842), .B(re), .Z(ep11_re) );
  GTECH_AND2 C1913 ( .A(N853), .B(re), .Z(ep12_re) );
  GTECH_AND2 C1914 ( .A(N863), .B(re), .Z(ep13_re) );
  GTECH_AND2 C1915 ( .A(N873), .B(re), .Z(ep14_re) );
  GTECH_AND2 C1916 ( .A(N883), .B(re), .Z(ep15_re) );
  GTECH_AND2 C1917 ( .A(N741), .B(we), .Z(ep0_we) );
  GTECH_AND2 C1918 ( .A(N749), .B(we), .Z(ep1_we) );
  GTECH_AND2 C1919 ( .A(N758), .B(we), .Z(ep2_we) );
  GTECH_AND2 C1920 ( .A(N766), .B(we), .Z(ep3_we) );
  GTECH_AND2 C1921 ( .A(N777), .B(we), .Z(ep4_we) );
  GTECH_AND2 C1922 ( .A(N787), .B(we), .Z(ep5_we) );
  GTECH_AND2 C1923 ( .A(N797), .B(we), .Z(ep6_we) );
  GTECH_AND2 C1924 ( .A(N807), .B(we), .Z(ep7_we) );
  GTECH_AND2 C1925 ( .A(N817), .B(we), .Z(ep8_we) );
  GTECH_AND2 C1926 ( .A(N827), .B(we), .Z(ep9_we) );
  GTECH_AND2 C1927 ( .A(N837), .B(we), .Z(ep10_we) );
  GTECH_AND2 C1928 ( .A(N847), .B(we), .Z(ep11_we) );
  GTECH_AND2 C1929 ( .A(N858), .B(we), .Z(ep12_we) );
  GTECH_AND2 C1930 ( .A(N868), .B(we), .Z(ep13_we) );
  GTECH_AND2 C1931 ( .A(N878), .B(we), .Z(ep14_we) );
  GTECH_AND2 C1932 ( .A(N888), .B(we), .Z(ep15_we) );
  GTECH_OR2 C1933 ( .A(N932), .B(ep15_match), .Z(N280) );
  GTECH_OR2 C1934 ( .A(N931), .B(ep14_match), .Z(N932) );
  GTECH_OR2 C1935 ( .A(N930), .B(ep13_match), .Z(N931) );
  GTECH_OR2 C1936 ( .A(N929), .B(ep12_match), .Z(N930) );
  GTECH_OR2 C1937 ( .A(N928), .B(ep11_match), .Z(N929) );
  GTECH_OR2 C1938 ( .A(N927), .B(ep10_match), .Z(N928) );
  GTECH_OR2 C1939 ( .A(N926), .B(ep9_match), .Z(N927) );
  GTECH_OR2 C1940 ( .A(N925), .B(ep8_match), .Z(N926) );
  GTECH_OR2 C1941 ( .A(N924), .B(ep7_match), .Z(N925) );
  GTECH_OR2 C1942 ( .A(N923), .B(ep6_match), .Z(N924) );
  GTECH_OR2 C1943 ( .A(N922), .B(ep5_match), .Z(N923) );
  GTECH_OR2 C1944 ( .A(N921), .B(ep4_match), .Z(N922) );
  GTECH_OR2 C1945 ( .A(N920), .B(ep3_match), .Z(N921) );
  GTECH_OR2 C1946 ( .A(N919), .B(ep2_match), .Z(N920) );
  GTECH_OR2 C1947 ( .A(ep0_match), .B(ep1_match), .Z(N919) );
  GTECH_OR2 C1964 ( .A(ep1_match), .B(ep0_match), .Z(N281) );
  GTECH_OR2 C1965 ( .A(ep2_match), .B(N281), .Z(N282) );
  GTECH_OR2 C1966 ( .A(ep3_match), .B(N282), .Z(N283) );
  GTECH_OR2 C1967 ( .A(ep4_match), .B(N283), .Z(N284) );
  GTECH_OR2 C1968 ( .A(ep5_match), .B(N284), .Z(N285) );
  GTECH_OR2 C1969 ( .A(ep6_match), .B(N285), .Z(N286) );
  GTECH_OR2 C1970 ( .A(ep7_match), .B(N286), .Z(N287) );
  GTECH_OR2 C1971 ( .A(ep8_match), .B(N287), .Z(N288) );
  GTECH_OR2 C1972 ( .A(ep9_match), .B(N288), .Z(N289) );
  GTECH_OR2 C1973 ( .A(ep10_match), .B(N289), .Z(N290) );
  GTECH_OR2 C1974 ( .A(ep11_match), .B(N290), .Z(N291) );
  GTECH_OR2 C1975 ( .A(ep12_match), .B(N291), .Z(N292) );
  GTECH_OR2 C1976 ( .A(ep13_match), .B(N292), .Z(N293) );
  GTECH_OR2 C1977 ( .A(ep14_match), .B(N293), .Z(N294) );
  GTECH_OR2 C1978 ( .A(ep15_match), .B(N294), .Z(N295) );
  GTECH_NOT I_96 ( .A(N295), .Z(N296) );
  GTECH_NOT I_97 ( .A(ep0_match), .Z(N330) );
  GTECH_AND2 C1981 ( .A(ep1_match), .B(N330), .Z(N331) );
  GTECH_NOT I_98 ( .A(ep1_match), .Z(N332) );
  GTECH_AND2 C1983 ( .A(N330), .B(N332), .Z(N333) );
  GTECH_AND2 C1984 ( .A(ep2_match), .B(N333), .Z(N334) );
  GTECH_NOT I_99 ( .A(ep2_match), .Z(N335) );
  GTECH_AND2 C1986 ( .A(N333), .B(N335), .Z(N336) );
  GTECH_AND2 C1987 ( .A(ep3_match), .B(N336), .Z(N337) );
  GTECH_NOT I_100 ( .A(ep3_match), .Z(N338) );
  GTECH_AND2 C1989 ( .A(N336), .B(N338), .Z(N339) );
  GTECH_AND2 C1990 ( .A(ep4_match), .B(N339), .Z(N340) );
  GTECH_NOT I_101 ( .A(ep4_match), .Z(N341) );
  GTECH_AND2 C1992 ( .A(N339), .B(N341), .Z(N342) );
  GTECH_AND2 C1993 ( .A(ep5_match), .B(N342), .Z(N343) );
  GTECH_NOT I_102 ( .A(ep5_match), .Z(N344) );
  GTECH_AND2 C1995 ( .A(N342), .B(N344), .Z(N345) );
  GTECH_AND2 C1996 ( .A(ep6_match), .B(N345), .Z(N346) );
  GTECH_NOT I_103 ( .A(ep6_match), .Z(N347) );
  GTECH_AND2 C1998 ( .A(N345), .B(N347), .Z(N348) );
  GTECH_AND2 C1999 ( .A(ep7_match), .B(N348), .Z(N349) );
  GTECH_NOT I_104 ( .A(ep7_match), .Z(N350) );
  GTECH_AND2 C2001 ( .A(N348), .B(N350), .Z(N351) );
  GTECH_AND2 C2002 ( .A(ep8_match), .B(N351), .Z(N352) );
  GTECH_NOT I_105 ( .A(ep8_match), .Z(N353) );
  GTECH_AND2 C2004 ( .A(N351), .B(N353), .Z(N354) );
  GTECH_AND2 C2005 ( .A(ep9_match), .B(N354), .Z(N355) );
  GTECH_NOT I_106 ( .A(ep9_match), .Z(N356) );
  GTECH_AND2 C2007 ( .A(N354), .B(N356), .Z(N357) );
  GTECH_AND2 C2008 ( .A(ep10_match), .B(N357), .Z(N358) );
  GTECH_NOT I_107 ( .A(ep10_match), .Z(N359) );
  GTECH_AND2 C2010 ( .A(N357), .B(N359), .Z(N360) );
  GTECH_AND2 C2011 ( .A(ep11_match), .B(N360), .Z(N361) );
  GTECH_NOT I_108 ( .A(ep11_match), .Z(N362) );
  GTECH_AND2 C2013 ( .A(N360), .B(N362), .Z(N363) );
  GTECH_AND2 C2014 ( .A(ep12_match), .B(N363), .Z(N364) );
  GTECH_NOT I_109 ( .A(ep12_match), .Z(N365) );
  GTECH_AND2 C2016 ( .A(N363), .B(N365), .Z(N366) );
  GTECH_AND2 C2017 ( .A(ep13_match), .B(N366), .Z(N367) );
  GTECH_NOT I_110 ( .A(ep13_match), .Z(N368) );
  GTECH_AND2 C2019 ( .A(N366), .B(N368), .Z(N369) );
  GTECH_AND2 C2020 ( .A(ep14_match), .B(N369), .Z(N370) );
  GTECH_NOT I_111 ( .A(ep14_match), .Z(N371) );
  GTECH_AND2 C2022 ( .A(N369), .B(N371), .Z(N372) );
  GTECH_AND2 C2023 ( .A(ep15_match), .B(N372), .Z(N373) );
  GTECH_OR2 C2040 ( .A(ep1_match), .B(ep0_match), .Z(N374) );
  GTECH_OR2 C2041 ( .A(ep2_match), .B(N374), .Z(N375) );
  GTECH_OR2 C2042 ( .A(ep3_match), .B(N375), .Z(N376) );
  GTECH_OR2 C2043 ( .A(ep4_match), .B(N376), .Z(N377) );
  GTECH_OR2 C2044 ( .A(ep5_match), .B(N377), .Z(N378) );
  GTECH_OR2 C2045 ( .A(ep6_match), .B(N378), .Z(N379) );
  GTECH_OR2 C2046 ( .A(ep7_match), .B(N379), .Z(N380) );
  GTECH_OR2 C2047 ( .A(ep8_match), .B(N380), .Z(N381) );
  GTECH_OR2 C2048 ( .A(ep9_match), .B(N381), .Z(N382) );
  GTECH_OR2 C2049 ( .A(ep10_match), .B(N382), .Z(N383) );
  GTECH_OR2 C2050 ( .A(ep11_match), .B(N383), .Z(N384) );
  GTECH_OR2 C2051 ( .A(ep12_match), .B(N384), .Z(N385) );
  GTECH_OR2 C2052 ( .A(ep13_match), .B(N385), .Z(N386) );
  GTECH_OR2 C2053 ( .A(ep14_match), .B(N386), .Z(N387) );
  GTECH_OR2 C2054 ( .A(ep15_match), .B(N387), .Z(N388) );
  GTECH_NOT I_112 ( .A(N388), .Z(N389) );
  GTECH_AND2 C2057 ( .A(ep1_match), .B(N330), .Z(N423) );
  GTECH_AND2 C2059 ( .A(N330), .B(N332), .Z(N424) );
  GTECH_AND2 C2060 ( .A(ep2_match), .B(N424), .Z(N425) );
  GTECH_AND2 C2062 ( .A(N424), .B(N335), .Z(N426) );
  GTECH_AND2 C2063 ( .A(ep3_match), .B(N426), .Z(N427) );
  GTECH_AND2 C2065 ( .A(N426), .B(N338), .Z(N428) );
  GTECH_AND2 C2066 ( .A(ep4_match), .B(N428), .Z(N429) );
  GTECH_AND2 C2068 ( .A(N428), .B(N341), .Z(N430) );
  GTECH_AND2 C2069 ( .A(ep5_match), .B(N430), .Z(N431) );
  GTECH_AND2 C2071 ( .A(N430), .B(N344), .Z(N432) );
  GTECH_AND2 C2072 ( .A(ep6_match), .B(N432), .Z(N433) );
  GTECH_AND2 C2074 ( .A(N432), .B(N347), .Z(N434) );
  GTECH_AND2 C2075 ( .A(ep7_match), .B(N434), .Z(N435) );
  GTECH_AND2 C2077 ( .A(N434), .B(N350), .Z(N436) );
  GTECH_AND2 C2078 ( .A(ep8_match), .B(N436), .Z(N437) );
  GTECH_AND2 C2080 ( .A(N436), .B(N353), .Z(N438) );
  GTECH_AND2 C2081 ( .A(ep9_match), .B(N438), .Z(N439) );
  GTECH_AND2 C2083 ( .A(N438), .B(N356), .Z(N440) );
  GTECH_AND2 C2084 ( .A(ep10_match), .B(N440), .Z(N441) );
  GTECH_AND2 C2086 ( .A(N440), .B(N359), .Z(N442) );
  GTECH_AND2 C2087 ( .A(ep11_match), .B(N442), .Z(N443) );
  GTECH_AND2 C2089 ( .A(N442), .B(N362), .Z(N444) );
  GTECH_AND2 C2090 ( .A(ep12_match), .B(N444), .Z(N445) );
  GTECH_AND2 C2092 ( .A(N444), .B(N365), .Z(N446) );
  GTECH_AND2 C2093 ( .A(ep13_match), .B(N446), .Z(N447) );
  GTECH_AND2 C2095 ( .A(N446), .B(N368), .Z(N448) );
  GTECH_AND2 C2096 ( .A(ep14_match), .B(N448), .Z(N449) );
  GTECH_AND2 C2098 ( .A(N448), .B(N371), .Z(N450) );
  GTECH_AND2 C2099 ( .A(ep15_match), .B(N450), .Z(N451) );
  GTECH_OR2 C2116 ( .A(ep1_match), .B(ep0_match), .Z(N452) );
  GTECH_OR2 C2117 ( .A(ep2_match), .B(N452), .Z(N453) );
  GTECH_OR2 C2118 ( .A(ep3_match), .B(N453), .Z(N454) );
  GTECH_OR2 C2119 ( .A(ep4_match), .B(N454), .Z(N455) );
  GTECH_OR2 C2120 ( .A(ep5_match), .B(N455), .Z(N456) );
  GTECH_OR2 C2121 ( .A(ep6_match), .B(N456), .Z(N457) );
  GTECH_OR2 C2122 ( .A(ep7_match), .B(N457), .Z(N458) );
  GTECH_OR2 C2123 ( .A(ep8_match), .B(N458), .Z(N459) );
  GTECH_OR2 C2124 ( .A(ep9_match), .B(N459), .Z(N460) );
  GTECH_OR2 C2125 ( .A(ep10_match), .B(N460), .Z(N461) );
  GTECH_OR2 C2126 ( .A(ep11_match), .B(N461), .Z(N462) );
  GTECH_OR2 C2127 ( .A(ep12_match), .B(N462), .Z(N463) );
  GTECH_OR2 C2128 ( .A(ep13_match), .B(N463), .Z(N464) );
  GTECH_OR2 C2129 ( .A(ep14_match), .B(N464), .Z(N465) );
  GTECH_OR2 C2130 ( .A(ep15_match), .B(N465), .Z(N466) );
  GTECH_NOT I_113 ( .A(N466), .Z(N467) );
  GTECH_AND2 C2133 ( .A(ep1_match), .B(N330), .Z(N501) );
  GTECH_AND2 C2135 ( .A(N330), .B(N332), .Z(N502) );
  GTECH_AND2 C2136 ( .A(ep2_match), .B(N502), .Z(N503) );
  GTECH_AND2 C2138 ( .A(N502), .B(N335), .Z(N504) );
  GTECH_AND2 C2139 ( .A(ep3_match), .B(N504), .Z(N505) );
  GTECH_AND2 C2141 ( .A(N504), .B(N338), .Z(N506) );
  GTECH_AND2 C2142 ( .A(ep4_match), .B(N506), .Z(N507) );
  GTECH_AND2 C2144 ( .A(N506), .B(N341), .Z(N508) );
  GTECH_AND2 C2145 ( .A(ep5_match), .B(N508), .Z(N509) );
  GTECH_AND2 C2147 ( .A(N508), .B(N344), .Z(N510) );
  GTECH_AND2 C2148 ( .A(ep6_match), .B(N510), .Z(N511) );
  GTECH_AND2 C2150 ( .A(N510), .B(N347), .Z(N512) );
  GTECH_AND2 C2151 ( .A(ep7_match), .B(N512), .Z(N513) );
  GTECH_AND2 C2153 ( .A(N512), .B(N350), .Z(N514) );
  GTECH_AND2 C2154 ( .A(ep8_match), .B(N514), .Z(N515) );
  GTECH_AND2 C2156 ( .A(N514), .B(N353), .Z(N516) );
  GTECH_AND2 C2157 ( .A(ep9_match), .B(N516), .Z(N517) );
  GTECH_AND2 C2159 ( .A(N516), .B(N356), .Z(N518) );
  GTECH_AND2 C2160 ( .A(ep10_match), .B(N518), .Z(N519) );
  GTECH_AND2 C2162 ( .A(N518), .B(N359), .Z(N520) );
  GTECH_AND2 C2163 ( .A(ep11_match), .B(N520), .Z(N521) );
  GTECH_AND2 C2165 ( .A(N520), .B(N362), .Z(N522) );
  GTECH_AND2 C2166 ( .A(ep12_match), .B(N522), .Z(N523) );
  GTECH_AND2 C2168 ( .A(N522), .B(N365), .Z(N524) );
  GTECH_AND2 C2169 ( .A(ep13_match), .B(N524), .Z(N525) );
  GTECH_AND2 C2171 ( .A(N524), .B(N368), .Z(N526) );
  GTECH_AND2 C2172 ( .A(ep14_match), .B(N526), .Z(N527) );
  GTECH_AND2 C2174 ( .A(N526), .B(N371), .Z(N528) );
  GTECH_AND2 C2175 ( .A(ep15_match), .B(N528), .Z(N529) );
  GTECH_OR2 C2192 ( .A(ep1_match), .B(ep0_match), .Z(N530) );
  GTECH_OR2 C2193 ( .A(ep2_match), .B(N530), .Z(N531) );
  GTECH_OR2 C2194 ( .A(ep3_match), .B(N531), .Z(N532) );
  GTECH_OR2 C2195 ( .A(ep4_match), .B(N532), .Z(N533) );
  GTECH_OR2 C2196 ( .A(ep5_match), .B(N533), .Z(N534) );
  GTECH_OR2 C2197 ( .A(ep6_match), .B(N534), .Z(N535) );
  GTECH_OR2 C2198 ( .A(ep7_match), .B(N535), .Z(N536) );
  GTECH_OR2 C2199 ( .A(ep8_match), .B(N536), .Z(N537) );
  GTECH_OR2 C2200 ( .A(ep9_match), .B(N537), .Z(N538) );
  GTECH_OR2 C2201 ( .A(ep10_match), .B(N538), .Z(N539) );
  GTECH_OR2 C2202 ( .A(ep11_match), .B(N539), .Z(N540) );
  GTECH_OR2 C2203 ( .A(ep12_match), .B(N540), .Z(N541) );
  GTECH_OR2 C2204 ( .A(ep13_match), .B(N541), .Z(N542) );
  GTECH_OR2 C2205 ( .A(ep14_match), .B(N542), .Z(N543) );
  GTECH_OR2 C2206 ( .A(ep15_match), .B(N543), .Z(N544) );
  GTECH_NOT I_114 ( .A(N544), .Z(N545) );
  GTECH_AND2 C2209 ( .A(ep1_match), .B(N330), .Z(N548) );
  GTECH_AND2 C2211 ( .A(N330), .B(N332), .Z(N549) );
  GTECH_AND2 C2212 ( .A(ep2_match), .B(N549), .Z(N550) );
  GTECH_AND2 C2214 ( .A(N549), .B(N335), .Z(N551) );
  GTECH_AND2 C2215 ( .A(ep3_match), .B(N551), .Z(N552) );
  GTECH_AND2 C2217 ( .A(N551), .B(N338), .Z(N553) );
  GTECH_AND2 C2218 ( .A(ep4_match), .B(N553), .Z(N554) );
  GTECH_AND2 C2220 ( .A(N553), .B(N341), .Z(N555) );
  GTECH_AND2 C2221 ( .A(ep5_match), .B(N555), .Z(N556) );
  GTECH_AND2 C2223 ( .A(N555), .B(N344), .Z(N557) );
  GTECH_AND2 C2224 ( .A(ep6_match), .B(N557), .Z(N558) );
  GTECH_AND2 C2226 ( .A(N557), .B(N347), .Z(N559) );
  GTECH_AND2 C2227 ( .A(ep7_match), .B(N559), .Z(N560) );
  GTECH_AND2 C2229 ( .A(N559), .B(N350), .Z(N561) );
  GTECH_AND2 C2230 ( .A(ep8_match), .B(N561), .Z(N562) );
  GTECH_AND2 C2232 ( .A(N561), .B(N353), .Z(N563) );
  GTECH_AND2 C2233 ( .A(ep9_match), .B(N563), .Z(N564) );
  GTECH_AND2 C2235 ( .A(N563), .B(N356), .Z(N565) );
  GTECH_AND2 C2236 ( .A(ep10_match), .B(N565), .Z(N566) );
  GTECH_AND2 C2238 ( .A(N565), .B(N359), .Z(N567) );
  GTECH_AND2 C2239 ( .A(ep11_match), .B(N567), .Z(N568) );
  GTECH_AND2 C2241 ( .A(N567), .B(N362), .Z(N569) );
  GTECH_AND2 C2242 ( .A(ep12_match), .B(N569), .Z(N570) );
  GTECH_AND2 C2244 ( .A(N569), .B(N365), .Z(N571) );
  GTECH_AND2 C2245 ( .A(ep13_match), .B(N571), .Z(N572) );
  GTECH_AND2 C2247 ( .A(N571), .B(N368), .Z(N573) );
  GTECH_AND2 C2248 ( .A(ep14_match), .B(N573), .Z(N574) );
  GTECH_AND2 C2250 ( .A(N573), .B(N371), .Z(N575) );
  GTECH_AND2 C2251 ( .A(ep15_match), .B(N575), .Z(N576) );
  GTECH_OR2 C2268 ( .A(ep1_match), .B(ep0_match), .Z(N577) );
  GTECH_OR2 C2269 ( .A(ep2_match), .B(N577), .Z(N578) );
  GTECH_OR2 C2270 ( .A(ep3_match), .B(N578), .Z(N579) );
  GTECH_OR2 C2271 ( .A(ep4_match), .B(N579), .Z(N580) );
  GTECH_OR2 C2272 ( .A(ep5_match), .B(N580), .Z(N581) );
  GTECH_OR2 C2273 ( .A(ep6_match), .B(N581), .Z(N582) );
  GTECH_OR2 C2274 ( .A(ep7_match), .B(N582), .Z(N583) );
  GTECH_OR2 C2275 ( .A(ep8_match), .B(N583), .Z(N584) );
  GTECH_OR2 C2276 ( .A(ep9_match), .B(N584), .Z(N585) );
  GTECH_OR2 C2277 ( .A(ep10_match), .B(N585), .Z(N586) );
  GTECH_OR2 C2278 ( .A(ep11_match), .B(N586), .Z(N587) );
  GTECH_OR2 C2279 ( .A(ep12_match), .B(N587), .Z(N588) );
  GTECH_OR2 C2280 ( .A(ep13_match), .B(N588), .Z(N589) );
  GTECH_OR2 C2281 ( .A(ep14_match), .B(N589), .Z(N590) );
  GTECH_OR2 C2282 ( .A(ep15_match), .B(N590), .Z(N591) );
  GTECH_NOT I_115 ( .A(N591), .Z(N592) );
  GTECH_AND2 C2285 ( .A(ep1_match), .B(N330), .Z(N595) );
  GTECH_AND2 C2287 ( .A(N330), .B(N332), .Z(N596) );
  GTECH_AND2 C2288 ( .A(ep2_match), .B(N596), .Z(N597) );
  GTECH_AND2 C2290 ( .A(N596), .B(N335), .Z(N598) );
  GTECH_AND2 C2291 ( .A(ep3_match), .B(N598), .Z(N599) );
  GTECH_AND2 C2293 ( .A(N598), .B(N338), .Z(N600) );
  GTECH_AND2 C2294 ( .A(ep4_match), .B(N600), .Z(N601) );
  GTECH_AND2 C2296 ( .A(N600), .B(N341), .Z(N602) );
  GTECH_AND2 C2297 ( .A(ep5_match), .B(N602), .Z(N603) );
  GTECH_AND2 C2299 ( .A(N602), .B(N344), .Z(N604) );
  GTECH_AND2 C2300 ( .A(ep6_match), .B(N604), .Z(N605) );
  GTECH_AND2 C2302 ( .A(N604), .B(N347), .Z(N606) );
  GTECH_AND2 C2303 ( .A(ep7_match), .B(N606), .Z(N607) );
  GTECH_AND2 C2305 ( .A(N606), .B(N350), .Z(N608) );
  GTECH_AND2 C2306 ( .A(ep8_match), .B(N608), .Z(N609) );
  GTECH_AND2 C2308 ( .A(N608), .B(N353), .Z(N610) );
  GTECH_AND2 C2309 ( .A(ep9_match), .B(N610), .Z(N611) );
  GTECH_AND2 C2311 ( .A(N610), .B(N356), .Z(N612) );
  GTECH_AND2 C2312 ( .A(ep10_match), .B(N612), .Z(N613) );
  GTECH_AND2 C2314 ( .A(N612), .B(N359), .Z(N614) );
  GTECH_AND2 C2315 ( .A(ep11_match), .B(N614), .Z(N615) );
  GTECH_AND2 C2317 ( .A(N614), .B(N362), .Z(N616) );
  GTECH_AND2 C2318 ( .A(ep12_match), .B(N616), .Z(N617) );
  GTECH_AND2 C2320 ( .A(N616), .B(N365), .Z(N618) );
  GTECH_AND2 C2321 ( .A(ep13_match), .B(N618), .Z(N619) );
  GTECH_AND2 C2323 ( .A(N618), .B(N368), .Z(N620) );
  GTECH_AND2 C2324 ( .A(ep14_match), .B(N620), .Z(N621) );
  GTECH_AND2 C2326 ( .A(N620), .B(N371), .Z(N622) );
  GTECH_AND2 C2327 ( .A(ep15_match), .B(N622), .Z(N623) );
  GTECH_AND2 C2328 ( .A(N933), .B(attach_r), .Z(attach) );
  GTECH_NOT I_116 ( .A(attach_r1), .Z(N933) );
  GTECH_AND2 C2330 ( .A(attach_r1), .B(N934), .Z(deattach) );
  GTECH_NOT I_117 ( .A(attach_r), .Z(N934) );
  GTECH_AND2 C2332 ( .A(N935), .B(suspend_r), .Z(suspend_start) );
  GTECH_NOT I_118 ( .A(suspend_r1), .Z(N935) );
  GTECH_AND2 C2334 ( .A(suspend_r1), .B(N936), .Z(suspend_end) );
  GTECH_NOT I_119 ( .A(suspend_r), .Z(N936) );
  GTECH_NOT I_120 ( .A(rst), .Z(N624) );
  GTECH_OR2 C2340 ( .A(int_src_re), .B(N624), .Z(N625) );
  GTECH_OR2 C2341 ( .A(usb_reset_r), .B(N625), .Z(N626) );
  GTECH_NOT I_121 ( .A(N626), .Z(N627) );
  GTECH_NOT I_122 ( .A(N624), .Z(N630) );
  GTECH_AND2 C2344 ( .A(int_src_re), .B(N630), .Z(N631) );
  GTECH_NOT I_123 ( .A(int_src_re), .Z(N632) );
  GTECH_AND2 C2346 ( .A(N630), .B(N632), .Z(N633) );
  GTECH_AND2 C2347 ( .A(usb_reset_r), .B(N633), .Z(N634) );
  GTECH_NOT I_124 ( .A(rst), .Z(N635) );
  GTECH_OR2 C2352 ( .A(int_src_re), .B(N635), .Z(N636) );
  GTECH_OR2 C2353 ( .A(rx_err_r), .B(N636), .Z(N637) );
  GTECH_NOT I_125 ( .A(N637), .Z(N638) );
  GTECH_NOT I_126 ( .A(N635), .Z(N641) );
  GTECH_AND2 C2356 ( .A(int_src_re), .B(N641), .Z(N642) );
  GTECH_AND2 C2358 ( .A(N641), .B(N632), .Z(N643) );
  GTECH_AND2 C2359 ( .A(rx_err_r), .B(N643), .Z(N644) );
  GTECH_NOT I_127 ( .A(rst), .Z(N645) );
  GTECH_OR2 C2364 ( .A(int_src_re), .B(N645), .Z(N646) );
  GTECH_OR2 C2365 ( .A(deattach), .B(N646), .Z(N647) );
  GTECH_NOT I_128 ( .A(N647), .Z(N648) );
  GTECH_NOT I_129 ( .A(N645), .Z(N651) );
  GTECH_AND2 C2368 ( .A(int_src_re), .B(N651), .Z(N652) );
  GTECH_AND2 C2370 ( .A(N651), .B(N632), .Z(N653) );
  GTECH_AND2 C2371 ( .A(deattach), .B(N653), .Z(N654) );
  GTECH_NOT I_130 ( .A(rst), .Z(N655) );
  GTECH_OR2 C2376 ( .A(int_src_re), .B(N655), .Z(N656) );
  GTECH_OR2 C2377 ( .A(attach), .B(N656), .Z(N657) );
  GTECH_NOT I_131 ( .A(N657), .Z(N658) );
  GTECH_NOT I_132 ( .A(N655), .Z(N661) );
  GTECH_AND2 C2380 ( .A(int_src_re), .B(N661), .Z(N662) );
  GTECH_AND2 C2382 ( .A(N661), .B(N632), .Z(N663) );
  GTECH_AND2 C2383 ( .A(attach), .B(N663), .Z(N664) );
  GTECH_NOT I_133 ( .A(rst), .Z(N665) );
  GTECH_OR2 C2388 ( .A(int_src_re), .B(N665), .Z(N666) );
  GTECH_OR2 C2389 ( .A(suspend_end), .B(N666), .Z(N667) );
  GTECH_NOT I_134 ( .A(N667), .Z(N668) );
  GTECH_NOT I_135 ( .A(N665), .Z(N671) );
  GTECH_AND2 C2392 ( .A(int_src_re), .B(N671), .Z(N672) );
  GTECH_AND2 C2394 ( .A(N671), .B(N632), .Z(N673) );
  GTECH_AND2 C2395 ( .A(suspend_end), .B(N673), .Z(N674) );
  GTECH_NOT I_136 ( .A(rst), .Z(N675) );
  GTECH_OR2 C2400 ( .A(int_src_re), .B(N675), .Z(N676) );
  GTECH_OR2 C2401 ( .A(suspend_start), .B(N676), .Z(N677) );
  GTECH_NOT I_137 ( .A(N677), .Z(N678) );
  GTECH_NOT I_138 ( .A(N675), .Z(N681) );
  GTECH_AND2 C2404 ( .A(int_src_re), .B(N681), .Z(N682) );
  GTECH_AND2 C2406 ( .A(N681), .B(N632), .Z(N683) );
  GTECH_AND2 C2407 ( .A(suspend_start), .B(N683), .Z(N684) );
  GTECH_NOT I_139 ( .A(rst), .Z(N685) );
  GTECH_OR2 C2412 ( .A(int_src_re), .B(N685), .Z(N686) );
  GTECH_OR2 C2413 ( .A(nse_err_r), .B(N686), .Z(N687) );
  GTECH_NOT I_140 ( .A(N687), .Z(N688) );
  GTECH_NOT I_141 ( .A(N685), .Z(N691) );
  GTECH_AND2 C2416 ( .A(int_src_re), .B(N691), .Z(N692) );
  GTECH_AND2 C2418 ( .A(N691), .B(N632), .Z(N693) );
  GTECH_AND2 C2419 ( .A(nse_err_r), .B(N693), .Z(N694) );
  GTECH_NOT I_142 ( .A(rst), .Z(N695) );
  GTECH_OR2 C2424 ( .A(int_src_re), .B(N695), .Z(N696) );
  GTECH_OR2 C2425 ( .A(pid_cs_err_r), .B(N696), .Z(N697) );
  GTECH_NOT I_143 ( .A(N697), .Z(N698) );
  GTECH_NOT I_144 ( .A(N695), .Z(N701) );
  GTECH_AND2 C2428 ( .A(int_src_re), .B(N701), .Z(N702) );
  GTECH_AND2 C2430 ( .A(N701), .B(N632), .Z(N703) );
  GTECH_AND2 C2431 ( .A(pid_cs_err_r), .B(N703), .Z(N704) );
  GTECH_NOT I_145 ( .A(rst), .Z(N705) );
  GTECH_OR2 C2436 ( .A(int_src_re), .B(N705), .Z(N706) );
  GTECH_OR2 C2437 ( .A(crc5_err_r), .B(N706), .Z(N707) );
  GTECH_NOT I_146 ( .A(N707), .Z(N708) );
  GTECH_NOT I_147 ( .A(N705), .Z(N711) );
  GTECH_AND2 C2440 ( .A(int_src_re), .B(N711), .Z(N712) );
  GTECH_AND2 C2442 ( .A(N711), .B(N632), .Z(N713) );
  GTECH_AND2 C2443 ( .A(crc5_err_r), .B(N713), .Z(N714) );
  GTECH_OR2 C2444 ( .A(ep15_inta), .B(ep15_intb), .Z(N715) );
  GTECH_OR2 C2445 ( .A(ep14_inta), .B(ep14_intb), .Z(N716) );
  GTECH_OR2 C2446 ( .A(ep13_inta), .B(ep13_intb), .Z(N717) );
  GTECH_OR2 C2447 ( .A(ep12_inta), .B(ep12_intb), .Z(N718) );
  GTECH_OR2 C2448 ( .A(ep11_inta), .B(ep11_intb), .Z(N719) );
  GTECH_OR2 C2449 ( .A(ep10_inta), .B(ep10_intb), .Z(N720) );
  GTECH_OR2 C2450 ( .A(ep9_inta), .B(ep9_intb), .Z(N721) );
  GTECH_OR2 C2451 ( .A(ep8_inta), .B(ep8_intb), .Z(N722) );
  GTECH_OR2 C2452 ( .A(ep7_inta), .B(ep7_intb), .Z(N723) );
  GTECH_OR2 C2453 ( .A(ep6_inta), .B(ep6_intb), .Z(N724) );
  GTECH_OR2 C2454 ( .A(ep5_inta), .B(ep5_intb), .Z(N725) );
  GTECH_OR2 C2455 ( .A(ep4_inta), .B(ep4_intb), .Z(N726) );
  GTECH_OR2 C2456 ( .A(ep3_inta), .B(ep3_intb), .Z(N727) );
  GTECH_OR2 C2457 ( .A(ep2_inta), .B(ep2_intb), .Z(N728) );
  GTECH_OR2 C2458 ( .A(ep1_inta), .B(ep1_intb), .Z(N729) );
  GTECH_OR2 C2459 ( .A(ep0_inta), .B(ep0_intb), .Z(N730) );
  GTECH_OR2 C2460 ( .A(N950), .B(ep15_inta), .Z(inta_ep) );
  GTECH_OR2 C2461 ( .A(N949), .B(ep14_inta), .Z(N950) );
  GTECH_OR2 C2462 ( .A(N948), .B(ep13_inta), .Z(N949) );
  GTECH_OR2 C2463 ( .A(N947), .B(ep12_inta), .Z(N948) );
  GTECH_OR2 C2464 ( .A(N946), .B(ep11_inta), .Z(N947) );
  GTECH_OR2 C2465 ( .A(N945), .B(ep10_inta), .Z(N946) );
  GTECH_OR2 C2466 ( .A(N944), .B(ep9_inta), .Z(N945) );
  GTECH_OR2 C2467 ( .A(N943), .B(ep8_inta), .Z(N944) );
  GTECH_OR2 C2468 ( .A(N942), .B(ep7_inta), .Z(N943) );
  GTECH_OR2 C2469 ( .A(N941), .B(ep6_inta), .Z(N942) );
  GTECH_OR2 C2470 ( .A(N940), .B(ep5_inta), .Z(N941) );
  GTECH_OR2 C2471 ( .A(N939), .B(ep4_inta), .Z(N940) );
  GTECH_OR2 C2472 ( .A(N938), .B(ep3_inta), .Z(N939) );
  GTECH_OR2 C2473 ( .A(N937), .B(ep2_inta), .Z(N938) );
  GTECH_OR2 C2474 ( .A(ep0_inta), .B(ep1_inta), .Z(N937) );
  GTECH_OR2 C2475 ( .A(N964), .B(ep15_intb), .Z(intb_ep) );
  GTECH_OR2 C2476 ( .A(N963), .B(ep14_intb), .Z(N964) );
  GTECH_OR2 C2477 ( .A(N962), .B(ep13_intb), .Z(N963) );
  GTECH_OR2 C2478 ( .A(N961), .B(ep12_intb), .Z(N962) );
  GTECH_OR2 C2479 ( .A(N960), .B(ep11_intb), .Z(N961) );
  GTECH_OR2 C2480 ( .A(N959), .B(ep10_intb), .Z(N960) );
  GTECH_OR2 C2481 ( .A(N958), .B(ep9_intb), .Z(N959) );
  GTECH_OR2 C2482 ( .A(N957), .B(ep8_intb), .Z(N958) );
  GTECH_OR2 C2483 ( .A(N956), .B(ep7_intb), .Z(N957) );
  GTECH_OR2 C2484 ( .A(N955), .B(ep6_intb), .Z(N956) );
  GTECH_OR2 C2485 ( .A(N954), .B(ep5_intb), .Z(N955) );
  GTECH_OR2 C2486 ( .A(N953), .B(ep4_intb), .Z(N954) );
  GTECH_OR2 C2487 ( .A(N952), .B(ep3_intb), .Z(N953) );
  GTECH_OR2 C2488 ( .A(N951), .B(ep2_intb), .Z(N952) );
  GTECH_OR2 C2489 ( .A(ep0_intb), .B(ep1_intb), .Z(N951) );
  GTECH_OR2 C2490 ( .A(N979), .B(N980), .Z(inta_rf) );
  GTECH_OR2 C2491 ( .A(N977), .B(N978), .Z(N979) );
  GTECH_OR2 C2492 ( .A(N975), .B(N976), .Z(N977) );
  GTECH_OR2 C2493 ( .A(N973), .B(N974), .Z(N975) );
  GTECH_OR2 C2494 ( .A(N971), .B(N972), .Z(N973) );
  GTECH_OR2 C2495 ( .A(N969), .B(N970), .Z(N971) );
  GTECH_OR2 C2496 ( .A(N967), .B(N968), .Z(N969) );
  GTECH_OR2 C2497 ( .A(N965), .B(N966), .Z(N967) );
  GTECH_AND2 C2498 ( .A(int_srcb[8]), .B(inta_msk[8]), .Z(N965) );
  GTECH_AND2 C2499 ( .A(int_srcb[7]), .B(inta_msk[7]), .Z(N966) );
  GTECH_AND2 C2500 ( .A(int_srcb[6]), .B(inta_msk[6]), .Z(N968) );
  GTECH_AND2 C2501 ( .A(int_srcb[5]), .B(inta_msk[5]), .Z(N970) );
  GTECH_AND2 C2502 ( .A(int_srcb[4]), .B(inta_msk[4]), .Z(N972) );
  GTECH_AND2 C2503 ( .A(int_srcb[3]), .B(inta_msk[3]), .Z(N974) );
  GTECH_AND2 C2504 ( .A(int_srcb[2]), .B(inta_msk[2]), .Z(N976) );
  GTECH_AND2 C2505 ( .A(int_srcb[1]), .B(inta_msk[1]), .Z(N978) );
  GTECH_AND2 C2506 ( .A(int_srcb[0]), .B(inta_msk[0]), .Z(N980) );
  GTECH_OR2 C2507 ( .A(N995), .B(N996), .Z(intb_rf) );
  GTECH_OR2 C2508 ( .A(N993), .B(N994), .Z(N995) );
  GTECH_OR2 C2509 ( .A(N991), .B(N992), .Z(N993) );
  GTECH_OR2 C2510 ( .A(N989), .B(N990), .Z(N991) );
  GTECH_OR2 C2511 ( .A(N987), .B(N988), .Z(N989) );
  GTECH_OR2 C2512 ( .A(N985), .B(N986), .Z(N987) );
  GTECH_OR2 C2513 ( .A(N983), .B(N984), .Z(N985) );
  GTECH_OR2 C2514 ( .A(N981), .B(N982), .Z(N983) );
  GTECH_AND2 C2515 ( .A(int_srcb[8]), .B(intb_msk[8]), .Z(N981) );
  GTECH_AND2 C2516 ( .A(int_srcb[7]), .B(intb_msk[7]), .Z(N982) );
  GTECH_AND2 C2517 ( .A(int_srcb[6]), .B(intb_msk[6]), .Z(N984) );
  GTECH_AND2 C2518 ( .A(int_srcb[5]), .B(intb_msk[5]), .Z(N986) );
  GTECH_AND2 C2519 ( .A(int_srcb[4]), .B(intb_msk[4]), .Z(N988) );
  GTECH_AND2 C2520 ( .A(int_srcb[3]), .B(intb_msk[3]), .Z(N990) );
  GTECH_AND2 C2521 ( .A(int_srcb[2]), .B(intb_msk[2]), .Z(N992) );
  GTECH_AND2 C2522 ( .A(int_srcb[1]), .B(intb_msk[1]), .Z(N994) );
  GTECH_AND2 C2523 ( .A(int_srcb[0]), .B(intb_msk[0]), .Z(N996) );
  GTECH_OR2 C2524 ( .A(inta_ep), .B(inta_rf), .Z(N731) );
  GTECH_OR2 C2525 ( .A(intb_ep), .B(intb_rf), .Z(N732) );
endmodule


module usbf_wb ( wb_clk, phy_clk, rst, wb_addr_i, wb_data_i, wb_data_o, 
        wb_ack_o, wb_we_i, wb_stb_i, wb_cyc_i, ma_adr, ma_dout, ma_din, ma_we, 
        ma_req, ma_ack, rf_re, rf_we, rf_din, rf_dout );
  input [17:0] wb_addr_i;
  input [31:0] wb_data_i;
  output [31:0] wb_data_o;
  output [17:0] ma_adr;
  output [31:0] ma_dout;
  input [31:0] ma_din;
  input [31:0] rf_din;
  output [31:0] rf_dout;
  input wb_clk, phy_clk, rst, wb_we_i, wb_stb_i, wb_cyc_i, ma_ack;
  output wb_ack_o, ma_we, ma_req, rf_re, rf_we;
  wire   N0, N1, N2, N3, N4, N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15,
         N16, N17, N18, N19, N20, N21, N22, N23, N24, N25, N26, N27, N28, N29,
         N30, N31, N32, N33, N34, N35, N36, N37, N38, N39, N40, N41, N42, N43,
         N44, N45, N46, wb_req_s1, wb_ack_s1, wb_ack_d, wb_ack_s2, N47,
         wb_ack_s1a, N48, N49, N50, N51, N52, N53, N54, N55, N56, N57, N58,
         N59, N60, N61, N62, N63, N64, N65, N66, N67, N68, N69, N70, N71, N72,
         N73, N74, N75, N76, N77, N78, N79, N80, N81, N82, N83, N84, N85, N86,
         N87, N88, N89, N90, N91, N92, N93, N94, N95, N96, N97, N98, N99, N100,
         N101, N102, N103, N104, N105, N106, N107, N108, N109, N110, N111,
         N112, N113, N114, N115, N116, N117, N118, N119, N120, N121, N122,
         N123, N124, N125, N126, N127, N128, N129, N130, N131, N132, N133,
         N134, N135, N136, N137, N138;
  wire   [5:0] state;
  wire   [5:0] next_state;
  assign ma_adr[17] = wb_addr_i[17];
  assign ma_adr[16] = wb_addr_i[16];
  assign ma_adr[15] = wb_addr_i[15];
  assign ma_adr[14] = wb_addr_i[14];
  assign ma_adr[13] = wb_addr_i[13];
  assign ma_adr[12] = wb_addr_i[12];
  assign ma_adr[11] = wb_addr_i[11];
  assign ma_adr[10] = wb_addr_i[10];
  assign ma_adr[9] = wb_addr_i[9];
  assign ma_adr[8] = wb_addr_i[8];
  assign ma_adr[7] = wb_addr_i[7];
  assign ma_adr[6] = wb_addr_i[6];
  assign ma_adr[5] = wb_addr_i[5];
  assign ma_adr[4] = wb_addr_i[4];
  assign ma_adr[3] = wb_addr_i[3];
  assign ma_adr[2] = wb_addr_i[2];
  assign ma_adr[1] = wb_addr_i[1];
  assign ma_adr[0] = wb_addr_i[0];
  assign rf_dout[31] = ma_dout[31];
  assign ma_dout[31] = wb_data_i[31];
  assign rf_dout[30] = ma_dout[30];
  assign ma_dout[30] = wb_data_i[30];
  assign rf_dout[29] = ma_dout[29];
  assign ma_dout[29] = wb_data_i[29];
  assign rf_dout[28] = ma_dout[28];
  assign ma_dout[28] = wb_data_i[28];
  assign rf_dout[27] = ma_dout[27];
  assign ma_dout[27] = wb_data_i[27];
  assign rf_dout[26] = ma_dout[26];
  assign ma_dout[26] = wb_data_i[26];
  assign rf_dout[25] = ma_dout[25];
  assign ma_dout[25] = wb_data_i[25];
  assign rf_dout[24] = ma_dout[24];
  assign ma_dout[24] = wb_data_i[24];
  assign rf_dout[23] = ma_dout[23];
  assign ma_dout[23] = wb_data_i[23];
  assign rf_dout[22] = ma_dout[22];
  assign ma_dout[22] = wb_data_i[22];
  assign rf_dout[21] = ma_dout[21];
  assign ma_dout[21] = wb_data_i[21];
  assign rf_dout[20] = ma_dout[20];
  assign ma_dout[20] = wb_data_i[20];
  assign rf_dout[19] = ma_dout[19];
  assign ma_dout[19] = wb_data_i[19];
  assign rf_dout[18] = ma_dout[18];
  assign ma_dout[18] = wb_data_i[18];
  assign rf_dout[17] = ma_dout[17];
  assign ma_dout[17] = wb_data_i[17];
  assign rf_dout[16] = ma_dout[16];
  assign ma_dout[16] = wb_data_i[16];
  assign rf_dout[15] = ma_dout[15];
  assign ma_dout[15] = wb_data_i[15];
  assign rf_dout[14] = ma_dout[14];
  assign ma_dout[14] = wb_data_i[14];
  assign rf_dout[13] = ma_dout[13];
  assign ma_dout[13] = wb_data_i[13];
  assign rf_dout[12] = ma_dout[12];
  assign ma_dout[12] = wb_data_i[12];
  assign rf_dout[11] = ma_dout[11];
  assign ma_dout[11] = wb_data_i[11];
  assign rf_dout[10] = ma_dout[10];
  assign ma_dout[10] = wb_data_i[10];
  assign rf_dout[9] = ma_dout[9];
  assign ma_dout[9] = wb_data_i[9];
  assign rf_dout[8] = ma_dout[8];
  assign ma_dout[8] = wb_data_i[8];
  assign rf_dout[7] = ma_dout[7];
  assign ma_dout[7] = wb_data_i[7];
  assign rf_dout[6] = ma_dout[6];
  assign ma_dout[6] = wb_data_i[6];
  assign rf_dout[5] = ma_dout[5];
  assign ma_dout[5] = wb_data_i[5];
  assign rf_dout[4] = ma_dout[4];
  assign ma_dout[4] = wb_data_i[4];
  assign rf_dout[3] = ma_dout[3];
  assign ma_dout[3] = wb_data_i[3];
  assign rf_dout[2] = ma_dout[2];
  assign ma_dout[2] = wb_data_i[2];
  assign rf_dout[1] = ma_dout[1];
  assign ma_dout[1] = wb_data_i[1];
  assign rf_dout[0] = ma_dout[0];
  assign ma_dout[0] = wb_data_i[0];

  \**SEQGEN**  \wb_data_o_reg[31]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N45), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[31]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[30]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N44), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[30]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[29]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N43), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[29]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[28]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N42), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[28]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[27]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N41), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[27]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[26]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N40), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[26]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[25]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N39), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[25]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[24]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N38), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[24]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[23]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N37), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[23]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[22]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N36), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[22]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[21]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N35), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[21]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[20]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N34), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[20]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[19]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N33), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[19]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[18]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N32), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[18]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[17]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N31), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[17]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[16]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N30), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[16]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[15]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N29), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[15]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[14]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N28), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[14]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[13]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N27), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[13]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[12]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N26), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[12]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[11]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N25), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[11]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[10]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N24), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[10]), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[9]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N23), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[9]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[8]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N22), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[8]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N21), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[7]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N20), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[6]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N19), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[5]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N18), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[4]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N17), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[3]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N16), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[2]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N15), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[1]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \wb_data_o_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        N14), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_data_o[0]), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  wb_req_s1_reg ( .clear(1'b0), .preset(1'b0), .next_state(N46), 
        .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(wb_req_s1), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  wb_ack_s1_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        wb_ack_d), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_ack_s1), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  wb_ack_o_reg ( .clear(1'b0), .preset(1'b0), .next_state(N47), 
        .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(wb_ack_o), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  wb_ack_s1a_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        wb_ack_s1), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_ack_s1a), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  wb_ack_s2_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        wb_ack_s1a), .clocked_on(wb_clk), .data_in(1'b0), .enable(1'b0), .Q(
        wb_ack_s2), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(
        1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \state_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(N54), 
        .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(state[5]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N130) );
  \**SEQGEN**  \state_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(N53), 
        .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(state[4]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N130) );
  \**SEQGEN**  \state_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(N52), 
        .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(state[3]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N130) );
  \**SEQGEN**  \state_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(N51), 
        .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(state[2]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N130) );
  \**SEQGEN**  \state_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(N50), 
        .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(state[1]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N130) );
  \**SEQGEN**  \state_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(N49), 
        .clocked_on(phy_clk), .data_in(1'b0), .enable(1'b0), .Q(state[0]), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N130) );
  GTECH_OR2 C56 ( .A(state[5]), .B(state[4]), .Z(N56) );
  GTECH_OR2 C57 ( .A(state[3]), .B(state[2]), .Z(N57) );
  GTECH_OR2 C58 ( .A(state[1]), .B(N55), .Z(N58) );
  GTECH_OR2 C59 ( .A(N56), .B(N57), .Z(N59) );
  GTECH_OR2 C60 ( .A(N59), .B(N58), .Z(N60) );
  GTECH_OR2 C63 ( .A(state[5]), .B(state[4]), .Z(N63) );
  GTECH_OR2 C64 ( .A(state[3]), .B(state[2]), .Z(N64) );
  GTECH_OR2 C65 ( .A(N62), .B(state[0]), .Z(N65) );
  GTECH_OR2 C66 ( .A(N63), .B(N64), .Z(N66) );
  GTECH_OR2 C67 ( .A(N66), .B(N65), .Z(N67) );
  GTECH_OR2 C70 ( .A(state[5]), .B(state[4]), .Z(N70) );
  GTECH_OR2 C71 ( .A(state[3]), .B(N69), .Z(N71) );
  GTECH_OR2 C72 ( .A(state[1]), .B(state[0]), .Z(N72) );
  GTECH_OR2 C73 ( .A(N70), .B(N71), .Z(N73) );
  GTECH_OR2 C74 ( .A(N73), .B(N72), .Z(N74) );
  GTECH_OR2 C77 ( .A(state[5]), .B(state[4]), .Z(N77) );
  GTECH_OR2 C78 ( .A(N76), .B(state[2]), .Z(N78) );
  GTECH_OR2 C79 ( .A(state[1]), .B(state[0]), .Z(N79) );
  GTECH_OR2 C80 ( .A(N77), .B(N78), .Z(N80) );
  GTECH_OR2 C81 ( .A(N80), .B(N79), .Z(N81) );
  GTECH_OR2 C84 ( .A(state[5]), .B(N83), .Z(N84) );
  GTECH_OR2 C85 ( .A(state[3]), .B(state[2]), .Z(N85) );
  GTECH_OR2 C86 ( .A(state[1]), .B(state[0]), .Z(N86) );
  GTECH_OR2 C87 ( .A(N84), .B(N85), .Z(N87) );
  GTECH_OR2 C88 ( .A(N87), .B(N86), .Z(N88) );
  GTECH_OR2 C91 ( .A(N90), .B(state[4]), .Z(N91) );
  GTECH_OR2 C92 ( .A(state[3]), .B(state[2]), .Z(N92) );
  GTECH_OR2 C93 ( .A(state[1]), .B(state[0]), .Z(N93) );
  GTECH_OR2 C94 ( .A(N91), .B(N92), .Z(N94) );
  GTECH_OR2 C95 ( .A(N94), .B(N93), .Z(N95) );
  SELECT_OP C279 ( .DATA1(rf_din), .DATA2(ma_din), .CONTROL1(N0), .CONTROL2(N1), .Z({N45, N44, N43, N42, N41, N40, N39, N38, N37, N36, N35, N34, N33, N32, 
        N31, N30, N29, N28, N27, N26, N25, N24, N23, N22, N21, N20, N19, N18, 
        N17, N16, N15, N14}) );
  GTECH_BUF B_0 ( .A(N13), .Z(N0) );
  GTECH_BUF B_1 ( .A(ma_adr[17]), .Z(N1) );
  SELECT_OP C280 ( .DATA1({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b1}), .DATA2(
        next_state), .CONTROL1(N2), .CONTROL2(N3), .Z({N54, N53, N52, N51, N50, 
        N49}) );
  GTECH_BUF B_2 ( .A(N48), .Z(N2) );
  GTECH_BUF B_3 ( .A(rst), .Z(N3) );
  SELECT_OP C281 ( .DATA1(1'b1), .DATA2(N97), .CONTROL1(N4), .CONTROL2(N100), 
        .Z(N101) );
  GTECH_BUF B_4 ( .A(N99), .Z(N4) );
  GTECH_NOT I_0 ( .A(N99), .Z(N102) );
  SELECT_OP C283 ( .DATA1({1'b0, 1'b0}), .DATA2({N99, N102}), .CONTROL1(N5), 
        .CONTROL2(N104), .Z({N106, N105}) );
  GTECH_BUF B_5 ( .A(N103), .Z(N5) );
  SELECT_OP C284 ( .DATA1({1'b1, 1'b0, 1'b0}), .DATA2({N103, N106, N105}), 
        .CONTROL1(N6), .CONTROL2(N108), .Z({N111, N110, N109}) );
  GTECH_BUF B_6 ( .A(N107), .Z(N6) );
  SELECT_OP C285 ( .DATA1({1'b0, 1'b0, N111, N110, N109, 1'b0}), .DATA2({1'b0, 
        1'b1, 1'b0, 1'b0, 1'b0, 1'b0}), .DATA3({1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 
        1'b0}), .DATA4({1'b0, 1'b1, 1'b0, 1'b0, 1'b0, 1'b0}), .DATA5({1'b1, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0}), .DATA6({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b1}), .DATA7(state), .CONTROL1(N7), .CONTROL2(N8), .CONTROL3(N9), 
        .CONTROL4(N10), .CONTROL5(N11), .CONTROL6(N12), .CONTROL7(N118), .Z(
        next_state) );
  GTECH_BUF B_7 ( .A(N61), .Z(N7) );
  GTECH_BUF B_8 ( .A(N68), .Z(N8) );
  GTECH_BUF B_9 ( .A(N75), .Z(N9) );
  GTECH_BUF B_10 ( .A(N82), .Z(N10) );
  GTECH_BUF B_11 ( .A(N89), .Z(N11) );
  GTECH_BUF B_12 ( .A(N96), .Z(N12) );
  SELECT_OP C286 ( .DATA1(N101), .DATA2(N112), .DATA3(N112), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .CONTROL1(N7), .CONTROL2(N8), 
        .CONTROL3(N9), .CONTROL4(N10), .CONTROL5(N11), .CONTROL6(N12), 
        .CONTROL7(N118), .Z(ma_req) );
  SELECT_OP C287 ( .DATA1(N97), .DATA2(N112), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .CONTROL1(N7), .CONTROL2(N8), 
        .CONTROL3(N9), .CONTROL4(N10), .CONTROL5(N11), .CONTROL6(N12), 
        .CONTROL7(N118), .Z(ma_we) );
  SELECT_OP C288 ( .DATA1(N103), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .CONTROL1(N7), .CONTROL2(N8), 
        .CONTROL3(N9), .CONTROL4(N10), .CONTROL5(N11), .CONTROL6(N12), 
        .CONTROL7(N118), .Z(rf_we) );
  SELECT_OP C289 ( .DATA1(N107), .DATA2(1'b0), .DATA3(1'b0), .DATA4(1'b0), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .CONTROL1(N7), .CONTROL2(N8), 
        .CONTROL3(N9), .CONTROL4(N10), .CONTROL5(N11), .CONTROL6(N12), 
        .CONTROL7(N118), .Z(rf_re) );
  SELECT_OP C290 ( .DATA1(1'b0), .DATA2(ma_ack), .DATA3(ma_ack), .DATA4(1'b1), 
        .DATA5(1'b0), .DATA6(1'b0), .DATA7(1'b0), .CONTROL1(N7), .CONTROL2(N8), 
        .CONTROL3(N9), .CONTROL4(N10), .CONTROL5(N11), .CONTROL6(N12), 
        .CONTROL7(N118), .Z(wb_ack_d) );
  GTECH_NOT I_1 ( .A(ma_adr[17]), .Z(N13) );
  GTECH_AND2 C296 ( .A(wb_stb_i), .B(wb_cyc_i), .Z(N46) );
  GTECH_AND2 C297 ( .A(N132), .B(N133), .Z(N47) );
  GTECH_AND2 C298 ( .A(wb_ack_s1), .B(N131), .Z(N132) );
  GTECH_NOT I_2 ( .A(wb_ack_s2), .Z(N131) );
  GTECH_NOT I_3 ( .A(wb_ack_o), .Z(N133) );
  GTECH_NOT I_4 ( .A(rst), .Z(N48) );
  GTECH_NOT I_5 ( .A(state[0]), .Z(N55) );
  GTECH_NOT I_6 ( .A(N60), .Z(N61) );
  GTECH_NOT I_7 ( .A(state[1]), .Z(N62) );
  GTECH_NOT I_8 ( .A(N67), .Z(N68) );
  GTECH_NOT I_9 ( .A(state[2]), .Z(N69) );
  GTECH_NOT I_10 ( .A(N74), .Z(N75) );
  GTECH_NOT I_11 ( .A(state[3]), .Z(N76) );
  GTECH_NOT I_12 ( .A(N81), .Z(N82) );
  GTECH_NOT I_13 ( .A(state[4]), .Z(N83) );
  GTECH_NOT I_14 ( .A(N88), .Z(N89) );
  GTECH_NOT I_15 ( .A(state[5]), .Z(N90) );
  GTECH_NOT I_16 ( .A(N95), .Z(N96) );
  GTECH_AND2 C322 ( .A(N134), .B(wb_we_i), .Z(N97) );
  GTECH_AND2 C323 ( .A(wb_req_s1), .B(ma_adr[17]), .Z(N134) );
  GTECH_NOT I_17 ( .A(N97), .Z(N98) );
  GTECH_AND2 C326 ( .A(N135), .B(N136), .Z(N99) );
  GTECH_AND2 C327 ( .A(wb_req_s1), .B(ma_adr[17]), .Z(N135) );
  GTECH_NOT I_18 ( .A(wb_we_i), .Z(N136) );
  GTECH_NOT I_19 ( .A(N99), .Z(N100) );
  GTECH_AND2 C331 ( .A(N137), .B(wb_we_i), .Z(N103) );
  GTECH_AND2 C332 ( .A(wb_req_s1), .B(N13), .Z(N137) );
  GTECH_NOT I_20 ( .A(N103), .Z(N104) );
  GTECH_AND2 C336 ( .A(N138), .B(N136), .Z(N107) );
  GTECH_AND2 C337 ( .A(wb_req_s1), .B(N13), .Z(N138) );
  GTECH_NOT I_21 ( .A(N107), .Z(N108) );
  GTECH_NOT I_22 ( .A(ma_ack), .Z(N112) );
  GTECH_OR2 C345 ( .A(N68), .B(N61), .Z(N113) );
  GTECH_OR2 C346 ( .A(N75), .B(N113), .Z(N114) );
  GTECH_OR2 C347 ( .A(N82), .B(N114), .Z(N115) );
  GTECH_OR2 C348 ( .A(N89), .B(N115), .Z(N116) );
  GTECH_OR2 C349 ( .A(N96), .B(N116), .Z(N117) );
  GTECH_NOT I_23 ( .A(N117), .Z(N118) );
  GTECH_AND2 C351 ( .A(N61), .B(rst), .Z(N119) );
  GTECH_AND2 C352 ( .A(N108), .B(N119), .Z(N120) );
  GTECH_AND2 C353 ( .A(N104), .B(N120), .Z(N121) );
  GTECH_AND2 C354 ( .A(N100), .B(N121), .Z(N122) );
  GTECH_AND2 C355 ( .A(N98), .B(N122), .Z(N123) );
  GTECH_AND2 C356 ( .A(N68), .B(rst), .Z(N124) );
  GTECH_AND2 C357 ( .A(N112), .B(N124), .Z(N125) );
  GTECH_OR2 C358 ( .A(N123), .B(N125), .Z(N126) );
  GTECH_AND2 C359 ( .A(N75), .B(rst), .Z(N127) );
  GTECH_AND2 C360 ( .A(N112), .B(N127), .Z(N128) );
  GTECH_OR2 C361 ( .A(N126), .B(N128), .Z(N129) );
  GTECH_NOT I_24 ( .A(N129), .Z(N130) );
endmodule


module usbf_top ( clk_i, rst_i, wb_addr_i, wb_data_i, wb_data_o, wb_ack_o, 
        wb_we_i, wb_stb_i, wb_cyc_i, inta_o, intb_o, dma_req_o, dma_ack_i, 
        susp_o, resume_req_i, phy_clk_pad_i, phy_rst_pad_o, DataOut_pad_o, 
        TxValid_pad_o, TxReady_pad_i, RxValid_pad_i, RxActive_pad_i, 
        RxError_pad_i, DataIn_pad_i, XcvSelect_pad_o, TermSel_pad_o, 
        SuspendM_pad_o, LineState_pad_i, OpMode_pad_o, usb_vbus_pad_i, 
        VControl_Load_pad_o, VControl_pad_o, VStatus_pad_i, sram_adr_o, 
        sram_data_i, sram_data_o, sram_re_o, sram_we_o );
  input [17:0] wb_addr_i;
  input [31:0] wb_data_i;
  output [31:0] wb_data_o;
  output [15:0] dma_req_o;
  input [15:0] dma_ack_i;
  output [7:0] DataOut_pad_o;
  input [7:0] DataIn_pad_i;
  input [1:0] LineState_pad_i;
  output [1:0] OpMode_pad_o;
  output [3:0] VControl_pad_o;
  input [7:0] VStatus_pad_i;
  output [14:0] sram_adr_o;
  input [31:0] sram_data_i;
  output [31:0] sram_data_o;
  input clk_i, rst_i, wb_we_i, wb_stb_i, wb_cyc_i, resume_req_i, phy_clk_pad_i,
         TxReady_pad_i, RxValid_pad_i, RxActive_pad_i, RxError_pad_i,
         usb_vbus_pad_i;
  output wb_ack_o, inta_o, intb_o, susp_o, phy_rst_pad_o, TxValid_pad_o,
         XcvSelect_pad_o, TermSel_pad_o, SuspendM_pad_o, VControl_Load_pad_o,
         sram_re_o, sram_we_o;
  wire   N0, phy_rst_pad_o, usb_suspend, suspend_clr_wr, suspend_clr, N1, N2,
         N3, N4, resume_req_r, N5, N6, N7, N8, N9, N10, N11, rx_valid,
         rx_active, rx_err, tx_valid, tx_valid_last, tx_ready, tx_first,
         mode_hs, usb_reset, usb_attached, mwe, mreq, mack, dma_in_buf_sz1,
         dma_out_buf_avail, match, buf0_rl, buf0_set, buf1_set, uc_bsel_set,
         uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, int_crc16_set,
         int_to_set, int_seqerr_set, out_to_small, pid_cs_err, nse_err,
         crc5_err, ma_we, ma_req, ma_ack, rf_re, rf_we, rf_resume_req;
  wire   [1:0] LineState_r;
  wire   [7:0] VStatus_r;
  wire   [7:0] rx_data;
  wire   [7:0] tx_data;
  wire   [14:0] madr;
  wire   [31:0] mdout;
  wire   [31:0] mdin;
  wire   [6:0] funct_adr;
  wire   [31:0] idin;
  wire   [3:0] ep_sel;
  wire   [31:0] csr;
  wire   [31:0] buf0;
  wire   [31:0] buf1;
  wire   [31:0] frm_nat;
  wire   [17:0] ma_adr;
  wire   [31:0] ma2wb_d;
  wire   [31:0] wb2ma_d;
  wire   [31:0] wb2rf_d;
  wire   [31:0] rf2wb_d;
  assign phy_rst_pad_o = rst_i;

  \**SEQGEN**  susp_o_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        usb_suspend), .clocked_on(clk_i), .data_in(1'b0), .enable(1'b0), .Q(
        susp_o), .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(1'b1) );
  \**SEQGEN**  \LineState_r_reg[1]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(LineState_pad_i[1]), .clocked_on(phy_clk_pad_i), .data_in(
        1'b0), .enable(1'b0), .Q(LineState_r[1]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \LineState_r_reg[0]  ( .clear(1'b0), .preset(1'b0), 
        .next_state(LineState_pad_i[0]), .clocked_on(phy_clk_pad_i), .data_in(
        1'b0), .enable(1'b0), .Q(LineState_r[0]), .synch_clear(1'b0), 
        .synch_preset(1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \VStatus_r_reg[7]  ( .clear(1'b0), .preset(1'b0), .next_state(
        VStatus_pad_i[7]), .clocked_on(phy_clk_pad_i), .data_in(1'b0), 
        .enable(1'b0), .Q(VStatus_r[7]), .synch_clear(1'b0), .synch_preset(
        1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \VStatus_r_reg[6]  ( .clear(1'b0), .preset(1'b0), .next_state(
        VStatus_pad_i[6]), .clocked_on(phy_clk_pad_i), .data_in(1'b0), 
        .enable(1'b0), .Q(VStatus_r[6]), .synch_clear(1'b0), .synch_preset(
        1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \VStatus_r_reg[5]  ( .clear(1'b0), .preset(1'b0), .next_state(
        VStatus_pad_i[5]), .clocked_on(phy_clk_pad_i), .data_in(1'b0), 
        .enable(1'b0), .Q(VStatus_r[5]), .synch_clear(1'b0), .synch_preset(
        1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \VStatus_r_reg[4]  ( .clear(1'b0), .preset(1'b0), .next_state(
        VStatus_pad_i[4]), .clocked_on(phy_clk_pad_i), .data_in(1'b0), 
        .enable(1'b0), .Q(VStatus_r[4]), .synch_clear(1'b0), .synch_preset(
        1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \VStatus_r_reg[3]  ( .clear(1'b0), .preset(1'b0), .next_state(
        VStatus_pad_i[3]), .clocked_on(phy_clk_pad_i), .data_in(1'b0), 
        .enable(1'b0), .Q(VStatus_r[3]), .synch_clear(1'b0), .synch_preset(
        1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \VStatus_r_reg[2]  ( .clear(1'b0), .preset(1'b0), .next_state(
        VStatus_pad_i[2]), .clocked_on(phy_clk_pad_i), .data_in(1'b0), 
        .enable(1'b0), .Q(VStatus_r[2]), .synch_clear(1'b0), .synch_preset(
        1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \VStatus_r_reg[1]  ( .clear(1'b0), .preset(1'b0), .next_state(
        VStatus_pad_i[1]), .clocked_on(phy_clk_pad_i), .data_in(1'b0), 
        .enable(1'b0), .Q(VStatus_r[1]), .synch_clear(1'b0), .synch_preset(
        1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  \VStatus_r_reg[0]  ( .clear(1'b0), .preset(1'b0), .next_state(
        VStatus_pad_i[0]), .clocked_on(phy_clk_pad_i), .data_in(1'b0), 
        .enable(1'b0), .Q(VStatus_r[0]), .synch_clear(1'b0), .synch_preset(
        1'b0), .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  suspend_clr_wr_reg ( .clear(1'b0), .preset(1'b0), .next_state(
        suspend_clr), .clocked_on(clk_i), .data_in(1'b0), .enable(1'b0), .Q(
        suspend_clr_wr), .synch_clear(1'b0), .synch_preset(1'b0), 
        .synch_toggle(1'b0), .synch_enable(1'b1) );
  \**SEQGEN**  resume_req_r_reg ( .clear(1'b0), .preset(1'b0), .next_state(N6), 
        .clocked_on(clk_i), .data_in(1'b0), .enable(1'b0), .Q(resume_req_r), 
        .synch_clear(1'b0), .synch_preset(1'b0), .synch_toggle(1'b0), 
        .synch_enable(N5) );
  usbf_utmi_if u0 ( .phy_clk(phy_clk_pad_i), .rst(phy_rst_pad_o), .DataOut(
        DataOut_pad_o), .TxValid(TxValid_pad_o), .TxReady(TxReady_pad_i), 
        .RxValid(RxValid_pad_i), .RxActive(RxActive_pad_i), .RxError(
        RxError_pad_i), .DataIn(DataIn_pad_i), .XcvSelect(XcvSelect_pad_o), 
        .TermSel(TermSel_pad_o), .SuspendM(SuspendM_pad_o), .LineState(
        LineState_pad_i), .OpMode(OpMode_pad_o), .usb_vbus(usb_vbus_pad_i), 
        .rx_data(rx_data), .rx_valid(rx_valid), .rx_active(rx_active), 
        .rx_err(rx_err), .tx_data(tx_data), .tx_valid(tx_valid), 
        .tx_valid_last(tx_valid_last), .tx_ready(tx_ready), .tx_first(tx_first), .mode_hs(mode_hs), .usb_reset(usb_reset), .usb_suspend(usb_suspend), 
        .usb_attached(usb_attached), .resume_req(resume_req_r), .suspend_clr(
        suspend_clr) );
  usbf_pl_SSRAM_HADR14 u1 ( .clk(phy_clk_pad_i), .rst(phy_rst_pad_o), 
        .rx_data(rx_data), .rx_valid(rx_valid), .rx_active(rx_active), 
        .rx_err(rx_err), .tx_data(tx_data), .tx_valid(tx_valid), 
        .tx_valid_last(tx_valid_last), .tx_ready(tx_ready), .tx_first(tx_first), .tx_valid_out(TxValid_pad_o), .mode_hs(mode_hs), .usb_reset(usb_reset), 
        .usb_suspend(usb_suspend), .usb_attached(usb_attached), .madr(madr), 
        .mdout(mdout), .mdin(mdin), .mwe(mwe), .mreq(mreq), .mack(mack), .fa(
        funct_adr), .idin(idin), .ep_sel(ep_sel), .match(match), 
        .dma_in_buf_sz1(dma_in_buf_sz1), .dma_out_buf_avail(dma_out_buf_avail), 
        .buf0_rl(buf0_rl), .buf0_set(buf0_set), .buf1_set(buf1_set), 
        .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), .int_buf1_set(
        int_buf1_set), .int_buf0_set(int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(csr), .buf0(buf0), 
        .buf1(buf1), .frm_nat(frm_nat), .pid_cs_err(pid_cs_err), .nse_err(
        nse_err), .crc5_err(crc5_err) );
  usbf_mem_arb_SSRAM_HADR14 u2 ( .phy_clk(phy_clk_pad_i), .wclk(clk_i), .rst(
        phy_rst_pad_o), .sram_adr(sram_adr_o), .sram_din(sram_data_i), 
        .sram_dout(sram_data_o), .sram_re(sram_re_o), .sram_we(sram_we_o), 
        .madr(madr), .mdout(mdin), .mdin(mdout), .mwe(mwe), .mreq(mreq), 
        .mack(mack), .wadr(ma_adr[16:2]), .wdout(ma2wb_d), .wdin(wb2ma_d), 
        .wwe(ma_we), .wreq(ma_req), .wack(ma_ack) );
  usbf_rf u4 ( .clk(phy_clk_pad_i), .wclk(clk_i), .rst(phy_rst_pad_o), .adr(
        ma_adr[8:2]), .re(rf_re), .we(rf_we), .din(wb2rf_d), .dout(rf2wb_d), 
        .inta(inta_o), .intb(intb_o), .dma_req(dma_req_o), .dma_ack(dma_ack_i), 
        .idin(idin), .ep_sel(ep_sel), .match(match), .buf0_rl(buf0_rl), 
        .buf0_set(buf0_set), .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), 
        .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(
        int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(
        int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(csr), .buf0(buf0), 
        .buf1(buf1), .funct_adr(funct_adr), .dma_in_buf_sz1(dma_in_buf_sz1), 
        .dma_out_buf_avail(dma_out_buf_avail), .frm_nat(frm_nat), 
        .utmi_vend_stat(VStatus_r), .utmi_vend_ctrl(VControl_pad_o), 
        .utmi_vend_wr(VControl_Load_pad_o), .line_stat(LineState_r), 
        .usb_attached(usb_attached), .mode_hs(mode_hs), .suspend(usb_suspend), 
        .attached(usb_attached), .usb_reset(usb_reset), .pid_cs_err(pid_cs_err), .nse_err(nse_err), .crc5_err(crc5_err), .rx_err(rx_err), .rf_resume_req(
        rf_resume_req) );
  usbf_wb u5 ( .wb_clk(clk_i), .phy_clk(phy_clk_pad_i), .rst(phy_rst_pad_o), 
        .wb_addr_i(wb_addr_i), .wb_data_i(wb_data_i), .wb_data_o(wb_data_o), 
        .wb_ack_o(wb_ack_o), .wb_we_i(wb_we_i), .wb_stb_i(wb_stb_i), 
        .wb_cyc_i(wb_cyc_i), .ma_adr(ma_adr), .ma_dout(wb2ma_d), .ma_din(
        ma2wb_d), .ma_we(ma_we), .ma_req(ma_req), .ma_ack(ma_ack), .rf_re(
        rf_re), .rf_we(rf_we), .rf_din(rf2wb_d), .rf_dout(wb2rf_d) );
  SELECT_OP C26 ( .DATA1(1'b1), .DATA2(1'b1), .DATA3(1'b1), .DATA4(1'b0), 
        .CONTROL1(N0), .CONTROL2(N8), .CONTROL3(N11), .CONTROL4(N4), .Z(N5) );
  GTECH_BUF B_0 ( .A(N1), .Z(N0) );
  SELECT_OP C27 ( .DATA1(1'b0), .DATA2(1'b0), .DATA3(1'b1), .CONTROL1(N0), 
        .CONTROL2(N8), .CONTROL3(N11), .Z(N6) );
  GTECH_NOT I_0 ( .A(phy_rst_pad_o), .Z(N1) );
  GTECH_OR2 C34 ( .A(suspend_clr_wr), .B(N1), .Z(N2) );
  GTECH_OR2 C35 ( .A(resume_req_i), .B(N2), .Z(N3) );
  GTECH_NOT I_1 ( .A(N3), .Z(N4) );
  GTECH_NOT I_2 ( .A(N1), .Z(N7) );
  GTECH_AND2 C38 ( .A(suspend_clr_wr), .B(N7), .Z(N8) );
  GTECH_NOT I_3 ( .A(suspend_clr_wr), .Z(N9) );
  GTECH_AND2 C40 ( .A(N7), .B(N9), .Z(N10) );
  GTECH_AND2 C41 ( .A(resume_req_i), .B(N10), .Z(N11) );
endmodule



module usbf_utmi_ls ( clk, rst, resume_req, rx_active, tx_ready, drive_k, 
        XcvSelect, TermSel, SuspendM, LineState, OpMode, usb_vbus, mode_hs, 
        usb_reset, usb_suspend, usb_attached, suspend_clr );
  input [1:0] LineState;
  output [1:0] OpMode;
  input clk, rst, resume_req, rx_active, tx_ready, usb_vbus;
  output drive_k, XcvSelect, TermSel, SuspendM, mode_hs, usb_reset,
         usb_suspend, usb_attached, suspend_clr;
  wire   drive_k_d, resume_req_s, resume_req_s1, ls_idle, ls_idle_r, idle_long,
         ls_k_r, ls_j_r, ls_se0_r, ps_cnt_clr, N85, N86, N87, N88,
         idle_cnt1_clr, T1_gt_5_0_mS, N105, N106, N107, N108, N109, N110, N111,
         N113, T1_gt_2_5_uS, N115, T1_st_3_0_mS, N117, T1_gt_3_0_mS, N119,
         me_ps_2_5_us, N123, N124, N125, N126, N127, N128, N129, N130, N131,
         N132, N133, N134, N135, N136, N137, me_ps2_0_5_ms, N143, N144, N145,
         N146, N147, N148, N149, N161, me_cnt_100_ms, N166, N167, N168, N169,
         N170, N171, N172, N173, N185, N186, T2_gt_100_uS, N188, T2_wakeup,
         T2_gt_1_0_mS, N192, T2_gt_1_2_mS, N193, chirp_cnt_is_6, n72, n304,
         n305, n306, n307, n308, n309, n310, n311, n312, n313, n314, n315,
         n316, n317, n318, n319, n320, n321, n322, n323, n324, n325, n326,
         n327, n328, n329, n330, n331, n332, n333, n334, n335, n336, n337,
         n338, n339, n340, n341, n342, n343, n344, n345, n346, n347, n348,
         n349, n350, n351, n352, n353, n354, n355, n356, n357, n358, n359, n1,
         n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44,
         n45, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58,
         n59, n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n73,
         n74, n75, n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87,
         n88, n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100,
         n101, n102, n103, n104, n105, n106, n107, n108, n109, n110, n111,
         n112, n113, n114, n115, n116, n117, n118, n119, n120, n121, n122,
         n123, n124, n125, n126, n127, n128, n129, n130, n131, n132, n133,
         n134, n135, n136, n137, n138, n139, n140, n141, n142, n143, n144,
         n145, n146, n147, n148, n149, n150, n151, n152, n153, n154, n155,
         n156, n157, n158, n159, n160, n161, n162, n163, n164, n165, n166,
         n167, n168, n169, n170, n171, n172, n173, n174, n175, n176, n177,
         n178, n179, n180, n181, n182, n183, n184, n185, n186, n187, n188,
         n189, n190, n191, n192, n193, n194, n195, n196, n197, n198, n199,
         n200, n201, n202, n203, n204, n205, n206, n207, n208, n209, n210,
         n211, n212, n213, n214, n215, n216, n217, n218, n219, n220, n221,
         n222, n223, n224, n225, n226, n227, n228, n229, n230, n231, n232,
         n233, n234, n235, n236, n237, n238, n239, n240, n241, n242, n243,
         n244, n245, n246, n247, n248, n249, n250, n251, n252, n253, n254,
         n255, n256, n257, n258, n259, n260, n261, n262, n263, n264, n265,
         n266, n267, n268, n269, n270, n271, n272, n273, n274, n275, n276,
         n277, n278, n279, n280, n281, n282, n283, n284, n285, n286, n287,
         n288, n289, n290, n291, n292, n293, n294, n295, n296, n297, n298,
         n299, n300, n301, n302, n303, n360, n361, n362, n363, n364, n365,
         n366, n367, n368, n369, n370, n371, n372, n373, n374, n375, n376,
         n377, n378, n379, n380, n381, n382, n383, n384, n385, n386, n387,
         n388, n389, n390, n391, n392, n393, n394, n395, n396, n397, n398,
         n399;
  wire   [1:0] line_state_r;
  wire   [3:0] ps_cnt;
  wire   [7:0] idle_cnt1;
  wire   [7:0] idle_cnt1_next;
  wire   [7:0] me_ps;
  wire   [7:0] me_ps2;
  wire   [7:0] me_cnt;
  wire   [2:0] chirp_cnt;
  wire   [14:0] state;
  assign OpMode[0] = 1'b0;

  inv_2 U370 ( .ip(rst), .op(n72) );
  dp_1 \OpMode_reg[1]  ( .ip(n305), .ck(n9), .q(OpMode[1]) );
  dp_1 T2_gt_1_2_mS_reg ( .ip(N192), .ck(n4), .q(T2_gt_1_2_mS) );
  dp_1 ls_k_r_reg ( .ip(n355), .ck(n2), .q(ls_k_r) );
  dp_1 ls_j_r_reg ( .ip(n354), .ck(n2), .q(ls_j_r) );
  dp_1 ls_idle_r_reg ( .ip(ls_idle), .ck(n5), .q(ls_idle_r) );
  dp_1 \idle_cnt1_next_reg[0]  ( .ip(n55), .ck(n6), .q(idle_cnt1_next[0]) );
  dp_1 \idle_cnt1_next_reg[1]  ( .ip(N105), .ck(n6), .q(idle_cnt1_next[1]) );
  dp_1 \idle_cnt1_next_reg[2]  ( .ip(N106), .ck(n6), .q(idle_cnt1_next[2]) );
  dp_1 \idle_cnt1_next_reg[3]  ( .ip(N107), .ck(n6), .q(idle_cnt1_next[3]) );
  dp_1 \idle_cnt1_next_reg[4]  ( .ip(N108), .ck(n7), .q(idle_cnt1_next[4]) );
  dp_1 \idle_cnt1_next_reg[5]  ( .ip(N109), .ck(n7), .q(idle_cnt1_next[5]) );
  dp_1 \idle_cnt1_next_reg[6]  ( .ip(N110), .ck(n7), .q(idle_cnt1_next[6]) );
  dp_1 \idle_cnt1_next_reg[7]  ( .ip(N111), .ck(n8), .q(idle_cnt1_next[7]) );
  dp_1 TermSel_reg ( .ip(n307), .ck(n8), .q(TermSel) );
  dp_1 T2_gt_1_0_mS_reg ( .ip(N192), .ck(n4), .q(T2_gt_1_0_mS) );
  dp_1 T1_st_3_0_mS_reg ( .ip(N115), .ck(n8), .q(T1_st_3_0_mS) );
  dp_1 ls_se0_r_reg ( .ip(n353), .ck(n2), .q(ls_se0_r) );
  dp_1 XcvSelect_reg ( .ip(n306), .ck(n8), .q(XcvSelect) );
  dp_1 \chirp_cnt_reg[1]  ( .ip(n346), .ck(n9), .q(chirp_cnt[1]) );
  dp_1 ps_cnt_clr_reg ( .ip(n356), .ck(n6), .q(ps_cnt_clr) );
  dp_1 resume_req_s_reg ( .ip(resume_req_s1), .ck(n2), .q(resume_req_s) );
  dp_1 \line_state_r_reg[1]  ( .ip(LineState[1]), .ck(n2), .q(line_state_r[1])
         );
  dp_1 \ps_cnt_reg[1]  ( .ip(N86), .ck(n6), .q(ps_cnt[1]) );
  dp_1 T1_gt_2_5_uS_reg ( .ip(N113), .ck(n5), .q(T1_gt_2_5_uS) );
  dp_1 T1_gt_3_0_mS_reg ( .ip(N117), .ck(n7), .q(T1_gt_3_0_mS) );
  dp_1 \chirp_cnt_reg[0]  ( .ip(n347), .ck(n8), .q(chirp_cnt[0]) );
  dp_1 \me_ps_reg[7]  ( .ip(N137), .ck(n3), .q(me_ps[7]) );
  dp_1 idle_cnt1_clr_reg ( .ip(n357), .ck(n8), .q(idle_cnt1_clr) );
  dp_1 T1_gt_5_0_mS_reg ( .ip(N119), .ck(n8), .q(T1_gt_5_0_mS) );
  dp_1 me_ps2_0_5_ms_reg ( .ip(N161), .ck(n4), .q(me_ps2_0_5_ms) );
  dp_1 T2_wakeup_reg ( .ip(N188), .ck(n5), .q(T2_wakeup) );
  dp_1 idle_long_reg ( .ip(n350), .ck(n5), .q(idle_long) );
  dp_1 \me_ps_reg[3]  ( .ip(N133), .ck(n3), .q(me_ps[3]) );
  dp_1 \me_ps_reg[6]  ( .ip(N136), .ck(n3), .q(me_ps[6]) );
  dp_1 T2_gt_100_uS_reg ( .ip(N186), .ck(n4), .q(T2_gt_100_uS) );
  dp_1 \line_state_r_reg[0]  ( .ip(LineState[0]), .ck(n2), .q(line_state_r[0])
         );
  dp_1 me_ps_2_5_us_reg ( .ip(n358), .ck(n3), .q(me_ps_2_5_us) );
  dp_1 \ps_cnt_reg[3]  ( .ip(N88), .ck(n6), .q(ps_cnt[3]) );
  dp_1 chirp_cnt_is_6_reg ( .ip(n359), .ck(n8), .q(chirp_cnt_is_6) );
  dp_1 \state_reg[0]  ( .ip(n352), .ck(n2), .q(state[0]) );
  dp_1 \ps_cnt_reg[0]  ( .ip(N85), .ck(n5), .q(ps_cnt[0]) );
  dp_1 \chirp_cnt_reg[2]  ( .ip(n348), .ck(n8), .q(chirp_cnt[2]) );
  dp_1 \me_ps_reg[5]  ( .ip(N135), .ck(n3), .q(me_ps[5]) );
  dp_1 usb_attached_reg ( .ip(n304), .ck(n8), .q(usb_attached) );
  dp_1 \me_ps_reg[2]  ( .ip(N132), .ck(n2), .q(me_ps[2]) );
  dp_1 \state_reg[8]  ( .ip(n340), .ck(n5), .q(state[8]) );
  dp_1 \state_reg[10]  ( .ip(n342), .ck(n7), .q(state[10]) );
  dp_1 \state_reg[9]  ( .ip(n341), .ck(n5), .q(state[9]) );
  dp_1 \state_reg[14]  ( .ip(n349), .ck(n2), .q(state[14]) );
  dp_1 \state_reg[3]  ( .ip(n335), .ck(n5), .q(state[3]) );
  dp_1 \state_reg[13]  ( .ip(n345), .ck(n5), .q(state[13]) );
  dp_1 \state_reg[7]  ( .ip(n339), .ck(n7), .q(state[7]) );
  dp_1 \state_reg[2]  ( .ip(n334), .ck(n7), .q(state[2]) );
  dp_1 \me_ps_reg[1]  ( .ip(N131), .ck(n2), .q(me_ps[1]) );
  dp_1 drive_k_reg ( .ip(drive_k_d), .ck(n8), .q(drive_k) );
  dp_1 \me_ps2_reg[7]  ( .ip(n317), .ck(n4), .q(me_ps2[7]) );
  dp_1 \me_cnt_reg[7]  ( .ip(n316), .ck(n4), .q(me_cnt[7]) );
  dp_1 \me_ps_reg[4]  ( .ip(N134), .ck(n3), .q(me_ps[4]) );
  dp_1 \me_cnt_reg[6]  ( .ip(n309), .ck(n4), .q(me_cnt[6]) );
  dp_1 \me_ps2_reg[6]  ( .ip(n318), .ck(n3), .q(me_ps2[6]) );
  dp_1 \state_reg[5]  ( .ip(n337), .ck(n7), .q(state[5]) );
  dp_1 \state_reg[11]  ( .ip(n343), .ck(n5), .q(state[11]) );
  dp_1 \state_reg[12]  ( .ip(n344), .ck(n5), .q(state[12]) );
  dp_1 \state_reg[6]  ( .ip(n338), .ck(n8), .q(state[6]) );
  dp_1 \state_reg[4]  ( .ip(n336), .ck(n7), .q(state[4]) );
  dp_1 \ps_cnt_reg[2]  ( .ip(N87), .ck(n6), .q(ps_cnt[2]) );
  dp_1 \me_cnt_reg[5]  ( .ip(n310), .ck(n4), .q(me_cnt[5]) );
  dp_1 \me_ps2_reg[5]  ( .ip(n319), .ck(n3), .q(me_ps2[5]) );
  dp_1 \me_cnt_reg[2]  ( .ip(n313), .ck(n4), .q(me_cnt[2]) );
  dp_1 \me_ps2_reg[2]  ( .ip(n322), .ck(n3), .q(me_ps2[2]) );
  dp_1 usb_suspend_reg ( .ip(n308), .ck(n8), .q(usb_suspend) );
  dp_1 \idle_cnt1_reg[2]  ( .ip(n329), .ck(n6), .q(idle_cnt1[2]) );
  dp_1 me_cnt_100_ms_reg ( .ip(N193), .ck(n5), .q(me_cnt_100_ms) );
  dp_1 \idle_cnt1_reg[5]  ( .ip(n326), .ck(n7), .q(idle_cnt1[5]) );
  dp_1 \idle_cnt1_reg[6]  ( .ip(n325), .ck(n7), .q(idle_cnt1[6]) );
  dp_1 \me_ps2_reg[3]  ( .ip(n321), .ck(n3), .q(me_ps2[3]) );
  dp_1 \idle_cnt1_reg[3]  ( .ip(n328), .ck(n6), .q(idle_cnt1[3]) );
  dp_1 \me_cnt_reg[3]  ( .ip(n312), .ck(n4), .q(me_cnt[3]) );
  dp_1 \state_reg[1]  ( .ip(n333), .ck(n2), .q(state[1]) );
  dp_1 \me_cnt_reg[4]  ( .ip(n311), .ck(n4), .q(me_cnt[4]) );
  dp_1 \me_ps2_reg[4]  ( .ip(n320), .ck(n3), .q(me_ps2[4]) );
  dp_1 \me_ps_reg[0]  ( .ip(N130), .ck(n2), .q(me_ps[0]) );
  dp_1 \me_cnt_reg[1]  ( .ip(n314), .ck(n4), .q(me_cnt[1]) );
  dp_1 \idle_cnt1_reg[7]  ( .ip(n332), .ck(n6), .q(idle_cnt1[7]) );
  dp_1 \me_ps2_reg[1]  ( .ip(n323), .ck(n3), .q(me_ps2[1]) );
  dp_1 \idle_cnt1_reg[4]  ( .ip(n327), .ck(n7), .q(idle_cnt1[4]) );
  dp_1 \idle_cnt1_reg[1]  ( .ip(n330), .ck(n6), .q(idle_cnt1[1]) );
  dp_1 \me_ps2_reg[0]  ( .ip(n324), .ck(n3), .q(me_ps2[0]) );
  dp_1 \me_cnt_reg[0]  ( .ip(n315), .ck(n4), .q(me_cnt[0]) );
  dp_1 \idle_cnt1_reg[0]  ( .ip(n331), .ck(n6), .q(idle_cnt1[0]) );
  dp_1 mode_hs_reg ( .ip(n351), .ck(n5), .q(mode_hs) );
  dp_1 usb_reset_reg ( .ip(n399), .ck(n7), .q(usb_reset) );
  dp_1 resume_req_s1_reg ( .ip(resume_req), .ck(n2), .q(resume_req_s1) );
  not_ab_or_c_or_d U4 ( .ip1(n140), .ip2(n141), .ip3(usb_vbus), .ip4(n72), 
        .op(n1) );
  nor3_2 U5 ( .ip1(n229), .ip2(me_cnt_100_ms), .ip3(n231), .op(n235) );
  nor3_2 U6 ( .ip1(n72), .ip2(usb_vbus), .ip3(n139), .op(n69) );
  and3_2 U7 ( .ip1(n230), .ip2(n231), .ip3(me_ps_2_5_us), .op(n214) );
  nor4_2 U8 ( .ip1(n210), .ip2(n209), .ip3(T1_gt_5_0_mS), .ip4(idle_cnt1_clr), 
        .op(n194) );
  buf_1 U9 ( .ip(clk), .op(n2) );
  buf_1 U10 ( .ip(clk), .op(n3) );
  buf_1 U11 ( .ip(clk), .op(n4) );
  buf_1 U12 ( .ip(clk), .op(n5) );
  buf_1 U13 ( .ip(clk), .op(n6) );
  buf_1 U14 ( .ip(clk), .op(n7) );
  buf_1 U15 ( .ip(clk), .op(n8) );
  buf_1 U16 ( .ip(clk), .op(n9) );
  xor2_1 U17 ( .ip1(idle_cnt1[1]), .ip2(idle_cnt1[0]), .op(N105) );
  nand2_1 U18 ( .ip1(idle_cnt1[1]), .ip2(idle_cnt1[0]), .op(n10) );
  xnor2_1 U19 ( .ip1(n10), .ip2(idle_cnt1[2]), .op(N106) );
  nand3_1 U20 ( .ip1(idle_cnt1[1]), .ip2(idle_cnt1[0]), .ip3(idle_cnt1[2]), 
        .op(n12) );
  xor2_1 U21 ( .ip1(n12), .ip2(n16), .op(N107) );
  nor2_1 U22 ( .ip1(n12), .ip2(n16), .op(n11) );
  xor2_1 U23 ( .ip1(idle_cnt1[4]), .ip2(n11), .op(N108) );
  nor3_1 U24 ( .ip1(n16), .ip2(n12), .ip3(n386), .op(n13) );
  xor2_1 U25 ( .ip1(idle_cnt1[5]), .ip2(n13), .op(N109) );
  nand2_1 U26 ( .ip1(idle_cnt1[5]), .ip2(n13), .op(n14) );
  xor2_1 U27 ( .ip1(n14), .ip2(n53), .op(N110) );
  nor2_1 U28 ( .ip1(n14), .ip2(n53), .op(n15) );
  xor2_1 U29 ( .ip1(idle_cnt1[7]), .ip2(n15), .op(N111) );
  inv_2 U30 ( .ip(idle_cnt1[3]), .op(n16) );
  xor2_1 U31 ( .ip1(me_ps[1]), .ip2(me_ps[0]), .op(N123) );
  nand2_1 U32 ( .ip1(me_ps[1]), .ip2(me_ps[0]), .op(n17) );
  xnor2_1 U33 ( .ip1(n17), .ip2(me_ps[2]), .op(N124) );
  nand3_1 U34 ( .ip1(me_ps[1]), .ip2(me_ps[0]), .ip3(me_ps[2]), .op(n19) );
  xor2_1 U35 ( .ip1(n19), .ip2(n25), .op(N125) );
  nor2_1 U36 ( .ip1(n19), .ip2(n25), .op(n18) );
  xor2_1 U37 ( .ip1(me_ps[4]), .ip2(n18), .op(N126) );
  nor3_1 U38 ( .ip1(n25), .ip2(n19), .ip3(n24), .op(n20) );
  xor2_1 U39 ( .ip1(me_ps[5]), .ip2(n20), .op(N127) );
  nand2_1 U40 ( .ip1(me_ps[5]), .ip2(n20), .op(n21) );
  xor2_1 U41 ( .ip1(n21), .ip2(n23), .op(N128) );
  nor2_1 U42 ( .ip1(n21), .ip2(n23), .op(n22) );
  xor2_1 U43 ( .ip1(me_ps[7]), .ip2(n22), .op(N129) );
  inv_2 U44 ( .ip(me_ps[6]), .op(n23) );
  inv_2 U45 ( .ip(me_ps[4]), .op(n24) );
  inv_2 U46 ( .ip(me_ps[3]), .op(n25) );
  xor2_1 U47 ( .ip1(me_ps2[1]), .ip2(me_ps2[0]), .op(N143) );
  nand2_1 U48 ( .ip1(me_ps2[1]), .ip2(me_ps2[0]), .op(n26) );
  xnor2_1 U49 ( .ip1(n26), .ip2(me_ps2[2]), .op(N144) );
  nand3_1 U50 ( .ip1(me_ps2[1]), .ip2(me_ps2[0]), .ip3(me_ps2[2]), .op(n28) );
  xor2_1 U51 ( .ip1(n28), .ip2(n34), .op(N145) );
  nor2_1 U52 ( .ip1(n28), .ip2(n34), .op(n27) );
  xor2_1 U53 ( .ip1(me_ps2[4]), .ip2(n27), .op(N146) );
  nor3_1 U54 ( .ip1(n34), .ip2(n28), .ip3(n33), .op(n29) );
  xor2_1 U55 ( .ip1(me_ps2[5]), .ip2(n29), .op(N147) );
  nand2_1 U56 ( .ip1(me_ps2[5]), .ip2(n29), .op(n30) );
  xor2_1 U57 ( .ip1(n30), .ip2(n32), .op(N148) );
  nor2_1 U58 ( .ip1(n30), .ip2(n32), .op(n31) );
  xor2_1 U59 ( .ip1(me_ps2[7]), .ip2(n31), .op(N149) );
  inv_2 U60 ( .ip(me_ps2[6]), .op(n32) );
  inv_2 U61 ( .ip(me_ps2[4]), .op(n33) );
  inv_2 U62 ( .ip(me_ps2[3]), .op(n34) );
  xor2_1 U63 ( .ip1(me_cnt[1]), .ip2(me_cnt[0]), .op(N167) );
  nand2_1 U64 ( .ip1(me_cnt[1]), .ip2(me_cnt[0]), .op(n35) );
  xnor2_1 U65 ( .ip1(n35), .ip2(me_cnt[2]), .op(N168) );
  nand3_1 U66 ( .ip1(me_cnt[1]), .ip2(me_cnt[0]), .ip3(me_cnt[2]), .op(n37) );
  xor2_1 U67 ( .ip1(n37), .ip2(n43), .op(N169) );
  nor2_1 U68 ( .ip1(n37), .ip2(n43), .op(n36) );
  xor2_1 U69 ( .ip1(me_cnt[4]), .ip2(n36), .op(N170) );
  nor3_1 U70 ( .ip1(n43), .ip2(n37), .ip3(n42), .op(n38) );
  xor2_1 U71 ( .ip1(me_cnt[5]), .ip2(n38), .op(N171) );
  nand2_1 U72 ( .ip1(me_cnt[5]), .ip2(n38), .op(n39) );
  xor2_1 U73 ( .ip1(n39), .ip2(n41), .op(N172) );
  nor2_1 U74 ( .ip1(n39), .ip2(n41), .op(n40) );
  xor2_1 U75 ( .ip1(me_cnt[7]), .ip2(n40), .op(N173) );
  inv_2 U76 ( .ip(me_cnt[6]), .op(n41) );
  inv_2 U77 ( .ip(me_cnt[4]), .op(n42) );
  inv_2 U78 ( .ip(me_cnt[3]), .op(n43) );
  inv_2 U79 ( .ip(me_cnt[0]), .op(N166) );
  nor3_1 U80 ( .ip1(me_ps2[0]), .ip2(me_ps2[2]), .ip3(me_ps2[1]), .op(n44) );
  nor2_1 U81 ( .ip1(n44), .ip2(n34), .op(n45) );
  or2_1 U82 ( .ip1(n45), .ip2(me_ps2[4]), .op(n46) );
  ab_or_c_or_d U83 ( .ip1(me_ps2[5]), .ip2(n46), .ip3(me_ps2[7]), .ip4(
        me_ps2[6]), .op(N185) );
  and3_1 U84 ( .ip1(chirp_cnt[2]), .ip2(n47), .ip3(chirp_cnt[1]), .op(n359) );
  nor2_1 U85 ( .ip1(n48), .ip2(n49), .op(n358) );
  nand4_1 U86 ( .ip1(me_ps[7]), .ip2(me_ps[4]), .ip3(me_ps[2]), .ip4(n50), 
        .op(n49) );
  inv_1 U87 ( .ip(me_ps[0]), .op(n50) );
  or4_1 U88 ( .ip1(me_ps[3]), .ip2(me_ps[1]), .ip3(me_ps[6]), .ip4(me_ps[5]), 
        .op(n48) );
  nor4_1 U89 ( .ip1(n51), .ip2(n52), .ip3(n16), .ip4(n53), .op(n357) );
  nand4_1 U90 ( .ip1(idle_cnt1[1]), .ip2(n54), .ip3(n55), .ip4(n56), .op(n51)
         );
  inv_1 U91 ( .ip(idle_cnt1[0]), .op(n55) );
  and4_1 U92 ( .ip1(n57), .ip2(ps_cnt[0]), .ip3(ps_cnt[2]), .ip4(ps_cnt[3]), 
        .op(n356) );
  ab_or_c_or_d U93 ( .ip1(n1), .ip2(state[0]), .ip3(usb_vbus), .ip4(n72), .op(
        n352) );
  nand2_1 U94 ( .ip1(n58), .ip2(n59), .op(n351) );
  nand4_1 U95 ( .ip1(mode_hs), .ip2(n60), .ip3(n61), .ip4(n62), .op(n59) );
  and2_1 U96 ( .ip1(rst), .ip2(n63), .op(n350) );
  nand2_1 U97 ( .ip1(n64), .ip2(n65), .op(n63) );
  nand2_1 U98 ( .ip1(ls_idle_r), .ip2(n66), .op(n65) );
  or2_1 U99 ( .ip1(ls_idle), .ip2(idle_long), .op(n66) );
  nand2_1 U100 ( .ip1(idle_long), .ip2(ls_idle), .op(n64) );
  nand2_1 U101 ( .ip1(n67), .ip2(n68), .op(n349) );
  nand4_1 U102 ( .ip1(n69), .ip2(n70), .ip3(n71), .ip4(n73), .op(n68) );
  nand2_1 U103 ( .ip1(n1), .ip2(state[14]), .op(n67) );
  mux2_1 U104 ( .ip1(n74), .ip2(n75), .s(chirp_cnt[2]), .op(n348) );
  nand2_1 U105 ( .ip1(n76), .ip2(n77), .op(n75) );
  nand2_1 U106 ( .ip1(n78), .ip2(n79), .op(n77) );
  inv_1 U107 ( .ip(n80), .op(n78) );
  nor3_1 U108 ( .ip1(n47), .ip2(n79), .ip3(n80), .op(n74) );
  mux2_1 U109 ( .ip1(n47), .ip2(n81), .s(n80), .op(n347) );
  nor2_1 U110 ( .ip1(n82), .ip2(n47), .op(n81) );
  mux2_1 U111 ( .ip1(n83), .ip2(n84), .s(n79), .op(n346) );
  inv_1 U112 ( .ip(chirp_cnt[1]), .op(n79) );
  nor2_1 U113 ( .ip1(n80), .ip2(n47), .op(n84) );
  inv_1 U114 ( .ip(chirp_cnt[0]), .op(n47) );
  inv_1 U115 ( .ip(n76), .op(n83) );
  mux2_1 U116 ( .ip1(chirp_cnt[0]), .ip2(n82), .s(n80), .op(n76) );
  nand2_1 U117 ( .ip1(n85), .ip2(n73), .op(n80) );
  inv_1 U118 ( .ip(chirp_cnt_is_6), .op(n73) );
  nand2_1 U119 ( .ip1(n86), .ip2(n87), .op(n85) );
  nand2_1 U120 ( .ip1(n88), .ip2(n89), .op(n87) );
  nand2_1 U121 ( .ip1(n90), .ip2(n91), .op(n86) );
  inv_1 U122 ( .ip(n92), .op(n82) );
  nand2_1 U123 ( .ip1(n93), .ip2(n94), .op(n345) );
  nand3_1 U124 ( .ip1(n69), .ip2(n71), .ip3(chirp_cnt_is_6), .op(n94) );
  nand2_1 U125 ( .ip1(n1), .ip2(state[13]), .op(n93) );
  nand2_1 U126 ( .ip1(n95), .ip2(n96), .op(n344) );
  nand3_1 U127 ( .ip1(n97), .ip2(n91), .ip3(n69), .op(n96) );
  nand2_1 U128 ( .ip1(n1), .ip2(state[12]), .op(n95) );
  nand2_1 U129 ( .ip1(n98), .ip2(n99), .op(n343) );
  nand2_1 U130 ( .ip1(n69), .ip2(n100), .op(n99) );
  nand2_1 U131 ( .ip1(n92), .ip2(n101), .op(n100) );
  nand2_1 U132 ( .ip1(n97), .ip2(n88), .op(n101) );
  nand2_1 U133 ( .ip1(n1), .ip2(state[11]), .op(n98) );
  nand2_1 U134 ( .ip1(n102), .ip2(n103), .op(n342) );
  nand2_1 U135 ( .ip1(n399), .ip2(n69), .op(n103) );
  inv_1 U136 ( .ip(n60), .op(n399) );
  nand2_1 U137 ( .ip1(n1), .ip2(state[10]), .op(n102) );
  nand2_1 U138 ( .ip1(n104), .ip2(n105), .op(n341) );
  or2_1 U139 ( .ip1(n106), .ip2(n107), .op(n105) );
  nand2_1 U140 ( .ip1(n1), .ip2(state[9]), .op(n104) );
  nand2_1 U141 ( .ip1(n108), .ip2(n109), .op(n340) );
  or2_1 U142 ( .ip1(n61), .ip2(n106), .op(n109) );
  nand2_1 U143 ( .ip1(n1), .ip2(state[8]), .op(n108) );
  nand2_1 U144 ( .ip1(n110), .ip2(n111), .op(n339) );
  nand2_1 U145 ( .ip1(n69), .ip2(n112), .op(n111) );
  nand2_1 U146 ( .ip1(n1), .ip2(state[7]), .op(n110) );
  nand2_1 U147 ( .ip1(n113), .ip2(n114), .op(n338) );
  nand2_1 U148 ( .ip1(n69), .ip2(n115), .op(n114) );
  nand2_1 U149 ( .ip1(n1), .ip2(state[6]), .op(n113) );
  nand2_1 U150 ( .ip1(n116), .ip2(n117), .op(n337) );
  nand2_1 U151 ( .ip1(n118), .ip2(n119), .op(n117) );
  nand2_1 U152 ( .ip1(n1), .ip2(state[5]), .op(n116) );
  ab_or_c_or_d U153 ( .ip1(n118), .ip2(n90), .ip3(n120), .ip4(n121), .op(n336)
         );
  and2_1 U154 ( .ip1(state[4]), .ip2(n1), .op(n121) );
  nor2_1 U155 ( .ip1(n106), .ip2(n122), .op(n120) );
  inv_1 U156 ( .ip(n69), .op(n106) );
  inv_1 U157 ( .ip(n119), .op(n90) );
  and3_1 U158 ( .ip1(n123), .ip2(n124), .ip3(n69), .op(n118) );
  nand2_1 U159 ( .ip1(n125), .ip2(n126), .op(n335) );
  nand2_1 U160 ( .ip1(n69), .ip2(n127), .op(n126) );
  nand2_1 U161 ( .ip1(n128), .ip2(n129), .op(n127) );
  nand2_1 U162 ( .ip1(n130), .ip2(n131), .op(n129) );
  nand2_1 U163 ( .ip1(n1), .ip2(state[3]), .op(n125) );
  nand2_1 U164 ( .ip1(n132), .ip2(n133), .op(n334) );
  nand3_1 U165 ( .ip1(n134), .ip2(n135), .ip3(n69), .op(n133) );
  nand2_1 U166 ( .ip1(n1), .ip2(state[2]), .op(n132) );
  nand2_1 U167 ( .ip1(n136), .ip2(n137), .op(n333) );
  nand2_1 U168 ( .ip1(n69), .ip2(n138), .op(n137) );
  inv_1 U169 ( .ip(n140), .op(n139) );
  nand2_1 U170 ( .ip1(n1), .ip2(state[1]), .op(n136) );
  nand4_1 U171 ( .ip1(n61), .ip2(n142), .ip3(n143), .ip4(n144), .op(n141) );
  nor3_1 U172 ( .ip1(n145), .ip2(n71), .ip3(n138), .op(n144) );
  nand4_1 U173 ( .ip1(n146), .ip2(n58), .ip3(n147), .ip4(n62), .op(n138) );
  nand2_1 U174 ( .ip1(n148), .ip2(n149), .op(n71) );
  nand3_1 U175 ( .ip1(n150), .ip2(n60), .ip3(n151), .op(n145) );
  nor3_1 U176 ( .ip1(n123), .ip2(drive_k_d), .ip3(n112), .op(n143) );
  not_ab_or_c_or_d U177 ( .ip1(n152), .ip2(n153), .ip3(n154), .ip4(n155), .op(
        n140) );
  nor3_1 U178 ( .ip1(n151), .ip2(n156), .ip3(n157), .op(n155) );
  nor3_1 U179 ( .ip1(n158), .ip2(n159), .ip3(n160), .op(n154) );
  nand2_1 U180 ( .ip1(n161), .ip2(n162), .op(n153) );
  nand2_1 U181 ( .ip1(n123), .ip2(n163), .op(n162) );
  nand2_1 U182 ( .ip1(resume_req_s), .ip2(T1_gt_5_0_mS), .op(n163) );
  inv_1 U183 ( .ip(n164), .op(n123) );
  nand2_1 U184 ( .ip1(n161), .ip2(n165), .op(n152) );
  nand2_1 U185 ( .ip1(n119), .ip2(n124), .op(n165) );
  and4_1 U186 ( .ip1(n166), .ip2(n167), .ip3(n168), .ip4(n169), .op(n161) );
  nor4_1 U187 ( .ip1(n170), .ip2(n171), .ip3(n172), .ip4(n173), .op(n169) );
  nor2_1 U188 ( .ip1(me_cnt_100_ms), .ip2(n147), .op(n173) );
  nor2_1 U189 ( .ip1(T2_wakeup), .ip2(n174), .op(n172) );
  nor2_1 U190 ( .ip1(n353), .ip2(n142), .op(n171) );
  nor2_1 U191 ( .ip1(T2_gt_1_2_mS), .ip2(n92), .op(n170) );
  nor2_1 U192 ( .ip1(n175), .ip2(n176), .op(n168) );
  nor2_1 U193 ( .ip1(n70), .ip2(n58), .op(n176) );
  nor2_1 U194 ( .ip1(T2_gt_100_uS), .ip2(n146), .op(n175) );
  nand4_1 U195 ( .ip1(state[6]), .ip2(n177), .ip3(n178), .ip4(n179), .op(n146)
         );
  nand2_1 U196 ( .ip1(n97), .ip2(n180), .op(n167) );
  nand2_1 U197 ( .ip1(n181), .ip2(n182), .op(n180) );
  nand2_1 U198 ( .ip1(n88), .ip2(n183), .op(n182) );
  inv_1 U199 ( .ip(n149), .op(n88) );
  nand4_1 U200 ( .ip1(state[12]), .ip2(n184), .ip3(n185), .ip4(n186), .op(n149) );
  nand2_1 U201 ( .ip1(n91), .ip2(n119), .op(n181) );
  nand2_1 U202 ( .ip1(ls_k_r), .ip2(n355), .op(n119) );
  nor2_1 U203 ( .ip1(n187), .ip2(line_state_r[0]), .op(n355) );
  inv_1 U204 ( .ip(n148), .op(n91) );
  nand4_1 U205 ( .ip1(state[11]), .ip2(n184), .ip3(n185), .ip4(n188), .op(n148) );
  nor2_1 U206 ( .ip1(n70), .ip2(chirp_cnt_is_6), .op(n97) );
  nand2_1 U207 ( .ip1(n189), .ip2(n190), .op(n166) );
  nand2_1 U208 ( .ip1(n60), .ip2(n122), .op(n189) );
  nand2_1 U209 ( .ip1(n191), .ip2(n192), .op(n332) );
  nand2_1 U210 ( .ip1(n193), .ip2(idle_cnt1[7]), .op(n192) );
  nand2_1 U211 ( .ip1(idle_cnt1_next[7]), .ip2(n194), .op(n191) );
  nand2_1 U212 ( .ip1(n195), .ip2(n196), .op(n331) );
  nand2_1 U213 ( .ip1(idle_cnt1[0]), .ip2(n193), .op(n196) );
  nand2_1 U214 ( .ip1(idle_cnt1_next[0]), .ip2(n194), .op(n195) );
  nand2_1 U215 ( .ip1(n197), .ip2(n198), .op(n330) );
  nand2_1 U216 ( .ip1(n193), .ip2(idle_cnt1[1]), .op(n198) );
  nand2_1 U217 ( .ip1(idle_cnt1_next[1]), .ip2(n194), .op(n197) );
  nand2_1 U218 ( .ip1(n199), .ip2(n200), .op(n329) );
  nand2_1 U219 ( .ip1(idle_cnt1[2]), .ip2(n193), .op(n200) );
  nand2_1 U220 ( .ip1(idle_cnt1_next[2]), .ip2(n194), .op(n199) );
  nand2_1 U221 ( .ip1(n201), .ip2(n202), .op(n328) );
  nand2_1 U222 ( .ip1(n193), .ip2(idle_cnt1[3]), .op(n202) );
  nand2_1 U223 ( .ip1(idle_cnt1_next[3]), .ip2(n194), .op(n201) );
  nand2_1 U224 ( .ip1(n203), .ip2(n204), .op(n327) );
  nand2_1 U225 ( .ip1(n193), .ip2(idle_cnt1[4]), .op(n204) );
  nand2_1 U226 ( .ip1(idle_cnt1_next[4]), .ip2(n194), .op(n203) );
  nand2_1 U227 ( .ip1(n205), .ip2(n206), .op(n326) );
  nand2_1 U228 ( .ip1(n193), .ip2(idle_cnt1[5]), .op(n206) );
  nand2_1 U229 ( .ip1(idle_cnt1_next[5]), .ip2(n194), .op(n205) );
  nand2_1 U230 ( .ip1(n207), .ip2(n208), .op(n325) );
  nand2_1 U231 ( .ip1(n193), .ip2(idle_cnt1[6]), .op(n208) );
  nor3_1 U232 ( .ip1(n194), .ip2(idle_cnt1_clr), .ip3(n209), .op(n193) );
  nand2_1 U233 ( .ip1(idle_cnt1_next[6]), .ip2(n194), .op(n207) );
  inv_1 U234 ( .ip(ps_cnt_clr), .op(n210) );
  nand2_1 U235 ( .ip1(n211), .ip2(n212), .op(n324) );
  nand2_1 U236 ( .ip1(me_ps2[0]), .ip2(n213), .op(n212) );
  nand2_1 U237 ( .ip1(n366), .ip2(n214), .op(n211) );
  nand2_1 U238 ( .ip1(n215), .ip2(n216), .op(n323) );
  nand2_1 U239 ( .ip1(me_ps2[1]), .ip2(n213), .op(n216) );
  nand2_1 U240 ( .ip1(N143), .ip2(n214), .op(n215) );
  nand2_1 U241 ( .ip1(n217), .ip2(n218), .op(n322) );
  nand2_1 U242 ( .ip1(me_ps2[2]), .ip2(n213), .op(n218) );
  nand2_1 U243 ( .ip1(N144), .ip2(n214), .op(n217) );
  nand2_1 U244 ( .ip1(n219), .ip2(n220), .op(n321) );
  nand2_1 U245 ( .ip1(me_ps2[3]), .ip2(n213), .op(n220) );
  nand2_1 U246 ( .ip1(N145), .ip2(n214), .op(n219) );
  nand2_1 U247 ( .ip1(n221), .ip2(n222), .op(n320) );
  nand2_1 U248 ( .ip1(me_ps2[4]), .ip2(n213), .op(n222) );
  nand2_1 U249 ( .ip1(N146), .ip2(n214), .op(n221) );
  nand2_1 U250 ( .ip1(n223), .ip2(n224), .op(n319) );
  nand2_1 U251 ( .ip1(me_ps2[5]), .ip2(n213), .op(n224) );
  nand2_1 U252 ( .ip1(N147), .ip2(n214), .op(n223) );
  nand2_1 U253 ( .ip1(n225), .ip2(n226), .op(n318) );
  nand2_1 U254 ( .ip1(me_ps2[6]), .ip2(n213), .op(n226) );
  nand2_1 U255 ( .ip1(N148), .ip2(n214), .op(n225) );
  nand2_1 U256 ( .ip1(n227), .ip2(n228), .op(n317) );
  nand2_1 U257 ( .ip1(me_ps2[7]), .ip2(n213), .op(n228) );
  nor3_1 U258 ( .ip1(n214), .ip2(me_ps2_0_5_ms), .ip3(n229), .op(n213) );
  nand2_1 U259 ( .ip1(N149), .ip2(n214), .op(n227) );
  nand2_1 U260 ( .ip1(n232), .ip2(n233), .op(n316) );
  nand2_1 U261 ( .ip1(me_cnt[7]), .ip2(n234), .op(n233) );
  nand2_1 U262 ( .ip1(N173), .ip2(n235), .op(n232) );
  nand2_1 U263 ( .ip1(n236), .ip2(n237), .op(n315) );
  nand2_1 U264 ( .ip1(me_cnt[0]), .ip2(n234), .op(n237) );
  nand2_1 U265 ( .ip1(N166), .ip2(n235), .op(n236) );
  nand2_1 U266 ( .ip1(n238), .ip2(n239), .op(n314) );
  nand2_1 U267 ( .ip1(me_cnt[1]), .ip2(n234), .op(n239) );
  nand2_1 U268 ( .ip1(N167), .ip2(n235), .op(n238) );
  nand2_1 U269 ( .ip1(n240), .ip2(n241), .op(n313) );
  nand2_1 U270 ( .ip1(me_cnt[2]), .ip2(n234), .op(n241) );
  nand2_1 U271 ( .ip1(N168), .ip2(n235), .op(n240) );
  nand2_1 U272 ( .ip1(n242), .ip2(n243), .op(n312) );
  nand2_1 U273 ( .ip1(me_cnt[3]), .ip2(n234), .op(n243) );
  nand2_1 U274 ( .ip1(N169), .ip2(n235), .op(n242) );
  nand2_1 U275 ( .ip1(n244), .ip2(n245), .op(n311) );
  nand2_1 U276 ( .ip1(me_cnt[4]), .ip2(n234), .op(n245) );
  nand2_1 U277 ( .ip1(N170), .ip2(n235), .op(n244) );
  nand2_1 U278 ( .ip1(n246), .ip2(n247), .op(n310) );
  nand2_1 U279 ( .ip1(me_cnt[5]), .ip2(n234), .op(n247) );
  nand2_1 U280 ( .ip1(N171), .ip2(n235), .op(n246) );
  nand2_1 U281 ( .ip1(n248), .ip2(n249), .op(n309) );
  nand2_1 U282 ( .ip1(me_cnt[6]), .ip2(n234), .op(n249) );
  nor2_1 U283 ( .ip1(n229), .ip2(n235), .op(n234) );
  nand2_1 U284 ( .ip1(N172), .ip2(n235), .op(n248) );
  inv_1 U285 ( .ip(me_ps2_0_5_ms), .op(n231) );
  nor2_1 U286 ( .ip1(suspend_clr), .ip2(n250), .op(n308) );
  nor2_1 U287 ( .ip1(usb_suspend), .ip2(n251), .op(n250) );
  nand4_1 U288 ( .ip1(n252), .ip2(n61), .ip3(n142), .ip4(n174), .op(
        suspend_clr) );
  or2_1 U289 ( .ip1(n164), .ip2(n124), .op(n252) );
  nor2_1 U290 ( .ip1(n253), .ip2(n254), .op(n307) );
  nor3_1 U291 ( .ip1(n255), .ip2(TermSel), .ip3(n256), .op(n253) );
  nand2_1 U292 ( .ip1(n257), .ip2(n258), .op(n306) );
  nand3_1 U293 ( .ip1(n259), .ip2(n60), .ip3(XcvSelect), .op(n258) );
  inv_1 U294 ( .ip(n254), .op(n259) );
  nand2_1 U295 ( .ip1(n58), .ip2(n260), .op(n254) );
  nand2_1 U296 ( .ip1(n261), .ip2(mode_hs), .op(n260) );
  inv_1 U297 ( .ip(n256), .op(n257) );
  nand2_1 U298 ( .ip1(n262), .ip2(n62), .op(n256) );
  nand2_1 U299 ( .ip1(n263), .ip2(n264), .op(n305) );
  nand4_1 U300 ( .ip1(OpMode[1]), .ip2(n265), .ip3(n58), .ip4(n62), .op(n264)
         );
  nand4_1 U301 ( .ip1(n266), .ip2(n267), .ip3(state[14]), .ip4(n268), .op(n62)
         );
  and4_1 U302 ( .ip1(n269), .ip2(n270), .ip3(n271), .ip4(n272), .op(n268) );
  nand4_1 U303 ( .ip1(n273), .ip2(n185), .ip3(state[13]), .ip4(n274), .op(n58)
         );
  nor3_1 U304 ( .ip1(state[11]), .ip2(state[1]), .ip3(state[12]), .op(n274) );
  inv_1 U305 ( .ip(state[10]), .op(n185) );
  inv_1 U306 ( .ip(n255), .op(n263) );
  nand2_1 U307 ( .ip1(n60), .ip2(n275), .op(n255) );
  nand2_1 U308 ( .ip1(T2_wakeup), .ip2(n112), .op(n275) );
  nand2_1 U309 ( .ip1(n276), .ip2(n277), .op(n304) );
  nand2_1 U310 ( .ip1(me_cnt_100_ms), .ip2(n278), .op(n277) );
  nand2_1 U311 ( .ip1(usb_attached), .ip2(n61), .op(n276) );
  mux2_1 U312 ( .ip1(n353), .ip2(n354), .s(n279), .op(ls_idle) );
  nand2_1 U313 ( .ip1(n122), .ip2(n92), .op(drive_k_d) );
  nand4_1 U314 ( .ip1(state[10]), .ip2(n184), .ip3(n186), .ip4(n188), .op(n92)
         );
  inv_1 U315 ( .ip(state[12]), .op(n188) );
  inv_1 U316 ( .ip(state[11]), .op(n186) );
  nor3_1 U317 ( .ip1(state[13]), .ip2(state[1]), .ip3(n280), .op(n184) );
  nand4_1 U318 ( .ip1(n281), .ip2(n267), .ip3(state[7]), .ip4(n282), .op(n122)
         );
  nor4_1 U319 ( .ip1(state[6]), .ip2(state[5]), .ip3(state[4]), .ip4(state[3]), 
        .op(n282) );
  nand2_1 U320 ( .ip1(n283), .ip2(n284), .op(SuspendM) );
  nand2_1 U321 ( .ip1(LineState[1]), .ip2(n285), .op(n284) );
  inv_1 U322 ( .ip(LineState[0]), .op(n285) );
  nand2_1 U323 ( .ip1(usb_suspend), .ip2(n286), .op(n283) );
  inv_1 U324 ( .ip(resume_req_s), .op(n286) );
  mux2_1 U325 ( .ip1(n287), .ip2(n288), .s(ps_cnt[3]), .op(N88) );
  nand2_1 U326 ( .ip1(n289), .ip2(n290), .op(n288) );
  or2_1 U327 ( .ip1(n291), .ip2(ps_cnt[2]), .op(n290) );
  and2_1 U328 ( .ip1(ps_cnt[2]), .ip2(n292), .op(n287) );
  mux2_1 U329 ( .ip1(n292), .ip2(n293), .s(ps_cnt[2]), .op(N87) );
  inv_1 U330 ( .ip(n289), .op(n293) );
  nor2_1 U331 ( .ip1(N85), .ip2(n294), .op(n289) );
  nor2_1 U332 ( .ip1(n291), .ip2(ps_cnt[1]), .op(n294) );
  nor3_1 U333 ( .ip1(n291), .ip2(n295), .ip3(n57), .op(n292) );
  mux2_1 U334 ( .ip1(N85), .ip2(n296), .s(n57), .op(N86) );
  inv_1 U335 ( .ip(ps_cnt[1]), .op(n57) );
  nor2_1 U336 ( .ip1(n295), .ip2(n291), .op(n296) );
  inv_1 U337 ( .ip(ps_cnt[0]), .op(n295) );
  nor2_1 U338 ( .ip1(n291), .ip2(ps_cnt[0]), .op(N85) );
  or2_1 U339 ( .ip1(n209), .ip2(ps_cnt_clr), .op(n291) );
  or2_1 U340 ( .ip1(n297), .ip2(n298), .op(n209) );
  nor4_1 U341 ( .ip1(n299), .ip2(n300), .ip3(me_cnt[1]), .ip4(me_cnt[0]), .op(
        N193) );
  or3_1 U342 ( .ip1(me_cnt[4]), .ip2(me_cnt[5]), .ip3(me_cnt[2]), .op(n300) );
  nand4_1 U343 ( .ip1(me_cnt[6]), .ip2(me_cnt[3]), .ip3(me_cnt[7]), .ip4(n230), 
        .op(n299) );
  nor2_1 U344 ( .ip1(n301), .ip2(n229), .op(N192) );
  nor3_1 U345 ( .ip1(n302), .ip2(me_cnt[3]), .ip3(n303), .op(n301) );
  nor2_1 U346 ( .ip1(n360), .ip2(n229), .op(N188) );
  nor2_1 U347 ( .ip1(n361), .ip2(n302), .op(n360) );
  or4_1 U348 ( .ip1(me_cnt[4]), .ip2(me_cnt[5]), .ip3(me_cnt[6]), .ip4(
        me_cnt[7]), .op(n302) );
  and2_1 U349 ( .ip1(n303), .ip2(me_cnt[3]), .op(n361) );
  nand2_1 U350 ( .ip1(n362), .ip2(n363), .op(n303) );
  nand2_1 U351 ( .ip1(me_cnt[1]), .ip2(me_cnt[0]), .op(n363) );
  inv_1 U352 ( .ip(me_cnt[2]), .op(n362) );
  and2_1 U353 ( .ip1(N185), .ip2(n230), .op(N186) );
  inv_1 U354 ( .ip(n229), .op(n230) );
  nor4_1 U355 ( .ip1(n364), .ip2(n365), .ip3(me_ps2[2]), .ip4(me_ps2[1]), .op(
        N161) );
  or3_1 U356 ( .ip1(me_ps2[5]), .ip2(me_ps2_0_5_ms), .ip3(me_ps2[4]), .op(n365) );
  nand4_1 U357 ( .ip1(me_ps2[7]), .ip2(me_ps2[6]), .ip3(me_ps2[3]), .ip4(n366), 
        .op(n364) );
  inv_1 U358 ( .ip(me_ps2[0]), .op(n366) );
  and2_1 U359 ( .ip1(N129), .ip2(n367), .op(N137) );
  and2_1 U360 ( .ip1(N128), .ip2(n367), .op(N136) );
  and2_1 U361 ( .ip1(N127), .ip2(n367), .op(N135) );
  and2_1 U362 ( .ip1(N126), .ip2(n367), .op(N134) );
  and2_1 U363 ( .ip1(N125), .ip2(n367), .op(N133) );
  and2_1 U364 ( .ip1(N124), .ip2(n367), .op(N132) );
  and2_1 U365 ( .ip1(N123), .ip2(n367), .op(N131) );
  and2_1 U366 ( .ip1(n50), .ip2(n367), .op(N130) );
  nor2_1 U367 ( .ip1(n229), .ip2(me_ps_2_5_us), .op(n367) );
  nand3_1 U368 ( .ip1(n107), .ip2(n262), .ip3(n368), .op(n229) );
  not_ab_or_c_or_d U369 ( .ip1(T2_wakeup), .ip2(n112), .ip3(n261), .ip4(n369), 
        .op(n368) );
  nor2_1 U371 ( .ip1(n60), .ip2(n190), .op(n369) );
  inv_1 U372 ( .ip(T2_gt_1_0_mS), .op(n190) );
  nand4_1 U373 ( .ip1(state[9]), .ip2(n266), .ip3(n281), .ip4(n370), .op(n60)
         );
  inv_1 U374 ( .ip(state[8]), .op(n370) );
  inv_1 U375 ( .ip(n265), .op(n261) );
  nand2_1 U376 ( .ip1(n115), .ip2(n353), .op(n265) );
  inv_1 U377 ( .ip(n142), .op(n115) );
  nand4_1 U378 ( .ip1(state[4]), .ip2(n177), .ip3(n179), .ip4(n371), .op(n142)
         );
  inv_1 U379 ( .ip(state[5]), .op(n179) );
  inv_1 U380 ( .ip(n174), .op(n112) );
  nand4_1 U381 ( .ip1(state[5]), .ip2(n177), .ip3(n178), .ip4(n371), .op(n174)
         );
  inv_1 U382 ( .ip(state[6]), .op(n371) );
  inv_1 U383 ( .ip(state[4]), .op(n178) );
  and4_1 U384 ( .ip1(n281), .ip2(n267), .ip3(n372), .ip4(n373), .op(n177) );
  inv_1 U385 ( .ip(state[7]), .op(n373) );
  and2_1 U386 ( .ip1(n61), .ip2(n374), .op(n262) );
  nand2_1 U387 ( .ip1(n159), .ip2(n375), .op(n374) );
  and2_1 U388 ( .ip1(mode_hs), .ip2(T1_gt_3_0_mS), .op(n159) );
  nand3_1 U389 ( .ip1(n376), .ip2(n269), .ip3(state[0]), .op(n61) );
  not_ab_or_c_or_d U390 ( .ip1(n156), .ip2(n130), .ip3(n377), .ip4(n378), .op(
        n107) );
  nor2_1 U391 ( .ip1(n150), .ip2(n379), .op(n378) );
  nor2_1 U392 ( .ip1(n124), .ip2(n164), .op(n377) );
  nand4_1 U393 ( .ip1(state[3]), .ip2(n380), .ip3(n281), .ip4(n267), .op(n164)
         );
  nand2_1 U394 ( .ip1(T1_gt_2_5_uS), .ip2(n70), .op(n124) );
  inv_1 U395 ( .ip(n131), .op(n156) );
  nand2_1 U396 ( .ip1(T2_gt_100_uS), .ip2(n70), .op(n131) );
  and2_1 U397 ( .ip1(ls_se0_r), .ip2(n353), .op(n70) );
  nor2_1 U398 ( .ip1(line_state_r[0]), .ip2(line_state_r[1]), .op(n353) );
  nor2_1 U399 ( .ip1(n381), .ip2(n297), .op(N119) );
  nor2_1 U400 ( .ip1(n382), .ip2(idle_cnt1[7]), .op(n381) );
  nor2_1 U401 ( .ip1(n383), .ip2(n53), .op(n382) );
  nor2_1 U402 ( .ip1(n384), .ip2(idle_cnt1[5]), .op(n383) );
  nor2_1 U403 ( .ip1(n385), .ip2(n386), .op(n384) );
  nor2_1 U404 ( .ip1(n387), .ip2(n297), .op(N117) );
  not_ab_or_c_or_d U405 ( .ip1(n54), .ip2(n388), .ip3(idle_cnt1[7]), .ip4(
        idle_cnt1[6]), .op(n387) );
  inv_1 U406 ( .ip(n385), .op(n388) );
  nor4_1 U407 ( .ip1(idle_cnt1[0]), .ip2(idle_cnt1[1]), .ip3(idle_cnt1[2]), 
        .ip4(idle_cnt1[3]), .op(n385) );
  nor4_1 U408 ( .ip1(idle_cnt1[7]), .ip2(idle_cnt1[6]), .ip3(n54), .ip4(n297), 
        .op(N115) );
  nor2_1 U409 ( .ip1(n389), .ip2(n386), .op(n54) );
  inv_1 U410 ( .ip(idle_cnt1[4]), .op(n386) );
  nor2_1 U411 ( .ip1(n390), .ip2(n297), .op(N113) );
  or2_1 U412 ( .ip1(n251), .ip2(n278), .op(n297) );
  inv_1 U413 ( .ip(n147), .op(n278) );
  nand4_1 U414 ( .ip1(state[8]), .ip2(n266), .ip3(n281), .ip4(n391), .op(n147)
         );
  inv_1 U415 ( .ip(state[9]), .op(n391) );
  and3_1 U416 ( .ip1(n272), .ip2(n271), .ip3(n392), .op(n281) );
  nor3_1 U417 ( .ip1(state[14]), .ip2(state[2]), .ip3(state[1]), .op(n392) );
  nand2_1 U418 ( .ip1(n128), .ip2(n393), .op(n251) );
  nand2_1 U419 ( .ip1(n157), .ip2(n130), .op(n393) );
  inv_1 U420 ( .ip(n151), .op(n130) );
  nand3_1 U421 ( .ip1(n376), .ip2(n271), .ip3(state[2]), .op(n151) );
  and3_1 U422 ( .ip1(n272), .ip2(n270), .ip3(n394), .op(n376) );
  inv_1 U423 ( .ip(state[1]), .op(n270) );
  and2_1 U424 ( .ip1(n89), .ip2(T2_gt_100_uS), .op(n157) );
  inv_1 U425 ( .ip(n183), .op(n89) );
  nand2_1 U426 ( .ip1(ls_j_r), .ip2(n354), .op(n183) );
  and2_1 U427 ( .ip1(line_state_r[0]), .ip2(n187), .op(n354) );
  inv_1 U428 ( .ip(line_state_r[1]), .op(n187) );
  nand2_1 U429 ( .ip1(n160), .ip2(n134), .op(n128) );
  inv_1 U430 ( .ip(n158), .op(n134) );
  nand2_1 U431 ( .ip1(n375), .ip2(n379), .op(n158) );
  nand4_1 U432 ( .ip1(T1_st_3_0_mS), .ip2(T1_gt_2_5_uS), .ip3(n298), .ip4(n279), .op(n379) );
  inv_1 U433 ( .ip(idle_long), .op(n298) );
  inv_1 U434 ( .ip(n150), .op(n375) );
  nand3_1 U435 ( .ip1(n273), .ip2(n272), .ip3(state[1]), .op(n150) );
  nor4_1 U436 ( .ip1(state[10]), .ip2(state[11]), .ip3(state[12]), .ip4(
        state[13]), .op(n272) );
  inv_1 U437 ( .ip(n280), .op(n273) );
  nand3_1 U438 ( .ip1(n271), .ip2(n269), .ip3(n394), .op(n280) );
  and3_1 U439 ( .ip1(n267), .ip2(n395), .ip3(n266), .op(n394) );
  and2_1 U440 ( .ip1(n380), .ip2(n372), .op(n266) );
  inv_1 U441 ( .ip(state[3]), .op(n372) );
  nor4_1 U442 ( .ip1(state[4]), .ip2(state[5]), .ip3(state[6]), .ip4(state[7]), 
        .op(n380) );
  inv_1 U443 ( .ip(state[14]), .op(n395) );
  nor2_1 U444 ( .ip1(state[8]), .ip2(state[9]), .op(n267) );
  inv_1 U445 ( .ip(state[2]), .op(n269) );
  inv_1 U446 ( .ip(state[0]), .op(n271) );
  inv_1 U447 ( .ip(n135), .op(n160) );
  nand2_1 U448 ( .ip1(T1_gt_3_0_mS), .ip2(n279), .op(n135) );
  inv_1 U449 ( .ip(mode_hs), .op(n279) );
  not_ab_or_c_or_d U450 ( .ip1(idle_cnt1[3]), .ip2(n396), .ip3(n397), .ip4(
        idle_cnt1[4]), .op(n390) );
  nand3_1 U451 ( .ip1(n53), .ip2(n52), .ip3(n389), .op(n397) );
  inv_1 U452 ( .ip(idle_cnt1[5]), .op(n389) );
  inv_1 U453 ( .ip(idle_cnt1[7]), .op(n52) );
  inv_1 U454 ( .ip(idle_cnt1[6]), .op(n53) );
  nand2_1 U455 ( .ip1(n56), .ip2(n398), .op(n396) );
  nand2_1 U456 ( .ip1(idle_cnt1[0]), .ip2(idle_cnt1[1]), .op(n398) );
  inv_1 U457 ( .ip(idle_cnt1[2]), .op(n56) );
endmodule


module usbf_utmi_if ( phy_clk, rst, DataOut, TxValid, TxReady, RxValid, 
        RxActive, RxError, DataIn, XcvSelect, TermSel, SuspendM, LineState, 
        OpMode, usb_vbus, rx_data, rx_valid, rx_active, rx_err, tx_data, 
        tx_valid, tx_valid_last, tx_ready, tx_first, mode_hs, usb_reset, 
        usb_suspend, usb_attached, resume_req, suspend_clr );
  output [7:0] DataOut;
  input [7:0] DataIn;
  input [1:0] LineState;
  output [1:0] OpMode;
  output [7:0] rx_data;
  input [7:0] tx_data;
  input phy_clk, rst, TxReady, RxValid, RxActive, RxError, usb_vbus, tx_valid,
         tx_valid_last, tx_first, resume_req;
  output TxValid, XcvSelect, TermSel, SuspendM, rx_valid, rx_active, rx_err,
         tx_ready, mode_hs, usb_reset, usb_suspend, usb_attached, suspend_clr;
  wire   N11, N14, N17, drive_k, drive_k_r, N35, n2, n3, n4, n5, n6, n7, n8,
         n9, n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21, n22,
         n23, n24, n25, n26, n27, n28, n29, n30, n1;
  wire   SYNOPSYS_UNCONNECTED__0;
  assign OpMode[0] = 1'b0;

  nand2_2 U3 ( .ip1(n3), .ip2(n4), .op(n23) );
  nand2_2 U4 ( .ip1(tx_data[7]), .ip2(n5), .op(n4) );
  nand2_2 U5 ( .ip1(DataOut[7]), .ip2(n6), .op(n3) );
  nand2_2 U6 ( .ip1(n7), .ip2(n8), .op(n24) );
  nand2_2 U7 ( .ip1(tx_data[6]), .ip2(n5), .op(n8) );
  nand2_2 U8 ( .ip1(DataOut[6]), .ip2(n6), .op(n7) );
  nand2_2 U9 ( .ip1(n9), .ip2(n10), .op(n25) );
  nand2_2 U10 ( .ip1(tx_data[5]), .ip2(n5), .op(n10) );
  nand2_2 U11 ( .ip1(DataOut[5]), .ip2(n6), .op(n9) );
  nand2_2 U12 ( .ip1(n11), .ip2(n12), .op(n26) );
  nand2_2 U13 ( .ip1(tx_data[4]), .ip2(n5), .op(n12) );
  nand2_2 U14 ( .ip1(DataOut[4]), .ip2(n6), .op(n11) );
  nand2_2 U15 ( .ip1(n13), .ip2(n14), .op(n27) );
  nand2_2 U16 ( .ip1(tx_data[3]), .ip2(n5), .op(n14) );
  nand2_2 U17 ( .ip1(DataOut[3]), .ip2(n6), .op(n13) );
  nand2_2 U18 ( .ip1(n15), .ip2(n16), .op(n28) );
  nand2_2 U19 ( .ip1(tx_data[2]), .ip2(n5), .op(n16) );
  nand2_2 U20 ( .ip1(DataOut[2]), .ip2(n6), .op(n15) );
  nand2_2 U21 ( .ip1(n17), .ip2(n18), .op(n29) );
  nand2_2 U22 ( .ip1(tx_data[1]), .ip2(n5), .op(n18) );
  nand2_2 U23 ( .ip1(DataOut[1]), .ip2(n6), .op(n17) );
  nand2_2 U24 ( .ip1(n19), .ip2(n20), .op(n30) );
  nand2_2 U25 ( .ip1(tx_data[0]), .ip2(n5), .op(n20) );
  nand2_2 U26 ( .ip1(DataOut[0]), .ip2(n6), .op(n19) );
  nor2_2 U27 ( .ip1(n5), .ip2(drive_k), .op(n6) );
  nor2_2 U29 ( .ip1(n21), .ip2(n2), .op(N35) );
  nor4_2 U30 ( .ip1(tx_valid_last), .ip2(tx_valid), .ip3(drive_k), .ip4(n22), 
        .op(n21) );
  and2_2 U32 ( .ip1(RxError), .ip2(rst), .op(N17) );
  and2_2 U33 ( .ip1(RxActive), .ip2(rst), .op(N14) );
  and2_2 U34 ( .ip1(RxValid), .ip2(rst), .op(N11) );
  inv_2 U36 ( .ip(rst), .op(n2) );
  usbf_utmi_ls u0 ( .clk(phy_clk), .rst(rst), .resume_req(resume_req), 
        .rx_active(rx_active), .tx_ready(tx_ready), .drive_k(drive_k), 
        .XcvSelect(XcvSelect), .TermSel(TermSel), .SuspendM(SuspendM), 
        .LineState(LineState), .OpMode({OpMode[1], SYNOPSYS_UNCONNECTED__0}), 
        .usb_vbus(usb_vbus), .mode_hs(mode_hs), .usb_reset(usb_reset), 
        .usb_suspend(usb_suspend), .usb_attached(usb_attached), .suspend_clr(
        suspend_clr) );
  dp_1 \DataOut_reg[3]  ( .ip(n27), .ck(phy_clk), .q(DataOut[3]) );
  dp_1 \DataOut_reg[2]  ( .ip(n28), .ck(phy_clk), .q(DataOut[2]) );
  dp_1 \DataOut_reg[1]  ( .ip(n29), .ck(phy_clk), .q(DataOut[1]) );
  dp_1 \DataOut_reg[0]  ( .ip(n30), .ck(phy_clk), .q(DataOut[0]) );
  dp_1 \DataOut_reg[7]  ( .ip(n23), .ck(phy_clk), .q(DataOut[7]) );
  dp_1 \DataOut_reg[6]  ( .ip(n24), .ck(phy_clk), .q(DataOut[6]) );
  dp_1 \DataOut_reg[5]  ( .ip(n25), .ck(phy_clk), .q(DataOut[5]) );
  dp_1 \DataOut_reg[4]  ( .ip(n26), .ck(phy_clk), .q(DataOut[4]) );
  dp_1 drive_k_r_reg ( .ip(drive_k), .ck(phy_clk), .q(drive_k_r) );
  dp_1 rx_err_reg ( .ip(N17), .ck(phy_clk), .q(rx_err) );
  dp_1 TxValid_reg ( .ip(N35), .ck(phy_clk), .q(TxValid) );
  dp_1 rx_valid_reg ( .ip(N11), .ck(phy_clk), .q(rx_valid) );
  dp_1 \rx_data_reg[6]  ( .ip(DataIn[6]), .ck(phy_clk), .q(rx_data[6]) );
  dp_1 \rx_data_reg[5]  ( .ip(DataIn[5]), .ck(phy_clk), .q(rx_data[5]) );
  dp_1 \rx_data_reg[3]  ( .ip(DataIn[3]), .ck(phy_clk), .q(rx_data[3]) );
  dp_1 \rx_data_reg[1]  ( .ip(DataIn[1]), .ck(phy_clk), .q(rx_data[1]) );
  dp_1 tx_ready_reg ( .ip(TxReady), .ck(phy_clk), .q(tx_ready) );
  dp_1 \rx_data_reg[7]  ( .ip(DataIn[7]), .ck(phy_clk), .q(rx_data[7]) );
  dp_1 \rx_data_reg[4]  ( .ip(DataIn[4]), .ck(phy_clk), .q(rx_data[4]) );
  dp_1 \rx_data_reg[2]  ( .ip(DataIn[2]), .ck(phy_clk), .q(rx_data[2]) );
  dp_1 \rx_data_reg[0]  ( .ip(DataIn[0]), .ck(phy_clk), .q(rx_data[0]) );
  dp_1 rx_active_reg ( .ip(N14), .ck(phy_clk), .q(rx_active) );
  or2_2 U28 ( .ip1(TxReady), .ip2(tx_first), .op(n5) );
  nor3_2 U31 ( .ip1(n1), .ip2(drive_k_r), .ip3(TxReady), .op(n22) );
  inv_2 U37 ( .ip(TxValid), .op(n1) );
endmodule


module usbf_crc5 ( crc_in, din, crc_out );
  input [4:0] crc_in;
  input [10:0] din;
  output [4:0] crc_out;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18;

  xor2_2 U1 ( .ip1(n1), .ip2(n2), .op(crc_out[4]) );
  xor2_2 U2 ( .ip1(din[5]), .ip2(din[4]), .op(n1) );
  xor2_2 U3 ( .ip1(n5), .ip2(n6), .op(crc_out[3]) );
  xor2_2 U4 ( .ip1(n7), .ip2(n8), .op(n6) );
  xor2_2 U5 ( .ip1(n9), .ip2(n10), .op(crc_out[2]) );
  xor2_2 U6 ( .ip1(n4), .ip2(n8), .op(n10) );
  xor2_2 U7 ( .ip1(n11), .ip2(n12), .op(n8) );
  xor2_2 U8 ( .ip1(din[2]), .ip2(n7), .op(n4) );
  xor2_2 U9 ( .ip1(din[8]), .ip2(crc_in[2]), .op(n7) );
  xor2_2 U10 ( .ip1(n13), .ip2(n14), .op(crc_out[1]) );
  xor2_2 U11 ( .ip1(n5), .ip2(n15), .op(n14) );
  xor2_2 U12 ( .ip1(din[1]), .ip2(din[4]), .op(n5) );
  xor2_2 U13 ( .ip1(n11), .ip2(n16), .op(n13) );
  xor2_2 U14 ( .ip1(n9), .ip2(n17), .op(crc_out[0]) );
  xor2_2 U15 ( .ip1(din[3]), .ip2(n3), .op(n12) );
  xor2_2 U16 ( .ip1(n18), .ip2(n16), .op(n3) );
  xor2_2 U17 ( .ip1(din[9]), .ip2(crc_in[3]), .op(n18) );
  xor2_2 U18 ( .ip1(din[0]), .ip2(n15), .op(n9) );
  xor2_2 U19 ( .ip1(din[6]), .ip2(crc_in[0]), .op(n15) );
  xnor2_1 U20 ( .ip1(n3), .ip2(n4), .op(n2) );
  xnor2_1 U21 ( .ip1(din[7]), .ip2(crc_in[1]), .op(n11) );
  xnor2_1 U22 ( .ip1(n12), .ip2(din[5]), .op(n17) );
  xnor2_1 U23 ( .ip1(din[10]), .ip2(crc_in[4]), .op(n16) );
endmodule


module usbf_crc16_0 ( crc_in, din, crc_out );
  input [15:0] crc_in;
  input [7:0] din;
  output [15:0] crc_out;
  wire   \crc_in[6] , \crc_in[5] , \crc_in[4] , \crc_in[3] , \crc_in[2] , n1,
         n2, n3, n4, n5, n6, n7, n8, n9, n10, n11;
  assign crc_out[14] = \crc_in[6] ;
  assign \crc_in[6]  = crc_in[6];
  assign crc_out[13] = \crc_in[5] ;
  assign \crc_in[5]  = crc_in[5];
  assign crc_out[12] = \crc_in[4] ;
  assign \crc_in[4]  = crc_in[4];
  assign crc_out[11] = \crc_in[3] ;
  assign \crc_in[3]  = crc_in[3];
  assign crc_out[10] = \crc_in[2] ;
  assign \crc_in[2]  = crc_in[2];

  xor2_2 U1 ( .ip1(crc_in[1]), .ip2(n1), .op(crc_out[9]) );
  xor2_2 U2 ( .ip1(crc_in[0]), .ip2(n2), .op(crc_out[8]) );
  xor2_2 U3 ( .ip1(n7), .ip2(n8), .op(crc_out[3]) );
  xor2_2 U4 ( .ip1(crc_in[7]), .ip2(crc_out[0]), .op(crc_out[15]) );
  xor2_2 U5 ( .ip1(n9), .ip2(crc_out[1]), .op(crc_out[0]) );
  xor2_2 U6 ( .ip1(crc_out[4]), .ip2(n7), .op(n11) );
  xor2_2 U7 ( .ip1(din[3]), .ip2(crc_in[11]), .op(n5) );
  xor2_2 U8 ( .ip1(n2), .ip2(crc_out[6]), .op(n10) );
  xor2_2 U9 ( .ip1(din[5]), .ip2(crc_in[13]), .op(n3) );
  xor2_2 U10 ( .ip1(din[7]), .ip2(crc_in[15]), .op(n1) );
  xor2_2 U11 ( .ip1(din[0]), .ip2(crc_in[8]), .op(n9) );
  xnor2_1 U12 ( .ip1(n3), .ip2(n4), .op(crc_out[7]) );
  xnor2_1 U13 ( .ip1(n5), .ip2(n6), .op(crc_out[5]) );
  xnor2_1 U14 ( .ip1(n9), .ip2(n7), .op(crc_out[2]) );
  xnor2_1 U15 ( .ip1(n10), .ip2(n11), .op(crc_out[1]) );
  xnor2_1 U16 ( .ip1(din[1]), .ip2(crc_in[9]), .op(n7) );
  xnor2_1 U17 ( .ip1(n5), .ip2(n8), .op(crc_out[4]) );
  xnor2_1 U18 ( .ip1(din[2]), .ip2(crc_in[10]), .op(n8) );
  xnor2_1 U19 ( .ip1(n3), .ip2(n6), .op(crc_out[6]) );
  xnor2_1 U20 ( .ip1(din[4]), .ip2(crc_in[12]), .op(n6) );
  xnor2_1 U21 ( .ip1(n1), .ip2(n4), .op(n2) );
  xnor2_1 U22 ( .ip1(din[6]), .ip2(crc_in[14]), .op(n4) );
endmodule


module usbf_pd ( clk, rst, rx_data, rx_valid, rx_active, rx_err, pid_OUT, 
        pid_IN, pid_SOF, pid_SETUP, pid_DATA0, pid_DATA1, pid_DATA2, pid_MDATA, 
        pid_ACK, pid_NACK, pid_STALL, pid_NYET, pid_PRE, pid_ERR, pid_SPLIT, 
        pid_PING, pid_cks_err, token_fadr, token_endp, token_valid, crc5_err, 
        frame_no, rx_data_st, rx_data_valid, rx_data_done, crc16_err, seq_err
 );
  input [7:0] rx_data;
  output [6:0] token_fadr;
  output [3:0] token_endp;
  output [10:0] frame_no;
  output [7:0] rx_data_st;
  input clk, rst, rx_valid, rx_active, rx_err;
  output pid_OUT, pid_IN, pid_SOF, pid_SETUP, pid_DATA0, pid_DATA1, pid_DATA2,
         pid_MDATA, pid_ACK, pid_NACK, pid_STALL, pid_NYET, pid_PRE, pid_ERR,
         pid_SPLIT, pid_PING, pid_cks_err, token_valid, crc5_err,
         rx_data_valid, rx_data_done, crc16_err, seq_err;
  wire   \token_endp[3] , \token_endp[2] , \token_endp[1] , \token_endp[0] ,
         \token_fadr[6] , \token_fadr[5] , \token_fadr[4] , \token_fadr[3] ,
         \token_fadr[2] , \token_fadr[1] , \token_fadr[0] , token_valid_r1,
         N29, N30, rxv1, rxv2, N50, rx_active_r, N221, n39, n44, n45, n46, n47,
         n48, n49, n50, n51, n52, n53, n54, n56, n57, n58, n59, n60, n62, n63,
         n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75, n76, n77,
         n78, n79, n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90, n91,
         n92, n93, n94, n95, n96, n97, n98, n99, n100, n101, n102, n103, n104,
         n105, n106, n107, n108, n109, n110, n111, n112, n113, n114, n115,
         n116, n117, n118, n119, n120, n121, n122, n123, n124, n125, n126,
         n127, n128, n129, n130, n131, n132, n133, n134, n135, n136, n137,
         n138, n139, n140, n141, n142, n143, n144, n145, n146, n147, n148,
         n149, n150, n151, n152, n153, n154, n155, n156, n157, n158, n159,
         n160, n161, n162, n163, n164, n165, n166, n167, n168, n169, n170,
         n171, n172, n173, n174, n175, n176, n177, n178, n179, n180, n181,
         n182, n183, n184, n185, n186, n187, n188, n189, n190, n191, n192,
         n193, n194, n195, n196, n197, n198, n199, n200, n201, n202, n203,
         n204, n205, n206, n207, n208, n209, n210, n211, n212, n213, n214,
         n215, n216, n217, n218, n219, n220, n221, n222, n223, n224, n225,
         n226, n227, n228, n229, n230, n231, n232, n233, n234, n235, n236,
         n237, n238, n239, n240, n241, n242, n243, n244, n245, n246, n247,
         n248, n249, n250, n251, n252, n253, n254, n255, n256, n257, n258,
         n259, n260, n261, n262, n263, n264, n265, n266, n267, n268, n269,
         n270, n271, n272, n273, n274, n275, n276, n277, n278, n279, n280,
         n281, n282, n283, n284, n285, n286, n287, n288, n289, n290, n291,
         n292, n293, n294, n295, n296, n297, n298, pid_PRE, \ne_2397/A[0] ,
         \ne_2397/A[1] , \ne_2397/A[2] , \ne_2397/A[3] , \ne_2397/A[4] , n1,
         n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32, n33, n34, n35, n36, n37, n38, n41, n42, n43, n55, n61, n299,
         n300, n301, n302, n303, n304, n305, n306, n307, n308, n309, n310,
         n311, n312, n313, n314, n315, n316, n317, n318, n319;
  wire   [7:0] pid;
  wire   [7:3] token1;
  wire   [4:0] crc5_out;
  wire   [7:0] d0;
  wire   [7:0] d1;
  wire   [15:0] crc16_sum;
  wire   [15:0] crc16_out;
  wire   [3:0] state;
  assign frame_no[10] = \token_endp[3] ;
  assign token_endp[3] = \token_endp[3] ;
  assign frame_no[9] = \token_endp[2] ;
  assign token_endp[2] = \token_endp[2] ;
  assign frame_no[8] = \token_endp[1] ;
  assign token_endp[1] = \token_endp[1] ;
  assign frame_no[7] = \token_endp[0] ;
  assign token_endp[0] = \token_endp[0] ;
  assign frame_no[6] = \token_fadr[6] ;
  assign token_fadr[6] = \token_fadr[6] ;
  assign frame_no[5] = \token_fadr[5] ;
  assign token_fadr[5] = \token_fadr[5] ;
  assign frame_no[4] = \token_fadr[4] ;
  assign token_fadr[4] = \token_fadr[4] ;
  assign frame_no[3] = \token_fadr[3] ;
  assign token_fadr[3] = \token_fadr[3] ;
  assign frame_no[2] = \token_fadr[2] ;
  assign token_fadr[2] = \token_fadr[2] ;
  assign frame_no[1] = \token_fadr[1] ;
  assign token_fadr[1] = \token_fadr[1] ;
  assign frame_no[0] = \token_fadr[0] ;
  assign token_fadr[0] = \token_fadr[0] ;
  assign pid_MDATA = N221;
  assign pid_ERR = pid_PRE;

  nor2_2 U3 ( .ip1(rx_err), .ip2(n44), .op(seq_err) );
  nor2_2 U5 ( .ip1(n45), .ip2(n46), .op(n44) );
  nor2_2 U6 ( .ip1(n318), .ip2(n42), .op(n46) );
  nor2_2 U7 ( .ip1(n37), .ip2(n47), .op(n45) );
  nand4_2 U8 ( .ip1(n48), .ip2(n49), .ip3(n50), .ip4(n51), .op(pid_cks_err) );
  xor2_2 U9 ( .ip1(pid[5]), .ip2(pid[1]), .op(n51) );
  xor2_2 U10 ( .ip1(pid[7]), .ip2(pid[3]), .op(n50) );
  xor2_2 U11 ( .ip1(pid[6]), .ip2(pid[2]), .op(n49) );
  xor2_2 U12 ( .ip1(pid[4]), .ip2(pid[0]), .op(n48) );
  nand2_2 U13 ( .ip1(n52), .ip2(n53), .op(n229) );
  or3_2 U14 ( .ip1(n54), .ip2(n20), .ip3(n39), .op(n53) );
  nand2_2 U15 ( .ip1(rxv2), .ip2(n54), .op(n52) );
  nand2_2 U16 ( .ip1(n57), .ip2(n58), .op(n230) );
  nand2_2 U17 ( .ip1(rst), .ip2(n5), .op(n58) );
  nand3_2 U18 ( .ip1(rxv1), .ip2(n56), .ip3(rst), .op(n57) );
  nand2_2 U19 ( .ip1(n59), .ip2(n60), .op(n231) );
  nand2_2 U20 ( .ip1(token1[7]), .ip2(n2), .op(n60) );
  nand2_2 U21 ( .ip1(rx_data[7]), .ip2(n43), .op(n59) );
  nand2_2 U22 ( .ip1(n62), .ip2(n63), .op(n232) );
  nand2_2 U23 ( .ip1(token1[6]), .ip2(n2), .op(n63) );
  nand2_2 U24 ( .ip1(rx_data[6]), .ip2(n43), .op(n62) );
  nand2_2 U25 ( .ip1(n64), .ip2(n65), .op(n233) );
  nand2_2 U26 ( .ip1(token1[5]), .ip2(n2), .op(n65) );
  nand2_2 U27 ( .ip1(rx_data[5]), .ip2(n43), .op(n64) );
  nand2_2 U28 ( .ip1(n66), .ip2(n67), .op(n234) );
  nand2_2 U29 ( .ip1(token1[4]), .ip2(n2), .op(n67) );
  nand2_2 U30 ( .ip1(rx_data[4]), .ip2(n43), .op(n66) );
  nand2_2 U31 ( .ip1(n68), .ip2(n69), .op(n235) );
  nand2_2 U32 ( .ip1(token1[3]), .ip2(n2), .op(n69) );
  nand2_2 U33 ( .ip1(rx_data[3]), .ip2(n43), .op(n68) );
  nand2_2 U34 ( .ip1(n70), .ip2(n71), .op(n236) );
  nand2_2 U35 ( .ip1(\token_endp[3] ), .ip2(n2), .op(n71) );
  nand2_2 U36 ( .ip1(rx_data[2]), .ip2(n43), .op(n70) );
  nand2_2 U37 ( .ip1(n72), .ip2(n73), .op(n237) );
  nand2_2 U38 ( .ip1(\token_endp[2] ), .ip2(n2), .op(n73) );
  nand2_2 U39 ( .ip1(rx_data[1]), .ip2(n43), .op(n72) );
  nand2_2 U40 ( .ip1(n74), .ip2(n75), .op(n238) );
  nand2_2 U41 ( .ip1(\token_endp[1] ), .ip2(n2), .op(n75) );
  nand2_2 U42 ( .ip1(rx_data[0]), .ip2(n43), .op(n74) );
  nor2_2 U44 ( .ip1(n5), .ip2(n315), .op(n79) );
  nor2_2 U45 ( .ip1(n5), .ip2(n314), .op(n80) );
  nor2_2 U46 ( .ip1(n5), .ip2(n313), .op(n81) );
  nor2_2 U47 ( .ip1(n5), .ip2(n312), .op(n82) );
  nor2_2 U48 ( .ip1(n5), .ip2(n311), .op(n83) );
  nor2_2 U49 ( .ip1(n5), .ip2(n310), .op(n84) );
  nor2_2 U50 ( .ip1(n5), .ip2(n309), .op(n85) );
  nor2_2 U51 ( .ip1(n5), .ip2(n308), .op(n86) );
  nor2_2 U52 ( .ip1(n5), .ip2(n307), .op(n87) );
  nor2_2 U53 ( .ip1(n5), .ip2(n306), .op(n88) );
  nor2_2 U54 ( .ip1(n5), .ip2(n305), .op(n89) );
  and2_2 U55 ( .ip1(n19), .ip2(crc16_sum[3]), .op(n90) );
  and2_2 U56 ( .ip1(n19), .ip2(crc16_sum[2]), .op(n91) );
  nor2_2 U57 ( .ip1(n5), .ip2(n304), .op(n92) );
  and2_2 U58 ( .ip1(n18), .ip2(crc16_sum[0]), .op(n93) );
  and2_2 U59 ( .ip1(n18), .ip2(crc16_sum[15]), .op(n94) );
  nand2_2 U61 ( .ip1(n95), .ip2(n96), .op(n255) );
  nand2_2 U62 ( .ip1(rx_data_st[7]), .ip2(n6), .op(n96) );
  nand2_2 U63 ( .ip1(d1[7]), .ip2(n4), .op(n95) );
  nand2_2 U64 ( .ip1(n97), .ip2(n98), .op(n256) );
  nand2_2 U65 ( .ip1(rx_data_st[6]), .ip2(n6), .op(n98) );
  nand2_2 U66 ( .ip1(d1[6]), .ip2(n4), .op(n97) );
  nand2_2 U67 ( .ip1(n99), .ip2(n100), .op(n257) );
  nand2_2 U68 ( .ip1(rx_data_st[5]), .ip2(n7), .op(n100) );
  nand2_2 U69 ( .ip1(d1[5]), .ip2(n4), .op(n99) );
  nand2_2 U70 ( .ip1(n101), .ip2(n102), .op(n258) );
  nand2_2 U71 ( .ip1(rx_data_st[4]), .ip2(n7), .op(n102) );
  nand2_2 U72 ( .ip1(d1[4]), .ip2(n4), .op(n101) );
  nand2_2 U73 ( .ip1(n103), .ip2(n104), .op(n259) );
  nand2_2 U74 ( .ip1(rx_data_st[3]), .ip2(n8), .op(n104) );
  nand2_2 U75 ( .ip1(d1[3]), .ip2(n4), .op(n103) );
  nand2_2 U76 ( .ip1(n105), .ip2(n106), .op(n260) );
  nand2_2 U77 ( .ip1(rx_data_st[2]), .ip2(n8), .op(n106) );
  nand2_2 U78 ( .ip1(d1[2]), .ip2(n4), .op(n105) );
  nand2_2 U79 ( .ip1(n107), .ip2(n108), .op(n261) );
  nand2_2 U80 ( .ip1(rx_data_st[1]), .ip2(n9), .op(n108) );
  nand2_2 U81 ( .ip1(d1[1]), .ip2(n4), .op(n107) );
  nand2_2 U82 ( .ip1(n109), .ip2(n110), .op(n262) );
  nand2_2 U83 ( .ip1(rx_data_st[0]), .ip2(n9), .op(n110) );
  nand2_2 U84 ( .ip1(d1[0]), .ip2(n4), .op(n109) );
  nand2_2 U85 ( .ip1(n111), .ip2(n112), .op(n263) );
  nand2_2 U86 ( .ip1(d1[7]), .ip2(n10), .op(n112) );
  nand2_2 U87 ( .ip1(d0[7]), .ip2(n4), .op(n111) );
  nand2_2 U88 ( .ip1(n113), .ip2(n114), .op(n264) );
  nand2_2 U89 ( .ip1(d1[6]), .ip2(n10), .op(n114) );
  nand2_2 U90 ( .ip1(d0[6]), .ip2(n4), .op(n113) );
  nand2_2 U91 ( .ip1(n115), .ip2(n116), .op(n265) );
  nand2_2 U92 ( .ip1(d1[5]), .ip2(n11), .op(n116) );
  nand2_2 U93 ( .ip1(d0[5]), .ip2(n4), .op(n115) );
  nand2_2 U94 ( .ip1(n117), .ip2(n118), .op(n266) );
  nand2_2 U95 ( .ip1(d1[4]), .ip2(n11), .op(n118) );
  nand2_2 U96 ( .ip1(d0[4]), .ip2(n4), .op(n117) );
  nand2_2 U97 ( .ip1(n119), .ip2(n120), .op(n267) );
  nand2_2 U98 ( .ip1(d1[3]), .ip2(n12), .op(n120) );
  nand2_2 U99 ( .ip1(d0[3]), .ip2(n3), .op(n119) );
  nand2_2 U100 ( .ip1(n121), .ip2(n122), .op(n268) );
  nand2_2 U101 ( .ip1(d1[2]), .ip2(n12), .op(n122) );
  nand2_2 U102 ( .ip1(d0[2]), .ip2(n3), .op(n121) );
  nand2_2 U103 ( .ip1(n123), .ip2(n124), .op(n269) );
  nand2_2 U104 ( .ip1(d1[1]), .ip2(n13), .op(n124) );
  nand2_2 U105 ( .ip1(d0[1]), .ip2(n3), .op(n123) );
  nand2_2 U106 ( .ip1(n125), .ip2(n126), .op(n270) );
  nand2_2 U107 ( .ip1(d1[0]), .ip2(n14), .op(n126) );
  nand2_2 U108 ( .ip1(d0[0]), .ip2(n3), .op(n125) );
  nand2_2 U109 ( .ip1(n127), .ip2(n128), .op(n271) );
  nand2_2 U110 ( .ip1(d0[7]), .ip2(n14), .op(n128) );
  nand2_2 U111 ( .ip1(rx_data[7]), .ip2(n3), .op(n127) );
  nand2_2 U112 ( .ip1(n129), .ip2(n130), .op(n272) );
  nand2_2 U113 ( .ip1(d0[6]), .ip2(n15), .op(n130) );
  nand2_2 U114 ( .ip1(rx_data[6]), .ip2(n3), .op(n129) );
  nand2_2 U115 ( .ip1(n131), .ip2(n132), .op(n273) );
  nand2_2 U116 ( .ip1(d0[5]), .ip2(n15), .op(n132) );
  nand2_2 U117 ( .ip1(rx_data[5]), .ip2(n3), .op(n131) );
  nand2_2 U118 ( .ip1(n133), .ip2(n134), .op(n274) );
  nand2_2 U119 ( .ip1(d0[4]), .ip2(n13), .op(n134) );
  nand2_2 U120 ( .ip1(rx_data[4]), .ip2(n3), .op(n133) );
  nand2_2 U121 ( .ip1(n135), .ip2(n136), .op(n275) );
  nand2_2 U122 ( .ip1(d0[3]), .ip2(n16), .op(n136) );
  nand2_2 U123 ( .ip1(rx_data[3]), .ip2(n3), .op(n135) );
  nand2_2 U124 ( .ip1(n137), .ip2(n138), .op(n276) );
  nand2_2 U125 ( .ip1(d0[2]), .ip2(n17), .op(n138) );
  nand2_2 U126 ( .ip1(rx_data[2]), .ip2(n3), .op(n137) );
  nand2_2 U127 ( .ip1(n139), .ip2(n140), .op(n277) );
  nand2_2 U128 ( .ip1(d0[1]), .ip2(n16), .op(n140) );
  nand2_2 U129 ( .ip1(rx_data[1]), .ip2(n3), .op(n139) );
  nand2_2 U130 ( .ip1(n141), .ip2(n142), .op(n278) );
  nand2_2 U131 ( .ip1(d0[0]), .ip2(n17), .op(n142) );
  nand2_2 U132 ( .ip1(rx_data[0]), .ip2(n3), .op(n141) );
  nand2_2 U133 ( .ip1(n143), .ip2(n144), .op(n279) );
  nand2_2 U134 ( .ip1(\token_endp[0] ), .ip2(n34), .op(n144) );
  nand2_2 U135 ( .ip1(n145), .ip2(rx_data[7]), .op(n143) );
  nand2_2 U136 ( .ip1(n146), .ip2(n147), .op(n280) );
  nand2_2 U137 ( .ip1(\token_fadr[6] ), .ip2(n34), .op(n147) );
  nand2_2 U138 ( .ip1(n145), .ip2(rx_data[6]), .op(n146) );
  nand2_2 U139 ( .ip1(n148), .ip2(n149), .op(n281) );
  nand2_2 U140 ( .ip1(\token_fadr[5] ), .ip2(n34), .op(n149) );
  nand2_2 U141 ( .ip1(n145), .ip2(rx_data[5]), .op(n148) );
  nand2_2 U142 ( .ip1(n150), .ip2(n151), .op(n282) );
  nand2_2 U143 ( .ip1(\token_fadr[4] ), .ip2(n34), .op(n151) );
  nand2_2 U144 ( .ip1(n145), .ip2(rx_data[4]), .op(n150) );
  nand2_2 U145 ( .ip1(n152), .ip2(n153), .op(n283) );
  nand2_2 U146 ( .ip1(\token_fadr[3] ), .ip2(n34), .op(n153) );
  nand2_2 U147 ( .ip1(n145), .ip2(rx_data[3]), .op(n152) );
  nand2_2 U148 ( .ip1(n154), .ip2(n155), .op(n284) );
  nand2_2 U149 ( .ip1(\token_fadr[2] ), .ip2(n34), .op(n155) );
  nand2_2 U150 ( .ip1(n145), .ip2(rx_data[2]), .op(n154) );
  nand2_2 U151 ( .ip1(n156), .ip2(n157), .op(n285) );
  nand2_2 U152 ( .ip1(\token_fadr[1] ), .ip2(n34), .op(n157) );
  nand2_2 U153 ( .ip1(n145), .ip2(rx_data[1]), .op(n156) );
  nand2_2 U154 ( .ip1(n158), .ip2(n159), .op(n286) );
  nand2_2 U155 ( .ip1(\token_fadr[0] ), .ip2(n34), .op(n159) );
  nand2_2 U156 ( .ip1(n145), .ip2(rx_data[0]), .op(n158) );
  and2_2 U157 ( .ip1(rx_data[7]), .ip2(n162), .op(n161) );
  and2_2 U158 ( .ip1(rx_data[6]), .ip2(n162), .op(n163) );
  and2_2 U159 ( .ip1(rx_data[5]), .ip2(n162), .op(n164) );
  and2_2 U160 ( .ip1(rx_data[4]), .ip2(n162), .op(n165) );
  nand2_2 U161 ( .ip1(n166), .ip2(n167), .op(n291) );
  nand2_2 U162 ( .ip1(n162), .ip2(rx_data[3]), .op(n167) );
  nand2_2 U163 ( .ip1(n160), .ip2(pid[3]), .op(n166) );
  nand2_2 U164 ( .ip1(n168), .ip2(n169), .op(n292) );
  nand2_2 U165 ( .ip1(n162), .ip2(rx_data[2]), .op(n169) );
  nand2_2 U166 ( .ip1(n160), .ip2(pid[2]), .op(n168) );
  nand2_2 U167 ( .ip1(n170), .ip2(n171), .op(n293) );
  nand2_2 U168 ( .ip1(n162), .ip2(rx_data[1]), .op(n171) );
  nand2_2 U169 ( .ip1(n160), .ip2(pid[1]), .op(n170) );
  nand2_2 U170 ( .ip1(n172), .ip2(n173), .op(n294) );
  nand2_2 U171 ( .ip1(n162), .ip2(rx_data[0]), .op(n173) );
  nand2_2 U172 ( .ip1(n160), .ip2(pid[0]), .op(n172) );
  nor2_2 U173 ( .ip1(n39), .ip2(n162), .op(n160) );
  nand2_2 U175 ( .ip1(n316), .ip2(n175), .op(n174) );
  nand2_2 U176 ( .ip1(n41), .ip2(rx_active), .op(n175) );
  nand2_2 U177 ( .ip1(n176), .ip2(n177), .op(n295) );
  nand2_2 U178 ( .ip1(n178), .ip2(n41), .op(n177) );
  nand2_2 U179 ( .ip1(state[1]), .ip2(n180), .op(n176) );
  nand2_2 U180 ( .ip1(n181), .ip2(n182), .op(n296) );
  nand2_2 U181 ( .ip1(n178), .ip2(n145), .op(n182) );
  nand2_2 U183 ( .ip1(state[2]), .ip2(n180), .op(n181) );
  nand2_2 U184 ( .ip1(n184), .ip2(n185), .op(n297) );
  nand3_2 U185 ( .ip1(n186), .ip2(n187), .ip3(n178), .op(n185) );
  nor2_2 U186 ( .ip1(n39), .ip2(n188), .op(n178) );
  nand2_2 U187 ( .ip1(state[3]), .ip2(n180), .op(n184) );
  nand2_2 U188 ( .ip1(n35), .ip2(n189), .op(n180) );
  nand2_2 U189 ( .ip1(rst), .ip2(n190), .op(n189) );
  nand2_2 U190 ( .ip1(n36), .ip2(n191), .op(n190) );
  or3_2 U191 ( .ip1(n37), .ip2(n317), .ip3(n47), .op(n191) );
  nand3_2 U192 ( .ip1(n183), .ip2(n192), .ip3(n193), .op(n47) );
  nand2_2 U193 ( .ip1(n76), .ip2(n194), .op(n183) );
  nand2_2 U194 ( .ip1(n195), .ip2(n196), .op(n194) );
  or2_2 U195 ( .ip1(n197), .ip2(pid[1]), .op(n196) );
  nand2_2 U196 ( .ip1(n199), .ip2(n200), .op(n298) );
  nand2_2 U197 ( .ip1(n188), .ip2(state[0]), .op(n200) );
  nand2_2 U198 ( .ip1(n201), .ip2(n35), .op(n199) );
  and3_2 U199 ( .ip1(rx_valid), .ip2(n202), .ip3(rx_active), .op(n203) );
  nand2_2 U200 ( .ip1(rx_active), .ip2(n204), .op(n202) );
  nand2_2 U201 ( .ip1(n205), .ip2(n38), .op(n204) );
  nand2_2 U202 ( .ip1(n206), .ip2(n207), .op(n205) );
  nand2_2 U203 ( .ip1(n42), .ip2(n192), .op(n207) );
  nand2_2 U204 ( .ip1(n76), .ip2(n37), .op(n206) );
  nand3_2 U205 ( .ip1(rst), .ip2(n208), .ip3(n209), .op(n201) );
  nand2_2 U206 ( .ip1(state[0]), .ip2(n198), .op(n208) );
  nand2_2 U207 ( .ip1(n211), .ip2(n212), .op(n198) );
  or2_2 U208 ( .ip1(n193), .ip2(n186), .op(n212) );
  nand2_2 U209 ( .ip1(n318), .ip2(n213), .op(n193) );
  nand2_2 U210 ( .ip1(rx_valid), .ip2(n301), .op(n213) );
  nand4_2 U211 ( .ip1(n179), .ip2(n38), .ip3(n37), .ip4(n42), .op(n211) );
  nor4_2 U212 ( .ip1(n61), .ip2(state[0]), .ip3(state[1]), .ip4(state[3]), 
        .op(n77) );
  nand4_2 U213 ( .ip1(state[0]), .ip2(n299), .ip3(n61), .ip4(n55), .op(n179)
         );
  nor2_2 U214 ( .ip1(n197), .ip2(n215), .op(pid_PING) );
  nor2_2 U215 ( .ip1(n197), .ip2(n195), .op(pid_SOF) );
  nor2_2 U217 ( .ip1(n197), .ip2(n216), .op(pid_DATA2) );
  nor2_2 U218 ( .ip1(pid[3]), .ip2(n301), .op(pid_DATA0) );
  nor2_2 U219 ( .ip1(n217), .ip2(n197), .op(pid_NYET) );
  nand2_2 U220 ( .ip1(pid[2]), .ip2(n300), .op(n197) );
  nor2_2 U221 ( .ip1(n195), .ip2(n218), .op(pid_SETUP) );
  nor2_2 U222 ( .ip1(n195), .ip2(n219), .op(pid_IN) );
  nand2_2 U223 ( .ip1(pid[0]), .ip2(n302), .op(n195) );
  nor2_2 U224 ( .ip1(n300), .ip2(n301), .op(pid_DATA1) );
  nor2_2 U225 ( .ip1(n215), .ip2(n219), .op(pid_SPLIT) );
  nor2_2 U226 ( .ip1(n218), .ip2(n215), .op(pid_PRE) );
  nand2_2 U227 ( .ip1(n303), .ip2(n302), .op(n215) );
  nor2_2 U228 ( .ip1(n217), .ip2(n218), .op(pid_STALL) );
  nor2_2 U229 ( .ip1(n217), .ip2(n219), .op(pid_NACK) );
  or2_2 U230 ( .ip1(n300), .ip2(pid[2]), .op(n219) );
  and2_2 U231 ( .ip1(token_valid), .ip2(N30), .op(crc5_err) );
  nor2_2 U232 ( .ip1(n221), .ip2(n56), .op(crc16_err) );
  nand2_2 U233 ( .ip1(n210), .ip2(n214), .op(n56) );
  nor4_2 U234 ( .ip1(n222), .ip2(n223), .ip3(n224), .ip4(n225), .op(n221) );
  nand4_2 U235 ( .ip1(crc16_sum[15]), .ip2(crc16_sum[0]), .ip3(crc16_sum[2]), 
        .ip4(crc16_sum[3]), .op(n225) );
  nand4_2 U236 ( .ip1(n311), .ip2(n312), .ip3(n313), .ip4(n314), .op(n224) );
  nand4_2 U237 ( .ip1(n315), .ip2(n304), .ip3(n305), .ip4(n306), .op(n223) );
  nand4_2 U238 ( .ip1(n307), .ip2(n308), .ip3(n309), .ip4(n310), .op(n222) );
  and2_2 U239 ( .ip1(n5), .ip2(rxv2), .op(N50) );
  nand2_2 U241 ( .ip1(n186), .ip2(n187), .op(n227) );
  and2_2 U242 ( .ip1(n220), .ip2(n76), .op(n186) );
  nor2_2 U243 ( .ip1(n216), .ip2(pid[2]), .op(n220) );
  nand2_2 U244 ( .ip1(n210), .ip2(n76), .op(n226) );
  nor2_2 U245 ( .ip1(n316), .ip2(n214), .op(n76) );
  nand2_2 U246 ( .ip1(rx_active), .ip2(n319), .op(n214) );
  nor4_2 U247 ( .ip1(n55), .ip2(state[0]), .ip3(state[1]), .ip4(state[2]), 
        .op(n210) );
  or2_2 U248 ( .ip1(n228), .ip2(token_valid_r1), .op(N29) );
  nor2_2 U249 ( .ip1(n37), .ip2(n192), .op(n228) );
  nand2_2 U250 ( .ip1(pid_ACK), .ip2(n319), .op(n192) );
  nand2_2 U252 ( .ip1(pid[1]), .ip2(n303), .op(n217) );
  nor4_2 U253 ( .ip1(n299), .ip2(state[0]), .ip3(state[2]), .ip4(state[3]), 
        .op(n187) );
  nor2_2 U254 ( .ip1(n216), .ip2(n218), .op(N221) );
  nand2_2 U255 ( .ip1(pid[3]), .ip2(pid[2]), .op(n218) );
  nand2_2 U256 ( .ip1(pid[0]), .ip2(pid[1]), .op(n216) );
  inv_2 U291 ( .ip(rst), .op(n39) );
  not_ab_or_c_or_d U296 ( .ip1(rxv1), .ip2(n5), .ip3(rx_data_done), .ip4(n39), 
        .op(n54) );
  ab_or_c_or_d U297 ( .ip1(crc16_out[14]), .ip2(n4), .ip3(n78), .ip4(n79), 
        .op(n239) );
  ab_or_c_or_d U298 ( .ip1(crc16_out[13]), .ip2(n3), .ip3(n78), .ip4(n80), 
        .op(n240) );
  ab_or_c_or_d U299 ( .ip1(crc16_out[12]), .ip2(n4), .ip3(n78), .ip4(n81), 
        .op(n241) );
  ab_or_c_or_d U300 ( .ip1(crc16_out[11]), .ip2(n5), .ip3(n78), .ip4(n82), 
        .op(n242) );
  ab_or_c_or_d U301 ( .ip1(crc16_out[10]), .ip2(n3), .ip3(n78), .ip4(n83), 
        .op(n243) );
  ab_or_c_or_d U302 ( .ip1(crc16_out[9]), .ip2(n4), .ip3(n78), .ip4(n84), .op(
        n244) );
  ab_or_c_or_d U303 ( .ip1(crc16_out[8]), .ip2(n5), .ip3(n78), .ip4(n85), .op(
        n245) );
  ab_or_c_or_d U304 ( .ip1(crc16_out[7]), .ip2(n3), .ip3(n78), .ip4(n86), .op(
        n246) );
  ab_or_c_or_d U305 ( .ip1(crc16_out[6]), .ip2(n4), .ip3(n78), .ip4(n87), .op(
        n247) );
  ab_or_c_or_d U306 ( .ip1(crc16_out[5]), .ip2(n3), .ip3(n78), .ip4(n88), .op(
        n248) );
  ab_or_c_or_d U307 ( .ip1(crc16_out[4]), .ip2(n5), .ip3(n78), .ip4(n89), .op(
        n249) );
  ab_or_c_or_d U308 ( .ip1(crc16_out[3]), .ip2(n3), .ip3(n78), .ip4(n90), .op(
        n250) );
  ab_or_c_or_d U309 ( .ip1(crc16_out[2]), .ip2(n4), .ip3(n78), .ip4(n91), .op(
        n251) );
  ab_or_c_or_d U310 ( .ip1(crc16_out[1]), .ip2(n5), .ip3(n78), .ip4(n92), .op(
        n252) );
  ab_or_c_or_d U311 ( .ip1(crc16_out[0]), .ip2(n3), .ip3(n78), .ip4(n93), .op(
        n253) );
  ab_or_c_or_d U312 ( .ip1(crc16_out[15]), .ip2(n4), .ip3(n78), .ip4(n94), 
        .op(n254) );
  ab_or_c_or_d U313 ( .ip1(n160), .ip2(pid[7]), .ip3(n161), .ip4(n39), .op(
        n287) );
  ab_or_c_or_d U314 ( .ip1(n160), .ip2(pid[6]), .ip3(n163), .ip4(n39), .op(
        n288) );
  ab_or_c_or_d U315 ( .ip1(n160), .ip2(pid[5]), .ip3(n164), .ip4(n39), .op(
        n289) );
  ab_or_c_or_d U316 ( .ip1(n160), .ip2(pid[4]), .ip3(n165), .ip4(n39), .op(
        n290) );
  not_ab_or_c_or_d U317 ( .ip1(n202), .ip2(n179), .ip3(n203), .ip4(n39), .op(
        n188) );
  not_ab_or_c_or_d U318 ( .ip1(n187), .ip2(n317), .ip3(n77), .ip4(n210), .op(
        n209) );
  usbf_crc5 u0 ( .crc_in({1'b1, 1'b1, 1'b1, 1'b1, 1'b1}), .din({
        \token_fadr[0] , \token_fadr[1] , \token_fadr[2] , \token_fadr[3] , 
        \token_fadr[4] , \token_fadr[5] , \token_fadr[6] , \token_endp[0] , 
        \token_endp[1] , \token_endp[2] , \token_endp[3] }), .crc_out(crc5_out) );
  usbf_crc16_0 u1 ( .crc_in(crc16_sum), .din({rx_data[0], rx_data[1], 
        rx_data[2], rx_data[3], rx_data[4], rx_data[5], rx_data[6], rx_data[7]}), .crc_out(crc16_out) );
  dp_1 token_valid_r1_reg ( .ip(n43), .ck(n23), .q(token_valid_r1) );
  dp_1 \d2_reg[2]  ( .ip(n260), .ck(n27), .q(rx_data_st[2]) );
  dp_1 \d2_reg[1]  ( .ip(n261), .ck(n27), .q(rx_data_st[1]) );
  dp_1 \d2_reg[0]  ( .ip(n262), .ck(n27), .q(rx_data_st[0]) );
  dp_1 data_valid0_reg ( .ip(N50), .ck(n27), .q(rx_data_valid) );
  dp_1 \d2_reg[7]  ( .ip(n255), .ck(n25), .q(rx_data_st[7]) );
  dp_1 \d2_reg[6]  ( .ip(n256), .ck(n26), .q(rx_data_st[6]) );
  dp_1 \d2_reg[5]  ( .ip(n257), .ck(n26), .q(rx_data_st[5]) );
  dp_1 \d2_reg[4]  ( .ip(n258), .ck(n26), .q(rx_data_st[4]) );
  dp_1 \d2_reg[3]  ( .ip(n259), .ck(n26), .q(rx_data_st[3]) );
  dp_1 rxv1_reg ( .ip(n230), .ck(n24), .q(rxv1) );
  dp_1 \crc16_sum_reg[4]  ( .ip(n249), .ck(n24), .q(crc16_sum[4]) );
  dp_1 \crc16_sum_reg[5]  ( .ip(n248), .ck(n24), .q(crc16_sum[5]) );
  dp_1 \crc16_sum_reg[6]  ( .ip(n247), .ck(n25), .q(crc16_sum[6]) );
  dp_1 \d1_reg[2]  ( .ip(n268), .ck(n27), .q(d1[2]) );
  dp_1 \d0_reg[1]  ( .ip(n277), .ck(n27), .q(d0[1]) );
  dp_1 \d1_reg[1]  ( .ip(n269), .ck(n27), .q(d1[1]) );
  dp_1 \d0_reg[0]  ( .ip(n278), .ck(n27), .q(d0[0]) );
  dp_1 \d1_reg[0]  ( .ip(n270), .ck(n27), .q(d1[0]) );
  dp_1 \crc16_sum_reg[9]  ( .ip(n244), .ck(n25), .q(crc16_sum[9]) );
  dp_1 \crc16_sum_reg[10]  ( .ip(n243), .ck(n25), .q(crc16_sum[10]) );
  dp_1 \crc16_sum_reg[12]  ( .ip(n241), .ck(n25), .q(crc16_sum[12]) );
  dp_1 \crc16_sum_reg[14]  ( .ip(n239), .ck(n25), .q(crc16_sum[14]) );
  dp_1 \token1_reg[7]  ( .ip(n231), .ck(n23), .q(token1[7]) );
  dp_1 \token1_reg[6]  ( .ip(n232), .ck(n23), .q(token1[6]) );
  dp_1 \token1_reg[5]  ( .ip(n233), .ck(n23), .q(token1[5]) );
  dp_1 \token1_reg[4]  ( .ip(n234), .ck(n24), .q(token1[4]) );
  dp_1 \token1_reg[3]  ( .ip(n235), .ck(n24), .q(token1[3]) );
  dp_1 \d0_reg[7]  ( .ip(n271), .ck(n25), .q(d0[7]) );
  dp_1 \d1_reg[7]  ( .ip(n263), .ck(n25), .q(d1[7]) );
  dp_1 \d0_reg[6]  ( .ip(n272), .ck(n26), .q(d0[6]) );
  dp_1 \d1_reg[6]  ( .ip(n264), .ck(n26), .q(d1[6]) );
  dp_1 \d0_reg[5]  ( .ip(n273), .ck(n26), .q(d0[5]) );
  dp_1 \d1_reg[5]  ( .ip(n265), .ck(n26), .q(d1[5]) );
  dp_1 \d0_reg[4]  ( .ip(n274), .ck(n26), .q(d0[4]) );
  dp_1 \d1_reg[4]  ( .ip(n266), .ck(n26), .q(d1[4]) );
  dp_1 \d0_reg[3]  ( .ip(n275), .ck(n26), .q(d0[3]) );
  dp_1 \d1_reg[3]  ( .ip(n267), .ck(n26), .q(d1[3]) );
  dp_1 \d0_reg[2]  ( .ip(n276), .ck(n26), .q(d0[2]) );
  dp_1 rx_active_r_reg ( .ip(rx_active), .ck(n22), .q(rx_active_r) );
  dp_1 rxv2_reg ( .ip(n229), .ck(n24), .q(rxv2) );
  dp_1 \pid_reg[7]  ( .ip(n287), .ck(n22), .q(pid[7]) );
  dp_1 \pid_reg[6]  ( .ip(n288), .ck(n22), .q(pid[6]) );
  dp_1 \pid_reg[5]  ( .ip(n289), .ck(n22), .q(pid[5]) );
  dp_1 \pid_reg[4]  ( .ip(n290), .ck(n22), .q(pid[4]) );
  dp_1 \crc16_sum_reg[2]  ( .ip(n251), .ck(n24), .q(crc16_sum[2]) );
  dp_1 \crc16_sum_reg[3]  ( .ip(n250), .ck(n24), .q(crc16_sum[3]) );
  dp_1 \crc16_sum_reg[8]  ( .ip(n245), .ck(n25), .q(crc16_sum[8]) );
  dp_1 \crc16_sum_reg[11]  ( .ip(n242), .ck(n25), .q(crc16_sum[11]) );
  dp_1 \crc16_sum_reg[13]  ( .ip(n240), .ck(n25), .q(crc16_sum[13]) );
  dp_1 \crc16_sum_reg[1]  ( .ip(n252), .ck(n24), .q(crc16_sum[1]) );
  dp_1 \crc16_sum_reg[7]  ( .ip(n246), .ck(n25), .q(crc16_sum[7]) );
  dp_1 \crc16_sum_reg[15]  ( .ip(n254), .ck(n25), .q(crc16_sum[15]) );
  dp_1 \state_reg[1]  ( .ip(n295), .ck(n22), .q(state[1]) );
  dp_1 \state_reg[2]  ( .ip(n296), .ck(n22), .q(state[2]) );
  dp_1 \state_reg[3]  ( .ip(n297), .ck(n22), .q(state[3]) );
  dp_1 \crc16_sum_reg[0]  ( .ip(n253), .ck(n24), .q(crc16_sum[0]) );
  dp_1 \token0_reg[3]  ( .ip(n283), .ck(n23), .q(\token_fadr[3] ) );
  dp_1 token_valid_str1_reg ( .ip(N29), .ck(n23), .q(token_valid) );
  dp_1 \token0_reg[0]  ( .ip(n286), .ck(n23), .q(\token_fadr[0] ) );
  dp_1 \token0_reg[4]  ( .ip(n282), .ck(n23), .q(\token_fadr[4] ) );
  dp_1 \token0_reg[2]  ( .ip(n284), .ck(n23), .q(\token_fadr[2] ) );
  dp_1 \pid_reg[0]  ( .ip(n294), .ck(n22), .q(pid[0]) );
  dp_1 \state_reg[0]  ( .ip(n298), .ck(n22), .q(state[0]) );
  dp_1 \token0_reg[5]  ( .ip(n281), .ck(n23), .q(\token_fadr[5] ) );
  dp_1 \token0_reg[1]  ( .ip(n285), .ck(n23), .q(\token_fadr[1] ) );
  dp_1 \pid_reg[1]  ( .ip(n293), .ck(n22), .q(pid[1]) );
  dp_1 \token0_reg[6]  ( .ip(n280), .ck(n23), .q(\token_fadr[6] ) );
  dp_1 \pid_reg[3]  ( .ip(n291), .ck(n22), .q(pid[3]) );
  dp_1 \pid_reg[2]  ( .ip(n292), .ck(n22), .q(pid[2]) );
  dp_1 \token0_reg[7]  ( .ip(n279), .ck(n23), .q(\token_endp[0] ) );
  dp_1 \token1_reg[2]  ( .ip(n236), .ck(n24), .q(\token_endp[3] ) );
  dp_1 \token1_reg[1]  ( .ip(n237), .ck(n24), .q(\token_endp[2] ) );
  dp_1 \token1_reg[0]  ( .ip(n238), .ck(n24), .q(\token_endp[1] ) );
  and2_2 U4 ( .ip1(n226), .ip2(n227), .op(n1) );
  nand2_2 U43 ( .ip1(n76), .ip2(n77), .op(n2) );
  inv_2 U60 ( .ip(n20), .op(n5) );
  inv_2 U174 ( .ip(n20), .op(n3) );
  inv_2 U182 ( .ip(n21), .op(n4) );
  inv_2 U216 ( .ip(n2), .op(n43) );
  nor2_2 U240 ( .ip1(n183), .ip2(n37), .op(n145) );
  nor2_2 U251 ( .ip1(rx_active_r), .ip2(n317), .op(n78) );
  nor3_2 U257 ( .ip1(n195), .ip2(pid[3]), .ip3(pid[2]), .op(pid_OUT) );
  nor3_2 U258 ( .ip1(pid[2]), .ip2(pid[3]), .ip3(n217), .op(pid_ACK) );
  and2_2 U259 ( .ip1(rst), .ip2(n174), .op(n162) );
  buf_1 U260 ( .ip(n10), .op(n6) );
  buf_1 U261 ( .ip(n11), .op(n7) );
  buf_1 U262 ( .ip(n12), .op(n8) );
  buf_1 U263 ( .ip(n13), .op(n9) );
  buf_1 U264 ( .ip(n15), .op(n10) );
  buf_1 U265 ( .ip(n16), .op(n11) );
  buf_1 U266 ( .ip(n17), .op(n12) );
  buf_1 U267 ( .ip(n1), .op(n13) );
  buf_1 U268 ( .ip(n1), .op(n14) );
  buf_1 U269 ( .ip(n1), .op(n15) );
  buf_1 U270 ( .ip(n1), .op(n16) );
  buf_1 U271 ( .ip(n1), .op(n17) );
  buf_1 U272 ( .ip(n19), .op(n18) );
  buf_1 U273 ( .ip(n14), .op(n19) );
  buf_1 U274 ( .ip(n1), .op(n20) );
  buf_1 U275 ( .ip(n6), .op(n21) );
  buf_1 U276 ( .ip(clk), .op(n22) );
  buf_1 U277 ( .ip(clk), .op(n23) );
  buf_1 U278 ( .ip(clk), .op(n24) );
  buf_1 U279 ( .ip(clk), .op(n25) );
  buf_1 U280 ( .ip(clk), .op(n26) );
  buf_1 U281 ( .ip(clk), .op(n27) );
  xor2_1 U282 ( .ip1(token1[6]), .ip2(\ne_2397/A[3] ), .op(n33) );
  xor2_1 U283 ( .ip1(token1[7]), .ip2(\ne_2397/A[4] ), .op(n32) );
  xor2_1 U284 ( .ip1(token1[5]), .ip2(\ne_2397/A[2] ), .op(n31) );
  xor2_1 U285 ( .ip1(token1[3]), .ip2(\ne_2397/A[0] ), .op(n29) );
  xor2_1 U286 ( .ip1(token1[4]), .ip2(\ne_2397/A[1] ), .op(n28) );
  or2_1 U287 ( .ip1(n29), .ip2(n28), .op(n30) );
  or4_1 U288 ( .ip1(n33), .ip2(n32), .ip3(n31), .ip4(n30), .op(N30) );
  inv_2 U289 ( .ip(crc5_out[0]), .op(\ne_2397/A[4] ) );
  inv_2 U290 ( .ip(crc5_out[2]), .op(\ne_2397/A[2] ) );
  inv_2 U292 ( .ip(crc5_out[1]), .op(\ne_2397/A[3] ) );
  inv_2 U293 ( .ip(crc5_out[3]), .op(\ne_2397/A[1] ) );
  inv_2 U294 ( .ip(crc5_out[4]), .op(\ne_2397/A[0] ) );
  inv_2 U295 ( .ip(n145), .op(n34) );
  inv_2 U319 ( .ip(n188), .op(n35) );
  inv_2 U320 ( .ip(n198), .op(n36) );
  inv_2 U321 ( .ip(n187), .op(n37) );
  inv_2 U322 ( .ip(n210), .op(n38) );
  inv_2 U323 ( .ip(n56), .op(rx_data_done) );
  inv_2 U324 ( .ip(n179), .op(n41) );
  inv_2 U325 ( .ip(n77), .op(n42) );
  inv_2 U326 ( .ip(state[3]), .op(n55) );
  inv_2 U327 ( .ip(state[2]), .op(n61) );
  inv_2 U328 ( .ip(state[1]), .op(n299) );
  inv_2 U329 ( .ip(pid[3]), .op(n300) );
  inv_2 U330 ( .ip(n220), .op(n301) );
  inv_2 U331 ( .ip(pid[1]), .op(n302) );
  inv_2 U332 ( .ip(pid[0]), .op(n303) );
  inv_2 U333 ( .ip(crc16_sum[1]), .op(n304) );
  inv_2 U334 ( .ip(crc16_sum[4]), .op(n305) );
  inv_2 U335 ( .ip(crc16_sum[5]), .op(n306) );
  inv_2 U336 ( .ip(crc16_sum[6]), .op(n307) );
  inv_2 U337 ( .ip(crc16_sum[7]), .op(n308) );
  inv_2 U338 ( .ip(crc16_sum[8]), .op(n309) );
  inv_2 U339 ( .ip(crc16_sum[9]), .op(n310) );
  inv_2 U340 ( .ip(crc16_sum[10]), .op(n311) );
  inv_2 U341 ( .ip(crc16_sum[11]), .op(n312) );
  inv_2 U342 ( .ip(crc16_sum[12]), .op(n313) );
  inv_2 U343 ( .ip(crc16_sum[13]), .op(n314) );
  inv_2 U344 ( .ip(crc16_sum[14]), .op(n315) );
  inv_2 U345 ( .ip(rx_valid), .op(n316) );
  inv_2 U346 ( .ip(rx_active), .op(n317) );
  inv_2 U347 ( .ip(n214), .op(n318) );
  inv_2 U348 ( .ip(rx_err), .op(n319) );
endmodule


module usbf_crc16_1 ( crc_in, din, crc_out );
  input [15:0] crc_in;
  input [7:0] din;
  output [15:0] crc_out;
  wire   \crc_in[6] , \crc_in[5] , \crc_in[4] , \crc_in[3] , \crc_in[2] , n12,
         n13, n14, n15, n16, n17, n18, n19, n20, n21, n22;
  assign crc_out[14] = \crc_in[6] ;
  assign \crc_in[6]  = crc_in[6];
  assign crc_out[13] = \crc_in[5] ;
  assign \crc_in[5]  = crc_in[5];
  assign crc_out[12] = \crc_in[4] ;
  assign \crc_in[4]  = crc_in[4];
  assign crc_out[11] = \crc_in[3] ;
  assign \crc_in[3]  = crc_in[3];
  assign crc_out[10] = \crc_in[2] ;
  assign \crc_in[2]  = crc_in[2];

  xor2_2 U1 ( .ip1(crc_in[1]), .ip2(n22), .op(crc_out[9]) );
  xor2_2 U2 ( .ip1(crc_in[0]), .ip2(n21), .op(crc_out[8]) );
  xor2_2 U3 ( .ip1(n16), .ip2(n15), .op(crc_out[3]) );
  xor2_2 U4 ( .ip1(crc_in[7]), .ip2(crc_out[0]), .op(crc_out[15]) );
  xor2_2 U5 ( .ip1(n14), .ip2(crc_out[1]), .op(crc_out[0]) );
  xor2_2 U6 ( .ip1(crc_out[4]), .ip2(n16), .op(n12) );
  xor2_2 U7 ( .ip1(din[3]), .ip2(crc_in[11]), .op(n18) );
  xor2_2 U8 ( .ip1(n21), .ip2(crc_out[6]), .op(n13) );
  xor2_2 U9 ( .ip1(din[5]), .ip2(crc_in[13]), .op(n20) );
  xor2_2 U10 ( .ip1(din[7]), .ip2(crc_in[15]), .op(n22) );
  xor2_2 U11 ( .ip1(din[0]), .ip2(crc_in[8]), .op(n14) );
  xnor2_1 U12 ( .ip1(n20), .ip2(n19), .op(crc_out[7]) );
  xnor2_1 U13 ( .ip1(n18), .ip2(n17), .op(crc_out[5]) );
  xnor2_1 U14 ( .ip1(n14), .ip2(n16), .op(crc_out[2]) );
  xnor2_1 U15 ( .ip1(n13), .ip2(n12), .op(crc_out[1]) );
  xnor2_1 U16 ( .ip1(din[1]), .ip2(crc_in[9]), .op(n16) );
  xnor2_1 U17 ( .ip1(n18), .ip2(n15), .op(crc_out[4]) );
  xnor2_1 U18 ( .ip1(din[2]), .ip2(crc_in[10]), .op(n15) );
  xnor2_1 U19 ( .ip1(n20), .ip2(n17), .op(crc_out[6]) );
  xnor2_1 U20 ( .ip1(din[4]), .ip2(crc_in[12]), .op(n17) );
  xnor2_1 U21 ( .ip1(n22), .ip2(n19), .op(n21) );
  xnor2_1 U22 ( .ip1(din[6]), .ip2(crc_in[14]), .op(n19) );
endmodule


module usbf_pa ( clk, rst, tx_data, tx_valid, tx_valid_last, tx_ready, 
        tx_first, send_token, token_pid_sel, send_data, data_pid_sel, 
        send_zero_length, tx_data_st, rd_next );
  output [7:0] tx_data;
  input [1:0] token_pid_sel;
  input [1:0] data_pid_sel;
  input [7:0] tx_data_st;
  input clk, rst, tx_ready, send_token, send_data, send_zero_length;
  output tx_valid, tx_valid_last, tx_first, rd_next;
  wire   send_zero_length_r, zero_length_r, tx_valid_r1, tx_valid_r,
         send_token_r, tx_first_r, send_data_r, send_data_r2, n32, n36, n37,
         n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51,
         n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n64, n65,
         n66, n67, n68, n69, n70, n71, n72, n73, n74, n75, n76, n77, n78, n79,
         n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n93,
         n94, n95, n96, n97, n98, n99, n100, n101, n102, n103, n104, n105,
         n106, n107, n108, n109, n110, n111, n112, n113, n114, n115, n116,
         n117, n118, n119, n120, n121, n122, n123, n124, n125, n126, n127,
         n128, n129, n130, n131, n132, n133, n134, n135, n136, n137, n138,
         n139, n140, n141, n142, n143, n144, n145, n146, n147, n148, n149,
         n150, n151, n152, n153, n154, n155, n156, n157, n158, n159, n160,
         n161, n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14,
         n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28,
         n29, n30, n31, n33, n34, n35, n162, n163, n164, n165, n166;
  wire   [15:0] crc16;
  wire   [15:0] crc16_next;
  wire   [4:0] state;

  or2_2 U3 ( .ip1(n36), .ip2(send_token), .op(tx_valid_last) );
  nand2_2 U4 ( .ip1(n14), .ip2(n37), .op(tx_valid) );
  nor2_2 U5 ( .ip1(tx_first_r), .ip2(n39), .op(tx_first) );
  nor2_2 U6 ( .ip1(n43), .ip2(n44), .op(n42) );
  nor2_2 U7 ( .ip1(n45), .ip2(n7), .op(n41) );
  nor2_2 U8 ( .ip1(data_pid_sel[0]), .ip2(n49), .op(n48) );
  nor2_2 U9 ( .ip1(crc16[8]), .ip2(n10), .op(n47) );
  nor2_2 U10 ( .ip1(token_pid_sel[1]), .ip2(n43), .op(n51) );
  nor2_2 U11 ( .ip1(n52), .ip2(n7), .op(n50) );
  nor2_2 U12 ( .ip1(data_pid_sel[1]), .ip2(n49), .op(n54) );
  nor2_2 U13 ( .ip1(crc16[9]), .ip2(n10), .op(n53) );
  nand2_2 U14 ( .ip1(n55), .ip2(n56), .op(tx_data[5]) );
  nand2_2 U15 ( .ip1(n57), .ip2(n58), .op(n56) );
  nand2_2 U16 ( .ip1(n59), .ip2(n60), .op(n58) );
  nand2_2 U17 ( .ip1(n46), .ip2(n26), .op(n60) );
  nand2_2 U18 ( .ip1(n61), .ip2(n35), .op(n59) );
  nand2_2 U19 ( .ip1(tx_data_st[5]), .ip2(n40), .op(n55) );
  nand4_2 U20 ( .ip1(n43), .ip2(n62), .ip3(n63), .ip4(n64), .op(tx_data[4]) );
  nand2_2 U21 ( .ip1(n46), .ip2(n27), .op(n64) );
  nand2_2 U22 ( .ip1(n61), .ip2(n162), .op(n63) );
  nand2_2 U23 ( .ip1(tx_data_st[4]), .ip2(n11), .op(n62) );
  and2_2 U24 ( .ip1(n8), .ip2(n44), .op(n66) );
  xor2_2 U25 ( .ip1(token_pid_sel[0]), .ip2(token_pid_sel[1]), .op(n44) );
  nor2_2 U26 ( .ip1(n67), .ip2(n7), .op(n65) );
  and2_2 U27 ( .ip1(n9), .ip2(data_pid_sel[0]), .op(n69) );
  nor2_2 U28 ( .ip1(crc16[12]), .ip2(n10), .op(n68) );
  and2_2 U29 ( .ip1(n8), .ip2(token_pid_sel[1]), .op(n71) );
  nor2_2 U30 ( .ip1(n72), .ip2(n7), .op(n70) );
  and2_2 U31 ( .ip1(n9), .ip2(data_pid_sel[1]), .op(n74) );
  nor2_2 U32 ( .ip1(crc16[13]), .ip2(n10), .op(n73) );
  nor2_2 U33 ( .ip1(n11), .ip2(n76), .op(n75) );
  nor2_2 U34 ( .ip1(crc16[14]), .ip2(n10), .op(n77) );
  nand2_2 U35 ( .ip1(n78), .ip2(n79), .op(tx_data[0]) );
  nand2_2 U36 ( .ip1(tx_data_st[0]), .ip2(n40), .op(n79) );
  nor2_2 U37 ( .ip1(n8), .ip2(n80), .op(n40) );
  or2_2 U38 ( .ip1(n81), .ip2(n7), .op(n78) );
  nor2_2 U39 ( .ip1(n8), .ip2(n11), .op(n57) );
  nand3_2 U40 ( .ip1(n37), .ip2(n82), .ip3(n83), .op(n80) );
  nor2_2 U41 ( .ip1(n84), .ip2(n85), .op(n37) );
  and2_2 U42 ( .ip1(send_data), .ip2(n86), .op(n85) );
  nor2_2 U43 ( .ip1(send_token_r), .ip2(send_token), .op(n43) );
  nor2_2 U44 ( .ip1(crc16[15]), .ip2(n10), .op(n87) );
  nor2_2 U45 ( .ip1(n61), .ip2(n9), .op(n46) );
  nand3_2 U46 ( .ip1(n88), .ip2(n82), .ip3(n10), .op(n49) );
  nand2_2 U47 ( .ip1(n83), .ip2(n89), .op(n61) );
  nand2_2 U48 ( .ip1(n84), .ip2(n166), .op(n89) );
  and2_2 U49 ( .ip1(n90), .ip2(n91), .op(n83) );
  nand2_2 U50 ( .ip1(n92), .ip2(n93), .op(n91) );
  nor2_2 U51 ( .ip1(n2), .ip2(n165), .op(n95) );
  nor2_2 U52 ( .ip1(n1), .ip2(n164), .op(n97) );
  nor2_2 U53 ( .ip1(n94), .ip2(n163), .op(n98) );
  nor2_2 U54 ( .ip1(n2), .ip2(n162), .op(n99) );
  nor2_2 U55 ( .ip1(n1), .ip2(n35), .op(n100) );
  nor2_2 U56 ( .ip1(n94), .ip2(n34), .op(n101) );
  nor2_2 U57 ( .ip1(n2), .ip2(n33), .op(n102) );
  nor2_2 U58 ( .ip1(n1), .ip2(n31), .op(n103) );
  nor2_2 U59 ( .ip1(n94), .ip2(n30), .op(n104) );
  nor2_2 U60 ( .ip1(n2), .ip2(n29), .op(n105) );
  nor2_2 U61 ( .ip1(n1), .ip2(n28), .op(n106) );
  nor2_2 U62 ( .ip1(n94), .ip2(n27), .op(n107) );
  nor2_2 U63 ( .ip1(n2), .ip2(n26), .op(n108) );
  nor2_2 U64 ( .ip1(n1), .ip2(n25), .op(n109) );
  nor2_2 U65 ( .ip1(n94), .ip2(n24), .op(n110) );
  nor2_2 U66 ( .ip1(n2), .ip2(n23), .op(n111) );
  and3_2 U68 ( .ip1(tx_valid_r), .ip2(tx_ready), .ip3(n93), .op(rd_next) );
  nor2_2 U69 ( .ip1(zero_length_r), .ip2(send_data_r2), .op(n112) );
  nand2_2 U70 ( .ip1(n113), .ip2(n114), .op(n155) );
  or4_2 U71 ( .ip1(n13), .ip2(n32), .ip3(n36), .ip4(n115), .op(n114) );
  nand2_2 U72 ( .ip1(zero_length_r), .ip2(n115), .op(n113) );
  nand2_2 U74 ( .ip1(n88), .ip2(n116), .op(n36) );
  nand2_2 U75 ( .ip1(n16), .ip2(n166), .op(n116) );
  nand2_2 U76 ( .ip1(tx_ready), .ip2(n84), .op(n88) );
  nor4_2 U77 ( .ip1(n19), .ip2(n21), .ip3(state[0]), .ip4(state[4]), .op(n84)
         );
  nand2_2 U79 ( .ip1(n117), .ip2(n118), .op(n156) );
  nand3_2 U80 ( .ip1(n86), .ip2(n13), .ip3(n119), .op(n118) );
  nand2_2 U81 ( .ip1(state[1]), .ip2(n120), .op(n117) );
  nand2_2 U82 ( .ip1(n121), .ip2(n122), .op(n157) );
  nand2_2 U83 ( .ip1(n119), .ip2(n38), .op(n122) );
  nand2_2 U84 ( .ip1(state[2]), .ip2(n120), .op(n121) );
  and2_2 U85 ( .ip1(n120), .ip2(state[3]), .op(n158) );
  nand2_2 U86 ( .ip1(n123), .ip2(n124), .op(n159) );
  nand3_2 U87 ( .ip1(send_zero_length_r), .ip2(n86), .ip3(n119), .op(n124) );
  and2_2 U88 ( .ip1(rst), .ip2(n125), .op(n119) );
  nand2_2 U89 ( .ip1(state[4]), .ip2(n120), .op(n123) );
  nand2_2 U90 ( .ip1(n125), .ip2(n126), .op(n120) );
  nand2_2 U91 ( .ip1(n127), .ip2(rst), .op(n126) );
  nand2_2 U92 ( .ip1(n128), .ip2(n129), .op(n160) );
  nand2_2 U93 ( .ip1(n130), .ip2(n125), .op(n129) );
  nand2_2 U94 ( .ip1(rst), .ip2(n82), .op(n130) );
  nand2_2 U95 ( .ip1(state[0]), .ip2(n131), .op(n128) );
  nand2_2 U96 ( .ip1(n15), .ip2(n125), .op(n131) );
  or2_2 U97 ( .ip1(n132), .ip2(n32), .op(n125) );
  nor2_2 U98 ( .ip1(tx_ready), .ip2(n82), .op(n134) );
  nor2_2 U99 ( .ip1(n92), .ip2(n18), .op(n133) );
  and3_2 U100 ( .ip1(tx_ready), .ip2(n12), .ip3(tx_valid_r), .op(n92) );
  nand2_2 U102 ( .ip1(n18), .ip2(n90), .op(n38) );
  nand4_2 U103 ( .ip1(state[4]), .ip2(n135), .ip3(n17), .ip4(n19), .op(n90) );
  nor4_2 U104 ( .ip1(n22), .ip2(n20), .ip3(state[0]), .ip4(state[2]), .op(n93)
         );
  nand4_2 U105 ( .ip1(state[2]), .ip2(n136), .ip3(n17), .ip4(n22), .op(n82) );
  nor2_2 U107 ( .ip1(state[4]), .ip2(state[3]), .op(n136) );
  nor2_2 U108 ( .ip1(state[2]), .ip2(state[1]), .op(n135) );
  nor2_2 U109 ( .ip1(n137), .ip2(n32), .op(n161) );
  nor2_2 U110 ( .ip1(n138), .ip2(send_token), .op(n137) );
  and2_2 U111 ( .ip1(n166), .ip2(send_token_r), .op(n138) );
  nor2_2 U112 ( .ip1(send_token), .ip2(send_data), .op(n39) );
  inv_2 U144 ( .ip(rst), .op(n32) );
  ab_or_c_or_d U148 ( .ip1(tx_data_st[7]), .ip2(n40), .ip3(n41), .ip4(n42), 
        .op(tx_data[7]) );
  not_ab_or_c_or_d U149 ( .ip1(n46), .ip2(n24), .ip3(n47), .ip4(n48), .op(n45)
         );
  ab_or_c_or_d U150 ( .ip1(tx_data_st[6]), .ip2(n40), .ip3(n50), .ip4(n51), 
        .op(tx_data[6]) );
  not_ab_or_c_or_d U151 ( .ip1(n46), .ip2(n25), .ip3(n53), .ip4(n54), .op(n52)
         );
  ab_or_c_or_d U152 ( .ip1(tx_data_st[3]), .ip2(n40), .ip3(n65), .ip4(n66), 
        .op(tx_data[3]) );
  not_ab_or_c_or_d U153 ( .ip1(n46), .ip2(n28), .ip3(n68), .ip4(n69), .op(n67)
         );
  ab_or_c_or_d U154 ( .ip1(tx_data_st[2]), .ip2(n40), .ip3(n70), .ip4(n71), 
        .op(tx_data[2]) );
  not_ab_or_c_or_d U155 ( .ip1(n46), .ip2(n29), .ip3(n73), .ip4(n74), .op(n72)
         );
  ab_or_c_or_d U156 ( .ip1(tx_data_st[1]), .ip2(n11), .ip3(n75), .ip4(n8), 
        .op(tx_data[1]) );
  not_ab_or_c_or_d U157 ( .ip1(n46), .ip2(n30), .ip3(n77), .ip4(n9), .op(n76)
         );
  not_ab_or_c_or_d U158 ( .ip1(n46), .ip2(n31), .ip3(n87), .ip4(n9), .op(n81)
         );
  ab_or_c_or_d U159 ( .ip1(crc16_next[14]), .ip2(n1), .ip3(n95), .ip4(n96), 
        .op(n139) );
  ab_or_c_or_d U160 ( .ip1(crc16_next[13]), .ip2(n94), .ip3(n97), .ip4(n96), 
        .op(n140) );
  ab_or_c_or_d U161 ( .ip1(crc16_next[12]), .ip2(n2), .ip3(n98), .ip4(n96), 
        .op(n141) );
  ab_or_c_or_d U162 ( .ip1(crc16_next[11]), .ip2(n1), .ip3(n99), .ip4(n96), 
        .op(n142) );
  ab_or_c_or_d U163 ( .ip1(crc16_next[10]), .ip2(n94), .ip3(n100), .ip4(n96), 
        .op(n143) );
  ab_or_c_or_d U164 ( .ip1(crc16_next[9]), .ip2(n2), .ip3(n101), .ip4(n96), 
        .op(n144) );
  ab_or_c_or_d U165 ( .ip1(crc16_next[8]), .ip2(n1), .ip3(n102), .ip4(n96), 
        .op(n145) );
  ab_or_c_or_d U166 ( .ip1(crc16_next[7]), .ip2(n94), .ip3(n103), .ip4(n96), 
        .op(n146) );
  ab_or_c_or_d U167 ( .ip1(crc16_next[6]), .ip2(n2), .ip3(n104), .ip4(n96), 
        .op(n147) );
  ab_or_c_or_d U168 ( .ip1(crc16_next[5]), .ip2(n1), .ip3(n105), .ip4(n96), 
        .op(n148) );
  ab_or_c_or_d U169 ( .ip1(crc16_next[4]), .ip2(n94), .ip3(n106), .ip4(n96), 
        .op(n149) );
  ab_or_c_or_d U170 ( .ip1(crc16_next[3]), .ip2(n2), .ip3(n107), .ip4(n96), 
        .op(n150) );
  ab_or_c_or_d U171 ( .ip1(crc16_next[2]), .ip2(n1), .ip3(n108), .ip4(n96), 
        .op(n151) );
  ab_or_c_or_d U172 ( .ip1(crc16_next[1]), .ip2(n94), .ip3(n109), .ip4(n96), 
        .op(n152) );
  ab_or_c_or_d U173 ( .ip1(crc16_next[0]), .ip2(n94), .ip3(n110), .ip4(n96), 
        .op(n153) );
  ab_or_c_or_d U174 ( .ip1(crc16_next[15]), .ip2(n1), .ip3(n111), .ip4(n96), 
        .op(n154) );
  not_ab_or_c_or_d U175 ( .ip1(n86), .ip2(n12), .ip3(n133), .ip4(n134), .op(
        n132) );
  usbf_crc16_1 u1 ( .crc_in(crc16), .din({tx_data_st[0], tx_data_st[1], 
        tx_data_st[2], tx_data_st[3], tx_data_st[4], tx_data_st[5], 
        tx_data_st[6], tx_data_st[7]}), .crc_out(crc16_next) );
  dp_1 tx_first_r_reg ( .ip(n6), .ck(n3), .q(tx_first_r) );
  dp_1 send_data_r2_reg ( .ip(send_data_r), .ck(n3), .q(send_data_r2) );
  dp_1 \crc16_reg[2]  ( .ip(n151), .ck(n4), .q(crc16[2]) );
  dp_1 \crc16_reg[3]  ( .ip(n150), .ck(n4), .q(crc16[3]) );
  dp_1 \crc16_reg[4]  ( .ip(n149), .ck(n4), .q(crc16[4]) );
  dp_1 \crc16_reg[5]  ( .ip(n148), .ck(n4), .q(crc16[5]) );
  dp_1 \crc16_reg[6]  ( .ip(n147), .ck(n4), .q(crc16[6]) );
  dp_1 \crc16_reg[10]  ( .ip(n143), .ck(n4), .q(crc16[10]) );
  dp_1 zero_length_r_reg ( .ip(n155), .ck(n3), .q(zero_length_r) );
  dp_1 send_token_r_reg ( .ip(n161), .ck(n3), .q(send_token_r) );
  dp_1 send_zero_length_r_reg ( .ip(send_zero_length), .ck(n3), .q(
        send_zero_length_r) );
  dp_1 \crc16_reg[12]  ( .ip(n141), .ck(n5), .q(crc16[12]) );
  dp_1 \crc16_reg[14]  ( .ip(n139), .ck(n5), .q(crc16[14]) );
  dp_1 tx_valid_r_reg ( .ip(tx_valid_r1), .ck(n3), .q(tx_valid_r) );
  dp_1 send_data_r_reg ( .ip(send_data), .ck(n3), .q(send_data_r) );
  dp_1 \crc16_reg[11]  ( .ip(n142), .ck(n4), .q(crc16[11]) );
  dp_1 \crc16_reg[9]  ( .ip(n144), .ck(n4), .q(crc16[9]) );
  dp_1 \crc16_reg[0]  ( .ip(n153), .ck(n4), .q(crc16[0]) );
  dp_1 \crc16_reg[1]  ( .ip(n152), .ck(n4), .q(crc16[1]) );
  dp_1 \crc16_reg[7]  ( .ip(n146), .ck(n4), .q(crc16[7]) );
  dp_1 \crc16_reg[13]  ( .ip(n140), .ck(n5), .q(crc16[13]) );
  dp_1 \state_reg[1]  ( .ip(n156), .ck(n3), .q(state[1]) );
  dp_1 \state_reg[3]  ( .ip(n158), .ck(n3), .q(state[3]) );
  dp_1 \crc16_reg[15]  ( .ip(n154), .ck(n4), .q(crc16[15]) );
  dp_1 \crc16_reg[8]  ( .ip(n145), .ck(n4), .q(crc16[8]) );
  dp_1 \state_reg[0]  ( .ip(n160), .ck(n3), .q(state[0]) );
  dp_1 \state_reg[4]  ( .ip(n159), .ck(n3), .q(state[4]) );
  dp_1 \state_reg[2]  ( .ip(n157), .ck(n3), .q(state[2]) );
  dp_1 tx_valid_r1_reg ( .ip(tx_valid), .ck(n3), .q(tx_valid_r1) );
  nor3_2 U67 ( .ip1(n86), .ip2(n16), .ip3(n38), .op(n127) );
  nor3_2 U73 ( .ip1(n21), .ip2(n20), .ip3(n17), .op(n86) );
  or2_2 U78 ( .ip1(n112), .ip2(rd_next), .op(n94) );
  or2_2 U101 ( .ip1(n112), .ip2(rd_next), .op(n1) );
  or2_2 U106 ( .ip1(n112), .ip2(rd_next), .op(n2) );
  nand2_2 U113 ( .ip1(send_data_r), .ip2(n12), .op(n96) );
  nor3_2 U114 ( .ip1(n96), .ip2(n36), .ip3(n32), .op(n115) );
  buf_1 U115 ( .ip(clk), .op(n3) );
  buf_1 U116 ( .ip(clk), .op(n4) );
  buf_1 U117 ( .ip(clk), .op(n5) );
  inv_2 U118 ( .ip(n39), .op(n6) );
  inv_2 U119 ( .ip(n57), .op(n7) );
  inv_2 U120 ( .ip(n43), .op(n8) );
  inv_2 U121 ( .ip(n49), .op(n9) );
  inv_2 U122 ( .ip(n61), .op(n10) );
  inv_2 U123 ( .ip(n80), .op(n11) );
  inv_2 U124 ( .ip(send_data), .op(n12) );
  inv_2 U125 ( .ip(send_zero_length_r), .op(n13) );
  inv_2 U126 ( .ip(n38), .op(n14) );
  inv_2 U127 ( .ip(n127), .op(n15) );
  inv_2 U128 ( .ip(n82), .op(n16) );
  inv_2 U129 ( .ip(state[0]), .op(n17) );
  inv_2 U130 ( .ip(n93), .op(n18) );
  inv_2 U131 ( .ip(state[3]), .op(n19) );
  inv_2 U132 ( .ip(n136), .op(n20) );
  inv_2 U133 ( .ip(n135), .op(n21) );
  inv_2 U134 ( .ip(state[1]), .op(n22) );
  inv_2 U135 ( .ip(crc16[15]), .op(n23) );
  inv_2 U136 ( .ip(crc16[0]), .op(n24) );
  inv_2 U137 ( .ip(crc16[1]), .op(n25) );
  inv_2 U138 ( .ip(crc16[2]), .op(n26) );
  inv_2 U139 ( .ip(crc16[3]), .op(n27) );
  inv_2 U140 ( .ip(crc16[4]), .op(n28) );
  inv_2 U141 ( .ip(crc16[5]), .op(n29) );
  inv_2 U142 ( .ip(crc16[6]), .op(n30) );
  inv_2 U143 ( .ip(crc16[7]), .op(n31) );
  inv_2 U145 ( .ip(crc16[8]), .op(n33) );
  inv_2 U146 ( .ip(crc16[9]), .op(n34) );
  inv_2 U147 ( .ip(crc16[10]), .op(n35) );
  inv_2 U176 ( .ip(crc16[11]), .op(n162) );
  inv_2 U177 ( .ip(crc16[12]), .op(n163) );
  inv_2 U178 ( .ip(crc16[13]), .op(n164) );
  inv_2 U179 ( .ip(crc16[14]), .op(n165) );
  inv_2 U180 ( .ip(tx_ready), .op(n166) );
endmodule


module usbf_idma_SSRAM_HADR14_DW01_dec_0 ( A, SUM );
  input [13:0] A;
  output [13:0] SUM;
  wire   n1, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25;

  inv_2 U1 ( .ip(n22), .op(n1) );
  inv_2 U2 ( .ip(A[0]), .op(SUM[0]) );
  nand2_1 U3 ( .ip1(n3), .ip2(n4), .op(SUM[9]) );
  nand2_1 U4 ( .ip1(A[9]), .ip2(n5), .op(n4) );
  nand2_1 U5 ( .ip1(n5), .ip2(n6), .op(SUM[8]) );
  nand2_1 U6 ( .ip1(A[8]), .ip2(n7), .op(n6) );
  nand2_1 U7 ( .ip1(n7), .ip2(n8), .op(SUM[7]) );
  nand2_1 U8 ( .ip1(A[7]), .ip2(n9), .op(n8) );
  nand2_1 U9 ( .ip1(n9), .ip2(n10), .op(SUM[6]) );
  nand2_1 U10 ( .ip1(A[6]), .ip2(n11), .op(n10) );
  nand2_1 U11 ( .ip1(n11), .ip2(n12), .op(SUM[5]) );
  nand2_1 U12 ( .ip1(A[5]), .ip2(n13), .op(n12) );
  nand2_1 U13 ( .ip1(n13), .ip2(n14), .op(SUM[4]) );
  nand2_1 U14 ( .ip1(A[4]), .ip2(n15), .op(n14) );
  nand2_1 U15 ( .ip1(n15), .ip2(n16), .op(SUM[3]) );
  nand2_1 U16 ( .ip1(A[3]), .ip2(n17), .op(n16) );
  nand2_1 U17 ( .ip1(n17), .ip2(n18), .op(SUM[2]) );
  nand2_1 U18 ( .ip1(A[2]), .ip2(n19), .op(n18) );
  nand2_1 U19 ( .ip1(n19), .ip2(n20), .op(SUM[1]) );
  nand2_1 U20 ( .ip1(A[1]), .ip2(A[0]), .op(n20) );
  xor2_1 U21 ( .ip1(A[13]), .ip2(n21), .op(SUM[13]) );
  nor2_1 U22 ( .ip1(A[12]), .ip2(n1), .op(n21) );
  xor2_1 U23 ( .ip1(A[12]), .ip2(n22), .op(SUM[12]) );
  nand2_1 U24 ( .ip1(n1), .ip2(n23), .op(SUM[11]) );
  nand2_1 U25 ( .ip1(A[11]), .ip2(n24), .op(n23) );
  nor2_1 U26 ( .ip1(n24), .ip2(A[11]), .op(n22) );
  nand2_1 U27 ( .ip1(n24), .ip2(n25), .op(SUM[10]) );
  nand2_1 U28 ( .ip1(A[10]), .ip2(n3), .op(n25) );
  or2_1 U29 ( .ip1(n3), .ip2(A[10]), .op(n24) );
  or2_1 U30 ( .ip1(n5), .ip2(A[9]), .op(n3) );
  or2_1 U31 ( .ip1(n7), .ip2(A[8]), .op(n5) );
  or2_1 U32 ( .ip1(n9), .ip2(A[7]), .op(n7) );
  or2_1 U33 ( .ip1(n11), .ip2(A[6]), .op(n9) );
  or2_1 U34 ( .ip1(n13), .ip2(A[5]), .op(n11) );
  or2_1 U35 ( .ip1(n15), .ip2(A[4]), .op(n13) );
  or2_1 U36 ( .ip1(n17), .ip2(A[3]), .op(n15) );
  or2_1 U37 ( .ip1(n19), .ip2(A[2]), .op(n17) );
  or2_1 U38 ( .ip1(A[1]), .ip2(A[0]), .op(n19) );
endmodule


module usbf_idma_SSRAM_HADR14_DW01_inc_1 ( A, SUM );
  input [14:0] A;
  output [14:0] SUM;
  wire   n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20;

  inv_2 U1 ( .ip(A[0]), .op(SUM[0]) );
  inv_2 U2 ( .ip(A[12]), .op(n2) );
  inv_2 U3 ( .ip(A[11]), .op(n3) );
  inv_2 U4 ( .ip(A[8]), .op(n4) );
  inv_2 U5 ( .ip(A[7]), .op(n5) );
  inv_2 U6 ( .ip(A[4]), .op(n6) );
  inv_2 U7 ( .ip(A[3]), .op(n7) );
  xor2_1 U8 ( .ip1(A[9]), .ip2(n8), .op(SUM[9]) );
  xor2_1 U9 ( .ip1(A[8]), .ip2(n9), .op(SUM[8]) );
  nor2_1 U10 ( .ip1(n10), .ip2(n5), .op(n9) );
  xor2_1 U11 ( .ip1(n10), .ip2(n5), .op(SUM[7]) );
  xnor2_1 U12 ( .ip1(n11), .ip2(A[6]), .op(SUM[6]) );
  nand2_1 U13 ( .ip1(A[5]), .ip2(n12), .op(n11) );
  xor2_1 U14 ( .ip1(A[5]), .ip2(n12), .op(SUM[5]) );
  xor2_1 U15 ( .ip1(A[4]), .ip2(n13), .op(SUM[4]) );
  nor2_1 U16 ( .ip1(n14), .ip2(n7), .op(n13) );
  xor2_1 U17 ( .ip1(n14), .ip2(n7), .op(SUM[3]) );
  xnor2_1 U18 ( .ip1(n15), .ip2(A[2]), .op(SUM[2]) );
  nand2_1 U19 ( .ip1(A[1]), .ip2(A[0]), .op(n15) );
  xor2_1 U20 ( .ip1(A[1]), .ip2(A[0]), .op(SUM[1]) );
  xor2_1 U21 ( .ip1(A[14]), .ip2(n16), .op(SUM[14]) );
  and2_1 U22 ( .ip1(n17), .ip2(A[13]), .op(n16) );
  xor2_1 U23 ( .ip1(A[13]), .ip2(n17), .op(SUM[13]) );
  nor3_1 U24 ( .ip1(n3), .ip2(n18), .ip3(n2), .op(n17) );
  xor2_1 U25 ( .ip1(A[12]), .ip2(n19), .op(SUM[12]) );
  nor2_1 U26 ( .ip1(n18), .ip2(n3), .op(n19) );
  xor2_1 U27 ( .ip1(n18), .ip2(n3), .op(SUM[11]) );
  nand3_1 U28 ( .ip1(n8), .ip2(A[9]), .ip3(A[10]), .op(n18) );
  xnor2_1 U29 ( .ip1(n20), .ip2(A[10]), .op(SUM[10]) );
  nand2_1 U30 ( .ip1(n8), .ip2(A[9]), .op(n20) );
  nor3_1 U31 ( .ip1(n5), .ip2(n10), .ip3(n4), .op(n8) );
  nand3_1 U32 ( .ip1(A[5]), .ip2(n12), .ip3(A[6]), .op(n10) );
  nor3_1 U33 ( .ip1(n7), .ip2(n14), .ip3(n6), .op(n12) );
  nand3_1 U34 ( .ip1(A[1]), .ip2(A[0]), .ip3(A[2]), .op(n14) );
endmodule


module usbf_idma_SSRAM_HADR14_DW01_add_0 ( A, B, CI, SUM, CO );
  input [14:0] A;
  input [14:0] B;
  output [14:0] SUM;
  input CI;
  output CO;
  wire   n1;
  wire   [14:1] carry;

  fulladder U1_13 ( .a(A[13]), .b(B[13]), .ci(carry[13]), .co(carry[14]), .s(
        SUM[13]) );
  fulladder U1_12 ( .a(A[12]), .b(B[12]), .ci(carry[12]), .co(carry[13]), .s(
        SUM[12]) );
  fulladder U1_11 ( .a(A[11]), .b(B[11]), .ci(carry[11]), .co(carry[12]), .s(
        SUM[11]) );
  fulladder U1_10 ( .a(A[10]), .b(B[10]), .ci(carry[10]), .co(carry[11]), .s(
        SUM[10]) );
  fulladder U1_9 ( .a(A[9]), .b(B[9]), .ci(carry[9]), .co(carry[10]), .s(
        SUM[9]) );
  fulladder U1_8 ( .a(A[8]), .b(B[8]), .ci(carry[8]), .co(carry[9]), .s(SUM[8]) );
  fulladder U1_7 ( .a(A[7]), .b(B[7]), .ci(carry[7]), .co(carry[8]), .s(SUM[7]) );
  fulladder U1_6 ( .a(A[6]), .b(B[6]), .ci(carry[6]), .co(carry[7]), .s(SUM[6]) );
  fulladder U1_5 ( .a(A[5]), .b(B[5]), .ci(carry[5]), .co(carry[6]), .s(SUM[5]) );
  fulladder U1_4 ( .a(A[4]), .b(B[4]), .ci(carry[4]), .co(carry[5]), .s(SUM[4]) );
  fulladder U1_3 ( .a(A[3]), .b(B[3]), .ci(carry[3]), .co(carry[4]), .s(SUM[3]) );
  fulladder U1_2 ( .a(A[2]), .b(B[2]), .ci(carry[2]), .co(carry[3]), .s(SUM[2]) );
  fulladder U1_1 ( .a(A[1]), .b(B[1]), .ci(n1), .co(carry[2]), .s(SUM[1]) );
  and2_2 U1 ( .ip1(B[0]), .ip2(A[0]), .op(n1) );
  xor2_2 U2 ( .ip1(A[14]), .ip2(carry[14]), .op(SUM[14]) );
  xor2_2 U3 ( .ip1(B[0]), .ip2(A[0]), .op(SUM[0]) );
endmodule


module usbf_idma_SSRAM_HADR14 ( clk, rst, rx_data_st, rx_data_valid, 
        rx_data_done, send_data, tx_data_st, rd_next, rx_dma_en, tx_dma_en, 
        abort, idma_done, buf_size, dma_en, send_zero_length, adr, size, 
        sizu_c, madr, mdout, mdin, mwe, mreq, mack );
  input [7:0] rx_data_st;
  output [7:0] tx_data_st;
  input [13:0] buf_size;
  input [16:0] adr;
  input [13:0] size;
  output [10:0] sizu_c;
  output [14:0] madr;
  output [31:0] mdout;
  input [31:0] mdin;
  input clk, rst, rx_data_valid, rx_data_done, rd_next, rx_dma_en, tx_dma_en,
         abort, dma_en, send_zero_length, mack;
  output send_data, idma_done, mwe, mreq;
  wire   mack_r, word_done_r, mwe_d, N27, rx_data_valid_r, rx_data_done_r,
         rx_data_done_r2, tx_dma_en_r, rx_dma_en_r, send_zero_length_r, N30,
         N31, N32, N33, N34, N35, N36, N37, N38, N39, N40, N41, N42, N43, N44,
         N45, N46, N47, N48, N49, N50, N51, N52, N53, N54, N55, N56, N57, N58,
         N59, N60, N64, N65, N66, N67, N68, N69, N70, N71, N72, N73, N74, N75,
         N76, N77, N78, N82, N83, N84, dtmp_sel_r, N95, N96, N97, N98, N99,
         N100, N101, N102, N103, N104, N105, N106, N107, N108, sizd_is_zero,
         N132, N133, N134, N135, N136, N137, N138, N139, N140, N141, N142,
         N159, dtmp_sel, wr_last, N200, word_done, N201, N202, wr_done_r,
         wr_done, send_data_r, n5, n6, n7, n8, n9, n10, n11, n12, n13, n15,
         n17, n19, n21, n22, n23, n25, n27, n28, n29, n30, n31, n32, n33, n34,
         n35, n36, n41, n42, n45, n46, n47, n48, n49, n50, n51, n52, n57, n58,
         n61, n62, n63, n64, n65, n66, n67, n68, n73, n74, n77, n78, n79, n80,
         n81, n82, n83, n84, n89, n90, n93, n94, n95, n96, n97, n98, n99, n100,
         n105, n106, n109, n110, n111, n112, n113, n114, n115, n116, n121,
         n122, n125, n126, n127, n128, n129, n130, n131, n132, n134, n135,
         n136, n142, n143, n147, n148, n149, n151, n152, n153, n154, n155,
         n156, n157, n158, n159, n160, n161, n162, n163, n164, n165, n166,
         n167, n168, n169, n170, n171, n172, n173, n174, n175, n176, n177,
         n178, n179, n180, n181, n182, n183, n184, n185, n186, n187, n188,
         n189, n190, n191, n192, n193, n194, n195, n196, n197, n198, n199,
         n200, n201, n202, n203, n204, n205, n206, n207, n208, n209, n210,
         n211, n212, n213, n214, n216, n217, n218, n219, n220, n221, n222,
         n223, n224, n225, n226, n227, n228, n229, n230, n231, n232, n233,
         n234, n235, n236, n237, n238, n239, n240, n241, n242, n243, n244,
         n245, n246, n247, n248, n249, n250, n251, n252, n253, n254, n255,
         n256, n257, n258, n259, n260, n261, n262, n263, n264, n265, n266,
         n267, n268, n269, n270, n271, n272, n273, n274, n275, n276, n277,
         n278, n280, n281, n283, n284, n285, n286, n287, n288, n289, n290,
         n291, n292, n293, n294, n295, n296, n297, n298, n299, n300, n301,
         n302, n303, n304, n305, n306, n307, n308, n309, n310, n311, n312,
         n313, n314, n315, n316, n317, n318, n319, n320, n321, n322, n323,
         n324, n325, n326, n327, n328, n329, n330, n331, n332, n333, n334,
         n335, n336, n337, n338, n339, n340, n341, n342, n343, n344, n345,
         n346, n348, n349, n350, n351, n352, n353, n355, n356, n358, n359,
         n361, n362, n365, n366, n369, n370, n373, n374, n377, n378, n381,
         n382, n385, n386, n389, n391, n392, n393, n394, n396, n397, n398,
         n399, n401, n402, n404, n405, n407, n408, n410, n411, n413, n414,
         n416, n417, n419, n421, n422, n424, n425, n426, n427, n429, n430,
         n432, n433, n435, n436, n438, n439, n441, n442, n444, n445, n447,
         n449, n450, n452, n453, n454, n455, n457, n458, n460, n461, n463,
         n464, n466, n467, n469, n470, n472, n473, n475, n476, n478, n479,
         n480, n481, n482, n483, n484, n485, n486, n487, n490, n491, n492,
         n494, n495, n496, n497, n498, n499, n500, n501, n504, n505, n506,
         n507, n509, n511, n512, n513, n514, n515, n516, n517, n518, n519,
         n520, n521, n522, n523, n524, n525, n526, n527, n528, n529, n530,
         n531, n532, n533, n534, n535, n536, n537, n538, n539, n540, n541,
         n542, n543, n544, n545, n546, n547, n548, n549, n550, n551, n552,
         n553, n554, n555, n556, n557, n558, n560, n561, n562, n563, n564,
         n565, n566, n567, n568, n569, n571, n573, n575, n576, n577, n578,
         n579, n580, n581, n582, n583, n584, n585, n588, n589, n591, n592,
         n593, n596, n597, n598, n599, n600, n601, n602, n603, n604, n605,
         n606, n607, n608, n609, n610, n611, n612, n613, n614, n615, n617,
         n618, n619, n620, n621, n623, n624, n625, n626, n627, n628, n629,
         n630, n632, n633, n634, n635, n636, n637, n639, n640, n641, n642,
         n643, n644, n645, n646, n647, n648, n649, n650, n651, n652, n653,
         n654, n655, n656, n657, n658, n659, n660, n661, n662, n663, n664,
         n665, n666, n667, n668, n669, n670, n671, n672, n673, n674, n675,
         n676, n677, n678, n679, n680, n681, n682, n683, n684, n685, n686,
         n687, n688, n689, n690, n691, n692, n693, n694, n695, n696, n700,
         n701, n702, n705, n706, n707, n708, n709, n710, n711, n712, n713,
         n714, n715, n716, n717, n718, n719, n720, n721, n722, n723, n724,
         n725, n726, n727, n728, n729, n730, n731, n732, n733, n734, n735,
         n736, n737, n738, n739, n740, n741, n742, n743, n744, n745, n746,
         n747, n748, n749, n750, n751, n752, n753, n754, n755, n756, n757,
         n758, n759, n760, n761, n762, n763, n764, n765, n766, n767, n768,
         n769, n770, n771, n772, n773, n774, n775, n776, n777, n778, n779,
         n780, n781, n782, n783, n784, n785, n786, n787, n788, n789, n790,
         n791, n792, n793, n794, n795, n796, n797, n798, n799, n800, n801,
         n802, n804, n805, n806, n807, n808, n809, n810, n811, n812, n813,
         n814, n815, n816, n817, n818, n819, n820, n821, n822, n823, n824,
         n825, n826, n827, n828, n829, n830, n831, n832, n833, n834, n835,
         n836, n837, n838, n839, n840, n841, n842, n843, n844, n845, n846,
         n847, n848, n849, n850, n851, n852, n853, n854, n855, n856, n857,
         n858, n859, n860, n861, n862, n863, n864, n865, n866, n867, n868,
         n869, n1, n3, n4, n14, n16, n18, n20, n24, n26, n37, n38, n39, n40,
         n43, n44, n53, n54, n55, n56, n59, n60, n69, n70, n71, n72, n75, n76,
         n85, n86, n87, n88, n91, n92, n101, n102, n103, n104, n107, n108,
         n117, n118, n119, n120, n123, n124, n133, n137, n138, n139, n140,
         n141, n144, n145, n146, n150, n215, n279, n282, n347, n354, n357,
         n360, n363, n364, n367, n368, n371, n372, n375, n376, n379, n380,
         n383, n384, n387, n388, n390, n395, n400, n403, n406, n409, n412,
         n415, n418, n420, n423, n428, n431, n434, n437, n440, n443, n446,
         n448, n451, n456, n459, n462, n465, n468, n471, n474, n477, n488,
         n489, n493, n502, n503, n508, n510, n559, n570, n572, n574, n586,
         n587, n590, n594, n595, n616, n622, n631, n638, n697, n698, n699,
         n703, n704, n803, n870, n871, n872, n873, n874, n875, n876, n877,
         n878, n879, n880, n881, n882, n883, n884, n885, n886, n887, n888,
         n889, n890, n891, n892, n893, n894, n895, n896, n897, n898, n899,
         n900, n901, n902, n903, n904, n905, n906, n907, n908, n909, n910,
         n911, n912, n913, n914, n915, n916, n917, n918, n919, n920, n921,
         n922, n923, n924, n925, n926, n927, n928, n929, n930, n931, n932,
         n933, n934, n935, n936, n937, n938, n939, n940, n941, n942, n943,
         n944, n945, n946, n947, n948, n949, n950, n951, n952, n953, n954,
         n955, n956, n957, n958, n959, n960, n961, n962, n963, n964, n965,
         n966, n967, n968, n969, n970, n971, n972, n973, n974, n975, n976,
         n977, n978, n979, n980, n981, n982, n983, n984, n985, n986, n987,
         n988, n989, n990, n991, n992, n993, n994, n995;
  wire   [7:0] rx_data_st_r;
  wire   [14:0] last_buf_adr;
  wire   [14:0] adrw_next;
  wire   [2:0] adr_cb;
  wire   [13:0] sizd_c;
  wire   [31:0] dtmp_r;
  wire   [31:0] rd_buf0;
  wire   [31:0] rd_buf1;
  wire   [7:0] state;

  ab_or_c_or_d U447 ( .ip1(dtmp_sel_r), .ip2(mdin[31]), .ip3(n355), .ip4(n356), 
        .op(n805) );
  ab_or_c_or_d U451 ( .ip1(dtmp_sel_r), .ip2(mdin[30]), .ip3(n361), .ip4(n362), 
        .op(n806) );
  ab_or_c_or_d U455 ( .ip1(dtmp_sel_r), .ip2(mdin[29]), .ip3(n365), .ip4(n366), 
        .op(n807) );
  ab_or_c_or_d U459 ( .ip1(dtmp_sel_r), .ip2(mdin[28]), .ip3(n369), .ip4(n370), 
        .op(n808) );
  ab_or_c_or_d U463 ( .ip1(dtmp_sel_r), .ip2(mdin[27]), .ip3(n373), .ip4(n374), 
        .op(n809) );
  ab_or_c_or_d U467 ( .ip1(dtmp_sel_r), .ip2(mdin[26]), .ip3(n377), .ip4(n378), 
        .op(n810) );
  ab_or_c_or_d U471 ( .ip1(dtmp_sel_r), .ip2(mdin[25]), .ip3(n381), .ip4(n382), 
        .op(n811) );
  ab_or_c_or_d U475 ( .ip1(dtmp_sel_r), .ip2(mdin[24]), .ip3(n385), .ip4(n386), 
        .op(n812) );
  ab_or_c_or_d U482 ( .ip1(dtmp_sel_r), .ip2(mdin[23]), .ip3(n393), .ip4(n394), 
        .op(n813) );
  ab_or_c_or_d U486 ( .ip1(dtmp_sel_r), .ip2(mdin[22]), .ip3(n398), .ip4(n399), 
        .op(n814) );
  ab_or_c_or_d U490 ( .ip1(dtmp_sel_r), .ip2(mdin[21]), .ip3(n401), .ip4(n402), 
        .op(n815) );
  ab_or_c_or_d U494 ( .ip1(dtmp_sel_r), .ip2(mdin[20]), .ip3(n404), .ip4(n405), 
        .op(n816) );
  ab_or_c_or_d U498 ( .ip1(dtmp_sel_r), .ip2(mdin[19]), .ip3(n407), .ip4(n408), 
        .op(n817) );
  ab_or_c_or_d U502 ( .ip1(dtmp_sel_r), .ip2(mdin[18]), .ip3(n410), .ip4(n411), 
        .op(n818) );
  ab_or_c_or_d U506 ( .ip1(dtmp_sel_r), .ip2(mdin[17]), .ip3(n413), .ip4(n414), 
        .op(n819) );
  ab_or_c_or_d U510 ( .ip1(dtmp_sel_r), .ip2(mdin[16]), .ip3(n416), .ip4(n417), 
        .op(n820) );
  ab_or_c_or_d U517 ( .ip1(dtmp_sel_r), .ip2(mdin[15]), .ip3(n421), .ip4(n422), 
        .op(n821) );
  ab_or_c_or_d U521 ( .ip1(dtmp_sel_r), .ip2(mdin[14]), .ip3(n426), .ip4(n427), 
        .op(n822) );
  ab_or_c_or_d U525 ( .ip1(dtmp_sel_r), .ip2(mdin[13]), .ip3(n429), .ip4(n430), 
        .op(n823) );
  ab_or_c_or_d U529 ( .ip1(dtmp_sel_r), .ip2(mdin[12]), .ip3(n432), .ip4(n433), 
        .op(n824) );
  ab_or_c_or_d U533 ( .ip1(dtmp_sel_r), .ip2(mdin[11]), .ip3(n435), .ip4(n436), 
        .op(n825) );
  ab_or_c_or_d U537 ( .ip1(dtmp_sel_r), .ip2(mdin[10]), .ip3(n438), .ip4(n439), 
        .op(n826) );
  ab_or_c_or_d U541 ( .ip1(dtmp_sel_r), .ip2(mdin[9]), .ip3(n441), .ip4(n442), 
        .op(n827) );
  ab_or_c_or_d U545 ( .ip1(dtmp_sel_r), .ip2(mdin[8]), .ip3(n444), .ip4(n445), 
        .op(n828) );
  ab_or_c_or_d U552 ( .ip1(dtmp_sel_r), .ip2(mdin[7]), .ip3(n449), .ip4(n450), 
        .op(n829) );
  ab_or_c_or_d U557 ( .ip1(dtmp_sel_r), .ip2(mdin[6]), .ip3(n454), .ip4(n455), 
        .op(n830) );
  ab_or_c_or_d U562 ( .ip1(dtmp_sel_r), .ip2(mdin[5]), .ip3(n457), .ip4(n458), 
        .op(n831) );
  ab_or_c_or_d U567 ( .ip1(dtmp_sel_r), .ip2(mdin[4]), .ip3(n460), .ip4(n461), 
        .op(n832) );
  ab_or_c_or_d U572 ( .ip1(dtmp_sel_r), .ip2(mdin[3]), .ip3(n463), .ip4(n464), 
        .op(n833) );
  ab_or_c_or_d U577 ( .ip1(dtmp_sel_r), .ip2(mdin[2]), .ip3(n466), .ip4(n467), 
        .op(n834) );
  ab_or_c_or_d U582 ( .ip1(dtmp_sel_r), .ip2(mdin[1]), .ip3(n469), .ip4(n470), 
        .op(n835) );
  ab_or_c_or_d U587 ( .ip1(dtmp_sel_r), .ip2(mdin[0]), .ip3(n472), .ip4(n473), 
        .op(n836) );
  not_ab_or_c_or_d U691 ( .ip1(n896), .ip2(n941), .ip3(abort), .ip4(n569), 
        .op(n568) );
  not_ab_or_c_or_d U706 ( .ip1(sizd_is_zero), .ip2(n901), .ip3(n504), .ip4(
        n582), .op(n581) );
  inv_1 U779 ( .ip(rst), .op(n504) );
  ab_or_c_or_d U878 ( .ip1(n588), .ip2(n941), .ip3(word_done_r), .ip4(dtmp_sel), .op(mreq) );
  not_ab_or_c_or_d U891 ( .ip1(n900), .ip2(n702), .ip3(rx_data_valid_r), .ip4(
        n476), .op(N202) );
  nand4_2 U3 ( .ip1(n5), .ip2(n6), .ip3(n7), .ip4(n8), .op(tx_data_st[7]) );
  nor4_2 U4 ( .ip1(n9), .ip2(n10), .ip3(n11), .ip4(n12), .op(n8) );
  nor2_2 U5 ( .ip1(n13), .ip2(n949), .op(n12) );
  nor2_2 U7 ( .ip1(n15), .ip2(n957), .op(n11) );
  nor2_2 U11 ( .ip1(n17), .ip2(n965), .op(n10) );
  nor2_2 U13 ( .ip1(n19), .ip2(n973), .op(n9) );
  nor2_2 U15 ( .ip1(n21), .ip2(n22), .op(n7) );
  nor2_2 U16 ( .ip1(n23), .ip2(n986), .op(n22) );
  nor2_2 U18 ( .ip1(n25), .ip2(n994), .op(n21) );
  nand2_2 U20 ( .ip1(rd_buf1[15]), .ip2(n27), .op(n6) );
  nand2_2 U21 ( .ip1(rd_buf1[7]), .ip2(n28), .op(n5) );
  nand4_2 U22 ( .ip1(n29), .ip2(n30), .ip3(n31), .ip4(n32), .op(tx_data_st[6])
         );
  nor4_2 U23 ( .ip1(n33), .ip2(n34), .ip3(n35), .ip4(n36), .op(n32) );
  nor2_2 U24 ( .ip1(n13), .ip2(n948), .op(n36) );
  nor2_2 U26 ( .ip1(n15), .ip2(n956), .op(n35) );
  nor2_2 U28 ( .ip1(n17), .ip2(n964), .op(n34) );
  nor2_2 U30 ( .ip1(n19), .ip2(n972), .op(n33) );
  nor2_2 U32 ( .ip1(n41), .ip2(n42), .op(n31) );
  nor2_2 U33 ( .ip1(n23), .ip2(n985), .op(n42) );
  nor2_2 U35 ( .ip1(n25), .ip2(n993), .op(n41) );
  nand2_2 U37 ( .ip1(rd_buf1[14]), .ip2(n27), .op(n30) );
  nand2_2 U38 ( .ip1(rd_buf1[6]), .ip2(n28), .op(n29) );
  nand4_2 U39 ( .ip1(n45), .ip2(n46), .ip3(n47), .ip4(n48), .op(tx_data_st[5])
         );
  nor4_2 U40 ( .ip1(n49), .ip2(n50), .ip3(n51), .ip4(n52), .op(n48) );
  nor2_2 U41 ( .ip1(n13), .ip2(n947), .op(n52) );
  nor2_2 U43 ( .ip1(n15), .ip2(n955), .op(n51) );
  nor2_2 U45 ( .ip1(n17), .ip2(n963), .op(n50) );
  nor2_2 U47 ( .ip1(n19), .ip2(n971), .op(n49) );
  nor2_2 U49 ( .ip1(n57), .ip2(n58), .op(n47) );
  nor2_2 U50 ( .ip1(n23), .ip2(n984), .op(n58) );
  nor2_2 U52 ( .ip1(n25), .ip2(n992), .op(n57) );
  nand2_2 U54 ( .ip1(rd_buf1[13]), .ip2(n27), .op(n46) );
  nand2_2 U55 ( .ip1(rd_buf1[5]), .ip2(n28), .op(n45) );
  nand4_2 U56 ( .ip1(n61), .ip2(n62), .ip3(n63), .ip4(n64), .op(tx_data_st[4])
         );
  nor4_2 U57 ( .ip1(n65), .ip2(n66), .ip3(n67), .ip4(n68), .op(n64) );
  nor2_2 U58 ( .ip1(n13), .ip2(n946), .op(n68) );
  nor2_2 U60 ( .ip1(n15), .ip2(n954), .op(n67) );
  nor2_2 U62 ( .ip1(n17), .ip2(n962), .op(n66) );
  nor2_2 U64 ( .ip1(n19), .ip2(n970), .op(n65) );
  nor2_2 U66 ( .ip1(n73), .ip2(n74), .op(n63) );
  nor2_2 U67 ( .ip1(n23), .ip2(n983), .op(n74) );
  nor2_2 U69 ( .ip1(n25), .ip2(n991), .op(n73) );
  nand2_2 U71 ( .ip1(rd_buf1[12]), .ip2(n27), .op(n62) );
  nand2_2 U72 ( .ip1(rd_buf1[4]), .ip2(n28), .op(n61) );
  nand4_2 U73 ( .ip1(n77), .ip2(n78), .ip3(n79), .ip4(n80), .op(tx_data_st[3])
         );
  nor4_2 U74 ( .ip1(n81), .ip2(n82), .ip3(n83), .ip4(n84), .op(n80) );
  nor2_2 U75 ( .ip1(n13), .ip2(n945), .op(n84) );
  nor2_2 U77 ( .ip1(n15), .ip2(n953), .op(n83) );
  nor2_2 U79 ( .ip1(n17), .ip2(n961), .op(n82) );
  nor2_2 U81 ( .ip1(n19), .ip2(n969), .op(n81) );
  nor2_2 U83 ( .ip1(n89), .ip2(n90), .op(n79) );
  nor2_2 U84 ( .ip1(n23), .ip2(n982), .op(n90) );
  nor2_2 U86 ( .ip1(n25), .ip2(n990), .op(n89) );
  nand2_2 U88 ( .ip1(rd_buf1[11]), .ip2(n27), .op(n78) );
  nand2_2 U89 ( .ip1(rd_buf1[3]), .ip2(n28), .op(n77) );
  nand4_2 U90 ( .ip1(n93), .ip2(n94), .ip3(n95), .ip4(n96), .op(tx_data_st[2])
         );
  nor4_2 U91 ( .ip1(n97), .ip2(n98), .ip3(n99), .ip4(n100), .op(n96) );
  nor2_2 U92 ( .ip1(n13), .ip2(n944), .op(n100) );
  nor2_2 U94 ( .ip1(n15), .ip2(n952), .op(n99) );
  nor2_2 U96 ( .ip1(n17), .ip2(n960), .op(n98) );
  nor2_2 U98 ( .ip1(n19), .ip2(n968), .op(n97) );
  nor2_2 U100 ( .ip1(n105), .ip2(n106), .op(n95) );
  nor2_2 U101 ( .ip1(n23), .ip2(n981), .op(n106) );
  nor2_2 U103 ( .ip1(n25), .ip2(n989), .op(n105) );
  nand2_2 U105 ( .ip1(rd_buf1[10]), .ip2(n27), .op(n94) );
  nand2_2 U106 ( .ip1(rd_buf1[2]), .ip2(n28), .op(n93) );
  nand4_2 U107 ( .ip1(n109), .ip2(n110), .ip3(n111), .ip4(n112), .op(
        tx_data_st[1]) );
  nor4_2 U108 ( .ip1(n113), .ip2(n114), .ip3(n115), .ip4(n116), .op(n112) );
  nor2_2 U109 ( .ip1(n13), .ip2(n943), .op(n116) );
  nor2_2 U111 ( .ip1(n15), .ip2(n951), .op(n115) );
  nor2_2 U113 ( .ip1(n17), .ip2(n959), .op(n114) );
  nor2_2 U115 ( .ip1(n19), .ip2(n967), .op(n113) );
  nor2_2 U117 ( .ip1(n121), .ip2(n122), .op(n111) );
  nor2_2 U118 ( .ip1(n23), .ip2(n980), .op(n122) );
  nor2_2 U120 ( .ip1(n25), .ip2(n988), .op(n121) );
  nand2_2 U122 ( .ip1(rd_buf1[9]), .ip2(n27), .op(n110) );
  nand2_2 U123 ( .ip1(rd_buf1[1]), .ip2(n28), .op(n109) );
  nand4_2 U124 ( .ip1(n125), .ip2(n126), .ip3(n127), .ip4(n128), .op(
        tx_data_st[0]) );
  nor4_2 U125 ( .ip1(n129), .ip2(n130), .ip3(n131), .ip4(n132), .op(n128) );
  nor2_2 U126 ( .ip1(n13), .ip2(n942), .op(n132) );
  nand3_2 U128 ( .ip1(n134), .ip2(n135), .ip3(n136), .op(n13) );
  nor2_2 U129 ( .ip1(n15), .ip2(n950), .op(n131) );
  nand3_2 U131 ( .ip1(n134), .ip2(n879), .ip3(n136), .op(n15) );
  nor2_2 U132 ( .ip1(n17), .ip2(n958), .op(n130) );
  nand3_2 U134 ( .ip1(n881), .ip2(n135), .ip3(n134), .op(n17) );
  nor2_2 U135 ( .ip1(n19), .ip2(n966), .op(n129) );
  nand3_2 U137 ( .ip1(n134), .ip2(n881), .ip3(n879), .op(n19) );
  nor2_2 U138 ( .ip1(n142), .ip2(n143), .op(n127) );
  nor2_2 U139 ( .ip1(n23), .ip2(n979), .op(n143) );
  nand3_2 U141 ( .ip1(n880), .ip2(n135), .ip3(n881), .op(n23) );
  nor2_2 U142 ( .ip1(n25), .ip2(n987), .op(n142) );
  nand3_2 U144 ( .ip1(n881), .ip2(n880), .ip3(n879), .op(n25) );
  nand2_2 U145 ( .ip1(rd_buf1[8]), .ip2(n27), .op(n126) );
  nand2_2 U147 ( .ip1(rd_buf1[0]), .ip2(n28), .op(n125) );
  or2_2 U149 ( .ip1(send_zero_length_r), .ip2(send_data_r), .op(send_data) );
  nand2_2 U150 ( .ip1(n147), .ip2(n148), .op(n707) );
  nand2_2 U151 ( .ip1(rd_buf1[31]), .ip2(n144), .op(n148) );
  nand2_2 U152 ( .ip1(mdin[31]), .ip2(n103), .op(n147) );
  nand2_2 U153 ( .ip1(n151), .ip2(n152), .op(n708) );
  nand2_2 U154 ( .ip1(rd_buf1[30]), .ip2(n144), .op(n152) );
  nand2_2 U155 ( .ip1(mdin[30]), .ip2(n102), .op(n151) );
  nand2_2 U156 ( .ip1(n153), .ip2(n154), .op(n709) );
  nand2_2 U157 ( .ip1(rd_buf1[29]), .ip2(n141), .op(n154) );
  nand2_2 U158 ( .ip1(mdin[29]), .ip2(n103), .op(n153) );
  nand2_2 U159 ( .ip1(n155), .ip2(n156), .op(n710) );
  nand2_2 U160 ( .ip1(rd_buf1[28]), .ip2(n141), .op(n156) );
  nand2_2 U161 ( .ip1(mdin[28]), .ip2(n102), .op(n155) );
  nand2_2 U162 ( .ip1(n157), .ip2(n158), .op(n711) );
  nand2_2 U163 ( .ip1(rd_buf1[27]), .ip2(n140), .op(n158) );
  nand2_2 U164 ( .ip1(mdin[27]), .ip2(n103), .op(n157) );
  nand2_2 U165 ( .ip1(n159), .ip2(n160), .op(n712) );
  nand2_2 U166 ( .ip1(rd_buf1[26]), .ip2(n140), .op(n160) );
  nand2_2 U167 ( .ip1(mdin[26]), .ip2(n102), .op(n159) );
  nand2_2 U168 ( .ip1(n161), .ip2(n162), .op(n713) );
  nand2_2 U169 ( .ip1(rd_buf1[25]), .ip2(n139), .op(n162) );
  nand2_2 U170 ( .ip1(mdin[25]), .ip2(n103), .op(n161) );
  nand2_2 U171 ( .ip1(n163), .ip2(n164), .op(n714) );
  nand2_2 U172 ( .ip1(rd_buf1[24]), .ip2(n139), .op(n164) );
  nand2_2 U173 ( .ip1(mdin[24]), .ip2(n102), .op(n163) );
  nand2_2 U174 ( .ip1(n165), .ip2(n166), .op(n715) );
  nand2_2 U175 ( .ip1(rd_buf1[23]), .ip2(n138), .op(n166) );
  nand2_2 U176 ( .ip1(mdin[23]), .ip2(n103), .op(n165) );
  nand2_2 U177 ( .ip1(n167), .ip2(n168), .op(n716) );
  nand2_2 U178 ( .ip1(rd_buf1[22]), .ip2(n138), .op(n168) );
  nand2_2 U179 ( .ip1(mdin[22]), .ip2(n103), .op(n167) );
  nand2_2 U180 ( .ip1(n169), .ip2(n170), .op(n717) );
  nand2_2 U181 ( .ip1(rd_buf1[21]), .ip2(n137), .op(n170) );
  nand2_2 U182 ( .ip1(mdin[21]), .ip2(n103), .op(n169) );
  nand2_2 U183 ( .ip1(n171), .ip2(n172), .op(n718) );
  nand2_2 U184 ( .ip1(rd_buf1[20]), .ip2(n137), .op(n172) );
  nand2_2 U185 ( .ip1(mdin[20]), .ip2(n103), .op(n171) );
  nand2_2 U186 ( .ip1(n173), .ip2(n174), .op(n719) );
  nand2_2 U187 ( .ip1(rd_buf1[19]), .ip2(n133), .op(n174) );
  nand2_2 U188 ( .ip1(mdin[19]), .ip2(n102), .op(n173) );
  nand2_2 U189 ( .ip1(n175), .ip2(n176), .op(n720) );
  nand2_2 U190 ( .ip1(rd_buf1[18]), .ip2(n133), .op(n176) );
  nand2_2 U191 ( .ip1(mdin[18]), .ip2(n102), .op(n175) );
  nand2_2 U192 ( .ip1(n177), .ip2(n178), .op(n721) );
  nand2_2 U193 ( .ip1(rd_buf1[17]), .ip2(n124), .op(n178) );
  nand2_2 U194 ( .ip1(mdin[17]), .ip2(n102), .op(n177) );
  nand2_2 U195 ( .ip1(n179), .ip2(n180), .op(n722) );
  nand2_2 U196 ( .ip1(rd_buf1[16]), .ip2(n124), .op(n180) );
  nand2_2 U197 ( .ip1(mdin[16]), .ip2(n102), .op(n179) );
  nand2_2 U198 ( .ip1(n181), .ip2(n182), .op(n723) );
  nand2_2 U199 ( .ip1(rd_buf1[15]), .ip2(n123), .op(n182) );
  nand2_2 U200 ( .ip1(mdin[15]), .ip2(n102), .op(n181) );
  nand2_2 U201 ( .ip1(n183), .ip2(n184), .op(n724) );
  nand2_2 U202 ( .ip1(rd_buf1[14]), .ip2(n123), .op(n184) );
  nand2_2 U203 ( .ip1(mdin[14]), .ip2(n102), .op(n183) );
  nand2_2 U204 ( .ip1(n185), .ip2(n186), .op(n725) );
  nand2_2 U205 ( .ip1(rd_buf1[13]), .ip2(n120), .op(n186) );
  nand2_2 U206 ( .ip1(mdin[13]), .ip2(n102), .op(n185) );
  nand2_2 U207 ( .ip1(n187), .ip2(n188), .op(n726) );
  nand2_2 U208 ( .ip1(rd_buf1[12]), .ip2(n120), .op(n188) );
  nand2_2 U209 ( .ip1(mdin[12]), .ip2(n102), .op(n187) );
  nand2_2 U210 ( .ip1(n189), .ip2(n190), .op(n727) );
  nand2_2 U211 ( .ip1(rd_buf1[11]), .ip2(n119), .op(n190) );
  nand2_2 U212 ( .ip1(mdin[11]), .ip2(n102), .op(n189) );
  nand2_2 U213 ( .ip1(n191), .ip2(n192), .op(n728) );
  nand2_2 U214 ( .ip1(rd_buf1[10]), .ip2(n119), .op(n192) );
  nand2_2 U215 ( .ip1(mdin[10]), .ip2(n102), .op(n191) );
  nand2_2 U216 ( .ip1(n193), .ip2(n194), .op(n729) );
  nand2_2 U217 ( .ip1(rd_buf1[9]), .ip2(n118), .op(n194) );
  nand2_2 U218 ( .ip1(mdin[9]), .ip2(n102), .op(n193) );
  nand2_2 U219 ( .ip1(n195), .ip2(n196), .op(n730) );
  nand2_2 U220 ( .ip1(rd_buf1[8]), .ip2(n118), .op(n196) );
  nand2_2 U221 ( .ip1(mdin[8]), .ip2(n102), .op(n195) );
  nand2_2 U222 ( .ip1(n197), .ip2(n198), .op(n731) );
  nand2_2 U223 ( .ip1(rd_buf1[7]), .ip2(n117), .op(n198) );
  nand2_2 U224 ( .ip1(mdin[7]), .ip2(n103), .op(n197) );
  nand2_2 U225 ( .ip1(n199), .ip2(n200), .op(n732) );
  nand2_2 U226 ( .ip1(rd_buf1[6]), .ip2(n117), .op(n200) );
  nand2_2 U227 ( .ip1(mdin[6]), .ip2(n103), .op(n199) );
  nand2_2 U228 ( .ip1(n201), .ip2(n202), .op(n733) );
  nand2_2 U229 ( .ip1(rd_buf1[5]), .ip2(n108), .op(n202) );
  nand2_2 U230 ( .ip1(mdin[5]), .ip2(n103), .op(n201) );
  nand2_2 U231 ( .ip1(n203), .ip2(n204), .op(n734) );
  nand2_2 U232 ( .ip1(rd_buf1[4]), .ip2(n108), .op(n204) );
  nand2_2 U233 ( .ip1(mdin[4]), .ip2(n103), .op(n203) );
  nand2_2 U234 ( .ip1(n205), .ip2(n206), .op(n735) );
  nand2_2 U235 ( .ip1(rd_buf1[3]), .ip2(n107), .op(n206) );
  nand2_2 U236 ( .ip1(mdin[3]), .ip2(n103), .op(n205) );
  nand2_2 U237 ( .ip1(n207), .ip2(n208), .op(n736) );
  nand2_2 U238 ( .ip1(rd_buf1[2]), .ip2(n107), .op(n208) );
  nand2_2 U239 ( .ip1(mdin[2]), .ip2(n103), .op(n207) );
  nand2_2 U240 ( .ip1(n209), .ip2(n210), .op(n737) );
  nand2_2 U241 ( .ip1(rd_buf1[1]), .ip2(n104), .op(n210) );
  nand2_2 U242 ( .ip1(mdin[1]), .ip2(n103), .op(n209) );
  nand2_2 U243 ( .ip1(n211), .ip2(n212), .op(n738) );
  nand2_2 U244 ( .ip1(rd_buf1[0]), .ip2(n104), .op(n212) );
  nand2_2 U245 ( .ip1(mdin[0]), .ip2(n103), .op(n211) );
  nand2_2 U247 ( .ip1(madr[0]), .ip2(mack_r), .op(n149) );
  nand2_2 U248 ( .ip1(n213), .ip2(n214), .op(n739) );
  nand2_2 U249 ( .ip1(rd_buf0[31]), .ip2(n38), .op(n214) );
  nand2_2 U250 ( .ip1(n76), .ip2(mdin[31]), .op(n213) );
  nand2_2 U251 ( .ip1(n217), .ip2(n218), .op(n740) );
  nand2_2 U252 ( .ip1(rd_buf0[30]), .ip2(n37), .op(n218) );
  nand2_2 U253 ( .ip1(n76), .ip2(mdin[30]), .op(n217) );
  nand2_2 U254 ( .ip1(n219), .ip2(n220), .op(n741) );
  nand2_2 U255 ( .ip1(rd_buf0[29]), .ip2(n38), .op(n220) );
  nand2_2 U256 ( .ip1(n75), .ip2(mdin[29]), .op(n219) );
  nand2_2 U257 ( .ip1(n221), .ip2(n222), .op(n742) );
  nand2_2 U258 ( .ip1(rd_buf0[28]), .ip2(n37), .op(n222) );
  nand2_2 U259 ( .ip1(n75), .ip2(mdin[28]), .op(n221) );
  nand2_2 U260 ( .ip1(n223), .ip2(n224), .op(n743) );
  nand2_2 U261 ( .ip1(rd_buf0[27]), .ip2(n38), .op(n224) );
  nand2_2 U262 ( .ip1(n72), .ip2(mdin[27]), .op(n223) );
  nand2_2 U263 ( .ip1(n225), .ip2(n226), .op(n744) );
  nand2_2 U264 ( .ip1(rd_buf0[26]), .ip2(n37), .op(n226) );
  nand2_2 U265 ( .ip1(n72), .ip2(mdin[26]), .op(n225) );
  nand2_2 U266 ( .ip1(n227), .ip2(n228), .op(n745) );
  nand2_2 U267 ( .ip1(rd_buf0[25]), .ip2(n38), .op(n228) );
  nand2_2 U268 ( .ip1(n71), .ip2(mdin[25]), .op(n227) );
  nand2_2 U269 ( .ip1(n229), .ip2(n230), .op(n746) );
  nand2_2 U270 ( .ip1(rd_buf0[24]), .ip2(n37), .op(n230) );
  nand2_2 U271 ( .ip1(n71), .ip2(mdin[24]), .op(n229) );
  nand2_2 U272 ( .ip1(n231), .ip2(n232), .op(n747) );
  nand2_2 U273 ( .ip1(rd_buf0[23]), .ip2(n38), .op(n232) );
  nand2_2 U274 ( .ip1(n70), .ip2(mdin[23]), .op(n231) );
  nand2_2 U275 ( .ip1(n233), .ip2(n234), .op(n748) );
  nand2_2 U276 ( .ip1(rd_buf0[22]), .ip2(n38), .op(n234) );
  nand2_2 U277 ( .ip1(n70), .ip2(mdin[22]), .op(n233) );
  nand2_2 U278 ( .ip1(n235), .ip2(n236), .op(n749) );
  nand2_2 U279 ( .ip1(rd_buf0[21]), .ip2(n38), .op(n236) );
  nand2_2 U280 ( .ip1(n69), .ip2(mdin[21]), .op(n235) );
  nand2_2 U281 ( .ip1(n237), .ip2(n238), .op(n750) );
  nand2_2 U282 ( .ip1(rd_buf0[20]), .ip2(n38), .op(n238) );
  nand2_2 U283 ( .ip1(n69), .ip2(mdin[20]), .op(n237) );
  nand2_2 U284 ( .ip1(n239), .ip2(n240), .op(n751) );
  nand2_2 U285 ( .ip1(rd_buf0[19]), .ip2(n37), .op(n240) );
  nand2_2 U286 ( .ip1(n60), .ip2(mdin[19]), .op(n239) );
  nand2_2 U287 ( .ip1(n241), .ip2(n242), .op(n752) );
  nand2_2 U288 ( .ip1(rd_buf0[18]), .ip2(n37), .op(n242) );
  nand2_2 U289 ( .ip1(n60), .ip2(mdin[18]), .op(n241) );
  nand2_2 U290 ( .ip1(n243), .ip2(n244), .op(n753) );
  nand2_2 U291 ( .ip1(rd_buf0[17]), .ip2(n37), .op(n244) );
  nand2_2 U292 ( .ip1(n59), .ip2(mdin[17]), .op(n243) );
  nand2_2 U293 ( .ip1(n245), .ip2(n246), .op(n754) );
  nand2_2 U294 ( .ip1(rd_buf0[16]), .ip2(n37), .op(n246) );
  nand2_2 U295 ( .ip1(n59), .ip2(mdin[16]), .op(n245) );
  nand2_2 U296 ( .ip1(n247), .ip2(n248), .op(n755) );
  nand2_2 U297 ( .ip1(rd_buf0[15]), .ip2(n37), .op(n248) );
  nand2_2 U298 ( .ip1(n56), .ip2(mdin[15]), .op(n247) );
  nand2_2 U299 ( .ip1(n249), .ip2(n250), .op(n756) );
  nand2_2 U300 ( .ip1(rd_buf0[14]), .ip2(n37), .op(n250) );
  nand2_2 U301 ( .ip1(n56), .ip2(mdin[14]), .op(n249) );
  nand2_2 U302 ( .ip1(n251), .ip2(n252), .op(n757) );
  nand2_2 U303 ( .ip1(rd_buf0[13]), .ip2(n37), .op(n252) );
  nand2_2 U304 ( .ip1(n55), .ip2(mdin[13]), .op(n251) );
  nand2_2 U305 ( .ip1(n253), .ip2(n254), .op(n758) );
  nand2_2 U306 ( .ip1(rd_buf0[12]), .ip2(n37), .op(n254) );
  nand2_2 U307 ( .ip1(n55), .ip2(mdin[12]), .op(n253) );
  nand2_2 U308 ( .ip1(n255), .ip2(n256), .op(n759) );
  nand2_2 U309 ( .ip1(rd_buf0[11]), .ip2(n37), .op(n256) );
  nand2_2 U310 ( .ip1(n54), .ip2(mdin[11]), .op(n255) );
  nand2_2 U311 ( .ip1(n257), .ip2(n258), .op(n760) );
  nand2_2 U312 ( .ip1(rd_buf0[10]), .ip2(n37), .op(n258) );
  nand2_2 U313 ( .ip1(n54), .ip2(mdin[10]), .op(n257) );
  nand2_2 U314 ( .ip1(n259), .ip2(n260), .op(n761) );
  nand2_2 U315 ( .ip1(rd_buf0[9]), .ip2(n37), .op(n260) );
  nand2_2 U316 ( .ip1(n53), .ip2(mdin[9]), .op(n259) );
  nand2_2 U317 ( .ip1(n261), .ip2(n262), .op(n762) );
  nand2_2 U318 ( .ip1(rd_buf0[8]), .ip2(n37), .op(n262) );
  nand2_2 U319 ( .ip1(n53), .ip2(mdin[8]), .op(n261) );
  nand2_2 U320 ( .ip1(n263), .ip2(n264), .op(n763) );
  nand2_2 U321 ( .ip1(rd_buf0[7]), .ip2(n38), .op(n264) );
  nand2_2 U322 ( .ip1(n44), .ip2(mdin[7]), .op(n263) );
  nand2_2 U323 ( .ip1(n265), .ip2(n266), .op(n764) );
  nand2_2 U324 ( .ip1(rd_buf0[6]), .ip2(n38), .op(n266) );
  nand2_2 U325 ( .ip1(n44), .ip2(mdin[6]), .op(n265) );
  nand2_2 U326 ( .ip1(n267), .ip2(n268), .op(n765) );
  nand2_2 U327 ( .ip1(rd_buf0[5]), .ip2(n38), .op(n268) );
  nand2_2 U328 ( .ip1(n43), .ip2(mdin[5]), .op(n267) );
  nand2_2 U329 ( .ip1(n269), .ip2(n270), .op(n766) );
  nand2_2 U330 ( .ip1(rd_buf0[4]), .ip2(n38), .op(n270) );
  nand2_2 U331 ( .ip1(n43), .ip2(mdin[4]), .op(n269) );
  nand2_2 U332 ( .ip1(n271), .ip2(n272), .op(n767) );
  nand2_2 U333 ( .ip1(rd_buf0[3]), .ip2(n38), .op(n272) );
  nand2_2 U334 ( .ip1(n40), .ip2(mdin[3]), .op(n271) );
  nand2_2 U335 ( .ip1(n273), .ip2(n274), .op(n768) );
  nand2_2 U336 ( .ip1(rd_buf0[2]), .ip2(n38), .op(n274) );
  nand2_2 U337 ( .ip1(n40), .ip2(mdin[2]), .op(n273) );
  nand2_2 U338 ( .ip1(n275), .ip2(n276), .op(n769) );
  nand2_2 U339 ( .ip1(rd_buf0[1]), .ip2(n38), .op(n276) );
  nand2_2 U340 ( .ip1(n39), .ip2(mdin[1]), .op(n275) );
  nand2_2 U341 ( .ip1(n277), .ip2(n278), .op(n770) );
  nand2_2 U342 ( .ip1(rd_buf0[0]), .ip2(n38), .op(n278) );
  nand2_2 U344 ( .ip1(n39), .ip2(mdin[0]), .op(n277) );
  nor2_2 U345 ( .ip1(n941), .ip2(madr[0]), .op(n216) );
  nand2_2 U346 ( .ip1(n280), .ip2(n281), .op(n771) );
  nand2_2 U347 ( .ip1(n379), .ip2(dtmp_r[0]), .op(n281) );
  nand2_2 U348 ( .ip1(mdout[0]), .ip2(n146), .op(n280) );
  nand2_2 U349 ( .ip1(n283), .ip2(n284), .op(n772) );
  nand2_2 U350 ( .ip1(dtmp_r[1]), .ip2(n379), .op(n284) );
  nand2_2 U351 ( .ip1(mdout[1]), .ip2(n146), .op(n283) );
  nand2_2 U352 ( .ip1(n285), .ip2(n286), .op(n773) );
  nand2_2 U353 ( .ip1(dtmp_r[2]), .ip2(n376), .op(n286) );
  nand2_2 U354 ( .ip1(mdout[2]), .ip2(n146), .op(n285) );
  nand2_2 U355 ( .ip1(n287), .ip2(n288), .op(n774) );
  nand2_2 U356 ( .ip1(dtmp_r[3]), .ip2(n376), .op(n288) );
  nand2_2 U357 ( .ip1(mdout[3]), .ip2(n146), .op(n287) );
  nand2_2 U358 ( .ip1(n289), .ip2(n290), .op(n775) );
  nand2_2 U359 ( .ip1(dtmp_r[4]), .ip2(n375), .op(n290) );
  nand2_2 U360 ( .ip1(mdout[4]), .ip2(n146), .op(n289) );
  nand2_2 U361 ( .ip1(n291), .ip2(n292), .op(n776) );
  nand2_2 U362 ( .ip1(dtmp_r[5]), .ip2(n375), .op(n292) );
  nand2_2 U363 ( .ip1(mdout[5]), .ip2(n146), .op(n291) );
  nand2_2 U364 ( .ip1(n293), .ip2(n294), .op(n777) );
  nand2_2 U365 ( .ip1(dtmp_r[6]), .ip2(n372), .op(n294) );
  nand2_2 U366 ( .ip1(mdout[6]), .ip2(n146), .op(n293) );
  nand2_2 U367 ( .ip1(n295), .ip2(n296), .op(n778) );
  nand2_2 U368 ( .ip1(dtmp_r[7]), .ip2(n372), .op(n296) );
  nand2_2 U369 ( .ip1(mdout[7]), .ip2(n146), .op(n295) );
  nand2_2 U370 ( .ip1(n297), .ip2(n298), .op(n779) );
  nand2_2 U371 ( .ip1(dtmp_r[8]), .ip2(n371), .op(n298) );
  nand2_2 U372 ( .ip1(mdout[8]), .ip2(n146), .op(n297) );
  nand2_2 U373 ( .ip1(n299), .ip2(n300), .op(n780) );
  nand2_2 U374 ( .ip1(dtmp_r[9]), .ip2(n371), .op(n300) );
  nand2_2 U375 ( .ip1(mdout[9]), .ip2(n146), .op(n299) );
  nand2_2 U376 ( .ip1(n301), .ip2(n302), .op(n781) );
  nand2_2 U377 ( .ip1(dtmp_r[10]), .ip2(n368), .op(n302) );
  nand2_2 U378 ( .ip1(mdout[10]), .ip2(n146), .op(n301) );
  nand2_2 U379 ( .ip1(n303), .ip2(n304), .op(n782) );
  nand2_2 U380 ( .ip1(dtmp_r[11]), .ip2(n368), .op(n304) );
  nand2_2 U381 ( .ip1(mdout[11]), .ip2(n146), .op(n303) );
  nand2_2 U382 ( .ip1(n305), .ip2(n306), .op(n783) );
  nand2_2 U383 ( .ip1(dtmp_r[12]), .ip2(n367), .op(n306) );
  nand2_2 U384 ( .ip1(mdout[12]), .ip2(n150), .op(n305) );
  nand2_2 U385 ( .ip1(n307), .ip2(n308), .op(n784) );
  nand2_2 U386 ( .ip1(dtmp_r[13]), .ip2(n367), .op(n308) );
  nand2_2 U387 ( .ip1(mdout[13]), .ip2(n150), .op(n307) );
  nand2_2 U388 ( .ip1(n309), .ip2(n310), .op(n785) );
  nand2_2 U389 ( .ip1(dtmp_r[14]), .ip2(n364), .op(n310) );
  nand2_2 U390 ( .ip1(mdout[14]), .ip2(n150), .op(n309) );
  nand2_2 U391 ( .ip1(n311), .ip2(n312), .op(n786) );
  nand2_2 U392 ( .ip1(dtmp_r[15]), .ip2(n364), .op(n312) );
  nand2_2 U393 ( .ip1(mdout[15]), .ip2(n150), .op(n311) );
  nand2_2 U394 ( .ip1(n313), .ip2(n314), .op(n787) );
  nand2_2 U395 ( .ip1(dtmp_r[16]), .ip2(n363), .op(n314) );
  nand2_2 U396 ( .ip1(mdout[16]), .ip2(n150), .op(n313) );
  nand2_2 U397 ( .ip1(n315), .ip2(n316), .op(n788) );
  nand2_2 U398 ( .ip1(dtmp_r[17]), .ip2(n363), .op(n316) );
  nand2_2 U399 ( .ip1(mdout[17]), .ip2(n150), .op(n315) );
  nand2_2 U400 ( .ip1(n317), .ip2(n318), .op(n789) );
  nand2_2 U401 ( .ip1(dtmp_r[18]), .ip2(n360), .op(n318) );
  nand2_2 U402 ( .ip1(mdout[18]), .ip2(n150), .op(n317) );
  nand2_2 U403 ( .ip1(n319), .ip2(n320), .op(n790) );
  nand2_2 U404 ( .ip1(dtmp_r[19]), .ip2(n360), .op(n320) );
  nand2_2 U405 ( .ip1(mdout[19]), .ip2(n150), .op(n319) );
  nand2_2 U406 ( .ip1(n321), .ip2(n322), .op(n791) );
  nand2_2 U407 ( .ip1(dtmp_r[20]), .ip2(n357), .op(n322) );
  nand2_2 U408 ( .ip1(mdout[20]), .ip2(n150), .op(n321) );
  nand2_2 U409 ( .ip1(n323), .ip2(n324), .op(n792) );
  nand2_2 U410 ( .ip1(dtmp_r[21]), .ip2(n357), .op(n324) );
  nand2_2 U411 ( .ip1(mdout[21]), .ip2(n150), .op(n323) );
  nand2_2 U412 ( .ip1(n325), .ip2(n326), .op(n793) );
  nand2_2 U413 ( .ip1(dtmp_r[22]), .ip2(n354), .op(n326) );
  nand2_2 U414 ( .ip1(mdout[22]), .ip2(n150), .op(n325) );
  nand2_2 U415 ( .ip1(n327), .ip2(n328), .op(n794) );
  nand2_2 U416 ( .ip1(dtmp_r[23]), .ip2(n354), .op(n328) );
  nand2_2 U417 ( .ip1(mdout[23]), .ip2(n150), .op(n327) );
  nand2_2 U418 ( .ip1(n329), .ip2(n330), .op(n795) );
  nand2_2 U419 ( .ip1(dtmp_r[24]), .ip2(n347), .op(n330) );
  nand2_2 U420 ( .ip1(mdout[24]), .ip2(n150), .op(n329) );
  nand2_2 U421 ( .ip1(n331), .ip2(n332), .op(n796) );
  nand2_2 U422 ( .ip1(dtmp_r[25]), .ip2(n347), .op(n332) );
  nand2_2 U423 ( .ip1(mdout[25]), .ip2(n146), .op(n331) );
  nand2_2 U424 ( .ip1(n333), .ip2(n334), .op(n797) );
  nand2_2 U425 ( .ip1(dtmp_r[26]), .ip2(n282), .op(n334) );
  nand2_2 U426 ( .ip1(mdout[26]), .ip2(n150), .op(n333) );
  nand2_2 U427 ( .ip1(n335), .ip2(n336), .op(n798) );
  nand2_2 U428 ( .ip1(dtmp_r[27]), .ip2(n282), .op(n336) );
  nand2_2 U429 ( .ip1(mdout[27]), .ip2(n146), .op(n335) );
  nand2_2 U430 ( .ip1(n337), .ip2(n338), .op(n799) );
  nand2_2 U431 ( .ip1(dtmp_r[28]), .ip2(n279), .op(n338) );
  nand2_2 U432 ( .ip1(mdout[28]), .ip2(n150), .op(n337) );
  nand2_2 U433 ( .ip1(n339), .ip2(n340), .op(n800) );
  nand2_2 U434 ( .ip1(dtmp_r[29]), .ip2(n279), .op(n340) );
  nand2_2 U435 ( .ip1(mdout[29]), .ip2(n146), .op(n339) );
  nand2_2 U436 ( .ip1(n341), .ip2(n342), .op(n801) );
  nand2_2 U437 ( .ip1(dtmp_r[30]), .ip2(n215), .op(n342) );
  nand2_2 U438 ( .ip1(mdout[30]), .ip2(n150), .op(n341) );
  nand2_2 U439 ( .ip1(n343), .ip2(n344), .op(n802) );
  nand2_2 U440 ( .ip1(dtmp_r[31]), .ip2(n215), .op(n344) );
  nand2_2 U441 ( .ip1(mdout[31]), .ip2(n146), .op(n343) );
  nand2_2 U442 ( .ip1(n345), .ip2(n346), .op(n804) );
  nand3_2 U443 ( .ip1(n898), .ip2(mack_r), .ip3(rst), .op(n346) );
  nand4_2 U444 ( .ip1(send_data_r), .ip2(rst), .ip3(n348), .ip4(n349), .op(
        n345) );
  nand4_2 U445 ( .ip1(n350), .ip2(n351), .ip3(n352), .ip4(n353), .op(n348) );
  nor2_2 U448 ( .ip1(n909), .ip2(n358), .op(n356) );
  nor2_2 U450 ( .ip1(n359), .ip2(n884), .op(n355) );
  nor2_2 U452 ( .ip1(n910), .ip2(n358), .op(n362) );
  nor2_2 U454 ( .ip1(n359), .ip2(n885), .op(n361) );
  nor2_2 U456 ( .ip1(n911), .ip2(n358), .op(n366) );
  nor2_2 U458 ( .ip1(n359), .ip2(n886), .op(n365) );
  nor2_2 U460 ( .ip1(n912), .ip2(n358), .op(n370) );
  nor2_2 U462 ( .ip1(n359), .ip2(n887), .op(n369) );
  nor2_2 U464 ( .ip1(n913), .ip2(n358), .op(n374) );
  nor2_2 U466 ( .ip1(n359), .ip2(n888), .op(n373) );
  nor2_2 U468 ( .ip1(n914), .ip2(n358), .op(n378) );
  nor2_2 U470 ( .ip1(n359), .ip2(n889), .op(n377) );
  nor2_2 U472 ( .ip1(n915), .ip2(n358), .op(n382) );
  nor2_2 U474 ( .ip1(n359), .ip2(n890), .op(n381) );
  nor2_2 U476 ( .ip1(n916), .ip2(n358), .op(n386) );
  nor2_2 U478 ( .ip1(n359), .ip2(n891), .op(n385) );
  nand2_2 U479 ( .ip1(n389), .ip2(n358), .op(n359) );
  nand2_2 U481 ( .ip1(n389), .ip2(n392), .op(n391) );
  nor2_2 U483 ( .ip1(n917), .ip2(n396), .op(n394) );
  nor2_2 U485 ( .ip1(n884), .ip2(n397), .op(n393) );
  nor2_2 U487 ( .ip1(n918), .ip2(n396), .op(n399) );
  nor2_2 U489 ( .ip1(n885), .ip2(n397), .op(n398) );
  nor2_2 U491 ( .ip1(n919), .ip2(n396), .op(n402) );
  nor2_2 U493 ( .ip1(n886), .ip2(n397), .op(n401) );
  nor2_2 U495 ( .ip1(n920), .ip2(n396), .op(n405) );
  nor2_2 U497 ( .ip1(n887), .ip2(n397), .op(n404) );
  nor2_2 U499 ( .ip1(n921), .ip2(n396), .op(n408) );
  nor2_2 U501 ( .ip1(n888), .ip2(n397), .op(n407) );
  nor2_2 U503 ( .ip1(n922), .ip2(n396), .op(n411) );
  nor2_2 U505 ( .ip1(n889), .ip2(n397), .op(n410) );
  nor2_2 U507 ( .ip1(n923), .ip2(n396), .op(n414) );
  nor2_2 U509 ( .ip1(n890), .ip2(n397), .op(n413) );
  nor2_2 U511 ( .ip1(n924), .ip2(n396), .op(n417) );
  nor2_2 U513 ( .ip1(n891), .ip2(n397), .op(n416) );
  nand2_2 U514 ( .ip1(n389), .ip2(n396), .op(n397) );
  nand3_2 U516 ( .ip1(adr_cb[1]), .ip2(n894), .ip3(n389), .op(n419) );
  nor2_2 U518 ( .ip1(n925), .ip2(n424), .op(n422) );
  nor2_2 U520 ( .ip1(n884), .ip2(n425), .op(n421) );
  nor2_2 U522 ( .ip1(n926), .ip2(n424), .op(n427) );
  nor2_2 U524 ( .ip1(n885), .ip2(n425), .op(n426) );
  nor2_2 U526 ( .ip1(n927), .ip2(n424), .op(n430) );
  nor2_2 U528 ( .ip1(n886), .ip2(n425), .op(n429) );
  nor2_2 U530 ( .ip1(n928), .ip2(n424), .op(n433) );
  nor2_2 U532 ( .ip1(n887), .ip2(n425), .op(n432) );
  nor2_2 U534 ( .ip1(n929), .ip2(n424), .op(n436) );
  nor2_2 U536 ( .ip1(n888), .ip2(n425), .op(n435) );
  nor2_2 U538 ( .ip1(n930), .ip2(n424), .op(n439) );
  nor2_2 U540 ( .ip1(n889), .ip2(n425), .op(n438) );
  nor2_2 U542 ( .ip1(n931), .ip2(n424), .op(n442) );
  nor2_2 U544 ( .ip1(n890), .ip2(n425), .op(n441) );
  nor2_2 U546 ( .ip1(n932), .ip2(n424), .op(n445) );
  nor2_2 U548 ( .ip1(n891), .ip2(n425), .op(n444) );
  nand2_2 U549 ( .ip1(n389), .ip2(n424), .op(n425) );
  nand3_2 U551 ( .ip1(adr_cb[0]), .ip2(n895), .ip3(n389), .op(n447) );
  nor2_2 U553 ( .ip1(n933), .ip2(n452), .op(n450) );
  nor2_2 U555 ( .ip1(n884), .ip2(n453), .op(n449) );
  nor2_2 U558 ( .ip1(n934), .ip2(n452), .op(n455) );
  nor2_2 U560 ( .ip1(n885), .ip2(n453), .op(n454) );
  nor2_2 U563 ( .ip1(n935), .ip2(n452), .op(n458) );
  nor2_2 U565 ( .ip1(n886), .ip2(n453), .op(n457) );
  nor2_2 U568 ( .ip1(n936), .ip2(n452), .op(n461) );
  nor2_2 U570 ( .ip1(n887), .ip2(n453), .op(n460) );
  nor2_2 U573 ( .ip1(n937), .ip2(n452), .op(n464) );
  nor2_2 U575 ( .ip1(n888), .ip2(n453), .op(n463) );
  nor2_2 U578 ( .ip1(n938), .ip2(n452), .op(n467) );
  nor2_2 U580 ( .ip1(n889), .ip2(n453), .op(n466) );
  nor2_2 U583 ( .ip1(n939), .ip2(n452), .op(n470) );
  nor2_2 U585 ( .ip1(n890), .ip2(n453), .op(n469) );
  nor2_2 U588 ( .ip1(n940), .ip2(n452), .op(n473) );
  nor2_2 U590 ( .ip1(n891), .ip2(n453), .op(n472) );
  nand2_2 U591 ( .ip1(n389), .ip2(n452), .op(n453) );
  nand2_2 U593 ( .ip1(n476), .ip2(n389), .op(n475) );
  nor2_2 U594 ( .ip1(n883), .ip2(dtmp_sel_r), .op(n389) );
  nand2_2 U596 ( .ip1(n478), .ip2(n479), .op(n837) );
  nand2_2 U597 ( .ip1(n480), .ip2(n481), .op(n479) );
  nand2_2 U598 ( .ip1(state[7]), .ip2(n482), .op(n478) );
  nand2_2 U599 ( .ip1(n483), .ip2(n484), .op(n838) );
  nand2_2 U600 ( .ip1(n480), .ip2(n485), .op(n484) );
  nand2_2 U601 ( .ip1(n486), .ip2(n487), .op(n485) );
  nand2_2 U602 ( .ip1(n901), .ip2(n975), .op(n487) );
  nand2_2 U603 ( .ip1(state[6]), .ip2(n482), .op(n483) );
  nand2_2 U604 ( .ip1(n490), .ip2(n491), .op(n839) );
  nand4_2 U605 ( .ip1(n480), .ip2(n492), .ip3(tx_dma_en_r), .ip4(n893), .op(
        n491) );
  nand2_2 U606 ( .ip1(state[5]), .ip2(n482), .op(n490) );
  nand2_2 U607 ( .ip1(n494), .ip2(n495), .op(n840) );
  nand3_2 U608 ( .ip1(n480), .ip2(n496), .ip3(wr_last), .op(n495) );
  nand2_2 U609 ( .ip1(state[4]), .ip2(n482), .op(n494) );
  nand2_2 U610 ( .ip1(n497), .ip2(n498), .op(n841) );
  nand2_2 U611 ( .ip1(n480), .ip2(n499), .op(n498) );
  nand2_2 U612 ( .ip1(state[3]), .ip2(n482), .op(n497) );
  nand2_2 U613 ( .ip1(n500), .ip2(n501), .op(n842) );
  nand2_2 U614 ( .ip1(n897), .ip2(n480), .op(n501) );
  nand2_2 U616 ( .ip1(state[2]), .ip2(n482), .op(n500) );
  nand4_2 U617 ( .ip1(rst), .ip2(n505), .ip3(n506), .ip4(n507), .op(n843) );
  nand2_2 U618 ( .ip1(sizd_c[13]), .ip2(n1), .op(n507) );
  nand2_2 U619 ( .ip1(N108), .ip2(n509), .op(n506) );
  nand2_2 U620 ( .ip1(size[13]), .ip2(n876), .op(n505) );
  nand4_2 U621 ( .ip1(rst), .ip2(n511), .ip3(n512), .ip4(n513), .op(n844) );
  nand2_2 U622 ( .ip1(sizd_c[12]), .ip2(n1), .op(n513) );
  nand2_2 U623 ( .ip1(N107), .ip2(n509), .op(n512) );
  nand2_2 U624 ( .ip1(size[12]), .ip2(n876), .op(n511) );
  nand4_2 U625 ( .ip1(rst), .ip2(n514), .ip3(n515), .ip4(n516), .op(n845) );
  nand2_2 U626 ( .ip1(sizd_c[11]), .ip2(n1), .op(n516) );
  nand2_2 U627 ( .ip1(N106), .ip2(n509), .op(n515) );
  nand2_2 U628 ( .ip1(size[11]), .ip2(n876), .op(n514) );
  nand4_2 U629 ( .ip1(rst), .ip2(n517), .ip3(n518), .ip4(n519), .op(n846) );
  nand2_2 U630 ( .ip1(sizd_c[10]), .ip2(n1), .op(n519) );
  nand2_2 U631 ( .ip1(N105), .ip2(n509), .op(n518) );
  nand2_2 U632 ( .ip1(size[10]), .ip2(n876), .op(n517) );
  nand4_2 U633 ( .ip1(rst), .ip2(n520), .ip3(n521), .ip4(n522), .op(n847) );
  nand2_2 U634 ( .ip1(sizd_c[9]), .ip2(n1), .op(n522) );
  nand2_2 U635 ( .ip1(N104), .ip2(n509), .op(n521) );
  nand2_2 U636 ( .ip1(size[9]), .ip2(n876), .op(n520) );
  nand4_2 U637 ( .ip1(rst), .ip2(n523), .ip3(n524), .ip4(n525), .op(n848) );
  nand2_2 U638 ( .ip1(sizd_c[8]), .ip2(n1), .op(n525) );
  nand2_2 U639 ( .ip1(N103), .ip2(n509), .op(n524) );
  nand2_2 U640 ( .ip1(size[8]), .ip2(n876), .op(n523) );
  nand4_2 U641 ( .ip1(rst), .ip2(n526), .ip3(n527), .ip4(n528), .op(n849) );
  nand2_2 U642 ( .ip1(sizd_c[7]), .ip2(n1), .op(n528) );
  nand2_2 U643 ( .ip1(N102), .ip2(n509), .op(n527) );
  nand2_2 U644 ( .ip1(size[7]), .ip2(n876), .op(n526) );
  nand4_2 U645 ( .ip1(rst), .ip2(n529), .ip3(n530), .ip4(n531), .op(n850) );
  nand2_2 U646 ( .ip1(sizd_c[6]), .ip2(n1), .op(n531) );
  nand2_2 U647 ( .ip1(N101), .ip2(n509), .op(n530) );
  nand2_2 U648 ( .ip1(size[6]), .ip2(n876), .op(n529) );
  nand4_2 U649 ( .ip1(rst), .ip2(n532), .ip3(n533), .ip4(n534), .op(n851) );
  nand2_2 U650 ( .ip1(sizd_c[5]), .ip2(n1), .op(n534) );
  nand2_2 U651 ( .ip1(N100), .ip2(n509), .op(n533) );
  nand2_2 U652 ( .ip1(size[5]), .ip2(n876), .op(n532) );
  nand4_2 U653 ( .ip1(rst), .ip2(n535), .ip3(n536), .ip4(n537), .op(n852) );
  nand2_2 U654 ( .ip1(sizd_c[4]), .ip2(n1), .op(n537) );
  nand2_2 U655 ( .ip1(N99), .ip2(n509), .op(n536) );
  nand2_2 U656 ( .ip1(size[4]), .ip2(n876), .op(n535) );
  nand4_2 U657 ( .ip1(rst), .ip2(n538), .ip3(n539), .ip4(n540), .op(n853) );
  nand2_2 U658 ( .ip1(sizd_c[3]), .ip2(n1), .op(n540) );
  nand2_2 U659 ( .ip1(N98), .ip2(n509), .op(n539) );
  nand2_2 U660 ( .ip1(size[3]), .ip2(n876), .op(n538) );
  nand4_2 U661 ( .ip1(rst), .ip2(n541), .ip3(n542), .ip4(n543), .op(n854) );
  nand2_2 U662 ( .ip1(sizd_c[2]), .ip2(n1), .op(n543) );
  nand2_2 U663 ( .ip1(N97), .ip2(n509), .op(n542) );
  nand2_2 U664 ( .ip1(size[2]), .ip2(n876), .op(n541) );
  nand4_2 U665 ( .ip1(rst), .ip2(n544), .ip3(n545), .ip4(n546), .op(n855) );
  nand2_2 U666 ( .ip1(sizd_c[1]), .ip2(n1), .op(n546) );
  nand2_2 U667 ( .ip1(N96), .ip2(n509), .op(n545) );
  nand2_2 U668 ( .ip1(size[1]), .ip2(n876), .op(n544) );
  nand4_2 U669 ( .ip1(rst), .ip2(n547), .ip3(n548), .ip4(n549), .op(n856) );
  nand2_2 U670 ( .ip1(sizd_c[0]), .ip2(n1), .op(n549) );
  nand2_2 U672 ( .ip1(N95), .ip2(n509), .op(n548) );
  nand2_2 U674 ( .ip1(n552), .ip2(n553), .op(n551) );
  nand2_2 U675 ( .ip1(n898), .ip2(mack_r), .op(n553) );
  nand2_2 U676 ( .ip1(rd_next), .ip2(n349), .op(n552) );
  nand2_2 U677 ( .ip1(size[0]), .ip2(n876), .op(n547) );
  nor2_2 U679 ( .ip1(tx_dma_en), .ip2(tx_dma_en_r), .op(n550) );
  nand2_2 U680 ( .ip1(n554), .ip2(n555), .op(n857) );
  or3_2 U681 ( .ip1(n504), .ip2(n877), .ip3(n556), .op(n555) );
  nand2_2 U682 ( .ip1(state[1]), .ip2(n482), .op(n554) );
  nand2_2 U683 ( .ip1(n557), .ip2(n558), .op(n482) );
  or4_2 U684 ( .ip1(n896), .ip2(n504), .ip3(n901), .ip4(mwe_d), .op(n558) );
  nand2_2 U685 ( .ip1(n560), .ip2(n561), .op(n858) );
  nand2_2 U686 ( .ip1(n877), .ip2(state[0]), .op(n561) );
  nand2_2 U688 ( .ip1(n562), .ip2(n557), .op(n560) );
  nand3_2 U689 ( .ip1(n563), .ip2(n564), .ip3(rst), .op(n557) );
  nand4_2 U690 ( .ip1(n565), .ip2(n566), .ip3(n567), .ip4(n568), .op(n564) );
  or2_2 U694 ( .ip1(n556), .ip2(rx_dma_en_r), .op(n566) );
  or2_2 U695 ( .ip1(n903), .ip2(rx_data_done_r2), .op(n565) );
  nand3_2 U696 ( .ip1(n567), .ip2(n556), .ip3(abort), .op(n563) );
  nand2_2 U697 ( .ip1(n492), .ip2(n573), .op(n556) );
  nand3_2 U698 ( .ip1(n878), .ip2(n893), .ip3(tx_dma_en_r), .op(n573) );
  and2_2 U700 ( .ip1(n575), .ip2(n576), .op(n567) );
  nand4_2 U701 ( .ip1(n901), .ip2(n577), .ip3(n878), .ip4(n975), .op(n576) );
  nand2_2 U703 ( .ip1(n392), .ip2(rd_next), .op(n577) );
  or2_2 U704 ( .ip1(n578), .ip2(mack_r), .op(n575) );
  nand4_2 U705 ( .ip1(n579), .ip2(n578), .ip3(n580), .ip4(n581), .op(n562) );
  nor2_2 U707 ( .ip1(wr_last), .ip2(n900), .op(n582) );
  nand2_2 U709 ( .ip1(abort), .ip2(n584), .op(n580) );
  nand4_2 U710 ( .ip1(n571), .ip2(n900), .ip3(n583), .ip4(n903), .op(n584) );
  nand4_2 U711 ( .ip1(state[7]), .ip2(n585), .ip3(n977), .ip4(n976), .op(n583)
         );
  nor2_2 U712 ( .ip1(n588), .ip2(n897), .op(n571) );
  or2_2 U714 ( .ip1(n902), .ip2(n492), .op(n579) );
  nor4_2 U715 ( .ip1(n902), .ip2(n591), .ip3(state[2]), .ip4(state[3]), .op(
        n492) );
  nand2_2 U716 ( .ip1(n592), .ip2(n593), .op(n859) );
  nand2_2 U717 ( .ip1(sizu_c[10]), .ip2(n4), .op(n593) );
  nand2_2 U718 ( .ip1(N142), .ip2(n3), .op(n592) );
  nand2_2 U719 ( .ip1(n596), .ip2(n597), .op(n860) );
  nand2_2 U720 ( .ip1(sizu_c[9]), .ip2(n4), .op(n597) );
  nand2_2 U721 ( .ip1(N141), .ip2(n3), .op(n596) );
  nand2_2 U722 ( .ip1(n598), .ip2(n599), .op(n861) );
  nand2_2 U723 ( .ip1(sizu_c[8]), .ip2(n4), .op(n599) );
  nand2_2 U724 ( .ip1(N140), .ip2(n3), .op(n598) );
  nand2_2 U725 ( .ip1(n600), .ip2(n601), .op(n862) );
  nand2_2 U726 ( .ip1(sizu_c[7]), .ip2(n4), .op(n601) );
  nand2_2 U727 ( .ip1(N139), .ip2(n3), .op(n600) );
  nand2_2 U728 ( .ip1(n602), .ip2(n603), .op(n863) );
  nand2_2 U729 ( .ip1(sizu_c[6]), .ip2(n4), .op(n603) );
  nand2_2 U730 ( .ip1(N138), .ip2(n3), .op(n602) );
  nand2_2 U731 ( .ip1(n604), .ip2(n605), .op(n864) );
  nand2_2 U732 ( .ip1(sizu_c[5]), .ip2(n4), .op(n605) );
  nand2_2 U733 ( .ip1(N137), .ip2(n3), .op(n604) );
  nand2_2 U734 ( .ip1(n606), .ip2(n607), .op(n865) );
  nand2_2 U735 ( .ip1(sizu_c[4]), .ip2(n4), .op(n607) );
  nand2_2 U736 ( .ip1(N136), .ip2(n3), .op(n606) );
  nand2_2 U737 ( .ip1(n608), .ip2(n609), .op(n866) );
  nand2_2 U738 ( .ip1(sizu_c[3]), .ip2(n4), .op(n609) );
  nand2_2 U739 ( .ip1(N135), .ip2(n3), .op(n608) );
  nand2_2 U740 ( .ip1(n610), .ip2(n611), .op(n867) );
  nand2_2 U741 ( .ip1(sizu_c[2]), .ip2(n4), .op(n611) );
  nand2_2 U742 ( .ip1(N134), .ip2(n3), .op(n610) );
  nand2_2 U743 ( .ip1(n612), .ip2(n613), .op(n868) );
  nand2_2 U744 ( .ip1(sizu_c[1]), .ip2(n4), .op(n613) );
  nand2_2 U745 ( .ip1(N133), .ip2(n3), .op(n612) );
  nand2_2 U746 ( .ip1(n614), .ip2(n615), .op(n869) );
  nand2_2 U747 ( .ip1(sizu_c[0]), .ip2(n4), .op(n615) );
  nand2_2 U749 ( .ip1(N132), .ip2(n3), .op(n614) );
  nand3_2 U751 ( .ip1(n900), .ip2(n903), .ip3(n578), .op(mwe_d) );
  nand4_2 U752 ( .ip1(state[4]), .ip2(n585), .ip3(n976), .ip4(n978), .op(n578)
         );
  nand2_2 U755 ( .ip1(n617), .ip2(n618), .op(N84) );
  nand2_2 U756 ( .ip1(adr[2]), .ip2(n619), .op(n618) );
  nand2_2 U757 ( .ip1(n620), .ip2(n880), .op(n617) );
  xor2_2 U759 ( .ip1(adr_cb[2]), .ip2(n621), .op(n134) );
  nand2_2 U760 ( .ip1(adr_cb[1]), .ip2(n882), .op(n621) );
  nand2_2 U761 ( .ip1(n623), .ip2(n624), .op(N83) );
  nand2_2 U762 ( .ip1(adr[1]), .ip2(n619), .op(n624) );
  nand2_2 U763 ( .ip1(n620), .ip2(n881), .op(n623) );
  xor2_2 U765 ( .ip1(n882), .ip2(n895), .op(n136) );
  nand2_2 U767 ( .ip1(n626), .ip2(n627), .op(N82) );
  nand2_2 U768 ( .ip1(adr[0]), .ip2(n619), .op(n627) );
  nor2_2 U769 ( .ip1(n504), .ip2(n628), .op(n619) );
  nand2_2 U770 ( .ip1(n620), .ip2(n879), .op(n626) );
  nand2_2 U772 ( .ip1(n625), .ip2(n629), .op(n135) );
  nand3_2 U773 ( .ip1(n995), .ip2(n883), .ip3(n894), .op(n629) );
  nand2_2 U774 ( .ip1(adr_cb[0]), .ip2(n630), .op(n625) );
  nand2_2 U775 ( .ip1(n883), .ip2(n995), .op(n630) );
  nor2_2 U778 ( .ip1(n892), .ip2(n504), .op(n620) );
  nand2_2 U780 ( .ip1(n632), .ip2(n633), .op(N44) );
  nand2_2 U781 ( .ip1(n634), .ip2(adrw_next[14]), .op(n633) );
  nand2_2 U782 ( .ip1(n635), .ip2(n636), .op(adrw_next[14]) );
  nand2_2 U783 ( .ip1(madr[14]), .ip2(n637), .op(n636) );
  nand2_2 U784 ( .ip1(N78), .ip2(n908), .op(n635) );
  nand2_2 U785 ( .ip1(adr[16]), .ip2(n892), .op(n632) );
  nand2_2 U786 ( .ip1(n639), .ip2(n640), .op(N43) );
  nand2_2 U787 ( .ip1(n634), .ip2(adrw_next[13]), .op(n640) );
  nand2_2 U788 ( .ip1(n641), .ip2(n642), .op(adrw_next[13]) );
  nand2_2 U789 ( .ip1(madr[13]), .ip2(n637), .op(n642) );
  nand2_2 U790 ( .ip1(N77), .ip2(n908), .op(n641) );
  nand2_2 U791 ( .ip1(adr[15]), .ip2(n892), .op(n639) );
  nand2_2 U792 ( .ip1(n643), .ip2(n644), .op(N42) );
  nand2_2 U793 ( .ip1(n634), .ip2(adrw_next[12]), .op(n644) );
  nand2_2 U794 ( .ip1(n645), .ip2(n646), .op(adrw_next[12]) );
  nand2_2 U795 ( .ip1(madr[12]), .ip2(n637), .op(n646) );
  nand2_2 U796 ( .ip1(N76), .ip2(n908), .op(n645) );
  nand2_2 U797 ( .ip1(adr[14]), .ip2(n892), .op(n643) );
  nand2_2 U798 ( .ip1(n647), .ip2(n648), .op(N41) );
  nand2_2 U799 ( .ip1(n634), .ip2(adrw_next[11]), .op(n648) );
  nand2_2 U800 ( .ip1(n649), .ip2(n650), .op(adrw_next[11]) );
  nand2_2 U801 ( .ip1(madr[11]), .ip2(n637), .op(n650) );
  nand2_2 U802 ( .ip1(N75), .ip2(n908), .op(n649) );
  nand2_2 U803 ( .ip1(adr[13]), .ip2(n892), .op(n647) );
  nand2_2 U804 ( .ip1(n651), .ip2(n652), .op(N40) );
  nand2_2 U805 ( .ip1(n634), .ip2(adrw_next[10]), .op(n652) );
  nand2_2 U806 ( .ip1(n653), .ip2(n654), .op(adrw_next[10]) );
  nand2_2 U807 ( .ip1(madr[10]), .ip2(n637), .op(n654) );
  nand2_2 U808 ( .ip1(N74), .ip2(n908), .op(n653) );
  nand2_2 U809 ( .ip1(adr[12]), .ip2(n892), .op(n651) );
  nand2_2 U810 ( .ip1(n655), .ip2(n656), .op(N39) );
  nand2_2 U811 ( .ip1(n634), .ip2(adrw_next[9]), .op(n656) );
  nand2_2 U812 ( .ip1(n657), .ip2(n658), .op(adrw_next[9]) );
  nand2_2 U813 ( .ip1(madr[9]), .ip2(n637), .op(n658) );
  nand2_2 U814 ( .ip1(N73), .ip2(n908), .op(n657) );
  nand2_2 U815 ( .ip1(adr[11]), .ip2(n892), .op(n655) );
  nand2_2 U816 ( .ip1(n659), .ip2(n660), .op(N38) );
  nand2_2 U817 ( .ip1(n634), .ip2(adrw_next[8]), .op(n660) );
  nand2_2 U818 ( .ip1(n661), .ip2(n662), .op(adrw_next[8]) );
  nand2_2 U819 ( .ip1(madr[8]), .ip2(n637), .op(n662) );
  nand2_2 U820 ( .ip1(N72), .ip2(n908), .op(n661) );
  nand2_2 U821 ( .ip1(adr[10]), .ip2(n892), .op(n659) );
  nand2_2 U822 ( .ip1(n663), .ip2(n664), .op(N37) );
  nand2_2 U823 ( .ip1(n634), .ip2(adrw_next[7]), .op(n664) );
  nand2_2 U824 ( .ip1(n665), .ip2(n666), .op(adrw_next[7]) );
  nand2_2 U825 ( .ip1(madr[7]), .ip2(n637), .op(n666) );
  nand2_2 U826 ( .ip1(N71), .ip2(n908), .op(n665) );
  nand2_2 U827 ( .ip1(adr[9]), .ip2(n892), .op(n663) );
  nand2_2 U828 ( .ip1(n667), .ip2(n668), .op(N36) );
  nand2_2 U829 ( .ip1(n634), .ip2(adrw_next[6]), .op(n668) );
  nand2_2 U830 ( .ip1(n669), .ip2(n670), .op(adrw_next[6]) );
  nand2_2 U831 ( .ip1(madr[6]), .ip2(n637), .op(n670) );
  nand2_2 U832 ( .ip1(N70), .ip2(n908), .op(n669) );
  nand2_2 U833 ( .ip1(adr[8]), .ip2(n892), .op(n667) );
  nand2_2 U834 ( .ip1(n671), .ip2(n672), .op(N35) );
  nand2_2 U835 ( .ip1(n634), .ip2(adrw_next[5]), .op(n672) );
  nand2_2 U836 ( .ip1(n673), .ip2(n674), .op(adrw_next[5]) );
  nand2_2 U837 ( .ip1(madr[5]), .ip2(n637), .op(n674) );
  nand2_2 U838 ( .ip1(N69), .ip2(n908), .op(n673) );
  nand2_2 U839 ( .ip1(adr[7]), .ip2(n892), .op(n671) );
  nand2_2 U840 ( .ip1(n675), .ip2(n676), .op(N34) );
  nand2_2 U841 ( .ip1(n634), .ip2(adrw_next[4]), .op(n676) );
  nand2_2 U842 ( .ip1(n677), .ip2(n678), .op(adrw_next[4]) );
  nand2_2 U843 ( .ip1(madr[4]), .ip2(n637), .op(n678) );
  nand2_2 U844 ( .ip1(N68), .ip2(n908), .op(n677) );
  nand2_2 U845 ( .ip1(adr[6]), .ip2(n892), .op(n675) );
  nand2_2 U846 ( .ip1(n679), .ip2(n680), .op(N33) );
  nand2_2 U847 ( .ip1(n634), .ip2(adrw_next[3]), .op(n680) );
  nand2_2 U848 ( .ip1(n681), .ip2(n682), .op(adrw_next[3]) );
  nand2_2 U849 ( .ip1(madr[3]), .ip2(n637), .op(n682) );
  nand2_2 U850 ( .ip1(N67), .ip2(n908), .op(n681) );
  nand2_2 U851 ( .ip1(adr[5]), .ip2(n892), .op(n679) );
  nand2_2 U852 ( .ip1(n683), .ip2(n684), .op(N32) );
  nand2_2 U853 ( .ip1(n634), .ip2(adrw_next[2]), .op(n684) );
  nand2_2 U854 ( .ip1(n685), .ip2(n686), .op(adrw_next[2]) );
  nand2_2 U855 ( .ip1(madr[2]), .ip2(n637), .op(n686) );
  nand2_2 U856 ( .ip1(N66), .ip2(n908), .op(n685) );
  nand2_2 U857 ( .ip1(adr[4]), .ip2(n892), .op(n683) );
  nand2_2 U858 ( .ip1(n687), .ip2(n688), .op(N31) );
  nand2_2 U859 ( .ip1(n634), .ip2(adrw_next[1]), .op(n688) );
  nand2_2 U860 ( .ip1(n689), .ip2(n690), .op(adrw_next[1]) );
  nand2_2 U861 ( .ip1(madr[1]), .ip2(n637), .op(n690) );
  nand2_2 U862 ( .ip1(N65), .ip2(n908), .op(n689) );
  nand2_2 U863 ( .ip1(adr[3]), .ip2(n892), .op(n687) );
  nand2_2 U864 ( .ip1(n691), .ip2(n692), .op(N30) );
  nand2_2 U865 ( .ip1(n634), .ip2(adrw_next[0]), .op(n692) );
  nand2_2 U866 ( .ip1(n693), .ip2(n694), .op(adrw_next[0]) );
  nand2_2 U867 ( .ip1(madr[0]), .ip2(n637), .op(n694) );
  nand2_2 U868 ( .ip1(N64), .ip2(n908), .op(n693) );
  nand2_2 U873 ( .ip1(dma_en), .ip2(N60), .op(n695) );
  nand2_2 U874 ( .ip1(adr[2]), .ip2(n892), .op(n691) );
  nor2_2 U876 ( .ip1(rx_dma_en_r), .ip2(tx_dma_en_r), .op(n628) );
  and2_2 U877 ( .ip1(mack), .ip2(mreq), .op(N27) );
  nand4_2 U880 ( .ip1(state[1]), .ip2(n696), .ip3(n899), .ip4(n905), .op(n589)
         );
  or2_2 U882 ( .ip1(n481), .ip2(n898), .op(n588) );
  nand4_2 U884 ( .ip1(n899), .ip2(n906), .ip3(state[5]), .ip4(n700), .op(n486)
         );
  nor4_2 U885 ( .ip1(state[7]), .ip2(state[6]), .ip3(state[4]), .ip4(state[3]), 
        .op(n700) );
  and4_2 U887 ( .ip1(state[6]), .ip2(n585), .ip3(n977), .ip4(n978), .op(n481)
         );
  nor4_2 U890 ( .ip1(n701), .ip2(state[1]), .ip3(state[3]), .ip4(state[5]), 
        .op(n585) );
  nor2_2 U892 ( .ip1(adr_cb[0]), .ip2(adr_cb[1]), .op(n476) );
  nand3_2 U893 ( .ip1(n499), .ip2(n878), .ip3(rx_data_done_r2), .op(n702) );
  nor4_2 U895 ( .ip1(n907), .ip2(n591), .ip3(state[0]), .ip4(state[3]), .op(
        n499) );
  nand2_2 U899 ( .ip1(n907), .ip2(n902), .op(n701) );
  nand2_2 U902 ( .ip1(n696), .ip2(n906), .op(n591) );
  nor4_2 U904 ( .ip1(state[4]), .ip2(state[5]), .ip3(state[6]), .ip4(state[7]), 
        .op(n696) );
  nor2_2 U905 ( .ip1(word_done_r), .ip2(n146), .op(N201) );
  nand2_2 U907 ( .ip1(n904), .ip2(n705), .op(N200) );
  nand2_2 U908 ( .ip1(n392), .ip2(rx_data_valid_r), .op(n705) );
  nor2_2 U909 ( .ip1(n895), .ip2(n894), .op(n392) );
  or2_2 U913 ( .ip1(n974), .ip2(rx_data_done_r), .op(N159) );
  nand4_2 U915 ( .ip1(n706), .ip2(n351), .ip3(n352), .ip4(n353), .op(n349) );
  nor4_2 U916 ( .ip1(sizd_c[9]), .ip2(sizd_c[8]), .ip3(sizd_c[7]), .ip4(
        sizd_c[6]), .op(n353) );
  nor4_2 U918 ( .ip1(sizd_c[2]), .ip2(sizd_c[1]), .ip3(sizd_c[13]), .ip4(
        sizd_c[12]), .op(n351) );
  usbf_idma_SSRAM_HADR14_DW01_dec_0 sub_3464_S2 ( .A(sizd_c), .SUM({N108, N107, 
        N106, N105, N104, N103, N102, N101, N100, N99, N98, N97, N96, N95}) );
  usbf_idma_SSRAM_HADR14_DW01_inc_1 add_3434_S2 ( .A(madr), .SUM({N78, N77, 
        N76, N75, N74, N73, N72, N71, N70, N69, N68, N67, N66, N65, N64}) );
  usbf_idma_SSRAM_HADR14_DW01_add_0 add_3427 ( .A(adr[14:0]), .B({1'b0, 
        buf_size}), .CI(1'b0), .SUM({N59, N58, N57, N56, N55, N54, N53, N52, 
        N51, N50, N49, N48, N47, N46, N45}) );
  dp_1 \last_buf_adr_reg[14]  ( .ip(N59), .ck(n406), .q(last_buf_adr[14]) );
  dp_1 \last_buf_adr_reg[13]  ( .ip(N58), .ck(n406), .q(last_buf_adr[13]) );
  dp_1 \last_buf_adr_reg[12]  ( .ip(N57), .ck(n406), .q(last_buf_adr[12]) );
  dp_1 \last_buf_adr_reg[11]  ( .ip(N56), .ck(n406), .q(last_buf_adr[11]) );
  dp_1 \last_buf_adr_reg[10]  ( .ip(N55), .ck(n406), .q(last_buf_adr[10]) );
  dp_1 \last_buf_adr_reg[9]  ( .ip(N54), .ck(n406), .q(last_buf_adr[9]) );
  dp_1 \last_buf_adr_reg[8]  ( .ip(N53), .ck(n406), .q(last_buf_adr[8]) );
  dp_1 \last_buf_adr_reg[7]  ( .ip(N52), .ck(n406), .q(last_buf_adr[7]) );
  dp_1 \last_buf_adr_reg[6]  ( .ip(N51), .ck(n406), .q(last_buf_adr[6]) );
  dp_1 \last_buf_adr_reg[5]  ( .ip(N50), .ck(n406), .q(last_buf_adr[5]) );
  dp_1 \last_buf_adr_reg[4]  ( .ip(N49), .ck(n406), .q(last_buf_adr[4]) );
  dp_1 \last_buf_adr_reg[3]  ( .ip(N48), .ck(n406), .q(last_buf_adr[3]) );
  dp_1 \last_buf_adr_reg[2]  ( .ip(N47), .ck(n409), .q(last_buf_adr[2]) );
  dp_1 idma_done_reg ( .ip(N159), .ck(n446), .q(idma_done) );
  dp_1 mwe_reg ( .ip(mwe_d), .ck(n446), .q(mwe) );
  dp_1 \rx_data_st_r_reg[7]  ( .ip(rx_data_st[7]), .ck(n403), .q(
        rx_data_st_r[7]) );
  dp_1 \rx_data_st_r_reg[6]  ( .ip(rx_data_st[6]), .ck(n403), .q(
        rx_data_st_r[6]) );
  dp_1 \rx_data_st_r_reg[5]  ( .ip(rx_data_st[5]), .ck(n403), .q(
        rx_data_st_r[5]) );
  dp_1 \rx_data_st_r_reg[4]  ( .ip(rx_data_st[4]), .ck(n403), .q(
        rx_data_st_r[4]) );
  dp_1 \rx_data_st_r_reg[3]  ( .ip(rx_data_st[3]), .ck(n403), .q(
        rx_data_st_r[3]) );
  dp_1 \rx_data_st_r_reg[2]  ( .ip(rx_data_st[2]), .ck(n403), .q(
        rx_data_st_r[2]) );
  dp_1 \rx_data_st_r_reg[1]  ( .ip(rx_data_st[1]), .ck(n403), .q(
        rx_data_st_r[1]) );
  dp_1 \rx_data_st_r_reg[0]  ( .ip(rx_data_st[0]), .ck(n403), .q(
        rx_data_st_r[0]) );
  dp_1 \last_buf_adr_reg[0]  ( .ip(N45), .ck(n409), .q(last_buf_adr[0]) );
  dp_1 word_done_r_reg ( .ip(N201), .ck(n412), .q(word_done_r) );
  dp_1 sizd_is_zero_reg ( .ip(n974), .ck(n443), .q(sizd_is_zero) );
  dp_1 \last_buf_adr_reg[1]  ( .ip(N46), .ck(n409), .q(last_buf_adr[1]) );
  dp_1 \rd_buf1_reg[21]  ( .ip(n717), .ck(n451), .q(rd_buf1[21]) );
  dp_1 \rd_buf1_reg[22]  ( .ip(n716), .ck(n451), .q(rd_buf1[22]) );
  dp_1 \rd_buf1_reg[23]  ( .ip(n715), .ck(n451), .q(rd_buf1[23]) );
  dp_1 \rd_buf1_reg[24]  ( .ip(n714), .ck(n451), .q(rd_buf1[24]) );
  dp_1 \rd_buf1_reg[25]  ( .ip(n713), .ck(n451), .q(rd_buf1[25]) );
  dp_1 \rd_buf1_reg[26]  ( .ip(n712), .ck(n451), .q(rd_buf1[26]) );
  dp_1 \rd_buf1_reg[27]  ( .ip(n711), .ck(n451), .q(rd_buf1[27]) );
  dp_1 \rd_buf1_reg[28]  ( .ip(n710), .ck(n451), .q(rd_buf1[28]) );
  dp_1 \rd_buf1_reg[29]  ( .ip(n709), .ck(n451), .q(rd_buf1[29]) );
  dp_1 \rd_buf1_reg[30]  ( .ip(n708), .ck(n451), .q(rd_buf1[30]) );
  dp_1 \rd_buf1_reg[31]  ( .ip(n707), .ck(n451), .q(rd_buf1[31]) );
  dp_1 rx_data_done_r_reg ( .ip(rx_data_done), .ck(n403), .q(rx_data_done_r)
         );
  dp_1 \adr_cb_reg[2]  ( .ip(N84), .ck(n409), .q(adr_cb[2]) );
  dp_1 send_data_r_reg ( .ip(n804), .ck(n446), .q(send_data_r) );
  dp_1 wr_done_reg ( .ip(wr_done_r), .ck(n412), .q(wr_done) );
  dp_1 \dout_r_reg[31]  ( .ip(n802), .ck(n415), .q(mdout[31]) );
  dp_1 \dout_r_reg[30]  ( .ip(n801), .ck(n415), .q(mdout[30]) );
  dp_1 \dout_r_reg[29]  ( .ip(n800), .ck(n415), .q(mdout[29]) );
  dp_1 \dout_r_reg[28]  ( .ip(n799), .ck(n415), .q(mdout[28]) );
  dp_1 \dout_r_reg[27]  ( .ip(n798), .ck(n415), .q(mdout[27]) );
  dp_1 \dout_r_reg[26]  ( .ip(n797), .ck(n415), .q(mdout[26]) );
  dp_1 \dout_r_reg[25]  ( .ip(n796), .ck(n418), .q(mdout[25]) );
  dp_1 \dout_r_reg[24]  ( .ip(n795), .ck(n418), .q(mdout[24]) );
  dp_1 \dout_r_reg[23]  ( .ip(n794), .ck(n418), .q(mdout[23]) );
  dp_1 \dout_r_reg[22]  ( .ip(n793), .ck(n418), .q(mdout[22]) );
  dp_1 \dout_r_reg[21]  ( .ip(n792), .ck(n418), .q(mdout[21]) );
  dp_1 \dout_r_reg[20]  ( .ip(n791), .ck(n418), .q(mdout[20]) );
  dp_1 \dout_r_reg[19]  ( .ip(n790), .ck(n420), .q(mdout[19]) );
  dp_1 \dout_r_reg[18]  ( .ip(n789), .ck(n420), .q(mdout[18]) );
  dp_1 \dout_r_reg[17]  ( .ip(n788), .ck(n420), .q(mdout[17]) );
  dp_1 \dout_r_reg[16]  ( .ip(n787), .ck(n420), .q(mdout[16]) );
  dp_1 \dout_r_reg[15]  ( .ip(n786), .ck(n420), .q(mdout[15]) );
  dp_1 \dout_r_reg[14]  ( .ip(n785), .ck(n420), .q(mdout[14]) );
  dp_1 \dout_r_reg[13]  ( .ip(n784), .ck(n420), .q(mdout[13]) );
  dp_1 \dout_r_reg[12]  ( .ip(n783), .ck(n423), .q(mdout[12]) );
  dp_1 \dout_r_reg[11]  ( .ip(n782), .ck(n423), .q(mdout[11]) );
  dp_1 \dout_r_reg[10]  ( .ip(n781), .ck(n423), .q(mdout[10]) );
  dp_1 \dout_r_reg[9]  ( .ip(n780), .ck(n423), .q(mdout[9]) );
  dp_1 \dout_r_reg[8]  ( .ip(n779), .ck(n423), .q(mdout[8]) );
  dp_1 \dout_r_reg[7]  ( .ip(n778), .ck(n423), .q(mdout[7]) );
  dp_1 \dout_r_reg[6]  ( .ip(n777), .ck(n428), .q(mdout[6]) );
  dp_1 \dout_r_reg[5]  ( .ip(n776), .ck(n428), .q(mdout[5]) );
  dp_1 \dout_r_reg[4]  ( .ip(n775), .ck(n428), .q(mdout[4]) );
  dp_1 \dout_r_reg[3]  ( .ip(n774), .ck(n428), .q(mdout[3]) );
  dp_1 \dout_r_reg[2]  ( .ip(n773), .ck(n428), .q(mdout[2]) );
  dp_1 \dout_r_reg[1]  ( .ip(n772), .ck(n428), .q(mdout[1]) );
  dp_1 \dout_r_reg[0]  ( .ip(n771), .ck(n428), .q(mdout[0]) );
  dp_1 \rd_buf1_reg[0]  ( .ip(n738), .ck(n446), .q(rd_buf1[0]) );
  dp_1 \rd_buf1_reg[1]  ( .ip(n737), .ck(n446), .q(rd_buf1[1]) );
  dp_1 \rd_buf1_reg[2]  ( .ip(n736), .ck(n446), .q(rd_buf1[2]) );
  dp_1 \rd_buf1_reg[3]  ( .ip(n735), .ck(n446), .q(rd_buf1[3]) );
  dp_1 \rd_buf1_reg[4]  ( .ip(n734), .ck(n446), .q(rd_buf1[4]) );
  dp_1 \rd_buf1_reg[5]  ( .ip(n733), .ck(n446), .q(rd_buf1[5]) );
  dp_1 \rd_buf1_reg[6]  ( .ip(n732), .ck(n446), .q(rd_buf1[6]) );
  dp_1 \rd_buf1_reg[7]  ( .ip(n731), .ck(n446), .q(rd_buf1[7]) );
  dp_1 \rd_buf1_reg[8]  ( .ip(n730), .ck(n448), .q(rd_buf1[8]) );
  dp_1 \rd_buf1_reg[9]  ( .ip(n729), .ck(n448), .q(rd_buf1[9]) );
  dp_1 \rd_buf1_reg[10]  ( .ip(n728), .ck(n448), .q(rd_buf1[10]) );
  dp_1 \rd_buf1_reg[11]  ( .ip(n727), .ck(n448), .q(rd_buf1[11]) );
  dp_1 \rd_buf1_reg[12]  ( .ip(n726), .ck(n448), .q(rd_buf1[12]) );
  dp_1 \rd_buf1_reg[13]  ( .ip(n725), .ck(n448), .q(rd_buf1[13]) );
  dp_1 \rd_buf1_reg[14]  ( .ip(n724), .ck(n448), .q(rd_buf1[14]) );
  dp_1 \rd_buf1_reg[15]  ( .ip(n723), .ck(n448), .q(rd_buf1[15]) );
  dp_1 send_zero_length_r_reg ( .ip(send_zero_length), .ck(n406), .q(
        send_zero_length_r) );
  dp_1 \dtmp_r_reg[31]  ( .ip(n805), .ck(n415), .q(dtmp_r[31]) );
  dp_1 \dtmp_r_reg[30]  ( .ip(n806), .ck(n415), .q(dtmp_r[30]) );
  dp_1 \dtmp_r_reg[29]  ( .ip(n807), .ck(n415), .q(dtmp_r[29]) );
  dp_1 \dtmp_r_reg[28]  ( .ip(n808), .ck(n415), .q(dtmp_r[28]) );
  dp_1 \dtmp_r_reg[27]  ( .ip(n809), .ck(n415), .q(dtmp_r[27]) );
  dp_1 \dtmp_r_reg[26]  ( .ip(n810), .ck(n415), .q(dtmp_r[26]) );
  dp_1 \dtmp_r_reg[25]  ( .ip(n811), .ck(n418), .q(dtmp_r[25]) );
  dp_1 \dtmp_r_reg[24]  ( .ip(n812), .ck(n418), .q(dtmp_r[24]) );
  dp_1 \dtmp_r_reg[23]  ( .ip(n813), .ck(n418), .q(dtmp_r[23]) );
  dp_1 \dtmp_r_reg[22]  ( .ip(n814), .ck(n418), .q(dtmp_r[22]) );
  dp_1 \dtmp_r_reg[21]  ( .ip(n815), .ck(n418), .q(dtmp_r[21]) );
  dp_1 \dtmp_r_reg[20]  ( .ip(n816), .ck(n418), .q(dtmp_r[20]) );
  dp_1 \dtmp_r_reg[19]  ( .ip(n817), .ck(n418), .q(dtmp_r[19]) );
  dp_1 \dtmp_r_reg[18]  ( .ip(n818), .ck(n420), .q(dtmp_r[18]) );
  dp_1 \dtmp_r_reg[17]  ( .ip(n819), .ck(n420), .q(dtmp_r[17]) );
  dp_1 \dtmp_r_reg[16]  ( .ip(n820), .ck(n420), .q(dtmp_r[16]) );
  dp_1 \dtmp_r_reg[15]  ( .ip(n821), .ck(n420), .q(dtmp_r[15]) );
  dp_1 \dtmp_r_reg[14]  ( .ip(n822), .ck(n420), .q(dtmp_r[14]) );
  dp_1 \dtmp_r_reg[13]  ( .ip(n823), .ck(n420), .q(dtmp_r[13]) );
  dp_1 \dtmp_r_reg[12]  ( .ip(n824), .ck(n423), .q(dtmp_r[12]) );
  dp_1 \dtmp_r_reg[11]  ( .ip(n825), .ck(n423), .q(dtmp_r[11]) );
  dp_1 \dtmp_r_reg[10]  ( .ip(n826), .ck(n423), .q(dtmp_r[10]) );
  dp_1 \dtmp_r_reg[9]  ( .ip(n827), .ck(n423), .q(dtmp_r[9]) );
  dp_1 \dtmp_r_reg[8]  ( .ip(n828), .ck(n423), .q(dtmp_r[8]) );
  dp_1 \dtmp_r_reg[7]  ( .ip(n829), .ck(n423), .q(dtmp_r[7]) );
  dp_1 \dtmp_r_reg[6]  ( .ip(n830), .ck(n423), .q(dtmp_r[6]) );
  dp_1 \dtmp_r_reg[5]  ( .ip(n831), .ck(n428), .q(dtmp_r[5]) );
  dp_1 \dtmp_r_reg[4]  ( .ip(n832), .ck(n428), .q(dtmp_r[4]) );
  dp_1 \dtmp_r_reg[3]  ( .ip(n833), .ck(n428), .q(dtmp_r[3]) );
  dp_1 \dtmp_r_reg[2]  ( .ip(n834), .ck(n428), .q(dtmp_r[2]) );
  dp_1 \dtmp_r_reg[1]  ( .ip(n835), .ck(n428), .q(dtmp_r[1]) );
  dp_1 \rd_buf0_reg[0]  ( .ip(n770), .ck(n434), .q(rd_buf0[0]) );
  dp_1 \rd_buf0_reg[1]  ( .ip(n769), .ck(n434), .q(rd_buf0[1]) );
  dp_1 \rd_buf0_reg[2]  ( .ip(n768), .ck(n434), .q(rd_buf0[2]) );
  dp_1 \rd_buf0_reg[3]  ( .ip(n767), .ck(n434), .q(rd_buf0[3]) );
  dp_1 \rd_buf0_reg[4]  ( .ip(n766), .ck(n434), .q(rd_buf0[4]) );
  dp_1 \rd_buf0_reg[5]  ( .ip(n765), .ck(n434), .q(rd_buf0[5]) );
  dp_1 \rd_buf0_reg[6]  ( .ip(n764), .ck(n434), .q(rd_buf0[6]) );
  dp_1 \rd_buf0_reg[7]  ( .ip(n763), .ck(n434), .q(rd_buf0[7]) );
  dp_1 \rd_buf0_reg[8]  ( .ip(n762), .ck(n434), .q(rd_buf0[8]) );
  dp_1 \rd_buf0_reg[9]  ( .ip(n761), .ck(n434), .q(rd_buf0[9]) );
  dp_1 \rd_buf0_reg[10]  ( .ip(n760), .ck(n437), .q(rd_buf0[10]) );
  dp_1 \rd_buf0_reg[11]  ( .ip(n759), .ck(n437), .q(rd_buf0[11]) );
  dp_1 \rd_buf0_reg[12]  ( .ip(n758), .ck(n437), .q(rd_buf0[12]) );
  dp_1 \rd_buf0_reg[13]  ( .ip(n757), .ck(n437), .q(rd_buf0[13]) );
  dp_1 \rd_buf0_reg[14]  ( .ip(n756), .ck(n437), .q(rd_buf0[14]) );
  dp_1 \rd_buf0_reg[15]  ( .ip(n755), .ck(n437), .q(rd_buf0[15]) );
  dp_1 \rd_buf0_reg[16]  ( .ip(n754), .ck(n437), .q(rd_buf0[16]) );
  dp_1 \rd_buf0_reg[17]  ( .ip(n753), .ck(n437), .q(rd_buf0[17]) );
  dp_1 \rd_buf0_reg[18]  ( .ip(n752), .ck(n437), .q(rd_buf0[18]) );
  dp_1 \rd_buf0_reg[19]  ( .ip(n751), .ck(n437), .q(rd_buf0[19]) );
  dp_1 \rd_buf0_reg[20]  ( .ip(n750), .ck(n437), .q(rd_buf0[20]) );
  dp_1 \rd_buf0_reg[21]  ( .ip(n749), .ck(n437), .q(rd_buf0[21]) );
  dp_1 \rd_buf0_reg[22]  ( .ip(n748), .ck(n437), .q(rd_buf0[22]) );
  dp_1 \rd_buf0_reg[23]  ( .ip(n747), .ck(n440), .q(rd_buf0[23]) );
  dp_1 \rd_buf0_reg[24]  ( .ip(n746), .ck(n440), .q(rd_buf0[24]) );
  dp_1 \rd_buf0_reg[25]  ( .ip(n745), .ck(n440), .q(rd_buf0[25]) );
  dp_1 \rd_buf0_reg[26]  ( .ip(n744), .ck(n440), .q(rd_buf0[26]) );
  dp_1 \rd_buf0_reg[27]  ( .ip(n743), .ck(n440), .q(rd_buf0[27]) );
  dp_1 \rd_buf0_reg[28]  ( .ip(n742), .ck(n440), .q(rd_buf0[28]) );
  dp_1 \rd_buf0_reg[29]  ( .ip(n741), .ck(n440), .q(rd_buf0[29]) );
  dp_1 \rd_buf0_reg[30]  ( .ip(n740), .ck(n440), .q(rd_buf0[30]) );
  dp_1 \rd_buf0_reg[31]  ( .ip(n739), .ck(n440), .q(rd_buf0[31]) );
  dp_1 \rd_buf1_reg[16]  ( .ip(n722), .ck(n448), .q(rd_buf1[16]) );
  dp_1 \rd_buf1_reg[17]  ( .ip(n721), .ck(n448), .q(rd_buf1[17]) );
  dp_1 \rd_buf1_reg[18]  ( .ip(n720), .ck(n448), .q(rd_buf1[18]) );
  dp_1 \rd_buf1_reg[19]  ( .ip(n719), .ck(n448), .q(rd_buf1[19]) );
  dp_1 \rd_buf1_reg[20]  ( .ip(n718), .ck(n448), .q(rd_buf1[20]) );
  dp_1 rx_data_done_r2_reg ( .ip(rx_data_done_r), .ck(n403), .q(
        rx_data_done_r2) );
  dp_1 dtmp_sel_r_reg ( .ip(dtmp_sel), .ck(n415), .q(dtmp_sel_r) );
  dp_1 \dtmp_r_reg[0]  ( .ip(n836), .ck(n428), .q(dtmp_r[0]) );
  dp_1 rx_data_valid_r_reg ( .ip(rx_data_valid), .ck(n403), .q(rx_data_valid_r) );
  dp_1 \sizd_c_reg[13]  ( .ip(n843), .ck(n443), .q(sizd_c[13]) );
  dp_1 \sizd_c_reg[7]  ( .ip(n849), .ck(n443), .q(sizd_c[7]) );
  dp_1 \adr_cw_reg[14]  ( .ip(N44), .ck(n434), .q(madr[14]) );
  dp_1 \sizd_c_reg[6]  ( .ip(n850), .ck(n443), .q(sizd_c[6]) );
  dp_1 \sizd_c_reg[8]  ( .ip(n848), .ck(n443), .q(sizd_c[8]) );
  dp_1 \sizd_c_reg[2]  ( .ip(n854), .ck(n440), .q(sizd_c[2]) );
  dp_1 \sizd_c_reg[9]  ( .ip(n847), .ck(n443), .q(sizd_c[9]) );
  dp_1 \sizd_c_reg[1]  ( .ip(n855), .ck(n440), .q(sizd_c[1]) );
  dp_1 \state_reg[2]  ( .ip(n842), .ck(n412), .q(state[2]) );
  dp_1 \adr_cw_reg[11]  ( .ip(N41), .ck(n431), .q(madr[11]) );
  dp_1 \adr_cw_reg[7]  ( .ip(N37), .ck(n431), .q(madr[7]) );
  dp_1 \adr_cw_reg[3]  ( .ip(N33), .ck(n431), .q(madr[3]) );
  dp_1 \state_reg[0]  ( .ip(n858), .ck(n412), .q(state[0]) );
  dp_1 \sizd_c_reg[12]  ( .ip(n844), .ck(n443), .q(sizd_c[12]) );
  dp_1 \adr_cw_reg[10]  ( .ip(N40), .ck(n431), .q(madr[10]) );
  dp_1 \adr_cw_reg[6]  ( .ip(N36), .ck(n431), .q(madr[6]) );
  dp_1 \adr_cw_reg[2]  ( .ip(N32), .ck(n434), .q(madr[2]) );
  dp_1 \adr_cw_reg[13]  ( .ip(N43), .ck(n431), .q(madr[13]) );
  dp_1 \adr_cw_reg[5]  ( .ip(N35), .ck(n431), .q(madr[5]) );
  dp_1 \adr_cw_reg[1]  ( .ip(N31), .ck(n434), .q(madr[1]) );
  dp_1 \adr_cw_reg[12]  ( .ip(N42), .ck(n431), .q(madr[12]) );
  dp_1 \adr_cw_reg[8]  ( .ip(N38), .ck(n431), .q(madr[8]) );
  dp_1 \adr_cw_reg[4]  ( .ip(N34), .ck(n431), .q(madr[4]) );
  dp_1 \sizd_c_reg[4]  ( .ip(n852), .ck(n443), .q(sizd_c[4]) );
  dp_1 \sizd_c_reg[5]  ( .ip(n851), .ck(n443), .q(sizd_c[5]) );
  dp_1 \state_reg[5]  ( .ip(n839), .ck(n443), .q(state[5]) );
  dp_1 \sizd_c_reg[3]  ( .ip(n853), .ck(n440), .q(sizd_c[3]) );
  dp_1 \adr_cw_reg[9]  ( .ip(N39), .ck(n431), .q(madr[9]) );
  dp_1 \state_reg[1]  ( .ip(n857), .ck(n412), .q(state[1]) );
  dp_1 \adr_cb_reg[0]  ( .ip(N82), .ck(n409), .q(adr_cb[0]) );
  dp_1 \adr_cb_reg[1]  ( .ip(N83), .ck(n409), .q(adr_cb[1]) );
  dp_1 tx_dma_en_r_reg ( .ip(tx_dma_en), .ck(n403), .q(tx_dma_en_r) );
  dp_1 \state_reg[4]  ( .ip(n840), .ck(n446), .q(state[4]) );
  dp_1 \state_reg[7]  ( .ip(n837), .ck(n446), .q(state[7]) );
  dp_1 \state_reg[6]  ( .ip(n838), .ck(n443), .q(state[6]) );
  dp_1 wr_last_reg ( .ip(N202), .ck(n412), .q(wr_last) );
  dp_1 \sizd_c_reg[0]  ( .ip(n856), .ck(n440), .q(sizd_c[0]) );
  dp_1 word_done_reg ( .ip(N200), .ck(n412), .q(word_done) );
  dp_1 \state_reg[3]  ( .ip(n841), .ck(n412), .q(state[3]) );
  dp_1 rx_dma_en_r_reg ( .ip(rx_dma_en), .ck(n403), .q(rx_dma_en_r) );
  dp_1 \sizd_c_reg[10]  ( .ip(n846), .ck(n443), .q(sizd_c[10]) );
  dp_1 \sizd_c_reg[11]  ( .ip(n845), .ck(n443), .q(sizd_c[11]) );
  dp_1 \sizu_c_reg[9]  ( .ip(n860), .ck(n412), .q(sizu_c[9]) );
  dp_1 \sizu_c_reg[3]  ( .ip(n866), .ck(n409), .q(sizu_c[3]) );
  dp_1 \sizu_c_reg[6]  ( .ip(n863), .ck(n409), .q(sizu_c[6]) );
  dp_1 \sizu_c_reg[2]  ( .ip(n867), .ck(n409), .q(sizu_c[2]) );
  dp_1 \sizu_c_reg[8]  ( .ip(n861), .ck(n412), .q(sizu_c[8]) );
  dp_1 \sizu_c_reg[10]  ( .ip(n859), .ck(n412), .q(sizu_c[10]) );
  dp_1 \adr_cw_reg[0]  ( .ip(N30), .ck(n431), .q(madr[0]) );
  dp_1 \sizu_c_reg[7]  ( .ip(n862), .ck(n412), .q(sizu_c[7]) );
  dp_1 \sizu_c_reg[4]  ( .ip(n865), .ck(n409), .q(sizu_c[4]) );
  dp_1 \sizu_c_reg[5]  ( .ip(n864), .ck(n409), .q(sizu_c[5]) );
  dp_1 \sizu_c_reg[1]  ( .ip(n868), .ck(n409), .q(sizu_c[1]) );
  dp_1 \sizu_c_reg[0]  ( .ip(n869), .ck(n409), .q(sizu_c[0]) );
  dp_1 mack_r_reg ( .ip(N27), .ck(n431), .q(mack_r) );
  dp_1 wr_done_r_reg ( .ip(rx_data_done_r), .ck(n412), .q(wr_done_r) );
  nor2_2 U6 ( .ip1(n876), .ip2(n509), .op(n1) );
  nor3_2 U8 ( .ip1(n504), .ip2(rx_dma_en_r), .ip3(n883), .op(n3) );
  nor3_2 U10 ( .ip1(n3), .ip2(rx_dma_en_r), .ip3(n504), .op(n4) );
  inv_2 U12 ( .ip(n145), .op(n102) );
  inv_2 U14 ( .ip(n85), .op(n37) );
  nor3_2 U17 ( .ip1(n135), .ip2(n134), .ip3(n881), .op(n27) );
  nor3_2 U19 ( .ip1(n134), .ip2(n879), .ip3(n881), .op(n28) );
  inv_2 U25 ( .ip(n637), .op(n908) );
  inv_2 U27 ( .ip(n628), .op(n892) );
  inv_2 U29 ( .ip(n380), .op(n146) );
  inv_2 U31 ( .ip(n380), .op(n150) );
  inv_2 U34 ( .ip(n550), .op(n876) );
  nand2_2 U36 ( .ip1(n383), .ip2(n475), .op(n452) );
  and2_2 U42 ( .ip1(n628), .ip2(n695), .op(n634) );
  nand2_2 U44 ( .ip1(n383), .ip2(n391), .op(n358) );
  nor3_2 U46 ( .ip1(n591), .ip2(n701), .ip3(n905), .op(n496) );
  nand2_2 U48 ( .ip1(mack_r), .ip2(n383), .op(n637) );
  nor3_2 U51 ( .ip1(sizd_c[0]), .ip2(sizd_c[11]), .ip3(sizd_c[10]), .op(n706)
         );
  nor3_2 U53 ( .ip1(n877), .ip2(abort), .ip3(n504), .op(n480) );
  nor3_2 U59 ( .ip1(n900), .ip2(wr_last), .ip3(wr_done), .op(n569) );
  and2_2 U61 ( .ip1(n550), .ip2(n551), .op(n509) );
  nand2_2 U63 ( .ip1(n383), .ip2(n447), .op(n424) );
  nand2_2 U65 ( .ip1(n383), .ip2(n419), .op(n396) );
  nor3_2 U68 ( .ip1(abort), .ip2(mack_r), .ip3(n589), .op(dtmp_sel) );
  nor3_2 U70 ( .ip1(sizd_c[3]), .ip2(sizd_c[5]), .ip3(sizd_c[4]), .op(n352) );
  nor3_2 U76 ( .ip1(n995), .ip2(sizd_c[11]), .ip3(sizd_c[10]), .op(n350) );
  buf_1 U78 ( .ip(n216), .op(n14) );
  buf_1 U80 ( .ip(n216), .op(n16) );
  buf_1 U82 ( .ip(n216), .op(n18) );
  buf_1 U85 ( .ip(n216), .op(n20) );
  buf_1 U87 ( .ip(n216), .op(n24) );
  buf_1 U93 ( .ip(n216), .op(n26) );
  inv_1 U95 ( .ip(n85), .op(n38) );
  buf_1 U97 ( .ip(n14), .op(n39) );
  buf_1 U99 ( .ip(n14), .op(n40) );
  buf_1 U102 ( .ip(n14), .op(n43) );
  buf_1 U104 ( .ip(n16), .op(n44) );
  buf_1 U110 ( .ip(n16), .op(n53) );
  buf_1 U112 ( .ip(n16), .op(n54) );
  buf_1 U114 ( .ip(n18), .op(n55) );
  buf_1 U116 ( .ip(n18), .op(n56) );
  buf_1 U119 ( .ip(n18), .op(n59) );
  buf_1 U121 ( .ip(n20), .op(n60) );
  buf_1 U127 ( .ip(n20), .op(n69) );
  buf_1 U130 ( .ip(n20), .op(n70) );
  buf_1 U133 ( .ip(n24), .op(n71) );
  buf_1 U136 ( .ip(n24), .op(n72) );
  buf_1 U140 ( .ip(n24), .op(n75) );
  buf_1 U143 ( .ip(n26), .op(n76) );
  buf_1 U146 ( .ip(n26), .op(n85) );
  buf_1 U148 ( .ip(n149), .op(n86) );
  buf_1 U246 ( .ip(n149), .op(n87) );
  buf_1 U343 ( .ip(n149), .op(n88) );
  buf_1 U446 ( .ip(n149), .op(n91) );
  buf_1 U449 ( .ip(n149), .op(n92) );
  buf_1 U453 ( .ip(n149), .op(n101) );
  inv_1 U457 ( .ip(n145), .op(n103) );
  buf_1 U461 ( .ip(n86), .op(n104) );
  buf_1 U465 ( .ip(n86), .op(n107) );
  buf_1 U469 ( .ip(n86), .op(n108) );
  buf_1 U473 ( .ip(n87), .op(n117) );
  buf_1 U477 ( .ip(n87), .op(n118) );
  buf_1 U480 ( .ip(n87), .op(n119) );
  buf_1 U484 ( .ip(n88), .op(n120) );
  buf_1 U488 ( .ip(n88), .op(n123) );
  buf_1 U492 ( .ip(n88), .op(n124) );
  buf_1 U496 ( .ip(n91), .op(n133) );
  buf_1 U500 ( .ip(n91), .op(n137) );
  buf_1 U504 ( .ip(n91), .op(n138) );
  buf_1 U508 ( .ip(n92), .op(n139) );
  buf_1 U512 ( .ip(n92), .op(n140) );
  buf_1 U515 ( .ip(n92), .op(n141) );
  buf_1 U519 ( .ip(n101), .op(n144) );
  buf_1 U523 ( .ip(n101), .op(n145) );
  buf_1 U527 ( .ip(word_done), .op(n215) );
  buf_1 U531 ( .ip(word_done), .op(n279) );
  buf_1 U535 ( .ip(word_done), .op(n282) );
  buf_1 U539 ( .ip(word_done), .op(n347) );
  buf_1 U543 ( .ip(word_done), .op(n354) );
  buf_1 U547 ( .ip(word_done), .op(n357) );
  buf_1 U550 ( .ip(word_done), .op(n360) );
  buf_1 U554 ( .ip(word_done), .op(n363) );
  buf_1 U556 ( .ip(word_done), .op(n364) );
  buf_1 U559 ( .ip(word_done), .op(n367) );
  buf_1 U561 ( .ip(word_done), .op(n368) );
  buf_1 U564 ( .ip(word_done), .op(n371) );
  buf_1 U566 ( .ip(word_done), .op(n372) );
  buf_1 U569 ( .ip(word_done), .op(n375) );
  buf_1 U571 ( .ip(word_done), .op(n376) );
  buf_1 U574 ( .ip(word_done), .op(n379) );
  buf_1 U576 ( .ip(word_done), .op(n380) );
  inv_2 U579 ( .ip(dtmp_sel_r), .op(n383) );
  buf_1 U581 ( .ip(clk), .op(n384) );
  buf_1 U584 ( .ip(clk), .op(n387) );
  buf_1 U586 ( .ip(clk), .op(n388) );
  buf_1 U589 ( .ip(clk), .op(n390) );
  buf_1 U592 ( .ip(clk), .op(n395) );
  buf_1 U595 ( .ip(clk), .op(n400) );
  buf_1 U615 ( .ip(n384), .op(n403) );
  buf_1 U671 ( .ip(n384), .op(n406) );
  buf_1 U673 ( .ip(n384), .op(n409) );
  buf_1 U678 ( .ip(n387), .op(n412) );
  buf_1 U687 ( .ip(n387), .op(n415) );
  buf_1 U692 ( .ip(n387), .op(n418) );
  buf_1 U693 ( .ip(n388), .op(n420) );
  buf_1 U699 ( .ip(n388), .op(n423) );
  buf_1 U702 ( .ip(n388), .op(n428) );
  buf_1 U708 ( .ip(n390), .op(n431) );
  buf_1 U713 ( .ip(n390), .op(n434) );
  buf_1 U748 ( .ip(n390), .op(n437) );
  buf_1 U750 ( .ip(n395), .op(n440) );
  buf_1 U753 ( .ip(n395), .op(n443) );
  buf_1 U754 ( .ip(n395), .op(n446) );
  buf_1 U758 ( .ip(n400), .op(n448) );
  buf_1 U764 ( .ip(n400), .op(n451) );
  nand3_1 U766 ( .ip1(sizu_c[1]), .ip2(sizu_c[0]), .ip3(sizu_c[2]), .op(n462)
         );
  nor3_1 U771 ( .ip1(n502), .ip2(n462), .ip3(n493), .op(n468) );
  and3_1 U776 ( .ip1(sizu_c[5]), .ip2(n468), .ip3(sizu_c[6]), .op(n474) );
  nand3_1 U777 ( .ip1(sizu_c[7]), .ip2(n474), .ip3(sizu_c[8]), .op(n488) );
  nor2_1 U869 ( .ip1(n489), .ip2(n488), .op(n456) );
  xor2_1 U870 ( .ip1(sizu_c[10]), .ip2(n456), .op(N142) );
  xor2_1 U871 ( .ip1(sizu_c[1]), .ip2(sizu_c[0]), .op(N133) );
  nand2_1 U872 ( .ip1(sizu_c[1]), .ip2(sizu_c[0]), .op(n459) );
  xnor2_1 U875 ( .ip1(n459), .ip2(sizu_c[2]), .op(N134) );
  xor2_1 U879 ( .ip1(n462), .ip2(n502), .op(N135) );
  nor2_1 U881 ( .ip1(n462), .ip2(n502), .op(n465) );
  xor2_1 U883 ( .ip1(sizu_c[4]), .ip2(n465), .op(N136) );
  xor2_1 U886 ( .ip1(sizu_c[5]), .ip2(n468), .op(N137) );
  nand2_1 U888 ( .ip1(sizu_c[5]), .ip2(n468), .op(n471) );
  xnor2_1 U889 ( .ip1(n471), .ip2(sizu_c[6]), .op(N138) );
  xor2_1 U894 ( .ip1(n474), .ip2(sizu_c[7]), .op(N139) );
  and2_1 U896 ( .ip1(n474), .ip2(sizu_c[7]), .op(n477) );
  xor2_1 U897 ( .ip1(sizu_c[8]), .ip2(n477), .op(N140) );
  xor2_1 U898 ( .ip1(n489), .ip2(n488), .op(N141) );
  inv_2 U900 ( .ip(sizu_c[9]), .op(n489) );
  inv_2 U901 ( .ip(sizu_c[4]), .op(n493) );
  inv_2 U903 ( .ip(sizu_c[3]), .op(n502) );
  inv_2 U906 ( .ip(sizu_c[0]), .op(N132) );
  xor2_1 U910 ( .ip1(last_buf_adr[14]), .ip2(adrw_next[14]), .op(n510) );
  xor2_1 U911 ( .ip1(last_buf_adr[12]), .ip2(adrw_next[12]), .op(n508) );
  xor2_1 U912 ( .ip1(last_buf_adr[13]), .ip2(adrw_next[13]), .op(n503) );
  nor3_1 U914 ( .ip1(n510), .ip2(n508), .ip3(n503), .op(n872) );
  xor2_1 U917 ( .ip1(last_buf_adr[8]), .ip2(adrw_next[8]), .op(n574) );
  xor2_1 U919 ( .ip1(last_buf_adr[9]), .ip2(adrw_next[9]), .op(n572) );
  xor2_1 U920 ( .ip1(last_buf_adr[10]), .ip2(adrw_next[10]), .op(n570) );
  xor2_1 U921 ( .ip1(last_buf_adr[11]), .ip2(adrw_next[11]), .op(n559) );
  nor4_1 U922 ( .ip1(n574), .ip2(n572), .ip3(n570), .ip4(n559), .op(n871) );
  xor2_1 U923 ( .ip1(last_buf_adr[4]), .ip2(adrw_next[4]), .op(n594) );
  xor2_1 U924 ( .ip1(last_buf_adr[5]), .ip2(adrw_next[5]), .op(n590) );
  xor2_1 U925 ( .ip1(last_buf_adr[6]), .ip2(adrw_next[6]), .op(n587) );
  xor2_1 U926 ( .ip1(last_buf_adr[7]), .ip2(adrw_next[7]), .op(n586) );
  nor4_1 U927 ( .ip1(n594), .ip2(n590), .ip3(n587), .ip4(n586), .op(n870) );
  nor2_1 U928 ( .ip1(n873), .ip2(last_buf_adr[0]), .op(n595) );
  nor2_1 U929 ( .ip1(n595), .ip2(n875), .op(n622) );
  nor2_1 U930 ( .ip1(adrw_next[1]), .ip2(n595), .op(n616) );
  nor2_1 U931 ( .ip1(n622), .ip2(n616), .op(n704) );
  and2_1 U932 ( .ip1(last_buf_adr[0]), .ip2(n873), .op(n631) );
  nor2_1 U933 ( .ip1(last_buf_adr[1]), .ip2(n631), .op(n697) );
  nor2_1 U934 ( .ip1(n631), .ip2(n874), .op(n638) );
  nor2_1 U935 ( .ip1(n697), .ip2(n638), .op(n703) );
  xor2_1 U936 ( .ip1(last_buf_adr[2]), .ip2(adrw_next[2]), .op(n699) );
  xor2_1 U937 ( .ip1(last_buf_adr[3]), .ip2(adrw_next[3]), .op(n698) );
  nor4_1 U938 ( .ip1(n704), .ip2(n703), .ip3(n699), .ip4(n698), .op(n803) );
  and4_1 U939 ( .ip1(n872), .ip2(n871), .ip3(n870), .ip4(n803), .op(N60) );
  inv_2 U940 ( .ip(adrw_next[0]), .op(n873) );
  inv_2 U941 ( .ip(adrw_next[1]), .op(n874) );
  inv_2 U942 ( .ip(last_buf_adr[1]), .op(n875) );
  inv_2 U943 ( .ip(n557), .op(n877) );
  inv_2 U944 ( .ip(abort), .op(n878) );
  inv_2 U945 ( .ip(n135), .op(n879) );
  inv_2 U946 ( .ip(n134), .op(n880) );
  inv_2 U947 ( .ip(n136), .op(n881) );
  inv_2 U948 ( .ip(n625), .op(n882) );
  inv_2 U949 ( .ip(rx_data_valid_r), .op(n883) );
  inv_2 U950 ( .ip(rx_data_st_r[7]), .op(n884) );
  inv_2 U951 ( .ip(rx_data_st_r[6]), .op(n885) );
  inv_2 U952 ( .ip(rx_data_st_r[5]), .op(n886) );
  inv_2 U953 ( .ip(rx_data_st_r[4]), .op(n887) );
  inv_2 U954 ( .ip(rx_data_st_r[3]), .op(n888) );
  inv_2 U955 ( .ip(rx_data_st_r[2]), .op(n889) );
  inv_2 U956 ( .ip(rx_data_st_r[1]), .op(n890) );
  inv_2 U957 ( .ip(rx_data_st_r[0]), .op(n891) );
  inv_2 U958 ( .ip(send_zero_length_r), .op(n893) );
  inv_2 U959 ( .ip(adr_cb[0]), .op(n894) );
  inv_2 U960 ( .ip(adr_cb[1]), .op(n895) );
  inv_2 U961 ( .ip(n571), .op(n896) );
  inv_2 U962 ( .ip(n589), .op(n897) );
  inv_2 U963 ( .ip(n486), .op(n898) );
  inv_2 U964 ( .ip(n701), .op(n899) );
  inv_2 U965 ( .ip(n496), .op(n900) );
  inv_2 U966 ( .ip(n583), .op(n901) );
  inv_2 U967 ( .ip(state[0]), .op(n902) );
  inv_2 U968 ( .ip(n499), .op(n903) );
  inv_2 U969 ( .ip(wr_last), .op(n904) );
  inv_2 U970 ( .ip(state[3]), .op(n905) );
  inv_2 U971 ( .ip(state[1]), .op(n906) );
  inv_2 U972 ( .ip(state[2]), .op(n907) );
  inv_2 U973 ( .ip(dtmp_r[31]), .op(n909) );
  inv_2 U974 ( .ip(dtmp_r[30]), .op(n910) );
  inv_2 U975 ( .ip(dtmp_r[29]), .op(n911) );
  inv_2 U976 ( .ip(dtmp_r[28]), .op(n912) );
  inv_2 U977 ( .ip(dtmp_r[27]), .op(n913) );
  inv_2 U978 ( .ip(dtmp_r[26]), .op(n914) );
  inv_2 U979 ( .ip(dtmp_r[25]), .op(n915) );
  inv_2 U980 ( .ip(dtmp_r[24]), .op(n916) );
  inv_2 U981 ( .ip(dtmp_r[23]), .op(n917) );
  inv_2 U982 ( .ip(dtmp_r[22]), .op(n918) );
  inv_2 U983 ( .ip(dtmp_r[21]), .op(n919) );
  inv_2 U984 ( .ip(dtmp_r[20]), .op(n920) );
  inv_2 U985 ( .ip(dtmp_r[19]), .op(n921) );
  inv_2 U986 ( .ip(dtmp_r[18]), .op(n922) );
  inv_2 U987 ( .ip(dtmp_r[17]), .op(n923) );
  inv_2 U988 ( .ip(dtmp_r[16]), .op(n924) );
  inv_2 U989 ( .ip(dtmp_r[15]), .op(n925) );
  inv_2 U990 ( .ip(dtmp_r[14]), .op(n926) );
  inv_2 U991 ( .ip(dtmp_r[13]), .op(n927) );
  inv_2 U992 ( .ip(dtmp_r[12]), .op(n928) );
  inv_2 U993 ( .ip(dtmp_r[11]), .op(n929) );
  inv_2 U994 ( .ip(dtmp_r[10]), .op(n930) );
  inv_2 U995 ( .ip(dtmp_r[9]), .op(n931) );
  inv_2 U996 ( .ip(dtmp_r[8]), .op(n932) );
  inv_2 U997 ( .ip(dtmp_r[7]), .op(n933) );
  inv_2 U998 ( .ip(dtmp_r[6]), .op(n934) );
  inv_2 U999 ( .ip(dtmp_r[5]), .op(n935) );
  inv_2 U1000 ( .ip(dtmp_r[4]), .op(n936) );
  inv_2 U1001 ( .ip(dtmp_r[3]), .op(n937) );
  inv_2 U1002 ( .ip(dtmp_r[2]), .op(n938) );
  inv_2 U1003 ( .ip(dtmp_r[1]), .op(n939) );
  inv_2 U1004 ( .ip(dtmp_r[0]), .op(n940) );
  inv_2 U1005 ( .ip(mack_r), .op(n941) );
  inv_2 U1006 ( .ip(rd_buf0[0]), .op(n942) );
  inv_2 U1007 ( .ip(rd_buf0[1]), .op(n943) );
  inv_2 U1008 ( .ip(rd_buf0[2]), .op(n944) );
  inv_2 U1009 ( .ip(rd_buf0[3]), .op(n945) );
  inv_2 U1010 ( .ip(rd_buf0[4]), .op(n946) );
  inv_2 U1011 ( .ip(rd_buf0[5]), .op(n947) );
  inv_2 U1012 ( .ip(rd_buf0[6]), .op(n948) );
  inv_2 U1013 ( .ip(rd_buf0[7]), .op(n949) );
  inv_2 U1014 ( .ip(rd_buf0[8]), .op(n950) );
  inv_2 U1015 ( .ip(rd_buf0[9]), .op(n951) );
  inv_2 U1016 ( .ip(rd_buf0[10]), .op(n952) );
  inv_2 U1017 ( .ip(rd_buf0[11]), .op(n953) );
  inv_2 U1018 ( .ip(rd_buf0[12]), .op(n954) );
  inv_2 U1019 ( .ip(rd_buf0[13]), .op(n955) );
  inv_2 U1020 ( .ip(rd_buf0[14]), .op(n956) );
  inv_2 U1021 ( .ip(rd_buf0[15]), .op(n957) );
  inv_2 U1022 ( .ip(rd_buf0[16]), .op(n958) );
  inv_2 U1023 ( .ip(rd_buf0[17]), .op(n959) );
  inv_2 U1024 ( .ip(rd_buf0[18]), .op(n960) );
  inv_2 U1025 ( .ip(rd_buf0[19]), .op(n961) );
  inv_2 U1026 ( .ip(rd_buf0[20]), .op(n962) );
  inv_2 U1027 ( .ip(rd_buf0[21]), .op(n963) );
  inv_2 U1028 ( .ip(rd_buf0[22]), .op(n964) );
  inv_2 U1029 ( .ip(rd_buf0[23]), .op(n965) );
  inv_2 U1030 ( .ip(rd_buf0[24]), .op(n966) );
  inv_2 U1031 ( .ip(rd_buf0[25]), .op(n967) );
  inv_2 U1032 ( .ip(rd_buf0[26]), .op(n968) );
  inv_2 U1033 ( .ip(rd_buf0[27]), .op(n969) );
  inv_2 U1034 ( .ip(rd_buf0[28]), .op(n970) );
  inv_2 U1035 ( .ip(rd_buf0[29]), .op(n971) );
  inv_2 U1036 ( .ip(rd_buf0[30]), .op(n972) );
  inv_2 U1037 ( .ip(rd_buf0[31]), .op(n973) );
  inv_2 U1038 ( .ip(n349), .op(n974) );
  inv_2 U1039 ( .ip(sizd_is_zero), .op(n975) );
  inv_2 U1040 ( .ip(state[6]), .op(n976) );
  inv_2 U1041 ( .ip(state[4]), .op(n977) );
  inv_2 U1042 ( .ip(state[7]), .op(n978) );
  inv_2 U1043 ( .ip(rd_buf1[16]), .op(n979) );
  inv_2 U1044 ( .ip(rd_buf1[17]), .op(n980) );
  inv_2 U1045 ( .ip(rd_buf1[18]), .op(n981) );
  inv_2 U1046 ( .ip(rd_buf1[19]), .op(n982) );
  inv_2 U1047 ( .ip(rd_buf1[20]), .op(n983) );
  inv_2 U1048 ( .ip(rd_buf1[21]), .op(n984) );
  inv_2 U1049 ( .ip(rd_buf1[22]), .op(n985) );
  inv_2 U1050 ( .ip(rd_buf1[23]), .op(n986) );
  inv_2 U1051 ( .ip(rd_buf1[24]), .op(n987) );
  inv_2 U1052 ( .ip(rd_buf1[25]), .op(n988) );
  inv_2 U1053 ( .ip(rd_buf1[26]), .op(n989) );
  inv_2 U1054 ( .ip(rd_buf1[27]), .op(n990) );
  inv_2 U1055 ( .ip(rd_buf1[28]), .op(n991) );
  inv_2 U1056 ( .ip(rd_buf1[29]), .op(n992) );
  inv_2 U1057 ( .ip(rd_buf1[30]), .op(n993) );
  inv_2 U1058 ( .ip(rd_buf1[31]), .op(n994) );
  inv_2 U1059 ( .ip(rd_next), .op(n995) );
endmodule


module usbf_pe_SSRAM_HADR14_DW01_add_0 ( A, B, CI, SUM, CO );
  input [16:0] A;
  input [16:0] B;
  output [16:0] SUM;
  input CI;
  output CO;
  wire   \carry<14> , \carry<13> , \carry<12> , \carry<11> , \carry<10> ,
         \carry<9> , \carry<8> , \carry<7> , \carry<6> , \carry<5> ,
         \carry<4> , \carry<3> , \carry<2> , n1, n2, n3;

  fulladder U1_13 ( .a(A[13]), .b(B[13]), .ci(\carry<13> ), .co(\carry<14> ), 
        .s(SUM[13]) );
  fulladder U1_12 ( .a(A[12]), .b(B[12]), .ci(\carry<12> ), .co(\carry<13> ), 
        .s(SUM[12]) );
  fulladder U1_11 ( .a(A[11]), .b(B[11]), .ci(\carry<11> ), .co(\carry<12> ), 
        .s(SUM[11]) );
  fulladder U1_10 ( .a(A[10]), .b(B[10]), .ci(\carry<10> ), .co(\carry<11> ), 
        .s(SUM[10]) );
  fulladder U1_9 ( .a(A[9]), .b(B[9]), .ci(\carry<9> ), .co(\carry<10> ), .s(
        SUM[9]) );
  fulladder U1_8 ( .a(A[8]), .b(B[8]), .ci(\carry<8> ), .co(\carry<9> ), .s(
        SUM[8]) );
  fulladder U1_7 ( .a(A[7]), .b(B[7]), .ci(\carry<7> ), .co(\carry<8> ), .s(
        SUM[7]) );
  fulladder U1_6 ( .a(A[6]), .b(B[6]), .ci(\carry<6> ), .co(\carry<7> ), .s(
        SUM[6]) );
  fulladder U1_5 ( .a(A[5]), .b(B[5]), .ci(\carry<5> ), .co(\carry<6> ), .s(
        SUM[5]) );
  fulladder U1_4 ( .a(A[4]), .b(B[4]), .ci(\carry<4> ), .co(\carry<5> ), .s(
        SUM[4]) );
  fulladder U1_3 ( .a(A[3]), .b(B[3]), .ci(\carry<3> ), .co(\carry<4> ), .s(
        SUM[3]) );
  fulladder U1_2 ( .a(A[2]), .b(B[2]), .ci(\carry<2> ), .co(\carry<3> ), .s(
        SUM[2]) );
  fulladder U1_1 ( .a(A[1]), .b(B[1]), .ci(n3), .co(\carry<2> ), .s(SUM[1]) );
  and2_2 U1 ( .ip1(A[14]), .ip2(\carry<14> ), .op(n1) );
  and2_2 U2 ( .ip1(A[15]), .ip2(n1), .op(n2) );
  and2_2 U3 ( .ip1(B[0]), .ip2(A[0]), .op(n3) );
  xor2_2 U4 ( .ip1(A[16]), .ip2(n2), .op(SUM[16]) );
  xor2_2 U5 ( .ip1(A[15]), .ip2(n1), .op(SUM[15]) );
  xor2_2 U6 ( .ip1(A[14]), .ip2(\carry<14> ), .op(SUM[14]) );
  xor2_2 U7 ( .ip1(B[0]), .ip2(A[0]), .op(SUM[0]) );
endmodule


module usbf_pe_SSRAM_HADR14_DW01_sub_0 ( A, B, CI, DIFF, CO );
  input [13:0] A;
  input [13:0] B;
  output [13:0] DIFF;
  input CI;
  output CO;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15;
  wire   [14:0] carry;

  fulladder U2_13 ( .a(A[13]), .b(n2), .ci(carry[13]), .s(DIFF[13]) );
  fulladder U2_12 ( .a(A[12]), .b(n3), .ci(carry[12]), .co(carry[13]), .s(
        DIFF[12]) );
  fulladder U2_11 ( .a(A[11]), .b(n4), .ci(carry[11]), .co(carry[12]), .s(
        DIFF[11]) );
  fulladder U2_10 ( .a(A[10]), .b(n5), .ci(carry[10]), .co(carry[11]), .s(
        DIFF[10]) );
  fulladder U2_9 ( .a(A[9]), .b(n6), .ci(carry[9]), .co(carry[10]), .s(DIFF[9]) );
  fulladder U2_8 ( .a(A[8]), .b(n7), .ci(carry[8]), .co(carry[9]), .s(DIFF[8])
         );
  fulladder U2_7 ( .a(A[7]), .b(n8), .ci(carry[7]), .co(carry[8]), .s(DIFF[7])
         );
  fulladder U2_6 ( .a(A[6]), .b(n9), .ci(carry[6]), .co(carry[7]), .s(DIFF[6])
         );
  fulladder U2_5 ( .a(A[5]), .b(n10), .ci(carry[5]), .co(carry[6]), .s(DIFF[5]) );
  fulladder U2_4 ( .a(A[4]), .b(n11), .ci(carry[4]), .co(carry[5]), .s(DIFF[4]) );
  fulladder U2_3 ( .a(A[3]), .b(n12), .ci(carry[3]), .co(carry[4]), .s(DIFF[3]) );
  fulladder U2_2 ( .a(A[2]), .b(n13), .ci(carry[2]), .co(carry[3]), .s(DIFF[2]) );
  fulladder U2_1 ( .a(A[1]), .b(n14), .ci(n1), .co(carry[2]), .s(DIFF[1]) );
  or2_2 U1 ( .ip1(A[0]), .ip2(n15), .op(n1) );
  xnor2_1 U2 ( .ip1(n15), .ip2(A[0]), .op(DIFF[0]) );
  inv_2 U3 ( .ip(B[13]), .op(n2) );
  inv_2 U4 ( .ip(B[12]), .op(n3) );
  inv_2 U5 ( .ip(B[11]), .op(n4) );
  inv_2 U6 ( .ip(B[10]), .op(n5) );
  inv_2 U7 ( .ip(B[9]), .op(n6) );
  inv_2 U8 ( .ip(B[8]), .op(n7) );
  inv_2 U9 ( .ip(B[7]), .op(n8) );
  inv_2 U10 ( .ip(B[6]), .op(n9) );
  inv_2 U11 ( .ip(B[5]), .op(n10) );
  inv_2 U12 ( .ip(B[4]), .op(n11) );
  inv_2 U13 ( .ip(B[3]), .op(n12) );
  inv_2 U14 ( .ip(B[2]), .op(n13) );
  inv_2 U15 ( .ip(B[1]), .op(n14) );
  inv_2 U16 ( .ip(B[0]), .op(n15) );
endmodule


module usbf_pe_SSRAM_HADR14_DW01_cmp6_2 ( A, B, TC, LT, GT, EQ, LE, GE, NE );
  input [10:0] A;
  input [10:0] B;
  input TC;
  output LT, GT, EQ, LE, GE, NE;
  wire   n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17,
         n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31,
         n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45,
         n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59,
         n60, n61, n62, n63;

  inv_2 U1 ( .ip(n15), .op(GT) );
  inv_2 U2 ( .ip(n16), .op(LT) );
  inv_2 U3 ( .ip(A[0]), .op(n3) );
  inv_2 U4 ( .ip(A[1]), .op(n4) );
  inv_2 U5 ( .ip(A[2]), .op(n5) );
  inv_2 U6 ( .ip(A[4]), .op(n6) );
  inv_2 U7 ( .ip(A[6]), .op(n7) );
  inv_2 U8 ( .ip(A[8]), .op(n8) );
  inv_2 U9 ( .ip(A[10]), .op(n9) );
  inv_2 U10 ( .ip(B[9]), .op(n10) );
  inv_2 U11 ( .ip(B[7]), .op(n11) );
  inv_2 U12 ( .ip(B[5]), .op(n12) );
  inv_2 U13 ( .ip(B[3]), .op(n13) );
  inv_2 U14 ( .ip(B[1]), .op(n14) );
  nand2_1 U15 ( .ip1(n15), .ip2(n16), .op(NE) );
  nor2_1 U16 ( .ip1(n17), .ip2(n18), .op(n16) );
  nor3_1 U17 ( .ip1(n19), .ip2(n20), .ip3(n21), .op(n18) );
  nor3_1 U18 ( .ip1(n22), .ip2(n23), .ip3(n24), .op(n21) );
  nor3_1 U19 ( .ip1(n25), .ip2(n26), .ip3(n27), .op(n24) );
  nor3_1 U20 ( .ip1(n28), .ip2(n29), .ip3(n30), .op(n27) );
  nor3_1 U21 ( .ip1(n31), .ip2(n32), .ip3(n33), .op(n30) );
  nor3_1 U22 ( .ip1(n34), .ip2(n35), .ip3(n36), .op(n33) );
  nor3_1 U23 ( .ip1(n37), .ip2(n38), .ip3(n39), .op(n36) );
  nor3_1 U24 ( .ip1(n40), .ip2(n41), .ip3(n42), .op(n39) );
  not_ab_or_c_or_d U25 ( .ip1(n43), .ip2(n14), .ip3(n44), .ip4(n45), .op(n42)
         );
  nor2_1 U26 ( .ip1(n46), .ip2(n4), .op(n45) );
  nand2_1 U27 ( .ip1(n46), .ip2(n4), .op(n43) );
  and2_1 U28 ( .ip1(B[0]), .ip2(n3), .op(n46) );
  and2_1 U29 ( .ip1(n9), .ip2(B[10]), .op(n17) );
  nor2_1 U30 ( .ip1(n47), .ip2(n48), .op(n15) );
  nor3_1 U31 ( .ip1(n19), .ip2(n22), .ip3(n49), .op(n48) );
  nor3_1 U32 ( .ip1(n50), .ip2(n51), .ip3(n20), .op(n49) );
  and2_1 U33 ( .ip1(A[9]), .ip2(n10), .op(n20) );
  nor3_1 U34 ( .ip1(n25), .ip2(n28), .ip3(n52), .op(n50) );
  nor3_1 U35 ( .ip1(n53), .ip2(n54), .ip3(n26), .op(n52) );
  and2_1 U36 ( .ip1(A[7]), .ip2(n11), .op(n26) );
  nor3_1 U37 ( .ip1(n31), .ip2(n34), .ip3(n55), .op(n53) );
  nor3_1 U38 ( .ip1(n56), .ip2(n57), .ip3(n32), .op(n55) );
  and2_1 U39 ( .ip1(A[5]), .ip2(n12), .op(n32) );
  nor3_1 U40 ( .ip1(n37), .ip2(n40), .ip3(n58), .op(n56) );
  nor3_1 U41 ( .ip1(n59), .ip2(n60), .ip3(n38), .op(n58) );
  and2_1 U42 ( .ip1(A[3]), .ip2(n13), .op(n38) );
  not_ab_or_c_or_d U43 ( .ip1(B[1]), .ip2(n61), .ip3(n44), .ip4(n62), .op(n59)
         );
  nor2_1 U44 ( .ip1(A[1]), .ip2(n63), .op(n62) );
  or2_1 U45 ( .ip1(n60), .ip2(n41), .op(n44) );
  and2_1 U46 ( .ip1(B[2]), .ip2(n5), .op(n41) );
  nor2_1 U47 ( .ip1(n5), .ip2(B[2]), .op(n60) );
  nand2_1 U48 ( .ip1(n63), .ip2(A[1]), .op(n61) );
  nor2_1 U49 ( .ip1(n3), .ip2(B[0]), .op(n63) );
  nor2_1 U50 ( .ip1(n13), .ip2(A[3]), .op(n40) );
  or2_1 U51 ( .ip1(n57), .ip2(n35), .op(n37) );
  and2_1 U52 ( .ip1(B[4]), .ip2(n6), .op(n35) );
  nor2_1 U53 ( .ip1(n6), .ip2(B[4]), .op(n57) );
  nor2_1 U54 ( .ip1(n12), .ip2(A[5]), .op(n34) );
  or2_1 U55 ( .ip1(n54), .ip2(n29), .op(n31) );
  and2_1 U56 ( .ip1(B[6]), .ip2(n7), .op(n29) );
  nor2_1 U57 ( .ip1(n7), .ip2(B[6]), .op(n54) );
  nor2_1 U58 ( .ip1(n11), .ip2(A[7]), .op(n28) );
  or2_1 U59 ( .ip1(n51), .ip2(n23), .op(n25) );
  and2_1 U60 ( .ip1(B[8]), .ip2(n8), .op(n23) );
  nor2_1 U61 ( .ip1(n8), .ip2(B[8]), .op(n51) );
  nor2_1 U62 ( .ip1(n10), .ip2(A[9]), .op(n22) );
  xor2_1 U63 ( .ip1(B[10]), .ip2(A[10]), .op(n19) );
  nor2_1 U64 ( .ip1(B[10]), .ip2(n9), .op(n47) );
endmodule


module usbf_pe_SSRAM_HADR14 ( clk, rst, tx_valid, rx_active, pid_OUT, pid_IN, 
        pid_SOF, pid_SETUP, pid_DATA0, pid_DATA1, pid_DATA2, pid_MDATA, 
        pid_ACK, pid_NACK, pid_STALL, pid_NYET, pid_PRE, pid_ERR, pid_SPLIT, 
        pid_PING, mode_hs, token_valid, crc5_err, rx_data_valid, rx_data_done, 
        crc16_err, send_token, token_pid_sel, data_pid_sel, send_zero_length, 
        rx_dma_en, tx_dma_en, abort, idma_done, adr, size, buf_size, sizu_c, 
        dma_en, fsel, idin, dma_in_buf_sz1, dma_out_buf_avail, ep_sel, match, 
        nse_err, buf0_rl, buf0_set, buf1_set, uc_bsel_set, uc_dpd_set, 
        int_buf1_set, int_buf0_set, int_upid_set, int_crc16_set, int_to_set, 
        int_seqerr_set, out_to_small, csr, buf0, buf1 );
  output [1:0] token_pid_sel;
  output [1:0] data_pid_sel;
  output [16:0] adr;
  output [13:0] size;
  output [13:0] buf_size;
  input [10:0] sizu_c;
  output [31:0] idin;
  input [3:0] ep_sel;
  input [31:0] csr;
  input [31:0] buf0;
  input [31:0] buf1;
  input clk, rst, tx_valid, rx_active, pid_OUT, pid_IN, pid_SOF, pid_SETUP,
         pid_DATA0, pid_DATA1, pid_DATA2, pid_MDATA, pid_ACK, pid_NACK,
         pid_STALL, pid_NYET, pid_PRE, pid_ERR, pid_SPLIT, pid_PING, mode_hs,
         token_valid, crc5_err, rx_data_valid, rx_data_done, crc16_err,
         idma_done, fsel, dma_in_buf_sz1, dma_out_buf_avail, match;
  output send_token, send_zero_length, rx_dma_en, tx_dma_en, abort, dma_en,
         nse_err, buf0_rl, buf0_set, buf1_set, uc_bsel_set, uc_dpd_set,
         int_buf1_set, int_buf0_set, int_upid_set, int_crc16_set, int_to_set,
         int_seqerr_set, out_to_small;
  wire   N87, buf0_na, N88, buf1_na, N89, buf0_not_aloc, N90, buf1_not_aloc,
         match_r, N91, send_token_d, setup_token, N248, N249, N250,
         pid_seq_err, in_token, out_token, N378, N379, N380, N381, N382, N383,
         N384, N385, N386, N387, N388, N389, N390, N391, N392, N393, N394,
         buf_smaller, N396, buffer_full, buffer_empty, buffer_done, N397, N398,
         buf0_st_max, N399, buf1_st_max, N402, no_bufs0, N404, no_bufs1, N421,
         N422, N423, N424, N425, N426, N427, N428, N429, N430, N431, N432,
         N433, N434, N435, N436, N437, N438, N439, N440, N441, N442, N443,
         N444, N445, N446, N447, N448, N452, N453, N454, N455, N456, N457,
         N458, N459, N460, N461, N462, N463, N464, N465, N468, N469,
         buffer_overflow, uc_stat_set_d, N470, N471, out_to_small_r, N472,
         N473, to_small, N474, N475, to_large, N484, N485, N486, N487, N488,
         N489, N490, N491, N492, N493, N494, N495, N496, N497, N498, N499,
         N500, N501, N502, N503, N504, N505, N506, N507, N508, N509, N510,
         N511, N514, N515, N516, N517, N520, N521, buf0_rl_d, N522, N523,
         rx_ack_to_clr, N525, N526, N527, N528, N529, N530, N531, N532, N533,
         N534, N535, N536, N537, N538, N539, N540, N541, rx_ack_to, N544, N545,
         N546, N547, N548, N549, N550, N551, N552, N553, N554, N555, N556,
         N557, N558, N559, N560, tx_data_to, pid_OUT_r, pid_IN_r, pid_PING_r,
         pid_SETUP_r, N561, int_seqerr_set_d, n66, n98, n99, n100, n101, n102,
         n103, n104, n105, n106, n107, n108, n109, n110, n111, n112, n113,
         n114, n115, n116, n117, n118, n119, n120, n121, n122, n123, n124,
         n125, n126, n127, n128, n129, n130, n131, n132, n133, n134, n135,
         n136, n137, n138, n139, n140, n141, n142, n143, n144, n145, n146,
         n147, n148, n149, n150, n151, n152, n153, n154, n155, n156, n157,
         n158, n159, n160, n161, n162, n163, n164, n165, n166, n167, n168,
         n169, n170, n171, n172, n173, n174, n175, n176, n177, n178, n179,
         n180, n181, n182, n183, n184, n185, n186, n187, n188, n189, n190,
         n191, n192, n193, n194, n195, n196, n197, n198, n199, n200, n201,
         n202, n203, n204, n205, n206, n207, n208, n209, n210, n211, n212,
         n213, n214, n215, n216, n217, n218, n219, n221, n222, n223, n224,
         n225, n226, n227, n228, n229, n230, n231, n232, n233, n234, n235,
         n236, n237, n238, n239, n240, n241, n242, n243, n244, n245, n246,
         n247, n248, n249, n250, n251, n252, n253, n254, n255, n256, n257,
         n258, n259, n260, n261, n262, n263, n264, n265, n266, n267, n268,
         n269, n270, n271, n272, n273, n274, n275, n276, n277, n278, n279,
         n280, n281, n282, n283, n284, n285, n286, n287, n288, n289, n290,
         n291, n292, n293, n294, n295, n296, n297, n298, n299, n300, n301,
         n302, n303, n304, n305, n306, n307, n308, n309, n310, n311, n312,
         n313, n314, n315, n316, n317, n318, n319, n320, n321, n322, n323,
         n324, n325, n326, n327, n328, n329, n330, n331, n332, n333, n334,
         n335, n336, n337, n338, n339, n340, n341, n342, n343, n344, n345,
         n346, n347, n348, n349, n350, n351, n352, n353, n354, n355, n356,
         n357, n358, n359, n360, n361, n362, n363, n364, n365, n366, n367,
         n368, n369, n370, n371, n373, n374, n375, n376, n377, n378, n379,
         n380, n381, n382, n383, n384, n385, n386, n387, n388, n389, n390,
         n391, n392, n393, n394, n395, n396, n397, n398, n399, n400, n401,
         n402, n403, n404, n405, n406, n407, n408, n409, n410, n411, n412,
         n413, n414, n415, n416, n417, n418, n419, n420, n421, n422, n423,
         n424, n425, n426, n427, n428, n429, n430, n431, n432, n433, n434,
         n435, n436, n437, n438, n439, n440, n441, n442, n443, n444, n445,
         n446, n447, n448, n449, n450, n451, n452, n453, n454, n455, n456,
         n457, n458, n459, n460, n461, n462, n463, n464, n465, n466, n467,
         n468, n469, n470, n471, n472, n473, n474, n475, n476, n477, n478,
         n479, n480, n481, n482, n483, n484, n485, n486, n487, n488, n489,
         n490, n491, n492, n493, n494, n495, n496, n497, n498, n499, n500,
         n501, n502, n503, n504, n505, n506, n507, n508, n509, n510, n511,
         n512, n513, n514, n515, n516, n517, n518, n519, n520, n521, n522,
         n523, n524, n525, n526, n527, n528, n529, n530, n531, n532, n533,
         n534, n535, n536, n537, n538, n539, n540, n541, n542, n543, n544,
         n545, n546, n547, n548, n549, n550, \eq_4480/B[5] , n1, n2, n3, n6,
         n7, n8, n9, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21,
         n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35,
         n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49,
         n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63,
         n64, n65, n67, n68, n69, n70, n71, n72, n73, n74, n75, n76, n77, n78,
         n79, n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92,
         n93, n94, n95, n96, n97, n220, n372, n551, n552, n553, n554, n555,
         n556, n557, n558, n559, n560, n561, n562, n563, n564, n565, n566,
         n567, n568, n569, n570, n571, n572, n573, n574, n575, n576, n577,
         n578, n579, n580, n581, n582, n583, n584, n585, n586, n587, n588,
         n589, n590, n591, n592, n593, n594, n595, n596, n597, n598, n599,
         n600, n601, n602, n603, n604, n605, n606, n607, n608, n609, n610,
         n611, n612, n613, n614, n615, n616, n617, n618, n619, n620, n621,
         n622, n623, n624, n625, n626, n627, n628, n629, n630, n631, n632,
         n633, n634, n635, n636, n637, n638, n639, n640, n641, n642, n643,
         n644, n645, n646, n647, n648, n649, n650, n651, n652, n653, n654,
         n655, n656, n657, n658, n659, n660, n661, n662, n663, n664, n665,
         n666, n667, n668, n669, n670, n671, n672, n673, n674, n675, n676,
         n677, n678, n679, n680, n681, n682, n683, n684, n685, n686, n687,
         n688, n689, n690, n691, n692, n697, n698, n699, n700, n701, n702,
         n703, n704, n705, n706, n707, n708, n709, n710, n711, n712, n713,
         n714, n715, n716, n717, n718, n719, n720, n721, n722, n723, n724,
         n725, n726, n727, n728, n729, n730, n731, n732, n733, n734, n735,
         n736, n737, n738, n739, n740, n741, n742, n743, n744, n745, n746,
         n747, n748, n749, n750, n752, n753, n754, n755, n756, n757, n758,
         n759, n760, n761;
  wire   [1:0] token_pid_sel_d;
  wire   [1:0] next_dpid;
  wire   [1:0] allow_pid;
  wire   [13:0] new_size;
  wire   [13:0] new_sizeb;
  wire   [16:0] adr_r;
  wire   [13:0] size_next_r;
  wire   [16:0] new_adr;
  wire   [9:0] state;
  wire   [7:0] rx_ack_to_cnt;
  wire   [7:0] tx_data_to_cnt;

  nand2_2 U3 ( .ip1(n99), .ip2(n100), .op(token_pid_sel_d[1]) );
  nand2_2 U4 ( .ip1(n101), .ip2(n102), .op(n100) );
  nand3_2 U5 ( .ip1(n103), .ip2(n99), .ip3(n104), .op(token_pid_sel_d[0]) );
  nand3_2 U6 ( .ip1(n105), .ip2(n755), .ip3(n102), .op(n104) );
  nand4_2 U7 ( .ip1(no_bufs1), .ip2(no_bufs0), .ip3(n106), .ip4(n107), .op(n99) );
  and2_2 U8 ( .ip1(mode_hs), .ip2(n108), .op(n106) );
  nand3_2 U19 ( .ip1(n706), .ip2(n721), .ip3(n108), .op(n103) );
  nor4_2 U20 ( .ip1(n112), .ip2(n113), .ip3(n98), .ip4(n114), .op(
        send_zero_length) );
  or2_2 U21 ( .ip1(csr[0]), .ip2(csr[10]), .op(n114) );
  nand2_2 U22 ( .ip1(n115), .ip2(n102), .op(n98) );
  or3_2 U23 ( .ip1(csr[2]), .ip2(csr[3]), .ip3(csr[1]), .op(n113) );
  or4_2 U24 ( .ip1(csr[5]), .ip2(csr[6]), .ip3(csr[4]), .ip4(n116), .op(n112)
         );
  or3_2 U25 ( .ip1(csr[7]), .ip2(csr[9]), .ip3(csr[8]), .op(n116) );
  nand2_2 U26 ( .ip1(n117), .ip2(n118), .op(send_token_d) );
  nand3_2 U27 ( .ip1(n119), .ip2(n721), .ip3(n108), .op(n118) );
  nor2_2 U28 ( .ip1(n120), .ip2(state[6]), .op(n108) );
  nand2_2 U29 ( .ip1(n102), .ip2(n121), .op(n117) );
  and3_2 U30 ( .ip1(n122), .ip2(n123), .ip3(n102), .op(rx_dma_en) );
  nor2_2 U31 ( .ip1(n124), .ip2(n125), .op(n102) );
  nand2_2 U32 ( .ip1(n126), .ip2(n127), .op(n532) );
  nand2_2 U33 ( .ip1(n128), .ip2(n121), .op(n127) );
  nand2_2 U34 ( .ip1(state[1]), .ip2(n129), .op(n126) );
  nand2_2 U35 ( .ip1(n130), .ip2(n131), .op(n533) );
  nand2_2 U36 ( .ip1(n128), .ip2(n115), .op(n131) );
  nor2_2 U37 ( .ip1(n121), .ip2(n737), .op(n115) );
  nand2_2 U38 ( .ip1(state[2]), .ip2(n129), .op(n130) );
  nand2_2 U39 ( .ip1(n133), .ip2(n134), .op(n534) );
  nand3_2 U40 ( .ip1(n135), .ip2(n753), .ip3(n136), .op(n134) );
  nand2_2 U41 ( .ip1(state[3]), .ip2(n129), .op(n133) );
  nand2_2 U42 ( .ip1(n137), .ip2(n138), .op(n535) );
  nand3_2 U43 ( .ip1(n122), .ip2(n123), .ip3(n128), .op(n138) );
  and2_2 U44 ( .ip1(n136), .ip2(n715), .op(n128) );
  nand2_2 U45 ( .ip1(state[4]), .ip2(n129), .op(n137) );
  nand2_2 U46 ( .ip1(n139), .ip2(n140), .op(n536) );
  nand4_2 U47 ( .ip1(n136), .ip2(n719), .ip3(n141), .ip4(n753), .op(n140) );
  nand2_2 U48 ( .ip1(state[5]), .ip2(n129), .op(n139) );
  and2_2 U49 ( .ip1(n129), .ip2(state[6]), .op(n537) );
  nand2_2 U50 ( .ip1(n142), .ip2(n143), .op(n538) );
  nand2_2 U51 ( .ip1(n144), .ip2(n136), .op(n143) );
  nand2_2 U52 ( .ip1(state[7]), .ip2(n129), .op(n142) );
  nand2_2 U53 ( .ip1(n145), .ip2(n146), .op(n539) );
  nand2_2 U54 ( .ip1(n136), .ip2(n147), .op(n146) );
  nand4_2 U55 ( .ip1(n148), .ip2(n149), .ip3(n150), .ip4(n713), .op(n147) );
  nand2_2 U56 ( .ip1(n107), .ip2(n708), .op(n150) );
  nand2_2 U57 ( .ip1(n151), .ip2(n135), .op(n149) );
  nand2_2 U58 ( .ip1(n152), .ip2(n725), .op(n148) );
  nand2_2 U59 ( .ip1(state[8]), .ip2(n129), .op(n145) );
  nand2_2 U60 ( .ip1(n153), .ip2(n154), .op(n540) );
  nand2_2 U61 ( .ip1(n710), .ip2(n136), .op(n154) );
  nand2_2 U62 ( .ip1(state[9]), .ip2(n129), .op(n153) );
  nand2_2 U64 ( .ip1(n157), .ip2(n158), .op(n156) );
  nor2_2 U65 ( .ip1(n714), .ip2(n155), .op(n160) );
  nand4_2 U66 ( .ip1(n718), .ip2(n161), .ip3(n162), .ip4(n163), .op(n159) );
  nor2_2 U67 ( .ip1(n107), .ip2(n120), .op(n165) );
  nor2_2 U69 ( .ip1(to_large), .ip2(to_small), .op(n119) );
  nand2_2 U70 ( .ip1(state[0]), .ip2(n158), .op(n162) );
  nand2_2 U71 ( .ip1(n166), .ip2(n167), .op(n158) );
  nand3_2 U72 ( .ip1(n122), .ip2(n715), .ip3(n739), .op(n167) );
  nand2_2 U73 ( .ip1(n168), .ip2(n169), .op(n123) );
  nand2_2 U74 ( .ip1(n170), .ip2(n171), .op(n169) );
  nor2_2 U75 ( .ip1(n132), .ip2(n121), .op(n122) );
  and3_2 U76 ( .ip1(pid_OUT), .ip2(n170), .ip3(buf0_na), .op(n173) );
  nand2_2 U77 ( .ip1(n697), .ip2(n175), .op(n172) );
  nor2_2 U78 ( .ip1(n756), .ip2(csr[22]), .op(n101) );
  nand2_2 U79 ( .ip1(n176), .ip2(n175), .op(n132) );
  nand2_2 U80 ( .ip1(pid_IN), .ip2(n170), .op(n175) );
  and2_2 U81 ( .ip1(n157), .ip2(n155), .op(n136) );
  or2_2 U82 ( .ip1(n178), .ip2(n733), .op(n155) );
  nand3_2 U83 ( .ip1(n181), .ip2(n720), .ip3(n182), .op(n179) );
  or2_2 U84 ( .ip1(idma_done), .ip2(n716), .op(n182) );
  or3_2 U85 ( .ip1(n707), .ip2(rx_data_done), .ip3(n183), .op(n181) );
  nor2_2 U86 ( .ip1(n66), .ip2(match), .op(n157) );
  nand3_2 U87 ( .ip1(n184), .ip2(n185), .ip3(n186), .op(n542) );
  and2_2 U88 ( .ip1(n190), .ip2(n191), .op(n189) );
  and2_2 U89 ( .ip1(n192), .ip2(n193), .op(n188) );
  nand2_2 U90 ( .ip1(setup_token), .ip2(n170), .op(n185) );
  nand2_2 U91 ( .ip1(next_dpid[1]), .ip2(n194), .op(n184) );
  nand4_2 U92 ( .ip1(n195), .ip2(n196), .ip3(n197), .ip4(n198), .op(n543) );
  and3_2 U93 ( .ip1(n202), .ip2(n736), .ip3(pid_MDATA), .op(n201) );
  nand2_2 U94 ( .ip1(n203), .ip2(n204), .op(n199) );
  nand2_2 U95 ( .ip1(n205), .ip2(n191), .op(n196) );
  nand2_2 U96 ( .ip1(next_dpid[0]), .ip2(n194), .op(n195) );
  and4_2 U97 ( .ip1(n206), .ip2(n207), .ip3(n208), .ip4(n209), .op(n194) );
  nand2_2 U98 ( .ip1(n212), .ip2(n738), .op(n208) );
  nand2_2 U99 ( .ip1(n213), .ip2(n214), .op(n212) );
  nand2_2 U100 ( .ip1(pid_DATA1), .ip2(n202), .op(n214) );
  nand2_2 U101 ( .ip1(pid_DATA2), .ip2(n743), .op(n213) );
  nand3_2 U102 ( .ip1(pid_MDATA), .ip2(n736), .ip3(n202), .op(n207) );
  nand2_2 U103 ( .ip1(n170), .ip2(n216), .op(n206) );
  nand2_2 U104 ( .ip1(n700), .ip2(n204), .op(n216) );
  nand2_2 U105 ( .ip1(n203), .ip2(n217), .op(n187) );
  and2_2 U106 ( .ip1(n218), .ip2(n219), .op(n203) );
  nand3_2 U107 ( .ip1(n29), .ip2(n746), .ip3(n221), .op(n219) );
  nand2_2 U108 ( .ip1(n222), .ip2(n747), .op(n218) );
  nand4_2 U109 ( .ip1(n223), .ip2(n224), .ip3(n225), .ip4(n226), .op(n544) );
  nand2_2 U110 ( .ip1(n227), .ip2(n193), .op(n226) );
  nand2_2 U111 ( .ip1(allow_pid[1]), .ip2(n228), .op(n225) );
  nand2_2 U112 ( .ip1(data_pid_sel[1]), .ip2(n229), .op(n224) );
  nand2_2 U113 ( .ip1(n230), .ip2(n231), .op(n545) );
  nor4_2 U114 ( .ip1(n200), .ip2(n202), .ip3(n190), .ip4(n232), .op(n231) );
  and2_2 U115 ( .ip1(n233), .ip2(n227), .op(n232) );
  nand2_2 U117 ( .ip1(n222), .ip2(csr[28]), .op(n217) );
  nand3_2 U118 ( .ip1(n221), .ip2(n29), .ip3(csr[29]), .op(n204) );
  nand2_2 U119 ( .ip1(n237), .ip2(n238), .op(n235) );
  nand2_2 U120 ( .ip1(data_pid_sel[0]), .ip2(n229), .op(n238) );
  and3_2 U121 ( .ip1(n223), .ip2(n701), .ip3(n239), .op(n229) );
  nor2_2 U122 ( .ip1(n168), .ip2(n241), .op(n211) );
  nand2_2 U123 ( .ip1(n699), .ip2(n242), .op(n240) );
  nand2_2 U124 ( .ip1(n221), .ip2(n29), .op(n242) );
  nor2_2 U125 ( .ip1(n30), .ip2(n221), .op(n222) );
  nand4_2 U126 ( .ip1(n197), .ip2(n237), .ip3(n243), .ip4(n244), .op(n210) );
  nand2_2 U127 ( .ip1(n749), .ip2(n245), .op(n244) );
  nand2_2 U128 ( .ip1(n234), .ip2(n241), .op(n245) );
  nand2_2 U129 ( .ip1(n246), .ip2(n193), .op(n243) );
  and2_2 U130 ( .ip1(n170), .ip2(setup_token), .op(n248) );
  nor2_2 U131 ( .ip1(csr[28]), .ip2(n249), .op(n247) );
  and2_2 U133 ( .ip1(n215), .ip2(n250), .op(n223) );
  nand2_2 U134 ( .ip1(n202), .ip2(n747), .op(n250) );
  nor2_2 U135 ( .ip1(n234), .ip2(n168), .op(n202) );
  or3_2 U136 ( .ip1(n753), .ip2(n742), .ip3(n251), .op(n234) );
  nor2_2 U137 ( .ip1(n205), .ip2(n190), .op(n215) );
  and2_2 U138 ( .ip1(n192), .ip2(n233), .op(n190) );
  and2_2 U139 ( .ip1(n246), .ip2(n233), .op(n205) );
  nor2_2 U141 ( .ip1(n753), .ip2(csr[28]), .op(n246) );
  and2_2 U142 ( .ip1(n253), .ip2(n254), .op(n237) );
  nand2_2 U143 ( .ip1(n193), .ip2(n192), .op(n254) );
  or2_2 U146 ( .ip1(n249), .ip2(n747), .op(n253) );
  nand2_2 U147 ( .ip1(n255), .ip2(n754), .op(n249) );
  nand2_2 U148 ( .ip1(n176), .ip2(n168), .op(n255) );
  nand2_2 U150 ( .ip1(n151), .ip2(n251), .op(n241) );
  nand2_2 U151 ( .ip1(csr[12]), .ip2(mode_hs), .op(n251) );
  nand2_2 U152 ( .ip1(csr[11]), .ip2(mode_hs), .op(n252) );
  nand2_2 U153 ( .ip1(n256), .ip2(n257), .op(n546) );
  nand2_2 U154 ( .ip1(pid_SETUP), .ip2(rst), .op(n257) );
  nand2_2 U155 ( .ip1(n258), .ip2(setup_token), .op(n256) );
  nand2_2 U156 ( .ip1(n259), .ip2(n260), .op(n547) );
  nand2_2 U157 ( .ip1(rst), .ip2(n171), .op(n260) );
  nand2_2 U158 ( .ip1(n258), .ip2(out_token), .op(n259) );
  nand2_2 U159 ( .ip1(n261), .ip2(n262), .op(n548) );
  nand2_2 U160 ( .ip1(rst), .ip2(pid_IN), .op(n262) );
  nand2_2 U161 ( .ip1(n258), .ip2(in_token), .op(n261) );
  and2_2 U162 ( .ip1(n263), .ip2(rst), .op(n258) );
  nand2_2 U163 ( .ip1(n264), .ip2(n265), .op(n549) );
  nand2_2 U164 ( .ip1(n266), .ip2(n702), .op(n265) );
  nand2_2 U165 ( .ip1(n267), .ip2(n268), .op(n266) );
  nand2_2 U166 ( .ip1(n703), .ip2(n740), .op(n268) );
  nand2_2 U167 ( .ip1(data_pid_sel[0]), .ip2(n736), .op(n267) );
  nand2_2 U168 ( .ip1(data_pid_sel[1]), .ip2(n269), .op(n264) );
  nand2_2 U169 ( .ip1(n270), .ip2(n271), .op(n269) );
  nand2_2 U170 ( .ip1(n703), .ip2(n734), .op(n271) );
  nand2_2 U171 ( .ip1(data_pid_sel[0]), .ip2(n738), .op(n270) );
  nor4_2 U172 ( .ip1(n272), .ip2(n273), .ip3(n274), .ip4(n275), .op(n550) );
  or3_2 U173 ( .ip1(new_size[10]), .ip2(new_size[11]), .ip3(new_size[0]), .op(
        n275) );
  or4_2 U174 ( .ip1(new_size[12]), .ip2(new_size[13]), .ip3(new_size[1]), 
        .ip4(new_size[2]), .op(n274) );
  or3_2 U175 ( .ip1(new_size[4]), .ip2(new_size[5]), .ip3(new_size[3]), .op(
        n273) );
  or4_2 U176 ( .ip1(new_size[6]), .ip2(new_size[7]), .ip3(new_size[8]), .ip4(
        new_size[9]), .op(n272) );
  nor2_2 U177 ( .ip1(state[6]), .ip2(n276), .op(int_to_set) );
  nor2_2 U178 ( .ip1(n277), .ip2(n164), .op(n276) );
  nor2_2 U179 ( .ip1(n717), .ip2(n725), .op(n164) );
  and2_2 U180 ( .ip1(n719), .ip2(tx_data_to), .op(n277) );
  and4_2 U181 ( .ip1(n720), .ip2(n144), .ip3(pid_seq_err), .ip4(rx_data_done), 
        .op(int_seqerr_set_d) );
  nor2_2 U183 ( .ip1(csr[25]), .ip2(n754), .op(n151) );
  and2_2 U185 ( .ip1(rx_data_done), .ip2(crc16_err), .op(int_crc16_set) );
  nor4_2 U186 ( .ip1(buf1_not_aloc), .ip2(n8), .ip3(n711), .ip4(n704), .op(
        int_buf1_set) );
  nor4_2 U187 ( .ip1(buf0_not_aloc), .ip2(n26), .ip3(n711), .ip4(n704), .op(
        int_buf0_set) );
  nor2_2 U188 ( .ip1(n711), .ip2(n279), .op(buf0_rl_d) );
  nor2_2 U190 ( .ip1(n171), .ip2(pid_IN), .op(n263) );
  or2_2 U191 ( .ip1(pid_SETUP), .ip2(pid_OUT), .op(n171) );
  or2_2 U192 ( .ip1(N90), .ip2(buf1[31]), .op(N88) );
  and4_2 U193 ( .ip1(n281), .ip2(n282), .ip3(n283), .ip4(n284), .op(N90) );
  and3_2 U194 ( .ip1(n285), .ip2(buf1[12]), .ip3(buf1[13]), .op(n284) );
  and3_2 U195 ( .ip1(buf1[10]), .ip2(buf1[0]), .ip3(buf1[11]), .op(n285) );
  and4_2 U196 ( .ip1(buf1[14]), .ip2(buf1[15]), .ip3(buf1[16]), .ip4(buf1[1]), 
        .op(n283) );
  and4_2 U197 ( .ip1(buf1[2]), .ip2(buf1[3]), .ip3(buf1[4]), .ip4(buf1[5]), 
        .op(n282) );
  and4_2 U198 ( .ip1(buf1[6]), .ip2(buf1[7]), .ip3(buf1[8]), .ip4(buf1[9]), 
        .op(n281) );
  or2_2 U199 ( .ip1(N89), .ip2(buf0[31]), .op(N87) );
  and4_2 U200 ( .ip1(n286), .ip2(n287), .ip3(n288), .ip4(n289), .op(N89) );
  and3_2 U201 ( .ip1(n290), .ip2(buf0[12]), .ip3(buf0[13]), .op(n289) );
  and3_2 U202 ( .ip1(buf0[10]), .ip2(buf0[0]), .ip3(buf0[11]), .op(n290) );
  and4_2 U203 ( .ip1(buf0[14]), .ip2(buf0[15]), .ip3(buf0[16]), .ip4(buf0[1]), 
        .op(n288) );
  and4_2 U204 ( .ip1(buf0[2]), .ip2(buf0[3]), .ip3(buf0[4]), .ip4(buf0[5]), 
        .op(n287) );
  and4_2 U205 ( .ip1(buf0[6]), .ip2(buf0[7]), .ip3(buf0[8]), .ip4(buf0[9]), 
        .op(n286) );
  or3_2 U206 ( .ip1(pid_SETUP_r), .ip2(pid_IN_r), .ip3(n752), .op(n293) );
  or2_2 U207 ( .ip1(n176), .ip2(pid_IN_r), .op(n291) );
  and2_2 U208 ( .ip1(N551), .ip2(n761), .op(N559) );
  and2_2 U209 ( .ip1(N550), .ip2(n761), .op(N558) );
  and2_2 U210 ( .ip1(N549), .ip2(n761), .op(N557) );
  and2_2 U211 ( .ip1(N548), .ip2(n761), .op(N556) );
  and2_2 U212 ( .ip1(N547), .ip2(n761), .op(N555) );
  and2_2 U213 ( .ip1(N546), .ip2(n761), .op(N554) );
  and2_2 U214 ( .ip1(N545), .ip2(n761), .op(N553) );
  and2_2 U215 ( .ip1(N544), .ip2(n761), .op(N552) );
  and2_2 U216 ( .ip1(N532), .ip2(n724), .op(N540) );
  and2_2 U217 ( .ip1(N531), .ip2(n724), .op(N539) );
  and2_2 U218 ( .ip1(N530), .ip2(n724), .op(N538) );
  and2_2 U219 ( .ip1(N529), .ip2(n724), .op(N537) );
  and2_2 U220 ( .ip1(N528), .ip2(n724), .op(N536) );
  and2_2 U221 ( .ip1(N527), .ip2(n724), .op(N535) );
  and2_2 U222 ( .ip1(N526), .ip2(n724), .op(N534) );
  and2_2 U223 ( .ip1(N525), .ip2(n724), .op(N533) );
  nand4_2 U224 ( .ip1(n294), .ip2(n161), .ip3(n295), .ip4(n296), .op(N523) );
  nor4_2 U225 ( .ip1(n297), .ip2(n177), .ip3(n708), .ip4(n719), .op(n296) );
  nand2_2 U226 ( .ip1(n298), .ip2(n713), .op(n297) );
  nand4_2 U228 ( .ip1(n716), .ip2(n183), .ip3(n300), .ip4(n301), .op(n166) );
  nor4_2 U229 ( .ip1(n152), .ip2(n177), .ip3(n299), .ip4(n710), .op(n301) );
  nor4_2 U230 ( .ip1(n730), .ip2(n302), .ip3(state[5]), .ip4(state[8]), .op(
        n299) );
  nor4_2 U232 ( .ip1(n726), .ip2(n303), .ip3(state[1]), .ip4(state[2]), .op(
        n152) );
  and3_2 U233 ( .ip1(n120), .ip2(n161), .ip3(n294), .op(n300) );
  nand3_2 U234 ( .ip1(n712), .ip2(n305), .ip3(state[5]), .op(n120) );
  nand3_2 U235 ( .ip1(state[4]), .ip2(n722), .ip3(n306), .op(n183) );
  nor4_2 U237 ( .ip1(n728), .ip2(n303), .ip3(state[1]), .ip4(state[3]), .op(
        n135) );
  or3_2 U238 ( .ip1(state[0]), .ip2(state[4]), .ip3(n307), .op(n303) );
  and2_2 U239 ( .ip1(match_r), .ip2(to_large), .op(n308) );
  nand2_2 U240 ( .ip1(n715), .ip2(n720), .op(n125) );
  nand3_2 U241 ( .ip1(n722), .ip2(n309), .ip3(state[0]), .op(n294) );
  nand3_2 U242 ( .ip1(n729), .ip2(n723), .ip3(n305), .op(n307) );
  nor2_2 U243 ( .ip1(n26), .ip2(n310), .op(N521) );
  nor2_2 U244 ( .ip1(n8), .ip2(n310), .op(N520) );
  nor2_2 U245 ( .ip1(n757), .ip2(n313), .op(n312) );
  and3_2 U246 ( .ip1(next_dpid[1]), .ip2(n31), .ip3(n310), .op(n311) );
  nor2_2 U247 ( .ip1(n758), .ip2(n313), .op(n315) );
  and3_2 U248 ( .ip1(next_dpid[0]), .ip2(n31), .ip3(n310), .op(n314) );
  nor2_2 U249 ( .ip1(n759), .ip2(n313), .op(n317) );
  nand3_2 U250 ( .ip1(csr[30]), .ip2(n744), .ip3(N498), .op(n318) );
  nor2_2 U251 ( .ip1(n760), .ip2(n313), .op(n321) );
  nand2_2 U252 ( .ip1(n39), .ip2(n310), .op(n313) );
  nand3_2 U253 ( .ip1(n704), .ip2(n32), .ip3(csr[30]), .op(n323) );
  nand2_2 U254 ( .ip1(N498), .ip2(n745), .op(n322) );
  nand2_2 U255 ( .ip1(n280), .ip2(n279), .op(n310) );
  nand2_2 U256 ( .ip1(buffer_done), .ip2(dma_en), .op(n279) );
  nor2_2 U257 ( .ip1(n298), .ip2(state[6]), .op(n280) );
  nand4_2 U258 ( .ip1(state[8]), .ip2(n712), .ip3(n729), .ip4(n730), .op(n298)
         );
  nand3_2 U259 ( .ip1(n714), .ip2(n723), .ip3(n309), .op(n302) );
  nand2_2 U260 ( .ip1(n324), .ip2(n325), .op(N511) );
  nand2_2 U261 ( .ip1(n40), .ip2(buf0[16]), .op(n325) );
  nand2_2 U262 ( .ip1(new_adr[16]), .ip2(n31), .op(n324) );
  nand2_2 U263 ( .ip1(n326), .ip2(n327), .op(N510) );
  nand2_2 U264 ( .ip1(n39), .ip2(buf0[15]), .op(n327) );
  nand2_2 U265 ( .ip1(new_adr[15]), .ip2(n31), .op(n326) );
  nand2_2 U266 ( .ip1(n328), .ip2(n329), .op(N509) );
  nand2_2 U267 ( .ip1(n40), .ip2(buf0[14]), .op(n329) );
  nand2_2 U268 ( .ip1(new_adr[14]), .ip2(n31), .op(n328) );
  nand2_2 U269 ( .ip1(n330), .ip2(n331), .op(N508) );
  nand2_2 U270 ( .ip1(n41), .ip2(buf0[13]), .op(n331) );
  nand2_2 U271 ( .ip1(new_adr[13]), .ip2(n32), .op(n330) );
  nand2_2 U272 ( .ip1(n332), .ip2(n333), .op(N507) );
  nand2_2 U273 ( .ip1(n43), .ip2(buf0[12]), .op(n333) );
  nand2_2 U274 ( .ip1(new_adr[12]), .ip2(n31), .op(n332) );
  nand2_2 U275 ( .ip1(n334), .ip2(n335), .op(N506) );
  nand2_2 U276 ( .ip1(n41), .ip2(buf0[11]), .op(n335) );
  nand2_2 U277 ( .ip1(new_adr[11]), .ip2(n31), .op(n334) );
  nand2_2 U278 ( .ip1(n336), .ip2(n337), .op(N505) );
  nand2_2 U279 ( .ip1(n42), .ip2(buf0[10]), .op(n337) );
  nand2_2 U280 ( .ip1(new_adr[10]), .ip2(n31), .op(n336) );
  nand2_2 U281 ( .ip1(n338), .ip2(n339), .op(N504) );
  nand2_2 U282 ( .ip1(n42), .ip2(buf0[9]), .op(n339) );
  nand2_2 U283 ( .ip1(new_adr[9]), .ip2(n31), .op(n338) );
  nand2_2 U284 ( .ip1(n340), .ip2(n341), .op(N503) );
  nand2_2 U285 ( .ip1(n43), .ip2(buf0[8]), .op(n341) );
  nand2_2 U286 ( .ip1(new_adr[8]), .ip2(n31), .op(n340) );
  nand2_2 U287 ( .ip1(n342), .ip2(n343), .op(N502) );
  nand2_2 U288 ( .ip1(n44), .ip2(buf0[7]), .op(n343) );
  nand2_2 U289 ( .ip1(new_adr[7]), .ip2(n31), .op(n342) );
  nand2_2 U290 ( .ip1(n344), .ip2(n345), .op(N501) );
  nand2_2 U291 ( .ip1(n44), .ip2(buf0[6]), .op(n345) );
  nand2_2 U292 ( .ip1(new_adr[6]), .ip2(n31), .op(n344) );
  nand2_2 U293 ( .ip1(n346), .ip2(n347), .op(N500) );
  nand2_2 U294 ( .ip1(n45), .ip2(buf0[5]), .op(n347) );
  nand2_2 U295 ( .ip1(new_adr[5]), .ip2(n31), .op(n346) );
  nand2_2 U296 ( .ip1(n348), .ip2(n349), .op(N499) );
  nand2_2 U297 ( .ip1(n45), .ip2(buf0[4]), .op(n349) );
  nand2_2 U298 ( .ip1(new_adr[4]), .ip2(n32), .op(n348) );
  nor2_2 U299 ( .ip1(n704), .ip2(n38), .op(N498) );
  and2_2 U300 ( .ip1(n31), .ip2(new_size[13]), .op(N497) );
  and2_2 U301 ( .ip1(n32), .ip2(new_size[12]), .op(N496) );
  and2_2 U302 ( .ip1(n32), .ip2(new_size[11]), .op(N495) );
  nand2_2 U303 ( .ip1(n350), .ip2(n351), .op(N494) );
  nand2_2 U304 ( .ip1(new_size[10]), .ip2(n32), .op(n351) );
  nand2_2 U305 ( .ip1(sizu_c[10]), .ip2(n38), .op(n350) );
  nand2_2 U306 ( .ip1(n352), .ip2(n353), .op(N493) );
  nand2_2 U307 ( .ip1(new_size[9]), .ip2(n32), .op(n353) );
  nand2_2 U308 ( .ip1(sizu_c[9]), .ip2(n37), .op(n352) );
  nand2_2 U309 ( .ip1(n354), .ip2(n355), .op(N492) );
  nand2_2 U310 ( .ip1(new_size[8]), .ip2(n32), .op(n355) );
  nand2_2 U311 ( .ip1(sizu_c[8]), .ip2(n37), .op(n354) );
  nand2_2 U312 ( .ip1(n356), .ip2(n357), .op(N491) );
  nand2_2 U313 ( .ip1(new_size[7]), .ip2(n32), .op(n357) );
  nand2_2 U314 ( .ip1(sizu_c[7]), .ip2(n33), .op(n356) );
  nand2_2 U315 ( .ip1(n358), .ip2(n359), .op(N490) );
  nand2_2 U316 ( .ip1(new_size[6]), .ip2(n32), .op(n359) );
  nand2_2 U317 ( .ip1(sizu_c[6]), .ip2(n35), .op(n358) );
  nand2_2 U318 ( .ip1(n360), .ip2(n361), .op(N489) );
  nand2_2 U319 ( .ip1(new_size[5]), .ip2(n32), .op(n361) );
  nand2_2 U320 ( .ip1(sizu_c[5]), .ip2(n36), .op(n360) );
  nand2_2 U321 ( .ip1(n362), .ip2(n363), .op(N488) );
  nand2_2 U322 ( .ip1(new_size[4]), .ip2(n32), .op(n363) );
  nand2_2 U323 ( .ip1(sizu_c[4]), .ip2(n36), .op(n362) );
  nand2_2 U324 ( .ip1(n364), .ip2(n365), .op(N487) );
  nand2_2 U325 ( .ip1(new_size[3]), .ip2(n32), .op(n365) );
  nand2_2 U326 ( .ip1(sizu_c[3]), .ip2(n34), .op(n364) );
  nand2_2 U327 ( .ip1(n366), .ip2(n367), .op(N486) );
  nand2_2 U328 ( .ip1(new_size[2]), .ip2(n32), .op(n367) );
  nand2_2 U329 ( .ip1(sizu_c[2]), .ip2(n35), .op(n366) );
  nand2_2 U330 ( .ip1(n368), .ip2(n369), .op(N485) );
  nand2_2 U331 ( .ip1(new_size[1]), .ip2(n32), .op(n369) );
  nand2_2 U332 ( .ip1(sizu_c[1]), .ip2(n34), .op(n368) );
  nand2_2 U333 ( .ip1(n370), .ip2(n371), .op(N484) );
  nand2_2 U334 ( .ip1(new_size[0]), .ip2(n31), .op(n371) );
  nand2_2 U335 ( .ip1(sizu_c[0]), .ip2(n33), .op(n370) );
  nor2_2 U336 ( .ip1(csr[17]), .ip2(n731), .op(N475) );
  nor2_2 U337 ( .ip1(csr[16]), .ip2(n732), .op(N473) );
  and3_2 U338 ( .ip1(uc_stat_set_d), .ip2(n1), .ip3(N470), .op(N471) );
  nor2_2 U339 ( .ip1(n161), .ip2(state[6]), .op(uc_stat_set_d) );
  nand3_2 U340 ( .ip1(state[9]), .ip2(n309), .ip3(n373), .op(n161) );
  and3_2 U341 ( .ip1(n305), .ip2(n729), .ip3(n714), .op(n373) );
  nor2_2 U342 ( .ip1(state[8]), .ip2(state[7]), .op(n305) );
  nand2_2 U344 ( .ip1(n726), .ip2(n728), .op(n304) );
  and2_2 U345 ( .ip1(rx_data_valid), .ip2(N468), .op(N469) );
  and2_2 U346 ( .ip1(size_next_r[13]), .ip2(n29), .op(N465) );
  and2_2 U347 ( .ip1(size_next_r[12]), .ip2(n29), .op(N464) );
  and2_2 U348 ( .ip1(size_next_r[11]), .ip2(n29), .op(N463) );
  nand2_2 U349 ( .ip1(n374), .ip2(n375), .op(N462) );
  nand2_2 U350 ( .ip1(size_next_r[10]), .ip2(n29), .op(n375) );
  nand2_2 U351 ( .ip1(n376), .ip2(n377), .op(N461) );
  nand2_2 U352 ( .ip1(size_next_r[9]), .ip2(n30), .op(n377) );
  nand2_2 U353 ( .ip1(n378), .ip2(n379), .op(N460) );
  nand2_2 U354 ( .ip1(size_next_r[8]), .ip2(n30), .op(n379) );
  nand2_2 U355 ( .ip1(n380), .ip2(n381), .op(N459) );
  nand2_2 U356 ( .ip1(size_next_r[7]), .ip2(n30), .op(n381) );
  nand2_2 U357 ( .ip1(n382), .ip2(n383), .op(N458) );
  nand2_2 U358 ( .ip1(size_next_r[6]), .ip2(n30), .op(n383) );
  nand2_2 U359 ( .ip1(n384), .ip2(n385), .op(N457) );
  nand2_2 U360 ( .ip1(size_next_r[5]), .ip2(n30), .op(n385) );
  nand2_2 U361 ( .ip1(n386), .ip2(n387), .op(N456) );
  nand2_2 U362 ( .ip1(size_next_r[4]), .ip2(n30), .op(n387) );
  nand2_2 U363 ( .ip1(n388), .ip2(n389), .op(N455) );
  nand2_2 U364 ( .ip1(size_next_r[3]), .ip2(n30), .op(n389) );
  nand2_2 U365 ( .ip1(n390), .ip2(n391), .op(N454) );
  nand2_2 U366 ( .ip1(size_next_r[2]), .ip2(n29), .op(n391) );
  nand2_2 U367 ( .ip1(n392), .ip2(n393), .op(N453) );
  nand2_2 U368 ( .ip1(size_next_r[1]), .ip2(n30), .op(n393) );
  nand2_2 U369 ( .ip1(n394), .ip2(n395), .op(N452) );
  nand2_2 U370 ( .ip1(size_next_r[0]), .ip2(n30), .op(n395) );
  nor2_2 U371 ( .ip1(n2), .ip2(n109), .op(N434) );
  nand2_2 U372 ( .ip1(buf_smaller), .ip2(buf_size[13]), .op(n109) );
  nand2_2 U373 ( .ip1(n396), .ip2(n397), .op(buf_size[13]) );
  nand2_2 U374 ( .ip1(buf0[30]), .ip2(n9), .op(n397) );
  nand2_2 U375 ( .ip1(buf1[30]), .ip2(n6), .op(n396) );
  nor2_2 U376 ( .ip1(n2), .ip2(n110), .op(N433) );
  nand2_2 U377 ( .ip1(buf_smaller), .ip2(buf_size[12]), .op(n110) );
  nand2_2 U378 ( .ip1(n398), .ip2(n399), .op(buf_size[12]) );
  nand2_2 U379 ( .ip1(buf0[29]), .ip2(n9), .op(n399) );
  nand2_2 U380 ( .ip1(buf1[29]), .ip2(n6), .op(n398) );
  nor2_2 U381 ( .ip1(n2), .ip2(n111), .op(N432) );
  nand2_2 U382 ( .ip1(buf_smaller), .ip2(buf_size[11]), .op(n111) );
  nand2_2 U383 ( .ip1(n400), .ip2(n401), .op(buf_size[11]) );
  nand2_2 U384 ( .ip1(buf0[28]), .ip2(n11), .op(n401) );
  nand2_2 U385 ( .ip1(buf1[28]), .ip2(n6), .op(n400) );
  nand2_2 U386 ( .ip1(n374), .ip2(n402), .op(N431) );
  nand2_2 U387 ( .ip1(n29), .ip2(size[10]), .op(n402) );
  nand2_2 U388 ( .ip1(n403), .ip2(n404), .op(size[10]) );
  nand2_2 U389 ( .ip1(buf_smaller), .ip2(buf_size[10]), .op(n404) );
  nand2_2 U390 ( .ip1(n405), .ip2(n406), .op(buf_size[10]) );
  nand2_2 U391 ( .ip1(buf0[27]), .ip2(n11), .op(n406) );
  nand2_2 U392 ( .ip1(buf1[27]), .ip2(n6), .op(n405) );
  nand2_2 U393 ( .ip1(csr[10]), .ip2(n3), .op(n403) );
  and2_2 U394 ( .ip1(n407), .ip2(n408), .op(n374) );
  nand2_2 U395 ( .ip1(n409), .ip2(sizu_c[10]), .op(n408) );
  nand2_2 U396 ( .ip1(n1), .ip2(csr[10]), .op(n407) );
  nand2_2 U397 ( .ip1(n376), .ip2(n410), .op(N430) );
  nand2_2 U398 ( .ip1(n29), .ip2(size[9]), .op(n410) );
  nand2_2 U399 ( .ip1(n411), .ip2(n412), .op(size[9]) );
  nand2_2 U400 ( .ip1(buf_smaller), .ip2(buf_size[9]), .op(n412) );
  nand2_2 U401 ( .ip1(n413), .ip2(n414), .op(buf_size[9]) );
  nand2_2 U402 ( .ip1(buf0[26]), .ip2(n12), .op(n414) );
  nand2_2 U403 ( .ip1(buf1[26]), .ip2(n6), .op(n413) );
  nand2_2 U404 ( .ip1(csr[9]), .ip2(n3), .op(n411) );
  and2_2 U405 ( .ip1(n415), .ip2(n416), .op(n376) );
  nand2_2 U406 ( .ip1(n409), .ip2(sizu_c[9]), .op(n416) );
  nand2_2 U407 ( .ip1(n1), .ip2(csr[9]), .op(n415) );
  nand2_2 U408 ( .ip1(n378), .ip2(n417), .op(N429) );
  nand2_2 U409 ( .ip1(n29), .ip2(size[8]), .op(n417) );
  nand2_2 U410 ( .ip1(n418), .ip2(n419), .op(size[8]) );
  nand2_2 U411 ( .ip1(buf_smaller), .ip2(buf_size[8]), .op(n419) );
  nand2_2 U412 ( .ip1(n420), .ip2(n421), .op(buf_size[8]) );
  nand2_2 U413 ( .ip1(buf0[25]), .ip2(n18), .op(n421) );
  nand2_2 U414 ( .ip1(buf1[25]), .ip2(n6), .op(n420) );
  nand2_2 U415 ( .ip1(csr[8]), .ip2(n3), .op(n418) );
  and2_2 U416 ( .ip1(n422), .ip2(n423), .op(n378) );
  nand2_2 U417 ( .ip1(n409), .ip2(sizu_c[8]), .op(n423) );
  nand2_2 U418 ( .ip1(n1), .ip2(csr[8]), .op(n422) );
  nand2_2 U419 ( .ip1(n380), .ip2(n424), .op(N428) );
  nand2_2 U420 ( .ip1(n29), .ip2(size[7]), .op(n424) );
  nand2_2 U421 ( .ip1(n425), .ip2(n426), .op(size[7]) );
  nand2_2 U422 ( .ip1(buf_smaller), .ip2(buf_size[7]), .op(n426) );
  nand2_2 U423 ( .ip1(n427), .ip2(n428), .op(buf_size[7]) );
  nand2_2 U424 ( .ip1(buf0[24]), .ip2(n12), .op(n428) );
  nand2_2 U425 ( .ip1(buf1[24]), .ip2(n7), .op(n427) );
  nand2_2 U426 ( .ip1(csr[7]), .ip2(n3), .op(n425) );
  and2_2 U427 ( .ip1(n429), .ip2(n430), .op(n380) );
  nand2_2 U428 ( .ip1(n409), .ip2(sizu_c[7]), .op(n430) );
  nand2_2 U429 ( .ip1(n1), .ip2(csr[7]), .op(n429) );
  nand2_2 U430 ( .ip1(n382), .ip2(n431), .op(N427) );
  nand2_2 U431 ( .ip1(n29), .ip2(size[6]), .op(n431) );
  nand2_2 U432 ( .ip1(n432), .ip2(n433), .op(size[6]) );
  nand2_2 U433 ( .ip1(buf_smaller), .ip2(buf_size[6]), .op(n433) );
  nand2_2 U434 ( .ip1(n434), .ip2(n435), .op(buf_size[6]) );
  nand2_2 U435 ( .ip1(buf0[23]), .ip2(n13), .op(n435) );
  nand2_2 U436 ( .ip1(buf1[23]), .ip2(n6), .op(n434) );
  nand2_2 U437 ( .ip1(csr[6]), .ip2(n3), .op(n432) );
  and2_2 U438 ( .ip1(n436), .ip2(n437), .op(n382) );
  nand2_2 U439 ( .ip1(n409), .ip2(sizu_c[6]), .op(n437) );
  nand2_2 U440 ( .ip1(n1), .ip2(csr[6]), .op(n436) );
  nand2_2 U441 ( .ip1(n384), .ip2(n438), .op(N426) );
  nand2_2 U442 ( .ip1(n29), .ip2(size[5]), .op(n438) );
  nand2_2 U443 ( .ip1(n439), .ip2(n440), .op(size[5]) );
  nand2_2 U444 ( .ip1(buf_smaller), .ip2(buf_size[5]), .op(n440) );
  nand2_2 U445 ( .ip1(n441), .ip2(n442), .op(buf_size[5]) );
  nand2_2 U446 ( .ip1(buf0[22]), .ip2(n13), .op(n442) );
  nand2_2 U447 ( .ip1(buf1[22]), .ip2(n6), .op(n441) );
  nand2_2 U448 ( .ip1(csr[5]), .ip2(n3), .op(n439) );
  and2_2 U449 ( .ip1(n443), .ip2(n444), .op(n384) );
  nand2_2 U450 ( .ip1(n409), .ip2(sizu_c[5]), .op(n444) );
  nand2_2 U451 ( .ip1(n1), .ip2(csr[5]), .op(n443) );
  nand2_2 U452 ( .ip1(n386), .ip2(n445), .op(N425) );
  nand2_2 U453 ( .ip1(n29), .ip2(size[4]), .op(n445) );
  nand2_2 U454 ( .ip1(n446), .ip2(n447), .op(size[4]) );
  nand2_2 U455 ( .ip1(buf_smaller), .ip2(buf_size[4]), .op(n447) );
  nand2_2 U456 ( .ip1(n448), .ip2(n449), .op(buf_size[4]) );
  nand2_2 U457 ( .ip1(buf0[21]), .ip2(n14), .op(n449) );
  nand2_2 U458 ( .ip1(buf1[21]), .ip2(n6), .op(n448) );
  nand2_2 U459 ( .ip1(csr[4]), .ip2(n3), .op(n446) );
  and2_2 U460 ( .ip1(n450), .ip2(n451), .op(n386) );
  nand2_2 U461 ( .ip1(n409), .ip2(sizu_c[4]), .op(n451) );
  nand2_2 U462 ( .ip1(n1), .ip2(csr[4]), .op(n450) );
  nand2_2 U463 ( .ip1(n388), .ip2(n452), .op(N424) );
  nand2_2 U464 ( .ip1(n30), .ip2(size[3]), .op(n452) );
  nand2_2 U465 ( .ip1(n453), .ip2(n454), .op(size[3]) );
  nand2_2 U466 ( .ip1(buf_smaller), .ip2(buf_size[3]), .op(n454) );
  nand2_2 U467 ( .ip1(n455), .ip2(n456), .op(buf_size[3]) );
  nand2_2 U468 ( .ip1(buf0[20]), .ip2(n14), .op(n456) );
  nand2_2 U469 ( .ip1(buf1[20]), .ip2(n6), .op(n455) );
  nand2_2 U470 ( .ip1(csr[3]), .ip2(n3), .op(n453) );
  and2_2 U471 ( .ip1(n457), .ip2(n458), .op(n388) );
  nand2_2 U472 ( .ip1(n409), .ip2(sizu_c[3]), .op(n458) );
  nand2_2 U473 ( .ip1(n1), .ip2(csr[3]), .op(n457) );
  nand2_2 U474 ( .ip1(n390), .ip2(n459), .op(N423) );
  nand2_2 U475 ( .ip1(n30), .ip2(size[2]), .op(n459) );
  nand2_2 U476 ( .ip1(n460), .ip2(n461), .op(size[2]) );
  nand2_2 U477 ( .ip1(buf_smaller), .ip2(buf_size[2]), .op(n461) );
  nand2_2 U478 ( .ip1(n462), .ip2(n463), .op(buf_size[2]) );
  nand2_2 U479 ( .ip1(buf0[19]), .ip2(n15), .op(n463) );
  nand2_2 U480 ( .ip1(buf1[19]), .ip2(n6), .op(n462) );
  nand2_2 U481 ( .ip1(csr[2]), .ip2(n3), .op(n460) );
  and2_2 U482 ( .ip1(n464), .ip2(n465), .op(n390) );
  nand2_2 U483 ( .ip1(n409), .ip2(sizu_c[2]), .op(n465) );
  nand2_2 U484 ( .ip1(n1), .ip2(csr[2]), .op(n464) );
  nand2_2 U485 ( .ip1(n392), .ip2(n466), .op(N422) );
  nand2_2 U486 ( .ip1(n30), .ip2(size[1]), .op(n466) );
  nand2_2 U487 ( .ip1(n467), .ip2(n468), .op(size[1]) );
  nand2_2 U488 ( .ip1(buf_smaller), .ip2(buf_size[1]), .op(n468) );
  nand2_2 U489 ( .ip1(n469), .ip2(n470), .op(buf_size[1]) );
  nand2_2 U490 ( .ip1(buf0[18]), .ip2(n15), .op(n470) );
  nand2_2 U491 ( .ip1(buf1[18]), .ip2(n7), .op(n469) );
  nand2_2 U492 ( .ip1(csr[1]), .ip2(n3), .op(n467) );
  and2_2 U493 ( .ip1(n471), .ip2(n472), .op(n392) );
  nand2_2 U494 ( .ip1(n409), .ip2(sizu_c[1]), .op(n472) );
  nand2_2 U495 ( .ip1(n1), .ip2(csr[1]), .op(n471) );
  nand2_2 U496 ( .ip1(n394), .ip2(n473), .op(N421) );
  nand2_2 U497 ( .ip1(n30), .ip2(size[0]), .op(n473) );
  nand2_2 U498 ( .ip1(n474), .ip2(n475), .op(size[0]) );
  nand2_2 U499 ( .ip1(buf_smaller), .ip2(buf_size[0]), .op(n475) );
  nand2_2 U500 ( .ip1(n476), .ip2(n477), .op(buf_size[0]) );
  nand2_2 U501 ( .ip1(buf0[17]), .ip2(n16), .op(n477) );
  nand2_2 U502 ( .ip1(buf1[17]), .ip2(n7), .op(n476) );
  nand2_2 U503 ( .ip1(csr[0]), .ip2(n3), .op(n474) );
  and2_2 U504 ( .ip1(n478), .ip2(n479), .op(n394) );
  nand2_2 U505 ( .ip1(n409), .ip2(sizu_c[0]), .op(n479) );
  nand2_2 U507 ( .ip1(n1), .ip2(csr[0]), .op(n478) );
  and2_2 U509 ( .ip1(n168), .ip2(n481), .op(n221) );
  nand2_2 U510 ( .ip1(out_token), .ip2(n170), .op(n481) );
  and2_2 U511 ( .ip1(n8), .ip2(buffer_full), .op(n482) );
  nand4_2 U512 ( .ip1(n483), .ip2(n484), .ip3(n174), .ip4(n697), .op(N402) );
  nand2_2 U513 ( .ip1(dma_en), .ip2(n485), .op(n174) );
  or2_2 U514 ( .ip1(n486), .ip2(n487), .op(n485) );
  nor2_2 U515 ( .ip1(dma_in_buf_sz1), .ip2(n176), .op(n487) );
  nor2_2 U516 ( .ip1(dma_out_buf_avail), .ip2(n168), .op(n486) );
  nand3_2 U518 ( .ip1(n25), .ip2(n480), .ip3(buffer_full), .op(n484) );
  nand2_2 U519 ( .ip1(buf0_st_max), .ip2(n7), .op(n483) );
  nand2_2 U520 ( .ip1(n488), .ip2(n489), .op(N397) );
  nand2_2 U521 ( .ip1(buffer_full), .ip2(n2), .op(n489) );
  nand2_2 U522 ( .ip1(buffer_empty), .ip2(n29), .op(n488) );
  nand2_2 U524 ( .ip1(in_token), .ip2(n170), .op(n490) );
  nand2_2 U525 ( .ip1(csr[26]), .ip2(n750), .op(n176) );
  nand2_2 U526 ( .ip1(n491), .ip2(n492), .op(N394) );
  nand2_2 U527 ( .ip1(buf0[16]), .ip2(n16), .op(n492) );
  nand2_2 U528 ( .ip1(buf1[16]), .ip2(n7), .op(n491) );
  nand2_2 U529 ( .ip1(n493), .ip2(n494), .op(N393) );
  nand2_2 U530 ( .ip1(buf0[15]), .ip2(n17), .op(n494) );
  nand2_2 U531 ( .ip1(buf1[15]), .ip2(n7), .op(n493) );
  nand2_2 U532 ( .ip1(n495), .ip2(n496), .op(N392) );
  nand2_2 U533 ( .ip1(buf0[14]), .ip2(n17), .op(n496) );
  nand2_2 U534 ( .ip1(buf1[14]), .ip2(n7), .op(n495) );
  nand2_2 U535 ( .ip1(n497), .ip2(n498), .op(N391) );
  nand2_2 U536 ( .ip1(buf0[13]), .ip2(n18), .op(n498) );
  nand2_2 U537 ( .ip1(buf1[13]), .ip2(n7), .op(n497) );
  nand2_2 U538 ( .ip1(n499), .ip2(n500), .op(N390) );
  nand2_2 U539 ( .ip1(buf0[12]), .ip2(n19), .op(n500) );
  nand2_2 U540 ( .ip1(buf1[12]), .ip2(n7), .op(n499) );
  nand2_2 U541 ( .ip1(n501), .ip2(n502), .op(N389) );
  nand2_2 U542 ( .ip1(buf0[11]), .ip2(n19), .op(n502) );
  nand2_2 U543 ( .ip1(buf1[11]), .ip2(n7), .op(n501) );
  nand2_2 U544 ( .ip1(n503), .ip2(n504), .op(N388) );
  nand2_2 U545 ( .ip1(buf0[10]), .ip2(n20), .op(n504) );
  nand2_2 U546 ( .ip1(buf1[10]), .ip2(n7), .op(n503) );
  nand2_2 U547 ( .ip1(n505), .ip2(n506), .op(N387) );
  nand2_2 U548 ( .ip1(buf0[9]), .ip2(n20), .op(n506) );
  nand2_2 U549 ( .ip1(buf1[9]), .ip2(n8), .op(n505) );
  nand2_2 U550 ( .ip1(n507), .ip2(n508), .op(N386) );
  nand2_2 U551 ( .ip1(buf0[8]), .ip2(n21), .op(n508) );
  nand2_2 U552 ( .ip1(buf1[8]), .ip2(n8), .op(n507) );
  nand2_2 U553 ( .ip1(n509), .ip2(n510), .op(N385) );
  nand2_2 U554 ( .ip1(buf0[7]), .ip2(n21), .op(n510) );
  nand2_2 U555 ( .ip1(buf1[7]), .ip2(n8), .op(n509) );
  nand2_2 U556 ( .ip1(n511), .ip2(n512), .op(N384) );
  nand2_2 U557 ( .ip1(buf0[6]), .ip2(n22), .op(n512) );
  nand2_2 U558 ( .ip1(buf1[6]), .ip2(n8), .op(n511) );
  nand2_2 U559 ( .ip1(n513), .ip2(n514), .op(N383) );
  nand2_2 U560 ( .ip1(buf0[5]), .ip2(n22), .op(n514) );
  nand2_2 U561 ( .ip1(buf1[5]), .ip2(n8), .op(n513) );
  nand2_2 U562 ( .ip1(n515), .ip2(n516), .op(N382) );
  nand2_2 U563 ( .ip1(buf0[4]), .ip2(n23), .op(n516) );
  nand2_2 U564 ( .ip1(buf1[4]), .ip2(n8), .op(n515) );
  nand2_2 U565 ( .ip1(n517), .ip2(n518), .op(N381) );
  nand2_2 U566 ( .ip1(buf0[3]), .ip2(n23), .op(n518) );
  nand2_2 U567 ( .ip1(buf1[3]), .ip2(n8), .op(n517) );
  nand2_2 U568 ( .ip1(n519), .ip2(n520), .op(N380) );
  nand2_2 U569 ( .ip1(buf0[2]), .ip2(n24), .op(n520) );
  nand2_2 U570 ( .ip1(buf1[2]), .ip2(n8), .op(n519) );
  nand2_2 U571 ( .ip1(n521), .ip2(n522), .op(N379) );
  nand2_2 U572 ( .ip1(buf0[1]), .ip2(n24), .op(n522) );
  nand2_2 U573 ( .ip1(buf1[1]), .ip2(n7), .op(n521) );
  nand2_2 U574 ( .ip1(n523), .ip2(n524), .op(N378) );
  nand2_2 U575 ( .ip1(buf0[0]), .ip2(n25), .op(n524) );
  nand2_2 U576 ( .ip1(buf1[0]), .ip2(n6), .op(n523) );
  nor2_2 U577 ( .ip1(dma_en), .ip2(n525), .op(n278) );
  nor2_2 U578 ( .ip1(n526), .ip2(n527), .op(n525) );
  and2_2 U579 ( .ip1(n170), .ip2(in_token), .op(n527) );
  nand2_2 U580 ( .ip1(csr[15]), .ip2(n752), .op(n480) );
  nand2_2 U582 ( .ip1(n528), .ip2(n529), .op(N249) );
  nand3_2 U583 ( .ip1(n529), .ip2(n735), .ip3(n530), .op(N248) );
  nand4_2 U584 ( .ip1(pid_DATA0), .ip2(n736), .ip3(n734), .ip4(n738), .op(n530) );
  nand2_2 U585 ( .ip1(n528), .ip2(n531), .op(N250) );
  nand4_2 U586 ( .ip1(pid_DATA2), .ip2(n740), .ip3(n736), .ip4(n738), .op(n531) );
  nand3_2 U587 ( .ip1(n740), .ip2(n736), .ip3(n191), .op(n528) );
  nor2_2 U588 ( .ip1(n738), .ip2(pid_DATA2), .op(n191) );
  nand4_2 U589 ( .ip1(pid_DATA1), .ip2(n740), .ip3(n734), .ip4(n738), .op(n529) );
  inv_2 U633 ( .ip(rst), .op(n66) );
  ab_or_c_or_d U665 ( .ip1(n136), .ip2(n159), .ip3(n160), .ip4(n733), .op(n541) );
  not_ab_or_c_or_d U666 ( .ip1(n719), .ip2(n707), .ip3(n164), .ip4(n165), .op(
        n163) );
  ab_or_c_or_d U667 ( .ip1(pid_PING), .ip2(mode_hs), .ip3(n101), .ip4(n105), 
        .op(n121) );
  ab_or_c_or_d U668 ( .ip1(buf1_na), .ip2(n172), .ip3(n748), .ip4(n173), .op(
        n105) );
  not_ab_or_c_or_d U669 ( .ip1(n715), .ip2(n124), .ip3(n179), .ip4(n180), .op(
        n178) );
  not_ab_or_c_or_d U670 ( .ip1(token_valid), .ip2(pid_ACK), .ip3(n717), .ip4(
        rx_ack_to), .op(n180) );
  ab_or_c_or_d U671 ( .ip1(csr[22]), .ip2(n756), .ip3(pid_SOF), .ip4(n698), 
        .op(n124) );
  not_ab_or_c_or_d U672 ( .ip1(n170), .ip2(n187), .ip3(n188), .ip4(n189), .op(
        n186) );
  not_ab_or_c_or_d U673 ( .ip1(n170), .ip2(n199), .ip3(n200), .ip4(n201), .op(
        n198) );
  not_ab_or_c_or_d U674 ( .ip1(n191), .ip2(n743), .ip3(n210), .ip4(n211), .op(
        n209) );
  not_ab_or_c_or_d U675 ( .ip1(allow_pid[0]), .ip2(n228), .ip3(n235), .ip4(
        n236), .op(n230) );
  not_ab_or_c_or_d U676 ( .ip1(n204), .ip2(n217), .ip3(n752), .ip4(setup_token), .op(n236) );
  not_ab_or_c_or_d U677 ( .ip1(n170), .ip2(n240), .ip3(n202), .ip4(n211), .op(
        n239) );
  not_ab_or_c_or_d U678 ( .ip1(n227), .ip2(n193), .ip3(n247), .ip4(n248), .op(
        n197) );
  not_ab_or_c_or_d U679 ( .ip1(n291), .ip2(n292), .ip3(n698), .ip4(pid_SOF), 
        .op(N561) );
  ab_or_c_or_d U680 ( .ip1(n168), .ip2(n293), .ip3(pid_PING_r), .ip4(pid_OUT_r), .op(n292) );
  ab_or_c_or_d U681 ( .ip1(match), .ip2(n125), .ip3(buffer_overflow), .ip4(
        n308), .op(N522) );
  ab_or_c_or_d U682 ( .ip1(new_adr[3]), .ip2(n705), .ip3(n311), .ip4(n312), 
        .op(N517) );
  ab_or_c_or_d U683 ( .ip1(new_adr[2]), .ip2(n705), .ip3(n314), .ip4(n315), 
        .op(N516) );
  ab_or_c_or_d U684 ( .ip1(new_adr[1]), .ip2(n705), .ip3(n316), .ip4(n317), 
        .op(N515) );
  not_ab_or_c_or_d U685 ( .ip1(n318), .ip2(n319), .ip3(dma_en), .ip4(n705), 
        .op(n316) );
  ab_or_c_or_d U686 ( .ip1(csr[30]), .ip2(buffer_done), .ip3(n46), .ip4(n744), 
        .op(n319) );
  ab_or_c_or_d U687 ( .ip1(new_adr[0]), .ip2(n705), .ip3(n320), .ip4(n321), 
        .op(N514) );
  not_ab_or_c_or_d U688 ( .ip1(n322), .ip2(n323), .ip3(dma_en), .ip4(n705), 
        .op(n320) );
  ab_or_c_or_d U689 ( .ip1(buf1_st_max), .ip2(n27), .ip3(buf1_na), .ip4(n482), 
        .op(N404) );
  not_ab_or_c_or_d U690 ( .ip1(n745), .ip2(n697), .ip3(buf1_na), .ip4(n170), 
        .op(n526) );
  usbf_pe_SSRAM_HADR14_DW01_add_0 add_4376 ( .A(adr_r), .B({1'b0, 1'b0, 1'b0, 
        N465, N464, N463, N462, N461, N460, N459, N458, N457, N456, N455, N454, 
        N453, N452}), .CI(1'b0), .SUM(new_adr) );
  usbf_pe_SSRAM_HADR14_DW01_sub_0 sub_4366 ( .A(buf_size), .B(new_sizeb), .CI(
        1'b0), .DIFF({N448, N447, N446, N445, N444, N443, N442, N441, N440, 
        N439, N438, N437, N436, N435}) );
  usbf_pe_SSRAM_HADR14_DW01_cmp6_2 r164 ( .A(sizu_c), .B(csr[10:0]), .TC(1'b0), 
        .LT(N472), .GT(N474), .NE(N470) );
  dp_1 buf1_st_max_reg ( .ip(N399), .ck(n54), .q(buf1_st_max) );
  dp_1 pid_OUT_r_reg ( .ip(pid_OUT), .ck(n60), .q(pid_OUT_r) );
  dp_1 buffer_overflow_reg ( .ip(N469), .ck(n59), .q(buffer_overflow) );
  dp_1 pid_PING_r_reg ( .ip(pid_PING), .ck(n60), .q(pid_PING_r) );
  dp_1 pid_SETUP_r_reg ( .ip(pid_SETUP), .ck(n60), .q(pid_SETUP_r) );
  dp_1 buf0_not_aloc_reg ( .ip(N89), .ck(n52), .q(buf0_not_aloc) );
  dp_1 buf1_not_aloc_reg ( .ip(N90), .ck(n52), .q(buf1_not_aloc) );
  dp_1 buf0_st_max_reg ( .ip(N398), .ck(n54), .q(buf0_st_max) );
  dp_1 buffer_empty_reg ( .ip(n550), .ck(n56), .q(buffer_empty) );
  dp_1 \size_next_r_reg[10]  ( .ip(size[10]), .ck(n58), .q(size_next_r[10]) );
  dp_1 \size_next_r_reg[9]  ( .ip(size[9]), .ck(n58), .q(size_next_r[9]) );
  dp_1 \size_next_r_reg[8]  ( .ip(size[8]), .ck(n58), .q(size_next_r[8]) );
  dp_1 \size_next_r_reg[7]  ( .ip(size[7]), .ck(n58), .q(size_next_r[7]) );
  dp_1 \size_next_r_reg[6]  ( .ip(size[6]), .ck(n58), .q(size_next_r[6]) );
  dp_1 \size_next_r_reg[5]  ( .ip(size[5]), .ck(n59), .q(size_next_r[5]) );
  dp_1 \size_next_r_reg[4]  ( .ip(size[4]), .ck(n59), .q(size_next_r[4]) );
  dp_1 \size_next_r_reg[3]  ( .ip(size[3]), .ck(n59), .q(size_next_r[3]) );
  dp_1 \size_next_r_reg[2]  ( .ip(size[2]), .ck(n59), .q(size_next_r[2]) );
  dp_1 \size_next_r_reg[1]  ( .ip(size[1]), .ck(n59), .q(size_next_r[1]) );
  dp_1 \size_next_r_reg[0]  ( .ip(size[0]), .ck(n59), .q(size_next_r[0]) );
  dp_1 to_small_reg ( .ip(N473), .ck(n59), .q(to_small) );
  dp_1 no_bufs1_reg ( .ip(N404), .ck(n56), .q(no_bufs1) );
  dp_1 \new_sizeb_reg[13]  ( .ip(N434), .ck(n54), .q(new_sizeb[13]) );
  dp_1 \new_sizeb_reg[12]  ( .ip(N433), .ck(n54), .q(new_sizeb[12]) );
  dp_1 \new_sizeb_reg[11]  ( .ip(N432), .ck(n54), .q(new_sizeb[11]) );
  dp_1 \new_sizeb_reg[10]  ( .ip(N431), .ck(n54), .q(new_sizeb[10]) );
  dp_1 \new_sizeb_reg[9]  ( .ip(N430), .ck(n54), .q(new_sizeb[9]) );
  dp_1 \new_sizeb_reg[8]  ( .ip(N429), .ck(n54), .q(new_sizeb[8]) );
  dp_1 \new_sizeb_reg[7]  ( .ip(N428), .ck(n55), .q(new_sizeb[7]) );
  dp_1 \new_sizeb_reg[6]  ( .ip(N427), .ck(n55), .q(new_sizeb[6]) );
  dp_1 \new_sizeb_reg[5]  ( .ip(N426), .ck(n55), .q(new_sizeb[5]) );
  dp_1 \new_sizeb_reg[4]  ( .ip(N425), .ck(n55), .q(new_sizeb[4]) );
  dp_1 \new_sizeb_reg[3]  ( .ip(N424), .ck(n55), .q(new_sizeb[3]) );
  dp_1 \new_sizeb_reg[2]  ( .ip(N423), .ck(n55), .q(new_sizeb[2]) );
  dp_1 \new_sizeb_reg[1]  ( .ip(N422), .ck(n55), .q(new_sizeb[1]) );
  dp_1 \new_sizeb_reg[0]  ( .ip(N421), .ck(n55), .q(new_sizeb[0]) );
  dp_1 rx_ack_to_clr_reg ( .ip(N523), .ck(n61), .q(rx_ack_to_clr) );
  dp_1 \size_next_r_reg[13]  ( .ip(size[13]), .ck(n58), .q(size_next_r[13]) );
  dp_1 \size_next_r_reg[12]  ( .ip(size[12]), .ck(n58), .q(size_next_r[12]) );
  dp_1 \size_next_r_reg[11]  ( .ip(size[11]), .ck(n58), .q(size_next_r[11]) );
  dp_1 no_bufs0_reg ( .ip(N402), .ck(n56), .q(no_bufs0) );
  lp_1 \allow_pid_reg[0]  ( .ck(N248), .ip(N249), .q(allow_pid[0]) );
  dp_1 \tx_data_to_cnt_reg[7]  ( .ip(N559), .ck(n60), .q(tx_data_to_cnt[7]) );
  dp_1 \rx_ack_to_cnt_reg[7]  ( .ip(N540), .ck(n61), .q(rx_ack_to_cnt[7]) );
  dp_1 buf1_na_reg ( .ip(N88), .ck(n52), .q(buf1_na) );
  dp_1 \adr_reg[16]  ( .ip(N394), .ck(n53), .q(adr[16]) );
  dp_1 \adr_reg[15]  ( .ip(N393), .ck(n53), .q(adr[15]) );
  lp_1 \allow_pid_reg[1]  ( .ck(N248), .ip(N250), .q(allow_pid[1]) );
  dp_1 \tx_data_to_cnt_reg[3]  ( .ip(N555), .ck(n59), .q(tx_data_to_cnt[3]) );
  dp_1 \rx_ack_to_cnt_reg[3]  ( .ip(N536), .ck(n61), .q(rx_ack_to_cnt[3]) );
  dp_1 rx_ack_to_reg ( .ip(N541), .ck(n61), .q(rx_ack_to) );
  dp_1 \tx_data_to_cnt_reg[6]  ( .ip(N558), .ck(n60), .q(tx_data_to_cnt[6]) );
  dp_1 \rx_ack_to_cnt_reg[6]  ( .ip(N539), .ck(n61), .q(rx_ack_to_cnt[6]) );
  dp_1 pid_IN_r_reg ( .ip(pid_IN), .ck(n60), .q(pid_IN_r) );
  dp_1 \adr_r_reg[13]  ( .ip(adr[13]), .ck(n57), .q(adr_r[13]) );
  dp_1 \adr_r_reg[12]  ( .ip(adr[12]), .ck(n57), .q(adr_r[12]) );
  dp_1 \adr_r_reg[11]  ( .ip(adr[11]), .ck(n57), .q(adr_r[11]) );
  dp_1 \adr_r_reg[10]  ( .ip(adr[10]), .ck(n57), .q(adr_r[10]) );
  dp_1 \adr_r_reg[9]  ( .ip(adr[9]), .ck(n57), .q(adr_r[9]) );
  dp_1 \adr_r_reg[8]  ( .ip(adr[8]), .ck(n57), .q(adr_r[8]) );
  dp_1 \adr_r_reg[7]  ( .ip(adr[7]), .ck(n57), .q(adr_r[7]) );
  dp_1 \adr_r_reg[6]  ( .ip(adr[6]), .ck(n57), .q(adr_r[6]) );
  dp_1 \adr_r_reg[5]  ( .ip(adr[5]), .ck(n57), .q(adr_r[5]) );
  dp_1 \adr_r_reg[4]  ( .ip(adr[4]), .ck(n58), .q(adr_r[4]) );
  dp_1 \adr_r_reg[3]  ( .ip(adr[3]), .ck(n58), .q(adr_r[3]) );
  dp_1 \adr_r_reg[2]  ( .ip(adr[2]), .ck(n58), .q(adr_r[2]) );
  dp_1 \adr_r_reg[1]  ( .ip(adr[1]), .ck(n58), .q(adr_r[1]) );
  dp_1 \adr_r_reg[16]  ( .ip(adr[16]), .ck(n57), .q(adr_r[16]) );
  dp_1 \token_pid_sel_reg[0]  ( .ip(token_pid_sel_d[0]), .ck(n62), .q(
        token_pid_sel[0]) );
  dp_1 \tx_data_to_cnt_reg[2]  ( .ip(N554), .ck(n59), .q(tx_data_to_cnt[2]) );
  dp_1 \rx_ack_to_cnt_reg[2]  ( .ip(N535), .ck(n61), .q(rx_ack_to_cnt[2]) );
  dp_1 to_large_reg ( .ip(N475), .ck(n59), .q(to_large) );
  dp_1 out_token_reg ( .ip(n547), .ck(n52), .q(out_token) );
  dp_1 match_r_reg ( .ip(match), .ck(n52), .q(match_r) );
  dp_1 \next_dpid_reg[1]  ( .ip(n542), .ck(n52), .q(next_dpid[1]) );
  dp_1 \next_dpid_reg[0]  ( .ip(n543), .ck(n52), .q(next_dpid[0]) );
  dp_1 buf0_na_reg ( .ip(N87), .ck(n52), .q(buf0_na) );
  dp_1 \idin_reg[3]  ( .ip(N517), .ck(n63), .q(idin[3]) );
  dp_1 \idin_reg[2]  ( .ip(N516), .ck(n63), .q(idin[2]) );
  dp_1 buf0_rl_reg ( .ip(buf0_rl_d), .ck(n62), .q(buf0_rl) );
  dp_1 \idin_reg[1]  ( .ip(N515), .ck(n64), .q(idin[1]) );
  dp_1 \idin_reg[0]  ( .ip(N514), .ck(n64), .q(idin[0]) );
  dp_1 \tx_data_to_cnt_reg[5]  ( .ip(N557), .ck(n60), .q(tx_data_to_cnt[5]) );
  dp_1 \rx_ack_to_cnt_reg[5]  ( .ip(N538), .ck(n61), .q(rx_ack_to_cnt[5]) );
  dp_1 \new_size_reg[0]  ( .ip(N435), .ck(n56), .q(new_size[0]) );
  dp_1 \new_size_reg[11]  ( .ip(N446), .ck(n55), .q(new_size[11]) );
  dp_1 \new_size_reg[12]  ( .ip(N447), .ck(n55), .q(new_size[12]) );
  dp_1 \new_size_reg[13]  ( .ip(N448), .ck(n55), .q(new_size[13]) );
  dp_1 buffer_done_reg ( .ip(N397), .ck(n57), .q(buffer_done) );
  dp_1 \adr_r_reg[0]  ( .ip(adr[0]), .ck(n58), .q(adr_r[0]) );
  dp_1 \adr_r_reg[15]  ( .ip(adr[15]), .ck(n57), .q(adr_r[15]) );
  dp_1 \adr_r_reg[14]  ( .ip(adr[14]), .ck(n57), .q(adr_r[14]) );
  dp_1 \new_size_reg[6]  ( .ip(N441), .ck(n56), .q(new_size[6]) );
  dp_1 \new_size_reg[8]  ( .ip(N443), .ck(n56), .q(new_size[8]) );
  dp_1 \new_size_reg[10]  ( .ip(N445), .ck(n55), .q(new_size[10]) );
  dp_1 \new_size_reg[4]  ( .ip(N439), .ck(n56), .q(new_size[4]) );
  dp_1 \new_size_reg[2]  ( .ip(N437), .ck(n56), .q(new_size[2]) );
  dp_1 \new_size_reg[5]  ( .ip(N440), .ck(n56), .q(new_size[5]) );
  dp_1 \new_size_reg[7]  ( .ip(N442), .ck(n56), .q(new_size[7]) );
  dp_1 \tx_data_to_cnt_reg[1]  ( .ip(N553), .ck(n59), .q(tx_data_to_cnt[1]) );
  dp_1 \rx_ack_to_cnt_reg[1]  ( .ip(N534), .ck(n61), .q(rx_ack_to_cnt[1]) );
  dp_1 \new_size_reg[1]  ( .ip(N436), .ck(n56), .q(new_size[1]) );
  dp_1 \new_size_reg[3]  ( .ip(N438), .ck(n56), .q(new_size[3]) );
  dp_1 \state_reg[3]  ( .ip(n534), .ck(n61), .q(state[3]) );
  dp_1 \state_reg[2]  ( .ip(n533), .ck(n62), .q(state[2]) );
  dp_1 \new_size_reg[9]  ( .ip(N444), .ck(n55), .q(new_size[9]) );
  dp_1 \state_reg[7]  ( .ip(n538), .ck(n62), .q(state[7]) );
  dp_1 tx_data_to_reg ( .ip(N560), .ck(n60), .q(tx_data_to) );
  dp_1 \state_reg[9]  ( .ip(n540), .ck(n61), .q(state[9]) );
  dp_1 \tx_data_to_cnt_reg[4]  ( .ip(N556), .ck(n60), .q(tx_data_to_cnt[4]) );
  dp_1 \rx_ack_to_cnt_reg[4]  ( .ip(N537), .ck(n61), .q(rx_ack_to_cnt[4]) );
  dp_1 pid_seq_err_reg ( .ip(n549), .ck(n53), .q(pid_seq_err) );
  dp_1 in_token_reg ( .ip(n548), .ck(n52), .q(in_token) );
  dp_1 buffer_full_reg ( .ip(N396), .ck(n56), .q(buffer_full) );
  dp_1 \adr_reg[13]  ( .ip(N391), .ck(n53), .q(adr[13]) );
  dp_1 \adr_reg[12]  ( .ip(N390), .ck(n53), .q(adr[12]) );
  dp_1 \adr_reg[11]  ( .ip(N389), .ck(n53), .q(adr[11]) );
  dp_1 \adr_reg[10]  ( .ip(N388), .ck(n53), .q(adr[10]) );
  dp_1 \adr_reg[9]  ( .ip(N387), .ck(n53), .q(adr[9]) );
  dp_1 \adr_reg[8]  ( .ip(N386), .ck(n53), .q(adr[8]) );
  dp_1 \adr_reg[7]  ( .ip(N385), .ck(n53), .q(adr[7]) );
  dp_1 \adr_reg[6]  ( .ip(N384), .ck(n53), .q(adr[6]) );
  dp_1 \adr_reg[5]  ( .ip(N383), .ck(n53), .q(adr[5]) );
  dp_1 \adr_reg[4]  ( .ip(N382), .ck(n54), .q(adr[4]) );
  dp_1 \adr_reg[3]  ( .ip(N381), .ck(n54), .q(adr[3]) );
  dp_1 \adr_reg[1]  ( .ip(N379), .ck(n54), .q(adr[1]) );
  dp_1 \adr_reg[14]  ( .ip(N392), .ck(n53), .q(adr[14]) );
  dp_1 setup_token_reg ( .ip(n546), .ck(n52), .q(setup_token) );
  dp_1 \token_pid_sel_reg[1]  ( .ip(token_pid_sel_d[1]), .ck(n62), .q(
        token_pid_sel[1]) );
  dp_1 send_token_reg ( .ip(send_token_d), .ck(n62), .q(send_token) );
  dp_1 buf1_set_reg ( .ip(N521), .ck(n62), .q(buf1_set) );
  dp_1 \tx_data_to_cnt_reg[0]  ( .ip(N552), .ck(n59), .q(tx_data_to_cnt[0]) );
  dp_1 \rx_ack_to_cnt_reg[0]  ( .ip(N533), .ck(n61), .q(rx_ack_to_cnt[0]) );
  dp_1 \state_reg[8]  ( .ip(n539), .ck(n61), .q(state[8]) );
  dp_1 uc_dpd_set_reg ( .ip(uc_stat_set_d), .ck(n63), .q(uc_dpd_set) );
  dp_1 int_upid_set_reg ( .ip(N561), .ck(n60), .q(int_upid_set) );
  dp_1 int_seqerr_set_reg ( .ip(int_seqerr_set_d), .ck(n62), .q(int_seqerr_set) );
  dp_1 uc_bsel_set_reg ( .ip(uc_stat_set_d), .ck(n62), .q(uc_bsel_set) );
  dp_1 \state_reg[5]  ( .ip(n536), .ck(n62), .q(state[5]) );
  dp_1 \adr_reg[0]  ( .ip(N378), .ck(n54), .q(adr[0]) );
  dp_1 \adr_reg[2]  ( .ip(N380), .ck(n54), .q(adr[2]) );
  dp_1 \state_reg[4]  ( .ip(n535), .ck(n62), .q(state[4]) );
  dp_1 \this_dpid_reg[1]  ( .ip(n544), .ck(n52), .q(data_pid_sel[1]) );
  dp_1 out_to_small_r_reg ( .ip(N471), .ck(n63), .q(out_to_small_r) );
  dp_1 \this_dpid_reg[0]  ( .ip(n545), .ck(n52), .q(data_pid_sel[0]) );
  dp_1 \state_reg[0]  ( .ip(n541), .ck(n60), .q(state[0]) );
  dp_1 \idin_reg[25]  ( .ip(N492), .ck(n65), .q(idin[25]) );
  dp_1 \idin_reg[24]  ( .ip(N491), .ck(n65), .q(idin[24]) );
  dp_1 \idin_reg[23]  ( .ip(N490), .ck(n65), .q(idin[23]) );
  dp_1 \idin_reg[22]  ( .ip(N489), .ck(n65), .q(idin[22]) );
  dp_1 \idin_reg[21]  ( .ip(N488), .ck(n65), .q(idin[21]) );
  dp_1 \idin_reg[20]  ( .ip(N487), .ck(n65), .q(idin[20]) );
  dp_1 \idin_reg[19]  ( .ip(N486), .ck(n65), .q(idin[19]) );
  dp_1 \idin_reg[18]  ( .ip(N485), .ck(n65), .q(idin[18]) );
  dp_1 \idin_reg[17]  ( .ip(N484), .ck(n65), .q(idin[17]) );
  dp_1 \state_reg[1]  ( .ip(n532), .ck(n62), .q(state[1]) );
  dp_1 buf0_set_reg ( .ip(N520), .ck(n62), .q(buf0_set) );
  dp_1 \state_reg[6]  ( .ip(n537), .ck(n60), .q(state[6]) );
  dp_1 \idin_reg[30]  ( .ip(N497), .ck(n63), .q(idin[30]) );
  dp_1 \idin_reg[29]  ( .ip(N496), .ck(n63), .q(idin[29]) );
  dp_1 \idin_reg[28]  ( .ip(N495), .ck(n63), .q(idin[28]) );
  dp_1 \idin_reg[16]  ( .ip(N511), .ck(n63), .q(idin[16]) );
  dp_1 \idin_reg[15]  ( .ip(N510), .ck(n63), .q(idin[15]) );
  dp_1 \idin_reg[14]  ( .ip(N509), .ck(n63), .q(idin[14]) );
  dp_1 \idin_reg[13]  ( .ip(N508), .ck(n63), .q(idin[13]) );
  dp_1 \idin_reg[12]  ( .ip(N507), .ck(n63), .q(idin[12]) );
  dp_1 \idin_reg[11]  ( .ip(N506), .ck(n64), .q(idin[11]) );
  dp_1 \idin_reg[10]  ( .ip(N505), .ck(n64), .q(idin[10]) );
  dp_1 \idin_reg[9]  ( .ip(N504), .ck(n64), .q(idin[9]) );
  dp_1 \idin_reg[8]  ( .ip(N503), .ck(n64), .q(idin[8]) );
  dp_1 \idin_reg[7]  ( .ip(N502), .ck(n64), .q(idin[7]) );
  dp_1 \idin_reg[6]  ( .ip(N501), .ck(n64), .q(idin[6]) );
  dp_1 \idin_reg[5]  ( .ip(N500), .ck(n64), .q(idin[5]) );
  dp_1 \idin_reg[4]  ( .ip(N499), .ck(n64), .q(idin[4]) );
  dp_1 \idin_reg[31]  ( .ip(N498), .ck(n64), .q(idin[31]) );
  dp_1 \idin_reg[27]  ( .ip(N494), .ck(n64), .q(idin[27]) );
  dp_1 \idin_reg[26]  ( .ip(N493), .ck(n64), .q(idin[26]) );
  dp_1 out_to_small_reg ( .ip(n45), .ck(n63), .q(out_to_small) );
  dp_1 abort_reg ( .ip(N522), .ck(n60), .q(abort) );
  dp_1 nse_err_reg ( .ip(N91), .ck(n52), .q(nse_err) );
  nor2_2 U9 ( .ip1(n480), .ip2(n221), .op(n1) );
  inv_2 U10 ( .ip(n27), .op(n6) );
  inv_2 U11 ( .ip(n27), .op(n7) );
  inv_2 U14 ( .ip(n27), .op(n8) );
  inv_2 U15 ( .ip(buf_smaller), .op(n3) );
  nor2_2 U16 ( .ip1(n30), .ip2(n1), .op(n409) );
  nor3_2 U17 ( .ip1(n252), .ip2(n176), .ip3(n251), .op(n193) );
  nor3_2 U63 ( .ip1(n252), .ip2(n168), .ip3(n251), .op(n233) );
  inv_2 U68 ( .ip(n46), .op(n32) );
  inv_2 U116 ( .ip(n46), .op(n31) );
  nor4_2 U132 ( .ip1(buf_size[13]), .ip2(buf_size[12]), .ip3(buf_size[11]), 
        .ip4(n558), .op(buf_smaller) );
  nand2_2 U140 ( .ip1(n155), .ip2(n156), .op(n129) );
  inv_2 U144 ( .ip(n2), .op(n29) );
  nor3_2 U145 ( .ip1(n183), .ip2(n707), .ip3(n753), .op(n144) );
  nor3_2 U149 ( .ip1(n168), .ip2(n742), .ip3(n241), .op(n228) );
  nor2_2 U182 ( .ip1(csr[27]), .ip2(csr[26]), .op(n170) );
  nor3_2 U184 ( .ip1(n304), .ip2(state[1]), .ip3(state[0]), .op(n306) );
  nor3_2 U189 ( .ip1(abort), .ip2(pid_seq_err), .ip3(n706), .op(n107) );
  nor3_2 U227 ( .ip1(n753), .ip2(csr[29]), .ip3(n747), .op(n192) );
  nor3_2 U231 ( .ip1(state[1]), .ip2(state[4]), .ip3(n304), .op(n309) );
  nor3_2 U236 ( .ip1(n303), .ip2(n304), .ip3(n727), .op(n177) );
  nor3_2 U343 ( .ip1(csr[28]), .ip2(csr[29]), .ip3(n753), .op(n227) );
  nor3_2 U506 ( .ip1(crc16_err), .ip2(tx_data_to), .ip3(abort), .op(n141) );
  or2_2 U508 ( .ip1(n750), .ip2(csr[26]), .op(n168) );
  nor3_2 U517 ( .ip1(n176), .ip2(csr[28]), .ip3(n234), .op(n200) );
  and2_2 U523 ( .ip1(n176), .ip2(n490), .op(n2) );
  nor3_2 U581 ( .ip1(n709), .ip2(tx_valid), .ip3(state[6]), .op(n295) );
  nor3_2 U590 ( .ip1(n741), .ip2(match), .ip3(n263), .op(N91) );
  buf_1 U591 ( .ip(n14), .op(n9) );
  buf_1 U592 ( .ip(n15), .op(n11) );
  buf_1 U593 ( .ip(n16), .op(n12) );
  buf_1 U594 ( .ip(n22), .op(n13) );
  buf_1 U595 ( .ip(n23), .op(n14) );
  buf_1 U596 ( .ip(n24), .op(n15) );
  buf_1 U597 ( .ip(n28), .op(n16) );
  buf_1 U598 ( .ip(n25), .op(n17) );
  buf_1 U599 ( .ip(n21), .op(n18) );
  buf_1 U600 ( .ip(n28), .op(n19) );
  buf_1 U601 ( .ip(n26), .op(n20) );
  buf_1 U602 ( .ip(n20), .op(n21) );
  buf_1 U603 ( .ip(n28), .op(n22) );
  buf_1 U604 ( .ip(n28), .op(n23) );
  buf_1 U605 ( .ip(n28), .op(n24) );
  buf_1 U606 ( .ip(n13), .op(n25) );
  buf_1 U607 ( .ip(n19), .op(n26) );
  buf_1 U608 ( .ip(n12), .op(n27) );
  inv_1 U609 ( .ip(n278), .op(n28) );
  inv_1 U610 ( .ip(n2), .op(n30) );
  buf_1 U611 ( .ip(out_to_small_r), .op(n33) );
  buf_1 U612 ( .ip(out_to_small_r), .op(n34) );
  buf_1 U613 ( .ip(out_to_small_r), .op(n35) );
  buf_1 U614 ( .ip(out_to_small_r), .op(n36) );
  buf_1 U615 ( .ip(out_to_small_r), .op(n37) );
  buf_1 U616 ( .ip(out_to_small_r), .op(n38) );
  buf_1 U617 ( .ip(out_to_small_r), .op(n39) );
  buf_1 U618 ( .ip(out_to_small_r), .op(n40) );
  buf_1 U619 ( .ip(out_to_small_r), .op(n41) );
  buf_1 U620 ( .ip(out_to_small_r), .op(n42) );
  buf_1 U621 ( .ip(out_to_small_r), .op(n43) );
  buf_1 U622 ( .ip(out_to_small_r), .op(n44) );
  buf_1 U623 ( .ip(out_to_small_r), .op(n45) );
  buf_1 U624 ( .ip(out_to_small_r), .op(n46) );
  buf_1 U625 ( .ip(clk), .op(n47) );
  buf_1 U626 ( .ip(clk), .op(n48) );
  buf_1 U627 ( .ip(clk), .op(n49) );
  buf_1 U628 ( .ip(clk), .op(n50) );
  buf_1 U629 ( .ip(clk), .op(n51) );
  buf_1 U630 ( .ip(n47), .op(n52) );
  buf_1 U631 ( .ip(n47), .op(n53) );
  buf_1 U632 ( .ip(n47), .op(n54) );
  buf_1 U634 ( .ip(n48), .op(n55) );
  buf_1 U635 ( .ip(n48), .op(n56) );
  buf_1 U636 ( .ip(n48), .op(n57) );
  buf_1 U637 ( .ip(n49), .op(n58) );
  buf_1 U638 ( .ip(n49), .op(n59) );
  buf_1 U639 ( .ip(n49), .op(n60) );
  buf_1 U640 ( .ip(n50), .op(n61) );
  buf_1 U641 ( .ip(n50), .op(n62) );
  buf_1 U642 ( .ip(n50), .op(n63) );
  buf_1 U643 ( .ip(n51), .op(n64) );
  buf_1 U644 ( .ip(n51), .op(n65) );
  xor2_1 U645 ( .ip1(rx_ack_to_cnt[1]), .ip2(rx_ack_to_cnt[0]), .op(N526) );
  nand2_1 U646 ( .ip1(rx_ack_to_cnt[1]), .ip2(rx_ack_to_cnt[0]), .op(n67) );
  xnor2_1 U647 ( .ip1(n67), .ip2(rx_ack_to_cnt[2]), .op(N527) );
  nand3_1 U648 ( .ip1(rx_ack_to_cnt[1]), .ip2(rx_ack_to_cnt[0]), .ip3(
        rx_ack_to_cnt[2]), .op(n69) );
  xor2_1 U649 ( .ip1(n69), .ip2(n75), .op(N528) );
  nor2_1 U650 ( .ip1(n69), .ip2(n75), .op(n68) );
  xor2_1 U651 ( .ip1(rx_ack_to_cnt[4]), .ip2(n68), .op(N529) );
  nor3_1 U652 ( .ip1(n75), .ip2(n69), .ip3(n74), .op(n70) );
  xor2_1 U653 ( .ip1(rx_ack_to_cnt[5]), .ip2(n70), .op(N530) );
  nand2_1 U654 ( .ip1(rx_ack_to_cnt[5]), .ip2(n70), .op(n71) );
  xor2_1 U655 ( .ip1(n71), .ip2(n73), .op(N531) );
  nor2_1 U656 ( .ip1(n71), .ip2(n73), .op(n72) );
  xor2_1 U657 ( .ip1(rx_ack_to_cnt[7]), .ip2(n72), .op(N532) );
  inv_2 U658 ( .ip(rx_ack_to_cnt[6]), .op(n73) );
  inv_2 U659 ( .ip(rx_ack_to_cnt[4]), .op(n74) );
  inv_2 U660 ( .ip(rx_ack_to_cnt[3]), .op(n75) );
  inv_2 U661 ( .ip(rx_ack_to_cnt[0]), .op(N525) );
  xor2_1 U662 ( .ip1(tx_data_to_cnt[1]), .ip2(tx_data_to_cnt[0]), .op(N545) );
  nand2_1 U663 ( .ip1(tx_data_to_cnt[1]), .ip2(tx_data_to_cnt[0]), .op(n76) );
  xnor2_1 U664 ( .ip1(n76), .ip2(tx_data_to_cnt[2]), .op(N546) );
  nand3_1 U691 ( .ip1(tx_data_to_cnt[1]), .ip2(tx_data_to_cnt[0]), .ip3(
        tx_data_to_cnt[2]), .op(n78) );
  xor2_1 U692 ( .ip1(n78), .ip2(n84), .op(N547) );
  nor2_1 U693 ( .ip1(n78), .ip2(n84), .op(n77) );
  xor2_1 U694 ( .ip1(tx_data_to_cnt[4]), .ip2(n77), .op(N548) );
  nor3_1 U695 ( .ip1(n84), .ip2(n78), .ip3(n83), .op(n79) );
  xor2_1 U696 ( .ip1(tx_data_to_cnt[5]), .ip2(n79), .op(N549) );
  nand2_1 U697 ( .ip1(tx_data_to_cnt[5]), .ip2(n79), .op(n80) );
  xor2_1 U698 ( .ip1(n80), .ip2(n82), .op(N550) );
  nor2_1 U699 ( .ip1(n80), .ip2(n82), .op(n81) );
  xor2_1 U700 ( .ip1(tx_data_to_cnt[7]), .ip2(n81), .op(N551) );
  inv_2 U701 ( .ip(tx_data_to_cnt[6]), .op(n82) );
  inv_2 U702 ( .ip(tx_data_to_cnt[4]), .op(n83) );
  inv_2 U703 ( .ip(tx_data_to_cnt[3]), .op(n84) );
  inv_2 U704 ( .ip(tx_data_to_cnt[0]), .op(N544) );
  nor2_1 U705 ( .ip1(buf_size[10]), .ip2(n564), .op(n557) );
  nor2_1 U706 ( .ip1(buf_size[8]), .ip2(n563), .op(n553) );
  nor2_1 U707 ( .ip1(buf_size[6]), .ip2(n562), .op(n220) );
  nor2_1 U708 ( .ip1(buf_size[4]), .ip2(n561), .op(n94) );
  nor2_1 U709 ( .ip1(buf_size[2]), .ip2(n560), .op(n90) );
  nor2_1 U710 ( .ip1(n559), .ip2(buf_size[0]), .op(n86) );
  and2_1 U711 ( .ip1(n565), .ip2(n86), .op(n85) );
  nor2_1 U712 ( .ip1(csr[1]), .ip2(n85), .op(n88) );
  nor2_1 U713 ( .ip1(n86), .ip2(n565), .op(n87) );
  not_ab_or_c_or_d U714 ( .ip1(buf_size[2]), .ip2(n560), .ip3(n88), .ip4(n87), 
        .op(n89) );
  not_ab_or_c_or_d U715 ( .ip1(csr[3]), .ip2(n566), .ip3(n90), .ip4(n89), .op(
        n92) );
  nor2_1 U716 ( .ip1(csr[3]), .ip2(n566), .op(n91) );
  not_ab_or_c_or_d U717 ( .ip1(buf_size[4]), .ip2(n561), .ip3(n92), .ip4(n91), 
        .op(n93) );
  not_ab_or_c_or_d U718 ( .ip1(csr[5]), .ip2(n567), .ip3(n94), .ip4(n93), .op(
        n96) );
  nor2_1 U719 ( .ip1(csr[5]), .ip2(n567), .op(n95) );
  not_ab_or_c_or_d U720 ( .ip1(buf_size[6]), .ip2(n562), .ip3(n96), .ip4(n95), 
        .op(n97) );
  not_ab_or_c_or_d U721 ( .ip1(csr[7]), .ip2(n568), .ip3(n220), .ip4(n97), 
        .op(n551) );
  nor2_1 U722 ( .ip1(csr[7]), .ip2(n568), .op(n372) );
  not_ab_or_c_or_d U723 ( .ip1(buf_size[8]), .ip2(n563), .ip3(n551), .ip4(n372), .op(n552) );
  not_ab_or_c_or_d U724 ( .ip1(csr[9]), .ip2(n569), .ip3(n553), .ip4(n552), 
        .op(n555) );
  nor2_1 U725 ( .ip1(csr[9]), .ip2(n569), .op(n554) );
  not_ab_or_c_or_d U726 ( .ip1(buf_size[10]), .ip2(n564), .ip3(n555), .ip4(
        n554), .op(n556) );
  nor2_1 U727 ( .ip1(n557), .ip2(n556), .op(n558) );
  inv_2 U728 ( .ip(csr[0]), .op(n559) );
  inv_2 U729 ( .ip(csr[2]), .op(n560) );
  inv_2 U730 ( .ip(csr[4]), .op(n561) );
  inv_2 U731 ( .ip(csr[6]), .op(n562) );
  inv_2 U732 ( .ip(csr[8]), .op(n563) );
  inv_2 U733 ( .ip(csr[10]), .op(n564) );
  inv_2 U734 ( .ip(buf_size[1]), .op(n565) );
  inv_2 U735 ( .ip(buf_size[3]), .op(n566) );
  inv_2 U736 ( .ip(buf_size[5]), .op(n567) );
  inv_2 U737 ( .ip(buf_size[7]), .op(n568) );
  inv_2 U738 ( .ip(buf_size[9]), .op(n569) );
  nor2_1 U739 ( .ip1(new_size[10]), .ip2(n564), .op(n591) );
  nor2_1 U740 ( .ip1(new_size[8]), .ip2(n563), .op(n587) );
  nor2_1 U741 ( .ip1(new_size[6]), .ip2(n562), .op(n583) );
  nor2_1 U742 ( .ip1(new_size[4]), .ip2(n561), .op(n579) );
  nor2_1 U743 ( .ip1(new_size[2]), .ip2(n560), .op(n575) );
  nor2_1 U744 ( .ip1(n559), .ip2(new_size[0]), .op(n571) );
  and2_1 U745 ( .ip1(n593), .ip2(n571), .op(n570) );
  nor2_1 U746 ( .ip1(csr[1]), .ip2(n570), .op(n573) );
  nor2_1 U747 ( .ip1(n571), .ip2(n593), .op(n572) );
  not_ab_or_c_or_d U748 ( .ip1(new_size[2]), .ip2(n560), .ip3(n573), .ip4(n572), .op(n574) );
  not_ab_or_c_or_d U749 ( .ip1(csr[3]), .ip2(n594), .ip3(n575), .ip4(n574), 
        .op(n577) );
  nor2_1 U750 ( .ip1(csr[3]), .ip2(n594), .op(n576) );
  not_ab_or_c_or_d U751 ( .ip1(new_size[4]), .ip2(n561), .ip3(n577), .ip4(n576), .op(n578) );
  not_ab_or_c_or_d U752 ( .ip1(csr[5]), .ip2(n595), .ip3(n579), .ip4(n578), 
        .op(n581) );
  nor2_1 U753 ( .ip1(csr[5]), .ip2(n595), .op(n580) );
  not_ab_or_c_or_d U754 ( .ip1(new_size[6]), .ip2(n562), .ip3(n581), .ip4(n580), .op(n582) );
  not_ab_or_c_or_d U755 ( .ip1(csr[7]), .ip2(n596), .ip3(n583), .ip4(n582), 
        .op(n585) );
  nor2_1 U756 ( .ip1(csr[7]), .ip2(n596), .op(n584) );
  not_ab_or_c_or_d U757 ( .ip1(new_size[8]), .ip2(n563), .ip3(n585), .ip4(n584), .op(n586) );
  not_ab_or_c_or_d U758 ( .ip1(csr[9]), .ip2(n597), .ip3(n587), .ip4(n586), 
        .op(n589) );
  nor2_1 U759 ( .ip1(csr[9]), .ip2(n597), .op(n588) );
  not_ab_or_c_or_d U760 ( .ip1(new_size[10]), .ip2(n564), .ip3(n589), .ip4(
        n588), .op(n590) );
  nor2_1 U761 ( .ip1(n591), .ip2(n590), .op(n592) );
  nor4_1 U762 ( .ip1(new_size[13]), .ip2(new_size[12]), .ip3(new_size[11]), 
        .ip4(n592), .op(N396) );
  inv_2 U763 ( .ip(new_size[1]), .op(n593) );
  inv_2 U764 ( .ip(new_size[3]), .op(n594) );
  inv_2 U765 ( .ip(new_size[5]), .op(n595) );
  inv_2 U766 ( .ip(new_size[7]), .op(n596) );
  inv_2 U767 ( .ip(new_size[9]), .op(n597) );
  nor2_1 U768 ( .ip1(buf0[27]), .ip2(n564), .op(n619) );
  nor2_1 U769 ( .ip1(buf0[25]), .ip2(n563), .op(n615) );
  nor2_1 U770 ( .ip1(buf0[23]), .ip2(n562), .op(n611) );
  nor2_1 U771 ( .ip1(buf0[21]), .ip2(n561), .op(n607) );
  nor2_1 U772 ( .ip1(buf0[19]), .ip2(n560), .op(n603) );
  nor2_1 U773 ( .ip1(n559), .ip2(buf0[17]), .op(n599) );
  and2_1 U774 ( .ip1(n621), .ip2(n599), .op(n598) );
  nor2_1 U775 ( .ip1(csr[1]), .ip2(n598), .op(n601) );
  nor2_1 U776 ( .ip1(n599), .ip2(n621), .op(n600) );
  not_ab_or_c_or_d U777 ( .ip1(buf0[19]), .ip2(n560), .ip3(n601), .ip4(n600), 
        .op(n602) );
  not_ab_or_c_or_d U778 ( .ip1(csr[3]), .ip2(n622), .ip3(n603), .ip4(n602), 
        .op(n605) );
  nor2_1 U779 ( .ip1(csr[3]), .ip2(n622), .op(n604) );
  not_ab_or_c_or_d U780 ( .ip1(buf0[21]), .ip2(n561), .ip3(n605), .ip4(n604), 
        .op(n606) );
  not_ab_or_c_or_d U781 ( .ip1(csr[5]), .ip2(n623), .ip3(n607), .ip4(n606), 
        .op(n609) );
  nor2_1 U782 ( .ip1(csr[5]), .ip2(n623), .op(n608) );
  not_ab_or_c_or_d U783 ( .ip1(buf0[23]), .ip2(n562), .ip3(n609), .ip4(n608), 
        .op(n610) );
  not_ab_or_c_or_d U784 ( .ip1(csr[7]), .ip2(n624), .ip3(n611), .ip4(n610), 
        .op(n613) );
  nor2_1 U785 ( .ip1(csr[7]), .ip2(n624), .op(n612) );
  not_ab_or_c_or_d U786 ( .ip1(buf0[25]), .ip2(n563), .ip3(n613), .ip4(n612), 
        .op(n614) );
  not_ab_or_c_or_d U787 ( .ip1(csr[9]), .ip2(n625), .ip3(n615), .ip4(n614), 
        .op(n617) );
  nor2_1 U788 ( .ip1(csr[9]), .ip2(n625), .op(n616) );
  not_ab_or_c_or_d U789 ( .ip1(buf0[27]), .ip2(n564), .ip3(n617), .ip4(n616), 
        .op(n618) );
  nor2_1 U790 ( .ip1(n619), .ip2(n618), .op(n620) );
  nor4_1 U791 ( .ip1(buf0[30]), .ip2(buf0[29]), .ip3(buf0[28]), .ip4(n620), 
        .op(N398) );
  inv_2 U792 ( .ip(buf0[18]), .op(n621) );
  inv_2 U793 ( .ip(buf0[20]), .op(n622) );
  inv_2 U794 ( .ip(buf0[22]), .op(n623) );
  inv_2 U795 ( .ip(buf0[24]), .op(n624) );
  inv_2 U796 ( .ip(buf0[26]), .op(n625) );
  nor2_1 U797 ( .ip1(buf1[27]), .ip2(n564), .op(n647) );
  nor2_1 U798 ( .ip1(buf1[25]), .ip2(n563), .op(n643) );
  nor2_1 U799 ( .ip1(buf1[23]), .ip2(n562), .op(n639) );
  nor2_1 U800 ( .ip1(buf1[21]), .ip2(n561), .op(n635) );
  nor2_1 U801 ( .ip1(buf1[19]), .ip2(n560), .op(n631) );
  nor2_1 U802 ( .ip1(n559), .ip2(buf1[17]), .op(n627) );
  and2_1 U803 ( .ip1(n649), .ip2(n627), .op(n626) );
  nor2_1 U804 ( .ip1(csr[1]), .ip2(n626), .op(n629) );
  nor2_1 U805 ( .ip1(n627), .ip2(n649), .op(n628) );
  not_ab_or_c_or_d U806 ( .ip1(buf1[19]), .ip2(n560), .ip3(n629), .ip4(n628), 
        .op(n630) );
  not_ab_or_c_or_d U807 ( .ip1(csr[3]), .ip2(n650), .ip3(n631), .ip4(n630), 
        .op(n633) );
  nor2_1 U808 ( .ip1(csr[3]), .ip2(n650), .op(n632) );
  not_ab_or_c_or_d U809 ( .ip1(buf1[21]), .ip2(n561), .ip3(n633), .ip4(n632), 
        .op(n634) );
  not_ab_or_c_or_d U810 ( .ip1(csr[5]), .ip2(n651), .ip3(n635), .ip4(n634), 
        .op(n637) );
  nor2_1 U811 ( .ip1(csr[5]), .ip2(n651), .op(n636) );
  not_ab_or_c_or_d U812 ( .ip1(buf1[23]), .ip2(n562), .ip3(n637), .ip4(n636), 
        .op(n638) );
  not_ab_or_c_or_d U813 ( .ip1(csr[7]), .ip2(n652), .ip3(n639), .ip4(n638), 
        .op(n641) );
  nor2_1 U814 ( .ip1(csr[7]), .ip2(n652), .op(n640) );
  not_ab_or_c_or_d U815 ( .ip1(buf1[25]), .ip2(n563), .ip3(n641), .ip4(n640), 
        .op(n642) );
  not_ab_or_c_or_d U816 ( .ip1(csr[9]), .ip2(n653), .ip3(n643), .ip4(n642), 
        .op(n645) );
  nor2_1 U817 ( .ip1(csr[9]), .ip2(n653), .op(n644) );
  not_ab_or_c_or_d U818 ( .ip1(buf1[27]), .ip2(n564), .ip3(n645), .ip4(n644), 
        .op(n646) );
  nor2_1 U819 ( .ip1(n647), .ip2(n646), .op(n648) );
  nor4_1 U820 ( .ip1(buf1[30]), .ip2(buf1[29]), .ip3(buf1[28]), .ip4(n648), 
        .op(N399) );
  inv_2 U821 ( .ip(buf1[18]), .op(n649) );
  inv_2 U822 ( .ip(buf1[20]), .op(n650) );
  inv_2 U823 ( .ip(buf1[22]), .op(n651) );
  inv_2 U824 ( .ip(buf1[24]), .op(n652) );
  inv_2 U825 ( .ip(buf1[26]), .op(n653) );
  nor2_1 U826 ( .ip1(buf_size[10]), .ip2(n677), .op(n675) );
  nor2_1 U827 ( .ip1(buf_size[8]), .ip2(n678), .op(n671) );
  nor2_1 U828 ( .ip1(buf_size[6]), .ip2(n679), .op(n667) );
  nor2_1 U829 ( .ip1(buf_size[4]), .ip2(n680), .op(n663) );
  nor2_1 U830 ( .ip1(buf_size[2]), .ip2(n681), .op(n659) );
  nor2_1 U831 ( .ip1(n682), .ip2(buf_size[0]), .op(n655) );
  and2_1 U832 ( .ip1(n565), .ip2(n655), .op(n654) );
  nor2_1 U833 ( .ip1(sizu_c[1]), .ip2(n654), .op(n657) );
  nor2_1 U834 ( .ip1(n655), .ip2(n565), .op(n656) );
  not_ab_or_c_or_d U835 ( .ip1(buf_size[2]), .ip2(n681), .ip3(n657), .ip4(n656), .op(n658) );
  not_ab_or_c_or_d U836 ( .ip1(sizu_c[3]), .ip2(n566), .ip3(n659), .ip4(n658), 
        .op(n661) );
  nor2_1 U837 ( .ip1(sizu_c[3]), .ip2(n566), .op(n660) );
  not_ab_or_c_or_d U838 ( .ip1(buf_size[4]), .ip2(n680), .ip3(n661), .ip4(n660), .op(n662) );
  not_ab_or_c_or_d U839 ( .ip1(sizu_c[5]), .ip2(n567), .ip3(n663), .ip4(n662), 
        .op(n665) );
  nor2_1 U840 ( .ip1(sizu_c[5]), .ip2(n567), .op(n664) );
  not_ab_or_c_or_d U841 ( .ip1(buf_size[6]), .ip2(n679), .ip3(n665), .ip4(n664), .op(n666) );
  not_ab_or_c_or_d U842 ( .ip1(sizu_c[7]), .ip2(n568), .ip3(n667), .ip4(n666), 
        .op(n669) );
  nor2_1 U843 ( .ip1(sizu_c[7]), .ip2(n568), .op(n668) );
  not_ab_or_c_or_d U844 ( .ip1(buf_size[8]), .ip2(n678), .ip3(n669), .ip4(n668), .op(n670) );
  not_ab_or_c_or_d U845 ( .ip1(sizu_c[9]), .ip2(n569), .ip3(n671), .ip4(n670), 
        .op(n673) );
  nor2_1 U846 ( .ip1(sizu_c[9]), .ip2(n569), .op(n672) );
  not_ab_or_c_or_d U847 ( .ip1(buf_size[10]), .ip2(n677), .ip3(n673), .ip4(
        n672), .op(n674) );
  nor2_1 U848 ( .ip1(n675), .ip2(n674), .op(n676) );
  nor4_1 U849 ( .ip1(buf_size[13]), .ip2(buf_size[12]), .ip3(buf_size[11]), 
        .ip4(n676), .op(N468) );
  inv_2 U850 ( .ip(sizu_c[10]), .op(n677) );
  inv_2 U851 ( .ip(sizu_c[8]), .op(n678) );
  inv_2 U852 ( .ip(sizu_c[6]), .op(n679) );
  inv_2 U853 ( .ip(sizu_c[4]), .op(n680) );
  inv_2 U854 ( .ip(sizu_c[2]), .op(n681) );
  inv_2 U855 ( .ip(sizu_c[0]), .op(n682) );
  and3_1 U856 ( .ip1(rx_ack_to_cnt[4]), .ip2(n687), .ip3(rx_ack_to_cnt[1]), 
        .op(n684) );
  nor3_1 U857 ( .ip1(n687), .ip2(rx_ack_to_cnt[4]), .ip3(rx_ack_to_cnt[1]), 
        .op(n683) );
  mux2_1 U858 ( .ip1(n684), .ip2(n683), .s(\eq_4480/B[5] ), .op(n685) );
  nand3_1 U859 ( .ip1(rx_ack_to_cnt[2]), .ip2(N525), .ip3(n685), .op(n686) );
  nor4_1 U860 ( .ip1(n686), .ip2(rx_ack_to_cnt[3]), .ip3(rx_ack_to_cnt[7]), 
        .ip4(rx_ack_to_cnt[6]), .op(N541) );
  inv_2 U861 ( .ip(rx_ack_to_cnt[5]), .op(n687) );
  and3_1 U862 ( .ip1(tx_data_to_cnt[4]), .ip2(n692), .ip3(tx_data_to_cnt[1]), 
        .op(n689) );
  nor3_1 U863 ( .ip1(n692), .ip2(tx_data_to_cnt[4]), .ip3(tx_data_to_cnt[1]), 
        .op(n688) );
  mux2_1 U864 ( .ip1(n689), .ip2(n688), .s(\eq_4480/B[5] ), .op(n690) );
  nand3_1 U865 ( .ip1(tx_data_to_cnt[2]), .ip2(N544), .ip3(n690), .op(n691) );
  nor4_1 U866 ( .ip1(n691), .ip2(tx_data_to_cnt[3]), .ip3(tx_data_to_cnt[7]), 
        .ip4(tx_data_to_cnt[6]), .op(N560) );
  inv_2 U867 ( .ip(tx_data_to_cnt[5]), .op(n692) );
  inv_2 U868 ( .ip(n111), .op(size[11]) );
  inv_2 U869 ( .ip(n110), .op(size[12]) );
  inv_2 U870 ( .ip(n109), .op(size[13]) );
  inv_2 U871 ( .ip(n98), .op(tx_dma_en) );
  inv_2 U872 ( .ip(buf0_na), .op(n697) );
  inv_2 U873 ( .ip(match_r), .op(n698) );
  inv_2 U874 ( .ip(n222), .op(n699) );
  inv_2 U875 ( .ip(n187), .op(n700) );
  inv_2 U876 ( .ip(n210), .op(n701) );
  inv_2 U877 ( .ip(data_pid_sel[1]), .op(n702) );
  inv_2 U878 ( .ip(data_pid_sel[0]), .op(n703) );
  inv_2 U879 ( .ip(buffer_done), .op(n704) );
  inv_2 U880 ( .ip(n310), .op(n705) );
  inv_2 U881 ( .ip(n119), .op(n706) );
  inv_2 U882 ( .ip(n141), .op(n707) );
  inv_2 U883 ( .ip(n120), .op(n708) );
  inv_2 U884 ( .ip(n166), .op(n709) );
  inv_2 U885 ( .ip(n298), .op(n710) );
  inv_2 U886 ( .ip(n280), .op(n711) );
  inv_2 U887 ( .ip(n302), .op(n712) );
  inv_2 U888 ( .ip(n299), .op(n713) );
  inv_2 U889 ( .ip(state[0]), .op(n714) );
  inv_2 U890 ( .ip(n294), .op(n715) );
  inv_2 U891 ( .ip(n135), .op(n716) );
  inv_2 U892 ( .ip(n152), .op(n717) );
  inv_2 U893 ( .ip(n177), .op(n718) );
  inv_2 U894 ( .ip(n183), .op(n719) );
  inv_2 U895 ( .ip(state[6]), .op(n720) );
  inv_2 U896 ( .ip(abort), .op(n721) );
  inv_2 U897 ( .ip(n307), .op(n722) );
  inv_2 U898 ( .ip(state[9]), .op(n723) );
  inv_2 U899 ( .ip(rx_ack_to_clr), .op(n724) );
  inv_2 U900 ( .ip(rx_ack_to), .op(n725) );
  inv_2 U901 ( .ip(state[3]), .op(n726) );
  inv_2 U902 ( .ip(state[1]), .op(n727) );
  inv_2 U903 ( .ip(state[2]), .op(n728) );
  inv_2 U904 ( .ip(state[5]), .op(n729) );
  inv_2 U905 ( .ip(state[7]), .op(n730) );
  inv_2 U906 ( .ip(N474), .op(n731) );
  inv_2 U907 ( .ip(N472), .op(n732) );
  inv_2 U908 ( .ip(n157), .op(n733) );
  inv_2 U909 ( .ip(pid_DATA2), .op(n734) );
  inv_2 U910 ( .ip(N250), .op(n735) );
  inv_2 U911 ( .ip(pid_DATA1), .op(n736) );
  inv_2 U912 ( .ip(n132), .op(n737) );
  inv_2 U913 ( .ip(pid_MDATA), .op(n738) );
  inv_2 U914 ( .ip(n123), .op(n739) );
  inv_2 U915 ( .ip(pid_DATA0), .op(n740) );
  inv_2 U916 ( .ip(token_valid), .op(n741) );
  inv_2 U917 ( .ip(mode_hs), .op(\eq_4480/B[5] ) );
  inv_2 U918 ( .ip(n252), .op(n742) );
  inv_2 U919 ( .ip(n215), .op(n743) );
  inv_2 U920 ( .ip(csr[31]), .op(n744) );
  inv_2 U921 ( .ip(csr[30]), .op(n745) );
  inv_2 U922 ( .ip(csr[29]), .op(n746) );
  inv_2 U923 ( .ip(csr[28]), .op(n747) );
  inv_2 U924 ( .ip(n174), .op(n748) );
  inv_2 U925 ( .ip(n176), .op(n749) );
  inv_2 U926 ( .ip(csr[27]), .op(n750) );
  inv_2 U927 ( .ip(n480), .op(dma_en) );
  inv_2 U928 ( .ip(n170), .op(n752) );
  inv_2 U929 ( .ip(n151), .op(n753) );
  inv_2 U930 ( .ip(csr[24]), .op(n754) );
  inv_2 U931 ( .ip(n101), .op(n755) );
  inv_2 U932 ( .ip(csr[23]), .op(n756) );
  inv_2 U933 ( .ip(buf0[3]), .op(n757) );
  inv_2 U934 ( .ip(buf0[2]), .op(n758) );
  inv_2 U935 ( .ip(buf0[1]), .op(n759) );
  inv_2 U936 ( .ip(buf0[0]), .op(n760) );
  inv_2 U937 ( .ip(rx_active), .op(n761) );
endmodule


module usbf_pl_SSRAM_HADR14_DW01_inc_1 ( A, SUM );
  input [11:0] A;
  output [11:0] SUM;
  wire   n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16;

  inv_2 U1 ( .ip(A[0]), .op(SUM[0]) );
  inv_2 U2 ( .ip(A[3]), .op(n2) );
  inv_2 U3 ( .ip(A[4]), .op(n3) );
  inv_2 U4 ( .ip(A[7]), .op(n4) );
  inv_2 U5 ( .ip(A[8]), .op(n5) );
  inv_2 U6 ( .ip(A[10]), .op(n6) );
  xor2_1 U7 ( .ip1(A[9]), .ip2(n7), .op(SUM[9]) );
  xor2_1 U8 ( .ip1(A[8]), .ip2(n8), .op(SUM[8]) );
  nor2_1 U9 ( .ip1(n9), .ip2(n4), .op(n8) );
  xor2_1 U10 ( .ip1(n9), .ip2(n4), .op(SUM[7]) );
  xnor2_1 U11 ( .ip1(n10), .ip2(A[6]), .op(SUM[6]) );
  nand2_1 U12 ( .ip1(A[5]), .ip2(n11), .op(n10) );
  xor2_1 U13 ( .ip1(A[5]), .ip2(n11), .op(SUM[5]) );
  xor2_1 U14 ( .ip1(A[4]), .ip2(n12), .op(SUM[4]) );
  nor2_1 U15 ( .ip1(n13), .ip2(n2), .op(n12) );
  xor2_1 U16 ( .ip1(n13), .ip2(n2), .op(SUM[3]) );
  xnor2_1 U17 ( .ip1(n14), .ip2(A[2]), .op(SUM[2]) );
  nand2_1 U18 ( .ip1(A[1]), .ip2(A[0]), .op(n14) );
  xor2_1 U19 ( .ip1(A[1]), .ip2(A[0]), .op(SUM[1]) );
  xor2_1 U20 ( .ip1(A[11]), .ip2(n15), .op(SUM[11]) );
  nor2_1 U21 ( .ip1(n16), .ip2(n6), .op(n15) );
  xor2_1 U22 ( .ip1(n16), .ip2(n6), .op(SUM[10]) );
  nand2_1 U23 ( .ip1(n7), .ip2(A[9]), .op(n16) );
  nor3_1 U24 ( .ip1(n4), .ip2(n9), .ip3(n5), .op(n7) );
  nand3_1 U25 ( .ip1(A[5]), .ip2(n11), .ip3(A[6]), .op(n9) );
  nor3_1 U26 ( .ip1(n2), .ip2(n13), .ip3(n3), .op(n11) );
  nand3_1 U27 ( .ip1(A[1]), .ip2(A[0]), .ip3(A[2]), .op(n13) );
endmodule


module usbf_pl_SSRAM_HADR14 ( clk, rst, rx_data, rx_valid, rx_active, rx_err, 
        tx_data, tx_valid, tx_valid_last, tx_ready, tx_first, tx_valid_out, 
        mode_hs, usb_reset, usb_suspend, usb_attached, madr, mdout, mdin, mwe, 
        mreq, mack, fa, idin, ep_sel, match, dma_in_buf_sz1, dma_out_buf_avail, 
        buf0_rl, buf0_set, buf1_set, uc_bsel_set, uc_dpd_set, int_buf1_set, 
        int_buf0_set, int_upid_set, int_crc16_set, int_to_set, int_seqerr_set, 
        out_to_small, csr, buf0, buf1, frm_nat, pid_cs_err, nse_err, crc5_err
 );
  input [7:0] rx_data;
  output [7:0] tx_data;
  output [14:0] madr;
  output [31:0] mdout;
  input [31:0] mdin;
  input [6:0] fa;
  output [31:0] idin;
  output [3:0] ep_sel;
  input [31:0] csr;
  input [31:0] buf0;
  input [31:0] buf1;
  output [31:0] frm_nat;
  input clk, rst, rx_valid, rx_active, rx_err, tx_ready, tx_valid_out, mode_hs,
         usb_reset, usb_suspend, usb_attached, mack, match, dma_in_buf_sz1,
         dma_out_buf_avail;
  output tx_valid, tx_valid_last, tx_first, mwe, mreq, buf0_rl, buf0_set,
         buf1_set, uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set,
         int_upid_set, int_crc16_set, int_to_set, int_seqerr_set, out_to_small,
         pid_cs_err, nse_err, crc5_err;
  wire   pid_ACK, pid_NACK, pid_STALL, pid_NYET, pid_PRE, pid_ERR, pid_SPLIT,
         pid_PING, fsel, token_valid, match_o, pid_SOF, frame_no_we,
         frame_no_we_r, N21, N22, frame_no_same, clr_sof_time, hms_clk, N46,
         N47, N48, N49, N50, N51, N52, N53, N54, N55, N56, N57, N77, N78, N79,
         N80, N81, N82, N83, N84, N85, N86, pid_OUT, pid_IN, pid_SETUP,
         pid_DATA0, pid_DATA1, pid_DATA2, pid_MDATA, rx_data_valid,
         rx_data_done, crc16_err, send_token, send_data, send_zero_length,
         rd_next, rx_dma_en, tx_dma_en, abort, idma_done, dma_en, n15, n16,
         n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32,
         n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46,
         n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60,
         n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74,
         n75, n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87, n88,
         n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100, n101,
         n102, n103, n104, n105, n106, n107, n108, n109, n110, n111, n112,
         n113, n114, n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13,
         n14, n17, n18, n115, n116, n117, n118, n119, n120, n121, n122, n123,
         n124, n125, n126, n127, n128, n129, n130, n131, n132, n133, n134,
         n135, n136, n137, n138, n139, n140, n141, n142, n143, n144, n145,
         n146, n147, n148, n149, n150, n151;
  wire   [10:0] frame_no;
  wire   [4:0] hms_cnt;
  wire   [6:0] token_fadr;
  wire   [7:0] rx_data_st;
  wire   [1:0] token_pid_sel;
  wire   [1:0] data_pid_sel;
  wire   [7:0] tx_data_st;
  wire   [16:0] adr;
  wire   [13:0] size;
  wire   [13:0] buf_size;
  wire   [10:0] sizu_c;
  assign frm_nat[12] = 1'b0;
  assign frm_nat[13] = 1'b0;
  assign frm_nat[14] = 1'b0;
  assign frm_nat[15] = 1'b0;
  assign frm_nat[27] = 1'b0;

  nand2_2 U3 ( .ip1(n15), .ip2(n16), .op(n87) );
  nand2_2 U4 ( .ip1(N56), .ip2(n1), .op(n16) );
  nand2_2 U5 ( .ip1(frm_nat[10]), .ip2(n2), .op(n15) );
  nand2_2 U6 ( .ip1(n19), .ip2(n20), .op(n88) );
  nand2_2 U9 ( .ip1(N55), .ip2(n1), .op(n20) );
  nand2_2 U10 ( .ip1(frm_nat[9]), .ip2(n2), .op(n19) );
  nand2_2 U11 ( .ip1(n21), .ip2(n22), .op(n89) );
  nand2_2 U12 ( .ip1(N54), .ip2(n1), .op(n22) );
  nand2_2 U13 ( .ip1(frm_nat[8]), .ip2(n2), .op(n21) );
  nand2_2 U14 ( .ip1(n23), .ip2(n24), .op(n90) );
  nand2_2 U15 ( .ip1(N53), .ip2(n1), .op(n24) );
  nand2_2 U16 ( .ip1(frm_nat[7]), .ip2(n2), .op(n23) );
  nand2_2 U17 ( .ip1(n25), .ip2(n26), .op(n91) );
  nand2_2 U18 ( .ip1(N52), .ip2(n1), .op(n26) );
  nand2_2 U19 ( .ip1(frm_nat[6]), .ip2(n2), .op(n25) );
  nand2_2 U20 ( .ip1(n27), .ip2(n28), .op(n92) );
  nand2_2 U21 ( .ip1(N51), .ip2(n1), .op(n28) );
  nand2_2 U22 ( .ip1(frm_nat[5]), .ip2(n2), .op(n27) );
  nand2_2 U23 ( .ip1(n29), .ip2(n30), .op(n93) );
  nand2_2 U24 ( .ip1(N50), .ip2(n1), .op(n30) );
  nand2_2 U25 ( .ip1(frm_nat[4]), .ip2(n2), .op(n29) );
  nand2_2 U26 ( .ip1(n31), .ip2(n32), .op(n94) );
  nand2_2 U27 ( .ip1(N49), .ip2(n1), .op(n32) );
  nand2_2 U28 ( .ip1(frm_nat[3]), .ip2(n2), .op(n31) );
  nand2_2 U29 ( .ip1(n33), .ip2(n34), .op(n95) );
  nand2_2 U30 ( .ip1(N48), .ip2(n1), .op(n34) );
  nand2_2 U31 ( .ip1(frm_nat[2]), .ip2(n2), .op(n33) );
  nand2_2 U32 ( .ip1(n35), .ip2(n36), .op(n96) );
  nand2_2 U33 ( .ip1(N47), .ip2(n1), .op(n36) );
  nand2_2 U34 ( .ip1(frm_nat[1]), .ip2(n2), .op(n35) );
  nand2_2 U35 ( .ip1(n37), .ip2(n38), .op(n97) );
  nand2_2 U36 ( .ip1(N46), .ip2(n1), .op(n38) );
  nand2_2 U37 ( .ip1(frm_nat[0]), .ip2(n2), .op(n37) );
  nand2_2 U38 ( .ip1(n39), .ip2(n40), .op(n98) );
  nand2_2 U39 ( .ip1(N57), .ip2(n1), .op(n40) );
  nand2_2 U40 ( .ip1(frm_nat[11]), .ip2(n2), .op(n39) );
  nand2_2 U43 ( .ip1(n41), .ip2(n42), .op(n99) );
  nand4_2 U44 ( .ip1(frm_nat[30]), .ip2(frm_nat[29]), .ip3(n43), .ip4(n150), 
        .op(n42) );
  nand2_2 U45 ( .ip1(frm_nat[31]), .ip2(n44), .op(n41) );
  nand2_2 U46 ( .ip1(n45), .ip2(n46), .op(n44) );
  nand2_2 U47 ( .ip1(n146), .ip2(n149), .op(n46) );
  nand2_2 U48 ( .ip1(n47), .ip2(n48), .op(n100) );
  nand3_2 U49 ( .ip1(n43), .ip2(n149), .ip3(frm_nat[29]), .op(n48) );
  or2_2 U50 ( .ip1(n149), .ip2(n45), .op(n47) );
  nor2_2 U51 ( .ip1(n49), .ip2(n50), .op(n45) );
  nor2_2 U52 ( .ip1(n51), .ip2(frm_nat[29]), .op(n50) );
  nand2_2 U53 ( .ip1(n52), .ip2(n53), .op(n101) );
  nand2_2 U54 ( .ip1(n43), .ip2(n148), .op(n53) );
  nor2_2 U55 ( .ip1(n147), .ip2(n51), .op(n43) );
  nand2_2 U56 ( .ip1(frm_nat[29]), .ip2(n49), .op(n52) );
  nand2_2 U57 ( .ip1(n54), .ip2(n55), .op(n49) );
  nand2_2 U58 ( .ip1(n56), .ip2(n55), .op(n102) );
  nand2_2 U59 ( .ip1(n146), .ip2(n147), .op(n55) );
  or2_2 U60 ( .ip1(n54), .ip2(n147), .op(n56) );
  nand3_2 U61 ( .ip1(n51), .ip2(n144), .ip3(rst), .op(n54) );
  nand2_2 U62 ( .ip1(rst), .ip2(frame_no_same), .op(n51) );
  nand2_2 U63 ( .ip1(n57), .ip2(n58), .op(n103) );
  nand2_2 U64 ( .ip1(frame_no[0]), .ip2(n145), .op(n58) );
  nand2_2 U65 ( .ip1(frm_nat[16]), .ip2(n59), .op(n57) );
  nand2_2 U66 ( .ip1(n60), .ip2(n61), .op(n104) );
  nand2_2 U67 ( .ip1(frame_no[1]), .ip2(n145), .op(n61) );
  nand2_2 U68 ( .ip1(frm_nat[17]), .ip2(n59), .op(n60) );
  nand2_2 U69 ( .ip1(n62), .ip2(n63), .op(n105) );
  nand2_2 U70 ( .ip1(frame_no[2]), .ip2(n145), .op(n63) );
  nand2_2 U71 ( .ip1(frm_nat[18]), .ip2(n59), .op(n62) );
  nand2_2 U72 ( .ip1(n64), .ip2(n65), .op(n106) );
  nand2_2 U73 ( .ip1(frame_no[3]), .ip2(n145), .op(n65) );
  nand2_2 U74 ( .ip1(frm_nat[19]), .ip2(n59), .op(n64) );
  nand2_2 U75 ( .ip1(n66), .ip2(n67), .op(n107) );
  nand2_2 U76 ( .ip1(frame_no[4]), .ip2(n145), .op(n67) );
  nand2_2 U77 ( .ip1(frm_nat[20]), .ip2(n59), .op(n66) );
  nand2_2 U78 ( .ip1(n68), .ip2(n69), .op(n108) );
  nand2_2 U79 ( .ip1(frame_no[5]), .ip2(n145), .op(n69) );
  nand2_2 U80 ( .ip1(frm_nat[21]), .ip2(n59), .op(n68) );
  nand2_2 U81 ( .ip1(n70), .ip2(n71), .op(n109) );
  nand2_2 U82 ( .ip1(frame_no[6]), .ip2(n145), .op(n71) );
  nand2_2 U83 ( .ip1(frm_nat[22]), .ip2(n59), .op(n70) );
  nand2_2 U84 ( .ip1(n72), .ip2(n73), .op(n110) );
  nand2_2 U85 ( .ip1(frame_no[7]), .ip2(n145), .op(n73) );
  nand2_2 U86 ( .ip1(frm_nat[23]), .ip2(n59), .op(n72) );
  nand2_2 U87 ( .ip1(n74), .ip2(n75), .op(n111) );
  nand2_2 U88 ( .ip1(frame_no[8]), .ip2(n145), .op(n75) );
  nand2_2 U89 ( .ip1(frm_nat[24]), .ip2(n59), .op(n74) );
  nand2_2 U90 ( .ip1(n76), .ip2(n77), .op(n112) );
  nand2_2 U91 ( .ip1(frame_no[9]), .ip2(n145), .op(n77) );
  nand2_2 U92 ( .ip1(frm_nat[25]), .ip2(n59), .op(n76) );
  nand2_2 U93 ( .ip1(n78), .ip2(n79), .op(n113) );
  nand2_2 U94 ( .ip1(frame_no[10]), .ip2(n145), .op(n79) );
  nand2_2 U95 ( .ip1(frm_nat[26]), .ip2(n59), .op(n78) );
  nand2_2 U97 ( .ip1(frame_no_we_r), .ip2(rst), .op(n80) );
  and4_2 U98 ( .ip1(hms_cnt[4]), .ip2(hms_cnt[3]), .ip3(n81), .ip4(hms_cnt[2]), 
        .op(n114) );
  nor2_2 U99 ( .ip1(hms_cnt[1]), .ip2(hms_cnt[0]), .op(n81) );
  and4_2 U100 ( .ip1(n82), .ip2(n83), .ip3(n84), .ip4(n85), .op(match_o) );
  and3_2 U103 ( .ip1(match), .ip2(fsel), .ip3(token_valid), .op(n82) );
  and2_2 U104 ( .ip1(N81), .ip2(n86), .op(N86) );
  and2_2 U105 ( .ip1(N80), .ip2(n86), .op(N85) );
  and2_2 U106 ( .ip1(N79), .ip2(n86), .op(N84) );
  and2_2 U107 ( .ip1(N78), .ip2(n86), .op(N83) );
  and2_2 U108 ( .ip1(N77), .ip2(n86), .op(N82) );
  and3_2 U109 ( .ip1(n144), .ip2(n151), .ip3(rst), .op(n86) );
  and2_2 U110 ( .ip1(N21), .ip2(frame_no_we), .op(N22) );
  and3_2 U111 ( .ip1(token_valid), .ip2(n142), .ip3(pid_SOF), .op(frame_no_we)
         );
  not_ab_or_c_or_d U122 ( .ip1(pid_PING), .ip2(n143), .ip3(pid_ACK), .ip4(
        crc5_err), .op(n83) );
  usbf_pd u0 ( .clk(clk), .rst(rst), .rx_data(rx_data), .rx_valid(rx_valid), 
        .rx_active(rx_active), .rx_err(rx_err), .pid_OUT(pid_OUT), .pid_IN(
        pid_IN), .pid_SOF(pid_SOF), .pid_SETUP(pid_SETUP), .pid_DATA0(
        pid_DATA0), .pid_DATA1(pid_DATA1), .pid_DATA2(pid_DATA2), .pid_MDATA(
        pid_MDATA), .pid_ACK(pid_ACK), .pid_NACK(pid_NACK), .pid_STALL(
        pid_STALL), .pid_NYET(pid_NYET), .pid_PRE(pid_PRE), .pid_ERR(pid_ERR), 
        .pid_SPLIT(pid_SPLIT), .pid_PING(pid_PING), .pid_cks_err(pid_cs_err), 
        .token_fadr(token_fadr), .token_endp(ep_sel), .token_valid(token_valid), .crc5_err(crc5_err), .frame_no(frame_no), .rx_data_st(rx_data_st), 
        .rx_data_valid(rx_data_valid), .rx_data_done(rx_data_done), 
        .crc16_err(crc16_err) );
  usbf_pa u1 ( .clk(clk), .rst(rst), .tx_data(tx_data), .tx_valid(tx_valid), 
        .tx_valid_last(tx_valid_last), .tx_ready(tx_ready), .tx_first(tx_first), .send_token(send_token), .token_pid_sel(token_pid_sel), .send_data(send_data), .data_pid_sel(data_pid_sel), .send_zero_length(send_zero_length), 
        .tx_data_st(tx_data_st), .rd_next(rd_next) );
  usbf_idma_SSRAM_HADR14 u2 ( .clk(clk), .rst(rst), .rx_data_st(rx_data_st), 
        .rx_data_valid(rx_data_valid), .rx_data_done(rx_data_done), 
        .send_data(send_data), .tx_data_st(tx_data_st), .rd_next(rd_next), 
        .rx_dma_en(rx_dma_en), .tx_dma_en(tx_dma_en), .abort(abort), 
        .idma_done(idma_done), .buf_size(buf_size), .dma_en(dma_en), 
        .send_zero_length(send_zero_length), .adr(adr), .size(size), .sizu_c(
        sizu_c), .madr(madr), .mdout(mdout), .mdin(mdin), .mwe(mwe), .mreq(
        mreq), .mack(mack) );
  usbf_pe_SSRAM_HADR14 u3 ( .clk(clk), .rst(rst), .tx_valid(tx_valid_out), 
        .rx_active(rx_active), .pid_OUT(pid_OUT), .pid_IN(pid_IN), .pid_SOF(
        pid_SOF), .pid_SETUP(pid_SETUP), .pid_DATA0(pid_DATA0), .pid_DATA1(
        pid_DATA1), .pid_DATA2(pid_DATA2), .pid_MDATA(pid_MDATA), .pid_ACK(
        pid_ACK), .pid_NACK(pid_NACK), .pid_STALL(pid_STALL), .pid_NYET(
        pid_NYET), .pid_PRE(pid_PRE), .pid_ERR(pid_ERR), .pid_SPLIT(pid_SPLIT), 
        .pid_PING(pid_PING), .mode_hs(mode_hs), .token_valid(token_valid), 
        .crc5_err(crc5_err), .rx_data_valid(rx_data_valid), .rx_data_done(
        rx_data_done), .crc16_err(crc16_err), .send_token(send_token), 
        .token_pid_sel(token_pid_sel), .data_pid_sel(data_pid_sel), 
        .send_zero_length(send_zero_length), .rx_dma_en(rx_dma_en), 
        .tx_dma_en(tx_dma_en), .abort(abort), .idma_done(idma_done), .adr(adr), 
        .size(size), .buf_size(buf_size), .sizu_c(sizu_c), .dma_en(dma_en), 
        .fsel(fsel), .idin(idin), .dma_in_buf_sz1(dma_in_buf_sz1), 
        .dma_out_buf_avail(dma_out_buf_avail), .ep_sel(ep_sel), .match(match_o), .nse_err(nse_err), .buf0_rl(buf0_rl), .buf0_set(buf0_set), .buf1_set(
        buf1_set), .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), 
        .int_buf1_set(int_buf1_set), .int_buf0_set(int_buf0_set), 
        .int_upid_set(int_upid_set), .int_crc16_set(int_crc16_set), 
        .int_to_set(int_to_set), .int_seqerr_set(int_seqerr_set), 
        .out_to_small(out_to_small), .csr(csr), .buf0(buf0), .buf1(buf1) );
  usbf_pl_SSRAM_HADR14_DW01_inc_1 add_1969_S2 ( .A(frm_nat[11:0]), .SUM({N57, 
        N56, N55, N54, N53, N52, N51, N50, N49, N48, N47, N46}) );
  dp_1 hms_clk_reg ( .ip(n114), .ck(clk), .q(hms_clk) );
  dp_1 frame_no_same_reg ( .ip(N22), .ck(clk), .q(frame_no_same) );
  dp_1 clr_sof_time_reg ( .ip(frame_no_we), .ck(clk), .q(clr_sof_time) );
  dp_1 \mfm_cnt_reg[0]  ( .ip(n102), .ck(clk), .q(frm_nat[28]) );
  dp_1 frame_no_we_r_reg ( .ip(frame_no_we), .ck(clk), .q(frame_no_we_r) );
  dp_1 \hms_cnt_reg[4]  ( .ip(N86), .ck(clk), .q(hms_cnt[4]) );
  dp_1 \hms_cnt_reg[3]  ( .ip(N85), .ck(clk), .q(hms_cnt[3]) );
  dp_1 \hms_cnt_reg[2]  ( .ip(N84), .ck(clk), .q(hms_cnt[2]) );
  dp_1 \frame_no_r_reg[2]  ( .ip(n105), .ck(clk), .q(frm_nat[18]) );
  dp_1 \frame_no_r_reg[10]  ( .ip(n113), .ck(clk), .q(frm_nat[26]) );
  dp_1 \frame_no_r_reg[9]  ( .ip(n112), .ck(clk), .q(frm_nat[25]) );
  dp_1 \frame_no_r_reg[8]  ( .ip(n111), .ck(clk), .q(frm_nat[24]) );
  dp_1 \frame_no_r_reg[7]  ( .ip(n110), .ck(clk), .q(frm_nat[23]) );
  dp_1 \frame_no_r_reg[6]  ( .ip(n109), .ck(clk), .q(frm_nat[22]) );
  dp_1 \frame_no_r_reg[5]  ( .ip(n108), .ck(clk), .q(frm_nat[21]) );
  dp_1 \frame_no_r_reg[4]  ( .ip(n107), .ck(clk), .q(frm_nat[20]) );
  dp_1 \frame_no_r_reg[3]  ( .ip(n106), .ck(clk), .q(frm_nat[19]) );
  dp_1 \hms_cnt_reg[1]  ( .ip(N83), .ck(clk), .q(hms_cnt[1]) );
  dp_1 \sof_time_reg[11]  ( .ip(n98), .ck(clk), .q(frm_nat[11]) );
  dp_1 \frame_no_r_reg[1]  ( .ip(n104), .ck(clk), .q(frm_nat[17]) );
  dp_1 \sof_time_reg[10]  ( .ip(n87), .ck(clk), .q(frm_nat[10]) );
  dp_1 \sof_time_reg[7]  ( .ip(n90), .ck(clk), .q(frm_nat[7]) );
  dp_1 \frame_no_r_reg[0]  ( .ip(n103), .ck(clk), .q(frm_nat[16]) );
  dp_1 \mfm_cnt_reg[3]  ( .ip(n99), .ck(clk), .q(frm_nat[31]) );
  dp_1 \sof_time_reg[3]  ( .ip(n94), .ck(clk), .q(frm_nat[3]) );
  dp_1 \mfm_cnt_reg[2]  ( .ip(n100), .ck(clk), .q(frm_nat[30]) );
  dp_1 \sof_time_reg[2]  ( .ip(n95), .ck(clk), .q(frm_nat[2]) );
  dp_1 \sof_time_reg[6]  ( .ip(n91), .ck(clk), .q(frm_nat[6]) );
  dp_1 \sof_time_reg[9]  ( .ip(n88), .ck(clk), .q(frm_nat[9]) );
  dp_1 \hms_cnt_reg[0]  ( .ip(N82), .ck(clk), .q(hms_cnt[0]) );
  dp_1 \sof_time_reg[8]  ( .ip(n89), .ck(clk), .q(frm_nat[8]) );
  dp_1 \sof_time_reg[1]  ( .ip(n96), .ck(clk), .q(frm_nat[1]) );
  dp_1 \sof_time_reg[5]  ( .ip(n92), .ck(clk), .q(frm_nat[5]) );
  dp_1 \sof_time_reg[4]  ( .ip(n93), .ck(clk), .q(frm_nat[4]) );
  dp_1 \sof_time_reg[0]  ( .ip(n97), .ck(clk), .q(frm_nat[0]) );
  dp_1 \mfm_cnt_reg[1]  ( .ip(n101), .ck(clk), .q(frm_nat[29]) );
  nor2_2 U7 ( .ip1(n151), .ip2(clr_sof_time), .op(n1) );
  nor2_2 U8 ( .ip1(clr_sof_time), .ip2(n1), .op(n2) );
  inv_2 U41 ( .ip(n80), .op(n145) );
  and2_2 U42 ( .ip1(rst), .ip2(n80), .op(n59) );
  nor3_2 U96 ( .ip1(pid_PRE), .ip2(pid_STALL), .ip3(pid_SPLIT), .op(n85) );
  nor3_2 U101 ( .ip1(pid_ERR), .ip2(pid_NYET), .ip3(pid_NACK), .op(n84) );
  xor2_1 U102 ( .ip1(hms_cnt[1]), .ip2(hms_cnt[0]), .op(N78) );
  nand2_1 U112 ( .ip1(hms_cnt[1]), .ip2(hms_cnt[0]), .op(n3) );
  xnor2_1 U113 ( .ip1(n3), .ip2(hms_cnt[2]), .op(N79) );
  nand3_1 U114 ( .ip1(hms_cnt[1]), .ip2(hms_cnt[0]), .ip3(hms_cnt[2]), .op(n4)
         );
  xor2_1 U115 ( .ip1(n4), .ip2(n6), .op(N80) );
  nor2_1 U116 ( .ip1(n4), .ip2(n6), .op(n5) );
  xor2_1 U117 ( .ip1(hms_cnt[4]), .ip2(n5), .op(N81) );
  inv_2 U118 ( .ip(hms_cnt[3]), .op(n6) );
  inv_2 U119 ( .ip(hms_cnt[0]), .op(N77) );
  xor2_1 U120 ( .ip1(frame_no[9]), .ip2(frm_nat[25]), .op(n8) );
  xor2_1 U121 ( .ip1(frame_no[10]), .ip2(frm_nat[26]), .op(n7) );
  nor2_1 U123 ( .ip1(n8), .ip2(n7), .op(n124) );
  xor2_1 U124 ( .ip1(frame_no[8]), .ip2(frm_nat[24]), .op(n11) );
  xor2_1 U125 ( .ip1(frame_no[6]), .ip2(frm_nat[22]), .op(n10) );
  xor2_1 U126 ( .ip1(frame_no[7]), .ip2(frm_nat[23]), .op(n9) );
  nor3_1 U127 ( .ip1(n11), .ip2(n10), .ip3(n9), .op(n123) );
  xor2_1 U128 ( .ip1(frame_no[5]), .ip2(frm_nat[21]), .op(n14) );
  xor2_1 U129 ( .ip1(frame_no[3]), .ip2(frm_nat[19]), .op(n13) );
  xor2_1 U130 ( .ip1(frame_no[4]), .ip2(frm_nat[20]), .op(n12) );
  nor3_1 U131 ( .ip1(n14), .ip2(n13), .ip3(n12), .op(n122) );
  or2_1 U132 ( .ip1(n141), .ip2(frm_nat[16]), .op(n17) );
  nand2_1 U133 ( .ip1(frm_nat[17]), .ip2(n17), .op(n120) );
  nand2_1 U134 ( .ip1(n17), .ip2(n125), .op(n119) );
  xor2_1 U135 ( .ip1(frame_no[2]), .ip2(frm_nat[18]), .op(n118) );
  and2_1 U136 ( .ip1(frm_nat[16]), .ip2(n141), .op(n18) );
  nor2_1 U137 ( .ip1(n18), .ip2(n125), .op(n116) );
  nor2_1 U138 ( .ip1(frm_nat[17]), .ip2(n18), .op(n115) );
  nor2_1 U139 ( .ip1(n116), .ip2(n115), .op(n117) );
  not_ab_or_c_or_d U140 ( .ip1(n120), .ip2(n119), .ip3(n118), .ip4(n117), .op(
        n121) );
  and4_1 U141 ( .ip1(n124), .ip2(n123), .ip3(n122), .ip4(n121), .op(N21) );
  inv_2 U142 ( .ip(frame_no[1]), .op(n125) );
  xor2_1 U143 ( .ip1(fa[2]), .ip2(token_fadr[2]), .op(n135) );
  xor2_1 U144 ( .ip1(fa[3]), .ip2(token_fadr[3]), .op(n134) );
  nor2_1 U145 ( .ip1(n141), .ip2(fa[0]), .op(n126) );
  nor2_1 U146 ( .ip1(n126), .ip2(n140), .op(n128) );
  nor2_1 U147 ( .ip1(token_fadr[1]), .ip2(n126), .op(n127) );
  nor2_1 U148 ( .ip1(n128), .ip2(n127), .op(n133) );
  and2_1 U149 ( .ip1(fa[0]), .ip2(n141), .op(n129) );
  nor2_1 U150 ( .ip1(fa[1]), .ip2(n129), .op(n131) );
  nor2_1 U151 ( .ip1(n129), .ip2(n125), .op(n130) );
  nor2_1 U152 ( .ip1(n131), .ip2(n130), .op(n132) );
  or4_1 U153 ( .ip1(n135), .ip2(n134), .ip3(n133), .ip4(n132), .op(n139) );
  xor2_1 U154 ( .ip1(fa[6]), .ip2(token_fadr[6]), .op(n138) );
  xor2_1 U155 ( .ip1(fa[4]), .ip2(token_fadr[4]), .op(n137) );
  xor2_1 U156 ( .ip1(fa[5]), .ip2(token_fadr[5]), .op(n136) );
  nor4_1 U157 ( .ip1(n139), .ip2(n138), .ip3(n137), .ip4(n136), .op(fsel) );
  inv_2 U158 ( .ip(fa[1]), .op(n140) );
  inv_2 U159 ( .ip(token_fadr[0]), .op(n141) );
  inv_2 U160 ( .ip(crc5_err), .op(n142) );
  inv_2 U161 ( .ip(mode_hs), .op(n143) );
  inv_2 U162 ( .ip(frame_no_we_r), .op(n144) );
  inv_2 U163 ( .ip(n51), .op(n146) );
  inv_2 U164 ( .ip(frm_nat[28]), .op(n147) );
  inv_2 U165 ( .ip(frm_nat[29]), .op(n148) );
  inv_2 U166 ( .ip(frm_nat[30]), .op(n149) );
  inv_2 U167 ( .ip(frm_nat[31]), .op(n150) );
  inv_2 U168 ( .ip(hms_clk), .op(n151) );
endmodule


module usbf_mem_arb_SSRAM_HADR14 ( phy_clk, wclk, rst, sram_adr, sram_din, 
        sram_dout, sram_re, sram_we, madr, mdout, mdin, mwe, mreq, mack, wadr, 
        wdout, wdin, wwe, wreq, wack );
  output [14:0] sram_adr;
  input [31:0] sram_din;
  output [31:0] sram_dout;
  input [14:0] madr;
  output [31:0] mdout;
  input [31:0] mdin;
  input [14:0] wadr;
  output [31:0] wdout;
  input [31:0] wdin;
  input phy_clk, wclk, rst, mwe, mreq, wwe, wreq;
  output sram_re, sram_we, mack, wack;
  wire   mreq, wack_r, N9, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14,
         n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28,
         n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42,
         n43, n44, n45, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56,
         n57, n58, n59, n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70,
         n71, n72, n73, n74, n75, n76, n77, n78, n79, n80, n81, n82, n83, n84,
         n85, n86, n87, n88, n89, n90, n91, n92, n93, n94, n95, n96, n97, n98,
         n99, n100, n101, n1, n2, n3, n102, n103, n104, n105, n106, n107, n108,
         n109, n110, n111, n112, n113, n114, n115, n116, n117, n118, n119,
         n120, n121, n122, n123, n124, n125, n126, n127, n128, n129, n130,
         n131, n132, n133;
  assign sram_re = 1'b1;
  assign wdout[31] = sram_din[31];
  assign mdout[31] = sram_din[31];
  assign wdout[30] = sram_din[30];
  assign mdout[30] = sram_din[30];
  assign wdout[29] = sram_din[29];
  assign mdout[29] = sram_din[29];
  assign wdout[28] = sram_din[28];
  assign mdout[28] = sram_din[28];
  assign wdout[27] = sram_din[27];
  assign mdout[27] = sram_din[27];
  assign wdout[26] = sram_din[26];
  assign mdout[26] = sram_din[26];
  assign wdout[25] = sram_din[25];
  assign mdout[25] = sram_din[25];
  assign wdout[24] = sram_din[24];
  assign mdout[24] = sram_din[24];
  assign wdout[23] = sram_din[23];
  assign mdout[23] = sram_din[23];
  assign wdout[22] = sram_din[22];
  assign mdout[22] = sram_din[22];
  assign wdout[21] = sram_din[21];
  assign mdout[21] = sram_din[21];
  assign wdout[20] = sram_din[20];
  assign mdout[20] = sram_din[20];
  assign wdout[19] = sram_din[19];
  assign mdout[19] = sram_din[19];
  assign wdout[18] = sram_din[18];
  assign mdout[18] = sram_din[18];
  assign wdout[17] = sram_din[17];
  assign mdout[17] = sram_din[17];
  assign wdout[16] = sram_din[16];
  assign mdout[16] = sram_din[16];
  assign wdout[15] = sram_din[15];
  assign mdout[15] = sram_din[15];
  assign wdout[14] = sram_din[14];
  assign mdout[14] = sram_din[14];
  assign wdout[13] = sram_din[13];
  assign mdout[13] = sram_din[13];
  assign wdout[12] = sram_din[12];
  assign mdout[12] = sram_din[12];
  assign wdout[11] = sram_din[11];
  assign mdout[11] = sram_din[11];
  assign wdout[10] = sram_din[10];
  assign mdout[10] = sram_din[10];
  assign wdout[9] = sram_din[9];
  assign mdout[9] = sram_din[9];
  assign wdout[8] = sram_din[8];
  assign mdout[8] = sram_din[8];
  assign wdout[7] = sram_din[7];
  assign mdout[7] = sram_din[7];
  assign wdout[6] = sram_din[6];
  assign mdout[6] = sram_din[6];
  assign wdout[5] = sram_din[5];
  assign mdout[5] = sram_din[5];
  assign wdout[4] = sram_din[4];
  assign mdout[4] = sram_din[4];
  assign wdout[3] = sram_din[3];
  assign mdout[3] = sram_din[3];
  assign wdout[2] = sram_din[2];
  assign mdout[2] = sram_din[2];
  assign wdout[1] = sram_din[1];
  assign mdout[1] = sram_din[1];
  assign wdout[0] = sram_din[0];
  assign mdout[0] = sram_din[0];
  assign mack = mreq;

  nand2_2 U3 ( .ip1(n4), .ip2(n5), .op(sram_we) );
  nand2_2 U4 ( .ip1(mwe), .ip2(mreq), .op(n5) );
  nand3_2 U5 ( .ip1(wreq), .ip2(n106), .ip3(wwe), .op(n4) );
  nand2_2 U6 ( .ip1(n6), .ip2(n7), .op(sram_dout[9]) );
  nand2_2 U7 ( .ip1(mdin[9]), .ip2(n107), .op(n7) );
  nand2_2 U8 ( .ip1(wdin[9]), .ip2(n105), .op(n6) );
  nand2_2 U9 ( .ip1(n9), .ip2(n10), .op(sram_dout[8]) );
  nand2_2 U10 ( .ip1(mdin[8]), .ip2(n107), .op(n10) );
  nand2_2 U11 ( .ip1(wdin[8]), .ip2(n104), .op(n9) );
  nand2_2 U12 ( .ip1(n11), .ip2(n12), .op(sram_dout[7]) );
  nand2_2 U13 ( .ip1(mdin[7]), .ip2(n108), .op(n12) );
  nand2_2 U14 ( .ip1(wdin[7]), .ip2(n106), .op(n11) );
  nand2_2 U15 ( .ip1(n13), .ip2(n14), .op(sram_dout[6]) );
  nand2_2 U16 ( .ip1(mdin[6]), .ip2(n108), .op(n14) );
  nand2_2 U17 ( .ip1(wdin[6]), .ip2(n105), .op(n13) );
  nand2_2 U18 ( .ip1(n15), .ip2(n16), .op(sram_dout[5]) );
  nand2_2 U19 ( .ip1(mdin[5]), .ip2(n109), .op(n16) );
  nand2_2 U20 ( .ip1(wdin[5]), .ip2(n104), .op(n15) );
  nand2_2 U21 ( .ip1(n17), .ip2(n18), .op(sram_dout[4]) );
  nand2_2 U22 ( .ip1(mdin[4]), .ip2(n109), .op(n18) );
  nand2_2 U23 ( .ip1(wdin[4]), .ip2(n106), .op(n17) );
  nand2_2 U24 ( .ip1(n19), .ip2(n20), .op(sram_dout[3]) );
  nand2_2 U25 ( .ip1(mdin[3]), .ip2(n110), .op(n20) );
  nand2_2 U26 ( .ip1(wdin[3]), .ip2(n105), .op(n19) );
  nand2_2 U27 ( .ip1(n21), .ip2(n22), .op(sram_dout[31]) );
  nand2_2 U28 ( .ip1(mdin[31]), .ip2(n110), .op(n22) );
  nand2_2 U29 ( .ip1(wdin[31]), .ip2(n104), .op(n21) );
  nand2_2 U30 ( .ip1(n23), .ip2(n24), .op(sram_dout[30]) );
  nand2_2 U31 ( .ip1(mdin[30]), .ip2(n111), .op(n24) );
  nand2_2 U32 ( .ip1(wdin[30]), .ip2(n106), .op(n23) );
  nand2_2 U33 ( .ip1(n25), .ip2(n26), .op(sram_dout[2]) );
  nand2_2 U34 ( .ip1(mdin[2]), .ip2(n111), .op(n26) );
  nand2_2 U35 ( .ip1(wdin[2]), .ip2(n105), .op(n25) );
  nand2_2 U36 ( .ip1(n27), .ip2(n28), .op(sram_dout[29]) );
  nand2_2 U37 ( .ip1(mdin[29]), .ip2(n112), .op(n28) );
  nand2_2 U38 ( .ip1(wdin[29]), .ip2(n104), .op(n27) );
  nand2_2 U39 ( .ip1(n29), .ip2(n30), .op(sram_dout[28]) );
  nand2_2 U40 ( .ip1(mdin[28]), .ip2(n112), .op(n30) );
  nand2_2 U41 ( .ip1(wdin[28]), .ip2(n106), .op(n29) );
  nand2_2 U42 ( .ip1(n31), .ip2(n32), .op(sram_dout[27]) );
  nand2_2 U43 ( .ip1(mdin[27]), .ip2(n113), .op(n32) );
  nand2_2 U44 ( .ip1(wdin[27]), .ip2(n106), .op(n31) );
  nand2_2 U45 ( .ip1(n33), .ip2(n34), .op(sram_dout[26]) );
  nand2_2 U46 ( .ip1(mdin[26]), .ip2(n113), .op(n34) );
  nand2_2 U47 ( .ip1(wdin[26]), .ip2(n106), .op(n33) );
  nand2_2 U48 ( .ip1(n35), .ip2(n36), .op(sram_dout[25]) );
  nand2_2 U49 ( .ip1(mdin[25]), .ip2(n114), .op(n36) );
  nand2_2 U50 ( .ip1(wdin[25]), .ip2(n106), .op(n35) );
  nand2_2 U51 ( .ip1(n37), .ip2(n38), .op(sram_dout[24]) );
  nand2_2 U52 ( .ip1(mdin[24]), .ip2(n114), .op(n38) );
  nand2_2 U53 ( .ip1(wdin[24]), .ip2(n106), .op(n37) );
  nand2_2 U54 ( .ip1(n39), .ip2(n40), .op(sram_dout[23]) );
  nand2_2 U55 ( .ip1(mdin[23]), .ip2(n115), .op(n40) );
  nand2_2 U56 ( .ip1(wdin[23]), .ip2(n106), .op(n39) );
  nand2_2 U57 ( .ip1(n41), .ip2(n42), .op(sram_dout[22]) );
  nand2_2 U58 ( .ip1(mdin[22]), .ip2(n115), .op(n42) );
  nand2_2 U59 ( .ip1(wdin[22]), .ip2(n106), .op(n41) );
  nand2_2 U60 ( .ip1(n43), .ip2(n44), .op(sram_dout[21]) );
  nand2_2 U61 ( .ip1(mdin[21]), .ip2(n116), .op(n44) );
  nand2_2 U62 ( .ip1(wdin[21]), .ip2(n106), .op(n43) );
  nand2_2 U63 ( .ip1(n45), .ip2(n46), .op(sram_dout[20]) );
  nand2_2 U64 ( .ip1(mdin[20]), .ip2(n116), .op(n46) );
  nand2_2 U65 ( .ip1(wdin[20]), .ip2(n106), .op(n45) );
  nand2_2 U66 ( .ip1(n47), .ip2(n48), .op(sram_dout[1]) );
  nand2_2 U67 ( .ip1(mdin[1]), .ip2(n117), .op(n48) );
  nand2_2 U68 ( .ip1(wdin[1]), .ip2(n106), .op(n47) );
  nand2_2 U69 ( .ip1(n49), .ip2(n50), .op(sram_dout[19]) );
  nand2_2 U70 ( .ip1(mdin[19]), .ip2(n117), .op(n50) );
  nand2_2 U71 ( .ip1(wdin[19]), .ip2(n106), .op(n49) );
  nand2_2 U72 ( .ip1(n51), .ip2(n52), .op(sram_dout[18]) );
  nand2_2 U73 ( .ip1(mdin[18]), .ip2(n118), .op(n52) );
  nand2_2 U74 ( .ip1(wdin[18]), .ip2(n106), .op(n51) );
  nand2_2 U75 ( .ip1(n53), .ip2(n54), .op(sram_dout[17]) );
  nand2_2 U76 ( .ip1(mdin[17]), .ip2(n118), .op(n54) );
  nand2_2 U77 ( .ip1(wdin[17]), .ip2(n105), .op(n53) );
  nand2_2 U78 ( .ip1(n55), .ip2(n56), .op(sram_dout[16]) );
  nand2_2 U79 ( .ip1(mdin[16]), .ip2(n119), .op(n56) );
  nand2_2 U80 ( .ip1(wdin[16]), .ip2(n105), .op(n55) );
  nand2_2 U81 ( .ip1(n57), .ip2(n58), .op(sram_dout[15]) );
  nand2_2 U82 ( .ip1(mdin[15]), .ip2(n119), .op(n58) );
  nand2_2 U83 ( .ip1(wdin[15]), .ip2(n105), .op(n57) );
  nand2_2 U84 ( .ip1(n59), .ip2(n60), .op(sram_dout[14]) );
  nand2_2 U85 ( .ip1(mdin[14]), .ip2(n120), .op(n60) );
  nand2_2 U86 ( .ip1(wdin[14]), .ip2(n105), .op(n59) );
  nand2_2 U87 ( .ip1(n61), .ip2(n62), .op(sram_dout[13]) );
  nand2_2 U88 ( .ip1(mdin[13]), .ip2(n120), .op(n62) );
  nand2_2 U89 ( .ip1(wdin[13]), .ip2(n105), .op(n61) );
  nand2_2 U90 ( .ip1(n63), .ip2(n64), .op(sram_dout[12]) );
  nand2_2 U91 ( .ip1(mdin[12]), .ip2(n121), .op(n64) );
  nand2_2 U92 ( .ip1(wdin[12]), .ip2(n105), .op(n63) );
  nand2_2 U93 ( .ip1(n65), .ip2(n66), .op(sram_dout[11]) );
  nand2_2 U94 ( .ip1(mdin[11]), .ip2(n121), .op(n66) );
  nand2_2 U95 ( .ip1(wdin[11]), .ip2(n105), .op(n65) );
  nand2_2 U96 ( .ip1(n67), .ip2(n68), .op(sram_dout[10]) );
  nand2_2 U97 ( .ip1(mdin[10]), .ip2(n122), .op(n68) );
  nand2_2 U98 ( .ip1(wdin[10]), .ip2(n105), .op(n67) );
  nand2_2 U99 ( .ip1(n69), .ip2(n70), .op(sram_dout[0]) );
  nand2_2 U100 ( .ip1(mdin[0]), .ip2(n122), .op(n70) );
  nand2_2 U101 ( .ip1(wdin[0]), .ip2(n105), .op(n69) );
  nand2_2 U102 ( .ip1(n71), .ip2(n72), .op(sram_adr[9]) );
  nand2_2 U103 ( .ip1(madr[9]), .ip2(n123), .op(n72) );
  nand2_2 U104 ( .ip1(wadr[9]), .ip2(n105), .op(n71) );
  nand2_2 U105 ( .ip1(n73), .ip2(n74), .op(sram_adr[8]) );
  nand2_2 U106 ( .ip1(madr[8]), .ip2(n123), .op(n74) );
  nand2_2 U107 ( .ip1(wadr[8]), .ip2(n105), .op(n73) );
  nand2_2 U108 ( .ip1(n75), .ip2(n76), .op(sram_adr[7]) );
  nand2_2 U109 ( .ip1(madr[7]), .ip2(n124), .op(n76) );
  nand2_2 U110 ( .ip1(wadr[7]), .ip2(n105), .op(n75) );
  nand2_2 U111 ( .ip1(n77), .ip2(n78), .op(sram_adr[6]) );
  nand2_2 U112 ( .ip1(madr[6]), .ip2(n124), .op(n78) );
  nand2_2 U113 ( .ip1(wadr[6]), .ip2(n104), .op(n77) );
  nand2_2 U114 ( .ip1(n79), .ip2(n80), .op(sram_adr[5]) );
  nand2_2 U115 ( .ip1(madr[5]), .ip2(n125), .op(n80) );
  nand2_2 U116 ( .ip1(wadr[5]), .ip2(n104), .op(n79) );
  nand2_2 U117 ( .ip1(n81), .ip2(n82), .op(sram_adr[4]) );
  nand2_2 U118 ( .ip1(madr[4]), .ip2(n125), .op(n82) );
  nand2_2 U119 ( .ip1(wadr[4]), .ip2(n104), .op(n81) );
  nand2_2 U120 ( .ip1(n83), .ip2(n84), .op(sram_adr[3]) );
  nand2_2 U121 ( .ip1(madr[3]), .ip2(n126), .op(n84) );
  nand2_2 U122 ( .ip1(wadr[3]), .ip2(n104), .op(n83) );
  nand2_2 U123 ( .ip1(n85), .ip2(n86), .op(sram_adr[2]) );
  nand2_2 U124 ( .ip1(madr[2]), .ip2(n126), .op(n86) );
  nand2_2 U125 ( .ip1(wadr[2]), .ip2(n104), .op(n85) );
  nand2_2 U126 ( .ip1(n87), .ip2(n88), .op(sram_adr[1]) );
  nand2_2 U127 ( .ip1(madr[1]), .ip2(n127), .op(n88) );
  nand2_2 U128 ( .ip1(wadr[1]), .ip2(n104), .op(n87) );
  nand2_2 U129 ( .ip1(n89), .ip2(n90), .op(sram_adr[14]) );
  nand2_2 U130 ( .ip1(madr[14]), .ip2(n127), .op(n90) );
  nand2_2 U131 ( .ip1(wadr[14]), .ip2(n104), .op(n89) );
  nand2_2 U132 ( .ip1(n91), .ip2(n92), .op(sram_adr[13]) );
  nand2_2 U133 ( .ip1(madr[13]), .ip2(n128), .op(n92) );
  nand2_2 U134 ( .ip1(wadr[13]), .ip2(n104), .op(n91) );
  nand2_2 U135 ( .ip1(n93), .ip2(n94), .op(sram_adr[12]) );
  nand2_2 U136 ( .ip1(madr[12]), .ip2(n128), .op(n94) );
  nand2_2 U137 ( .ip1(wadr[12]), .ip2(n104), .op(n93) );
  nand2_2 U138 ( .ip1(n95), .ip2(n96), .op(sram_adr[11]) );
  nand2_2 U139 ( .ip1(madr[11]), .ip2(n129), .op(n96) );
  nand2_2 U140 ( .ip1(wadr[11]), .ip2(n104), .op(n95) );
  nand2_2 U141 ( .ip1(n97), .ip2(n98), .op(sram_adr[10]) );
  nand2_2 U142 ( .ip1(madr[10]), .ip2(n129), .op(n98) );
  nand2_2 U143 ( .ip1(wadr[10]), .ip2(n104), .op(n97) );
  nand2_2 U144 ( .ip1(n99), .ip2(n100), .op(sram_adr[0]) );
  nand2_2 U145 ( .ip1(madr[0]), .ip2(n130), .op(n100) );
  nand2_2 U146 ( .ip1(wadr[0]), .ip2(n104), .op(n99) );
  nor2_2 U147 ( .ip1(wack), .ip2(n101), .op(n8) );
  and2_2 U148 ( .ip1(wreq), .ip2(n132), .op(n101) );
  nor2_2 U149 ( .ip1(n133), .ip2(mreq), .op(wack) );
  and4_2 U150 ( .ip1(rst), .ip2(wreq), .ip3(n132), .ip4(n133), .op(N9) );
  dp_1 wack_r_reg ( .ip(N9), .ck(phy_clk), .q(wack_r) );
  inv_2 U151 ( .ip(n130), .op(n105) );
  inv_2 U152 ( .ip(n130), .op(n104) );
  inv_2 U153 ( .ip(n131), .op(n106) );
  buf_2 U154 ( .ip(n8), .op(n1) );
  buf_2 U155 ( .ip(n8), .op(n2) );
  buf_2 U156 ( .ip(n8), .op(n3) );
  buf_2 U157 ( .ip(n8), .op(n102) );
  buf_2 U158 ( .ip(n8), .op(n103) );
  buf_1 U159 ( .ip(n1), .op(n107) );
  buf_1 U160 ( .ip(n1), .op(n108) );
  buf_1 U161 ( .ip(n1), .op(n109) );
  buf_1 U162 ( .ip(n1), .op(n110) );
  buf_1 U163 ( .ip(n1), .op(n111) );
  buf_1 U164 ( .ip(n2), .op(n112) );
  buf_1 U165 ( .ip(n2), .op(n113) );
  buf_1 U166 ( .ip(n2), .op(n114) );
  buf_1 U167 ( .ip(n2), .op(n115) );
  buf_1 U168 ( .ip(n2), .op(n116) );
  buf_1 U169 ( .ip(n3), .op(n117) );
  buf_1 U170 ( .ip(n3), .op(n118) );
  buf_1 U171 ( .ip(n3), .op(n119) );
  buf_1 U172 ( .ip(n3), .op(n120) );
  buf_1 U173 ( .ip(n3), .op(n121) );
  buf_1 U174 ( .ip(n102), .op(n122) );
  buf_1 U175 ( .ip(n102), .op(n123) );
  buf_1 U176 ( .ip(n102), .op(n124) );
  buf_1 U177 ( .ip(n102), .op(n125) );
  buf_1 U178 ( .ip(n102), .op(n126) );
  buf_1 U179 ( .ip(n103), .op(n127) );
  buf_1 U180 ( .ip(n103), .op(n128) );
  buf_1 U181 ( .ip(n103), .op(n129) );
  buf_1 U182 ( .ip(n103), .op(n130) );
  buf_1 U183 ( .ip(n103), .op(n131) );
  inv_2 U184 ( .ip(mreq), .op(n132) );
  inv_2 U185 ( .ip(wack_r), .op(n133) );
endmodule


module usbf_ep_rf_0_DW01_sub_1 ( A, B, CI, DIFF, CO );
  input [11:0] A;
  input [11:0] B;
  output [11:0] DIFF;
  input CI;
  output CO;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13;
  wire   [12:0] carry;

  fulladder U2_11 ( .a(A[11]), .b(n2), .ci(carry[11]), .s(DIFF[11]) );
  fulladder U2_10 ( .a(A[10]), .b(n3), .ci(carry[10]), .co(carry[11]), .s(
        DIFF[10]) );
  fulladder U2_9 ( .a(A[9]), .b(n13), .ci(carry[9]), .co(carry[10]), .s(
        DIFF[9]) );
  fulladder U2_8 ( .a(A[8]), .b(n12), .ci(carry[8]), .co(carry[9]), .s(DIFF[8]) );
  fulladder U2_7 ( .a(A[7]), .b(n11), .ci(carry[7]), .co(carry[8]), .s(DIFF[7]) );
  fulladder U2_6 ( .a(A[6]), .b(n10), .ci(carry[6]), .co(carry[7]), .s(DIFF[6]) );
  fulladder U2_5 ( .a(A[5]), .b(n9), .ci(carry[5]), .co(carry[6]), .s(DIFF[5])
         );
  fulladder U2_4 ( .a(A[4]), .b(n8), .ci(carry[4]), .co(carry[5]), .s(DIFF[4])
         );
  fulladder U2_3 ( .a(A[3]), .b(n7), .ci(carry[3]), .co(carry[4]), .s(DIFF[3])
         );
  fulladder U2_2 ( .a(A[2]), .b(n6), .ci(carry[2]), .co(carry[3]), .s(DIFF[2])
         );
  fulladder U2_1 ( .a(A[1]), .b(n5), .ci(n1), .co(carry[2]), .s(DIFF[1]) );
  or2_2 U1 ( .ip1(A[0]), .ip2(n4), .op(n1) );
  xnor2_1 U2 ( .ip1(n4), .ip2(A[0]), .op(DIFF[0]) );
  inv_2 U3 ( .ip(B[11]), .op(n2) );
  inv_2 U4 ( .ip(B[10]), .op(n3) );
  inv_2 U5 ( .ip(B[0]), .op(n4) );
  inv_2 U6 ( .ip(B[1]), .op(n5) );
  inv_2 U7 ( .ip(B[2]), .op(n6) );
  inv_2 U8 ( .ip(B[3]), .op(n7) );
  inv_2 U9 ( .ip(B[4]), .op(n8) );
  inv_2 U10 ( .ip(B[5]), .op(n9) );
  inv_2 U11 ( .ip(B[6]), .op(n10) );
  inv_2 U12 ( .ip(B[7]), .op(n11) );
  inv_2 U13 ( .ip(B[8]), .op(n12) );
  inv_2 U14 ( .ip(B[9]), .op(n13) );
endmodule


module usbf_ep_rf_0_DW01_sub_2 ( A, B, CI, DIFF, CO );
  input [11:0] A;
  input [11:0] B;
  output [11:0] DIFF;
  input CI;
  output CO;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12;
  wire   [12:0] carry;

  fulladder U2_8 ( .a(A[8]), .b(n4), .ci(carry[8]), .co(carry[9]), .s(DIFF[8])
         );
  fulladder U2_7 ( .a(A[7]), .b(n5), .ci(carry[7]), .co(carry[8]), .s(DIFF[7])
         );
  fulladder U2_6 ( .a(A[6]), .b(n6), .ci(carry[6]), .co(carry[7]), .s(DIFF[6])
         );
  fulladder U2_5 ( .a(A[5]), .b(n7), .ci(carry[5]), .co(carry[6]), .s(DIFF[5])
         );
  fulladder U2_4 ( .a(A[4]), .b(n8), .ci(carry[4]), .co(carry[5]), .s(DIFF[4])
         );
  fulladder U2_3 ( .a(A[3]), .b(n9), .ci(carry[3]), .co(carry[4]), .s(DIFF[3])
         );
  fulladder U2_2 ( .a(A[2]), .b(n10), .ci(carry[2]), .co(carry[3]), .s(DIFF[2]) );
  fulladder U2_1 ( .a(A[1]), .b(n11), .ci(n2), .co(carry[2]), .s(DIFF[1]) );
  or2_2 U1 ( .ip1(carry[9]), .ip2(A[9]), .op(n1) );
  or2_2 U2 ( .ip1(A[0]), .ip2(n12), .op(n2) );
  nor2_2 U3 ( .ip1(n1), .ip2(A[10]), .op(n3) );
  xnor2_1 U4 ( .ip1(A[10]), .ip2(n1), .op(DIFF[10]) );
  xor2_2 U5 ( .ip1(A[11]), .ip2(n3), .op(DIFF[11]) );
  xnor2_1 U6 ( .ip1(A[9]), .ip2(carry[9]), .op(DIFF[9]) );
  xnor2_1 U7 ( .ip1(n12), .ip2(A[0]), .op(DIFF[0]) );
  inv_2 U8 ( .ip(B[8]), .op(n4) );
  inv_2 U9 ( .ip(B[7]), .op(n5) );
  inv_2 U10 ( .ip(B[6]), .op(n6) );
  inv_2 U11 ( .ip(B[5]), .op(n7) );
  inv_2 U12 ( .ip(B[4]), .op(n8) );
  inv_2 U13 ( .ip(B[3]), .op(n9) );
  inv_2 U14 ( .ip(B[2]), .op(n10) );
  inv_2 U15 ( .ip(B[1]), .op(n11) );
  inv_2 U16 ( .ip(B[0]), .op(n12) );
endmodule


module usbf_ep_rf_0_DW01_inc_0 ( A, SUM );
  input [11:0] A;
  output [11:0] SUM;
  wire   n1, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16;

  inv_2 U1 ( .ip(A[10]), .op(n1) );
  inv_2 U2 ( .ip(A[0]), .op(SUM[0]) );
  inv_2 U3 ( .ip(A[3]), .op(n3) );
  inv_2 U4 ( .ip(A[4]), .op(n4) );
  inv_2 U5 ( .ip(A[7]), .op(n5) );
  inv_2 U6 ( .ip(A[8]), .op(n6) );
  xor2_1 U7 ( .ip1(A[9]), .ip2(n7), .op(SUM[9]) );
  xor2_1 U8 ( .ip1(A[8]), .ip2(n8), .op(SUM[8]) );
  nor2_1 U9 ( .ip1(n9), .ip2(n5), .op(n8) );
  xor2_1 U10 ( .ip1(n9), .ip2(n5), .op(SUM[7]) );
  xnor2_1 U11 ( .ip1(n10), .ip2(A[6]), .op(SUM[6]) );
  nand2_1 U12 ( .ip1(A[5]), .ip2(n11), .op(n10) );
  xor2_1 U13 ( .ip1(A[5]), .ip2(n11), .op(SUM[5]) );
  xor2_1 U14 ( .ip1(A[4]), .ip2(n12), .op(SUM[4]) );
  nor2_1 U15 ( .ip1(n13), .ip2(n3), .op(n12) );
  xor2_1 U16 ( .ip1(n13), .ip2(n3), .op(SUM[3]) );
  xnor2_1 U17 ( .ip1(n14), .ip2(A[2]), .op(SUM[2]) );
  nand2_1 U18 ( .ip1(A[1]), .ip2(A[0]), .op(n14) );
  xor2_1 U19 ( .ip1(A[1]), .ip2(A[0]), .op(SUM[1]) );
  xor2_1 U20 ( .ip1(A[11]), .ip2(n15), .op(SUM[11]) );
  nor2_1 U21 ( .ip1(n16), .ip2(n1), .op(n15) );
  xor2_1 U22 ( .ip1(n16), .ip2(n1), .op(SUM[10]) );
  nand2_1 U23 ( .ip1(n7), .ip2(A[9]), .op(n16) );
  nor3_1 U24 ( .ip1(n5), .ip2(n9), .ip3(n6), .op(n7) );
  nand3_1 U25 ( .ip1(A[5]), .ip2(n11), .ip3(A[6]), .op(n9) );
  nor3_1 U26 ( .ip1(n3), .ip2(n13), .ip3(n4), .op(n11) );
  nand3_1 U27 ( .ip1(A[1]), .ip2(A[0]), .ip3(A[2]), .op(n13) );
endmodule


module usbf_ep_rf_0_DW01_add_0 ( A, B, CI, SUM, CO );
  input [11:0] A;
  input [11:0] B;
  output [11:0] SUM;
  input CI;
  output CO;
  wire   n1, n2, n3;
  wire   [11:1] carry;

  fulladder U1_8 ( .a(A[8]), .b(B[8]), .ci(carry[8]), .co(carry[9]), .s(SUM[8]) );
  fulladder U1_7 ( .a(A[7]), .b(B[7]), .ci(carry[7]), .co(carry[8]), .s(SUM[7]) );
  fulladder U1_6 ( .a(A[6]), .b(B[6]), .ci(carry[6]), .co(carry[7]), .s(SUM[6]) );
  fulladder U1_5 ( .a(A[5]), .b(B[5]), .ci(carry[5]), .co(carry[6]), .s(SUM[5]) );
  fulladder U1_4 ( .a(A[4]), .b(B[4]), .ci(carry[4]), .co(carry[5]), .s(SUM[4]) );
  fulladder U1_3 ( .a(A[3]), .b(B[3]), .ci(carry[3]), .co(carry[4]), .s(SUM[3]) );
  fulladder U1_2 ( .a(A[2]), .b(B[2]), .ci(carry[2]), .co(carry[3]), .s(SUM[2]) );
  fulladder U1_1 ( .a(A[1]), .b(B[1]), .ci(n2), .co(carry[2]), .s(SUM[1]) );
  and2_2 U1 ( .ip1(A[9]), .ip2(carry[9]), .op(n1) );
  and2_2 U2 ( .ip1(B[0]), .ip2(A[0]), .op(n2) );
  and2_2 U3 ( .ip1(A[10]), .ip2(n1), .op(n3) );
  xor2_2 U4 ( .ip1(A[11]), .ip2(n3), .op(SUM[11]) );
  xor2_2 U5 ( .ip1(A[10]), .ip2(n1), .op(SUM[10]) );
  xor2_2 U6 ( .ip1(A[9]), .ip2(carry[9]), .op(SUM[9]) );
  xor2_2 U7 ( .ip1(B[0]), .ip2(A[0]), .op(SUM[0]) );
endmodule


module usbf_ep_rf_0_DW01_dec_0 ( A, SUM );
  input [11:0] A;
  output [11:0] SUM;
  wire   n1, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21;

  inv_2 U1 ( .ip(n21), .op(n1) );
  inv_2 U2 ( .ip(A[0]), .op(SUM[0]) );
  nand2_1 U3 ( .ip1(n1), .ip2(n3), .op(SUM[9]) );
  nand2_1 U4 ( .ip1(A[9]), .ip2(n4), .op(n3) );
  nand2_1 U5 ( .ip1(n4), .ip2(n5), .op(SUM[8]) );
  nand2_1 U6 ( .ip1(A[8]), .ip2(n6), .op(n5) );
  nand2_1 U7 ( .ip1(n6), .ip2(n7), .op(SUM[7]) );
  nand2_1 U8 ( .ip1(A[7]), .ip2(n8), .op(n7) );
  nand2_1 U9 ( .ip1(n8), .ip2(n9), .op(SUM[6]) );
  nand2_1 U10 ( .ip1(A[6]), .ip2(n10), .op(n9) );
  nand2_1 U11 ( .ip1(n10), .ip2(n11), .op(SUM[5]) );
  nand2_1 U12 ( .ip1(A[5]), .ip2(n12), .op(n11) );
  nand2_1 U13 ( .ip1(n12), .ip2(n13), .op(SUM[4]) );
  nand2_1 U14 ( .ip1(A[4]), .ip2(n14), .op(n13) );
  nand2_1 U15 ( .ip1(n14), .ip2(n15), .op(SUM[3]) );
  nand2_1 U16 ( .ip1(A[3]), .ip2(n16), .op(n15) );
  nand2_1 U17 ( .ip1(n16), .ip2(n17), .op(SUM[2]) );
  nand2_1 U18 ( .ip1(A[2]), .ip2(n18), .op(n17) );
  nand2_1 U19 ( .ip1(n18), .ip2(n19), .op(SUM[1]) );
  nand2_1 U20 ( .ip1(A[1]), .ip2(A[0]), .op(n19) );
  xor2_1 U21 ( .ip1(A[11]), .ip2(n20), .op(SUM[11]) );
  nor2_1 U22 ( .ip1(A[10]), .ip2(n1), .op(n20) );
  xor2_1 U23 ( .ip1(A[10]), .ip2(n21), .op(SUM[10]) );
  nor2_1 U24 ( .ip1(n4), .ip2(A[9]), .op(n21) );
  or2_1 U25 ( .ip1(n6), .ip2(A[8]), .op(n4) );
  or2_1 U26 ( .ip1(n8), .ip2(A[7]), .op(n6) );
  or2_1 U27 ( .ip1(n10), .ip2(A[6]), .op(n8) );
  or2_1 U28 ( .ip1(n12), .ip2(A[5]), .op(n10) );
  or2_1 U29 ( .ip1(n14), .ip2(A[4]), .op(n12) );
  or2_1 U30 ( .ip1(n16), .ip2(A[3]), .op(n14) );
  or2_1 U31 ( .ip1(n18), .ip2(A[2]), .op(n16) );
  or2_1 U32 ( .ip1(A[1]), .ip2(A[0]), .op(n18) );
endmodule


module usbf_ep_rf_0 ( clk, wclk, rst, adr, re, we, din, dout, inta, intb, 
        dma_req, dma_ack, idin, ep_sel, ep_match, buf0_rl, buf0_set, buf1_set, 
        uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, 
        int_crc16_set, int_to_set, int_seqerr_set, out_to_small, csr, buf0, 
        buf1, dma_in_buf_sz1, dma_out_buf_avail );
  input [1:0] adr;
  input [31:0] din;
  output [31:0] dout;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  input clk, wclk, rst, re, we, dma_ack, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small;
  output inta, intb, dma_req, ep_match, dma_in_buf_sz1, dma_out_buf_avail;
  wire   n1141, int__29, int__28, int__27, int__26, int__25, int__24, int__21,
         int__20, int__19, int__18, int__17, int__16, ep_match_r, N191, int_re,
         N221, N222, set_r, N229, N230, N231, N232, N233, N234, N235, N236,
         N237, N238, N239, N240, N242, N243, N244, N245, N246, N247, N248,
         N249, N250, N251, N252, N253, N271, N272, dma_req_out_hold, N278,
         N279, N280, N281, N282, N283, N284, N285, N286, N287, N288, N289,
         N291, N292, N293, N294, N295, N296, N297, N298, N299, N300, N301,
         N302, N319, N320, N321, N322, N323, N324, N325, N326, N327, N328,
         N329, N330, N331, N332, N333, N336, N337, N338, N339, N340, N341,
         N342, N343, N344, N345, N346, N347, dma_req_in_hold2, N348,
         dma_req_in_hold, r1, r2, r4, r5, N361, dma_ack_clr1, dma_ack_wr1, n77,
         n126, n127, n128, n129, n130, n131, n132, n133, n134, n135, n136,
         n137, n138, n139, n140, n141, n142, n143, n144, n145, n146, n147,
         n148, n149, n150, n151, n152, n153, n154, n155, n156, n157, n158,
         n159, n160, n161, n162, n163, n164, n165, n166, n168, n169, n171,
         n172, n174, n175, n177, n178, n180, n181, n183, n184, n186, n187,
         n189, n190, n192, n193, n195, n196, n198, n199, n201, n202, n205,
         n206, n207, n208, n209, n210, n211, n212, n213, n214, n215, n216,
         n217, n218, n219, n220, n222, n223, n224, n225, n226, n227, n228,
         n229, n230, n231, n232, n233, n234, n235, n236, n237, n238, n239,
         n240, n241, n242, n243, n244, n245, n246, n247, n248, n249, n250,
         n251, n252, n253, n254, n255, n256, n257, n258, n259, n260, n261,
         n262, n263, n264, n265, n266, n267, n268, n269, n270, n271, n272,
         n273, n274, n275, n276, n277, n278, n279, n280, n281, n282, n283,
         n284, n285, n286, n287, n288, n289, n290, n291, n292, n293, n294,
         n295, n296, n297, n298, n299, n300, n301, n302, n303, n304, n305,
         n306, n307, n308, n309, n310, n311, n312, n313, n314, n315, n316,
         n317, n318, n319, n320, n321, n322, n323, n324, n325, n326, n327,
         n328, n329, n330, n331, n332, n333, n334, n335, n336, n337, n338,
         n339, n340, n341, n342, n343, n344, n345, n346, n347, n348, n349,
         n350, n351, n352, n353, n354, n355, n356, n357, n358, n359, n360,
         n361, n362, n363, n364, n365, n366, n367, n368, n369, n370, n371,
         n372, n373, n374, n375, n376, n377, n378, n379, n380, n381, n382,
         n383, n384, n385, n386, n387, n388, n389, n390, n391, n392, n393,
         n394, n395, n396, n397, n398, n399, n400, n401, n402, n403, n404,
         n405, n406, n407, n408, n409, n410, n411, n412, n413, n414, n415,
         n416, n417, n418, n419, n420, n421, n422, n423, n424, n425, n426,
         n427, n428, n429, n430, n431, n432, n433, n434, n435, n436, n437,
         n438, n439, n440, n441, n442, n443, n444, n445, n446, n447, n448,
         n449, n450, n451, n452, n453, n454, n455, n456, n457, n458, n459,
         n460, n461, n462, n463, n464, n465, n466, n467, n468, n469, n470,
         n471, n472, n473, n474, n475, n476, n477, n478, n479, n480, n481,
         n482, n483, n484, n485, n486, n487, n488, n489, n490, n491, n492,
         n493, n494, n495, n496, n497, n498, n499, n500, n501, n502, n503,
         n504, n505, n506, n507, n508, n509, n510, n511, n512, n513, n514,
         n515, n516, n517, n518, n519, n520, n521, n522, n523, n524, n525,
         n526, n527, n528, n529, n530, n531, n532, n533, n534, n535, n536,
         n537, n538, n539, n542, n543, n544, n545, n546, n547, n548, n549,
         n550, n551, n552, n553, n554, n555, n556, n557, n558, n559, n560,
         n561, n562, n563, n564, n565, n566, n567, n568, n569, n570, n571,
         n572, n573, n574, n575, n576, n577, n578, n579, n580, n581, n582,
         n583, n584, n585, n586, n587, n588, n589, n590, n591, n592, n593,
         n594, n595, n596, n597, n598, n599, n600, n601, n602, n603, n604,
         n605, n606, n607, n608, n609, n610, n611, n612, n613, n614, n615,
         n616, n617, n618, n619, n620, n621, n622, n623, n624, n625, n626,
         n627, n628, n629, n630, n631, n632, n633, n634, n635, n636, n637,
         n638, n639, n640, n641, n642, n643, n644, n645, n646, n647, n648,
         n649, n650, n651, n652, n653, n654, n655, n656, n657, n658, n659,
         n660, n661, n662, n663, n664, n665, n666, n667, n668, n669, n670,
         n671, n672, n673, n674, n675, n676, n677, n678, n679, n680, n681,
         n682, n683, n684, n685, n686, n687, n688, n689, n690, n691, n692,
         n693, n694, n695, n696, n697, n698, n699, n700, n701, n702, n703,
         n704, n705, n714, n715, n716, n717, n718, n719, n720, n721, n726,
         n727, n728, n733, n734, n735, n736, n737, n738, n739, n740, n741,
         n742, n743, n744, n745, n746, n747, n748, n749, n750, n751, n752,
         n753, n754, n755, n756, n757, n758, n759, n760, n761, n762, n763,
         n764, n765, n766, n767, n768, n769, n770, n771, n772, n773, n774,
         n775, n776, n777, n778, n779, n780, n781, n782, n783, n784, n785,
         n786, n787, n788, n789, n790, n791, n792, n793, n794, n795, n796,
         n797, n798, n799, n800, n801, n802, n803, n804, n805, n806, n807,
         n808, n809, n810, n811, n812, n813, n814, n815, n816, n817, n818,
         n819, n820, n821, n822, n823, n824, n825, n826, n827, n828, n829,
         n830, n831, n832, n833, n834, n835, n836, n837, n838, n839, n840,
         n841, n842, n843, n844, n845, n846, n847, n848, n849, n850, n851,
         n852, n853, n854, n855, n856, n857, n858, n859, n860, n861, n862,
         n863, n864, n865, n866, n867, n868, n869, n870, n871, n872, n873,
         n874, n875, n876, n877, n878, n879, n880, n881, n882, n883, n884,
         n885, n886, n887, n888, n889, n890, n891, n892, n893, n894, n895,
         n896, n897, n898, n899, n900, n901, n902, n903, n904, n905, n906,
         n907, n1, n2, n3, n4, n5, n7, n8, n11, n12, n13, n14, n15, n16, n17,
         n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31,
         n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45,
         n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59,
         n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73,
         n74, n75, n76, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87, n88,
         n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100, n101,
         n102, n104, n105, n106, n107, n108, n109, n110, n111, n112, n113,
         n114, n115, n116, n117, n118, n119, n120, n121, n122, n123, n124,
         n125, n167, n170, n173, n176, n179, n182, n185, n188, n191, n194,
         n197, n200, n203, n204, n221, n540, n541, n706, n707, n708, n709,
         n710, n711, n712, n713, n722, n723, n724, n725, n729, n730, n731,
         n732, n908, n909, n910, n911, n912, n913, n914, n915, n916, n917,
         n918, n919, n920, n921, n922, n923, n924, n925, n926, n927, n928,
         n929, n930, n931, n932, n933, n934, n935, n936, n937, n938, n939,
         n940, n941, n942, n943, n944, n945, n946, n947, n948, n949, n950,
         n951, n952, n953, n954, n955, n956, n957, n958, n959, n960, n961,
         n962, n963, n964, n965, n966, n967, n968, n969, n970, n971, n972,
         n973, n974, n975, n976, n977, n978, n979, n980, n981, n982, n983,
         n984, n985, n986, n987, n988, n989, n990, n991, n992, n993, n994,
         n995, n996, n997, n998, n999, n1000, n1001, n1002, n1003, n1004,
         n1005, n1006, n1007, n1008, n1009, n1010, n1011, n1012, n1013, n1014,
         n1015, n1016, n1017, n1018, n1019, n1020, n1021, n1022, n1023, n1024,
         n1025, n1026, n1027, n1028, n1029, n1030, n1031, n1032, n1033, n1034,
         n1035, n1036, n1037, n1038, n1039, n1040, n1041, n1042, n1043, n1044,
         n1045, n1046, n1047, n1048, n1049, n1050, n1051, n1052, n1053, n1054,
         n1055, n1056, n1057, n1058, n1059, n1060, n1061, n1062, n1063, n1064,
         n1065, n1066, n1067, n1068, n1069, n1070, n1071, n1072, n1073, n1074,
         n1075, n1076, n1077, n1078, n1079, n1080, n1081, n1082, n1083, n1084,
         n1085, n1086, n1087, n1088, n1089, n1090, n1091, n1092, n1093, n1094,
         n1095, n1096, n1097, n1098, n1099, n1100, n1101, n1102, n1103, n1104,
         n1105, n1106, n1107, n1108, n1109, n1110, n1111, n1112, n1113, n1114,
         n1115, n1116, n1117, n1118, n1119, n1120, n1121, n1122, n1123, n1124,
         n1125, n1126, n1127, n1128, n1129, n1130, n1131, n1132, n1133, n1134,
         n1135, n1136, n1137, n1138, n1139, n1140;
  wire   [6:0] int_;
  wire   [31:0] buf0_orig;
  wire   [11:0] dma_out_cnt;
  wire   [11:0] dma_in_cnt;
  wire   [11:0] dma_out_left;
  wire   [11:0] buf0_orig_m3;
  assign csr[14] = 1'b0;

  nand3_2 U24 ( .ip1(n126), .ip2(n127), .ip3(n128), .op(n737) );
  nand2_2 U25 ( .ip1(dma_out_cnt[10]), .ip2(n101), .op(n128) );
  nand2_2 U26 ( .ip1(N252), .ip2(n36), .op(n127) );
  nand2_2 U27 ( .ip1(N239), .ip2(n100), .op(n126) );
  nand3_2 U28 ( .ip1(n132), .ip2(n133), .ip3(n134), .op(n738) );
  nand2_2 U29 ( .ip1(dma_out_cnt[9]), .ip2(n101), .op(n134) );
  nand2_2 U30 ( .ip1(N251), .ip2(n130), .op(n133) );
  nand2_2 U31 ( .ip1(N238), .ip2(n99), .op(n132) );
  nand3_2 U32 ( .ip1(n135), .ip2(n136), .ip3(n137), .op(n739) );
  nand2_2 U33 ( .ip1(dma_out_cnt[8]), .ip2(n101), .op(n137) );
  nand2_2 U34 ( .ip1(N250), .ip2(n36), .op(n136) );
  nand2_2 U35 ( .ip1(N237), .ip2(n99), .op(n135) );
  nand3_2 U36 ( .ip1(n138), .ip2(n139), .ip3(n140), .op(n740) );
  nand2_2 U37 ( .ip1(dma_out_cnt[7]), .ip2(n101), .op(n140) );
  nand2_2 U38 ( .ip1(N249), .ip2(n130), .op(n139) );
  nand2_2 U39 ( .ip1(N236), .ip2(n99), .op(n138) );
  nand3_2 U40 ( .ip1(n141), .ip2(n142), .ip3(n143), .op(n741) );
  nand2_2 U41 ( .ip1(dma_out_cnt[6]), .ip2(n101), .op(n143) );
  nand2_2 U42 ( .ip1(N248), .ip2(n36), .op(n142) );
  nand2_2 U43 ( .ip1(N235), .ip2(n99), .op(n141) );
  nand3_2 U44 ( .ip1(n144), .ip2(n145), .ip3(n146), .op(n742) );
  nand2_2 U45 ( .ip1(dma_out_cnt[5]), .ip2(n101), .op(n146) );
  nand2_2 U46 ( .ip1(N247), .ip2(n130), .op(n145) );
  nand2_2 U47 ( .ip1(N234), .ip2(n99), .op(n144) );
  nand3_2 U48 ( .ip1(n147), .ip2(n148), .ip3(n149), .op(n743) );
  nand2_2 U49 ( .ip1(dma_out_cnt[4]), .ip2(n101), .op(n149) );
  nand2_2 U50 ( .ip1(N246), .ip2(n36), .op(n148) );
  nand2_2 U51 ( .ip1(N233), .ip2(n99), .op(n147) );
  nand3_2 U52 ( .ip1(n150), .ip2(n151), .ip3(n152), .op(n744) );
  nand2_2 U53 ( .ip1(dma_out_cnt[3]), .ip2(n101), .op(n152) );
  nand2_2 U54 ( .ip1(N245), .ip2(n130), .op(n151) );
  nand2_2 U55 ( .ip1(N232), .ip2(n99), .op(n150) );
  nand3_2 U56 ( .ip1(n153), .ip2(n154), .ip3(n155), .op(n745) );
  nand2_2 U57 ( .ip1(dma_out_cnt[2]), .ip2(n101), .op(n155) );
  nand2_2 U58 ( .ip1(N244), .ip2(n130), .op(n154) );
  nand2_2 U59 ( .ip1(N231), .ip2(n99), .op(n153) );
  nand3_2 U60 ( .ip1(n156), .ip2(n157), .ip3(n158), .op(n746) );
  nand2_2 U61 ( .ip1(dma_out_cnt[1]), .ip2(n101), .op(n158) );
  nand2_2 U62 ( .ip1(N243), .ip2(n130), .op(n157) );
  nand2_2 U63 ( .ip1(N230), .ip2(n99), .op(n156) );
  nand3_2 U64 ( .ip1(n159), .ip2(n160), .ip3(n161), .op(n747) );
  nand2_2 U65 ( .ip1(dma_out_cnt[0]), .ip2(n102), .op(n161) );
  nand2_2 U66 ( .ip1(N242), .ip2(n130), .op(n160) );
  nand2_2 U67 ( .ip1(N229), .ip2(n99), .op(n159) );
  nand3_2 U68 ( .ip1(n162), .ip2(n163), .ip3(n164), .op(n748) );
  nand2_2 U69 ( .ip1(dma_out_cnt[11]), .ip2(n101), .op(n164) );
  nand2_2 U70 ( .ip1(N253), .ip2(n130), .op(n163) );
  nand2_2 U71 ( .ip1(N240), .ip2(n99), .op(n162) );
  nand2_2 U74 ( .ip1(N301), .ip2(n36), .op(n166) );
  nand2_2 U75 ( .ip1(N288), .ip2(n100), .op(n165) );
  nand2_2 U78 ( .ip1(N300), .ip2(n130), .op(n169) );
  nand2_2 U79 ( .ip1(N287), .ip2(n100), .op(n168) );
  nand2_2 U82 ( .ip1(N299), .ip2(n36), .op(n172) );
  nand2_2 U83 ( .ip1(N286), .ip2(n100), .op(n171) );
  nand2_2 U86 ( .ip1(N298), .ip2(n36), .op(n175) );
  nand2_2 U87 ( .ip1(N285), .ip2(n100), .op(n174) );
  nand2_2 U90 ( .ip1(N297), .ip2(n36), .op(n178) );
  nand2_2 U91 ( .ip1(N284), .ip2(n100), .op(n177) );
  nand2_2 U94 ( .ip1(N296), .ip2(n130), .op(n181) );
  nand2_2 U95 ( .ip1(N283), .ip2(n100), .op(n180) );
  nand2_2 U98 ( .ip1(N295), .ip2(n36), .op(n184) );
  nand2_2 U99 ( .ip1(N282), .ip2(n100), .op(n183) );
  nand2_2 U102 ( .ip1(N294), .ip2(n130), .op(n187) );
  nand2_2 U103 ( .ip1(N281), .ip2(n100), .op(n186) );
  nand2_2 U106 ( .ip1(N293), .ip2(n36), .op(n190) );
  nand2_2 U107 ( .ip1(N280), .ip2(n100), .op(n189) );
  nand2_2 U110 ( .ip1(N292), .ip2(n36), .op(n193) );
  nand2_2 U111 ( .ip1(N279), .ip2(n100), .op(n192) );
  nand2_2 U114 ( .ip1(N291), .ip2(n36), .op(n196) );
  nand2_2 U115 ( .ip1(N278), .ip2(n100), .op(n195) );
  nand2_2 U119 ( .ip1(N302), .ip2(n36), .op(n199) );
  nor4_2 U120 ( .ip1(n1071), .ip2(n1099), .ip3(r5), .ip4(n201), .op(n130) );
  nor2_2 U121 ( .ip1(set_r), .ip2(n1105), .op(n201) );
  nand2_2 U122 ( .ip1(N289), .ip2(n99), .op(n198) );
  nor2_2 U123 ( .ip1(n985), .ip2(n1071), .op(n131) );
  nand2_2 U127 ( .ip1(n205), .ip2(n206), .op(n762) );
  nand3_2 U128 ( .ip1(n207), .ip2(n208), .ip3(idin[0]), .op(n206) );
  nand2_2 U129 ( .ip1(csr[30]), .ip2(n1101), .op(n205) );
  nand2_2 U130 ( .ip1(n209), .ip2(n210), .op(n763) );
  nand3_2 U131 ( .ip1(n207), .ip2(n208), .ip3(idin[1]), .op(n210) );
  nand2_2 U132 ( .ip1(csr[31]), .ip2(n1101), .op(n209) );
  nand2_2 U133 ( .ip1(rst), .ip2(n211), .op(n208) );
  nand2_2 U134 ( .ip1(uc_bsel_set), .ip2(ep_match_r), .op(n211) );
  nand2_2 U135 ( .ip1(n212), .ip2(n213), .op(n764) );
  nand3_2 U136 ( .ip1(n207), .ip2(n214), .ip3(idin[2]), .op(n213) );
  nand2_2 U137 ( .ip1(csr[28]), .ip2(n1100), .op(n212) );
  nand2_2 U138 ( .ip1(n215), .ip2(n216), .op(n765) );
  nand3_2 U139 ( .ip1(n207), .ip2(n214), .ip3(idin[3]), .op(n216) );
  nand2_2 U140 ( .ip1(csr[29]), .ip2(n1100), .op(n215) );
  nand2_2 U141 ( .ip1(rst), .ip2(n217), .op(n214) );
  nand2_2 U142 ( .ip1(uc_dpd_set), .ip2(ep_match_r), .op(n217) );
  nand2_2 U143 ( .ip1(n218), .ip2(n219), .op(n766) );
  nand2_2 U144 ( .ip1(int_[0]), .ip2(n1098), .op(n219) );
  nand2_2 U145 ( .ip1(n1), .ip2(n220), .op(n218) );
  nand2_2 U146 ( .ip1(n222), .ip2(n223), .op(n220) );
  nand2_2 U147 ( .ip1(int_to_set), .ip2(n1), .op(n223) );
  nand2_2 U148 ( .ip1(n224), .ip2(n225), .op(n767) );
  nand2_2 U149 ( .ip1(int_[1]), .ip2(n1097), .op(n225) );
  nand2_2 U150 ( .ip1(n1), .ip2(n226), .op(n224) );
  nand2_2 U151 ( .ip1(n222), .ip2(n227), .op(n226) );
  nand2_2 U152 ( .ip1(int_crc16_set), .ip2(n1), .op(n227) );
  nand2_2 U153 ( .ip1(n228), .ip2(n229), .op(n768) );
  nand2_2 U154 ( .ip1(int_[2]), .ip2(n1096), .op(n229) );
  nand2_2 U155 ( .ip1(n1), .ip2(n230), .op(n228) );
  nand2_2 U156 ( .ip1(n222), .ip2(n231), .op(n230) );
  nand2_2 U157 ( .ip1(int_upid_set), .ip2(n1), .op(n231) );
  nand2_2 U158 ( .ip1(n232), .ip2(n233), .op(n769) );
  or2_2 U159 ( .ip1(n1103), .ip2(n234), .op(n233) );
  nand2_2 U160 ( .ip1(n1), .ip2(n234), .op(n232) );
  nand2_2 U161 ( .ip1(n222), .ip2(n235), .op(n234) );
  nand2_2 U162 ( .ip1(int_buf0_set), .ip2(n1), .op(n235) );
  nand2_2 U163 ( .ip1(n236), .ip2(n237), .op(n770) );
  or2_2 U164 ( .ip1(n1102), .ip2(n238), .op(n237) );
  nand2_2 U165 ( .ip1(n1), .ip2(n238), .op(n236) );
  nand2_2 U166 ( .ip1(n222), .ip2(n239), .op(n238) );
  nand2_2 U167 ( .ip1(int_buf1_set), .ip2(n1), .op(n239) );
  nand2_2 U168 ( .ip1(n240), .ip2(n241), .op(n771) );
  nand2_2 U169 ( .ip1(int_[5]), .ip2(n1095), .op(n241) );
  nand2_2 U170 ( .ip1(n1), .ip2(n242), .op(n240) );
  nand2_2 U171 ( .ip1(n222), .ip2(n243), .op(n242) );
  nand2_2 U172 ( .ip1(int_seqerr_set), .ip2(n1), .op(n243) );
  nand2_2 U173 ( .ip1(n244), .ip2(n245), .op(n772) );
  nand2_2 U174 ( .ip1(int_[6]), .ip2(n1094), .op(n245) );
  nand2_2 U175 ( .ip1(n1), .ip2(n246), .op(n244) );
  nand2_2 U176 ( .ip1(n222), .ip2(n247), .op(n246) );
  nand2_2 U177 ( .ip1(out_to_small), .ip2(n1), .op(n247) );
  nor2_2 U178 ( .ip1(n77), .ip2(int_re), .op(n222) );
  nand4_2 U180 ( .ip1(rst), .ip2(n248), .ip3(n249), .ip4(n250), .op(n773) );
  nand2_2 U181 ( .ip1(idin[31]), .ip2(n97), .op(n250) );
  nand2_2 U182 ( .ip1(din[31]), .ip2(n52), .op(n249) );
  nand2_2 U183 ( .ip1(buf1[31]), .ip2(n95), .op(n248) );
  nand4_2 U184 ( .ip1(rst), .ip2(n253), .ip3(n254), .ip4(n255), .op(n774) );
  nand2_2 U185 ( .ip1(idin[30]), .ip2(n96), .op(n255) );
  nand2_2 U186 ( .ip1(din[30]), .ip2(n52), .op(n254) );
  nand2_2 U187 ( .ip1(buf1[30]), .ip2(n95), .op(n253) );
  nand4_2 U188 ( .ip1(rst), .ip2(n256), .ip3(n257), .ip4(n258), .op(n775) );
  nand2_2 U189 ( .ip1(idin[29]), .ip2(n96), .op(n258) );
  nand2_2 U190 ( .ip1(din[29]), .ip2(n52), .op(n257) );
  nand2_2 U191 ( .ip1(buf1[29]), .ip2(n95), .op(n256) );
  nand4_2 U192 ( .ip1(rst), .ip2(n259), .ip3(n260), .ip4(n261), .op(n776) );
  nand2_2 U193 ( .ip1(idin[28]), .ip2(n96), .op(n261) );
  nand2_2 U194 ( .ip1(din[28]), .ip2(n52), .op(n260) );
  nand2_2 U195 ( .ip1(buf1[28]), .ip2(n95), .op(n259) );
  nand4_2 U196 ( .ip1(rst), .ip2(n262), .ip3(n263), .ip4(n264), .op(n777) );
  nand2_2 U197 ( .ip1(idin[27]), .ip2(n96), .op(n264) );
  nand2_2 U198 ( .ip1(din[27]), .ip2(n52), .op(n263) );
  nand2_2 U199 ( .ip1(buf1[27]), .ip2(n95), .op(n262) );
  nand4_2 U200 ( .ip1(rst), .ip2(n265), .ip3(n266), .ip4(n267), .op(n778) );
  nand2_2 U201 ( .ip1(idin[26]), .ip2(n96), .op(n267) );
  nand2_2 U202 ( .ip1(din[26]), .ip2(n52), .op(n266) );
  nand2_2 U203 ( .ip1(buf1[26]), .ip2(n95), .op(n265) );
  nand4_2 U204 ( .ip1(rst), .ip2(n268), .ip3(n269), .ip4(n270), .op(n779) );
  nand2_2 U205 ( .ip1(idin[25]), .ip2(n96), .op(n270) );
  nand2_2 U206 ( .ip1(din[25]), .ip2(n52), .op(n269) );
  nand2_2 U207 ( .ip1(buf1[25]), .ip2(n95), .op(n268) );
  nand4_2 U208 ( .ip1(rst), .ip2(n271), .ip3(n272), .ip4(n273), .op(n780) );
  nand2_2 U209 ( .ip1(idin[24]), .ip2(n96), .op(n273) );
  nand2_2 U210 ( .ip1(din[24]), .ip2(n52), .op(n272) );
  nand2_2 U211 ( .ip1(buf1[24]), .ip2(n95), .op(n271) );
  nand4_2 U212 ( .ip1(rst), .ip2(n274), .ip3(n275), .ip4(n276), .op(n781) );
  nand2_2 U213 ( .ip1(idin[23]), .ip2(n96), .op(n276) );
  nand2_2 U214 ( .ip1(din[23]), .ip2(n52), .op(n275) );
  nand2_2 U215 ( .ip1(buf1[23]), .ip2(n94), .op(n274) );
  nand4_2 U216 ( .ip1(rst), .ip2(n277), .ip3(n278), .ip4(n279), .op(n782) );
  nand2_2 U217 ( .ip1(idin[22]), .ip2(n96), .op(n279) );
  nand2_2 U218 ( .ip1(din[22]), .ip2(n52), .op(n278) );
  nand2_2 U219 ( .ip1(buf1[22]), .ip2(n94), .op(n277) );
  nand4_2 U220 ( .ip1(rst), .ip2(n280), .ip3(n281), .ip4(n282), .op(n783) );
  nand2_2 U221 ( .ip1(idin[21]), .ip2(n96), .op(n282) );
  nand2_2 U222 ( .ip1(din[21]), .ip2(n52), .op(n281) );
  nand2_2 U223 ( .ip1(buf1[21]), .ip2(n94), .op(n280) );
  nand4_2 U224 ( .ip1(rst), .ip2(n283), .ip3(n284), .ip4(n285), .op(n784) );
  nand2_2 U225 ( .ip1(idin[20]), .ip2(n96), .op(n285) );
  nand2_2 U226 ( .ip1(din[20]), .ip2(n52), .op(n284) );
  nand2_2 U227 ( .ip1(buf1[20]), .ip2(n94), .op(n283) );
  nand4_2 U228 ( .ip1(rst), .ip2(n286), .ip3(n287), .ip4(n288), .op(n785) );
  nand2_2 U229 ( .ip1(idin[19]), .ip2(n96), .op(n288) );
  nand2_2 U230 ( .ip1(din[19]), .ip2(n53), .op(n287) );
  nand2_2 U231 ( .ip1(buf1[19]), .ip2(n94), .op(n286) );
  nand4_2 U232 ( .ip1(rst), .ip2(n289), .ip3(n290), .ip4(n291), .op(n786) );
  nand2_2 U233 ( .ip1(idin[18]), .ip2(n97), .op(n291) );
  nand2_2 U234 ( .ip1(din[18]), .ip2(n53), .op(n290) );
  nand2_2 U235 ( .ip1(buf1[18]), .ip2(n94), .op(n289) );
  nand4_2 U236 ( .ip1(rst), .ip2(n292), .ip3(n293), .ip4(n294), .op(n787) );
  nand2_2 U237 ( .ip1(idin[17]), .ip2(n97), .op(n294) );
  nand2_2 U238 ( .ip1(din[17]), .ip2(n53), .op(n293) );
  nand2_2 U239 ( .ip1(buf1[17]), .ip2(n94), .op(n292) );
  nand4_2 U240 ( .ip1(rst), .ip2(n295), .ip3(n296), .ip4(n297), .op(n788) );
  nand2_2 U241 ( .ip1(idin[16]), .ip2(n97), .op(n297) );
  nand2_2 U242 ( .ip1(din[16]), .ip2(n53), .op(n296) );
  nand2_2 U243 ( .ip1(buf1[16]), .ip2(n94), .op(n295) );
  nand4_2 U244 ( .ip1(rst), .ip2(n298), .ip3(n299), .ip4(n300), .op(n789) );
  nand2_2 U245 ( .ip1(idin[15]), .ip2(n97), .op(n300) );
  nand2_2 U246 ( .ip1(din[15]), .ip2(n53), .op(n299) );
  nand2_2 U247 ( .ip1(buf1[15]), .ip2(n94), .op(n298) );
  nand4_2 U248 ( .ip1(rst), .ip2(n301), .ip3(n302), .ip4(n303), .op(n790) );
  nand2_2 U249 ( .ip1(idin[14]), .ip2(n97), .op(n303) );
  nand2_2 U250 ( .ip1(din[14]), .ip2(n53), .op(n302) );
  nand2_2 U251 ( .ip1(buf1[14]), .ip2(n94), .op(n301) );
  nand4_2 U252 ( .ip1(rst), .ip2(n304), .ip3(n305), .ip4(n306), .op(n791) );
  nand2_2 U253 ( .ip1(idin[13]), .ip2(n97), .op(n306) );
  nand2_2 U254 ( .ip1(din[13]), .ip2(n53), .op(n305) );
  nand2_2 U255 ( .ip1(buf1[13]), .ip2(n94), .op(n304) );
  nand4_2 U256 ( .ip1(rst), .ip2(n307), .ip3(n308), .ip4(n309), .op(n792) );
  nand2_2 U257 ( .ip1(idin[12]), .ip2(n97), .op(n309) );
  nand2_2 U258 ( .ip1(din[12]), .ip2(n53), .op(n308) );
  nand2_2 U259 ( .ip1(buf1[12]), .ip2(n94), .op(n307) );
  nand4_2 U260 ( .ip1(rst), .ip2(n310), .ip3(n311), .ip4(n312), .op(n793) );
  nand2_2 U261 ( .ip1(idin[11]), .ip2(n97), .op(n312) );
  nand2_2 U262 ( .ip1(din[11]), .ip2(n53), .op(n311) );
  nand2_2 U263 ( .ip1(buf1[11]), .ip2(n93), .op(n310) );
  nand4_2 U264 ( .ip1(rst), .ip2(n313), .ip3(n314), .ip4(n315), .op(n794) );
  nand2_2 U265 ( .ip1(idin[10]), .ip2(n97), .op(n315) );
  nand2_2 U266 ( .ip1(din[10]), .ip2(n53), .op(n314) );
  nand2_2 U267 ( .ip1(buf1[10]), .ip2(n93), .op(n313) );
  nand4_2 U268 ( .ip1(rst), .ip2(n316), .ip3(n317), .ip4(n318), .op(n795) );
  nand2_2 U269 ( .ip1(idin[9]), .ip2(n97), .op(n318) );
  nand2_2 U270 ( .ip1(din[9]), .ip2(n53), .op(n317) );
  nand2_2 U271 ( .ip1(buf1[9]), .ip2(n93), .op(n316) );
  nand4_2 U272 ( .ip1(rst), .ip2(n319), .ip3(n320), .ip4(n321), .op(n796) );
  nand2_2 U273 ( .ip1(idin[8]), .ip2(n97), .op(n321) );
  nand2_2 U274 ( .ip1(din[8]), .ip2(n53), .op(n320) );
  nand2_2 U275 ( .ip1(buf1[8]), .ip2(n93), .op(n319) );
  nand4_2 U276 ( .ip1(rst), .ip2(n322), .ip3(n323), .ip4(n324), .op(n797) );
  nand2_2 U277 ( .ip1(idin[7]), .ip2(n98), .op(n324) );
  nand2_2 U278 ( .ip1(din[7]), .ip2(n54), .op(n323) );
  nand2_2 U279 ( .ip1(buf1[7]), .ip2(n93), .op(n322) );
  nand4_2 U280 ( .ip1(rst), .ip2(n325), .ip3(n326), .ip4(n327), .op(n798) );
  nand2_2 U281 ( .ip1(idin[6]), .ip2(n98), .op(n327) );
  nand2_2 U282 ( .ip1(din[6]), .ip2(n54), .op(n326) );
  nand2_2 U283 ( .ip1(buf1[6]), .ip2(n93), .op(n325) );
  nand4_2 U284 ( .ip1(rst), .ip2(n328), .ip3(n329), .ip4(n330), .op(n799) );
  nand2_2 U285 ( .ip1(idin[5]), .ip2(n98), .op(n330) );
  nand2_2 U286 ( .ip1(din[5]), .ip2(n54), .op(n329) );
  nand2_2 U287 ( .ip1(buf1[5]), .ip2(n93), .op(n328) );
  nand4_2 U288 ( .ip1(rst), .ip2(n331), .ip3(n332), .ip4(n333), .op(n800) );
  nand2_2 U289 ( .ip1(idin[4]), .ip2(n98), .op(n333) );
  nand2_2 U290 ( .ip1(din[4]), .ip2(n54), .op(n332) );
  nand2_2 U291 ( .ip1(buf1[4]), .ip2(n93), .op(n331) );
  nand4_2 U292 ( .ip1(rst), .ip2(n334), .ip3(n335), .ip4(n336), .op(n801) );
  nand2_2 U293 ( .ip1(n98), .ip2(idin[3]), .op(n336) );
  nand2_2 U294 ( .ip1(din[3]), .ip2(n54), .op(n335) );
  nand2_2 U295 ( .ip1(buf1[3]), .ip2(n93), .op(n334) );
  nand4_2 U296 ( .ip1(rst), .ip2(n337), .ip3(n338), .ip4(n339), .op(n802) );
  nand2_2 U297 ( .ip1(n98), .ip2(idin[2]), .op(n339) );
  nand2_2 U298 ( .ip1(din[2]), .ip2(n54), .op(n338) );
  nand2_2 U299 ( .ip1(buf1[2]), .ip2(n93), .op(n337) );
  nand4_2 U300 ( .ip1(rst), .ip2(n340), .ip3(n341), .ip4(n342), .op(n803) );
  nand2_2 U301 ( .ip1(n98), .ip2(idin[1]), .op(n342) );
  nand2_2 U302 ( .ip1(din[1]), .ip2(n54), .op(n341) );
  nand2_2 U303 ( .ip1(buf1[1]), .ip2(n93), .op(n340) );
  nand4_2 U304 ( .ip1(rst), .ip2(n343), .ip3(n344), .ip4(n345), .op(n804) );
  nand2_2 U305 ( .ip1(n98), .ip2(idin[0]), .op(n345) );
  nand2_2 U306 ( .ip1(din[0]), .ip2(n54), .op(n344) );
  nand2_2 U307 ( .ip1(buf1[0]), .ip2(n93), .op(n343) );
  nor2_2 U308 ( .ip1(n54), .ip2(n98), .op(n252) );
  and3_2 U309 ( .ip1(n346), .ip2(n347), .ip3(ep_match_r), .op(n251) );
  or2_2 U310 ( .ip1(buf1_set), .ip2(out_to_small), .op(n346) );
  nand2_2 U311 ( .ip1(we), .ip2(n90), .op(n347) );
  nand3_2 U312 ( .ip1(rst), .ip2(n349), .ip3(n350), .op(n805) );
  nor2_2 U313 ( .ip1(n89), .ip2(n1072), .op(n352) );
  nor2_2 U314 ( .ip1(n1124), .ip2(n86), .op(n351) );
  nand2_2 U315 ( .ip1(buf0[31]), .ip2(n83), .op(n349) );
  nand3_2 U316 ( .ip1(rst), .ip2(n356), .ip3(n357), .op(n806) );
  nor2_2 U317 ( .ip1(n89), .ip2(n986), .op(n359) );
  nor2_2 U318 ( .ip1(n1106), .ip2(n86), .op(n358) );
  nand2_2 U319 ( .ip1(buf0[30]), .ip2(n83), .op(n356) );
  nand3_2 U320 ( .ip1(rst), .ip2(n360), .ip3(n361), .op(n807) );
  nor2_2 U321 ( .ip1(n89), .ip2(n1067), .op(n363) );
  nor2_2 U322 ( .ip1(n1107), .ip2(n86), .op(n362) );
  nand2_2 U323 ( .ip1(buf0[29]), .ip2(n83), .op(n360) );
  nand3_2 U324 ( .ip1(rst), .ip2(n364), .ip3(n365), .op(n808) );
  nor2_2 U325 ( .ip1(n89), .ip2(n987), .op(n367) );
  nor2_2 U326 ( .ip1(n1108), .ip2(n86), .op(n366) );
  nand2_2 U327 ( .ip1(buf0[28]), .ip2(n83), .op(n364) );
  nand3_2 U328 ( .ip1(rst), .ip2(n368), .ip3(n369), .op(n809) );
  nor2_2 U329 ( .ip1(n89), .ip2(n993), .op(n371) );
  nor2_2 U330 ( .ip1(n1127), .ip2(n86), .op(n370) );
  nand2_2 U331 ( .ip1(buf0[27]), .ip2(n83), .op(n368) );
  nand3_2 U332 ( .ip1(rst), .ip2(n372), .ip3(n373), .op(n810) );
  nor2_2 U333 ( .ip1(n88), .ip2(n988), .op(n375) );
  nor2_2 U334 ( .ip1(n1128), .ip2(n86), .op(n374) );
  nand2_2 U335 ( .ip1(buf0[26]), .ip2(n83), .op(n372) );
  nand3_2 U336 ( .ip1(rst), .ip2(n376), .ip3(n377), .op(n811) );
  nor2_2 U337 ( .ip1(n88), .ip2(n992), .op(n379) );
  nor2_2 U338 ( .ip1(n1129), .ip2(n86), .op(n378) );
  nand2_2 U339 ( .ip1(buf0[25]), .ip2(n83), .op(n376) );
  nand3_2 U340 ( .ip1(rst), .ip2(n380), .ip3(n381), .op(n812) );
  nor2_2 U341 ( .ip1(n88), .ip2(n989), .op(n383) );
  nor2_2 U342 ( .ip1(n1130), .ip2(n85), .op(n382) );
  nand2_2 U343 ( .ip1(buf0[24]), .ip2(n83), .op(n380) );
  nand3_2 U344 ( .ip1(rst), .ip2(n384), .ip3(n385), .op(n813) );
  nor2_2 U345 ( .ip1(n88), .ip2(n991), .op(n387) );
  nor2_2 U346 ( .ip1(n1131), .ip2(n85), .op(n386) );
  nand2_2 U347 ( .ip1(buf0[23]), .ip2(n82), .op(n384) );
  nand3_2 U348 ( .ip1(rst), .ip2(n388), .ip3(n389), .op(n814) );
  nor2_2 U349 ( .ip1(n88), .ip2(n1073), .op(n391) );
  nor2_2 U350 ( .ip1(n1132), .ip2(n85), .op(n390) );
  nand2_2 U351 ( .ip1(buf0[22]), .ip2(n82), .op(n388) );
  nand3_2 U352 ( .ip1(rst), .ip2(n392), .ip3(n393), .op(n815) );
  nor2_2 U353 ( .ip1(n88), .ip2(n1066), .op(n395) );
  nor2_2 U354 ( .ip1(n1133), .ip2(n85), .op(n394) );
  nand2_2 U355 ( .ip1(buf0[21]), .ip2(n82), .op(n392) );
  nand3_2 U356 ( .ip1(rst), .ip2(n396), .ip3(n397), .op(n816) );
  nor2_2 U357 ( .ip1(n88), .ip2(n1074), .op(n399) );
  nor2_2 U358 ( .ip1(n1134), .ip2(n85), .op(n398) );
  nand2_2 U359 ( .ip1(buf0[20]), .ip2(n82), .op(n396) );
  nand3_2 U360 ( .ip1(rst), .ip2(n400), .ip3(n401), .op(n817) );
  nor2_2 U361 ( .ip1(n88), .ip2(n990), .op(n403) );
  nor2_2 U362 ( .ip1(n1135), .ip2(n85), .op(n402) );
  nand2_2 U363 ( .ip1(buf0[19]), .ip2(n82), .op(n400) );
  nand3_2 U364 ( .ip1(rst), .ip2(n404), .ip3(n405), .op(n818) );
  nor2_2 U365 ( .ip1(n88), .ip2(n1075), .op(n407) );
  nor2_2 U366 ( .ip1(n1136), .ip2(n85), .op(n406) );
  nand2_2 U367 ( .ip1(buf0[18]), .ip2(n82), .op(n404) );
  nand3_2 U368 ( .ip1(rst), .ip2(n408), .ip3(n409), .op(n819) );
  nor2_2 U369 ( .ip1(n88), .ip2(n1076), .op(n411) );
  nor2_2 U370 ( .ip1(n1137), .ip2(n85), .op(n410) );
  nand2_2 U371 ( .ip1(buf0[17]), .ip2(n82), .op(n408) );
  nand3_2 U372 ( .ip1(rst), .ip2(n412), .ip3(n413), .op(n820) );
  nor2_2 U373 ( .ip1(n88), .ip2(n1077), .op(n415) );
  nor2_2 U374 ( .ip1(n1111), .ip2(n85), .op(n414) );
  nand2_2 U375 ( .ip1(buf0[16]), .ip2(n82), .op(n412) );
  nand3_2 U376 ( .ip1(rst), .ip2(n416), .ip3(n417), .op(n821) );
  nor2_2 U377 ( .ip1(n88), .ip2(n1078), .op(n419) );
  nor2_2 U378 ( .ip1(n1112), .ip2(n85), .op(n418) );
  nand2_2 U379 ( .ip1(buf0[15]), .ip2(n82), .op(n416) );
  nand3_2 U380 ( .ip1(rst), .ip2(n420), .ip3(n421), .op(n822) );
  nor2_2 U381 ( .ip1(n88), .ip2(n1079), .op(n423) );
  nor2_2 U382 ( .ip1(n1113), .ip2(n85), .op(n422) );
  nand2_2 U383 ( .ip1(buf0[14]), .ip2(n82), .op(n420) );
  nand3_2 U384 ( .ip1(rst), .ip2(n424), .ip3(n425), .op(n823) );
  nor2_2 U385 ( .ip1(n88), .ip2(n1080), .op(n427) );
  nor2_2 U386 ( .ip1(n1114), .ip2(n85), .op(n426) );
  nand2_2 U387 ( .ip1(buf0[13]), .ip2(n82), .op(n424) );
  nand3_2 U388 ( .ip1(rst), .ip2(n428), .ip3(n429), .op(n824) );
  nor2_2 U389 ( .ip1(n87), .ip2(n1081), .op(n431) );
  nor2_2 U390 ( .ip1(n1115), .ip2(n85), .op(n430) );
  nand2_2 U391 ( .ip1(buf0[12]), .ip2(n82), .op(n428) );
  nand3_2 U392 ( .ip1(rst), .ip2(n432), .ip3(n433), .op(n825) );
  nor2_2 U393 ( .ip1(n87), .ip2(n1082), .op(n435) );
  nor2_2 U394 ( .ip1(n1116), .ip2(n84), .op(n434) );
  nand2_2 U395 ( .ip1(buf0[11]), .ip2(n81), .op(n432) );
  nand3_2 U396 ( .ip1(rst), .ip2(n436), .ip3(n437), .op(n826) );
  nor2_2 U397 ( .ip1(n87), .ip2(n1083), .op(n439) );
  nor2_2 U398 ( .ip1(n1117), .ip2(n84), .op(n438) );
  nand2_2 U399 ( .ip1(buf0[10]), .ip2(n81), .op(n436) );
  nand3_2 U400 ( .ip1(rst), .ip2(n440), .ip3(n441), .op(n827) );
  nor2_2 U401 ( .ip1(n87), .ip2(n1084), .op(n443) );
  nor2_2 U402 ( .ip1(n1118), .ip2(n84), .op(n442) );
  nand2_2 U403 ( .ip1(buf0[9]), .ip2(n81), .op(n440) );
  nand3_2 U404 ( .ip1(rst), .ip2(n444), .ip3(n445), .op(n828) );
  nor2_2 U405 ( .ip1(n87), .ip2(n1085), .op(n447) );
  nor2_2 U406 ( .ip1(n1119), .ip2(n84), .op(n446) );
  nand2_2 U407 ( .ip1(buf0[8]), .ip2(n81), .op(n444) );
  nand3_2 U408 ( .ip1(rst), .ip2(n448), .ip3(n449), .op(n829) );
  nor2_2 U409 ( .ip1(n87), .ip2(n1086), .op(n451) );
  nor2_2 U410 ( .ip1(n1120), .ip2(n84), .op(n450) );
  nand2_2 U411 ( .ip1(buf0[7]), .ip2(n81), .op(n448) );
  nand3_2 U412 ( .ip1(rst), .ip2(n452), .ip3(n453), .op(n830) );
  nor2_2 U413 ( .ip1(n87), .ip2(n1087), .op(n455) );
  nor2_2 U414 ( .ip1(n1121), .ip2(n84), .op(n454) );
  nand2_2 U415 ( .ip1(buf0[6]), .ip2(n81), .op(n452) );
  nand3_2 U416 ( .ip1(rst), .ip2(n456), .ip3(n457), .op(n831) );
  nor2_2 U417 ( .ip1(n87), .ip2(n1088), .op(n459) );
  nor2_2 U418 ( .ip1(n1122), .ip2(n84), .op(n458) );
  nand2_2 U419 ( .ip1(buf0[5]), .ip2(n81), .op(n456) );
  nand3_2 U420 ( .ip1(rst), .ip2(n460), .ip3(n461), .op(n832) );
  nor2_2 U421 ( .ip1(n87), .ip2(n1089), .op(n463) );
  nor2_2 U422 ( .ip1(n1123), .ip2(n84), .op(n462) );
  nand2_2 U423 ( .ip1(buf0[4]), .ip2(n81), .op(n460) );
  nand3_2 U424 ( .ip1(rst), .ip2(n464), .ip3(n465), .op(n833) );
  nor2_2 U425 ( .ip1(n87), .ip2(n1090), .op(n467) );
  nor2_2 U426 ( .ip1(n1109), .ip2(n84), .op(n466) );
  nand2_2 U427 ( .ip1(buf0[3]), .ip2(n81), .op(n464) );
  nand3_2 U428 ( .ip1(rst), .ip2(n468), .ip3(n469), .op(n834) );
  nor2_2 U429 ( .ip1(n87), .ip2(n1091), .op(n471) );
  nor2_2 U430 ( .ip1(n1110), .ip2(n84), .op(n470) );
  nand2_2 U431 ( .ip1(buf0[2]), .ip2(n81), .op(n468) );
  nand3_2 U432 ( .ip1(rst), .ip2(n472), .ip3(n473), .op(n835) );
  nor2_2 U433 ( .ip1(n87), .ip2(n1092), .op(n475) );
  nor2_2 U434 ( .ip1(n1125), .ip2(n84), .op(n474) );
  nand2_2 U435 ( .ip1(buf0[1]), .ip2(n81), .op(n472) );
  nand3_2 U436 ( .ip1(rst), .ip2(n476), .ip3(n477), .op(n836) );
  nor2_2 U437 ( .ip1(n87), .ip2(n1093), .op(n479) );
  nor2_2 U438 ( .ip1(n1126), .ip2(n84), .op(n478) );
  nand2_2 U439 ( .ip1(buf0[0]), .ip2(n81), .op(n476) );
  and3_2 U440 ( .ip1(n87), .ip2(n66), .ip3(n84), .op(n355) );
  nand4_2 U441 ( .ip1(buf0_set), .ip2(n207), .ip3(n66), .ip4(n1104), .op(n354)
         );
  nand3_2 U442 ( .ip1(n207), .ip2(n67), .ip3(buf0_rl), .op(n353) );
  nor2_2 U443 ( .ip1(n1099), .ip2(n77), .op(n207) );
  and2_2 U444 ( .ip1(din[0]), .ip2(n80), .op(n481) );
  and2_2 U445 ( .ip1(din[1]), .ip2(n80), .op(n482) );
  and2_2 U446 ( .ip1(din[2]), .ip2(n80), .op(n483) );
  and2_2 U447 ( .ip1(din[3]), .ip2(n80), .op(n484) );
  and2_2 U448 ( .ip1(din[4]), .ip2(n80), .op(n485) );
  and2_2 U449 ( .ip1(din[5]), .ip2(n80), .op(n486) );
  and2_2 U450 ( .ip1(din[6]), .ip2(n79), .op(n487) );
  and2_2 U451 ( .ip1(din[7]), .ip2(n79), .op(n488) );
  and2_2 U452 ( .ip1(din[8]), .ip2(n79), .op(n489) );
  and2_2 U453 ( .ip1(din[9]), .ip2(n79), .op(n490) );
  and2_2 U454 ( .ip1(din[10]), .ip2(n79), .op(n491) );
  and2_2 U455 ( .ip1(din[11]), .ip2(n79), .op(n492) );
  and2_2 U456 ( .ip1(din[12]), .ip2(n79), .op(n493) );
  and2_2 U457 ( .ip1(din[13]), .ip2(n79), .op(n494) );
  and2_2 U458 ( .ip1(din[14]), .ip2(n79), .op(n495) );
  and2_2 U459 ( .ip1(din[15]), .ip2(n79), .op(n496) );
  and2_2 U460 ( .ip1(din[16]), .ip2(n79), .op(n497) );
  and2_2 U461 ( .ip1(din[17]), .ip2(n79), .op(n498) );
  and2_2 U462 ( .ip1(din[18]), .ip2(n79), .op(n499) );
  and2_2 U463 ( .ip1(din[19]), .ip2(n78), .op(n500) );
  and2_2 U464 ( .ip1(din[20]), .ip2(n78), .op(n501) );
  and2_2 U465 ( .ip1(din[21]), .ip2(n78), .op(n502) );
  and2_2 U466 ( .ip1(din[22]), .ip2(n78), .op(n503) );
  and2_2 U467 ( .ip1(din[23]), .ip2(n78), .op(n504) );
  and2_2 U468 ( .ip1(din[24]), .ip2(n78), .op(n505) );
  and2_2 U469 ( .ip1(din[25]), .ip2(n78), .op(n506) );
  and2_2 U470 ( .ip1(din[26]), .ip2(n78), .op(n507) );
  and2_2 U471 ( .ip1(din[27]), .ip2(n78), .op(n508) );
  and2_2 U472 ( .ip1(din[28]), .ip2(n78), .op(n509) );
  and2_2 U473 ( .ip1(din[29]), .ip2(n78), .op(n510) );
  and2_2 U474 ( .ip1(din[30]), .ip2(n78), .op(n511) );
  and2_2 U475 ( .ip1(din[31]), .ip2(n78), .op(n512) );
  nand2_2 U477 ( .ip1(n514), .ip2(n515), .op(n869) );
  nand2_2 U478 ( .ip1(n516), .ip2(din[16]), .op(n515) );
  nand2_2 U479 ( .ip1(int__16), .ip2(n517), .op(n514) );
  nand2_2 U480 ( .ip1(n518), .ip2(n519), .op(n870) );
  nand2_2 U481 ( .ip1(n516), .ip2(din[17]), .op(n519) );
  nand2_2 U482 ( .ip1(int__17), .ip2(n517), .op(n518) );
  nand2_2 U483 ( .ip1(n520), .ip2(n521), .op(n871) );
  nand2_2 U484 ( .ip1(n516), .ip2(din[18]), .op(n521) );
  nand2_2 U485 ( .ip1(int__18), .ip2(n517), .op(n520) );
  nand2_2 U486 ( .ip1(n522), .ip2(n523), .op(n872) );
  nand2_2 U487 ( .ip1(n516), .ip2(din[19]), .op(n523) );
  nand2_2 U488 ( .ip1(int__19), .ip2(n517), .op(n522) );
  nand2_2 U489 ( .ip1(n524), .ip2(n525), .op(n873) );
  nand2_2 U490 ( .ip1(n516), .ip2(din[20]), .op(n525) );
  nand2_2 U491 ( .ip1(int__20), .ip2(n517), .op(n524) );
  nand2_2 U492 ( .ip1(n526), .ip2(n527), .op(n874) );
  nand2_2 U493 ( .ip1(n516), .ip2(din[21]), .op(n527) );
  nand2_2 U494 ( .ip1(int__21), .ip2(n517), .op(n526) );
  nand2_2 U495 ( .ip1(n528), .ip2(n529), .op(n875) );
  nand2_2 U496 ( .ip1(n516), .ip2(din[24]), .op(n529) );
  nand2_2 U497 ( .ip1(int__24), .ip2(n517), .op(n528) );
  nand2_2 U498 ( .ip1(n530), .ip2(n531), .op(n876) );
  nand2_2 U499 ( .ip1(n516), .ip2(din[25]), .op(n531) );
  nand2_2 U500 ( .ip1(int__25), .ip2(n517), .op(n530) );
  nand2_2 U501 ( .ip1(n532), .ip2(n533), .op(n877) );
  nand2_2 U502 ( .ip1(n516), .ip2(din[26]), .op(n533) );
  nand2_2 U503 ( .ip1(int__26), .ip2(n517), .op(n532) );
  nand2_2 U504 ( .ip1(n534), .ip2(n535), .op(n878) );
  nand2_2 U505 ( .ip1(n516), .ip2(din[27]), .op(n535) );
  nand2_2 U506 ( .ip1(int__27), .ip2(n517), .op(n534) );
  nand2_2 U507 ( .ip1(n536), .ip2(n537), .op(n879) );
  nand2_2 U508 ( .ip1(n516), .ip2(din[28]), .op(n537) );
  nand2_2 U509 ( .ip1(int__28), .ip2(n517), .op(n536) );
  nand2_2 U510 ( .ip1(n538), .ip2(n539), .op(n880) );
  nand2_2 U511 ( .ip1(n516), .ip2(din[29]), .op(n539) );
  nand2_2 U512 ( .ip1(int__29), .ip2(n517), .op(n538) );
  nand2_2 U516 ( .ip1(n61), .ip2(din[15]), .op(n542) );
  nand2_2 U518 ( .ip1(n545), .ip2(n546), .op(n882) );
  nand2_2 U519 ( .ip1(n60), .ip2(din[16]), .op(n546) );
  nand2_2 U520 ( .ip1(csr[16]), .ip2(n58), .op(n545) );
  nand2_2 U521 ( .ip1(n547), .ip2(n548), .op(n883) );
  nand2_2 U522 ( .ip1(n60), .ip2(din[17]), .op(n548) );
  nand2_2 U523 ( .ip1(csr[17]), .ip2(n58), .op(n547) );
  nand2_2 U524 ( .ip1(n549), .ip2(n550), .op(n884) );
  nand2_2 U525 ( .ip1(n60), .ip2(din[18]), .op(n550) );
  nand2_2 U526 ( .ip1(csr[18]), .ip2(n58), .op(n549) );
  nand2_2 U527 ( .ip1(n551), .ip2(n552), .op(n885) );
  nand2_2 U528 ( .ip1(n60), .ip2(din[19]), .op(n552) );
  nand2_2 U529 ( .ip1(csr[19]), .ip2(n58), .op(n551) );
  nand2_2 U530 ( .ip1(n553), .ip2(n554), .op(n886) );
  nand2_2 U531 ( .ip1(n60), .ip2(din[20]), .op(n554) );
  nand2_2 U532 ( .ip1(csr[20]), .ip2(n58), .op(n553) );
  nand2_2 U533 ( .ip1(n555), .ip2(n556), .op(n887) );
  nand2_2 U534 ( .ip1(n60), .ip2(din[21]), .op(n556) );
  nand2_2 U535 ( .ip1(csr[21]), .ip2(n58), .op(n555) );
  and2_2 U536 ( .ip1(din[22]), .ip2(n60), .op(n558) );
  nand2_2 U537 ( .ip1(n560), .ip2(n561), .op(n889) );
  nand2_2 U538 ( .ip1(n60), .ip2(din[23]), .op(n561) );
  nand2_2 U539 ( .ip1(csr[23]), .ip2(n557), .op(n560) );
  and2_2 U540 ( .ip1(n59), .ip2(n559), .op(n557) );
  nand4_2 U541 ( .ip1(csr[13]), .ip2(out_to_small), .ip3(rst), .ip4(n562), 
        .op(n559) );
  nand2_2 U542 ( .ip1(n563), .ip2(n564), .op(n890) );
  nand2_2 U543 ( .ip1(n60), .ip2(din[24]), .op(n564) );
  nand2_2 U544 ( .ip1(csr[24]), .ip2(n58), .op(n563) );
  nand2_2 U545 ( .ip1(n565), .ip2(n566), .op(n891) );
  nand2_2 U546 ( .ip1(n60), .ip2(din[25]), .op(n566) );
  nand2_2 U547 ( .ip1(csr[25]), .ip2(n58), .op(n565) );
  nand2_2 U548 ( .ip1(n567), .ip2(n568), .op(n892) );
  nand2_2 U549 ( .ip1(n60), .ip2(din[26]), .op(n568) );
  nand2_2 U550 ( .ip1(csr[26]), .ip2(n58), .op(n567) );
  nand2_2 U551 ( .ip1(n569), .ip2(n570), .op(n893) );
  nand2_2 U552 ( .ip1(n61), .ip2(din[27]), .op(n570) );
  nand2_2 U553 ( .ip1(n59), .ip2(csr[27]), .op(n569) );
  nand2_2 U554 ( .ip1(n571), .ip2(n572), .op(n894) );
  nand2_2 U555 ( .ip1(n61), .ip2(din[0]), .op(n572) );
  nand2_2 U556 ( .ip1(csr[0]), .ip2(n58), .op(n571) );
  nand2_2 U557 ( .ip1(n573), .ip2(n574), .op(n895) );
  nand2_2 U558 ( .ip1(n61), .ip2(din[1]), .op(n574) );
  nand2_2 U559 ( .ip1(csr[1]), .ip2(n58), .op(n573) );
  nand2_2 U560 ( .ip1(n575), .ip2(n576), .op(n896) );
  nand2_2 U561 ( .ip1(n61), .ip2(din[2]), .op(n576) );
  nand2_2 U562 ( .ip1(csr[2]), .ip2(n58), .op(n575) );
  nand2_2 U563 ( .ip1(n577), .ip2(n578), .op(n897) );
  nand2_2 U564 ( .ip1(n61), .ip2(din[3]), .op(n578) );
  nand2_2 U565 ( .ip1(csr[3]), .ip2(n59), .op(n577) );
  nand2_2 U566 ( .ip1(n579), .ip2(n580), .op(n898) );
  nand2_2 U567 ( .ip1(n61), .ip2(din[4]), .op(n580) );
  nand2_2 U568 ( .ip1(csr[4]), .ip2(n59), .op(n579) );
  nand2_2 U569 ( .ip1(n581), .ip2(n582), .op(n899) );
  nand2_2 U570 ( .ip1(n61), .ip2(din[5]), .op(n582) );
  nand2_2 U571 ( .ip1(csr[5]), .ip2(n59), .op(n581) );
  nand2_2 U572 ( .ip1(n583), .ip2(n584), .op(n900) );
  nand2_2 U573 ( .ip1(n61), .ip2(din[6]), .op(n584) );
  nand2_2 U574 ( .ip1(csr[6]), .ip2(n59), .op(n583) );
  nand2_2 U575 ( .ip1(n585), .ip2(n586), .op(n901) );
  nand2_2 U576 ( .ip1(n61), .ip2(din[7]), .op(n586) );
  nand2_2 U577 ( .ip1(csr[7]), .ip2(n59), .op(n585) );
  nand2_2 U578 ( .ip1(n587), .ip2(n588), .op(n902) );
  nand2_2 U579 ( .ip1(n61), .ip2(din[8]), .op(n588) );
  nand2_2 U580 ( .ip1(csr[8]), .ip2(n59), .op(n587) );
  nand2_2 U581 ( .ip1(n589), .ip2(n590), .op(n903) );
  nand2_2 U582 ( .ip1(n61), .ip2(din[9]), .op(n590) );
  nand2_2 U583 ( .ip1(csr[9]), .ip2(n59), .op(n589) );
  nand2_2 U584 ( .ip1(n591), .ip2(n592), .op(n904) );
  nand2_2 U585 ( .ip1(n61), .ip2(din[10]), .op(n592) );
  nand2_2 U586 ( .ip1(csr[10]), .ip2(n59), .op(n591) );
  nand2_2 U587 ( .ip1(n593), .ip2(n594), .op(n905) );
  nand2_2 U588 ( .ip1(n62), .ip2(din[11]), .op(n594) );
  nand2_2 U589 ( .ip1(csr[11]), .ip2(n59), .op(n593) );
  nand2_2 U590 ( .ip1(n595), .ip2(n596), .op(n906) );
  nand2_2 U591 ( .ip1(n62), .ip2(din[12]), .op(n596) );
  nand2_2 U592 ( .ip1(csr[12]), .ip2(n59), .op(n595) );
  nand2_2 U593 ( .ip1(n597), .ip2(n598), .op(n907) );
  nand2_2 U594 ( .ip1(n60), .ip2(din[13]), .op(n598) );
  nand2_2 U595 ( .ip1(csr[13]), .ip2(n59), .op(n597) );
  nor2_2 U596 ( .ip1(n77), .ip2(n60), .op(n544) );
  nor2_2 U597 ( .ip1(n562), .ip2(n77), .op(n543) );
  nand2_2 U598 ( .ip1(n56), .ip2(we), .op(n562) );
  nor4_2 U599 ( .ip1(n600), .ip2(n601), .ip3(n602), .ip4(n603), .op(n1141) );
  xor2_2 U600 ( .ip1(ep_sel[1]), .ip2(csr[19]), .op(n603) );
  xor2_2 U601 ( .ip1(ep_sel[2]), .ip2(csr[20]), .op(n602) );
  xor2_2 U602 ( .ip1(ep_sel[3]), .ip2(csr[21]), .op(n601) );
  xor2_2 U603 ( .ip1(ep_sel[0]), .ip2(csr[18]), .op(n600) );
  and2_2 U604 ( .ip1(n65), .ip2(buf0[9]), .op(n605) );
  and2_2 U605 ( .ip1(n92), .ip2(buf1[9]), .op(n604) );
  and2_2 U606 ( .ip1(n65), .ip2(buf0[8]), .op(n607) );
  and2_2 U607 ( .ip1(n92), .ip2(buf1[8]), .op(n606) );
  and2_2 U608 ( .ip1(n65), .ip2(buf0[7]), .op(n609) );
  and2_2 U609 ( .ip1(n92), .ip2(buf1[7]), .op(n608) );
  nand4_2 U610 ( .ip1(n610), .ip2(n611), .ip3(n612), .ip4(n613), .op(dout[6])
         );
  nand2_2 U611 ( .ip1(buf1[6]), .ip2(n91), .op(n613) );
  nand2_2 U612 ( .ip1(buf0[6]), .ip2(n63), .op(n612) );
  nand2_2 U613 ( .ip1(n51), .ip2(int_[6]), .op(n611) );
  nand2_2 U614 ( .ip1(csr[6]), .ip2(n55), .op(n610) );
  nand4_2 U615 ( .ip1(n614), .ip2(n615), .ip3(n616), .ip4(n617), .op(dout[5])
         );
  nand2_2 U616 ( .ip1(buf1[5]), .ip2(n90), .op(n617) );
  nand2_2 U617 ( .ip1(buf0[5]), .ip2(n63), .op(n616) );
  nand2_2 U618 ( .ip1(n51), .ip2(int_[5]), .op(n615) );
  nand2_2 U619 ( .ip1(csr[5]), .ip2(n56), .op(n614) );
  nand4_2 U620 ( .ip1(n618), .ip2(n619), .ip3(n620), .ip4(n621), .op(dout[4])
         );
  nand2_2 U621 ( .ip1(buf1[4]), .ip2(n90), .op(n621) );
  nand2_2 U622 ( .ip1(buf0[4]), .ip2(n63), .op(n620) );
  nand2_2 U623 ( .ip1(n50), .ip2(int_[4]), .op(n619) );
  nand2_2 U624 ( .ip1(csr[4]), .ip2(n56), .op(n618) );
  nand4_2 U625 ( .ip1(n622), .ip2(n623), .ip3(n624), .ip4(n625), .op(dout[3])
         );
  nand2_2 U626 ( .ip1(buf1[3]), .ip2(n90), .op(n625) );
  nand2_2 U627 ( .ip1(buf0[3]), .ip2(n63), .op(n624) );
  nand2_2 U628 ( .ip1(n49), .ip2(int_[3]), .op(n623) );
  nand2_2 U629 ( .ip1(csr[3]), .ip2(n56), .op(n622) );
  and2_2 U630 ( .ip1(n64), .ip2(buf0[31]), .op(n627) );
  and2_2 U631 ( .ip1(n92), .ip2(buf1[31]), .op(n626) );
  and2_2 U632 ( .ip1(n64), .ip2(buf0[30]), .op(n629) );
  and2_2 U633 ( .ip1(n91), .ip2(buf1[30]), .op(n628) );
  nand4_2 U634 ( .ip1(n630), .ip2(n631), .ip3(n632), .ip4(n633), .op(dout[2])
         );
  nand2_2 U635 ( .ip1(buf1[2]), .ip2(n90), .op(n633) );
  nand2_2 U636 ( .ip1(buf0[2]), .ip2(n63), .op(n632) );
  nand2_2 U637 ( .ip1(n48), .ip2(int_[2]), .op(n631) );
  nand2_2 U638 ( .ip1(csr[2]), .ip2(n56), .op(n630) );
  nand4_2 U639 ( .ip1(n634), .ip2(n635), .ip3(n636), .ip4(n637), .op(dout[29])
         );
  nand2_2 U640 ( .ip1(buf1[29]), .ip2(n90), .op(n637) );
  nand2_2 U641 ( .ip1(buf0[29]), .ip2(n63), .op(n636) );
  nand2_2 U642 ( .ip1(int__29), .ip2(n41), .op(n635) );
  nand2_2 U643 ( .ip1(n56), .ip2(csr[29]), .op(n634) );
  nand4_2 U644 ( .ip1(n638), .ip2(n639), .ip3(n640), .ip4(n641), .op(dout[28])
         );
  nand2_2 U645 ( .ip1(buf1[28]), .ip2(n90), .op(n641) );
  nand2_2 U646 ( .ip1(buf0[28]), .ip2(n63), .op(n640) );
  nand2_2 U647 ( .ip1(int__28), .ip2(n43), .op(n639) );
  nand2_2 U648 ( .ip1(n56), .ip2(csr[28]), .op(n638) );
  nand4_2 U649 ( .ip1(n642), .ip2(n643), .ip3(n644), .ip4(n645), .op(dout[27])
         );
  nand2_2 U650 ( .ip1(buf1[27]), .ip2(n90), .op(n645) );
  nand2_2 U651 ( .ip1(buf0[27]), .ip2(n63), .op(n644) );
  nand2_2 U652 ( .ip1(int__27), .ip2(n47), .op(n643) );
  nand2_2 U653 ( .ip1(n56), .ip2(csr[27]), .op(n642) );
  nand4_2 U654 ( .ip1(n646), .ip2(n647), .ip3(n648), .ip4(n649), .op(dout[26])
         );
  nand2_2 U655 ( .ip1(buf1[26]), .ip2(n90), .op(n649) );
  nand2_2 U656 ( .ip1(buf0[26]), .ip2(n63), .op(n648) );
  nand2_2 U657 ( .ip1(int__26), .ip2(n46), .op(n647) );
  nand2_2 U658 ( .ip1(csr[26]), .ip2(n55), .op(n646) );
  nand4_2 U659 ( .ip1(n650), .ip2(n651), .ip3(n652), .ip4(n653), .op(dout[25])
         );
  nand2_2 U660 ( .ip1(buf1[25]), .ip2(n90), .op(n653) );
  nand2_2 U661 ( .ip1(buf0[25]), .ip2(n63), .op(n652) );
  nand2_2 U662 ( .ip1(int__25), .ip2(n43), .op(n651) );
  nand2_2 U663 ( .ip1(csr[25]), .ip2(n55), .op(n650) );
  nand4_2 U664 ( .ip1(n654), .ip2(n655), .ip3(n656), .ip4(n657), .op(dout[24])
         );
  nand2_2 U665 ( .ip1(buf1[24]), .ip2(n90), .op(n657) );
  nand2_2 U666 ( .ip1(buf0[24]), .ip2(n63), .op(n656) );
  nand2_2 U667 ( .ip1(int__24), .ip2(n42), .op(n655) );
  nand2_2 U668 ( .ip1(csr[24]), .ip2(n55), .op(n654) );
  and2_2 U669 ( .ip1(n64), .ip2(buf0[23]), .op(n659) );
  and2_2 U670 ( .ip1(n91), .ip2(buf1[23]), .op(n658) );
  and2_2 U671 ( .ip1(n64), .ip2(buf0[22]), .op(n661) );
  and2_2 U672 ( .ip1(n91), .ip2(buf1[22]), .op(n660) );
  nand4_2 U673 ( .ip1(n662), .ip2(n663), .ip3(n664), .ip4(n665), .op(dout[21])
         );
  nand2_2 U674 ( .ip1(buf1[21]), .ip2(n91), .op(n665) );
  nand2_2 U675 ( .ip1(buf0[21]), .ip2(n63), .op(n664) );
  nand2_2 U676 ( .ip1(int__21), .ip2(n41), .op(n663) );
  nand2_2 U677 ( .ip1(csr[21]), .ip2(n55), .op(n662) );
  nand4_2 U678 ( .ip1(n666), .ip2(n667), .ip3(n668), .ip4(n669), .op(dout[20])
         );
  nand2_2 U679 ( .ip1(buf1[20]), .ip2(n91), .op(n669) );
  nand2_2 U680 ( .ip1(buf0[20]), .ip2(n64), .op(n668) );
  nand2_2 U681 ( .ip1(int__20), .ip2(n40), .op(n667) );
  nand2_2 U682 ( .ip1(csr[20]), .ip2(n55), .op(n666) );
  nand4_2 U683 ( .ip1(n670), .ip2(n671), .ip3(n672), .ip4(n673), .op(dout[1])
         );
  nand2_2 U684 ( .ip1(buf1[1]), .ip2(n91), .op(n673) );
  nand2_2 U685 ( .ip1(buf0[1]), .ip2(n64), .op(n672) );
  nand2_2 U686 ( .ip1(n47), .ip2(int_[1]), .op(n671) );
  nand2_2 U687 ( .ip1(csr[1]), .ip2(n55), .op(n670) );
  nand4_2 U688 ( .ip1(n674), .ip2(n675), .ip3(n676), .ip4(n677), .op(dout[19])
         );
  nand2_2 U689 ( .ip1(buf1[19]), .ip2(n91), .op(n677) );
  nand2_2 U690 ( .ip1(buf0[19]), .ip2(n64), .op(n676) );
  nand2_2 U691 ( .ip1(int__19), .ip2(n37), .op(n675) );
  nand2_2 U692 ( .ip1(csr[19]), .ip2(n55), .op(n674) );
  nand4_2 U693 ( .ip1(n678), .ip2(n679), .ip3(n680), .ip4(n681), .op(dout[18])
         );
  nand2_2 U694 ( .ip1(buf1[18]), .ip2(n91), .op(n681) );
  nand2_2 U695 ( .ip1(buf0[18]), .ip2(n64), .op(n680) );
  nand2_2 U696 ( .ip1(int__18), .ip2(n50), .op(n679) );
  nand2_2 U697 ( .ip1(csr[18]), .ip2(n55), .op(n678) );
  nand4_2 U698 ( .ip1(n682), .ip2(n683), .ip3(n684), .ip4(n685), .op(dout[17])
         );
  nand2_2 U699 ( .ip1(buf1[17]), .ip2(n91), .op(n685) );
  nand2_2 U700 ( .ip1(buf0[17]), .ip2(n64), .op(n684) );
  nand2_2 U701 ( .ip1(int__17), .ip2(n49), .op(n683) );
  nand2_2 U702 ( .ip1(csr[17]), .ip2(n55), .op(n682) );
  nand4_2 U703 ( .ip1(n686), .ip2(n687), .ip3(n688), .ip4(n689), .op(dout[16])
         );
  nand2_2 U704 ( .ip1(buf1[16]), .ip2(n91), .op(n689) );
  nand2_2 U705 ( .ip1(buf0[16]), .ip2(n64), .op(n688) );
  nand2_2 U706 ( .ip1(int__16), .ip2(n48), .op(n687) );
  nand2_2 U707 ( .ip1(csr[16]), .ip2(n55), .op(n686) );
  and2_2 U708 ( .ip1(n64), .ip2(buf0[15]), .op(n691) );
  and2_2 U709 ( .ip1(n91), .ip2(buf1[15]), .op(n690) );
  nand2_2 U710 ( .ip1(n692), .ip2(n693), .op(dout[14]) );
  nand2_2 U711 ( .ip1(buf1[14]), .ip2(n91), .op(n693) );
  nand2_2 U712 ( .ip1(buf0[14]), .ip2(n64), .op(n692) );
  and2_2 U713 ( .ip1(n65), .ip2(buf0[13]), .op(n695) );
  and2_2 U714 ( .ip1(n92), .ip2(buf1[13]), .op(n694) );
  and2_2 U715 ( .ip1(n65), .ip2(buf0[12]), .op(n697) );
  and2_2 U716 ( .ip1(n92), .ip2(buf1[12]), .op(n696) );
  and2_2 U717 ( .ip1(n65), .ip2(buf0[11]), .op(n699) );
  and2_2 U718 ( .ip1(n92), .ip2(buf1[11]), .op(n698) );
  and2_2 U719 ( .ip1(n65), .ip2(buf0[10]), .op(n701) );
  and2_2 U720 ( .ip1(n92), .ip2(buf1[10]), .op(n700) );
  nand4_2 U721 ( .ip1(n702), .ip2(n703), .ip3(n704), .ip4(n705), .op(dout[0])
         );
  nand2_2 U722 ( .ip1(buf1[0]), .ip2(n90), .op(n705) );
  nor2_2 U723 ( .ip1(n1139), .ip2(n1140), .op(n348) );
  nand2_2 U724 ( .ip1(buf0[0]), .ip2(n64), .op(n704) );
  nor2_2 U725 ( .ip1(n1139), .ip2(adr[0]), .op(n513) );
  nand2_2 U726 ( .ip1(n46), .ip2(int_[0]), .op(n703) );
  nand2_2 U727 ( .ip1(csr[0]), .ip2(n55), .op(n702) );
  nor2_2 U728 ( .ip1(adr[0]), .ip2(adr[1]), .op(n599) );
  nor2_2 U737 ( .ip1(buf0_orig[30]), .ip2(buf0_orig[29]), .op(n714) );
  and2_2 U738 ( .ip1(N319), .ip2(n715), .op(N320) );
  nand4_2 U739 ( .ip1(n1070), .ip2(n1069), .ip3(n716), .ip4(n717), .op(n715)
         );
  nor4_2 U740 ( .ip1(n718), .ip2(csr[4]), .ip3(csr[6]), .ip4(csr[5]), .op(n717) );
  or3_2 U741 ( .ip1(csr[8]), .ip2(csr[9]), .ip3(csr[7]), .op(n718) );
  or3_2 U746 ( .ip1(n721), .ip2(dma_out_cnt[6]), .ip3(dma_out_cnt[5]), .op(
        n720) );
  or3_2 U747 ( .ip1(dma_out_cnt[8]), .ip2(dma_out_cnt[9]), .ip3(dma_out_cnt[7]), .op(n721) );
  or3_2 U748 ( .ip1(dma_out_cnt[2]), .ip2(dma_out_cnt[4]), .ip3(dma_out_cnt[3]), .op(n719) );
  nor2_2 U749 ( .ip1(n202), .ip2(n985), .op(N271) );
  nor2_2 U750 ( .ip1(buf0_set), .ip2(buf0_rl), .op(n202) );
  and2_2 U752 ( .ip1(int_[5]), .ip2(int__20), .op(n727) );
  and2_2 U753 ( .ip1(int_[2]), .ip2(int__18), .op(n726) );
  and2_2 U758 ( .ip1(int_[5]), .ip2(int__28), .op(n734) );
  and2_2 U759 ( .ip1(int_[2]), .ip2(int__26), .op(n733) );
  nand2_2 U762 ( .ip1(n1102), .ip2(n1103), .op(n728) );
  and2_2 U764 ( .ip1(re), .ip2(n42), .op(N191) );
  inv_2 U820 ( .ip(rst), .op(n77) );
  not_ab_or_c_or_d U859 ( .ip1(n80), .ip2(din[31]), .ip3(n351), .ip4(n352), 
        .op(n350) );
  not_ab_or_c_or_d U860 ( .ip1(n80), .ip2(din[30]), .ip3(n358), .ip4(n359), 
        .op(n357) );
  not_ab_or_c_or_d U861 ( .ip1(n80), .ip2(din[29]), .ip3(n362), .ip4(n363), 
        .op(n361) );
  not_ab_or_c_or_d U862 ( .ip1(n80), .ip2(din[28]), .ip3(n366), .ip4(n367), 
        .op(n365) );
  not_ab_or_c_or_d U863 ( .ip1(n80), .ip2(din[27]), .ip3(n370), .ip4(n371), 
        .op(n369) );
  not_ab_or_c_or_d U864 ( .ip1(n80), .ip2(din[26]), .ip3(n374), .ip4(n375), 
        .op(n373) );
  not_ab_or_c_or_d U865 ( .ip1(n80), .ip2(din[25]), .ip3(n378), .ip4(n379), 
        .op(n377) );
  not_ab_or_c_or_d U866 ( .ip1(n80), .ip2(din[24]), .ip3(n382), .ip4(n383), 
        .op(n381) );
  not_ab_or_c_or_d U867 ( .ip1(n80), .ip2(din[23]), .ip3(n386), .ip4(n387), 
        .op(n385) );
  not_ab_or_c_or_d U868 ( .ip1(n80), .ip2(din[22]), .ip3(n390), .ip4(n391), 
        .op(n389) );
  not_ab_or_c_or_d U869 ( .ip1(n80), .ip2(din[21]), .ip3(n394), .ip4(n395), 
        .op(n393) );
  not_ab_or_c_or_d U870 ( .ip1(n80), .ip2(din[20]), .ip3(n398), .ip4(n399), 
        .op(n397) );
  not_ab_or_c_or_d U871 ( .ip1(n80), .ip2(din[19]), .ip3(n402), .ip4(n403), 
        .op(n401) );
  not_ab_or_c_or_d U872 ( .ip1(n80), .ip2(din[18]), .ip3(n406), .ip4(n407), 
        .op(n405) );
  not_ab_or_c_or_d U873 ( .ip1(n79), .ip2(din[17]), .ip3(n410), .ip4(n411), 
        .op(n409) );
  not_ab_or_c_or_d U874 ( .ip1(n80), .ip2(din[16]), .ip3(n414), .ip4(n415), 
        .op(n413) );
  not_ab_or_c_or_d U875 ( .ip1(n78), .ip2(din[15]), .ip3(n418), .ip4(n419), 
        .op(n417) );
  not_ab_or_c_or_d U876 ( .ip1(n79), .ip2(din[14]), .ip3(n422), .ip4(n423), 
        .op(n421) );
  not_ab_or_c_or_d U877 ( .ip1(n80), .ip2(din[13]), .ip3(n426), .ip4(n427), 
        .op(n425) );
  not_ab_or_c_or_d U878 ( .ip1(n78), .ip2(din[12]), .ip3(n430), .ip4(n431), 
        .op(n429) );
  not_ab_or_c_or_d U879 ( .ip1(n79), .ip2(din[11]), .ip3(n434), .ip4(n435), 
        .op(n433) );
  not_ab_or_c_or_d U880 ( .ip1(n80), .ip2(din[10]), .ip3(n438), .ip4(n439), 
        .op(n437) );
  not_ab_or_c_or_d U881 ( .ip1(n78), .ip2(din[9]), .ip3(n442), .ip4(n443), 
        .op(n441) );
  not_ab_or_c_or_d U882 ( .ip1(n79), .ip2(din[8]), .ip3(n446), .ip4(n447), 
        .op(n445) );
  not_ab_or_c_or_d U883 ( .ip1(n80), .ip2(din[7]), .ip3(n450), .ip4(n451), 
        .op(n449) );
  not_ab_or_c_or_d U884 ( .ip1(n78), .ip2(din[6]), .ip3(n454), .ip4(n455), 
        .op(n453) );
  not_ab_or_c_or_d U885 ( .ip1(n79), .ip2(din[5]), .ip3(n458), .ip4(n459), 
        .op(n457) );
  not_ab_or_c_or_d U886 ( .ip1(n80), .ip2(din[4]), .ip3(n462), .ip4(n463), 
        .op(n461) );
  not_ab_or_c_or_d U887 ( .ip1(n78), .ip2(din[3]), .ip3(n466), .ip4(n467), 
        .op(n465) );
  not_ab_or_c_or_d U888 ( .ip1(n79), .ip2(din[2]), .ip3(n470), .ip4(n471), 
        .op(n469) );
  not_ab_or_c_or_d U889 ( .ip1(n80), .ip2(din[1]), .ip3(n474), .ip4(n475), 
        .op(n473) );
  not_ab_or_c_or_d U890 ( .ip1(n80), .ip2(din[0]), .ip3(n478), .ip4(n479), 
        .op(n477) );
  ab_or_c_or_d U891 ( .ip1(buf0_orig[0]), .ip2(n69), .ip3(n481), .ip4(n77), 
        .op(n837) );
  ab_or_c_or_d U892 ( .ip1(buf0_orig[1]), .ip2(n72), .ip3(n482), .ip4(n77), 
        .op(n838) );
  ab_or_c_or_d U893 ( .ip1(buf0_orig[2]), .ip2(n72), .ip3(n483), .ip4(n77), 
        .op(n839) );
  ab_or_c_or_d U894 ( .ip1(buf0_orig[3]), .ip2(n72), .ip3(n484), .ip4(n77), 
        .op(n840) );
  ab_or_c_or_d U895 ( .ip1(buf0_orig[4]), .ip2(n71), .ip3(n485), .ip4(n77), 
        .op(n841) );
  ab_or_c_or_d U896 ( .ip1(buf0_orig[5]), .ip2(n71), .ip3(n486), .ip4(n77), 
        .op(n842) );
  ab_or_c_or_d U897 ( .ip1(buf0_orig[6]), .ip2(n71), .ip3(n487), .ip4(n77), 
        .op(n843) );
  ab_or_c_or_d U898 ( .ip1(buf0_orig[7]), .ip2(n71), .ip3(n488), .ip4(n77), 
        .op(n844) );
  ab_or_c_or_d U899 ( .ip1(buf0_orig[8]), .ip2(n70), .ip3(n489), .ip4(n77), 
        .op(n845) );
  ab_or_c_or_d U900 ( .ip1(buf0_orig[9]), .ip2(n70), .ip3(n490), .ip4(n77), 
        .op(n846) );
  ab_or_c_or_d U901 ( .ip1(buf0_orig[10]), .ip2(n70), .ip3(n491), .ip4(n77), 
        .op(n847) );
  ab_or_c_or_d U902 ( .ip1(buf0_orig[11]), .ip2(n70), .ip3(n492), .ip4(n77), 
        .op(n848) );
  ab_or_c_or_d U903 ( .ip1(buf0_orig[12]), .ip2(n69), .ip3(n493), .ip4(n77), 
        .op(n849) );
  ab_or_c_or_d U904 ( .ip1(buf0_orig[13]), .ip2(n69), .ip3(n494), .ip4(n77), 
        .op(n850) );
  ab_or_c_or_d U905 ( .ip1(buf0_orig[14]), .ip2(n69), .ip3(n495), .ip4(n77), 
        .op(n851) );
  ab_or_c_or_d U906 ( .ip1(buf0_orig[15]), .ip2(n68), .ip3(n496), .ip4(n77), 
        .op(n852) );
  ab_or_c_or_d U907 ( .ip1(buf0_orig[16]), .ip2(n68), .ip3(n497), .ip4(n77), 
        .op(n853) );
  ab_or_c_or_d U908 ( .ip1(buf0_orig[17]), .ip2(n68), .ip3(n498), .ip4(n77), 
        .op(n854) );
  ab_or_c_or_d U909 ( .ip1(buf0_orig[18]), .ip2(n68), .ip3(n499), .ip4(n77), 
        .op(n855) );
  ab_or_c_or_d U922 ( .ip1(buf0_orig[31]), .ip2(n67), .ip3(n512), .ip4(n77), 
        .op(n868) );
  ab_or_c_or_d U923 ( .ip1(csr[22]), .ip2(n557), .ip3(n1068), .ip4(n558), .op(
        n888) );
  ab_or_c_or_d U924 ( .ip1(csr[9]), .ip2(n56), .ip3(n604), .ip4(n605), .op(
        dout[9]) );
  ab_or_c_or_d U925 ( .ip1(csr[8]), .ip2(n56), .ip3(n606), .ip4(n607), .op(
        dout[8]) );
  ab_or_c_or_d U926 ( .ip1(csr[7]), .ip2(n56), .ip3(n608), .ip4(n609), .op(
        dout[7]) );
  ab_or_c_or_d U927 ( .ip1(n57), .ip2(csr[31]), .ip3(n626), .ip4(n627), .op(
        dout[31]) );
  ab_or_c_or_d U928 ( .ip1(n57), .ip2(csr[30]), .ip3(n628), .ip4(n629), .op(
        dout[30]) );
  ab_or_c_or_d U929 ( .ip1(csr[23]), .ip2(n56), .ip3(n658), .ip4(n659), .op(
        dout[23]) );
  ab_or_c_or_d U930 ( .ip1(csr[22]), .ip2(n56), .ip3(n660), .ip4(n661), .op(
        dout[22]) );
  ab_or_c_or_d U931 ( .ip1(n57), .ip2(csr[15]), .ip3(n690), .ip4(n691), .op(
        dout[15]) );
  ab_or_c_or_d U932 ( .ip1(csr[13]), .ip2(n56), .ip3(n694), .ip4(n695), .op(
        dout[13]) );
  ab_or_c_or_d U933 ( .ip1(csr[12]), .ip2(n56), .ip3(n696), .ip4(n697), .op(
        dout[12]) );
  ab_or_c_or_d U934 ( .ip1(csr[11]), .ip2(n56), .ip3(n698), .ip4(n699), .op(
        dout[11]) );
  ab_or_c_or_d U935 ( .ip1(csr[10]), .ip2(n56), .ip3(n700), .ip4(n701), .op(
        dout[10]) );
  dp_2 intb_reg ( .ip(N222), .ck(wclk), .q(intb) );
  dp_2 \buf0_orig_m3_reg[10]  ( .ip(N345), .ck(wclk), .q(buf0_orig_m3[10]) );
  dp_2 \buf0_orig_m3_reg[9]  ( .ip(N344), .ck(wclk), .q(buf0_orig_m3[9]) );
  dp_2 \buf0_orig_m3_reg[8]  ( .ip(N343), .ck(wclk), .q(buf0_orig_m3[8]) );
  dp_2 \buf0_orig_m3_reg[7]  ( .ip(N342), .ck(wclk), .q(buf0_orig_m3[7]) );
  dp_2 \buf0_orig_m3_reg[0]  ( .ip(n990), .ck(wclk), .q(buf0_orig_m3[0]) );
  usbf_ep_rf_0_DW01_sub_1 sub_7204 ( .A(buf0_orig[30:19]), .B(dma_out_cnt), 
        .CI(1'b0), .DIFF({N332, N331, N330, N329, N328, N327, N326, N325, N324, 
        N323, N322, N321}) );
  usbf_ep_rf_0_DW01_sub_2 sub_7193 ( .A(dma_in_cnt), .B({1'b0, 1'b0, 1'b0, 
        csr[10:2]}), .CI(1'b0), .DIFF({N302, N301, N300, N299, N298, N297, 
        N296, N295, N294, N293, N292, N291}) );
  usbf_ep_rf_0_DW01_inc_0 add_7190_S2 ( .A(dma_in_cnt), .SUM({N289, N288, N287, 
        N286, N285, N284, N283, N282, N281, N280, N279, N278}) );
  usbf_ep_rf_0_DW01_add_0 add_7168 ( .A(dma_out_cnt), .B({1'b0, 1'b0, 1'b0, 
        csr[10:2]}), .CI(1'b0), .SUM({N253, N252, N251, N250, N249, N248, N247, 
        N246, N245, N244, N243, N242}) );
  usbf_ep_rf_0_DW01_dec_0 sub_7165_S2 ( .A(dma_out_cnt), .SUM({N240, N239, 
        N238, N237, N236, N235, N234, N233, N232, N231, N230, N229}) );
  dp_1 dma_in_buf_sz1_reg ( .ip(N320), .ck(n34), .q(dma_in_buf_sz1) );
  dp_1 \dma_out_left_reg[11]  ( .ip(N332), .ck(n33), .q(dma_out_left[11]) );
  dp_1 \dma_out_left_reg[10]  ( .ip(N331), .ck(n33), .q(dma_out_left[10]) );
  dp_1 \dma_out_left_reg[9]  ( .ip(N330), .ck(n32), .q(dma_out_left[9]) );
  dp_1 \dma_out_left_reg[0]  ( .ip(N321), .ck(n32), .q(dma_out_left[0]) );
  dp_1 set_r_reg ( .ip(N271), .ck(n31), .q(set_r) );
  dp_1 \dma_out_left_reg[1]  ( .ip(N322), .ck(n32), .q(dma_out_left[1]) );
  dp_1 \dma_out_left_reg[3]  ( .ip(N324), .ck(n32), .q(dma_out_left[3]) );
  dp_1 \dma_out_left_reg[5]  ( .ip(N326), .ck(n32), .q(dma_out_left[5]) );
  dp_1 \dma_out_left_reg[7]  ( .ip(N328), .ck(n32), .q(dma_out_left[7]) );
  dp_1 \dma_out_left_reg[2]  ( .ip(N323), .ck(n32), .q(dma_out_left[2]) );
  dp_1 \dma_out_left_reg[4]  ( .ip(N325), .ck(n32), .q(dma_out_left[4]) );
  dp_1 \dma_out_left_reg[6]  ( .ip(N327), .ck(n32), .q(dma_out_left[6]) );
  dp_1 dma_out_buf_avail_reg ( .ip(N333), .ck(n33), .q(dma_out_buf_avail) );
  dp_1 \buf0_orig_reg[31]  ( .ip(n868), .ck(n22), .q(buf0_orig[31]) );
  dp_1 \buf0_orig_reg[18]  ( .ip(n855), .ck(n23), .q(buf0_orig[18]) );
  dp_1 \buf0_orig_reg[17]  ( .ip(n854), .ck(n23), .q(buf0_orig[17]) );
  dp_1 \buf0_orig_reg[16]  ( .ip(n853), .ck(n23), .q(buf0_orig[16]) );
  dp_1 \buf0_orig_reg[15]  ( .ip(n852), .ck(n23), .q(buf0_orig[15]) );
  dp_1 \buf0_orig_reg[14]  ( .ip(n851), .ck(n23), .q(buf0_orig[14]) );
  dp_1 \buf0_orig_reg[13]  ( .ip(n850), .ck(n24), .q(buf0_orig[13]) );
  dp_1 \buf0_orig_reg[12]  ( .ip(n849), .ck(n24), .q(buf0_orig[12]) );
  dp_1 \buf0_orig_reg[11]  ( .ip(n848), .ck(n24), .q(buf0_orig[11]) );
  dp_1 \buf0_orig_reg[10]  ( .ip(n847), .ck(n24), .q(buf0_orig[10]) );
  dp_1 \buf0_orig_reg[9]  ( .ip(n846), .ck(n24), .q(buf0_orig[9]) );
  dp_1 \buf0_orig_reg[8]  ( .ip(n845), .ck(n24), .q(buf0_orig[8]) );
  dp_1 \buf0_orig_reg[7]  ( .ip(n844), .ck(n24), .q(buf0_orig[7]) );
  dp_1 \buf0_orig_reg[6]  ( .ip(n843), .ck(n24), .q(buf0_orig[6]) );
  dp_1 \buf0_orig_reg[5]  ( .ip(n842), .ck(n24), .q(buf0_orig[5]) );
  dp_1 \buf0_orig_reg[4]  ( .ip(n841), .ck(n24), .q(buf0_orig[4]) );
  dp_1 \buf0_orig_reg[3]  ( .ip(n840), .ck(n24), .q(buf0_orig[3]) );
  dp_1 \buf0_orig_reg[2]  ( .ip(n839), .ck(n24), .q(buf0_orig[2]) );
  dp_1 \buf0_orig_reg[1]  ( .ip(n838), .ck(n24), .q(buf0_orig[1]) );
  dp_1 \buf0_orig_reg[0]  ( .ip(n837), .ck(n25), .q(buf0_orig[0]) );
  dp_1 \dma_out_left_reg[8]  ( .ip(N329), .ck(n32), .q(dma_out_left[8]) );
  dp_1 \csr1_reg[7]  ( .ip(n888), .ck(n21), .q(csr[22]) );
  dp_1 r5_reg ( .ip(r4), .ck(n31), .q(r5) );
  dp_1 int_re_reg ( .ip(N191), .ck(n30), .q(int_re) );
  dp_1 \int_stat_reg[4]  ( .ip(n770), .ck(n30), .q(int_[4]) );
  dp_1 \int_stat_reg[3]  ( .ip(n769), .ck(n30), .q(int_[3]) );
  dp_1 \csr0_reg[12]  ( .ip(n906), .ck(n20), .q(csr[12]) );
  dp_1 \csr0_reg[11]  ( .ip(n905), .ck(n20), .q(csr[11]) );
  dp_1 \csr1_reg[8]  ( .ip(n889), .ck(n21), .q(csr[23]) );
  dp_1 \iena_reg[3]  ( .ip(n878), .ck(n21), .q(int__27) );
  dp_1 \ienb_reg[3]  ( .ip(n872), .ck(n22), .q(int__19) );
  dp_1 \uc_bsel_reg[1]  ( .ip(n763), .ck(n30), .q(csr[31]) );
  dp_1 \uc_bsel_reg[0]  ( .ip(n762), .ck(n31), .q(csr[30]) );
  dp_1 \csr1_reg[10]  ( .ip(n891), .ck(n20), .q(csr[25]) );
  dp_1 \csr1_reg[9]  ( .ip(n890), .ck(n20), .q(csr[24]) );
  dp_1 \csr1_reg[2]  ( .ip(n883), .ck(n21), .q(csr[17]) );
  dp_1 \csr1_reg[1]  ( .ip(n882), .ck(n21), .q(csr[16]) );
  dp_1 \iena_reg[5]  ( .ip(n880), .ck(n21), .q(int__29) );
  dp_1 \iena_reg[1]  ( .ip(n876), .ck(n22), .q(int__25) );
  dp_1 \iena_reg[0]  ( .ip(n875), .ck(n22), .q(int__24) );
  dp_1 \ienb_reg[5]  ( .ip(n874), .ck(n22), .q(int__21) );
  dp_1 \ienb_reg[1]  ( .ip(n870), .ck(n22), .q(int__17) );
  dp_1 \ienb_reg[0]  ( .ip(n869), .ck(n22), .q(int__16) );
  dp_1 \buf0_reg[29]  ( .ip(n807), .ck(n25), .q(buf0[29]) );
  dp_1 \buf0_reg[28]  ( .ip(n808), .ck(n25), .q(buf0[28]) );
  dp_1 \buf0_reg[27]  ( .ip(n809), .ck(n25), .q(buf0[27]) );
  dp_1 \buf0_reg[26]  ( .ip(n810), .ck(n25), .q(buf0[26]) );
  dp_1 \buf0_reg[25]  ( .ip(n811), .ck(n25), .q(buf0[25]) );
  dp_1 \buf0_reg[24]  ( .ip(n812), .ck(n25), .q(buf0[24]) );
  dp_1 \buf0_reg[21]  ( .ip(n815), .ck(n25), .q(buf0[21]) );
  dp_1 \buf0_reg[20]  ( .ip(n816), .ck(n26), .q(buf0[20]) );
  dp_1 \buf0_reg[19]  ( .ip(n817), .ck(n26), .q(buf0[19]) );
  dp_1 \buf0_reg[18]  ( .ip(n818), .ck(n26), .q(buf0[18]) );
  dp_1 \buf0_reg[17]  ( .ip(n819), .ck(n26), .q(buf0[17]) );
  dp_1 \buf0_reg[16]  ( .ip(n820), .ck(n26), .q(buf0[16]) );
  dp_1 \buf0_reg[14]  ( .ip(n822), .ck(n26), .q(buf0[14]) );
  dp_1 \buf0_reg[6]  ( .ip(n830), .ck(n27), .q(buf0[6]) );
  dp_1 \buf0_reg[5]  ( .ip(n831), .ck(n27), .q(buf0[5]) );
  dp_1 \buf0_reg[4]  ( .ip(n832), .ck(n27), .q(buf0[4]) );
  dp_1 \buf0_reg[3]  ( .ip(n833), .ck(n27), .q(buf0[3]) );
  dp_1 \buf0_reg[2]  ( .ip(n834), .ck(n27), .q(buf0[2]) );
  dp_1 \buf0_reg[1]  ( .ip(n835), .ck(n27), .q(buf0[1]) );
  dp_1 \buf0_reg[0]  ( .ip(n836), .ck(n27), .q(buf0[0]) );
  dp_1 \buf1_reg[29]  ( .ip(n775), .ck(n27), .q(buf1[29]) );
  dp_1 \buf1_reg[28]  ( .ip(n776), .ck(n27), .q(buf1[28]) );
  dp_1 \buf1_reg[27]  ( .ip(n777), .ck(n28), .q(buf1[27]) );
  dp_1 \buf1_reg[26]  ( .ip(n778), .ck(n28), .q(buf1[26]) );
  dp_1 \buf1_reg[25]  ( .ip(n779), .ck(n28), .q(buf1[25]) );
  dp_1 \buf1_reg[24]  ( .ip(n780), .ck(n28), .q(buf1[24]) );
  dp_1 \buf1_reg[21]  ( .ip(n783), .ck(n28), .q(buf1[21]) );
  dp_1 \buf1_reg[20]  ( .ip(n784), .ck(n28), .q(buf1[20]) );
  dp_1 \buf1_reg[19]  ( .ip(n785), .ck(n28), .q(buf1[19]) );
  dp_1 \buf1_reg[18]  ( .ip(n786), .ck(n28), .q(buf1[18]) );
  dp_1 \buf1_reg[17]  ( .ip(n787), .ck(n28), .q(buf1[17]) );
  dp_1 \buf1_reg[16]  ( .ip(n788), .ck(n28), .q(buf1[16]) );
  dp_1 \buf1_reg[14]  ( .ip(n790), .ck(n29), .q(buf1[14]) );
  dp_1 \buf1_reg[6]  ( .ip(n798), .ck(n29), .q(buf1[6]) );
  dp_1 \buf1_reg[5]  ( .ip(n799), .ck(n29), .q(buf1[5]) );
  dp_1 \buf1_reg[4]  ( .ip(n800), .ck(n29), .q(buf1[4]) );
  dp_1 \buf1_reg[3]  ( .ip(n801), .ck(n29), .q(buf1[3]) );
  dp_1 \buf1_reg[2]  ( .ip(n802), .ck(n29), .q(buf1[2]) );
  dp_1 \buf1_reg[1]  ( .ip(n803), .ck(n30), .q(buf1[1]) );
  dp_1 \buf1_reg[0]  ( .ip(n804), .ck(n30), .q(buf1[0]) );
  dp_1 \iena_reg[4]  ( .ip(n879), .ck(n21), .q(int__28) );
  dp_1 \iena_reg[2]  ( .ip(n877), .ck(n21), .q(int__26) );
  dp_1 \ienb_reg[4]  ( .ip(n873), .ck(n22), .q(int__20) );
  dp_1 \ienb_reg[2]  ( .ip(n871), .ck(n22), .q(int__18) );
  dp_1 \buf0_reg[31]  ( .ip(n805), .ck(n25), .q(buf0[31]) );
  dp_1 \buf0_reg[30]  ( .ip(n806), .ck(n25), .q(buf0[30]) );
  dp_1 \buf0_reg[23]  ( .ip(n813), .ck(n25), .q(buf0[23]) );
  dp_1 \buf0_reg[22]  ( .ip(n814), .ck(n25), .q(buf0[22]) );
  dp_1 \buf0_reg[15]  ( .ip(n821), .ck(n26), .q(buf0[15]) );
  dp_1 \buf0_reg[13]  ( .ip(n823), .ck(n26), .q(buf0[13]) );
  dp_1 \buf0_reg[12]  ( .ip(n824), .ck(n26), .q(buf0[12]) );
  dp_1 \buf0_reg[11]  ( .ip(n825), .ck(n26), .q(buf0[11]) );
  dp_1 \buf0_reg[10]  ( .ip(n826), .ck(n26), .q(buf0[10]) );
  dp_1 \buf0_reg[9]  ( .ip(n827), .ck(n26), .q(buf0[9]) );
  dp_1 \buf0_reg[8]  ( .ip(n828), .ck(n26), .q(buf0[8]) );
  dp_1 \buf0_reg[7]  ( .ip(n829), .ck(n27), .q(buf0[7]) );
  dp_1 \buf1_reg[31]  ( .ip(n773), .ck(n27), .q(buf1[31]) );
  dp_1 \buf1_reg[30]  ( .ip(n774), .ck(n27), .q(buf1[30]) );
  dp_1 \buf1_reg[23]  ( .ip(n781), .ck(n28), .q(buf1[23]) );
  dp_1 \buf1_reg[22]  ( .ip(n782), .ck(n28), .q(buf1[22]) );
  dp_1 \buf1_reg[15]  ( .ip(n789), .ck(n28), .q(buf1[15]) );
  dp_1 \buf1_reg[13]  ( .ip(n791), .ck(n29), .q(buf1[13]) );
  dp_1 \buf1_reg[12]  ( .ip(n792), .ck(n29), .q(buf1[12]) );
  dp_1 \buf1_reg[11]  ( .ip(n793), .ck(n29), .q(buf1[11]) );
  dp_1 \buf1_reg[10]  ( .ip(n794), .ck(n29), .q(buf1[10]) );
  dp_1 \buf1_reg[9]  ( .ip(n795), .ck(n29), .q(buf1[9]) );
  dp_1 \buf1_reg[8]  ( .ip(n796), .ck(n29), .q(buf1[8]) );
  dp_1 \buf1_reg[7]  ( .ip(n797), .ck(n29), .q(buf1[7]) );
  dp_1 \uc_dpd_reg[1]  ( .ip(n765), .ck(n30), .q(csr[29]) );
  dp_1 \uc_dpd_reg[0]  ( .ip(n764), .ck(n30), .q(csr[28]) );
  dp_1 ots_stop_reg ( .ip(n907), .ck(n27), .q(csr[13]) );
  dp_1 \csr0_reg[0]  ( .ip(n894), .ck(n20), .q(csr[0]) );
  dp_1 \csr1_reg[11]  ( .ip(n892), .ck(n20), .q(csr[26]) );
  dp_1 \int_stat_reg[5]  ( .ip(n771), .ck(n30), .q(int_[5]) );
  dp_1 \int_stat_reg[2]  ( .ip(n768), .ck(n30), .q(int_[2]) );
  dp_1 \int_stat_reg[6]  ( .ip(n772), .ck(n30), .q(int_[6]) );
  dp_1 \int_stat_reg[1]  ( .ip(n767), .ck(n30), .q(int_[1]) );
  dp_1 \int_stat_reg[0]  ( .ip(n766), .ck(n30), .q(int_[0]) );
  dp_1 ep_match_r_reg ( .ip(ep_match), .ck(n25), .q(ep_match_r) );
  dp_1 \buf0_orig_reg[27]  ( .ip(n864), .ck(n22), .q(buf0_orig[27]) );
  dp_1 \csr1_reg[0]  ( .ip(n881), .ck(n21), .q(csr[15]) );
  dp_1 \csr1_reg[6]  ( .ip(n887), .ck(n21), .q(csr[21]) );
  dp_1 \csr1_reg[5]  ( .ip(n886), .ck(n21), .q(csr[20]) );
  dp_1 \csr1_reg[4]  ( .ip(n885), .ck(n21), .q(csr[19]) );
  dp_1 \csr1_reg[3]  ( .ip(n884), .ck(n21), .q(csr[18]) );
  dp_1 \csr0_reg[8]  ( .ip(n902), .ck(n34), .q(csr[8]) );
  dp_1 \csr0_reg[1]  ( .ip(n895), .ck(n20), .q(csr[1]) );
  dp_1 \csr0_reg[6]  ( .ip(n900), .ck(n34), .q(csr[6]) );
  dp_1 \csr0_reg[4]  ( .ip(n898), .ck(n34), .q(csr[4]) );
  dp_1 \buf0_orig_reg[28]  ( .ip(n865), .ck(n22), .q(buf0_orig[28]) );
  dp_1 \csr0_reg[10]  ( .ip(n904), .ck(n34), .q(csr[10]) );
  dp_1 \buf0_orig_reg[25]  ( .ip(n862), .ck(n23), .q(buf0_orig[25]) );
  dp_1 \buf0_orig_reg[23]  ( .ip(n860), .ck(n23), .q(buf0_orig[23]) );
  dp_1 \dma_in_cnt_reg[8]  ( .ip(n751), .ck(n34), .q(dma_in_cnt[8]) );
  dp_1 \dma_out_cnt_reg[11]  ( .ip(n748), .ck(n31), .q(dma_out_cnt[11]) );
  dp_1 \dma_out_cnt_reg[4]  ( .ip(n743), .ck(n31), .q(dma_out_cnt[4]) );
  dp_1 \dma_out_cnt_reg[6]  ( .ip(n741), .ck(n31), .q(dma_out_cnt[6]) );
  dp_1 \dma_out_cnt_reg[3]  ( .ip(n744), .ck(n31), .q(dma_out_cnt[3]) );
  dp_1 \dma_out_cnt_reg[5]  ( .ip(n742), .ck(n31), .q(dma_out_cnt[5]) );
  dp_1 \dma_out_cnt_reg[7]  ( .ip(n740), .ck(n32), .q(dma_out_cnt[7]) );
  dp_1 \dma_out_cnt_reg[2]  ( .ip(n745), .ck(n31), .q(dma_out_cnt[2]) );
  dp_1 \dma_out_cnt_reg[8]  ( .ip(n739), .ck(n32), .q(dma_out_cnt[8]) );
  dp_1 \dma_out_cnt_reg[1]  ( .ip(n746), .ck(n31), .q(dma_out_cnt[1]) );
  dp_1 \dma_in_cnt_reg[9]  ( .ip(n750), .ck(n34), .q(dma_in_cnt[9]) );
  dp_1 \buf0_orig_reg[26]  ( .ip(n863), .ck(n23), .q(buf0_orig[26]) );
  dp_1 \buf0_orig_reg[24]  ( .ip(n861), .ck(n23), .q(buf0_orig[24]) );
  dp_1 \dma_out_cnt_reg[9]  ( .ip(n738), .ck(n32), .q(dma_out_cnt[9]) );
  dp_1 \dma_in_cnt_reg[11]  ( .ip(n760), .ck(n33), .q(dma_in_cnt[11]) );
  dp_1 \dma_in_cnt_reg[5]  ( .ip(n754), .ck(n33), .q(dma_in_cnt[5]) );
  dp_1 \dma_in_cnt_reg[10]  ( .ip(n749), .ck(n33), .q(dma_in_cnt[10]) );
  dp_1 \dma_in_cnt_reg[3]  ( .ip(n756), .ck(n33), .q(dma_in_cnt[3]) );
  dp_1 \dma_in_cnt_reg[7]  ( .ip(n752), .ck(n33), .q(dma_in_cnt[7]) );
  dp_1 \buf0_orig_reg[19]  ( .ip(n856), .ck(n23), .q(buf0_orig[19]) );
  dp_1 \csr1_reg[12]  ( .ip(n893), .ck(n20), .q(csr[27]) );
  dp_1 \buf0_orig_reg[30]  ( .ip(n867), .ck(n22), .q(buf0_orig[30]) );
  dp_1 \dma_out_cnt_reg[10]  ( .ip(n737), .ck(n31), .q(dma_out_cnt[10]) );
  dp_1 \buf0_orig_reg[22]  ( .ip(n859), .ck(n23), .q(buf0_orig[22]) );
  dp_1 \dma_in_cnt_reg[6]  ( .ip(n753), .ck(n33), .q(dma_in_cnt[6]) );
  dp_1 \buf0_orig_reg[21]  ( .ip(n858), .ck(n23), .q(buf0_orig[21]) );
  dp_1 \dma_in_cnt_reg[0]  ( .ip(n759), .ck(n33), .q(dma_in_cnt[0]) );
  dp_1 \dma_out_cnt_reg[0]  ( .ip(n747), .ck(n31), .q(dma_out_cnt[0]) );
  dp_1 \buf0_orig_reg[20]  ( .ip(n857), .ck(n23), .q(buf0_orig[20]) );
  dp_1 \dma_in_cnt_reg[1]  ( .ip(n758), .ck(n33), .q(dma_in_cnt[1]) );
  dp_1 \csr0_reg[9]  ( .ip(n903), .ck(n20), .q(csr[9]) );
  dp_1 \csr0_reg[7]  ( .ip(n901), .ck(n20), .q(csr[7]) );
  dp_1 \buf0_orig_reg[29]  ( .ip(n866), .ck(n22), .q(buf0_orig[29]) );
  dp_1 \dma_in_cnt_reg[2]  ( .ip(n757), .ck(n33), .q(dma_in_cnt[2]) );
  dp_1 \csr0_reg[5]  ( .ip(n899), .ck(n20), .q(csr[5]) );
  dp_1 \dma_in_cnt_reg[4]  ( .ip(n755), .ck(n33), .q(dma_in_cnt[4]) );
  dp_1 \csr0_reg[3]  ( .ip(n897), .ck(n20), .q(csr[3]) );
  dp_1 \csr0_reg[2]  ( .ip(n896), .ck(n20), .q(csr[2]) );
  dp_1 dma_ack_clr1_reg ( .ip(r4), .ck(wclk), .q(dma_ack_clr1) );
  dp_1 inta_reg ( .ip(N221), .ck(wclk), .q(inta) );
  dp_1 dma_req_out_hold_reg ( .ip(N272), .ck(wclk), .q(dma_req_out_hold) );
  dp_1 dma_req_in_hold_reg ( .ip(N348), .ck(wclk), .q(dma_req_in_hold) );
  dp_1 \buf0_orig_m3_reg[1]  ( .ip(N336), .ck(wclk), .q(buf0_orig_m3[1]) );
  dp_1 \buf0_orig_m3_reg[2]  ( .ip(N337), .ck(wclk), .q(buf0_orig_m3[2]) );
  dp_1 r4_reg ( .ip(dma_ack_wr1), .ck(n31), .q(r4) );
  dp_1 r1_reg ( .ip(N361), .ck(wclk), .q(r1) );
  dp_1 dma_ack_wr1_reg ( .ip(n761), .ck(wclk), .q(dma_ack_wr1) );
  dp_1 r2_reg ( .ip(n736), .ck(wclk), .q(r2) );
  dp_1 dma_req_r_reg ( .ip(n735), .ck(wclk), .q(dma_req) );
  dp_2 dma_req_in_hold2_reg ( .ip(N347), .ck(wclk), .q(dma_req_in_hold2) );
  dp_1 \buf0_orig_m3_reg[11]  ( .ip(N346), .ck(wclk), .q(buf0_orig_m3[11]) );
  dp_1 \buf0_orig_m3_reg[6]  ( .ip(N341), .ck(wclk), .q(buf0_orig_m3[6]) );
  dp_1 \buf0_orig_m3_reg[5]  ( .ip(N340), .ck(wclk), .q(buf0_orig_m3[5]) );
  dp_1 \buf0_orig_m3_reg[3]  ( .ip(N338), .ck(wclk), .q(buf0_orig_m3[3]) );
  dp_2 \buf0_orig_m3_reg[4]  ( .ip(N339), .ck(wclk), .q(buf0_orig_m3[4]) );
  and2_2 U3 ( .ip1(n207), .ip2(n19), .op(n1) );
  and2_1 U4 ( .ip1(buf0_orig_m3[3]), .ip2(n997), .op(n2) );
  and2_2 U5 ( .ip1(csr[27]), .ip2(n960), .op(n3) );
  nand2_1 U6 ( .ip1(dma_ack), .ip2(n972), .op(n973) );
  nand2_1 U7 ( .ip1(dma_req_in_hold2), .ip2(dma_req_in_hold), .op(n971) );
  and2_1 U8 ( .ip1(rst), .ip2(n974), .op(n735) );
  nand2_1 U9 ( .ip1(n731), .ip2(buf0_orig_m3[4]), .op(n4) );
  nand2_1 U10 ( .ip1(n731), .ip2(buf0_orig_m3[4]), .op(n912) );
  nand2_1 U12 ( .ip1(n996), .ip2(buf0_orig_m3[5]), .op(n5) );
  nand2_1 U13 ( .ip1(n996), .ip2(buf0_orig_m3[5]), .op(n911) );
  nand2_1 U16 ( .ip1(n942), .ip2(n941), .op(n947) );
  nand2_1 U17 ( .ip1(n947), .ip2(n946), .op(n948) );
  inv_1 U18 ( .ip(n5), .op(n908) );
  nand2_1 U19 ( .ip1(n927), .ip2(n928), .op(n949) );
  nand2_1 U20 ( .ip1(r1), .ip2(n969), .op(n11) );
  inv_1 U21 ( .ip(n725), .op(n15) );
  nand2_1 U22 ( .ip1(dma_ack_wr1), .ip2(n950), .op(n14) );
  inv_2 U23 ( .ip(dma_ack), .op(n13) );
  nand2_1 U72 ( .ip1(n7), .ip2(n8), .op(N347) );
  nand2_1 U73 ( .ip1(n948), .ip2(n949), .op(n7) );
  nand2_1 U76 ( .ip1(buf0_orig_m3[11]), .ip2(n961), .op(n8) );
  nand2_1 U77 ( .ip1(dma_in_cnt[3]), .ip2(n730), .op(n909) );
  or2_1 U80 ( .ip1(n731), .ip2(buf0_orig_m3[4]), .op(n910) );
  nand2_1 U81 ( .ip1(n11), .ip2(n12), .op(n974) );
  nand2_1 U84 ( .ip1(dma_req), .ip2(n973), .op(n12) );
  nand2_1 U85 ( .ip1(n14), .ip2(n13), .op(n952) );
  inv_2 U88 ( .ip(int_re), .op(n19) );
  nand3_1 U89 ( .ip1(n926), .ip2(n924), .ip3(n925), .op(n927) );
  nand2_1 U92 ( .ip1(n923), .ip2(n922), .op(n924) );
  nand2_1 U93 ( .ip1(n911), .ip2(n912), .op(n913) );
  inv_1 U96 ( .ip(n4), .op(n732) );
  buf_2 U97 ( .ip(n1138), .op(n53) );
  buf_2 U100 ( .ip(n1138), .op(n52) );
  buf_2 U101 ( .ip(n1138), .op(n54) );
  inv_2 U104 ( .ip(n73), .op(n78) );
  inv_2 U105 ( .ip(n73), .op(n80) );
  inv_2 U108 ( .ip(n72), .op(n79) );
  buf_2 U109 ( .ip(n252), .op(n93) );
  buf_2 U112 ( .ip(n252), .op(n94) );
  buf_2 U113 ( .ip(n355), .op(n81) );
  buf_2 U116 ( .ip(n355), .op(n82) );
  nand2_2 U117 ( .ip1(n65), .ip2(we), .op(n480) );
  buf_2 U118 ( .ip(n131), .op(n99) );
  buf_2 U124 ( .ip(n348), .op(n91) );
  buf_2 U125 ( .ip(n353), .op(n87) );
  buf_2 U126 ( .ip(n353), .op(n88) );
  buf_2 U179 ( .ip(n348), .op(n90) );
  buf_2 U476 ( .ip(n131), .op(n100) );
  buf_2 U513 ( .ip(n129), .op(n101) );
  buf_2 U514 ( .ip(n129), .op(n102) );
  inv_2 U515 ( .ip(n35), .op(n36) );
  and2_2 U517 ( .ip1(n16), .ip2(n15), .op(n928) );
  and2_1 U729 ( .ip1(n935), .ip2(n936), .op(n16) );
  nor3_2 U730 ( .ip1(n130), .ip2(n99), .ip3(n1071), .op(n129) );
  buf_2 U731 ( .ip(n513), .op(n64) );
  buf_2 U732 ( .ip(n544), .op(n59) );
  buf_2 U733 ( .ip(n354), .op(n84) );
  buf_2 U734 ( .ip(n543), .op(n60) );
  buf_2 U735 ( .ip(n543), .op(n61) );
  buf_2 U736 ( .ip(n354), .op(n85) );
  buf_2 U742 ( .ip(n599), .op(n56) );
  buf_2 U743 ( .ip(n544), .op(n58) );
  buf_2 U744 ( .ip(n513), .op(n63) );
  buf_2 U745 ( .ip(n599), .op(n55) );
  buf_2 U751 ( .ip(n251), .op(n96) );
  buf_2 U754 ( .ip(n251), .op(n97) );
  buf_2 U755 ( .ip(n251), .op(n98) );
  inv_2 U756 ( .ip(n130), .op(n35) );
  and2_2 U757 ( .ip1(n17), .ip2(n18), .op(n926) );
  or2_1 U760 ( .ip1(buf0_orig_m3[5]), .ip2(n996), .op(n17) );
  or2_1 U761 ( .ip1(buf0_orig_m3[6]), .ip2(n729), .op(n18) );
  nor2_2 U763 ( .ip1(n77), .ip2(n516), .op(n517) );
  and3_2 U765 ( .ip1(we), .ip2(rst), .ip3(n40), .op(n516) );
  nor3_2 U766 ( .ip1(csr[1]), .ip2(csr[3]), .ip3(csr[2]), .op(n716) );
  nor3_2 U767 ( .ip1(csr[27]), .ip2(n124), .ip3(n960), .op(N348) );
  buf_1 U768 ( .ip(clk), .op(n20) );
  buf_1 U769 ( .ip(clk), .op(n21) );
  buf_1 U770 ( .ip(clk), .op(n22) );
  buf_1 U771 ( .ip(clk), .op(n23) );
  buf_1 U772 ( .ip(clk), .op(n24) );
  buf_1 U773 ( .ip(clk), .op(n25) );
  buf_1 U774 ( .ip(clk), .op(n26) );
  buf_1 U775 ( .ip(clk), .op(n27) );
  buf_1 U776 ( .ip(clk), .op(n28) );
  buf_1 U777 ( .ip(clk), .op(n29) );
  buf_1 U778 ( .ip(clk), .op(n30) );
  buf_1 U779 ( .ip(clk), .op(n31) );
  buf_1 U780 ( .ip(clk), .op(n32) );
  buf_1 U781 ( .ip(clk), .op(n33) );
  buf_1 U782 ( .ip(clk), .op(n34) );
  nor2_2 U783 ( .ip1(n1140), .ip2(adr[1]), .op(n37) );
  nor2_2 U784 ( .ip1(n1140), .ip2(adr[1]), .op(n38) );
  inv_1 U785 ( .ip(n37), .op(n39) );
  inv_1 U786 ( .ip(n44), .op(n40) );
  inv_1 U787 ( .ip(n45), .op(n41) );
  inv_1 U788 ( .ip(n39), .op(n42) );
  inv_1 U789 ( .ip(n39), .op(n43) );
  inv_1 U790 ( .ip(n38), .op(n44) );
  inv_1 U791 ( .ip(n38), .op(n45) );
  inv_1 U792 ( .ip(n44), .op(n46) );
  inv_1 U793 ( .ip(n44), .op(n47) );
  inv_1 U794 ( .ip(n45), .op(n48) );
  inv_1 U795 ( .ip(n45), .op(n49) );
  inv_1 U796 ( .ip(n39), .op(n50) );
  inv_1 U797 ( .ip(n44), .op(n51) );
  buf_1 U798 ( .ip(n599), .op(n57) );
  buf_1 U799 ( .ip(n543), .op(n62) );
  buf_1 U800 ( .ip(n513), .op(n65) );
  buf_1 U801 ( .ip(n480), .op(n66) );
  buf_1 U802 ( .ip(n480), .op(n67) );
  buf_1 U803 ( .ip(n480), .op(n68) );
  buf_1 U804 ( .ip(n480), .op(n69) );
  buf_1 U805 ( .ip(n480), .op(n70) );
  buf_1 U806 ( .ip(n480), .op(n71) );
  buf_1 U807 ( .ip(n480), .op(n72) );
  buf_1 U808 ( .ip(n480), .op(n73) );
  buf_1 U809 ( .ip(n480), .op(n74) );
  buf_1 U810 ( .ip(n480), .op(n75) );
  buf_1 U811 ( .ip(n480), .op(n76) );
  buf_1 U812 ( .ip(n355), .op(n83) );
  buf_1 U813 ( .ip(n354), .op(n86) );
  buf_1 U814 ( .ip(n353), .op(n89) );
  buf_1 U815 ( .ip(n348), .op(n92) );
  buf_1 U816 ( .ip(n252), .op(n95) );
  buf_1 U817 ( .ip(n1141), .op(ep_match) );
  nand2_2 U818 ( .ip1(n102), .ip2(dma_in_cnt[1]), .op(n104) );
  nand3_2 U819 ( .ip1(n192), .ip2(n193), .ip3(n104), .op(n758) );
  nand2_2 U821 ( .ip1(dma_in_cnt[0]), .ip2(n101), .op(n105) );
  nand3_2 U822 ( .ip1(n195), .ip2(n196), .ip3(n105), .op(n759) );
  nand2_2 U823 ( .ip1(n102), .ip2(dma_in_cnt[3]), .op(n106) );
  nand3_2 U824 ( .ip1(n186), .ip2(n187), .ip3(n106), .op(n756) );
  nand2_2 U825 ( .ip1(n102), .ip2(dma_in_cnt[5]), .op(n107) );
  nand3_2 U826 ( .ip1(n180), .ip2(n181), .ip3(n107), .op(n754) );
  nand2_2 U827 ( .ip1(n102), .ip2(dma_in_cnt[7]), .op(n108) );
  nand3_2 U828 ( .ip1(n174), .ip2(n175), .ip3(n108), .op(n752) );
  nand2_2 U829 ( .ip1(n102), .ip2(dma_in_cnt[9]), .op(n109) );
  nand3_2 U830 ( .ip1(n168), .ip2(n169), .ip3(n109), .op(n750) );
  nand2_2 U831 ( .ip1(n102), .ip2(dma_in_cnt[11]), .op(n110) );
  nand3_2 U832 ( .ip1(n198), .ip2(n199), .ip3(n110), .op(n760) );
  nand2_2 U833 ( .ip1(n102), .ip2(dma_in_cnt[10]), .op(n111) );
  nand3_2 U834 ( .ip1(n165), .ip2(n166), .ip3(n111), .op(n749) );
  nand2_2 U835 ( .ip1(n102), .ip2(dma_in_cnt[8]), .op(n112) );
  nand3_2 U836 ( .ip1(n171), .ip2(n172), .ip3(n112), .op(n751) );
  nand2_2 U837 ( .ip1(n102), .ip2(dma_in_cnt[6]), .op(n113) );
  nand3_2 U838 ( .ip1(n177), .ip2(n178), .ip3(n113), .op(n753) );
  nand2_2 U839 ( .ip1(n102), .ip2(dma_in_cnt[4]), .op(n114) );
  nand3_2 U840 ( .ip1(n183), .ip2(n184), .ip3(n114), .op(n755) );
  nand2_2 U841 ( .ip1(n102), .ip2(dma_in_cnt[2]), .op(n115) );
  nand3_2 U842 ( .ip1(n189), .ip2(n190), .ip3(n115), .op(n757) );
  ab_or_c_or_d U843 ( .ip1(n76), .ip2(buf0_orig[27]), .ip3(n77), .ip4(n508), 
        .op(n864) );
  inv_2 U844 ( .ip(buf0_orig[27]), .op(n993) );
  ab_or_c_or_d U845 ( .ip1(n75), .ip2(buf0_orig[25]), .ip3(n77), .ip4(n506), 
        .op(n862) );
  inv_2 U846 ( .ip(buf0_orig[25]), .op(n992) );
  ab_or_c_or_d U847 ( .ip1(n75), .ip2(buf0_orig[23]), .ip3(n77), .ip4(n504), 
        .op(n860) );
  inv_2 U848 ( .ip(buf0_orig[23]), .op(n991) );
  ab_or_c_or_d U849 ( .ip1(n75), .ip2(buf0_orig[19]), .ip3(n77), .ip4(n500), 
        .op(n856) );
  inv_2 U850 ( .ip(buf0_orig[19]), .op(n990) );
  inv_2 U851 ( .ip(dma_in_cnt[1]), .op(n998) );
  ab_or_c_or_d U852 ( .ip1(n75), .ip2(buf0_orig[20]), .ip3(n77), .ip4(n501), 
        .op(n857) );
  ab_or_c_or_d U853 ( .ip1(n74), .ip2(buf0_orig[22]), .ip3(n77), .ip4(n503), 
        .op(n859) );
  inv_2 U854 ( .ip(dma_in_cnt[3]), .op(n997) );
  ab_or_c_or_d U855 ( .ip1(n74), .ip2(buf0_orig[24]), .ip3(n77), .ip4(n505), 
        .op(n861) );
  inv_2 U856 ( .ip(dma_in_cnt[5]), .op(n996) );
  ab_or_c_or_d U857 ( .ip1(n76), .ip2(buf0_orig[26]), .ip3(n77), .ip4(n507), 
        .op(n863) );
  inv_2 U858 ( .ip(dma_in_cnt[7]), .op(n995) );
  ab_or_c_or_d U910 ( .ip1(n74), .ip2(buf0_orig[28]), .ip3(n77), .ip4(n509), 
        .op(n865) );
  inv_2 U911 ( .ip(dma_in_cnt[9]), .op(n994) );
  ab_or_c_or_d U912 ( .ip1(n73), .ip2(buf0_orig[30]), .ip3(n511), .ip4(n77), 
        .op(n867) );
  inv_2 U913 ( .ip(buf0_orig[30]), .op(n986) );
  ab_or_c_or_d U914 ( .ip1(n74), .ip2(buf0_orig[21]), .ip3(n77), .ip4(n502), 
        .op(n858) );
  ab_or_c_or_d U915 ( .ip1(buf0_orig[29]), .ip2(n67), .ip3(n77), .ip4(n510), 
        .op(n866) );
  nand2_2 U916 ( .ip1(n544), .ip2(csr[15]), .op(n116) );
  nand2_2 U917 ( .ip1(n542), .ip2(n116), .op(n881) );
  inv_2 U918 ( .ip(csr[26]), .op(n960) );
  nor2_2 U919 ( .ip1(dma_out_cnt[11]), .ip2(dma_out_cnt[10]), .op(n118) );
  nor2_2 U920 ( .ip1(n720), .ip2(n719), .op(n117) );
  nand2_2 U921 ( .ip1(n118), .ip2(n117), .op(n955) );
  and2_2 U936 ( .ip1(n3), .ip2(n955), .op(N272) );
  nor2_2 U937 ( .ip1(buf0_orig[24]), .ip2(buf0_orig[23]), .op(n120) );
  nor2_2 U938 ( .ip1(buf0_orig[21]), .ip2(buf0_orig[22]), .op(n119) );
  nand2_2 U939 ( .ip1(n120), .ip2(n119), .op(n123) );
  inv_2 U940 ( .ip(buf0_orig[28]), .op(n987) );
  nor2_2 U941 ( .ip1(buf0_orig[26]), .ip2(buf0_orig[25]), .op(n121) );
  nand4_2 U942 ( .ip1(n714), .ip2(n987), .ip3(n993), .ip4(n121), .op(n122) );
  nor2_2 U943 ( .ip1(n123), .ip2(n122), .op(n124) );
  nand2_2 U944 ( .ip1(buf0_orig[20]), .ip2(buf0_orig[19]), .op(n125) );
  nand2_2 U945 ( .ip1(n125), .ip2(n1066), .op(n712) );
  or2_2 U946 ( .ip1(buf0_orig[22]), .ip2(n712), .op(n708) );
  inv_2 U947 ( .ip(n708), .op(n167) );
  nand2_2 U948 ( .ip1(n167), .ip2(n991), .op(n710) );
  inv_2 U949 ( .ip(n710), .op(n170) );
  inv_2 U950 ( .ip(buf0_orig[24]), .op(n989) );
  nand2_2 U951 ( .ip1(n170), .ip2(n989), .op(n540) );
  inv_2 U952 ( .ip(n540), .op(n173) );
  nand2_2 U953 ( .ip1(n173), .ip2(n992), .op(n706) );
  inv_2 U954 ( .ip(n706), .op(n176) );
  inv_2 U955 ( .ip(buf0_orig[26]), .op(n988) );
  nand2_2 U956 ( .ip1(n176), .ip2(n988), .op(n194) );
  inv_2 U957 ( .ip(n194), .op(n179) );
  nand2_2 U958 ( .ip1(n179), .ip2(n993), .op(n200) );
  inv_2 U959 ( .ip(n200), .op(n182) );
  nand2_2 U960 ( .ip1(n182), .ip2(n987), .op(n204) );
  nor2_2 U961 ( .ip1(buf0_orig[29]), .ip2(n204), .op(n185) );
  xor2_2 U962 ( .ip1(buf0_orig[30]), .ip2(n185), .op(N346) );
  inv_2 U963 ( .ip(n204), .op(n188) );
  xor2_2 U964 ( .ip1(buf0_orig[29]), .ip2(n188), .op(N345) );
  nand2_2 U965 ( .ip1(n706), .ip2(buf0_orig[26]), .op(n191) );
  nand2_2 U966 ( .ip1(n194), .ip2(n191), .op(N342) );
  nand2_2 U967 ( .ip1(n194), .ip2(buf0_orig[27]), .op(n197) );
  nand2_2 U968 ( .ip1(n200), .ip2(n197), .op(N343) );
  nand2_2 U969 ( .ip1(n200), .ip2(buf0_orig[28]), .op(n203) );
  nand2_2 U970 ( .ip1(n204), .ip2(n203), .op(N344) );
  nand2_2 U971 ( .ip1(n710), .ip2(buf0_orig[24]), .op(n221) );
  nand2_2 U972 ( .ip1(n540), .ip2(n221), .op(N340) );
  nand2_2 U973 ( .ip1(n540), .ip2(buf0_orig[25]), .op(n541) );
  nand2_2 U974 ( .ip1(n706), .ip2(n541), .op(N341) );
  nand2_2 U975 ( .ip1(buf0_orig[22]), .ip2(n712), .op(n707) );
  nand2_2 U976 ( .ip1(n708), .ip2(n707), .op(N338) );
  nand2_2 U977 ( .ip1(n708), .ip2(buf0_orig[23]), .op(n709) );
  nand2_2 U978 ( .ip1(n710), .ip2(n709), .op(N339) );
  xor2_2 U979 ( .ip1(buf0_orig[20]), .ip2(buf0_orig[19]), .op(N336) );
  nand3_2 U980 ( .ip1(buf0_orig[21]), .ip2(buf0_orig[20]), .ip3(buf0_orig[19]), 
        .op(n711) );
  nand2_2 U981 ( .ip1(n712), .ip2(n711), .op(N337) );
  nand2_2 U982 ( .ip1(buf0_orig_m3[9]), .ip2(n994), .op(n935) );
  inv_2 U983 ( .ip(dma_in_cnt[8]), .op(n713) );
  nand2_2 U984 ( .ip1(buf0_orig_m3[8]), .ip2(n713), .op(n936) );
  inv_2 U985 ( .ip(dma_in_cnt[10]), .op(n722) );
  nand2_2 U986 ( .ip1(buf0_orig_m3[10]), .ip2(n722), .op(n943) );
  inv_2 U987 ( .ip(dma_in_cnt[6]), .op(n729) );
  nand2_2 U988 ( .ip1(buf0_orig_m3[6]), .ip2(n729), .op(n724) );
  nand2_2 U989 ( .ip1(buf0_orig_m3[7]), .ip2(n995), .op(n723) );
  nand3_2 U990 ( .ip1(n943), .ip2(n724), .ip3(n723), .op(n725) );
  inv_2 U991 ( .ip(buf0_orig_m3[3]), .op(n730) );
  inv_2 U992 ( .ip(dma_in_cnt[4]), .op(n731) );
  ab_or_c_or_d U993 ( .ip1(n910), .ip2(n909), .ip3(n908), .ip4(n732), .op(n925) );
  not_ab_or_c_or_d U994 ( .ip1(buf0_orig_m3[2]), .ip2(n1015), .ip3(n913), 
        .ip4(n2), .op(n923) );
  inv_2 U995 ( .ip(dma_in_cnt[0]), .op(n916) );
  nand3_2 U996 ( .ip1(n998), .ip2(n916), .ip3(buf0_orig_m3[0]), .op(n915) );
  inv_2 U997 ( .ip(buf0_orig_m3[1]), .op(n914) );
  nand2_2 U998 ( .ip1(n915), .ip2(n914), .op(n921) );
  nand2_2 U999 ( .ip1(buf0_orig_m3[0]), .ip2(n916), .op(n917) );
  nand2_2 U1000 ( .ip1(n917), .ip2(dma_in_cnt[1]), .op(n920) );
  inv_2 U1001 ( .ip(buf0_orig_m3[2]), .op(n918) );
  nand2_2 U1002 ( .ip1(dma_in_cnt[2]), .ip2(n918), .op(n919) );
  nand3_2 U1003 ( .ip1(n921), .ip2(n920), .ip3(n919), .op(n922) );
  inv_2 U1004 ( .ip(buf0_orig_m3[10]), .op(n932) );
  inv_2 U1005 ( .ip(buf0_orig_m3[11]), .op(n929) );
  nand2_2 U1006 ( .ip1(dma_in_cnt[11]), .ip2(n929), .op(n944) );
  inv_2 U1007 ( .ip(n944), .op(n931) );
  nor2_2 U1008 ( .ip1(buf0_orig_m3[9]), .ip2(n994), .op(n930) );
  not_ab_or_c_or_d U1009 ( .ip1(dma_in_cnt[10]), .ip2(n932), .ip3(n931), .ip4(
        n930), .op(n942) );
  inv_2 U1010 ( .ip(buf0_orig_m3[8]), .op(n933) );
  nand2_2 U1011 ( .ip1(dma_in_cnt[8]), .ip2(n933), .op(n940) );
  inv_2 U1012 ( .ip(buf0_orig_m3[7]), .op(n934) );
  nand2_2 U1013 ( .ip1(dma_in_cnt[7]), .ip2(n934), .op(n939) );
  inv_2 U1014 ( .ip(n935), .op(n938) );
  inv_2 U1015 ( .ip(n936), .op(n937) );
  ab_or_c_or_d U1016 ( .ip1(n940), .ip2(n939), .ip3(n938), .ip4(n937), .op(
        n941) );
  inv_2 U1017 ( .ip(n943), .op(n945) );
  nand2_2 U1018 ( .ip1(n945), .ip2(n944), .op(n946) );
  inv_2 U1019 ( .ip(dma_in_cnt[11]), .op(n961) );
  inv_2 U1020 ( .ip(dma_ack_clr1), .op(n950) );
  inv_2 U1021 ( .ip(n77), .op(n951) );
  and2_2 U1022 ( .ip1(n952), .ip2(n951), .op(n761) );
  inv_2 U1023 ( .ip(r2), .op(n969) );
  nor2_2 U1024 ( .ip1(r4), .ip2(n969), .op(n953) );
  nor2_2 U1025 ( .ip1(r1), .ip2(n953), .op(n954) );
  nor2_2 U1026 ( .ip1(n77), .ip2(n954), .op(n736) );
  inv_2 U1027 ( .ip(n955), .op(n958) );
  inv_2 U1028 ( .ip(dma_out_cnt[1]), .op(n957) );
  inv_2 U1029 ( .ip(dma_out_cnt[0]), .op(n956) );
  nand3_2 U1030 ( .ip1(n958), .ip2(n957), .ip3(n956), .op(n959) );
  nand2_2 U1031 ( .ip1(n3), .ip2(n959), .op(n968) );
  nor2_2 U1032 ( .ip1(csr[27]), .ip2(n960), .op(n964) );
  nand2_2 U1033 ( .ip1(buf0_orig[30]), .ip2(n961), .op(n962) );
  nand2_2 U1034 ( .ip1(n1065), .ip2(n962), .op(n963) );
  nand2_2 U1035 ( .ip1(n964), .ip2(n963), .op(n967) );
  or2_2 U1036 ( .ip1(r2), .ip2(r4), .op(n966) );
  inv_2 U1037 ( .ip(r5), .op(n985) );
  nand2_2 U1038 ( .ip1(csr[15]), .ip2(n985), .op(n965) );
  not_ab_or_c_or_d U1039 ( .ip1(n968), .ip2(n967), .ip3(n966), .ip4(n965), 
        .op(N361) );
  inv_2 U1040 ( .ip(dma_req_out_hold), .op(n970) );
  mux2_1 U1041 ( .ip1(n971), .ip2(n970), .s(n3), .op(n972) );
  ab_or_c_or_d U1042 ( .ip1(int__19), .ip2(n728), .ip3(n727), .ip4(n726), .op(
        n979) );
  nand2_2 U1043 ( .ip1(int__17), .ip2(int_[1]), .op(n977) );
  nand2_2 U1044 ( .ip1(int__16), .ip2(int_[0]), .op(n976) );
  nand2_2 U1045 ( .ip1(int__21), .ip2(int_[6]), .op(n975) );
  nand3_2 U1046 ( .ip1(n977), .ip2(n976), .ip3(n975), .op(n978) );
  or2_2 U1047 ( .ip1(n979), .ip2(n978), .op(N222) );
  ab_or_c_or_d U1048 ( .ip1(int__27), .ip2(n728), .ip3(n734), .ip4(n733), .op(
        n984) );
  nand2_2 U1049 ( .ip1(int__25), .ip2(int_[1]), .op(n982) );
  nand2_2 U1050 ( .ip1(int__24), .ip2(int_[0]), .op(n981) );
  nand2_2 U1051 ( .ip1(int__29), .ip2(int_[6]), .op(n980) );
  nand3_2 U1052 ( .ip1(n982), .ip2(n981), .ip3(n980), .op(n983) );
  or2_2 U1053 ( .ip1(n984), .ip2(n983), .op(N221) );
  inv_2 U1054 ( .ip(csr[15]), .op(n1071) );
  nor2_1 U1055 ( .ip1(csr[8]), .ip2(n729), .op(n1010) );
  nor2_1 U1056 ( .ip1(csr[6]), .ip2(n731), .op(n1006) );
  nor2_1 U1057 ( .ip1(csr[4]), .ip2(n1015), .op(n1002) );
  not_ab_or_c_or_d U1058 ( .ip1(dma_in_cnt[1]), .ip2(n1040), .ip3(n1016), 
        .ip4(dma_in_cnt[0]), .op(n1000) );
  nor2_1 U1059 ( .ip1(dma_in_cnt[1]), .ip2(n1040), .op(n999) );
  not_ab_or_c_or_d U1060 ( .ip1(csr[4]), .ip2(n1015), .ip3(n1000), .ip4(n999), 
        .op(n1001) );
  not_ab_or_c_or_d U1061 ( .ip1(dma_in_cnt[3]), .ip2(n1017), .ip3(n1002), 
        .ip4(n1001), .op(n1004) );
  nor2_1 U1062 ( .ip1(dma_in_cnt[3]), .ip2(n1017), .op(n1003) );
  not_ab_or_c_or_d U1063 ( .ip1(csr[6]), .ip2(n731), .ip3(n1004), .ip4(n1003), 
        .op(n1005) );
  not_ab_or_c_or_d U1064 ( .ip1(dma_in_cnt[5]), .ip2(n1018), .ip3(n1006), 
        .ip4(n1005), .op(n1008) );
  nor2_1 U1065 ( .ip1(dma_in_cnt[5]), .ip2(n1018), .op(n1007) );
  not_ab_or_c_or_d U1066 ( .ip1(csr[8]), .ip2(n729), .ip3(n1008), .ip4(n1007), 
        .op(n1009) );
  not_ab_or_c_or_d U1067 ( .ip1(dma_in_cnt[7]), .ip2(n1019), .ip3(n1010), 
        .ip4(n1009), .op(n1012) );
  nor2_1 U1068 ( .ip1(dma_in_cnt[7]), .ip2(n1019), .op(n1011) );
  not_ab_or_c_or_d U1069 ( .ip1(csr[10]), .ip2(n713), .ip3(n1012), .ip4(n1011), 
        .op(n1014) );
  or3_1 U1070 ( .ip1(dma_in_cnt[9]), .ip2(dma_in_cnt[11]), .ip3(dma_in_cnt[10]), .op(n1013) );
  ab_or_c_or_d U1071 ( .ip1(dma_in_cnt[8]), .ip2(n1069), .ip3(n1014), .ip4(
        n1013), .op(N319) );
  inv_2 U1072 ( .ip(dma_in_cnt[2]), .op(n1015) );
  inv_2 U1073 ( .ip(csr[2]), .op(n1016) );
  inv_2 U1074 ( .ip(csr[5]), .op(n1017) );
  inv_2 U1075 ( .ip(csr[7]), .op(n1018) );
  inv_2 U1076 ( .ip(csr[9]), .op(n1019) );
  nor2_1 U1077 ( .ip1(csr[8]), .ip2(n1037), .op(n1031) );
  nor2_1 U1078 ( .ip1(csr[6]), .ip2(n1038), .op(n1027) );
  nor2_1 U1079 ( .ip1(csr[4]), .ip2(n1039), .op(n1023) );
  not_ab_or_c_or_d U1080 ( .ip1(dma_out_left[1]), .ip2(n1040), .ip3(n1016), 
        .ip4(dma_out_left[0]), .op(n1021) );
  nor2_1 U1081 ( .ip1(dma_out_left[1]), .ip2(n1040), .op(n1020) );
  not_ab_or_c_or_d U1082 ( .ip1(csr[4]), .ip2(n1039), .ip3(n1021), .ip4(n1020), 
        .op(n1022) );
  not_ab_or_c_or_d U1083 ( .ip1(dma_out_left[3]), .ip2(n1017), .ip3(n1023), 
        .ip4(n1022), .op(n1025) );
  nor2_1 U1084 ( .ip1(dma_out_left[3]), .ip2(n1017), .op(n1024) );
  not_ab_or_c_or_d U1085 ( .ip1(csr[6]), .ip2(n1038), .ip3(n1025), .ip4(n1024), 
        .op(n1026) );
  not_ab_or_c_or_d U1086 ( .ip1(dma_out_left[5]), .ip2(n1018), .ip3(n1027), 
        .ip4(n1026), .op(n1029) );
  nor2_1 U1087 ( .ip1(dma_out_left[5]), .ip2(n1018), .op(n1028) );
  not_ab_or_c_or_d U1088 ( .ip1(csr[8]), .ip2(n1037), .ip3(n1029), .ip4(n1028), 
        .op(n1030) );
  not_ab_or_c_or_d U1089 ( .ip1(dma_out_left[7]), .ip2(n1019), .ip3(n1031), 
        .ip4(n1030), .op(n1033) );
  nor2_1 U1090 ( .ip1(dma_out_left[7]), .ip2(n1019), .op(n1032) );
  not_ab_or_c_or_d U1091 ( .ip1(csr[10]), .ip2(n1036), .ip3(n1033), .ip4(n1032), .op(n1035) );
  or3_1 U1092 ( .ip1(dma_out_left[9]), .ip2(dma_out_left[11]), .ip3(
        dma_out_left[10]), .op(n1034) );
  ab_or_c_or_d U1093 ( .ip1(dma_out_left[8]), .ip2(n1069), .ip3(n1035), .ip4(
        n1034), .op(N333) );
  inv_2 U1094 ( .ip(dma_out_left[8]), .op(n1036) );
  inv_2 U1095 ( .ip(dma_out_left[6]), .op(n1037) );
  inv_2 U1096 ( .ip(dma_out_left[4]), .op(n1038) );
  inv_2 U1097 ( .ip(dma_out_left[2]), .op(n1039) );
  inv_2 U1098 ( .ip(csr[3]), .op(n1040) );
  nor2_1 U1099 ( .ip1(dma_in_cnt[10]), .ip2(n1067), .op(n1062) );
  nor2_1 U1100 ( .ip1(dma_in_cnt[8]), .ip2(n993), .op(n1058) );
  nor2_1 U1101 ( .ip1(dma_in_cnt[6]), .ip2(n992), .op(n1054) );
  nor2_1 U1102 ( .ip1(dma_in_cnt[4]), .ip2(n991), .op(n1050) );
  nor2_1 U1103 ( .ip1(dma_in_cnt[2]), .ip2(n1066), .op(n1046) );
  nor2_1 U1104 ( .ip1(n990), .ip2(dma_in_cnt[0]), .op(n1042) );
  and2_1 U1105 ( .ip1(n998), .ip2(n1042), .op(n1041) );
  nor2_1 U1106 ( .ip1(buf0_orig[20]), .ip2(n1041), .op(n1044) );
  nor2_1 U1107 ( .ip1(n1042), .ip2(n998), .op(n1043) );
  not_ab_or_c_or_d U1108 ( .ip1(dma_in_cnt[2]), .ip2(n1066), .ip3(n1044), 
        .ip4(n1043), .op(n1045) );
  not_ab_or_c_or_d U1109 ( .ip1(buf0_orig[22]), .ip2(n997), .ip3(n1046), .ip4(
        n1045), .op(n1048) );
  nor2_1 U1110 ( .ip1(buf0_orig[22]), .ip2(n997), .op(n1047) );
  not_ab_or_c_or_d U1111 ( .ip1(dma_in_cnt[4]), .ip2(n991), .ip3(n1048), .ip4(
        n1047), .op(n1049) );
  not_ab_or_c_or_d U1112 ( .ip1(buf0_orig[24]), .ip2(n996), .ip3(n1050), .ip4(
        n1049), .op(n1052) );
  nor2_1 U1113 ( .ip1(buf0_orig[24]), .ip2(n996), .op(n1051) );
  not_ab_or_c_or_d U1114 ( .ip1(dma_in_cnt[6]), .ip2(n992), .ip3(n1052), .ip4(
        n1051), .op(n1053) );
  not_ab_or_c_or_d U1115 ( .ip1(buf0_orig[26]), .ip2(n995), .ip3(n1054), .ip4(
        n1053), .op(n1056) );
  nor2_1 U1116 ( .ip1(buf0_orig[26]), .ip2(n995), .op(n1055) );
  not_ab_or_c_or_d U1117 ( .ip1(dma_in_cnt[8]), .ip2(n993), .ip3(n1056), .ip4(
        n1055), .op(n1057) );
  not_ab_or_c_or_d U1118 ( .ip1(buf0_orig[28]), .ip2(n994), .ip3(n1058), .ip4(
        n1057), .op(n1060) );
  nor2_1 U1119 ( .ip1(buf0_orig[28]), .ip2(n994), .op(n1059) );
  not_ab_or_c_or_d U1120 ( .ip1(dma_in_cnt[10]), .ip2(n1067), .ip3(n1060), 
        .ip4(n1059), .op(n1061) );
  or2_1 U1121 ( .ip1(n1062), .ip2(n1061), .op(n1064) );
  nand2_1 U1122 ( .ip1(dma_in_cnt[11]), .ip2(n986), .op(n1063) );
  nand2_1 U1123 ( .ip1(n1064), .ip2(n1063), .op(n1065) );
  inv_2 U1124 ( .ip(buf0_orig[21]), .op(n1066) );
  inv_2 U1125 ( .ip(buf0_orig[29]), .op(n1067) );
  inv_2 U1126 ( .ip(n559), .op(n1068) );
  inv_2 U1127 ( .ip(csr[10]), .op(n1069) );
  inv_2 U1128 ( .ip(csr[0]), .op(n1070) );
  inv_2 U1129 ( .ip(buf0_orig[31]), .op(n1072) );
  inv_2 U1130 ( .ip(buf0_orig[22]), .op(n1073) );
  inv_2 U1131 ( .ip(buf0_orig[20]), .op(n1074) );
  inv_2 U1132 ( .ip(buf0_orig[18]), .op(n1075) );
  inv_2 U1133 ( .ip(buf0_orig[17]), .op(n1076) );
  inv_2 U1134 ( .ip(buf0_orig[16]), .op(n1077) );
  inv_2 U1135 ( .ip(buf0_orig[15]), .op(n1078) );
  inv_2 U1136 ( .ip(buf0_orig[14]), .op(n1079) );
  inv_2 U1137 ( .ip(buf0_orig[13]), .op(n1080) );
  inv_2 U1138 ( .ip(buf0_orig[12]), .op(n1081) );
  inv_2 U1139 ( .ip(buf0_orig[11]), .op(n1082) );
  inv_2 U1140 ( .ip(buf0_orig[10]), .op(n1083) );
  inv_2 U1141 ( .ip(buf0_orig[9]), .op(n1084) );
  inv_2 U1142 ( .ip(buf0_orig[8]), .op(n1085) );
  inv_2 U1143 ( .ip(buf0_orig[7]), .op(n1086) );
  inv_2 U1144 ( .ip(buf0_orig[6]), .op(n1087) );
  inv_2 U1145 ( .ip(buf0_orig[5]), .op(n1088) );
  inv_2 U1146 ( .ip(buf0_orig[4]), .op(n1089) );
  inv_2 U1147 ( .ip(buf0_orig[3]), .op(n1090) );
  inv_2 U1148 ( .ip(buf0_orig[2]), .op(n1091) );
  inv_2 U1149 ( .ip(buf0_orig[1]), .op(n1092) );
  inv_2 U1150 ( .ip(buf0_orig[0]), .op(n1093) );
  inv_2 U1151 ( .ip(n246), .op(n1094) );
  inv_2 U1152 ( .ip(n242), .op(n1095) );
  inv_2 U1153 ( .ip(n230), .op(n1096) );
  inv_2 U1154 ( .ip(n226), .op(n1097) );
  inv_2 U1155 ( .ip(n220), .op(n1098) );
  inv_2 U1156 ( .ip(ep_match_r), .op(n1099) );
  inv_2 U1157 ( .ip(n214), .op(n1100) );
  inv_2 U1158 ( .ip(n208), .op(n1101) );
  inv_2 U1159 ( .ip(int_[4]), .op(n1102) );
  inv_2 U1160 ( .ip(int_[3]), .op(n1103) );
  inv_2 U1161 ( .ip(buf0_rl), .op(n1104) );
  inv_2 U1162 ( .ip(n202), .op(n1105) );
  inv_2 U1163 ( .ip(idin[30]), .op(n1106) );
  inv_2 U1164 ( .ip(idin[29]), .op(n1107) );
  inv_2 U1165 ( .ip(idin[28]), .op(n1108) );
  inv_2 U1166 ( .ip(idin[3]), .op(n1109) );
  inv_2 U1167 ( .ip(idin[2]), .op(n1110) );
  inv_2 U1168 ( .ip(idin[16]), .op(n1111) );
  inv_2 U1169 ( .ip(idin[15]), .op(n1112) );
  inv_2 U1170 ( .ip(idin[14]), .op(n1113) );
  inv_2 U1171 ( .ip(idin[13]), .op(n1114) );
  inv_2 U1172 ( .ip(idin[12]), .op(n1115) );
  inv_2 U1173 ( .ip(idin[11]), .op(n1116) );
  inv_2 U1174 ( .ip(idin[10]), .op(n1117) );
  inv_2 U1175 ( .ip(idin[9]), .op(n1118) );
  inv_2 U1176 ( .ip(idin[8]), .op(n1119) );
  inv_2 U1177 ( .ip(idin[7]), .op(n1120) );
  inv_2 U1178 ( .ip(idin[6]), .op(n1121) );
  inv_2 U1179 ( .ip(idin[5]), .op(n1122) );
  inv_2 U1180 ( .ip(idin[4]), .op(n1123) );
  inv_2 U1181 ( .ip(idin[31]), .op(n1124) );
  inv_2 U1182 ( .ip(idin[1]), .op(n1125) );
  inv_2 U1183 ( .ip(idin[0]), .op(n1126) );
  inv_2 U1184 ( .ip(idin[27]), .op(n1127) );
  inv_2 U1185 ( .ip(idin[26]), .op(n1128) );
  inv_2 U1186 ( .ip(idin[25]), .op(n1129) );
  inv_2 U1187 ( .ip(idin[24]), .op(n1130) );
  inv_2 U1188 ( .ip(idin[23]), .op(n1131) );
  inv_2 U1189 ( .ip(idin[22]), .op(n1132) );
  inv_2 U1190 ( .ip(idin[21]), .op(n1133) );
  inv_2 U1191 ( .ip(idin[20]), .op(n1134) );
  inv_2 U1192 ( .ip(idin[19]), .op(n1135) );
  inv_2 U1193 ( .ip(idin[18]), .op(n1136) );
  inv_2 U1194 ( .ip(idin[17]), .op(n1137) );
  inv_2 U1195 ( .ip(n347), .op(n1138) );
  inv_2 U1196 ( .ip(adr[1]), .op(n1139) );
  inv_2 U1197 ( .ip(adr[0]), .op(n1140) );
endmodule


module usbf_ep_rf_3_DW01_sub_1 ( A, B, CI, DIFF, CO );
  input [11:0] A;
  input [11:0] B;
  output [11:0] DIFF;
  input CI;
  output CO;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13;
  wire   [12:0] carry;

  fulladder U2_11 ( .a(A[11]), .b(n2), .ci(carry[11]), .s(DIFF[11]) );
  fulladder U2_10 ( .a(A[10]), .b(n3), .ci(carry[10]), .co(carry[11]), .s(
        DIFF[10]) );
  fulladder U2_9 ( .a(A[9]), .b(n13), .ci(carry[9]), .co(carry[10]), .s(
        DIFF[9]) );
  fulladder U2_8 ( .a(A[8]), .b(n12), .ci(carry[8]), .co(carry[9]), .s(DIFF[8]) );
  fulladder U2_7 ( .a(A[7]), .b(n11), .ci(carry[7]), .co(carry[8]), .s(DIFF[7]) );
  fulladder U2_6 ( .a(A[6]), .b(n10), .ci(carry[6]), .co(carry[7]), .s(DIFF[6]) );
  fulladder U2_5 ( .a(A[5]), .b(n9), .ci(carry[5]), .co(carry[6]), .s(DIFF[5])
         );
  fulladder U2_4 ( .a(A[4]), .b(n8), .ci(carry[4]), .co(carry[5]), .s(DIFF[4])
         );
  fulladder U2_3 ( .a(A[3]), .b(n7), .ci(carry[3]), .co(carry[4]), .s(DIFF[3])
         );
  fulladder U2_2 ( .a(A[2]), .b(n6), .ci(carry[2]), .co(carry[3]), .s(DIFF[2])
         );
  fulladder U2_1 ( .a(A[1]), .b(n5), .ci(n1), .co(carry[2]), .s(DIFF[1]) );
  or2_2 U1 ( .ip1(A[0]), .ip2(n4), .op(n1) );
  xnor2_1 U2 ( .ip1(n4), .ip2(A[0]), .op(DIFF[0]) );
  inv_2 U3 ( .ip(B[11]), .op(n2) );
  inv_2 U4 ( .ip(B[10]), .op(n3) );
  inv_2 U5 ( .ip(B[0]), .op(n4) );
  inv_2 U6 ( .ip(B[1]), .op(n5) );
  inv_2 U7 ( .ip(B[2]), .op(n6) );
  inv_2 U8 ( .ip(B[3]), .op(n7) );
  inv_2 U9 ( .ip(B[4]), .op(n8) );
  inv_2 U10 ( .ip(B[5]), .op(n9) );
  inv_2 U11 ( .ip(B[6]), .op(n10) );
  inv_2 U12 ( .ip(B[7]), .op(n11) );
  inv_2 U13 ( .ip(B[8]), .op(n12) );
  inv_2 U14 ( .ip(B[9]), .op(n13) );
endmodule


module usbf_ep_rf_3_DW01_sub_2 ( A, B, CI, DIFF, CO );
  input [11:0] A;
  input [11:0] B;
  output [11:0] DIFF;
  input CI;
  output CO;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12;
  wire   [12:0] carry;

  fulladder U2_8 ( .a(A[8]), .b(n4), .ci(carry[8]), .co(carry[9]), .s(DIFF[8])
         );
  fulladder U2_7 ( .a(A[7]), .b(n5), .ci(carry[7]), .co(carry[8]), .s(DIFF[7])
         );
  fulladder U2_6 ( .a(A[6]), .b(n6), .ci(carry[6]), .co(carry[7]), .s(DIFF[6])
         );
  fulladder U2_5 ( .a(A[5]), .b(n7), .ci(carry[5]), .co(carry[6]), .s(DIFF[5])
         );
  fulladder U2_4 ( .a(A[4]), .b(n8), .ci(carry[4]), .co(carry[5]), .s(DIFF[4])
         );
  fulladder U2_3 ( .a(A[3]), .b(n9), .ci(carry[3]), .co(carry[4]), .s(DIFF[3])
         );
  fulladder U2_2 ( .a(A[2]), .b(n10), .ci(carry[2]), .co(carry[3]), .s(DIFF[2]) );
  fulladder U2_1 ( .a(A[1]), .b(n11), .ci(n2), .co(carry[2]), .s(DIFF[1]) );
  or2_2 U1 ( .ip1(carry[9]), .ip2(A[9]), .op(n1) );
  or2_2 U2 ( .ip1(A[0]), .ip2(n12), .op(n2) );
  nor2_2 U3 ( .ip1(n1), .ip2(A[10]), .op(n3) );
  xnor2_1 U4 ( .ip1(A[10]), .ip2(n1), .op(DIFF[10]) );
  xor2_2 U5 ( .ip1(A[11]), .ip2(n3), .op(DIFF[11]) );
  xnor2_1 U6 ( .ip1(A[9]), .ip2(carry[9]), .op(DIFF[9]) );
  xnor2_1 U7 ( .ip1(n12), .ip2(A[0]), .op(DIFF[0]) );
  inv_2 U8 ( .ip(B[8]), .op(n4) );
  inv_2 U9 ( .ip(B[7]), .op(n5) );
  inv_2 U10 ( .ip(B[6]), .op(n6) );
  inv_2 U11 ( .ip(B[5]), .op(n7) );
  inv_2 U12 ( .ip(B[4]), .op(n8) );
  inv_2 U13 ( .ip(B[3]), .op(n9) );
  inv_2 U14 ( .ip(B[2]), .op(n10) );
  inv_2 U15 ( .ip(B[1]), .op(n11) );
  inv_2 U16 ( .ip(B[0]), .op(n12) );
endmodule


module usbf_ep_rf_3_DW01_inc_0 ( A, SUM );
  input [11:0] A;
  output [11:0] SUM;
  wire   n1, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16;

  inv_2 U1 ( .ip(A[10]), .op(n1) );
  inv_2 U2 ( .ip(A[0]), .op(SUM[0]) );
  inv_2 U3 ( .ip(A[3]), .op(n3) );
  inv_2 U4 ( .ip(A[4]), .op(n4) );
  inv_2 U5 ( .ip(A[7]), .op(n5) );
  inv_2 U6 ( .ip(A[8]), .op(n6) );
  xor2_1 U7 ( .ip1(A[9]), .ip2(n7), .op(SUM[9]) );
  xor2_1 U8 ( .ip1(A[8]), .ip2(n8), .op(SUM[8]) );
  nor2_1 U9 ( .ip1(n9), .ip2(n5), .op(n8) );
  xor2_1 U10 ( .ip1(n9), .ip2(n5), .op(SUM[7]) );
  xnor2_1 U11 ( .ip1(n10), .ip2(A[6]), .op(SUM[6]) );
  nand2_1 U12 ( .ip1(A[5]), .ip2(n11), .op(n10) );
  xor2_1 U13 ( .ip1(A[5]), .ip2(n11), .op(SUM[5]) );
  xor2_1 U14 ( .ip1(A[4]), .ip2(n12), .op(SUM[4]) );
  nor2_1 U15 ( .ip1(n13), .ip2(n3), .op(n12) );
  xor2_1 U16 ( .ip1(n13), .ip2(n3), .op(SUM[3]) );
  xnor2_1 U17 ( .ip1(n14), .ip2(A[2]), .op(SUM[2]) );
  nand2_1 U18 ( .ip1(A[1]), .ip2(A[0]), .op(n14) );
  xor2_1 U19 ( .ip1(A[1]), .ip2(A[0]), .op(SUM[1]) );
  xor2_1 U20 ( .ip1(A[11]), .ip2(n15), .op(SUM[11]) );
  nor2_1 U21 ( .ip1(n16), .ip2(n1), .op(n15) );
  xor2_1 U22 ( .ip1(n16), .ip2(n1), .op(SUM[10]) );
  nand2_1 U23 ( .ip1(n7), .ip2(A[9]), .op(n16) );
  nor3_1 U24 ( .ip1(n5), .ip2(n9), .ip3(n6), .op(n7) );
  nand3_1 U25 ( .ip1(A[5]), .ip2(n11), .ip3(A[6]), .op(n9) );
  nor3_1 U26 ( .ip1(n3), .ip2(n13), .ip3(n4), .op(n11) );
  nand3_1 U27 ( .ip1(A[1]), .ip2(A[0]), .ip3(A[2]), .op(n13) );
endmodule


module usbf_ep_rf_3_DW01_add_0 ( A, B, CI, SUM, CO );
  input [11:0] A;
  input [11:0] B;
  output [11:0] SUM;
  input CI;
  output CO;
  wire   n1, n2, n3;
  wire   [11:1] carry;

  fulladder U1_8 ( .a(A[8]), .b(B[8]), .ci(carry[8]), .co(carry[9]), .s(SUM[8]) );
  fulladder U1_7 ( .a(A[7]), .b(B[7]), .ci(carry[7]), .co(carry[8]), .s(SUM[7]) );
  fulladder U1_6 ( .a(A[6]), .b(B[6]), .ci(carry[6]), .co(carry[7]), .s(SUM[6]) );
  fulladder U1_5 ( .a(A[5]), .b(B[5]), .ci(carry[5]), .co(carry[6]), .s(SUM[5]) );
  fulladder U1_4 ( .a(A[4]), .b(B[4]), .ci(carry[4]), .co(carry[5]), .s(SUM[4]) );
  fulladder U1_3 ( .a(A[3]), .b(B[3]), .ci(carry[3]), .co(carry[4]), .s(SUM[3]) );
  fulladder U1_2 ( .a(A[2]), .b(B[2]), .ci(carry[2]), .co(carry[3]), .s(SUM[2]) );
  fulladder U1_1 ( .a(A[1]), .b(B[1]), .ci(n2), .co(carry[2]), .s(SUM[1]) );
  and2_2 U1 ( .ip1(A[9]), .ip2(carry[9]), .op(n1) );
  and2_2 U2 ( .ip1(B[0]), .ip2(A[0]), .op(n2) );
  and2_2 U3 ( .ip1(A[10]), .ip2(n1), .op(n3) );
  xor2_2 U4 ( .ip1(A[11]), .ip2(n3), .op(SUM[11]) );
  xor2_2 U5 ( .ip1(A[10]), .ip2(n1), .op(SUM[10]) );
  xor2_2 U6 ( .ip1(A[9]), .ip2(carry[9]), .op(SUM[9]) );
  xor2_2 U7 ( .ip1(B[0]), .ip2(A[0]), .op(SUM[0]) );
endmodule


module usbf_ep_rf_3_DW01_dec_0 ( A, SUM );
  input [11:0] A;
  output [11:0] SUM;
  wire   n1, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21;

  inv_2 U1 ( .ip(n21), .op(n1) );
  inv_2 U2 ( .ip(A[0]), .op(SUM[0]) );
  nand2_1 U3 ( .ip1(n1), .ip2(n3), .op(SUM[9]) );
  nand2_1 U4 ( .ip1(A[9]), .ip2(n4), .op(n3) );
  nand2_1 U5 ( .ip1(n4), .ip2(n5), .op(SUM[8]) );
  nand2_1 U6 ( .ip1(A[8]), .ip2(n6), .op(n5) );
  nand2_1 U7 ( .ip1(n6), .ip2(n7), .op(SUM[7]) );
  nand2_1 U8 ( .ip1(A[7]), .ip2(n8), .op(n7) );
  nand2_1 U9 ( .ip1(n8), .ip2(n9), .op(SUM[6]) );
  nand2_1 U10 ( .ip1(A[6]), .ip2(n10), .op(n9) );
  nand2_1 U11 ( .ip1(n10), .ip2(n11), .op(SUM[5]) );
  nand2_1 U12 ( .ip1(A[5]), .ip2(n12), .op(n11) );
  nand2_1 U13 ( .ip1(n12), .ip2(n13), .op(SUM[4]) );
  nand2_1 U14 ( .ip1(A[4]), .ip2(n14), .op(n13) );
  nand2_1 U15 ( .ip1(n14), .ip2(n15), .op(SUM[3]) );
  nand2_1 U16 ( .ip1(A[3]), .ip2(n16), .op(n15) );
  nand2_1 U17 ( .ip1(n16), .ip2(n17), .op(SUM[2]) );
  nand2_1 U18 ( .ip1(A[2]), .ip2(n18), .op(n17) );
  nand2_1 U19 ( .ip1(n18), .ip2(n19), .op(SUM[1]) );
  nand2_1 U20 ( .ip1(A[1]), .ip2(A[0]), .op(n19) );
  xor2_1 U21 ( .ip1(A[11]), .ip2(n20), .op(SUM[11]) );
  nor2_1 U22 ( .ip1(A[10]), .ip2(n1), .op(n20) );
  xor2_1 U23 ( .ip1(A[10]), .ip2(n21), .op(SUM[10]) );
  nor2_1 U24 ( .ip1(n4), .ip2(A[9]), .op(n21) );
  or2_1 U25 ( .ip1(n6), .ip2(A[8]), .op(n4) );
  or2_1 U26 ( .ip1(n8), .ip2(A[7]), .op(n6) );
  or2_1 U27 ( .ip1(n10), .ip2(A[6]), .op(n8) );
  or2_1 U28 ( .ip1(n12), .ip2(A[5]), .op(n10) );
  or2_1 U29 ( .ip1(n14), .ip2(A[4]), .op(n12) );
  or2_1 U30 ( .ip1(n16), .ip2(A[3]), .op(n14) );
  or2_1 U31 ( .ip1(n18), .ip2(A[2]), .op(n16) );
  or2_1 U32 ( .ip1(A[1]), .ip2(A[0]), .op(n18) );
endmodule


module usbf_ep_rf_3 ( clk, wclk, rst, adr, re, we, din, dout, inta, intb, 
        dma_req, dma_ack, idin, ep_sel, ep_match, buf0_rl, buf0_set, buf1_set, 
        uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, 
        int_crc16_set, int_to_set, int_seqerr_set, out_to_small, csr, buf0, 
        buf1, dma_in_buf_sz1, dma_out_buf_avail );
  input [1:0] adr;
  input [31:0] din;
  output [31:0] dout;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  input clk, wclk, rst, re, we, dma_ack, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small;
  output inta, intb, dma_req, ep_match, dma_in_buf_sz1, dma_out_buf_avail;
  wire   int__29, int__28, int__27, int__26, int__25, int__24, int__21,
         int__20, int__19, int__18, int__17, int__16, ep_match_r, N191, int_re,
         N221, N222, set_r, N229, N230, N231, N232, N233, N234, N235, N236,
         N237, N238, N239, N240, N242, N243, N244, N245, N246, N247, N248,
         N249, N250, N251, N252, N253, N271, N272, dma_req_out_hold, N278,
         N279, N280, N281, N282, N283, N284, N285, N286, N287, N288, N289,
         N291, N292, N293, N294, N295, N296, N297, N298, N299, N300, N301,
         N302, N319, N320, N321, N322, N323, N324, N325, N326, N327, N328,
         N329, N330, N331, N332, N333, N336, N337, N338, N339, N340, N341,
         N342, N343, N344, N345, N346, N347, dma_req_in_hold2, N348,
         dma_req_in_hold, r1, r2, r4, r5, N361, dma_ack_clr1, dma_ack_wr1, n1,
         n2, n3, n4, n5, n7, n8, n11, n12, n13, n14, n15, n16, n17, n18, n19,
         n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33,
         n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47,
         n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61,
         n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75,
         n76, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90,
         n91, n92, n93, n94, n95, n96, n97, n98, n99, n100, n101, n102, n103,
         n104, n105, n106, n107, n108, n109, n110, n111, n112, n113, n114,
         n115, n116, n117, n118, n119, n120, n121, n122, n123, n124, n125,
         n167, n170, n173, n176, n179, n182, n185, n188, n191, n194, n197,
         n200, n203, n204, n221, n540, n541, n706, n707, n708, n709, n710,
         n711, n712, n713, n722, n723, n724, n725, n729, n730, n731, n732,
         n908, n909, n910, n911, n912, n913, n914, n915, n916, n917, n918,
         n919, n920, n921, n922, n923, n924, n925, n926, n927, n928, n929,
         n930, n931, n932, n933, n934, n935, n936, n937, n938, n939, n940,
         n941, n942, n943, n944, n945, n946, n947, n948, n949, n950, n951,
         n952, n953, n954, n955, n956, n957, n958, n959, n960, n961, n962,
         n963, n964, n965, n966, n967, n968, n969, n970, n971, n972, n973,
         n974, n975, n976, n977, n978, n979, n980, n981, n982, n983, n984,
         n985, n986, n987, n988, n989, n990, n991, n992, n993, n994, n995,
         n996, n997, n998, n999, n1000, n1001, n1002, n1003, n1004, n1005,
         n1006, n1007, n1008, n1009, n1010, n1011, n1012, n1013, n1014, n1015,
         n1016, n1017, n1018, n1019, n1020, n1021, n1022, n1023, n1024, n1025,
         n1026, n1027, n1028, n1029, n1030, n1031, n1032, n1033, n1034, n1035,
         n1036, n1037, n1038, n1039, n1040, n1041, n1042, n1043, n1044, n1045,
         n1046, n1047, n1048, n1049, n1050, n1051, n1052, n1053, n1054, n1055,
         n1056, n1057, n1058, n1059, n1060, n1061, n1062, n1063, n1064, n1065,
         n1066, n1067, n1068, n1069, n1070, n1071, n1072, n1073, n1074, n1075,
         n1076, n1077, n1078, n1079, n1080, n1081, n1082, n1083, n1084, n1085,
         n1086, n1087, n1088, n1089, n1090, n1091, n1092, n1093, n1094, n1095,
         n1096, n1097, n1098, n1099, n1100, n1101, n1102, n1103, n1104, n1105,
         n1106, n1107, n1108, n1109, n1110, n1111, n1112, n1113, n1114, n1115,
         n1116, n1117, n1118, n1119, n1120, n1121, n1122, n1123, n1124, n1125,
         n1126, n1127, n1128, n1129, n1130, n1131, n1132, n1133, n1134, n1135,
         n1136, n1137, n1138, n1139, n1140, n1141, n1142, n1143, n1144, n1145,
         n1146, n1147, n1148, n1149, n1150, n1151, n1152, n1153, n1154, n1155,
         n1156, n1157, n1158, n1159, n1160, n1161, n1162, n1163, n1164, n1165,
         n1166, n1167, n1168, n1169, n1170, n1171, n1172, n1173, n1174, n1175,
         n1176, n1177, n1178, n1179, n1180, n1181, n1182, n1183, n1184, n1185,
         n1186, n1187, n1188, n1189, n1190, n1191, n1192, n1193, n1194, n1195,
         n1196, n1197, n1198, n1199, n1200, n1201, n1202, n1203, n1204, n1205,
         n1206, n1207, n1208, n1209, n1210, n1211, n1212, n1213, n1214, n1215,
         n1216, n1217, n1218, n1219, n1220, n1221, n1222, n1223, n1224, n1225,
         n1226, n1227, n1228, n1229, n1230, n1231, n1232, n1233, n1234, n1235,
         n1236, n1237, n1238, n1239, n1240, n1241, n1242, n1243, n1244, n1245,
         n1246, n1247, n1248, n1249, n1250, n1251, n1252, n1253, n1254, n1255,
         n1256, n1257, n1258, n1259, n1260, n1261, n1262, n1263, n1264, n1265,
         n1266, n1267, n1268, n1269, n1270, n1271, n1272, n1273, n1274, n1275,
         n1276, n1277, n1278, n1279, n1280, n1281, n1282, n1283, n1284, n1285,
         n1286, n1287, n1288, n1289, n1290, n1291, n1292, n1293, n1294, n1295,
         n1296, n1297, n1298, n1299, n1300, n1301, n1302, n1303, n1304, n1305,
         n1306, n1307, n1308, n1309, n1310, n1311, n1312, n1313, n1314, n1315,
         n1316, n1317, n1318, n1319, n1320, n1321, n1322, n1323, n1324, n1325,
         n1326, n1327, n1328, n1329, n1330, n1331, n1332, n1333, n1334, n1335,
         n1336, n1337, n1338, n1339, n1340, n1341, n1342, n1343, n1344, n1345,
         n1346, n1347, n1348, n1349, n1350, n1351, n1352, n1353, n1354, n1355,
         n1356, n1357, n1358, n1359, n1360, n1361, n1362, n1363, n1364, n1365,
         n1366, n1367, n1368, n1369, n1370, n1371, n1372, n1373, n1374, n1375,
         n1376, n1377, n1378, n1379, n1380, n1381, n1382, n1383, n1384, n1385,
         n1386, n1387, n1388, n1389, n1390, n1391, n1392, n1393, n1394, n1395,
         n1396, n1397, n1398, n1399, n1400, n1401, n1402, n1403, n1404, n1405,
         n1406, n1407, n1408, n1409, n1410, n1411, n1412, n1413, n1414, n1415,
         n1416, n1417, n1418, n1419, n1420, n1421, n1422, n1423, n1424, n1425,
         n1426, n1427, n1428, n1429, n1430, n1431, n1432, n1433, n1434, n1435,
         n1436, n1437, n1438, n1439, n1440, n1441, n1442, n1443, n1444, n1445,
         n1446, n1447, n1448, n1449, n1450, n1451, n1452, n1453, n1454, n1455,
         n1456, n1457, n1458, n1459, n1460, n1461, n1462, n1463, n1464, n1465,
         n1466, n1467, n1468, n1469, n1470, n1471, n1472, n1473, n1474, n1475,
         n1476, n1477, n1478, n1479, n1480, n1481, n1482, n1483, n1484, n1485,
         n1486, n1487, n1488, n1489, n1490, n1491, n1492, n1493, n1494, n1495,
         n1496, n1497, n1498, n1499, n1500, n1501, n1502, n1503, n1504, n1505,
         n1506, n1507, n1508, n1509, n1510, n1511, n1512, n1513, n1514, n1515,
         n1516, n1517, n1518, n1519, n1520, n1521, n1522, n1523, n1524, n1525,
         n1526, n1527, n1528, n1529, n1530, n1531, n1532, n1533, n1534, n1535,
         n1536, n1537, n1538, n1539, n1540, n1541, n1542, n1543, n1544, n1545,
         n1546, n1547, n1548, n1549, n1550, n1551, n1552, n1553, n1554, n1555,
         n1556, n1557, n1558, n1559, n1560, n1561, n1562, n1563, n1564, n1565,
         n1566, n1567, n1568, n1569, n1570, n1571, n1572, n1573, n1574, n1575,
         n1576, n1577, n1578, n1579, n1580, n1581, n1582, n1583, n1584, n1585,
         n1586, n1587, n1588, n1589, n1590, n1591, n1592, n1593, n1594, n1595,
         n1596, n1597, n1598, n1599, n1600, n1601, n1602, n1603, n1604, n1605,
         n1606, n1607, n1608, n1609, n1610, n1611, n1612, n1613, n1614, n1615,
         n1616, n1617, n1618, n1619, n1620, n1621, n1622, n1623, n1624, n1625,
         n1626, n1627, n1628, n1629, n1630, n1631, n1632, n1633, n1634, n1635,
         n1636, n1637, n1638, n1639, n1640, n1641, n1642, n1643, n1644, n1645,
         n1646, n1647, n1648, n1649, n1650, n1651, n1652, n1653, n1654, n1655,
         n1656, n1657, n1658, n1659, n1660, n1661, n1662, n1663, n1664, n1665,
         n1666, n1667, n1668, n1669, n1670, n1671, n1672, n1673, n1674, n1675,
         n1676, n1677, n1678, n1679, n1680, n1681, n1682, n1683, n1684, n1685,
         n1686, n1687, n1688, n1689, n1690, n1691, n1692, n1693, n1694, n1695,
         n1696, n1697, n1698, n1699, n1700, n1701, n1702, n1703, n1704, n1705,
         n1706, n1707, n1708, n1709, n1710, n1711, n1712, n1713, n1714, n1715,
         n1716, n1717, n1718, n1719, n1720, n1721, n1722, n1723, n1724, n1725,
         n1726, n1727, n1728, n1729, n1730, n1731, n1732, n1733, n1734, n1735,
         n1736, n1737, n1738, n1739, n1740, n1741, n1742, n1743, n1744, n1745,
         n1746, n1747, n1748, n1749, n1750, n1751, n1752, n1753, n1754, n1755,
         n1756, n1757, n1758, n1759, n1760, n1761, n1762, n1763, n1764, n1765,
         n1766, n1767, n1768, n1769, n1770, n1771, n1772, n1773, n1774, n1775,
         n1776, n1777, n1778, n1779, n1780, n1781, n1782, n1783, n1784, n1785,
         n1786, n1787, n1788, n1789, n1790, n1791, n1792, n1793, n1794, n1795,
         n1796, n1797, n1798, n1799, n1800, n1801, n1802, n1803, n1804, n1805,
         n1806, n1807, n1808, n1809, n1810, n1811, n1812, n1813, n1814, n1815,
         n1816, n1817, n1818, n1819, n1820, n1821, n1822, n1823, n1824, n1825,
         n1826, n1827, n1828, n1829, n1830, n1831, n1832, n1833, n1834, n1835,
         n1836, n1837, n1838, n1839, n1840, n1841, n1842, n1843, n1844, n1845,
         n1846, n1847, n1848, n1849, n1850, n1851, n1852, n1853, n1854, n1855,
         n1856, n1857, n1858, n1859, n1860, n1861, n1862, n1863, n1864, n1865,
         n1866, n1867, n1868, n1869, n1870, n1871, n1872, n1873, n1874, n1875,
         n1876, n1877, n1878, n1879, n1880, n1881, n1882, n1883, n1884, n1885,
         n1886, n1887, n1888, n1889;
  wire   [6:0] int_;
  wire   [31:0] buf0_orig;
  wire   [11:0] dma_out_cnt;
  wire   [11:0] dma_in_cnt;
  wire   [11:0] dma_out_left;
  wire   [11:0] buf0_orig_m3;
  assign csr[14] = 1'b0;

  nand3_2 U24 ( .ip1(n1888), .ip2(n1887), .ip3(n1886), .op(n1310) );
  nand2_2 U25 ( .ip1(dma_out_cnt[10]), .ip2(n101), .op(n1886) );
  nand2_2 U26 ( .ip1(N252), .ip2(n36), .op(n1887) );
  nand2_2 U27 ( .ip1(N239), .ip2(n100), .op(n1888) );
  nand3_2 U28 ( .ip1(n1882), .ip2(n1881), .ip3(n1880), .op(n1309) );
  nand2_2 U29 ( .ip1(dma_out_cnt[9]), .ip2(n101), .op(n1880) );
  nand2_2 U30 ( .ip1(N251), .ip2(n1884), .op(n1881) );
  nand2_2 U31 ( .ip1(N238), .ip2(n99), .op(n1882) );
  nand3_2 U32 ( .ip1(n1879), .ip2(n1878), .ip3(n1877), .op(n1308) );
  nand2_2 U33 ( .ip1(dma_out_cnt[8]), .ip2(n101), .op(n1877) );
  nand2_2 U34 ( .ip1(N250), .ip2(n36), .op(n1878) );
  nand2_2 U35 ( .ip1(N237), .ip2(n99), .op(n1879) );
  nand3_2 U36 ( .ip1(n1876), .ip2(n1875), .ip3(n1874), .op(n1307) );
  nand2_2 U37 ( .ip1(dma_out_cnt[7]), .ip2(n101), .op(n1874) );
  nand2_2 U38 ( .ip1(N249), .ip2(n1884), .op(n1875) );
  nand2_2 U39 ( .ip1(N236), .ip2(n99), .op(n1876) );
  nand3_2 U40 ( .ip1(n1873), .ip2(n1872), .ip3(n1871), .op(n1306) );
  nand2_2 U41 ( .ip1(dma_out_cnt[6]), .ip2(n101), .op(n1871) );
  nand2_2 U42 ( .ip1(N248), .ip2(n36), .op(n1872) );
  nand2_2 U43 ( .ip1(N235), .ip2(n99), .op(n1873) );
  nand3_2 U44 ( .ip1(n1870), .ip2(n1869), .ip3(n1868), .op(n1305) );
  nand2_2 U45 ( .ip1(dma_out_cnt[5]), .ip2(n101), .op(n1868) );
  nand2_2 U46 ( .ip1(N247), .ip2(n1884), .op(n1869) );
  nand2_2 U47 ( .ip1(N234), .ip2(n99), .op(n1870) );
  nand3_2 U48 ( .ip1(n1867), .ip2(n1866), .ip3(n1865), .op(n1304) );
  nand2_2 U49 ( .ip1(dma_out_cnt[4]), .ip2(n101), .op(n1865) );
  nand2_2 U50 ( .ip1(N246), .ip2(n36), .op(n1866) );
  nand2_2 U51 ( .ip1(N233), .ip2(n99), .op(n1867) );
  nand3_2 U52 ( .ip1(n1864), .ip2(n1863), .ip3(n1862), .op(n1303) );
  nand2_2 U53 ( .ip1(dma_out_cnt[3]), .ip2(n101), .op(n1862) );
  nand2_2 U54 ( .ip1(N245), .ip2(n1884), .op(n1863) );
  nand2_2 U55 ( .ip1(N232), .ip2(n99), .op(n1864) );
  nand3_2 U56 ( .ip1(n1861), .ip2(n1860), .ip3(n1859), .op(n1302) );
  nand2_2 U57 ( .ip1(dma_out_cnt[2]), .ip2(n101), .op(n1859) );
  nand2_2 U58 ( .ip1(N244), .ip2(n1884), .op(n1860) );
  nand2_2 U59 ( .ip1(N231), .ip2(n99), .op(n1861) );
  nand3_2 U60 ( .ip1(n1858), .ip2(n1857), .ip3(n1856), .op(n1301) );
  nand2_2 U61 ( .ip1(dma_out_cnt[1]), .ip2(n101), .op(n1856) );
  nand2_2 U62 ( .ip1(N243), .ip2(n1884), .op(n1857) );
  nand2_2 U63 ( .ip1(N230), .ip2(n99), .op(n1858) );
  nand3_2 U64 ( .ip1(n1855), .ip2(n1854), .ip3(n1853), .op(n1300) );
  nand2_2 U65 ( .ip1(dma_out_cnt[0]), .ip2(n102), .op(n1853) );
  nand2_2 U66 ( .ip1(N242), .ip2(n1884), .op(n1854) );
  nand2_2 U67 ( .ip1(N229), .ip2(n99), .op(n1855) );
  nand3_2 U68 ( .ip1(n1852), .ip2(n1851), .ip3(n1850), .op(n1299) );
  nand2_2 U69 ( .ip1(dma_out_cnt[11]), .ip2(n101), .op(n1850) );
  nand2_2 U70 ( .ip1(N253), .ip2(n1884), .op(n1851) );
  nand2_2 U71 ( .ip1(N240), .ip2(n99), .op(n1852) );
  nand2_2 U74 ( .ip1(N301), .ip2(n36), .op(n1848) );
  nand2_2 U75 ( .ip1(N288), .ip2(n100), .op(n1849) );
  nand2_2 U78 ( .ip1(N300), .ip2(n1884), .op(n1846) );
  nand2_2 U79 ( .ip1(N287), .ip2(n100), .op(n1847) );
  nand2_2 U82 ( .ip1(N299), .ip2(n36), .op(n1844) );
  nand2_2 U83 ( .ip1(N286), .ip2(n100), .op(n1845) );
  nand2_2 U86 ( .ip1(N298), .ip2(n36), .op(n1842) );
  nand2_2 U87 ( .ip1(N285), .ip2(n100), .op(n1843) );
  nand2_2 U90 ( .ip1(N297), .ip2(n36), .op(n1840) );
  nand2_2 U91 ( .ip1(N284), .ip2(n100), .op(n1841) );
  nand2_2 U94 ( .ip1(N296), .ip2(n1884), .op(n1838) );
  nand2_2 U95 ( .ip1(N283), .ip2(n100), .op(n1839) );
  nand2_2 U98 ( .ip1(N295), .ip2(n36), .op(n1836) );
  nand2_2 U99 ( .ip1(N282), .ip2(n100), .op(n1837) );
  nand2_2 U102 ( .ip1(N294), .ip2(n1884), .op(n1834) );
  nand2_2 U103 ( .ip1(N281), .ip2(n100), .op(n1835) );
  nand2_2 U106 ( .ip1(N293), .ip2(n36), .op(n1832) );
  nand2_2 U107 ( .ip1(N280), .ip2(n100), .op(n1833) );
  nand2_2 U110 ( .ip1(N292), .ip2(n36), .op(n1830) );
  nand2_2 U111 ( .ip1(N279), .ip2(n100), .op(n1831) );
  nand2_2 U114 ( .ip1(N291), .ip2(n36), .op(n1828) );
  nand2_2 U115 ( .ip1(N278), .ip2(n100), .op(n1829) );
  nand2_2 U119 ( .ip1(N302), .ip2(n36), .op(n1826) );
  nor4_2 U120 ( .ip1(n1070), .ip2(n1098), .ip3(r5), .ip4(n1825), .op(n1884) );
  nor2_2 U121 ( .ip1(set_r), .ip2(n1104), .op(n1825) );
  nand2_2 U122 ( .ip1(N289), .ip2(n99), .op(n1827) );
  nor2_2 U123 ( .ip1(n984), .ip2(n1070), .op(n1883) );
  nand2_2 U127 ( .ip1(n1823), .ip2(n1822), .op(n1285) );
  nand3_2 U128 ( .ip1(n1821), .ip2(n1820), .ip3(idin[0]), .op(n1822) );
  nand2_2 U129 ( .ip1(csr[30]), .ip2(n1100), .op(n1823) );
  nand2_2 U130 ( .ip1(n1819), .ip2(n1818), .op(n1284) );
  nand3_2 U131 ( .ip1(n1821), .ip2(n1820), .ip3(idin[1]), .op(n1818) );
  nand2_2 U132 ( .ip1(csr[31]), .ip2(n1100), .op(n1819) );
  nand2_2 U133 ( .ip1(rst), .ip2(n1817), .op(n1820) );
  nand2_2 U134 ( .ip1(uc_bsel_set), .ip2(ep_match_r), .op(n1817) );
  nand2_2 U135 ( .ip1(n1816), .ip2(n1815), .op(n1283) );
  nand3_2 U136 ( .ip1(n1821), .ip2(n1814), .ip3(idin[2]), .op(n1815) );
  nand2_2 U137 ( .ip1(csr[28]), .ip2(n1099), .op(n1816) );
  nand2_2 U138 ( .ip1(n1813), .ip2(n1812), .op(n1282) );
  nand3_2 U139 ( .ip1(n1821), .ip2(n1814), .ip3(idin[3]), .op(n1812) );
  nand2_2 U140 ( .ip1(csr[29]), .ip2(n1099), .op(n1813) );
  nand2_2 U141 ( .ip1(rst), .ip2(n1811), .op(n1814) );
  nand2_2 U142 ( .ip1(uc_dpd_set), .ip2(ep_match_r), .op(n1811) );
  nand2_2 U143 ( .ip1(n1810), .ip2(n1809), .op(n1281) );
  nand2_2 U144 ( .ip1(int_[0]), .ip2(n1097), .op(n1809) );
  nand2_2 U145 ( .ip1(n1), .ip2(n1808), .op(n1810) );
  nand2_2 U146 ( .ip1(n1807), .ip2(n1806), .op(n1808) );
  nand2_2 U147 ( .ip1(int_to_set), .ip2(n1), .op(n1806) );
  nand2_2 U148 ( .ip1(n1805), .ip2(n1804), .op(n1280) );
  nand2_2 U149 ( .ip1(int_[1]), .ip2(n1096), .op(n1804) );
  nand2_2 U150 ( .ip1(n1), .ip2(n1803), .op(n1805) );
  nand2_2 U151 ( .ip1(n1807), .ip2(n1802), .op(n1803) );
  nand2_2 U152 ( .ip1(int_crc16_set), .ip2(n1), .op(n1802) );
  nand2_2 U153 ( .ip1(n1801), .ip2(n1800), .op(n1279) );
  nand2_2 U154 ( .ip1(int_[2]), .ip2(n1095), .op(n1800) );
  nand2_2 U155 ( .ip1(n1), .ip2(n1799), .op(n1801) );
  nand2_2 U156 ( .ip1(n1807), .ip2(n1798), .op(n1799) );
  nand2_2 U157 ( .ip1(int_upid_set), .ip2(n1), .op(n1798) );
  nand2_2 U158 ( .ip1(n1797), .ip2(n1796), .op(n1278) );
  or2_2 U159 ( .ip1(n1102), .ip2(n1795), .op(n1796) );
  nand2_2 U160 ( .ip1(n1), .ip2(n1795), .op(n1797) );
  nand2_2 U161 ( .ip1(n1807), .ip2(n1794), .op(n1795) );
  nand2_2 U162 ( .ip1(int_buf0_set), .ip2(n1), .op(n1794) );
  nand2_2 U163 ( .ip1(n1793), .ip2(n1792), .op(n1277) );
  or2_2 U164 ( .ip1(n1101), .ip2(n1791), .op(n1792) );
  nand2_2 U165 ( .ip1(n1), .ip2(n1791), .op(n1793) );
  nand2_2 U166 ( .ip1(n1807), .ip2(n1790), .op(n1791) );
  nand2_2 U167 ( .ip1(int_buf1_set), .ip2(n1), .op(n1790) );
  nand2_2 U168 ( .ip1(n1789), .ip2(n1788), .op(n1276) );
  nand2_2 U169 ( .ip1(int_[5]), .ip2(n1094), .op(n1788) );
  nand2_2 U170 ( .ip1(n1), .ip2(n1787), .op(n1789) );
  nand2_2 U171 ( .ip1(n1807), .ip2(n1786), .op(n1787) );
  nand2_2 U172 ( .ip1(int_seqerr_set), .ip2(n1), .op(n1786) );
  nand2_2 U173 ( .ip1(n1785), .ip2(n1784), .op(n1275) );
  nand2_2 U174 ( .ip1(int_[6]), .ip2(n1093), .op(n1784) );
  nand2_2 U175 ( .ip1(n1), .ip2(n1783), .op(n1785) );
  nand2_2 U176 ( .ip1(n1807), .ip2(n1782), .op(n1783) );
  nand2_2 U177 ( .ip1(out_to_small), .ip2(n1), .op(n1782) );
  nor2_2 U178 ( .ip1(n1889), .ip2(int_re), .op(n1807) );
  nand4_2 U180 ( .ip1(rst), .ip2(n1781), .ip3(n1780), .ip4(n1779), .op(n1274)
         );
  nand2_2 U181 ( .ip1(idin[31]), .ip2(n97), .op(n1779) );
  nand2_2 U182 ( .ip1(din[31]), .ip2(n52), .op(n1780) );
  nand2_2 U183 ( .ip1(buf1[31]), .ip2(n95), .op(n1781) );
  nand4_2 U184 ( .ip1(rst), .ip2(n1776), .ip3(n1775), .ip4(n1774), .op(n1273)
         );
  nand2_2 U185 ( .ip1(idin[30]), .ip2(n96), .op(n1774) );
  nand2_2 U186 ( .ip1(din[30]), .ip2(n52), .op(n1775) );
  nand2_2 U187 ( .ip1(buf1[30]), .ip2(n95), .op(n1776) );
  nand4_2 U188 ( .ip1(rst), .ip2(n1773), .ip3(n1772), .ip4(n1771), .op(n1272)
         );
  nand2_2 U189 ( .ip1(idin[29]), .ip2(n96), .op(n1771) );
  nand2_2 U190 ( .ip1(din[29]), .ip2(n52), .op(n1772) );
  nand2_2 U191 ( .ip1(buf1[29]), .ip2(n95), .op(n1773) );
  nand4_2 U192 ( .ip1(rst), .ip2(n1770), .ip3(n1769), .ip4(n1768), .op(n1271)
         );
  nand2_2 U193 ( .ip1(idin[28]), .ip2(n96), .op(n1768) );
  nand2_2 U194 ( .ip1(din[28]), .ip2(n52), .op(n1769) );
  nand2_2 U195 ( .ip1(buf1[28]), .ip2(n95), .op(n1770) );
  nand4_2 U196 ( .ip1(rst), .ip2(n1767), .ip3(n1766), .ip4(n1765), .op(n1270)
         );
  nand2_2 U197 ( .ip1(idin[27]), .ip2(n96), .op(n1765) );
  nand2_2 U198 ( .ip1(din[27]), .ip2(n52), .op(n1766) );
  nand2_2 U199 ( .ip1(buf1[27]), .ip2(n95), .op(n1767) );
  nand4_2 U200 ( .ip1(rst), .ip2(n1764), .ip3(n1763), .ip4(n1762), .op(n1269)
         );
  nand2_2 U201 ( .ip1(idin[26]), .ip2(n96), .op(n1762) );
  nand2_2 U202 ( .ip1(din[26]), .ip2(n52), .op(n1763) );
  nand2_2 U203 ( .ip1(buf1[26]), .ip2(n95), .op(n1764) );
  nand4_2 U204 ( .ip1(rst), .ip2(n1761), .ip3(n1760), .ip4(n1759), .op(n1268)
         );
  nand2_2 U205 ( .ip1(idin[25]), .ip2(n96), .op(n1759) );
  nand2_2 U206 ( .ip1(din[25]), .ip2(n52), .op(n1760) );
  nand2_2 U207 ( .ip1(buf1[25]), .ip2(n95), .op(n1761) );
  nand4_2 U208 ( .ip1(rst), .ip2(n1758), .ip3(n1757), .ip4(n1756), .op(n1267)
         );
  nand2_2 U209 ( .ip1(idin[24]), .ip2(n96), .op(n1756) );
  nand2_2 U210 ( .ip1(din[24]), .ip2(n52), .op(n1757) );
  nand2_2 U211 ( .ip1(buf1[24]), .ip2(n95), .op(n1758) );
  nand4_2 U212 ( .ip1(rst), .ip2(n1755), .ip3(n1754), .ip4(n1753), .op(n1266)
         );
  nand2_2 U213 ( .ip1(idin[23]), .ip2(n96), .op(n1753) );
  nand2_2 U214 ( .ip1(din[23]), .ip2(n52), .op(n1754) );
  nand2_2 U215 ( .ip1(buf1[23]), .ip2(n94), .op(n1755) );
  nand4_2 U216 ( .ip1(rst), .ip2(n1752), .ip3(n1751), .ip4(n1750), .op(n1265)
         );
  nand2_2 U217 ( .ip1(idin[22]), .ip2(n96), .op(n1750) );
  nand2_2 U218 ( .ip1(din[22]), .ip2(n52), .op(n1751) );
  nand2_2 U219 ( .ip1(buf1[22]), .ip2(n94), .op(n1752) );
  nand4_2 U220 ( .ip1(rst), .ip2(n1749), .ip3(n1748), .ip4(n1747), .op(n1264)
         );
  nand2_2 U221 ( .ip1(idin[21]), .ip2(n96), .op(n1747) );
  nand2_2 U222 ( .ip1(din[21]), .ip2(n52), .op(n1748) );
  nand2_2 U223 ( .ip1(buf1[21]), .ip2(n94), .op(n1749) );
  nand4_2 U224 ( .ip1(rst), .ip2(n1746), .ip3(n1745), .ip4(n1744), .op(n1263)
         );
  nand2_2 U225 ( .ip1(idin[20]), .ip2(n96), .op(n1744) );
  nand2_2 U226 ( .ip1(din[20]), .ip2(n52), .op(n1745) );
  nand2_2 U227 ( .ip1(buf1[20]), .ip2(n94), .op(n1746) );
  nand4_2 U228 ( .ip1(rst), .ip2(n1743), .ip3(n1742), .ip4(n1741), .op(n1262)
         );
  nand2_2 U229 ( .ip1(idin[19]), .ip2(n96), .op(n1741) );
  nand2_2 U230 ( .ip1(din[19]), .ip2(n53), .op(n1742) );
  nand2_2 U231 ( .ip1(buf1[19]), .ip2(n94), .op(n1743) );
  nand4_2 U232 ( .ip1(rst), .ip2(n1740), .ip3(n1739), .ip4(n1738), .op(n1261)
         );
  nand2_2 U233 ( .ip1(idin[18]), .ip2(n97), .op(n1738) );
  nand2_2 U234 ( .ip1(din[18]), .ip2(n53), .op(n1739) );
  nand2_2 U235 ( .ip1(buf1[18]), .ip2(n94), .op(n1740) );
  nand4_2 U236 ( .ip1(rst), .ip2(n1737), .ip3(n1736), .ip4(n1735), .op(n1260)
         );
  nand2_2 U237 ( .ip1(idin[17]), .ip2(n97), .op(n1735) );
  nand2_2 U238 ( .ip1(din[17]), .ip2(n53), .op(n1736) );
  nand2_2 U239 ( .ip1(buf1[17]), .ip2(n94), .op(n1737) );
  nand4_2 U240 ( .ip1(rst), .ip2(n1734), .ip3(n1733), .ip4(n1732), .op(n1259)
         );
  nand2_2 U241 ( .ip1(idin[16]), .ip2(n97), .op(n1732) );
  nand2_2 U242 ( .ip1(din[16]), .ip2(n53), .op(n1733) );
  nand2_2 U243 ( .ip1(buf1[16]), .ip2(n94), .op(n1734) );
  nand4_2 U244 ( .ip1(rst), .ip2(n1731), .ip3(n1730), .ip4(n1729), .op(n1258)
         );
  nand2_2 U245 ( .ip1(idin[15]), .ip2(n97), .op(n1729) );
  nand2_2 U246 ( .ip1(din[15]), .ip2(n53), .op(n1730) );
  nand2_2 U247 ( .ip1(buf1[15]), .ip2(n94), .op(n1731) );
  nand4_2 U248 ( .ip1(rst), .ip2(n1728), .ip3(n1727), .ip4(n1726), .op(n1257)
         );
  nand2_2 U249 ( .ip1(idin[14]), .ip2(n97), .op(n1726) );
  nand2_2 U250 ( .ip1(din[14]), .ip2(n53), .op(n1727) );
  nand2_2 U251 ( .ip1(buf1[14]), .ip2(n94), .op(n1728) );
  nand4_2 U252 ( .ip1(rst), .ip2(n1725), .ip3(n1724), .ip4(n1723), .op(n1256)
         );
  nand2_2 U253 ( .ip1(idin[13]), .ip2(n97), .op(n1723) );
  nand2_2 U254 ( .ip1(din[13]), .ip2(n53), .op(n1724) );
  nand2_2 U255 ( .ip1(buf1[13]), .ip2(n94), .op(n1725) );
  nand4_2 U256 ( .ip1(rst), .ip2(n1722), .ip3(n1721), .ip4(n1720), .op(n1255)
         );
  nand2_2 U257 ( .ip1(idin[12]), .ip2(n97), .op(n1720) );
  nand2_2 U258 ( .ip1(din[12]), .ip2(n53), .op(n1721) );
  nand2_2 U259 ( .ip1(buf1[12]), .ip2(n94), .op(n1722) );
  nand4_2 U260 ( .ip1(rst), .ip2(n1719), .ip3(n1718), .ip4(n1717), .op(n1254)
         );
  nand2_2 U261 ( .ip1(idin[11]), .ip2(n97), .op(n1717) );
  nand2_2 U262 ( .ip1(din[11]), .ip2(n53), .op(n1718) );
  nand2_2 U263 ( .ip1(buf1[11]), .ip2(n93), .op(n1719) );
  nand4_2 U264 ( .ip1(rst), .ip2(n1716), .ip3(n1715), .ip4(n1714), .op(n1253)
         );
  nand2_2 U265 ( .ip1(idin[10]), .ip2(n97), .op(n1714) );
  nand2_2 U266 ( .ip1(din[10]), .ip2(n53), .op(n1715) );
  nand2_2 U267 ( .ip1(buf1[10]), .ip2(n93), .op(n1716) );
  nand4_2 U268 ( .ip1(rst), .ip2(n1713), .ip3(n1712), .ip4(n1711), .op(n1252)
         );
  nand2_2 U269 ( .ip1(idin[9]), .ip2(n97), .op(n1711) );
  nand2_2 U270 ( .ip1(din[9]), .ip2(n53), .op(n1712) );
  nand2_2 U271 ( .ip1(buf1[9]), .ip2(n93), .op(n1713) );
  nand4_2 U272 ( .ip1(rst), .ip2(n1710), .ip3(n1709), .ip4(n1708), .op(n1251)
         );
  nand2_2 U273 ( .ip1(idin[8]), .ip2(n97), .op(n1708) );
  nand2_2 U274 ( .ip1(din[8]), .ip2(n53), .op(n1709) );
  nand2_2 U275 ( .ip1(buf1[8]), .ip2(n93), .op(n1710) );
  nand4_2 U276 ( .ip1(rst), .ip2(n1707), .ip3(n1706), .ip4(n1705), .op(n1250)
         );
  nand2_2 U277 ( .ip1(idin[7]), .ip2(n98), .op(n1705) );
  nand2_2 U278 ( .ip1(din[7]), .ip2(n54), .op(n1706) );
  nand2_2 U279 ( .ip1(buf1[7]), .ip2(n93), .op(n1707) );
  nand4_2 U280 ( .ip1(rst), .ip2(n1704), .ip3(n1703), .ip4(n1702), .op(n1249)
         );
  nand2_2 U281 ( .ip1(idin[6]), .ip2(n98), .op(n1702) );
  nand2_2 U282 ( .ip1(din[6]), .ip2(n54), .op(n1703) );
  nand2_2 U283 ( .ip1(buf1[6]), .ip2(n93), .op(n1704) );
  nand4_2 U284 ( .ip1(rst), .ip2(n1701), .ip3(n1700), .ip4(n1699), .op(n1248)
         );
  nand2_2 U285 ( .ip1(idin[5]), .ip2(n98), .op(n1699) );
  nand2_2 U286 ( .ip1(din[5]), .ip2(n54), .op(n1700) );
  nand2_2 U287 ( .ip1(buf1[5]), .ip2(n93), .op(n1701) );
  nand4_2 U288 ( .ip1(rst), .ip2(n1698), .ip3(n1697), .ip4(n1696), .op(n1247)
         );
  nand2_2 U289 ( .ip1(idin[4]), .ip2(n98), .op(n1696) );
  nand2_2 U290 ( .ip1(din[4]), .ip2(n54), .op(n1697) );
  nand2_2 U291 ( .ip1(buf1[4]), .ip2(n93), .op(n1698) );
  nand4_2 U292 ( .ip1(rst), .ip2(n1695), .ip3(n1694), .ip4(n1693), .op(n1246)
         );
  nand2_2 U293 ( .ip1(n98), .ip2(idin[3]), .op(n1693) );
  nand2_2 U294 ( .ip1(din[3]), .ip2(n54), .op(n1694) );
  nand2_2 U295 ( .ip1(buf1[3]), .ip2(n93), .op(n1695) );
  nand4_2 U296 ( .ip1(rst), .ip2(n1692), .ip3(n1691), .ip4(n1690), .op(n1245)
         );
  nand2_2 U297 ( .ip1(n98), .ip2(idin[2]), .op(n1690) );
  nand2_2 U298 ( .ip1(din[2]), .ip2(n54), .op(n1691) );
  nand2_2 U299 ( .ip1(buf1[2]), .ip2(n93), .op(n1692) );
  nand4_2 U300 ( .ip1(rst), .ip2(n1689), .ip3(n1688), .ip4(n1687), .op(n1244)
         );
  nand2_2 U301 ( .ip1(n98), .ip2(idin[1]), .op(n1687) );
  nand2_2 U302 ( .ip1(din[1]), .ip2(n54), .op(n1688) );
  nand2_2 U303 ( .ip1(buf1[1]), .ip2(n93), .op(n1689) );
  nand4_2 U304 ( .ip1(rst), .ip2(n1686), .ip3(n1685), .ip4(n1684), .op(n1243)
         );
  nand2_2 U305 ( .ip1(n98), .ip2(idin[0]), .op(n1684) );
  nand2_2 U306 ( .ip1(din[0]), .ip2(n54), .op(n1685) );
  nand2_2 U307 ( .ip1(buf1[0]), .ip2(n93), .op(n1686) );
  nor2_2 U308 ( .ip1(n54), .ip2(n98), .op(n1777) );
  and3_2 U309 ( .ip1(n1683), .ip2(n1682), .ip3(ep_match_r), .op(n1778) );
  or2_2 U310 ( .ip1(buf1_set), .ip2(out_to_small), .op(n1683) );
  nand2_2 U311 ( .ip1(we), .ip2(n90), .op(n1682) );
  nand3_2 U312 ( .ip1(rst), .ip2(n1680), .ip3(n1679), .op(n1242) );
  nor2_2 U313 ( .ip1(n89), .ip2(n1071), .op(n1677) );
  nor2_2 U314 ( .ip1(n1123), .ip2(n86), .op(n1678) );
  nand2_2 U315 ( .ip1(buf0[31]), .ip2(n83), .op(n1680) );
  nand3_2 U316 ( .ip1(rst), .ip2(n1673), .ip3(n1672), .op(n1241) );
  nor2_2 U317 ( .ip1(n89), .ip2(n985), .op(n1670) );
  nor2_2 U318 ( .ip1(n1105), .ip2(n86), .op(n1671) );
  nand2_2 U319 ( .ip1(buf0[30]), .ip2(n83), .op(n1673) );
  nand3_2 U320 ( .ip1(rst), .ip2(n1669), .ip3(n1668), .op(n1240) );
  nor2_2 U321 ( .ip1(n89), .ip2(n1066), .op(n1666) );
  nor2_2 U322 ( .ip1(n1106), .ip2(n86), .op(n1667) );
  nand2_2 U323 ( .ip1(buf0[29]), .ip2(n83), .op(n1669) );
  nand3_2 U324 ( .ip1(rst), .ip2(n1665), .ip3(n1664), .op(n1239) );
  nor2_2 U325 ( .ip1(n89), .ip2(n986), .op(n1662) );
  nor2_2 U326 ( .ip1(n1107), .ip2(n86), .op(n1663) );
  nand2_2 U327 ( .ip1(buf0[28]), .ip2(n83), .op(n1665) );
  nand3_2 U328 ( .ip1(rst), .ip2(n1661), .ip3(n1660), .op(n1238) );
  nor2_2 U329 ( .ip1(n89), .ip2(n992), .op(n1658) );
  nor2_2 U330 ( .ip1(n1126), .ip2(n86), .op(n1659) );
  nand2_2 U331 ( .ip1(buf0[27]), .ip2(n83), .op(n1661) );
  nand3_2 U332 ( .ip1(rst), .ip2(n1657), .ip3(n1656), .op(n1237) );
  nor2_2 U333 ( .ip1(n88), .ip2(n987), .op(n1654) );
  nor2_2 U334 ( .ip1(n1127), .ip2(n86), .op(n1655) );
  nand2_2 U335 ( .ip1(buf0[26]), .ip2(n83), .op(n1657) );
  nand3_2 U336 ( .ip1(rst), .ip2(n1653), .ip3(n1652), .op(n1236) );
  nor2_2 U337 ( .ip1(n88), .ip2(n991), .op(n1650) );
  nor2_2 U338 ( .ip1(n1128), .ip2(n86), .op(n1651) );
  nand2_2 U339 ( .ip1(buf0[25]), .ip2(n83), .op(n1653) );
  nand3_2 U340 ( .ip1(rst), .ip2(n1649), .ip3(n1648), .op(n1235) );
  nor2_2 U341 ( .ip1(n88), .ip2(n988), .op(n1646) );
  nor2_2 U342 ( .ip1(n1129), .ip2(n85), .op(n1647) );
  nand2_2 U343 ( .ip1(buf0[24]), .ip2(n83), .op(n1649) );
  nand3_2 U344 ( .ip1(rst), .ip2(n1645), .ip3(n1644), .op(n1234) );
  nor2_2 U345 ( .ip1(n88), .ip2(n990), .op(n1642) );
  nor2_2 U346 ( .ip1(n1130), .ip2(n85), .op(n1643) );
  nand2_2 U347 ( .ip1(buf0[23]), .ip2(n82), .op(n1645) );
  nand3_2 U348 ( .ip1(rst), .ip2(n1641), .ip3(n1640), .op(n1233) );
  nor2_2 U349 ( .ip1(n88), .ip2(n1072), .op(n1638) );
  nor2_2 U350 ( .ip1(n1131), .ip2(n85), .op(n1639) );
  nand2_2 U351 ( .ip1(buf0[22]), .ip2(n82), .op(n1641) );
  nand3_2 U352 ( .ip1(rst), .ip2(n1637), .ip3(n1636), .op(n1232) );
  nor2_2 U353 ( .ip1(n88), .ip2(n1065), .op(n1634) );
  nor2_2 U354 ( .ip1(n1132), .ip2(n85), .op(n1635) );
  nand2_2 U355 ( .ip1(buf0[21]), .ip2(n82), .op(n1637) );
  nand3_2 U356 ( .ip1(rst), .ip2(n1633), .ip3(n1632), .op(n1231) );
  nor2_2 U357 ( .ip1(n88), .ip2(n1073), .op(n1630) );
  nor2_2 U358 ( .ip1(n1133), .ip2(n85), .op(n1631) );
  nand2_2 U359 ( .ip1(buf0[20]), .ip2(n82), .op(n1633) );
  nand3_2 U360 ( .ip1(rst), .ip2(n1629), .ip3(n1628), .op(n1230) );
  nor2_2 U361 ( .ip1(n88), .ip2(n989), .op(n1626) );
  nor2_2 U362 ( .ip1(n1134), .ip2(n85), .op(n1627) );
  nand2_2 U363 ( .ip1(buf0[19]), .ip2(n82), .op(n1629) );
  nand3_2 U364 ( .ip1(rst), .ip2(n1625), .ip3(n1624), .op(n1229) );
  nor2_2 U365 ( .ip1(n88), .ip2(n1074), .op(n1622) );
  nor2_2 U366 ( .ip1(n1135), .ip2(n85), .op(n1623) );
  nand2_2 U367 ( .ip1(buf0[18]), .ip2(n82), .op(n1625) );
  nand3_2 U368 ( .ip1(rst), .ip2(n1621), .ip3(n1620), .op(n1228) );
  nor2_2 U369 ( .ip1(n88), .ip2(n1075), .op(n1618) );
  nor2_2 U370 ( .ip1(n1136), .ip2(n85), .op(n1619) );
  nand2_2 U371 ( .ip1(buf0[17]), .ip2(n82), .op(n1621) );
  nand3_2 U372 ( .ip1(rst), .ip2(n1617), .ip3(n1616), .op(n1227) );
  nor2_2 U373 ( .ip1(n88), .ip2(n1076), .op(n1614) );
  nor2_2 U374 ( .ip1(n1110), .ip2(n85), .op(n1615) );
  nand2_2 U375 ( .ip1(buf0[16]), .ip2(n82), .op(n1617) );
  nand3_2 U376 ( .ip1(rst), .ip2(n1613), .ip3(n1612), .op(n1226) );
  nor2_2 U377 ( .ip1(n88), .ip2(n1077), .op(n1610) );
  nor2_2 U378 ( .ip1(n1111), .ip2(n85), .op(n1611) );
  nand2_2 U379 ( .ip1(buf0[15]), .ip2(n82), .op(n1613) );
  nand3_2 U380 ( .ip1(rst), .ip2(n1609), .ip3(n1608), .op(n1225) );
  nor2_2 U381 ( .ip1(n88), .ip2(n1078), .op(n1606) );
  nor2_2 U382 ( .ip1(n1112), .ip2(n85), .op(n1607) );
  nand2_2 U383 ( .ip1(buf0[14]), .ip2(n82), .op(n1609) );
  nand3_2 U384 ( .ip1(rst), .ip2(n1605), .ip3(n1604), .op(n1224) );
  nor2_2 U385 ( .ip1(n88), .ip2(n1079), .op(n1602) );
  nor2_2 U386 ( .ip1(n1113), .ip2(n85), .op(n1603) );
  nand2_2 U387 ( .ip1(buf0[13]), .ip2(n82), .op(n1605) );
  nand3_2 U388 ( .ip1(rst), .ip2(n1601), .ip3(n1600), .op(n1223) );
  nor2_2 U389 ( .ip1(n87), .ip2(n1080), .op(n1598) );
  nor2_2 U390 ( .ip1(n1114), .ip2(n85), .op(n1599) );
  nand2_2 U391 ( .ip1(buf0[12]), .ip2(n82), .op(n1601) );
  nand3_2 U392 ( .ip1(rst), .ip2(n1597), .ip3(n1596), .op(n1222) );
  nor2_2 U393 ( .ip1(n87), .ip2(n1081), .op(n1594) );
  nor2_2 U394 ( .ip1(n1115), .ip2(n84), .op(n1595) );
  nand2_2 U395 ( .ip1(buf0[11]), .ip2(n81), .op(n1597) );
  nand3_2 U396 ( .ip1(rst), .ip2(n1593), .ip3(n1592), .op(n1221) );
  nor2_2 U397 ( .ip1(n87), .ip2(n1082), .op(n1590) );
  nor2_2 U398 ( .ip1(n1116), .ip2(n84), .op(n1591) );
  nand2_2 U399 ( .ip1(buf0[10]), .ip2(n81), .op(n1593) );
  nand3_2 U400 ( .ip1(rst), .ip2(n1589), .ip3(n1588), .op(n1220) );
  nor2_2 U401 ( .ip1(n87), .ip2(n1083), .op(n1586) );
  nor2_2 U402 ( .ip1(n1117), .ip2(n84), .op(n1587) );
  nand2_2 U403 ( .ip1(buf0[9]), .ip2(n81), .op(n1589) );
  nand3_2 U404 ( .ip1(rst), .ip2(n1585), .ip3(n1584), .op(n1219) );
  nor2_2 U405 ( .ip1(n87), .ip2(n1084), .op(n1582) );
  nor2_2 U406 ( .ip1(n1118), .ip2(n84), .op(n1583) );
  nand2_2 U407 ( .ip1(buf0[8]), .ip2(n81), .op(n1585) );
  nand3_2 U408 ( .ip1(rst), .ip2(n1581), .ip3(n1580), .op(n1218) );
  nor2_2 U409 ( .ip1(n87), .ip2(n1085), .op(n1578) );
  nor2_2 U410 ( .ip1(n1119), .ip2(n84), .op(n1579) );
  nand2_2 U411 ( .ip1(buf0[7]), .ip2(n81), .op(n1581) );
  nand3_2 U412 ( .ip1(rst), .ip2(n1577), .ip3(n1576), .op(n1217) );
  nor2_2 U413 ( .ip1(n87), .ip2(n1086), .op(n1574) );
  nor2_2 U414 ( .ip1(n1120), .ip2(n84), .op(n1575) );
  nand2_2 U415 ( .ip1(buf0[6]), .ip2(n81), .op(n1577) );
  nand3_2 U416 ( .ip1(rst), .ip2(n1573), .ip3(n1572), .op(n1216) );
  nor2_2 U417 ( .ip1(n87), .ip2(n1087), .op(n1570) );
  nor2_2 U418 ( .ip1(n1121), .ip2(n84), .op(n1571) );
  nand2_2 U419 ( .ip1(buf0[5]), .ip2(n81), .op(n1573) );
  nand3_2 U420 ( .ip1(rst), .ip2(n1569), .ip3(n1568), .op(n1215) );
  nor2_2 U421 ( .ip1(n87), .ip2(n1088), .op(n1566) );
  nor2_2 U422 ( .ip1(n1122), .ip2(n84), .op(n1567) );
  nand2_2 U423 ( .ip1(buf0[4]), .ip2(n81), .op(n1569) );
  nand3_2 U424 ( .ip1(rst), .ip2(n1565), .ip3(n1564), .op(n1214) );
  nor2_2 U425 ( .ip1(n87), .ip2(n1089), .op(n1562) );
  nor2_2 U426 ( .ip1(n1108), .ip2(n84), .op(n1563) );
  nand2_2 U427 ( .ip1(buf0[3]), .ip2(n81), .op(n1565) );
  nand3_2 U428 ( .ip1(rst), .ip2(n1561), .ip3(n1560), .op(n1213) );
  nor2_2 U429 ( .ip1(n87), .ip2(n1090), .op(n1558) );
  nor2_2 U430 ( .ip1(n1109), .ip2(n84), .op(n1559) );
  nand2_2 U431 ( .ip1(buf0[2]), .ip2(n81), .op(n1561) );
  nand3_2 U432 ( .ip1(rst), .ip2(n1557), .ip3(n1556), .op(n1212) );
  nor2_2 U433 ( .ip1(n87), .ip2(n1091), .op(n1554) );
  nor2_2 U434 ( .ip1(n1124), .ip2(n84), .op(n1555) );
  nand2_2 U435 ( .ip1(buf0[1]), .ip2(n81), .op(n1557) );
  nand3_2 U436 ( .ip1(rst), .ip2(n1553), .ip3(n1552), .op(n1211) );
  nor2_2 U437 ( .ip1(n87), .ip2(n1092), .op(n1550) );
  nor2_2 U438 ( .ip1(n1125), .ip2(n84), .op(n1551) );
  nand2_2 U439 ( .ip1(buf0[0]), .ip2(n81), .op(n1553) );
  and3_2 U440 ( .ip1(n87), .ip2(n66), .ip3(n84), .op(n1674) );
  nand4_2 U441 ( .ip1(buf0_set), .ip2(n1821), .ip3(n66), .ip4(n1103), .op(
        n1675) );
  nand3_2 U442 ( .ip1(n1821), .ip2(n67), .ip3(buf0_rl), .op(n1676) );
  nor2_2 U443 ( .ip1(n1098), .ip2(n1889), .op(n1821) );
  and2_2 U444 ( .ip1(din[0]), .ip2(n80), .op(n1548) );
  and2_2 U445 ( .ip1(din[1]), .ip2(n80), .op(n1547) );
  and2_2 U446 ( .ip1(din[2]), .ip2(n80), .op(n1546) );
  and2_2 U447 ( .ip1(din[3]), .ip2(n80), .op(n1545) );
  and2_2 U448 ( .ip1(din[4]), .ip2(n80), .op(n1544) );
  and2_2 U449 ( .ip1(din[5]), .ip2(n80), .op(n1543) );
  and2_2 U450 ( .ip1(din[6]), .ip2(n79), .op(n1542) );
  and2_2 U451 ( .ip1(din[7]), .ip2(n79), .op(n1541) );
  and2_2 U452 ( .ip1(din[8]), .ip2(n79), .op(n1540) );
  and2_2 U453 ( .ip1(din[9]), .ip2(n79), .op(n1539) );
  and2_2 U454 ( .ip1(din[10]), .ip2(n79), .op(n1538) );
  and2_2 U455 ( .ip1(din[11]), .ip2(n79), .op(n1537) );
  and2_2 U456 ( .ip1(din[12]), .ip2(n79), .op(n1536) );
  and2_2 U457 ( .ip1(din[13]), .ip2(n79), .op(n1535) );
  and2_2 U458 ( .ip1(din[14]), .ip2(n79), .op(n1534) );
  and2_2 U459 ( .ip1(din[15]), .ip2(n79), .op(n1533) );
  and2_2 U460 ( .ip1(din[16]), .ip2(n79), .op(n1532) );
  and2_2 U461 ( .ip1(din[17]), .ip2(n79), .op(n1531) );
  and2_2 U462 ( .ip1(din[18]), .ip2(n79), .op(n1530) );
  and2_2 U463 ( .ip1(din[19]), .ip2(n78), .op(n1529) );
  and2_2 U464 ( .ip1(din[20]), .ip2(n78), .op(n1528) );
  and2_2 U465 ( .ip1(din[21]), .ip2(n78), .op(n1527) );
  and2_2 U466 ( .ip1(din[22]), .ip2(n78), .op(n1526) );
  and2_2 U467 ( .ip1(din[23]), .ip2(n78), .op(n1525) );
  and2_2 U468 ( .ip1(din[24]), .ip2(n78), .op(n1524) );
  and2_2 U469 ( .ip1(din[25]), .ip2(n78), .op(n1523) );
  and2_2 U470 ( .ip1(din[26]), .ip2(n78), .op(n1522) );
  and2_2 U471 ( .ip1(din[27]), .ip2(n78), .op(n1521) );
  and2_2 U472 ( .ip1(din[28]), .ip2(n78), .op(n1520) );
  and2_2 U473 ( .ip1(din[29]), .ip2(n78), .op(n1519) );
  and2_2 U474 ( .ip1(din[30]), .ip2(n78), .op(n1518) );
  and2_2 U475 ( .ip1(din[31]), .ip2(n78), .op(n1517) );
  nand2_2 U477 ( .ip1(n1515), .ip2(n1514), .op(n1178) );
  nand2_2 U478 ( .ip1(n1513), .ip2(din[16]), .op(n1514) );
  nand2_2 U479 ( .ip1(int__16), .ip2(n1512), .op(n1515) );
  nand2_2 U480 ( .ip1(n1511), .ip2(n1510), .op(n1177) );
  nand2_2 U481 ( .ip1(n1513), .ip2(din[17]), .op(n1510) );
  nand2_2 U482 ( .ip1(int__17), .ip2(n1512), .op(n1511) );
  nand2_2 U483 ( .ip1(n1509), .ip2(n1508), .op(n1176) );
  nand2_2 U484 ( .ip1(n1513), .ip2(din[18]), .op(n1508) );
  nand2_2 U485 ( .ip1(int__18), .ip2(n1512), .op(n1509) );
  nand2_2 U486 ( .ip1(n1507), .ip2(n1506), .op(n1175) );
  nand2_2 U487 ( .ip1(n1513), .ip2(din[19]), .op(n1506) );
  nand2_2 U488 ( .ip1(int__19), .ip2(n1512), .op(n1507) );
  nand2_2 U489 ( .ip1(n1505), .ip2(n1504), .op(n1174) );
  nand2_2 U490 ( .ip1(n1513), .ip2(din[20]), .op(n1504) );
  nand2_2 U491 ( .ip1(int__20), .ip2(n1512), .op(n1505) );
  nand2_2 U492 ( .ip1(n1503), .ip2(n1502), .op(n1173) );
  nand2_2 U493 ( .ip1(n1513), .ip2(din[21]), .op(n1502) );
  nand2_2 U494 ( .ip1(int__21), .ip2(n1512), .op(n1503) );
  nand2_2 U495 ( .ip1(n1501), .ip2(n1500), .op(n1172) );
  nand2_2 U496 ( .ip1(n1513), .ip2(din[24]), .op(n1500) );
  nand2_2 U497 ( .ip1(int__24), .ip2(n1512), .op(n1501) );
  nand2_2 U498 ( .ip1(n1499), .ip2(n1498), .op(n1171) );
  nand2_2 U499 ( .ip1(n1513), .ip2(din[25]), .op(n1498) );
  nand2_2 U500 ( .ip1(int__25), .ip2(n1512), .op(n1499) );
  nand2_2 U501 ( .ip1(n1497), .ip2(n1496), .op(n1170) );
  nand2_2 U502 ( .ip1(n1513), .ip2(din[26]), .op(n1496) );
  nand2_2 U503 ( .ip1(int__26), .ip2(n1512), .op(n1497) );
  nand2_2 U504 ( .ip1(n1495), .ip2(n1494), .op(n1169) );
  nand2_2 U505 ( .ip1(n1513), .ip2(din[27]), .op(n1494) );
  nand2_2 U506 ( .ip1(int__27), .ip2(n1512), .op(n1495) );
  nand2_2 U507 ( .ip1(n1493), .ip2(n1492), .op(n1168) );
  nand2_2 U508 ( .ip1(n1513), .ip2(din[28]), .op(n1492) );
  nand2_2 U509 ( .ip1(int__28), .ip2(n1512), .op(n1493) );
  nand2_2 U510 ( .ip1(n1491), .ip2(n1490), .op(n1167) );
  nand2_2 U511 ( .ip1(n1513), .ip2(din[29]), .op(n1490) );
  nand2_2 U512 ( .ip1(int__29), .ip2(n1512), .op(n1491) );
  nand2_2 U516 ( .ip1(n61), .ip2(din[15]), .op(n1489) );
  nand2_2 U518 ( .ip1(n1486), .ip2(n1485), .op(n1165) );
  nand2_2 U519 ( .ip1(n60), .ip2(din[16]), .op(n1485) );
  nand2_2 U520 ( .ip1(csr[16]), .ip2(n58), .op(n1486) );
  nand2_2 U521 ( .ip1(n1484), .ip2(n1483), .op(n1164) );
  nand2_2 U522 ( .ip1(n60), .ip2(din[17]), .op(n1483) );
  nand2_2 U523 ( .ip1(csr[17]), .ip2(n58), .op(n1484) );
  nand2_2 U524 ( .ip1(n1482), .ip2(n1481), .op(n1163) );
  nand2_2 U525 ( .ip1(n60), .ip2(din[18]), .op(n1481) );
  nand2_2 U526 ( .ip1(csr[18]), .ip2(n58), .op(n1482) );
  nand2_2 U527 ( .ip1(n1480), .ip2(n1479), .op(n1162) );
  nand2_2 U528 ( .ip1(n60), .ip2(din[19]), .op(n1479) );
  nand2_2 U529 ( .ip1(csr[19]), .ip2(n58), .op(n1480) );
  nand2_2 U530 ( .ip1(n1478), .ip2(n1477), .op(n1161) );
  nand2_2 U531 ( .ip1(n60), .ip2(din[20]), .op(n1477) );
  nand2_2 U532 ( .ip1(csr[20]), .ip2(n58), .op(n1478) );
  nand2_2 U533 ( .ip1(n1476), .ip2(n1475), .op(n1160) );
  nand2_2 U534 ( .ip1(n60), .ip2(din[21]), .op(n1475) );
  nand2_2 U535 ( .ip1(csr[21]), .ip2(n58), .op(n1476) );
  and2_2 U536 ( .ip1(din[22]), .ip2(n60), .op(n1473) );
  nand2_2 U537 ( .ip1(n1471), .ip2(n1470), .op(n1158) );
  nand2_2 U538 ( .ip1(n60), .ip2(din[23]), .op(n1470) );
  nand2_2 U539 ( .ip1(csr[23]), .ip2(n1474), .op(n1471) );
  and2_2 U540 ( .ip1(n59), .ip2(n1472), .op(n1474) );
  nand4_2 U541 ( .ip1(csr[13]), .ip2(out_to_small), .ip3(rst), .ip4(n1469), 
        .op(n1472) );
  nand2_2 U542 ( .ip1(n1468), .ip2(n1467), .op(n1157) );
  nand2_2 U543 ( .ip1(n60), .ip2(din[24]), .op(n1467) );
  nand2_2 U544 ( .ip1(csr[24]), .ip2(n58), .op(n1468) );
  nand2_2 U545 ( .ip1(n1466), .ip2(n1465), .op(n1156) );
  nand2_2 U546 ( .ip1(n60), .ip2(din[25]), .op(n1465) );
  nand2_2 U547 ( .ip1(csr[25]), .ip2(n58), .op(n1466) );
  nand2_2 U548 ( .ip1(n1464), .ip2(n1463), .op(n1155) );
  nand2_2 U549 ( .ip1(n60), .ip2(din[26]), .op(n1463) );
  nand2_2 U550 ( .ip1(csr[26]), .ip2(n58), .op(n1464) );
  nand2_2 U551 ( .ip1(n1462), .ip2(n1461), .op(n1154) );
  nand2_2 U552 ( .ip1(n61), .ip2(din[27]), .op(n1461) );
  nand2_2 U553 ( .ip1(n59), .ip2(csr[27]), .op(n1462) );
  nand2_2 U554 ( .ip1(n1460), .ip2(n1459), .op(n1153) );
  nand2_2 U555 ( .ip1(n61), .ip2(din[0]), .op(n1459) );
  nand2_2 U556 ( .ip1(csr[0]), .ip2(n58), .op(n1460) );
  nand2_2 U557 ( .ip1(n1458), .ip2(n1457), .op(n1152) );
  nand2_2 U558 ( .ip1(n61), .ip2(din[1]), .op(n1457) );
  nand2_2 U559 ( .ip1(csr[1]), .ip2(n58), .op(n1458) );
  nand2_2 U560 ( .ip1(n1456), .ip2(n1455), .op(n1151) );
  nand2_2 U561 ( .ip1(n61), .ip2(din[2]), .op(n1455) );
  nand2_2 U562 ( .ip1(csr[2]), .ip2(n58), .op(n1456) );
  nand2_2 U563 ( .ip1(n1454), .ip2(n1453), .op(n1150) );
  nand2_2 U564 ( .ip1(n61), .ip2(din[3]), .op(n1453) );
  nand2_2 U565 ( .ip1(csr[3]), .ip2(n59), .op(n1454) );
  nand2_2 U566 ( .ip1(n1452), .ip2(n1451), .op(n1149) );
  nand2_2 U567 ( .ip1(n61), .ip2(din[4]), .op(n1451) );
  nand2_2 U568 ( .ip1(csr[4]), .ip2(n59), .op(n1452) );
  nand2_2 U569 ( .ip1(n1450), .ip2(n1449), .op(n1148) );
  nand2_2 U570 ( .ip1(n61), .ip2(din[5]), .op(n1449) );
  nand2_2 U571 ( .ip1(csr[5]), .ip2(n59), .op(n1450) );
  nand2_2 U572 ( .ip1(n1448), .ip2(n1447), .op(n1147) );
  nand2_2 U573 ( .ip1(n61), .ip2(din[6]), .op(n1447) );
  nand2_2 U574 ( .ip1(csr[6]), .ip2(n59), .op(n1448) );
  nand2_2 U575 ( .ip1(n1446), .ip2(n1445), .op(n1146) );
  nand2_2 U576 ( .ip1(n61), .ip2(din[7]), .op(n1445) );
  nand2_2 U577 ( .ip1(csr[7]), .ip2(n59), .op(n1446) );
  nand2_2 U578 ( .ip1(n1444), .ip2(n1443), .op(n1145) );
  nand2_2 U579 ( .ip1(n61), .ip2(din[8]), .op(n1443) );
  nand2_2 U580 ( .ip1(csr[8]), .ip2(n59), .op(n1444) );
  nand2_2 U581 ( .ip1(n1442), .ip2(n1441), .op(n1144) );
  nand2_2 U582 ( .ip1(n61), .ip2(din[9]), .op(n1441) );
  nand2_2 U583 ( .ip1(csr[9]), .ip2(n59), .op(n1442) );
  nand2_2 U584 ( .ip1(n1440), .ip2(n1439), .op(n1143) );
  nand2_2 U585 ( .ip1(n61), .ip2(din[10]), .op(n1439) );
  nand2_2 U586 ( .ip1(csr[10]), .ip2(n59), .op(n1440) );
  nand2_2 U587 ( .ip1(n1438), .ip2(n1437), .op(n1142) );
  nand2_2 U588 ( .ip1(n62), .ip2(din[11]), .op(n1437) );
  nand2_2 U589 ( .ip1(csr[11]), .ip2(n59), .op(n1438) );
  nand2_2 U590 ( .ip1(n1436), .ip2(n1435), .op(n1141) );
  nand2_2 U591 ( .ip1(n62), .ip2(din[12]), .op(n1435) );
  nand2_2 U592 ( .ip1(csr[12]), .ip2(n59), .op(n1436) );
  nand2_2 U593 ( .ip1(n1434), .ip2(n1433), .op(n1140) );
  nand2_2 U594 ( .ip1(n60), .ip2(din[13]), .op(n1433) );
  nand2_2 U595 ( .ip1(csr[13]), .ip2(n59), .op(n1434) );
  nor2_2 U596 ( .ip1(n1889), .ip2(n60), .op(n1487) );
  nor2_2 U597 ( .ip1(n1469), .ip2(n1889), .op(n1488) );
  nand2_2 U598 ( .ip1(n56), .ip2(we), .op(n1469) );
  nor4_2 U599 ( .ip1(n1431), .ip2(n1430), .ip3(n1429), .ip4(n1428), .op(
        ep_match) );
  xor2_2 U600 ( .ip1(ep_sel[1]), .ip2(csr[19]), .op(n1428) );
  xor2_2 U601 ( .ip1(ep_sel[2]), .ip2(csr[20]), .op(n1429) );
  xor2_2 U602 ( .ip1(ep_sel[3]), .ip2(csr[21]), .op(n1430) );
  xor2_2 U603 ( .ip1(ep_sel[0]), .ip2(csr[18]), .op(n1431) );
  and2_2 U604 ( .ip1(n65), .ip2(buf0[9]), .op(n1426) );
  and2_2 U605 ( .ip1(n92), .ip2(buf1[9]), .op(n1427) );
  and2_2 U606 ( .ip1(n65), .ip2(buf0[8]), .op(n1424) );
  and2_2 U607 ( .ip1(n92), .ip2(buf1[8]), .op(n1425) );
  and2_2 U608 ( .ip1(n65), .ip2(buf0[7]), .op(n1422) );
  and2_2 U609 ( .ip1(n92), .ip2(buf1[7]), .op(n1423) );
  nand4_2 U610 ( .ip1(n1421), .ip2(n1420), .ip3(n1419), .ip4(n1418), .op(
        dout[6]) );
  nand2_2 U611 ( .ip1(buf1[6]), .ip2(n91), .op(n1418) );
  nand2_2 U612 ( .ip1(buf0[6]), .ip2(n63), .op(n1419) );
  nand2_2 U613 ( .ip1(n51), .ip2(int_[6]), .op(n1420) );
  nand2_2 U614 ( .ip1(csr[6]), .ip2(n55), .op(n1421) );
  nand4_2 U615 ( .ip1(n1417), .ip2(n1416), .ip3(n1415), .ip4(n1414), .op(
        dout[5]) );
  nand2_2 U616 ( .ip1(buf1[5]), .ip2(n90), .op(n1414) );
  nand2_2 U617 ( .ip1(buf0[5]), .ip2(n63), .op(n1415) );
  nand2_2 U618 ( .ip1(n51), .ip2(int_[5]), .op(n1416) );
  nand2_2 U619 ( .ip1(csr[5]), .ip2(n56), .op(n1417) );
  nand4_2 U620 ( .ip1(n1413), .ip2(n1412), .ip3(n1411), .ip4(n1410), .op(
        dout[4]) );
  nand2_2 U621 ( .ip1(buf1[4]), .ip2(n90), .op(n1410) );
  nand2_2 U622 ( .ip1(buf0[4]), .ip2(n63), .op(n1411) );
  nand2_2 U623 ( .ip1(n50), .ip2(int_[4]), .op(n1412) );
  nand2_2 U624 ( .ip1(csr[4]), .ip2(n56), .op(n1413) );
  nand4_2 U625 ( .ip1(n1409), .ip2(n1408), .ip3(n1407), .ip4(n1406), .op(
        dout[3]) );
  nand2_2 U626 ( .ip1(buf1[3]), .ip2(n90), .op(n1406) );
  nand2_2 U627 ( .ip1(buf0[3]), .ip2(n63), .op(n1407) );
  nand2_2 U628 ( .ip1(n49), .ip2(int_[3]), .op(n1408) );
  nand2_2 U629 ( .ip1(csr[3]), .ip2(n56), .op(n1409) );
  and2_2 U630 ( .ip1(n64), .ip2(buf0[31]), .op(n1404) );
  and2_2 U631 ( .ip1(n92), .ip2(buf1[31]), .op(n1405) );
  and2_2 U632 ( .ip1(n64), .ip2(buf0[30]), .op(n1402) );
  and2_2 U633 ( .ip1(n91), .ip2(buf1[30]), .op(n1403) );
  nand4_2 U634 ( .ip1(n1401), .ip2(n1400), .ip3(n1399), .ip4(n1398), .op(
        dout[2]) );
  nand2_2 U635 ( .ip1(buf1[2]), .ip2(n90), .op(n1398) );
  nand2_2 U636 ( .ip1(buf0[2]), .ip2(n63), .op(n1399) );
  nand2_2 U637 ( .ip1(n48), .ip2(int_[2]), .op(n1400) );
  nand2_2 U638 ( .ip1(csr[2]), .ip2(n56), .op(n1401) );
  nand4_2 U639 ( .ip1(n1397), .ip2(n1396), .ip3(n1395), .ip4(n1394), .op(
        dout[29]) );
  nand2_2 U640 ( .ip1(buf1[29]), .ip2(n90), .op(n1394) );
  nand2_2 U641 ( .ip1(buf0[29]), .ip2(n63), .op(n1395) );
  nand2_2 U642 ( .ip1(int__29), .ip2(n41), .op(n1396) );
  nand2_2 U643 ( .ip1(n56), .ip2(csr[29]), .op(n1397) );
  nand4_2 U644 ( .ip1(n1393), .ip2(n1392), .ip3(n1391), .ip4(n1390), .op(
        dout[28]) );
  nand2_2 U645 ( .ip1(buf1[28]), .ip2(n90), .op(n1390) );
  nand2_2 U646 ( .ip1(buf0[28]), .ip2(n63), .op(n1391) );
  nand2_2 U647 ( .ip1(int__28), .ip2(n43), .op(n1392) );
  nand2_2 U648 ( .ip1(n56), .ip2(csr[28]), .op(n1393) );
  nand4_2 U649 ( .ip1(n1389), .ip2(n1388), .ip3(n1387), .ip4(n1386), .op(
        dout[27]) );
  nand2_2 U650 ( .ip1(buf1[27]), .ip2(n90), .op(n1386) );
  nand2_2 U651 ( .ip1(buf0[27]), .ip2(n63), .op(n1387) );
  nand2_2 U652 ( .ip1(int__27), .ip2(n47), .op(n1388) );
  nand2_2 U653 ( .ip1(n56), .ip2(csr[27]), .op(n1389) );
  nand4_2 U654 ( .ip1(n1385), .ip2(n1384), .ip3(n1383), .ip4(n1382), .op(
        dout[26]) );
  nand2_2 U655 ( .ip1(buf1[26]), .ip2(n90), .op(n1382) );
  nand2_2 U656 ( .ip1(buf0[26]), .ip2(n63), .op(n1383) );
  nand2_2 U657 ( .ip1(int__26), .ip2(n46), .op(n1384) );
  nand2_2 U658 ( .ip1(csr[26]), .ip2(n55), .op(n1385) );
  nand4_2 U659 ( .ip1(n1381), .ip2(n1380), .ip3(n1379), .ip4(n1378), .op(
        dout[25]) );
  nand2_2 U660 ( .ip1(buf1[25]), .ip2(n90), .op(n1378) );
  nand2_2 U661 ( .ip1(buf0[25]), .ip2(n63), .op(n1379) );
  nand2_2 U662 ( .ip1(int__25), .ip2(n43), .op(n1380) );
  nand2_2 U663 ( .ip1(csr[25]), .ip2(n55), .op(n1381) );
  nand4_2 U664 ( .ip1(n1377), .ip2(n1376), .ip3(n1375), .ip4(n1374), .op(
        dout[24]) );
  nand2_2 U665 ( .ip1(buf1[24]), .ip2(n90), .op(n1374) );
  nand2_2 U666 ( .ip1(buf0[24]), .ip2(n63), .op(n1375) );
  nand2_2 U667 ( .ip1(int__24), .ip2(n42), .op(n1376) );
  nand2_2 U668 ( .ip1(csr[24]), .ip2(n55), .op(n1377) );
  and2_2 U669 ( .ip1(n64), .ip2(buf0[23]), .op(n1372) );
  and2_2 U670 ( .ip1(n91), .ip2(buf1[23]), .op(n1373) );
  and2_2 U671 ( .ip1(n64), .ip2(buf0[22]), .op(n1370) );
  and2_2 U672 ( .ip1(n91), .ip2(buf1[22]), .op(n1371) );
  nand4_2 U673 ( .ip1(n1369), .ip2(n1368), .ip3(n1367), .ip4(n1366), .op(
        dout[21]) );
  nand2_2 U674 ( .ip1(buf1[21]), .ip2(n91), .op(n1366) );
  nand2_2 U675 ( .ip1(buf0[21]), .ip2(n63), .op(n1367) );
  nand2_2 U676 ( .ip1(int__21), .ip2(n41), .op(n1368) );
  nand2_2 U677 ( .ip1(csr[21]), .ip2(n55), .op(n1369) );
  nand4_2 U678 ( .ip1(n1365), .ip2(n1364), .ip3(n1363), .ip4(n1362), .op(
        dout[20]) );
  nand2_2 U679 ( .ip1(buf1[20]), .ip2(n91), .op(n1362) );
  nand2_2 U680 ( .ip1(buf0[20]), .ip2(n64), .op(n1363) );
  nand2_2 U681 ( .ip1(int__20), .ip2(n40), .op(n1364) );
  nand2_2 U682 ( .ip1(csr[20]), .ip2(n55), .op(n1365) );
  nand4_2 U683 ( .ip1(n1361), .ip2(n1360), .ip3(n1359), .ip4(n1358), .op(
        dout[1]) );
  nand2_2 U684 ( .ip1(buf1[1]), .ip2(n91), .op(n1358) );
  nand2_2 U685 ( .ip1(buf0[1]), .ip2(n64), .op(n1359) );
  nand2_2 U686 ( .ip1(n47), .ip2(int_[1]), .op(n1360) );
  nand2_2 U687 ( .ip1(csr[1]), .ip2(n55), .op(n1361) );
  nand4_2 U688 ( .ip1(n1357), .ip2(n1356), .ip3(n1355), .ip4(n1354), .op(
        dout[19]) );
  nand2_2 U689 ( .ip1(buf1[19]), .ip2(n91), .op(n1354) );
  nand2_2 U690 ( .ip1(buf0[19]), .ip2(n64), .op(n1355) );
  nand2_2 U691 ( .ip1(int__19), .ip2(n37), .op(n1356) );
  nand2_2 U692 ( .ip1(csr[19]), .ip2(n55), .op(n1357) );
  nand4_2 U693 ( .ip1(n1353), .ip2(n1352), .ip3(n1351), .ip4(n1350), .op(
        dout[18]) );
  nand2_2 U694 ( .ip1(buf1[18]), .ip2(n91), .op(n1350) );
  nand2_2 U695 ( .ip1(buf0[18]), .ip2(n64), .op(n1351) );
  nand2_2 U696 ( .ip1(int__18), .ip2(n50), .op(n1352) );
  nand2_2 U697 ( .ip1(csr[18]), .ip2(n55), .op(n1353) );
  nand4_2 U698 ( .ip1(n1349), .ip2(n1348), .ip3(n1347), .ip4(n1346), .op(
        dout[17]) );
  nand2_2 U699 ( .ip1(buf1[17]), .ip2(n91), .op(n1346) );
  nand2_2 U700 ( .ip1(buf0[17]), .ip2(n64), .op(n1347) );
  nand2_2 U701 ( .ip1(int__17), .ip2(n49), .op(n1348) );
  nand2_2 U702 ( .ip1(csr[17]), .ip2(n55), .op(n1349) );
  nand4_2 U703 ( .ip1(n1345), .ip2(n1344), .ip3(n1343), .ip4(n1342), .op(
        dout[16]) );
  nand2_2 U704 ( .ip1(buf1[16]), .ip2(n91), .op(n1342) );
  nand2_2 U705 ( .ip1(buf0[16]), .ip2(n64), .op(n1343) );
  nand2_2 U706 ( .ip1(int__16), .ip2(n48), .op(n1344) );
  nand2_2 U707 ( .ip1(csr[16]), .ip2(n55), .op(n1345) );
  and2_2 U708 ( .ip1(n64), .ip2(buf0[15]), .op(n1340) );
  and2_2 U709 ( .ip1(n91), .ip2(buf1[15]), .op(n1341) );
  nand2_2 U710 ( .ip1(n1339), .ip2(n1338), .op(dout[14]) );
  nand2_2 U711 ( .ip1(buf1[14]), .ip2(n91), .op(n1338) );
  nand2_2 U712 ( .ip1(buf0[14]), .ip2(n64), .op(n1339) );
  and2_2 U713 ( .ip1(n65), .ip2(buf0[13]), .op(n1336) );
  and2_2 U714 ( .ip1(n92), .ip2(buf1[13]), .op(n1337) );
  and2_2 U715 ( .ip1(n65), .ip2(buf0[12]), .op(n1334) );
  and2_2 U716 ( .ip1(n92), .ip2(buf1[12]), .op(n1335) );
  and2_2 U717 ( .ip1(n65), .ip2(buf0[11]), .op(n1332) );
  and2_2 U718 ( .ip1(n92), .ip2(buf1[11]), .op(n1333) );
  and2_2 U719 ( .ip1(n65), .ip2(buf0[10]), .op(n1330) );
  and2_2 U720 ( .ip1(n92), .ip2(buf1[10]), .op(n1331) );
  nand4_2 U721 ( .ip1(n1329), .ip2(n1328), .ip3(n1327), .ip4(n1326), .op(
        dout[0]) );
  nand2_2 U722 ( .ip1(buf1[0]), .ip2(n90), .op(n1326) );
  nor2_2 U723 ( .ip1(n1138), .ip2(n1139), .op(n1681) );
  nand2_2 U724 ( .ip1(buf0[0]), .ip2(n64), .op(n1327) );
  nor2_2 U725 ( .ip1(n1138), .ip2(adr[0]), .op(n1516) );
  nand2_2 U726 ( .ip1(n46), .ip2(int_[0]), .op(n1328) );
  nand2_2 U727 ( .ip1(csr[0]), .ip2(n55), .op(n1329) );
  nor2_2 U728 ( .ip1(adr[0]), .ip2(adr[1]), .op(n1432) );
  nor2_2 U737 ( .ip1(buf0_orig[30]), .ip2(buf0_orig[29]), .op(n1325) );
  and2_2 U738 ( .ip1(N319), .ip2(n1324), .op(N320) );
  nand4_2 U739 ( .ip1(n1069), .ip2(n1068), .ip3(n1323), .ip4(n1322), .op(n1324) );
  nor4_2 U740 ( .ip1(n1321), .ip2(csr[4]), .ip3(csr[6]), .ip4(csr[5]), .op(
        n1322) );
  or3_2 U741 ( .ip1(csr[8]), .ip2(csr[9]), .ip3(csr[7]), .op(n1321) );
  or3_2 U746 ( .ip1(n1318), .ip2(dma_out_cnt[6]), .ip3(dma_out_cnt[5]), .op(
        n1319) );
  or3_2 U747 ( .ip1(dma_out_cnt[8]), .ip2(dma_out_cnt[9]), .ip3(dma_out_cnt[7]), .op(n1318) );
  or3_2 U748 ( .ip1(dma_out_cnt[2]), .ip2(dma_out_cnt[4]), .ip3(dma_out_cnt[3]), .op(n1320) );
  nor2_2 U749 ( .ip1(n1824), .ip2(n984), .op(N271) );
  nor2_2 U750 ( .ip1(buf0_set), .ip2(buf0_rl), .op(n1824) );
  and2_2 U752 ( .ip1(int_[5]), .ip2(int__20), .op(n1316) );
  and2_2 U753 ( .ip1(int_[2]), .ip2(int__18), .op(n1317) );
  and2_2 U758 ( .ip1(int_[5]), .ip2(int__28), .op(n1313) );
  and2_2 U759 ( .ip1(int_[2]), .ip2(int__26), .op(n1314) );
  nand2_2 U762 ( .ip1(n1101), .ip2(n1102), .op(n1315) );
  and2_2 U764 ( .ip1(re), .ip2(n42), .op(N191) );
  inv_2 U820 ( .ip(rst), .op(n1889) );
  not_ab_or_c_or_d U859 ( .ip1(n80), .ip2(din[31]), .ip3(n1678), .ip4(n1677), 
        .op(n1679) );
  not_ab_or_c_or_d U860 ( .ip1(n80), .ip2(din[30]), .ip3(n1671), .ip4(n1670), 
        .op(n1672) );
  not_ab_or_c_or_d U861 ( .ip1(n80), .ip2(din[29]), .ip3(n1667), .ip4(n1666), 
        .op(n1668) );
  not_ab_or_c_or_d U862 ( .ip1(n80), .ip2(din[28]), .ip3(n1663), .ip4(n1662), 
        .op(n1664) );
  not_ab_or_c_or_d U863 ( .ip1(n80), .ip2(din[27]), .ip3(n1659), .ip4(n1658), 
        .op(n1660) );
  not_ab_or_c_or_d U864 ( .ip1(n80), .ip2(din[26]), .ip3(n1655), .ip4(n1654), 
        .op(n1656) );
  not_ab_or_c_or_d U865 ( .ip1(n80), .ip2(din[25]), .ip3(n1651), .ip4(n1650), 
        .op(n1652) );
  not_ab_or_c_or_d U866 ( .ip1(n80), .ip2(din[24]), .ip3(n1647), .ip4(n1646), 
        .op(n1648) );
  not_ab_or_c_or_d U867 ( .ip1(n80), .ip2(din[23]), .ip3(n1643), .ip4(n1642), 
        .op(n1644) );
  not_ab_or_c_or_d U868 ( .ip1(n80), .ip2(din[22]), .ip3(n1639), .ip4(n1638), 
        .op(n1640) );
  not_ab_or_c_or_d U869 ( .ip1(n80), .ip2(din[21]), .ip3(n1635), .ip4(n1634), 
        .op(n1636) );
  not_ab_or_c_or_d U870 ( .ip1(n80), .ip2(din[20]), .ip3(n1631), .ip4(n1630), 
        .op(n1632) );
  not_ab_or_c_or_d U871 ( .ip1(n80), .ip2(din[19]), .ip3(n1627), .ip4(n1626), 
        .op(n1628) );
  not_ab_or_c_or_d U872 ( .ip1(n80), .ip2(din[18]), .ip3(n1623), .ip4(n1622), 
        .op(n1624) );
  not_ab_or_c_or_d U873 ( .ip1(n79), .ip2(din[17]), .ip3(n1619), .ip4(n1618), 
        .op(n1620) );
  not_ab_or_c_or_d U874 ( .ip1(n80), .ip2(din[16]), .ip3(n1615), .ip4(n1614), 
        .op(n1616) );
  not_ab_or_c_or_d U875 ( .ip1(n78), .ip2(din[15]), .ip3(n1611), .ip4(n1610), 
        .op(n1612) );
  not_ab_or_c_or_d U876 ( .ip1(n79), .ip2(din[14]), .ip3(n1607), .ip4(n1606), 
        .op(n1608) );
  not_ab_or_c_or_d U877 ( .ip1(n80), .ip2(din[13]), .ip3(n1603), .ip4(n1602), 
        .op(n1604) );
  not_ab_or_c_or_d U878 ( .ip1(n78), .ip2(din[12]), .ip3(n1599), .ip4(n1598), 
        .op(n1600) );
  not_ab_or_c_or_d U879 ( .ip1(n79), .ip2(din[11]), .ip3(n1595), .ip4(n1594), 
        .op(n1596) );
  not_ab_or_c_or_d U880 ( .ip1(n80), .ip2(din[10]), .ip3(n1591), .ip4(n1590), 
        .op(n1592) );
  not_ab_or_c_or_d U881 ( .ip1(n78), .ip2(din[9]), .ip3(n1587), .ip4(n1586), 
        .op(n1588) );
  not_ab_or_c_or_d U882 ( .ip1(n79), .ip2(din[8]), .ip3(n1583), .ip4(n1582), 
        .op(n1584) );
  not_ab_or_c_or_d U883 ( .ip1(n80), .ip2(din[7]), .ip3(n1579), .ip4(n1578), 
        .op(n1580) );
  not_ab_or_c_or_d U884 ( .ip1(n78), .ip2(din[6]), .ip3(n1575), .ip4(n1574), 
        .op(n1576) );
  not_ab_or_c_or_d U885 ( .ip1(n79), .ip2(din[5]), .ip3(n1571), .ip4(n1570), 
        .op(n1572) );
  not_ab_or_c_or_d U886 ( .ip1(n80), .ip2(din[4]), .ip3(n1567), .ip4(n1566), 
        .op(n1568) );
  not_ab_or_c_or_d U887 ( .ip1(n78), .ip2(din[3]), .ip3(n1563), .ip4(n1562), 
        .op(n1564) );
  not_ab_or_c_or_d U888 ( .ip1(n79), .ip2(din[2]), .ip3(n1559), .ip4(n1558), 
        .op(n1560) );
  not_ab_or_c_or_d U889 ( .ip1(n80), .ip2(din[1]), .ip3(n1555), .ip4(n1554), 
        .op(n1556) );
  not_ab_or_c_or_d U890 ( .ip1(n80), .ip2(din[0]), .ip3(n1551), .ip4(n1550), 
        .op(n1552) );
  ab_or_c_or_d U891 ( .ip1(buf0_orig[0]), .ip2(n69), .ip3(n1548), .ip4(n1889), 
        .op(n1210) );
  ab_or_c_or_d U892 ( .ip1(buf0_orig[1]), .ip2(n72), .ip3(n1547), .ip4(n1889), 
        .op(n1209) );
  ab_or_c_or_d U893 ( .ip1(buf0_orig[2]), .ip2(n72), .ip3(n1546), .ip4(n1889), 
        .op(n1208) );
  ab_or_c_or_d U894 ( .ip1(buf0_orig[3]), .ip2(n72), .ip3(n1545), .ip4(n1889), 
        .op(n1207) );
  ab_or_c_or_d U895 ( .ip1(buf0_orig[4]), .ip2(n71), .ip3(n1544), .ip4(n1889), 
        .op(n1206) );
  ab_or_c_or_d U896 ( .ip1(buf0_orig[5]), .ip2(n71), .ip3(n1543), .ip4(n1889), 
        .op(n1205) );
  ab_or_c_or_d U897 ( .ip1(buf0_orig[6]), .ip2(n71), .ip3(n1542), .ip4(n1889), 
        .op(n1204) );
  ab_or_c_or_d U898 ( .ip1(buf0_orig[7]), .ip2(n71), .ip3(n1541), .ip4(n1889), 
        .op(n1203) );
  ab_or_c_or_d U899 ( .ip1(buf0_orig[8]), .ip2(n70), .ip3(n1540), .ip4(n1889), 
        .op(n1202) );
  ab_or_c_or_d U900 ( .ip1(buf0_orig[9]), .ip2(n70), .ip3(n1539), .ip4(n1889), 
        .op(n1201) );
  ab_or_c_or_d U901 ( .ip1(buf0_orig[10]), .ip2(n70), .ip3(n1538), .ip4(n1889), 
        .op(n1200) );
  ab_or_c_or_d U902 ( .ip1(buf0_orig[11]), .ip2(n70), .ip3(n1537), .ip4(n1889), 
        .op(n1199) );
  ab_or_c_or_d U903 ( .ip1(buf0_orig[12]), .ip2(n69), .ip3(n1536), .ip4(n1889), 
        .op(n1198) );
  ab_or_c_or_d U904 ( .ip1(buf0_orig[13]), .ip2(n69), .ip3(n1535), .ip4(n1889), 
        .op(n1197) );
  ab_or_c_or_d U905 ( .ip1(buf0_orig[14]), .ip2(n69), .ip3(n1534), .ip4(n1889), 
        .op(n1196) );
  ab_or_c_or_d U906 ( .ip1(buf0_orig[15]), .ip2(n68), .ip3(n1533), .ip4(n1889), 
        .op(n1195) );
  ab_or_c_or_d U907 ( .ip1(buf0_orig[16]), .ip2(n68), .ip3(n1532), .ip4(n1889), 
        .op(n1194) );
  ab_or_c_or_d U908 ( .ip1(buf0_orig[17]), .ip2(n68), .ip3(n1531), .ip4(n1889), 
        .op(n1193) );
  ab_or_c_or_d U909 ( .ip1(buf0_orig[18]), .ip2(n68), .ip3(n1530), .ip4(n1889), 
        .op(n1192) );
  ab_or_c_or_d U922 ( .ip1(buf0_orig[31]), .ip2(n67), .ip3(n1517), .ip4(n1889), 
        .op(n1179) );
  ab_or_c_or_d U923 ( .ip1(csr[22]), .ip2(n1474), .ip3(n1067), .ip4(n1473), 
        .op(n1159) );
  ab_or_c_or_d U924 ( .ip1(csr[9]), .ip2(n56), .ip3(n1427), .ip4(n1426), .op(
        dout[9]) );
  ab_or_c_or_d U925 ( .ip1(csr[8]), .ip2(n56), .ip3(n1425), .ip4(n1424), .op(
        dout[8]) );
  ab_or_c_or_d U926 ( .ip1(csr[7]), .ip2(n56), .ip3(n1423), .ip4(n1422), .op(
        dout[7]) );
  ab_or_c_or_d U927 ( .ip1(n57), .ip2(csr[31]), .ip3(n1405), .ip4(n1404), .op(
        dout[31]) );
  ab_or_c_or_d U928 ( .ip1(n57), .ip2(csr[30]), .ip3(n1403), .ip4(n1402), .op(
        dout[30]) );
  ab_or_c_or_d U929 ( .ip1(csr[23]), .ip2(n56), .ip3(n1373), .ip4(n1372), .op(
        dout[23]) );
  ab_or_c_or_d U930 ( .ip1(csr[22]), .ip2(n56), .ip3(n1371), .ip4(n1370), .op(
        dout[22]) );
  ab_or_c_or_d U931 ( .ip1(n57), .ip2(csr[15]), .ip3(n1341), .ip4(n1340), .op(
        dout[15]) );
  ab_or_c_or_d U932 ( .ip1(csr[13]), .ip2(n56), .ip3(n1337), .ip4(n1336), .op(
        dout[13]) );
  ab_or_c_or_d U933 ( .ip1(csr[12]), .ip2(n56), .ip3(n1335), .ip4(n1334), .op(
        dout[12]) );
  ab_or_c_or_d U934 ( .ip1(csr[11]), .ip2(n56), .ip3(n1333), .ip4(n1332), .op(
        dout[11]) );
  ab_or_c_or_d U935 ( .ip1(csr[10]), .ip2(n56), .ip3(n1331), .ip4(n1330), .op(
        dout[10]) );
  dp_2 inta_reg ( .ip(N221), .ck(wclk), .q(inta) );
  dp_2 intb_reg ( .ip(N222), .ck(wclk), .q(intb) );
  dp_2 \buf0_orig_m3_reg[10]  ( .ip(N345), .ck(wclk), .q(buf0_orig_m3[10]) );
  dp_2 \buf0_orig_m3_reg[9]  ( .ip(N344), .ck(wclk), .q(buf0_orig_m3[9]) );
  dp_2 \buf0_orig_m3_reg[8]  ( .ip(N343), .ck(wclk), .q(buf0_orig_m3[8]) );
  dp_2 \buf0_orig_m3_reg[7]  ( .ip(N342), .ck(wclk), .q(buf0_orig_m3[7]) );
  dp_2 \buf0_orig_m3_reg[0]  ( .ip(n989), .ck(wclk), .q(buf0_orig_m3[0]) );
  usbf_ep_rf_3_DW01_sub_1 sub_7204 ( .A(buf0_orig[30:19]), .B(dma_out_cnt), 
        .CI(1'b0), .DIFF({N332, N331, N330, N329, N328, N327, N326, N325, N324, 
        N323, N322, N321}) );
  usbf_ep_rf_3_DW01_sub_2 sub_7193 ( .A(dma_in_cnt), .B({1'b0, 1'b0, 1'b0, 
        csr[10:2]}), .CI(1'b0), .DIFF({N302, N301, N300, N299, N298, N297, 
        N296, N295, N294, N293, N292, N291}) );
  usbf_ep_rf_3_DW01_inc_0 add_7190_S2 ( .A(dma_in_cnt), .SUM({N289, N288, N287, 
        N286, N285, N284, N283, N282, N281, N280, N279, N278}) );
  usbf_ep_rf_3_DW01_add_0 add_7168 ( .A(dma_out_cnt), .B({1'b0, 1'b0, 1'b0, 
        csr[10:2]}), .CI(1'b0), .SUM({N253, N252, N251, N250, N249, N248, N247, 
        N246, N245, N244, N243, N242}) );
  usbf_ep_rf_3_DW01_dec_0 sub_7165_S2 ( .A(dma_out_cnt), .SUM({N240, N239, 
        N238, N237, N236, N235, N234, N233, N232, N231, N230, N229}) );
  dp_1 dma_in_buf_sz1_reg ( .ip(N320), .ck(n34), .q(dma_in_buf_sz1) );
  dp_1 \dma_out_left_reg[11]  ( .ip(N332), .ck(n33), .q(dma_out_left[11]) );
  dp_1 \dma_out_left_reg[10]  ( .ip(N331), .ck(n33), .q(dma_out_left[10]) );
  dp_1 \dma_out_left_reg[9]  ( .ip(N330), .ck(n32), .q(dma_out_left[9]) );
  dp_1 \dma_out_left_reg[0]  ( .ip(N321), .ck(n32), .q(dma_out_left[0]) );
  dp_1 set_r_reg ( .ip(N271), .ck(n31), .q(set_r) );
  dp_1 \dma_out_left_reg[1]  ( .ip(N322), .ck(n32), .q(dma_out_left[1]) );
  dp_1 \dma_out_left_reg[3]  ( .ip(N324), .ck(n32), .q(dma_out_left[3]) );
  dp_1 \dma_out_left_reg[5]  ( .ip(N326), .ck(n32), .q(dma_out_left[5]) );
  dp_1 \dma_out_left_reg[7]  ( .ip(N328), .ck(n32), .q(dma_out_left[7]) );
  dp_1 dma_out_buf_avail_reg ( .ip(N333), .ck(n33), .q(dma_out_buf_avail) );
  dp_1 \dma_out_left_reg[2]  ( .ip(N323), .ck(n32), .q(dma_out_left[2]) );
  dp_1 \dma_out_left_reg[4]  ( .ip(N325), .ck(n32), .q(dma_out_left[4]) );
  dp_1 \dma_out_left_reg[6]  ( .ip(N327), .ck(n32), .q(dma_out_left[6]) );
  dp_1 \buf0_orig_reg[31]  ( .ip(n1179), .ck(n22), .q(buf0_orig[31]) );
  dp_1 \buf0_orig_reg[18]  ( .ip(n1192), .ck(n23), .q(buf0_orig[18]) );
  dp_1 \buf0_orig_reg[17]  ( .ip(n1193), .ck(n23), .q(buf0_orig[17]) );
  dp_1 \buf0_orig_reg[16]  ( .ip(n1194), .ck(n23), .q(buf0_orig[16]) );
  dp_1 \buf0_orig_reg[15]  ( .ip(n1195), .ck(n23), .q(buf0_orig[15]) );
  dp_1 \buf0_orig_reg[14]  ( .ip(n1196), .ck(n23), .q(buf0_orig[14]) );
  dp_1 \buf0_orig_reg[13]  ( .ip(n1197), .ck(n24), .q(buf0_orig[13]) );
  dp_1 \buf0_orig_reg[12]  ( .ip(n1198), .ck(n24), .q(buf0_orig[12]) );
  dp_1 \buf0_orig_reg[11]  ( .ip(n1199), .ck(n24), .q(buf0_orig[11]) );
  dp_1 \buf0_orig_reg[10]  ( .ip(n1200), .ck(n24), .q(buf0_orig[10]) );
  dp_1 \buf0_orig_reg[9]  ( .ip(n1201), .ck(n24), .q(buf0_orig[9]) );
  dp_1 \buf0_orig_reg[8]  ( .ip(n1202), .ck(n24), .q(buf0_orig[8]) );
  dp_1 \buf0_orig_reg[7]  ( .ip(n1203), .ck(n24), .q(buf0_orig[7]) );
  dp_1 \buf0_orig_reg[6]  ( .ip(n1204), .ck(n24), .q(buf0_orig[6]) );
  dp_1 \buf0_orig_reg[5]  ( .ip(n1205), .ck(n24), .q(buf0_orig[5]) );
  dp_1 \buf0_orig_reg[4]  ( .ip(n1206), .ck(n24), .q(buf0_orig[4]) );
  dp_1 \buf0_orig_reg[3]  ( .ip(n1207), .ck(n24), .q(buf0_orig[3]) );
  dp_1 \buf0_orig_reg[2]  ( .ip(n1208), .ck(n24), .q(buf0_orig[2]) );
  dp_1 \buf0_orig_reg[1]  ( .ip(n1209), .ck(n24), .q(buf0_orig[1]) );
  dp_1 \buf0_orig_reg[0]  ( .ip(n1210), .ck(n25), .q(buf0_orig[0]) );
  dp_1 \dma_out_left_reg[8]  ( .ip(N329), .ck(n32), .q(dma_out_left[8]) );
  dp_1 \csr1_reg[7]  ( .ip(n1159), .ck(n21), .q(csr[22]) );
  dp_1 r5_reg ( .ip(r4), .ck(n31), .q(r5) );
  dp_1 int_re_reg ( .ip(N191), .ck(n30), .q(int_re) );
  dp_1 \int_stat_reg[4]  ( .ip(n1277), .ck(n30), .q(int_[4]) );
  dp_1 \int_stat_reg[3]  ( .ip(n1278), .ck(n30), .q(int_[3]) );
  dp_1 \csr0_reg[12]  ( .ip(n1141), .ck(n20), .q(csr[12]) );
  dp_1 \csr0_reg[11]  ( .ip(n1142), .ck(n20), .q(csr[11]) );
  dp_1 \csr1_reg[8]  ( .ip(n1158), .ck(n21), .q(csr[23]) );
  dp_1 \iena_reg[3]  ( .ip(n1169), .ck(n21), .q(int__27) );
  dp_1 \ienb_reg[3]  ( .ip(n1175), .ck(n22), .q(int__19) );
  dp_1 \uc_bsel_reg[1]  ( .ip(n1284), .ck(n30), .q(csr[31]) );
  dp_1 \uc_bsel_reg[0]  ( .ip(n1285), .ck(n31), .q(csr[30]) );
  dp_1 \csr1_reg[10]  ( .ip(n1156), .ck(n20), .q(csr[25]) );
  dp_1 \csr1_reg[9]  ( .ip(n1157), .ck(n20), .q(csr[24]) );
  dp_1 \csr1_reg[2]  ( .ip(n1164), .ck(n21), .q(csr[17]) );
  dp_1 \csr1_reg[1]  ( .ip(n1165), .ck(n21), .q(csr[16]) );
  dp_1 \iena_reg[5]  ( .ip(n1167), .ck(n21), .q(int__29) );
  dp_1 \iena_reg[1]  ( .ip(n1171), .ck(n22), .q(int__25) );
  dp_1 \iena_reg[0]  ( .ip(n1172), .ck(n22), .q(int__24) );
  dp_1 \ienb_reg[5]  ( .ip(n1173), .ck(n22), .q(int__21) );
  dp_1 \ienb_reg[1]  ( .ip(n1177), .ck(n22), .q(int__17) );
  dp_1 \ienb_reg[0]  ( .ip(n1178), .ck(n22), .q(int__16) );
  dp_1 \buf0_reg[29]  ( .ip(n1240), .ck(n25), .q(buf0[29]) );
  dp_1 \buf0_reg[28]  ( .ip(n1239), .ck(n25), .q(buf0[28]) );
  dp_1 \buf0_reg[27]  ( .ip(n1238), .ck(n25), .q(buf0[27]) );
  dp_1 \buf0_reg[26]  ( .ip(n1237), .ck(n25), .q(buf0[26]) );
  dp_1 \buf0_reg[25]  ( .ip(n1236), .ck(n25), .q(buf0[25]) );
  dp_1 \buf0_reg[24]  ( .ip(n1235), .ck(n25), .q(buf0[24]) );
  dp_1 \buf0_reg[21]  ( .ip(n1232), .ck(n25), .q(buf0[21]) );
  dp_1 \buf0_reg[20]  ( .ip(n1231), .ck(n26), .q(buf0[20]) );
  dp_1 \buf0_reg[19]  ( .ip(n1230), .ck(n26), .q(buf0[19]) );
  dp_1 \buf0_reg[18]  ( .ip(n1229), .ck(n26), .q(buf0[18]) );
  dp_1 \buf0_reg[17]  ( .ip(n1228), .ck(n26), .q(buf0[17]) );
  dp_1 \buf0_reg[16]  ( .ip(n1227), .ck(n26), .q(buf0[16]) );
  dp_1 \buf0_reg[14]  ( .ip(n1225), .ck(n26), .q(buf0[14]) );
  dp_1 \buf0_reg[6]  ( .ip(n1217), .ck(n27), .q(buf0[6]) );
  dp_1 \buf0_reg[5]  ( .ip(n1216), .ck(n27), .q(buf0[5]) );
  dp_1 \buf0_reg[4]  ( .ip(n1215), .ck(n27), .q(buf0[4]) );
  dp_1 \buf0_reg[3]  ( .ip(n1214), .ck(n27), .q(buf0[3]) );
  dp_1 \buf0_reg[2]  ( .ip(n1213), .ck(n27), .q(buf0[2]) );
  dp_1 \buf0_reg[1]  ( .ip(n1212), .ck(n27), .q(buf0[1]) );
  dp_1 \buf0_reg[0]  ( .ip(n1211), .ck(n27), .q(buf0[0]) );
  dp_1 \buf1_reg[29]  ( .ip(n1272), .ck(n27), .q(buf1[29]) );
  dp_1 \buf1_reg[28]  ( .ip(n1271), .ck(n27), .q(buf1[28]) );
  dp_1 \buf1_reg[27]  ( .ip(n1270), .ck(n28), .q(buf1[27]) );
  dp_1 \buf1_reg[26]  ( .ip(n1269), .ck(n28), .q(buf1[26]) );
  dp_1 \buf1_reg[25]  ( .ip(n1268), .ck(n28), .q(buf1[25]) );
  dp_1 \buf1_reg[24]  ( .ip(n1267), .ck(n28), .q(buf1[24]) );
  dp_1 \buf1_reg[21]  ( .ip(n1264), .ck(n28), .q(buf1[21]) );
  dp_1 \buf1_reg[20]  ( .ip(n1263), .ck(n28), .q(buf1[20]) );
  dp_1 \buf1_reg[19]  ( .ip(n1262), .ck(n28), .q(buf1[19]) );
  dp_1 \buf1_reg[18]  ( .ip(n1261), .ck(n28), .q(buf1[18]) );
  dp_1 \buf1_reg[17]  ( .ip(n1260), .ck(n28), .q(buf1[17]) );
  dp_1 \buf1_reg[16]  ( .ip(n1259), .ck(n28), .q(buf1[16]) );
  dp_1 \buf1_reg[14]  ( .ip(n1257), .ck(n29), .q(buf1[14]) );
  dp_1 \buf1_reg[6]  ( .ip(n1249), .ck(n29), .q(buf1[6]) );
  dp_1 \buf1_reg[5]  ( .ip(n1248), .ck(n29), .q(buf1[5]) );
  dp_1 \buf1_reg[4]  ( .ip(n1247), .ck(n29), .q(buf1[4]) );
  dp_1 \buf1_reg[3]  ( .ip(n1246), .ck(n29), .q(buf1[3]) );
  dp_1 \buf1_reg[2]  ( .ip(n1245), .ck(n29), .q(buf1[2]) );
  dp_1 \buf1_reg[1]  ( .ip(n1244), .ck(n30), .q(buf1[1]) );
  dp_1 \buf1_reg[0]  ( .ip(n1243), .ck(n30), .q(buf1[0]) );
  dp_1 \iena_reg[4]  ( .ip(n1168), .ck(n21), .q(int__28) );
  dp_1 \iena_reg[2]  ( .ip(n1170), .ck(n21), .q(int__26) );
  dp_1 \ienb_reg[4]  ( .ip(n1174), .ck(n22), .q(int__20) );
  dp_1 \ienb_reg[2]  ( .ip(n1176), .ck(n22), .q(int__18) );
  dp_1 \buf0_reg[31]  ( .ip(n1242), .ck(n25), .q(buf0[31]) );
  dp_1 \buf0_reg[30]  ( .ip(n1241), .ck(n25), .q(buf0[30]) );
  dp_1 \buf0_reg[23]  ( .ip(n1234), .ck(n25), .q(buf0[23]) );
  dp_1 \buf0_reg[22]  ( .ip(n1233), .ck(n25), .q(buf0[22]) );
  dp_1 \buf0_reg[15]  ( .ip(n1226), .ck(n26), .q(buf0[15]) );
  dp_1 \buf0_reg[13]  ( .ip(n1224), .ck(n26), .q(buf0[13]) );
  dp_1 \buf0_reg[12]  ( .ip(n1223), .ck(n26), .q(buf0[12]) );
  dp_1 \buf0_reg[11]  ( .ip(n1222), .ck(n26), .q(buf0[11]) );
  dp_1 \buf0_reg[10]  ( .ip(n1221), .ck(n26), .q(buf0[10]) );
  dp_1 \buf0_reg[9]  ( .ip(n1220), .ck(n26), .q(buf0[9]) );
  dp_1 \buf0_reg[8]  ( .ip(n1219), .ck(n26), .q(buf0[8]) );
  dp_1 \buf0_reg[7]  ( .ip(n1218), .ck(n27), .q(buf0[7]) );
  dp_1 \buf1_reg[31]  ( .ip(n1274), .ck(n27), .q(buf1[31]) );
  dp_1 \buf1_reg[30]  ( .ip(n1273), .ck(n27), .q(buf1[30]) );
  dp_1 \buf1_reg[23]  ( .ip(n1266), .ck(n28), .q(buf1[23]) );
  dp_1 \buf1_reg[22]  ( .ip(n1265), .ck(n28), .q(buf1[22]) );
  dp_1 \buf1_reg[15]  ( .ip(n1258), .ck(n28), .q(buf1[15]) );
  dp_1 \buf1_reg[13]  ( .ip(n1256), .ck(n29), .q(buf1[13]) );
  dp_1 \buf1_reg[12]  ( .ip(n1255), .ck(n29), .q(buf1[12]) );
  dp_1 \buf1_reg[11]  ( .ip(n1254), .ck(n29), .q(buf1[11]) );
  dp_1 \buf1_reg[10]  ( .ip(n1253), .ck(n29), .q(buf1[10]) );
  dp_1 \buf1_reg[9]  ( .ip(n1252), .ck(n29), .q(buf1[9]) );
  dp_1 \buf1_reg[8]  ( .ip(n1251), .ck(n29), .q(buf1[8]) );
  dp_1 \buf1_reg[7]  ( .ip(n1250), .ck(n29), .q(buf1[7]) );
  dp_1 \uc_dpd_reg[1]  ( .ip(n1282), .ck(n30), .q(csr[29]) );
  dp_1 \uc_dpd_reg[0]  ( .ip(n1283), .ck(n30), .q(csr[28]) );
  dp_1 ots_stop_reg ( .ip(n1140), .ck(n27), .q(csr[13]) );
  dp_1 \csr0_reg[0]  ( .ip(n1153), .ck(n20), .q(csr[0]) );
  dp_1 \csr1_reg[11]  ( .ip(n1155), .ck(n20), .q(csr[26]) );
  dp_1 \int_stat_reg[5]  ( .ip(n1276), .ck(n30), .q(int_[5]) );
  dp_1 \int_stat_reg[2]  ( .ip(n1279), .ck(n30), .q(int_[2]) );
  dp_1 \int_stat_reg[6]  ( .ip(n1275), .ck(n30), .q(int_[6]) );
  dp_1 \int_stat_reg[1]  ( .ip(n1280), .ck(n30), .q(int_[1]) );
  dp_1 \int_stat_reg[0]  ( .ip(n1281), .ck(n30), .q(int_[0]) );
  dp_1 ep_match_r_reg ( .ip(ep_match), .ck(n25), .q(ep_match_r) );
  dp_1 \buf0_orig_reg[27]  ( .ip(n1183), .ck(n22), .q(buf0_orig[27]) );
  dp_1 \csr1_reg[0]  ( .ip(n1166), .ck(n21), .q(csr[15]) );
  dp_1 \csr1_reg[6]  ( .ip(n1160), .ck(n21), .q(csr[21]) );
  dp_1 \csr1_reg[5]  ( .ip(n1161), .ck(n21), .q(csr[20]) );
  dp_1 \csr1_reg[4]  ( .ip(n1162), .ck(n21), .q(csr[19]) );
  dp_1 \csr1_reg[3]  ( .ip(n1163), .ck(n21), .q(csr[18]) );
  dp_1 \csr0_reg[8]  ( .ip(n1145), .ck(n34), .q(csr[8]) );
  dp_1 \csr0_reg[1]  ( .ip(n1152), .ck(n20), .q(csr[1]) );
  dp_1 \csr0_reg[6]  ( .ip(n1147), .ck(n34), .q(csr[6]) );
  dp_1 \csr0_reg[4]  ( .ip(n1149), .ck(n34), .q(csr[4]) );
  dp_1 \buf0_orig_reg[28]  ( .ip(n1182), .ck(n22), .q(buf0_orig[28]) );
  dp_1 \csr0_reg[10]  ( .ip(n1143), .ck(n34), .q(csr[10]) );
  dp_1 \buf0_orig_reg[25]  ( .ip(n1185), .ck(n23), .q(buf0_orig[25]) );
  dp_1 \buf0_orig_reg[23]  ( .ip(n1187), .ck(n23), .q(buf0_orig[23]) );
  dp_1 \dma_in_cnt_reg[8]  ( .ip(n1296), .ck(n34), .q(dma_in_cnt[8]) );
  dp_1 \dma_out_cnt_reg[11]  ( .ip(n1299), .ck(n31), .q(dma_out_cnt[11]) );
  dp_1 \dma_out_cnt_reg[4]  ( .ip(n1304), .ck(n31), .q(dma_out_cnt[4]) );
  dp_1 \dma_out_cnt_reg[6]  ( .ip(n1306), .ck(n31), .q(dma_out_cnt[6]) );
  dp_1 \dma_out_cnt_reg[3]  ( .ip(n1303), .ck(n31), .q(dma_out_cnt[3]) );
  dp_1 \dma_out_cnt_reg[5]  ( .ip(n1305), .ck(n31), .q(dma_out_cnt[5]) );
  dp_1 \dma_out_cnt_reg[7]  ( .ip(n1307), .ck(n32), .q(dma_out_cnt[7]) );
  dp_1 \dma_out_cnt_reg[2]  ( .ip(n1302), .ck(n31), .q(dma_out_cnt[2]) );
  dp_1 \dma_out_cnt_reg[8]  ( .ip(n1308), .ck(n32), .q(dma_out_cnt[8]) );
  dp_1 \dma_out_cnt_reg[1]  ( .ip(n1301), .ck(n31), .q(dma_out_cnt[1]) );
  dp_1 \dma_in_cnt_reg[9]  ( .ip(n1297), .ck(n34), .q(dma_in_cnt[9]) );
  dp_1 \buf0_orig_reg[26]  ( .ip(n1184), .ck(n23), .q(buf0_orig[26]) );
  dp_1 \buf0_orig_reg[24]  ( .ip(n1186), .ck(n23), .q(buf0_orig[24]) );
  dp_1 \dma_out_cnt_reg[9]  ( .ip(n1309), .ck(n32), .q(dma_out_cnt[9]) );
  dp_1 \dma_in_cnt_reg[11]  ( .ip(n1287), .ck(n33), .q(dma_in_cnt[11]) );
  dp_1 \dma_in_cnt_reg[5]  ( .ip(n1293), .ck(n33), .q(dma_in_cnt[5]) );
  dp_1 \dma_in_cnt_reg[10]  ( .ip(n1298), .ck(n33), .q(dma_in_cnt[10]) );
  dp_1 \dma_in_cnt_reg[3]  ( .ip(n1291), .ck(n33), .q(dma_in_cnt[3]) );
  dp_1 \dma_in_cnt_reg[7]  ( .ip(n1295), .ck(n33), .q(dma_in_cnt[7]) );
  dp_1 \buf0_orig_reg[19]  ( .ip(n1191), .ck(n23), .q(buf0_orig[19]) );
  dp_1 \csr1_reg[12]  ( .ip(n1154), .ck(n20), .q(csr[27]) );
  dp_1 \buf0_orig_reg[30]  ( .ip(n1180), .ck(n22), .q(buf0_orig[30]) );
  dp_1 \dma_out_cnt_reg[10]  ( .ip(n1310), .ck(n31), .q(dma_out_cnt[10]) );
  dp_1 \buf0_orig_reg[22]  ( .ip(n1188), .ck(n23), .q(buf0_orig[22]) );
  dp_1 \dma_in_cnt_reg[6]  ( .ip(n1294), .ck(n33), .q(dma_in_cnt[6]) );
  dp_1 \buf0_orig_reg[21]  ( .ip(n1189), .ck(n23), .q(buf0_orig[21]) );
  dp_1 \dma_in_cnt_reg[0]  ( .ip(n1288), .ck(n33), .q(dma_in_cnt[0]) );
  dp_1 \dma_out_cnt_reg[0]  ( .ip(n1300), .ck(n31), .q(dma_out_cnt[0]) );
  dp_1 \buf0_orig_reg[20]  ( .ip(n1190), .ck(n23), .q(buf0_orig[20]) );
  dp_1 \dma_in_cnt_reg[1]  ( .ip(n1289), .ck(n33), .q(dma_in_cnt[1]) );
  dp_1 \csr0_reg[9]  ( .ip(n1144), .ck(n20), .q(csr[9]) );
  dp_1 \csr0_reg[7]  ( .ip(n1146), .ck(n20), .q(csr[7]) );
  dp_1 \buf0_orig_reg[29]  ( .ip(n1181), .ck(n22), .q(buf0_orig[29]) );
  dp_1 \dma_in_cnt_reg[2]  ( .ip(n1290), .ck(n33), .q(dma_in_cnt[2]) );
  dp_1 \csr0_reg[5]  ( .ip(n1148), .ck(n20), .q(csr[5]) );
  dp_1 \dma_in_cnt_reg[4]  ( .ip(n1292), .ck(n33), .q(dma_in_cnt[4]) );
  dp_1 \csr0_reg[3]  ( .ip(n1150), .ck(n20), .q(csr[3]) );
  dp_1 \csr0_reg[2]  ( .ip(n1151), .ck(n20), .q(csr[2]) );
  dp_1 dma_ack_clr1_reg ( .ip(r4), .ck(wclk), .q(dma_ack_clr1) );
  dp_1 dma_req_out_hold_reg ( .ip(N272), .ck(wclk), .q(dma_req_out_hold) );
  dp_1 dma_req_in_hold_reg ( .ip(N348), .ck(wclk), .q(dma_req_in_hold) );
  dp_1 \buf0_orig_m3_reg[1]  ( .ip(N336), .ck(wclk), .q(buf0_orig_m3[1]) );
  dp_1 \buf0_orig_m3_reg[2]  ( .ip(N337), .ck(wclk), .q(buf0_orig_m3[2]) );
  dp_1 r4_reg ( .ip(dma_ack_wr1), .ck(n31), .q(r4) );
  dp_1 r1_reg ( .ip(N361), .ck(wclk), .q(r1) );
  dp_1 dma_ack_wr1_reg ( .ip(n1286), .ck(wclk), .q(dma_ack_wr1) );
  dp_1 r2_reg ( .ip(n1311), .ck(wclk), .q(r2) );
  dp_1 dma_req_r_reg ( .ip(n1312), .ck(wclk), .q(dma_req) );
  dp_2 dma_req_in_hold2_reg ( .ip(N347), .ck(wclk), .q(dma_req_in_hold2) );
  dp_1 \buf0_orig_m3_reg[11]  ( .ip(N346), .ck(wclk), .q(buf0_orig_m3[11]) );
  dp_1 \buf0_orig_m3_reg[6]  ( .ip(N341), .ck(wclk), .q(buf0_orig_m3[6]) );
  dp_1 \buf0_orig_m3_reg[5]  ( .ip(N340), .ck(wclk), .q(buf0_orig_m3[5]) );
  dp_1 \buf0_orig_m3_reg[3]  ( .ip(N338), .ck(wclk), .q(buf0_orig_m3[3]) );
  dp_2 \buf0_orig_m3_reg[4]  ( .ip(N339), .ck(wclk), .q(buf0_orig_m3[4]) );
  and2_2 U3 ( .ip1(n1821), .ip2(n19), .op(n1) );
  and2_1 U4 ( .ip1(buf0_orig_m3[3]), .ip2(n996), .op(n2) );
  and2_2 U5 ( .ip1(csr[27]), .ip2(n959), .op(n3) );
  nand2_1 U6 ( .ip1(dma_ack), .ip2(n971), .op(n972) );
  nand2_1 U7 ( .ip1(dma_req_in_hold2), .ip2(dma_req_in_hold), .op(n970) );
  and2_1 U8 ( .ip1(rst), .ip2(n973), .op(n1312) );
  nand2_1 U9 ( .ip1(n730), .ip2(buf0_orig_m3[4]), .op(n4) );
  nand2_1 U10 ( .ip1(n730), .ip2(buf0_orig_m3[4]), .op(n911) );
  nand2_1 U12 ( .ip1(n995), .ip2(buf0_orig_m3[5]), .op(n5) );
  nand2_1 U13 ( .ip1(n995), .ip2(buf0_orig_m3[5]), .op(n910) );
  nand2_1 U16 ( .ip1(n941), .ip2(n940), .op(n946) );
  nand2_1 U17 ( .ip1(n946), .ip2(n945), .op(n947) );
  inv_1 U18 ( .ip(n5), .op(n732) );
  nand2_1 U19 ( .ip1(n926), .ip2(n927), .op(n948) );
  nand2_1 U20 ( .ip1(r1), .ip2(n968), .op(n11) );
  inv_1 U21 ( .ip(n724), .op(n15) );
  nand2_1 U22 ( .ip1(dma_ack_wr1), .ip2(n949), .op(n14) );
  inv_2 U23 ( .ip(dma_ack), .op(n13) );
  nand2_1 U72 ( .ip1(n7), .ip2(n8), .op(N347) );
  nand2_1 U73 ( .ip1(n947), .ip2(n948), .op(n7) );
  nand2_1 U76 ( .ip1(buf0_orig_m3[11]), .ip2(n960), .op(n8) );
  nand2_1 U77 ( .ip1(dma_in_cnt[3]), .ip2(n729), .op(n908) );
  or2_1 U80 ( .ip1(n730), .ip2(buf0_orig_m3[4]), .op(n909) );
  nand2_1 U81 ( .ip1(n11), .ip2(n12), .op(n973) );
  nand2_1 U84 ( .ip1(dma_req), .ip2(n972), .op(n12) );
  nand2_1 U85 ( .ip1(n14), .ip2(n13), .op(n951) );
  inv_2 U88 ( .ip(int_re), .op(n19) );
  nand3_1 U89 ( .ip1(n925), .ip2(n923), .ip3(n924), .op(n926) );
  nand2_1 U92 ( .ip1(n922), .ip2(n921), .op(n923) );
  nand2_1 U93 ( .ip1(n910), .ip2(n911), .op(n912) );
  inv_1 U96 ( .ip(n4), .op(n731) );
  buf_2 U97 ( .ip(n1137), .op(n53) );
  buf_2 U100 ( .ip(n1137), .op(n52) );
  buf_2 U101 ( .ip(n1137), .op(n54) );
  inv_2 U104 ( .ip(n73), .op(n78) );
  inv_2 U105 ( .ip(n73), .op(n80) );
  inv_2 U108 ( .ip(n72), .op(n79) );
  buf_2 U109 ( .ip(n1777), .op(n93) );
  buf_2 U112 ( .ip(n1777), .op(n94) );
  buf_2 U113 ( .ip(n1674), .op(n81) );
  buf_2 U116 ( .ip(n1674), .op(n82) );
  nand2_2 U117 ( .ip1(n65), .ip2(we), .op(n1549) );
  buf_2 U118 ( .ip(n1883), .op(n99) );
  buf_2 U124 ( .ip(n1681), .op(n91) );
  buf_2 U125 ( .ip(n1676), .op(n87) );
  buf_2 U126 ( .ip(n1676), .op(n88) );
  buf_2 U179 ( .ip(n1681), .op(n90) );
  buf_2 U476 ( .ip(n1883), .op(n100) );
  buf_2 U513 ( .ip(n1885), .op(n101) );
  buf_2 U514 ( .ip(n1885), .op(n102) );
  inv_2 U515 ( .ip(n35), .op(n36) );
  and2_2 U517 ( .ip1(n16), .ip2(n15), .op(n927) );
  and2_1 U729 ( .ip1(n934), .ip2(n935), .op(n16) );
  nor3_2 U730 ( .ip1(n1884), .ip2(n99), .ip3(n1070), .op(n1885) );
  buf_2 U731 ( .ip(n1516), .op(n64) );
  buf_2 U732 ( .ip(n1487), .op(n59) );
  buf_2 U733 ( .ip(n1675), .op(n84) );
  buf_2 U734 ( .ip(n1488), .op(n60) );
  buf_2 U735 ( .ip(n1488), .op(n61) );
  buf_2 U736 ( .ip(n1675), .op(n85) );
  buf_2 U742 ( .ip(n1432), .op(n56) );
  buf_2 U743 ( .ip(n1487), .op(n58) );
  buf_2 U744 ( .ip(n1516), .op(n63) );
  buf_2 U745 ( .ip(n1432), .op(n55) );
  buf_2 U751 ( .ip(n1778), .op(n96) );
  buf_2 U754 ( .ip(n1778), .op(n97) );
  buf_2 U755 ( .ip(n1778), .op(n98) );
  inv_2 U756 ( .ip(n1884), .op(n35) );
  and2_2 U757 ( .ip1(n17), .ip2(n18), .op(n925) );
  or2_1 U760 ( .ip1(buf0_orig_m3[5]), .ip2(n995), .op(n17) );
  or2_1 U761 ( .ip1(buf0_orig_m3[6]), .ip2(n725), .op(n18) );
  nor2_2 U763 ( .ip1(n1889), .ip2(n1513), .op(n1512) );
  and3_2 U765 ( .ip1(we), .ip2(rst), .ip3(n40), .op(n1513) );
  nor3_2 U766 ( .ip1(csr[1]), .ip2(csr[3]), .ip3(csr[2]), .op(n1323) );
  nor3_2 U767 ( .ip1(csr[27]), .ip2(n123), .ip3(n959), .op(N348) );
  buf_1 U768 ( .ip(clk), .op(n20) );
  buf_1 U769 ( .ip(clk), .op(n21) );
  buf_1 U770 ( .ip(clk), .op(n22) );
  buf_1 U771 ( .ip(clk), .op(n23) );
  buf_1 U772 ( .ip(clk), .op(n24) );
  buf_1 U773 ( .ip(clk), .op(n25) );
  buf_1 U774 ( .ip(clk), .op(n26) );
  buf_1 U775 ( .ip(clk), .op(n27) );
  buf_1 U776 ( .ip(clk), .op(n28) );
  buf_1 U777 ( .ip(clk), .op(n29) );
  buf_1 U778 ( .ip(clk), .op(n30) );
  buf_1 U779 ( .ip(clk), .op(n31) );
  buf_1 U780 ( .ip(clk), .op(n32) );
  buf_1 U781 ( .ip(clk), .op(n33) );
  buf_1 U782 ( .ip(clk), .op(n34) );
  nor2_2 U783 ( .ip1(n1139), .ip2(adr[1]), .op(n37) );
  nor2_2 U784 ( .ip1(n1139), .ip2(adr[1]), .op(n38) );
  inv_1 U785 ( .ip(n37), .op(n39) );
  inv_1 U786 ( .ip(n44), .op(n40) );
  inv_1 U787 ( .ip(n45), .op(n41) );
  inv_1 U788 ( .ip(n39), .op(n42) );
  inv_1 U789 ( .ip(n39), .op(n43) );
  inv_1 U790 ( .ip(n38), .op(n44) );
  inv_1 U791 ( .ip(n38), .op(n45) );
  inv_1 U792 ( .ip(n44), .op(n46) );
  inv_1 U793 ( .ip(n44), .op(n47) );
  inv_1 U794 ( .ip(n45), .op(n48) );
  inv_1 U795 ( .ip(n45), .op(n49) );
  inv_1 U796 ( .ip(n39), .op(n50) );
  inv_1 U797 ( .ip(n44), .op(n51) );
  buf_1 U798 ( .ip(n1432), .op(n57) );
  buf_1 U799 ( .ip(n1488), .op(n62) );
  buf_1 U800 ( .ip(n1516), .op(n65) );
  buf_1 U801 ( .ip(n1549), .op(n66) );
  buf_1 U802 ( .ip(n1549), .op(n67) );
  buf_1 U803 ( .ip(n1549), .op(n68) );
  buf_1 U804 ( .ip(n1549), .op(n69) );
  buf_1 U805 ( .ip(n1549), .op(n70) );
  buf_1 U806 ( .ip(n1549), .op(n71) );
  buf_1 U807 ( .ip(n1549), .op(n72) );
  buf_1 U808 ( .ip(n1549), .op(n73) );
  buf_1 U809 ( .ip(n1549), .op(n74) );
  buf_1 U810 ( .ip(n1549), .op(n75) );
  buf_1 U811 ( .ip(n1549), .op(n76) );
  buf_1 U812 ( .ip(n1674), .op(n83) );
  buf_1 U813 ( .ip(n1675), .op(n86) );
  buf_1 U814 ( .ip(n1676), .op(n89) );
  buf_1 U815 ( .ip(n1681), .op(n92) );
  buf_1 U816 ( .ip(n1777), .op(n95) );
  nand2_2 U817 ( .ip1(n102), .ip2(dma_in_cnt[1]), .op(n103) );
  nand3_2 U818 ( .ip1(n1831), .ip2(n1830), .ip3(n103), .op(n1289) );
  nand2_2 U819 ( .ip1(dma_in_cnt[0]), .ip2(n101), .op(n104) );
  nand3_2 U821 ( .ip1(n1829), .ip2(n1828), .ip3(n104), .op(n1288) );
  nand2_2 U822 ( .ip1(n102), .ip2(dma_in_cnt[3]), .op(n105) );
  nand3_2 U823 ( .ip1(n1835), .ip2(n1834), .ip3(n105), .op(n1291) );
  nand2_2 U824 ( .ip1(n102), .ip2(dma_in_cnt[5]), .op(n106) );
  nand3_2 U825 ( .ip1(n1839), .ip2(n1838), .ip3(n106), .op(n1293) );
  nand2_2 U826 ( .ip1(n102), .ip2(dma_in_cnt[7]), .op(n107) );
  nand3_2 U827 ( .ip1(n1843), .ip2(n1842), .ip3(n107), .op(n1295) );
  nand2_2 U828 ( .ip1(n102), .ip2(dma_in_cnt[9]), .op(n108) );
  nand3_2 U829 ( .ip1(n1847), .ip2(n1846), .ip3(n108), .op(n1297) );
  nand2_2 U830 ( .ip1(n102), .ip2(dma_in_cnt[11]), .op(n109) );
  nand3_2 U831 ( .ip1(n1827), .ip2(n1826), .ip3(n109), .op(n1287) );
  nand2_2 U832 ( .ip1(n102), .ip2(dma_in_cnt[10]), .op(n110) );
  nand3_2 U833 ( .ip1(n1849), .ip2(n1848), .ip3(n110), .op(n1298) );
  nand2_2 U834 ( .ip1(n102), .ip2(dma_in_cnt[8]), .op(n111) );
  nand3_2 U835 ( .ip1(n1845), .ip2(n1844), .ip3(n111), .op(n1296) );
  nand2_2 U836 ( .ip1(n102), .ip2(dma_in_cnt[6]), .op(n112) );
  nand3_2 U837 ( .ip1(n1841), .ip2(n1840), .ip3(n112), .op(n1294) );
  nand2_2 U838 ( .ip1(n102), .ip2(dma_in_cnt[4]), .op(n113) );
  nand3_2 U839 ( .ip1(n1837), .ip2(n1836), .ip3(n113), .op(n1292) );
  nand2_2 U840 ( .ip1(n102), .ip2(dma_in_cnt[2]), .op(n114) );
  nand3_2 U841 ( .ip1(n1833), .ip2(n1832), .ip3(n114), .op(n1290) );
  ab_or_c_or_d U842 ( .ip1(n76), .ip2(buf0_orig[27]), .ip3(n1889), .ip4(n1521), 
        .op(n1183) );
  inv_2 U843 ( .ip(buf0_orig[27]), .op(n992) );
  ab_or_c_or_d U844 ( .ip1(n75), .ip2(buf0_orig[25]), .ip3(n1889), .ip4(n1523), 
        .op(n1185) );
  inv_2 U845 ( .ip(buf0_orig[25]), .op(n991) );
  ab_or_c_or_d U846 ( .ip1(n75), .ip2(buf0_orig[23]), .ip3(n1889), .ip4(n1525), 
        .op(n1187) );
  inv_2 U847 ( .ip(buf0_orig[23]), .op(n990) );
  ab_or_c_or_d U848 ( .ip1(n75), .ip2(buf0_orig[19]), .ip3(n1889), .ip4(n1529), 
        .op(n1191) );
  inv_2 U849 ( .ip(buf0_orig[19]), .op(n989) );
  inv_2 U850 ( .ip(dma_in_cnt[1]), .op(n997) );
  ab_or_c_or_d U851 ( .ip1(n75), .ip2(buf0_orig[20]), .ip3(n1889), .ip4(n1528), 
        .op(n1190) );
  ab_or_c_or_d U852 ( .ip1(n74), .ip2(buf0_orig[22]), .ip3(n1889), .ip4(n1526), 
        .op(n1188) );
  inv_2 U853 ( .ip(dma_in_cnt[3]), .op(n996) );
  ab_or_c_or_d U854 ( .ip1(n74), .ip2(buf0_orig[24]), .ip3(n1889), .ip4(n1524), 
        .op(n1186) );
  inv_2 U855 ( .ip(dma_in_cnt[5]), .op(n995) );
  ab_or_c_or_d U856 ( .ip1(n76), .ip2(buf0_orig[26]), .ip3(n1889), .ip4(n1522), 
        .op(n1184) );
  inv_2 U857 ( .ip(dma_in_cnt[7]), .op(n994) );
  ab_or_c_or_d U858 ( .ip1(n74), .ip2(buf0_orig[28]), .ip3(n1889), .ip4(n1520), 
        .op(n1182) );
  inv_2 U910 ( .ip(dma_in_cnt[9]), .op(n993) );
  ab_or_c_or_d U911 ( .ip1(n73), .ip2(buf0_orig[30]), .ip3(n1518), .ip4(n1889), 
        .op(n1180) );
  inv_2 U912 ( .ip(buf0_orig[30]), .op(n985) );
  ab_or_c_or_d U913 ( .ip1(n74), .ip2(buf0_orig[21]), .ip3(n1889), .ip4(n1527), 
        .op(n1189) );
  ab_or_c_or_d U914 ( .ip1(buf0_orig[29]), .ip2(n67), .ip3(n1889), .ip4(n1519), 
        .op(n1181) );
  nand2_2 U915 ( .ip1(n1487), .ip2(csr[15]), .op(n115) );
  nand2_2 U916 ( .ip1(n1489), .ip2(n115), .op(n1166) );
  inv_2 U917 ( .ip(csr[26]), .op(n959) );
  nor2_2 U918 ( .ip1(dma_out_cnt[11]), .ip2(dma_out_cnt[10]), .op(n117) );
  nor2_2 U919 ( .ip1(n1319), .ip2(n1320), .op(n116) );
  nand2_2 U920 ( .ip1(n117), .ip2(n116), .op(n954) );
  and2_2 U921 ( .ip1(n3), .ip2(n954), .op(N272) );
  nor2_2 U936 ( .ip1(buf0_orig[24]), .ip2(buf0_orig[23]), .op(n119) );
  nor2_2 U937 ( .ip1(buf0_orig[21]), .ip2(buf0_orig[22]), .op(n118) );
  nand2_2 U938 ( .ip1(n119), .ip2(n118), .op(n122) );
  inv_2 U939 ( .ip(buf0_orig[28]), .op(n986) );
  nor2_2 U940 ( .ip1(buf0_orig[26]), .ip2(buf0_orig[25]), .op(n120) );
  nand4_2 U941 ( .ip1(n1325), .ip2(n986), .ip3(n992), .ip4(n120), .op(n121) );
  nor2_2 U942 ( .ip1(n122), .ip2(n121), .op(n123) );
  nand2_2 U943 ( .ip1(buf0_orig[20]), .ip2(buf0_orig[19]), .op(n124) );
  nand2_2 U944 ( .ip1(n124), .ip2(n1065), .op(n711) );
  or2_2 U945 ( .ip1(buf0_orig[22]), .ip2(n711), .op(n707) );
  inv_2 U946 ( .ip(n707), .op(n125) );
  nand2_2 U947 ( .ip1(n125), .ip2(n990), .op(n709) );
  inv_2 U948 ( .ip(n709), .op(n167) );
  inv_2 U949 ( .ip(buf0_orig[24]), .op(n988) );
  nand2_2 U950 ( .ip1(n167), .ip2(n988), .op(n221) );
  inv_2 U951 ( .ip(n221), .op(n170) );
  nand2_2 U952 ( .ip1(n170), .ip2(n991), .op(n541) );
  inv_2 U953 ( .ip(n541), .op(n173) );
  inv_2 U954 ( .ip(buf0_orig[26]), .op(n987) );
  nand2_2 U955 ( .ip1(n173), .ip2(n987), .op(n191) );
  inv_2 U956 ( .ip(n191), .op(n176) );
  nand2_2 U957 ( .ip1(n176), .ip2(n992), .op(n197) );
  inv_2 U958 ( .ip(n197), .op(n179) );
  nand2_2 U959 ( .ip1(n179), .ip2(n986), .op(n203) );
  nor2_2 U960 ( .ip1(buf0_orig[29]), .ip2(n203), .op(n182) );
  xor2_2 U961 ( .ip1(buf0_orig[30]), .ip2(n182), .op(N346) );
  inv_2 U962 ( .ip(n203), .op(n185) );
  xor2_2 U963 ( .ip1(buf0_orig[29]), .ip2(n185), .op(N345) );
  nand2_2 U964 ( .ip1(n541), .ip2(buf0_orig[26]), .op(n188) );
  nand2_2 U965 ( .ip1(n191), .ip2(n188), .op(N342) );
  nand2_2 U966 ( .ip1(n191), .ip2(buf0_orig[27]), .op(n194) );
  nand2_2 U967 ( .ip1(n197), .ip2(n194), .op(N343) );
  nand2_2 U968 ( .ip1(n197), .ip2(buf0_orig[28]), .op(n200) );
  nand2_2 U969 ( .ip1(n203), .ip2(n200), .op(N344) );
  nand2_2 U970 ( .ip1(n709), .ip2(buf0_orig[24]), .op(n204) );
  nand2_2 U971 ( .ip1(n221), .ip2(n204), .op(N340) );
  nand2_2 U972 ( .ip1(n221), .ip2(buf0_orig[25]), .op(n540) );
  nand2_2 U973 ( .ip1(n541), .ip2(n540), .op(N341) );
  nand2_2 U974 ( .ip1(buf0_orig[22]), .ip2(n711), .op(n706) );
  nand2_2 U975 ( .ip1(n707), .ip2(n706), .op(N338) );
  nand2_2 U976 ( .ip1(n707), .ip2(buf0_orig[23]), .op(n708) );
  nand2_2 U977 ( .ip1(n709), .ip2(n708), .op(N339) );
  xor2_2 U978 ( .ip1(buf0_orig[20]), .ip2(buf0_orig[19]), .op(N336) );
  nand3_2 U979 ( .ip1(buf0_orig[21]), .ip2(buf0_orig[20]), .ip3(buf0_orig[19]), 
        .op(n710) );
  nand2_2 U980 ( .ip1(n711), .ip2(n710), .op(N337) );
  nand2_2 U981 ( .ip1(buf0_orig_m3[9]), .ip2(n993), .op(n934) );
  inv_2 U982 ( .ip(dma_in_cnt[8]), .op(n712) );
  nand2_2 U983 ( .ip1(buf0_orig_m3[8]), .ip2(n712), .op(n935) );
  inv_2 U984 ( .ip(dma_in_cnt[10]), .op(n713) );
  nand2_2 U985 ( .ip1(buf0_orig_m3[10]), .ip2(n713), .op(n942) );
  inv_2 U986 ( .ip(dma_in_cnt[6]), .op(n725) );
  nand2_2 U987 ( .ip1(buf0_orig_m3[6]), .ip2(n725), .op(n723) );
  nand2_2 U988 ( .ip1(buf0_orig_m3[7]), .ip2(n994), .op(n722) );
  nand3_2 U989 ( .ip1(n942), .ip2(n723), .ip3(n722), .op(n724) );
  inv_2 U990 ( .ip(buf0_orig_m3[3]), .op(n729) );
  inv_2 U991 ( .ip(dma_in_cnt[4]), .op(n730) );
  ab_or_c_or_d U992 ( .ip1(n909), .ip2(n908), .ip3(n732), .ip4(n731), .op(n924) );
  not_ab_or_c_or_d U993 ( .ip1(buf0_orig_m3[2]), .ip2(n1014), .ip3(n912), 
        .ip4(n2), .op(n922) );
  inv_2 U994 ( .ip(dma_in_cnt[0]), .op(n915) );
  nand3_2 U995 ( .ip1(n997), .ip2(n915), .ip3(buf0_orig_m3[0]), .op(n914) );
  inv_2 U996 ( .ip(buf0_orig_m3[1]), .op(n913) );
  nand2_2 U997 ( .ip1(n914), .ip2(n913), .op(n920) );
  nand2_2 U998 ( .ip1(buf0_orig_m3[0]), .ip2(n915), .op(n916) );
  nand2_2 U999 ( .ip1(n916), .ip2(dma_in_cnt[1]), .op(n919) );
  inv_2 U1000 ( .ip(buf0_orig_m3[2]), .op(n917) );
  nand2_2 U1001 ( .ip1(dma_in_cnt[2]), .ip2(n917), .op(n918) );
  nand3_2 U1002 ( .ip1(n920), .ip2(n919), .ip3(n918), .op(n921) );
  inv_2 U1003 ( .ip(buf0_orig_m3[10]), .op(n931) );
  inv_2 U1004 ( .ip(buf0_orig_m3[11]), .op(n928) );
  nand2_2 U1005 ( .ip1(dma_in_cnt[11]), .ip2(n928), .op(n943) );
  inv_2 U1006 ( .ip(n943), .op(n930) );
  nor2_2 U1007 ( .ip1(buf0_orig_m3[9]), .ip2(n993), .op(n929) );
  not_ab_or_c_or_d U1008 ( .ip1(dma_in_cnt[10]), .ip2(n931), .ip3(n930), .ip4(
        n929), .op(n941) );
  inv_2 U1009 ( .ip(buf0_orig_m3[8]), .op(n932) );
  nand2_2 U1010 ( .ip1(dma_in_cnt[8]), .ip2(n932), .op(n939) );
  inv_2 U1011 ( .ip(buf0_orig_m3[7]), .op(n933) );
  nand2_2 U1012 ( .ip1(dma_in_cnt[7]), .ip2(n933), .op(n938) );
  inv_2 U1013 ( .ip(n934), .op(n937) );
  inv_2 U1014 ( .ip(n935), .op(n936) );
  ab_or_c_or_d U1015 ( .ip1(n939), .ip2(n938), .ip3(n937), .ip4(n936), .op(
        n940) );
  inv_2 U1016 ( .ip(n942), .op(n944) );
  nand2_2 U1017 ( .ip1(n944), .ip2(n943), .op(n945) );
  inv_2 U1018 ( .ip(dma_in_cnt[11]), .op(n960) );
  inv_2 U1019 ( .ip(dma_ack_clr1), .op(n949) );
  inv_2 U1020 ( .ip(n1889), .op(n950) );
  and2_2 U1021 ( .ip1(n951), .ip2(n950), .op(n1286) );
  inv_2 U1022 ( .ip(r2), .op(n968) );
  nor2_2 U1023 ( .ip1(r4), .ip2(n968), .op(n952) );
  nor2_2 U1024 ( .ip1(r1), .ip2(n952), .op(n953) );
  nor2_2 U1025 ( .ip1(n1889), .ip2(n953), .op(n1311) );
  inv_2 U1026 ( .ip(n954), .op(n957) );
  inv_2 U1027 ( .ip(dma_out_cnt[1]), .op(n956) );
  inv_2 U1028 ( .ip(dma_out_cnt[0]), .op(n955) );
  nand3_2 U1029 ( .ip1(n957), .ip2(n956), .ip3(n955), .op(n958) );
  nand2_2 U1030 ( .ip1(n3), .ip2(n958), .op(n967) );
  nor2_2 U1031 ( .ip1(csr[27]), .ip2(n959), .op(n963) );
  nand2_2 U1032 ( .ip1(buf0_orig[30]), .ip2(n960), .op(n961) );
  nand2_2 U1033 ( .ip1(n1064), .ip2(n961), .op(n962) );
  nand2_2 U1034 ( .ip1(n963), .ip2(n962), .op(n966) );
  or2_2 U1035 ( .ip1(r2), .ip2(r4), .op(n965) );
  inv_2 U1036 ( .ip(r5), .op(n984) );
  nand2_2 U1037 ( .ip1(csr[15]), .ip2(n984), .op(n964) );
  not_ab_or_c_or_d U1038 ( .ip1(n967), .ip2(n966), .ip3(n965), .ip4(n964), 
        .op(N361) );
  inv_2 U1039 ( .ip(dma_req_out_hold), .op(n969) );
  mux2_1 U1040 ( .ip1(n970), .ip2(n969), .s(n3), .op(n971) );
  ab_or_c_or_d U1041 ( .ip1(int__19), .ip2(n1315), .ip3(n1316), .ip4(n1317), 
        .op(n978) );
  nand2_2 U1042 ( .ip1(int__17), .ip2(int_[1]), .op(n976) );
  nand2_2 U1043 ( .ip1(int__16), .ip2(int_[0]), .op(n975) );
  nand2_2 U1044 ( .ip1(int__21), .ip2(int_[6]), .op(n974) );
  nand3_2 U1045 ( .ip1(n976), .ip2(n975), .ip3(n974), .op(n977) );
  or2_2 U1046 ( .ip1(n978), .ip2(n977), .op(N222) );
  ab_or_c_or_d U1047 ( .ip1(int__27), .ip2(n1315), .ip3(n1313), .ip4(n1314), 
        .op(n983) );
  nand2_2 U1048 ( .ip1(int__25), .ip2(int_[1]), .op(n981) );
  nand2_2 U1049 ( .ip1(int__24), .ip2(int_[0]), .op(n980) );
  nand2_2 U1050 ( .ip1(int__29), .ip2(int_[6]), .op(n979) );
  nand3_2 U1051 ( .ip1(n981), .ip2(n980), .ip3(n979), .op(n982) );
  or2_2 U1052 ( .ip1(n983), .ip2(n982), .op(N221) );
  inv_2 U1053 ( .ip(csr[15]), .op(n1070) );
  nor2_1 U1054 ( .ip1(csr[8]), .ip2(n725), .op(n1009) );
  nor2_1 U1055 ( .ip1(csr[6]), .ip2(n730), .op(n1005) );
  nor2_1 U1056 ( .ip1(csr[4]), .ip2(n1014), .op(n1001) );
  not_ab_or_c_or_d U1057 ( .ip1(dma_in_cnt[1]), .ip2(n1039), .ip3(n1015), 
        .ip4(dma_in_cnt[0]), .op(n999) );
  nor2_1 U1058 ( .ip1(dma_in_cnt[1]), .ip2(n1039), .op(n998) );
  not_ab_or_c_or_d U1059 ( .ip1(csr[4]), .ip2(n1014), .ip3(n999), .ip4(n998), 
        .op(n1000) );
  not_ab_or_c_or_d U1060 ( .ip1(dma_in_cnt[3]), .ip2(n1016), .ip3(n1001), 
        .ip4(n1000), .op(n1003) );
  nor2_1 U1061 ( .ip1(dma_in_cnt[3]), .ip2(n1016), .op(n1002) );
  not_ab_or_c_or_d U1062 ( .ip1(csr[6]), .ip2(n730), .ip3(n1003), .ip4(n1002), 
        .op(n1004) );
  not_ab_or_c_or_d U1063 ( .ip1(dma_in_cnt[5]), .ip2(n1017), .ip3(n1005), 
        .ip4(n1004), .op(n1007) );
  nor2_1 U1064 ( .ip1(dma_in_cnt[5]), .ip2(n1017), .op(n1006) );
  not_ab_or_c_or_d U1065 ( .ip1(csr[8]), .ip2(n725), .ip3(n1007), .ip4(n1006), 
        .op(n1008) );
  not_ab_or_c_or_d U1066 ( .ip1(dma_in_cnt[7]), .ip2(n1018), .ip3(n1009), 
        .ip4(n1008), .op(n1011) );
  nor2_1 U1067 ( .ip1(dma_in_cnt[7]), .ip2(n1018), .op(n1010) );
  not_ab_or_c_or_d U1068 ( .ip1(csr[10]), .ip2(n712), .ip3(n1011), .ip4(n1010), 
        .op(n1013) );
  or3_1 U1069 ( .ip1(dma_in_cnt[9]), .ip2(dma_in_cnt[11]), .ip3(dma_in_cnt[10]), .op(n1012) );
  ab_or_c_or_d U1070 ( .ip1(dma_in_cnt[8]), .ip2(n1068), .ip3(n1013), .ip4(
        n1012), .op(N319) );
  inv_2 U1071 ( .ip(dma_in_cnt[2]), .op(n1014) );
  inv_2 U1072 ( .ip(csr[2]), .op(n1015) );
  inv_2 U1073 ( .ip(csr[5]), .op(n1016) );
  inv_2 U1074 ( .ip(csr[7]), .op(n1017) );
  inv_2 U1075 ( .ip(csr[9]), .op(n1018) );
  nor2_1 U1076 ( .ip1(csr[8]), .ip2(n1036), .op(n1030) );
  nor2_1 U1077 ( .ip1(csr[6]), .ip2(n1037), .op(n1026) );
  nor2_1 U1078 ( .ip1(csr[4]), .ip2(n1038), .op(n1022) );
  not_ab_or_c_or_d U1079 ( .ip1(dma_out_left[1]), .ip2(n1039), .ip3(n1015), 
        .ip4(dma_out_left[0]), .op(n1020) );
  nor2_1 U1080 ( .ip1(dma_out_left[1]), .ip2(n1039), .op(n1019) );
  not_ab_or_c_or_d U1081 ( .ip1(csr[4]), .ip2(n1038), .ip3(n1020), .ip4(n1019), 
        .op(n1021) );
  not_ab_or_c_or_d U1082 ( .ip1(dma_out_left[3]), .ip2(n1016), .ip3(n1022), 
        .ip4(n1021), .op(n1024) );
  nor2_1 U1083 ( .ip1(dma_out_left[3]), .ip2(n1016), .op(n1023) );
  not_ab_or_c_or_d U1084 ( .ip1(csr[6]), .ip2(n1037), .ip3(n1024), .ip4(n1023), 
        .op(n1025) );
  not_ab_or_c_or_d U1085 ( .ip1(dma_out_left[5]), .ip2(n1017), .ip3(n1026), 
        .ip4(n1025), .op(n1028) );
  nor2_1 U1086 ( .ip1(dma_out_left[5]), .ip2(n1017), .op(n1027) );
  not_ab_or_c_or_d U1087 ( .ip1(csr[8]), .ip2(n1036), .ip3(n1028), .ip4(n1027), 
        .op(n1029) );
  not_ab_or_c_or_d U1088 ( .ip1(dma_out_left[7]), .ip2(n1018), .ip3(n1030), 
        .ip4(n1029), .op(n1032) );
  nor2_1 U1089 ( .ip1(dma_out_left[7]), .ip2(n1018), .op(n1031) );
  not_ab_or_c_or_d U1090 ( .ip1(csr[10]), .ip2(n1035), .ip3(n1032), .ip4(n1031), .op(n1034) );
  or3_1 U1091 ( .ip1(dma_out_left[9]), .ip2(dma_out_left[11]), .ip3(
        dma_out_left[10]), .op(n1033) );
  ab_or_c_or_d U1092 ( .ip1(dma_out_left[8]), .ip2(n1068), .ip3(n1034), .ip4(
        n1033), .op(N333) );
  inv_2 U1093 ( .ip(dma_out_left[8]), .op(n1035) );
  inv_2 U1094 ( .ip(dma_out_left[6]), .op(n1036) );
  inv_2 U1095 ( .ip(dma_out_left[4]), .op(n1037) );
  inv_2 U1096 ( .ip(dma_out_left[2]), .op(n1038) );
  inv_2 U1097 ( .ip(csr[3]), .op(n1039) );
  nor2_1 U1098 ( .ip1(dma_in_cnt[10]), .ip2(n1066), .op(n1061) );
  nor2_1 U1099 ( .ip1(dma_in_cnt[8]), .ip2(n992), .op(n1057) );
  nor2_1 U1100 ( .ip1(dma_in_cnt[6]), .ip2(n991), .op(n1053) );
  nor2_1 U1101 ( .ip1(dma_in_cnt[4]), .ip2(n990), .op(n1049) );
  nor2_1 U1102 ( .ip1(dma_in_cnt[2]), .ip2(n1065), .op(n1045) );
  nor2_1 U1103 ( .ip1(n989), .ip2(dma_in_cnt[0]), .op(n1041) );
  and2_1 U1104 ( .ip1(n997), .ip2(n1041), .op(n1040) );
  nor2_1 U1105 ( .ip1(buf0_orig[20]), .ip2(n1040), .op(n1043) );
  nor2_1 U1106 ( .ip1(n1041), .ip2(n997), .op(n1042) );
  not_ab_or_c_or_d U1107 ( .ip1(dma_in_cnt[2]), .ip2(n1065), .ip3(n1043), 
        .ip4(n1042), .op(n1044) );
  not_ab_or_c_or_d U1108 ( .ip1(buf0_orig[22]), .ip2(n996), .ip3(n1045), .ip4(
        n1044), .op(n1047) );
  nor2_1 U1109 ( .ip1(buf0_orig[22]), .ip2(n996), .op(n1046) );
  not_ab_or_c_or_d U1110 ( .ip1(dma_in_cnt[4]), .ip2(n990), .ip3(n1047), .ip4(
        n1046), .op(n1048) );
  not_ab_or_c_or_d U1111 ( .ip1(buf0_orig[24]), .ip2(n995), .ip3(n1049), .ip4(
        n1048), .op(n1051) );
  nor2_1 U1112 ( .ip1(buf0_orig[24]), .ip2(n995), .op(n1050) );
  not_ab_or_c_or_d U1113 ( .ip1(dma_in_cnt[6]), .ip2(n991), .ip3(n1051), .ip4(
        n1050), .op(n1052) );
  not_ab_or_c_or_d U1114 ( .ip1(buf0_orig[26]), .ip2(n994), .ip3(n1053), .ip4(
        n1052), .op(n1055) );
  nor2_1 U1115 ( .ip1(buf0_orig[26]), .ip2(n994), .op(n1054) );
  not_ab_or_c_or_d U1116 ( .ip1(dma_in_cnt[8]), .ip2(n992), .ip3(n1055), .ip4(
        n1054), .op(n1056) );
  not_ab_or_c_or_d U1117 ( .ip1(buf0_orig[28]), .ip2(n993), .ip3(n1057), .ip4(
        n1056), .op(n1059) );
  nor2_1 U1118 ( .ip1(buf0_orig[28]), .ip2(n993), .op(n1058) );
  not_ab_or_c_or_d U1119 ( .ip1(dma_in_cnt[10]), .ip2(n1066), .ip3(n1059), 
        .ip4(n1058), .op(n1060) );
  or2_1 U1120 ( .ip1(n1061), .ip2(n1060), .op(n1063) );
  nand2_1 U1121 ( .ip1(dma_in_cnt[11]), .ip2(n985), .op(n1062) );
  nand2_1 U1122 ( .ip1(n1063), .ip2(n1062), .op(n1064) );
  inv_2 U1123 ( .ip(buf0_orig[21]), .op(n1065) );
  inv_2 U1124 ( .ip(buf0_orig[29]), .op(n1066) );
  inv_2 U1125 ( .ip(n1472), .op(n1067) );
  inv_2 U1126 ( .ip(csr[10]), .op(n1068) );
  inv_2 U1127 ( .ip(csr[0]), .op(n1069) );
  inv_2 U1128 ( .ip(buf0_orig[31]), .op(n1071) );
  inv_2 U1129 ( .ip(buf0_orig[22]), .op(n1072) );
  inv_2 U1130 ( .ip(buf0_orig[20]), .op(n1073) );
  inv_2 U1131 ( .ip(buf0_orig[18]), .op(n1074) );
  inv_2 U1132 ( .ip(buf0_orig[17]), .op(n1075) );
  inv_2 U1133 ( .ip(buf0_orig[16]), .op(n1076) );
  inv_2 U1134 ( .ip(buf0_orig[15]), .op(n1077) );
  inv_2 U1135 ( .ip(buf0_orig[14]), .op(n1078) );
  inv_2 U1136 ( .ip(buf0_orig[13]), .op(n1079) );
  inv_2 U1137 ( .ip(buf0_orig[12]), .op(n1080) );
  inv_2 U1138 ( .ip(buf0_orig[11]), .op(n1081) );
  inv_2 U1139 ( .ip(buf0_orig[10]), .op(n1082) );
  inv_2 U1140 ( .ip(buf0_orig[9]), .op(n1083) );
  inv_2 U1141 ( .ip(buf0_orig[8]), .op(n1084) );
  inv_2 U1142 ( .ip(buf0_orig[7]), .op(n1085) );
  inv_2 U1143 ( .ip(buf0_orig[6]), .op(n1086) );
  inv_2 U1144 ( .ip(buf0_orig[5]), .op(n1087) );
  inv_2 U1145 ( .ip(buf0_orig[4]), .op(n1088) );
  inv_2 U1146 ( .ip(buf0_orig[3]), .op(n1089) );
  inv_2 U1147 ( .ip(buf0_orig[2]), .op(n1090) );
  inv_2 U1148 ( .ip(buf0_orig[1]), .op(n1091) );
  inv_2 U1149 ( .ip(buf0_orig[0]), .op(n1092) );
  inv_2 U1150 ( .ip(n1783), .op(n1093) );
  inv_2 U1151 ( .ip(n1787), .op(n1094) );
  inv_2 U1152 ( .ip(n1799), .op(n1095) );
  inv_2 U1153 ( .ip(n1803), .op(n1096) );
  inv_2 U1154 ( .ip(n1808), .op(n1097) );
  inv_2 U1155 ( .ip(ep_match_r), .op(n1098) );
  inv_2 U1156 ( .ip(n1814), .op(n1099) );
  inv_2 U1157 ( .ip(n1820), .op(n1100) );
  inv_2 U1158 ( .ip(int_[4]), .op(n1101) );
  inv_2 U1159 ( .ip(int_[3]), .op(n1102) );
  inv_2 U1160 ( .ip(buf0_rl), .op(n1103) );
  inv_2 U1161 ( .ip(n1824), .op(n1104) );
  inv_2 U1162 ( .ip(idin[30]), .op(n1105) );
  inv_2 U1163 ( .ip(idin[29]), .op(n1106) );
  inv_2 U1164 ( .ip(idin[28]), .op(n1107) );
  inv_2 U1165 ( .ip(idin[3]), .op(n1108) );
  inv_2 U1166 ( .ip(idin[2]), .op(n1109) );
  inv_2 U1167 ( .ip(idin[16]), .op(n1110) );
  inv_2 U1168 ( .ip(idin[15]), .op(n1111) );
  inv_2 U1169 ( .ip(idin[14]), .op(n1112) );
  inv_2 U1170 ( .ip(idin[13]), .op(n1113) );
  inv_2 U1171 ( .ip(idin[12]), .op(n1114) );
  inv_2 U1172 ( .ip(idin[11]), .op(n1115) );
  inv_2 U1173 ( .ip(idin[10]), .op(n1116) );
  inv_2 U1174 ( .ip(idin[9]), .op(n1117) );
  inv_2 U1175 ( .ip(idin[8]), .op(n1118) );
  inv_2 U1176 ( .ip(idin[7]), .op(n1119) );
  inv_2 U1177 ( .ip(idin[6]), .op(n1120) );
  inv_2 U1178 ( .ip(idin[5]), .op(n1121) );
  inv_2 U1179 ( .ip(idin[4]), .op(n1122) );
  inv_2 U1180 ( .ip(idin[31]), .op(n1123) );
  inv_2 U1181 ( .ip(idin[1]), .op(n1124) );
  inv_2 U1182 ( .ip(idin[0]), .op(n1125) );
  inv_2 U1183 ( .ip(idin[27]), .op(n1126) );
  inv_2 U1184 ( .ip(idin[26]), .op(n1127) );
  inv_2 U1185 ( .ip(idin[25]), .op(n1128) );
  inv_2 U1186 ( .ip(idin[24]), .op(n1129) );
  inv_2 U1187 ( .ip(idin[23]), .op(n1130) );
  inv_2 U1188 ( .ip(idin[22]), .op(n1131) );
  inv_2 U1189 ( .ip(idin[21]), .op(n1132) );
  inv_2 U1190 ( .ip(idin[20]), .op(n1133) );
  inv_2 U1191 ( .ip(idin[19]), .op(n1134) );
  inv_2 U1192 ( .ip(idin[18]), .op(n1135) );
  inv_2 U1193 ( .ip(idin[17]), .op(n1136) );
  inv_2 U1194 ( .ip(n1682), .op(n1137) );
  inv_2 U1195 ( .ip(adr[1]), .op(n1138) );
  inv_2 U1196 ( .ip(adr[0]), .op(n1139) );
endmodule


module usbf_ep_rf_2_DW01_sub_1 ( A, B, CI, DIFF, CO );
  input [11:0] A;
  input [11:0] B;
  output [11:0] DIFF;
  input CI;
  output CO;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13;
  wire   [12:0] carry;

  fulladder U2_11 ( .a(A[11]), .b(n2), .ci(carry[11]), .s(DIFF[11]) );
  fulladder U2_10 ( .a(A[10]), .b(n3), .ci(carry[10]), .co(carry[11]), .s(
        DIFF[10]) );
  fulladder U2_9 ( .a(A[9]), .b(n13), .ci(carry[9]), .co(carry[10]), .s(
        DIFF[9]) );
  fulladder U2_8 ( .a(A[8]), .b(n12), .ci(carry[8]), .co(carry[9]), .s(DIFF[8]) );
  fulladder U2_7 ( .a(A[7]), .b(n11), .ci(carry[7]), .co(carry[8]), .s(DIFF[7]) );
  fulladder U2_6 ( .a(A[6]), .b(n10), .ci(carry[6]), .co(carry[7]), .s(DIFF[6]) );
  fulladder U2_5 ( .a(A[5]), .b(n9), .ci(carry[5]), .co(carry[6]), .s(DIFF[5])
         );
  fulladder U2_4 ( .a(A[4]), .b(n8), .ci(carry[4]), .co(carry[5]), .s(DIFF[4])
         );
  fulladder U2_3 ( .a(A[3]), .b(n7), .ci(carry[3]), .co(carry[4]), .s(DIFF[3])
         );
  fulladder U2_2 ( .a(A[2]), .b(n6), .ci(carry[2]), .co(carry[3]), .s(DIFF[2])
         );
  fulladder U2_1 ( .a(A[1]), .b(n5), .ci(n1), .co(carry[2]), .s(DIFF[1]) );
  or2_2 U1 ( .ip1(A[0]), .ip2(n4), .op(n1) );
  xnor2_1 U2 ( .ip1(n4), .ip2(A[0]), .op(DIFF[0]) );
  inv_2 U3 ( .ip(B[11]), .op(n2) );
  inv_2 U4 ( .ip(B[10]), .op(n3) );
  inv_2 U5 ( .ip(B[0]), .op(n4) );
  inv_2 U6 ( .ip(B[1]), .op(n5) );
  inv_2 U7 ( .ip(B[2]), .op(n6) );
  inv_2 U8 ( .ip(B[3]), .op(n7) );
  inv_2 U9 ( .ip(B[4]), .op(n8) );
  inv_2 U10 ( .ip(B[5]), .op(n9) );
  inv_2 U11 ( .ip(B[6]), .op(n10) );
  inv_2 U12 ( .ip(B[7]), .op(n11) );
  inv_2 U13 ( .ip(B[8]), .op(n12) );
  inv_2 U14 ( .ip(B[9]), .op(n13) );
endmodule


module usbf_ep_rf_2_DW01_sub_2 ( A, B, CI, DIFF, CO );
  input [11:0] A;
  input [11:0] B;
  output [11:0] DIFF;
  input CI;
  output CO;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12;
  wire   [12:0] carry;

  fulladder U2_8 ( .a(A[8]), .b(n4), .ci(carry[8]), .co(carry[9]), .s(DIFF[8])
         );
  fulladder U2_7 ( .a(A[7]), .b(n5), .ci(carry[7]), .co(carry[8]), .s(DIFF[7])
         );
  fulladder U2_6 ( .a(A[6]), .b(n6), .ci(carry[6]), .co(carry[7]), .s(DIFF[6])
         );
  fulladder U2_5 ( .a(A[5]), .b(n7), .ci(carry[5]), .co(carry[6]), .s(DIFF[5])
         );
  fulladder U2_4 ( .a(A[4]), .b(n8), .ci(carry[4]), .co(carry[5]), .s(DIFF[4])
         );
  fulladder U2_3 ( .a(A[3]), .b(n9), .ci(carry[3]), .co(carry[4]), .s(DIFF[3])
         );
  fulladder U2_2 ( .a(A[2]), .b(n10), .ci(carry[2]), .co(carry[3]), .s(DIFF[2]) );
  fulladder U2_1 ( .a(A[1]), .b(n11), .ci(n2), .co(carry[2]), .s(DIFF[1]) );
  or2_2 U1 ( .ip1(carry[9]), .ip2(A[9]), .op(n1) );
  or2_2 U2 ( .ip1(A[0]), .ip2(n12), .op(n2) );
  nor2_2 U3 ( .ip1(n1), .ip2(A[10]), .op(n3) );
  xnor2_1 U4 ( .ip1(A[10]), .ip2(n1), .op(DIFF[10]) );
  xor2_2 U5 ( .ip1(A[11]), .ip2(n3), .op(DIFF[11]) );
  xnor2_1 U6 ( .ip1(A[9]), .ip2(carry[9]), .op(DIFF[9]) );
  xnor2_1 U7 ( .ip1(n12), .ip2(A[0]), .op(DIFF[0]) );
  inv_2 U8 ( .ip(B[8]), .op(n4) );
  inv_2 U9 ( .ip(B[7]), .op(n5) );
  inv_2 U10 ( .ip(B[6]), .op(n6) );
  inv_2 U11 ( .ip(B[5]), .op(n7) );
  inv_2 U12 ( .ip(B[4]), .op(n8) );
  inv_2 U13 ( .ip(B[3]), .op(n9) );
  inv_2 U14 ( .ip(B[2]), .op(n10) );
  inv_2 U15 ( .ip(B[1]), .op(n11) );
  inv_2 U16 ( .ip(B[0]), .op(n12) );
endmodule


module usbf_ep_rf_2_DW01_inc_0 ( A, SUM );
  input [11:0] A;
  output [11:0] SUM;
  wire   n1, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16;

  inv_2 U1 ( .ip(A[10]), .op(n1) );
  inv_2 U2 ( .ip(A[0]), .op(SUM[0]) );
  inv_2 U3 ( .ip(A[3]), .op(n3) );
  inv_2 U4 ( .ip(A[4]), .op(n4) );
  inv_2 U5 ( .ip(A[7]), .op(n5) );
  inv_2 U6 ( .ip(A[8]), .op(n6) );
  xor2_1 U7 ( .ip1(A[9]), .ip2(n7), .op(SUM[9]) );
  xor2_1 U8 ( .ip1(A[8]), .ip2(n8), .op(SUM[8]) );
  nor2_1 U9 ( .ip1(n9), .ip2(n5), .op(n8) );
  xor2_1 U10 ( .ip1(n9), .ip2(n5), .op(SUM[7]) );
  xnor2_1 U11 ( .ip1(n10), .ip2(A[6]), .op(SUM[6]) );
  nand2_1 U12 ( .ip1(A[5]), .ip2(n11), .op(n10) );
  xor2_1 U13 ( .ip1(A[5]), .ip2(n11), .op(SUM[5]) );
  xor2_1 U14 ( .ip1(A[4]), .ip2(n12), .op(SUM[4]) );
  nor2_1 U15 ( .ip1(n13), .ip2(n3), .op(n12) );
  xor2_1 U16 ( .ip1(n13), .ip2(n3), .op(SUM[3]) );
  xnor2_1 U17 ( .ip1(n14), .ip2(A[2]), .op(SUM[2]) );
  nand2_1 U18 ( .ip1(A[1]), .ip2(A[0]), .op(n14) );
  xor2_1 U19 ( .ip1(A[1]), .ip2(A[0]), .op(SUM[1]) );
  xor2_1 U20 ( .ip1(A[11]), .ip2(n15), .op(SUM[11]) );
  nor2_1 U21 ( .ip1(n16), .ip2(n1), .op(n15) );
  xor2_1 U22 ( .ip1(n16), .ip2(n1), .op(SUM[10]) );
  nand2_1 U23 ( .ip1(n7), .ip2(A[9]), .op(n16) );
  nor3_1 U24 ( .ip1(n5), .ip2(n9), .ip3(n6), .op(n7) );
  nand3_1 U25 ( .ip1(A[5]), .ip2(n11), .ip3(A[6]), .op(n9) );
  nor3_1 U26 ( .ip1(n3), .ip2(n13), .ip3(n4), .op(n11) );
  nand3_1 U27 ( .ip1(A[1]), .ip2(A[0]), .ip3(A[2]), .op(n13) );
endmodule


module usbf_ep_rf_2_DW01_add_0 ( A, B, CI, SUM, CO );
  input [11:0] A;
  input [11:0] B;
  output [11:0] SUM;
  input CI;
  output CO;
  wire   n1, n2, n3;
  wire   [11:1] carry;

  fulladder U1_8 ( .a(A[8]), .b(B[8]), .ci(carry[8]), .co(carry[9]), .s(SUM[8]) );
  fulladder U1_7 ( .a(A[7]), .b(B[7]), .ci(carry[7]), .co(carry[8]), .s(SUM[7]) );
  fulladder U1_6 ( .a(A[6]), .b(B[6]), .ci(carry[6]), .co(carry[7]), .s(SUM[6]) );
  fulladder U1_5 ( .a(A[5]), .b(B[5]), .ci(carry[5]), .co(carry[6]), .s(SUM[5]) );
  fulladder U1_4 ( .a(A[4]), .b(B[4]), .ci(carry[4]), .co(carry[5]), .s(SUM[4]) );
  fulladder U1_3 ( .a(A[3]), .b(B[3]), .ci(carry[3]), .co(carry[4]), .s(SUM[3]) );
  fulladder U1_2 ( .a(A[2]), .b(B[2]), .ci(carry[2]), .co(carry[3]), .s(SUM[2]) );
  fulladder U1_1 ( .a(A[1]), .b(B[1]), .ci(n2), .co(carry[2]), .s(SUM[1]) );
  and2_2 U1 ( .ip1(A[9]), .ip2(carry[9]), .op(n1) );
  and2_2 U2 ( .ip1(B[0]), .ip2(A[0]), .op(n2) );
  and2_2 U3 ( .ip1(A[10]), .ip2(n1), .op(n3) );
  xor2_2 U4 ( .ip1(A[11]), .ip2(n3), .op(SUM[11]) );
  xor2_2 U5 ( .ip1(A[10]), .ip2(n1), .op(SUM[10]) );
  xor2_2 U6 ( .ip1(A[9]), .ip2(carry[9]), .op(SUM[9]) );
  xor2_2 U7 ( .ip1(B[0]), .ip2(A[0]), .op(SUM[0]) );
endmodule


module usbf_ep_rf_2_DW01_dec_0 ( A, SUM );
  input [11:0] A;
  output [11:0] SUM;
  wire   n1, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21;

  inv_2 U1 ( .ip(n21), .op(n1) );
  inv_2 U2 ( .ip(A[0]), .op(SUM[0]) );
  nand2_1 U3 ( .ip1(n1), .ip2(n3), .op(SUM[9]) );
  nand2_1 U4 ( .ip1(A[9]), .ip2(n4), .op(n3) );
  nand2_1 U5 ( .ip1(n4), .ip2(n5), .op(SUM[8]) );
  nand2_1 U6 ( .ip1(A[8]), .ip2(n6), .op(n5) );
  nand2_1 U7 ( .ip1(n6), .ip2(n7), .op(SUM[7]) );
  nand2_1 U8 ( .ip1(A[7]), .ip2(n8), .op(n7) );
  nand2_1 U9 ( .ip1(n8), .ip2(n9), .op(SUM[6]) );
  nand2_1 U10 ( .ip1(A[6]), .ip2(n10), .op(n9) );
  nand2_1 U11 ( .ip1(n10), .ip2(n11), .op(SUM[5]) );
  nand2_1 U12 ( .ip1(A[5]), .ip2(n12), .op(n11) );
  nand2_1 U13 ( .ip1(n12), .ip2(n13), .op(SUM[4]) );
  nand2_1 U14 ( .ip1(A[4]), .ip2(n14), .op(n13) );
  nand2_1 U15 ( .ip1(n14), .ip2(n15), .op(SUM[3]) );
  nand2_1 U16 ( .ip1(A[3]), .ip2(n16), .op(n15) );
  nand2_1 U17 ( .ip1(n16), .ip2(n17), .op(SUM[2]) );
  nand2_1 U18 ( .ip1(A[2]), .ip2(n18), .op(n17) );
  nand2_1 U19 ( .ip1(n18), .ip2(n19), .op(SUM[1]) );
  nand2_1 U20 ( .ip1(A[1]), .ip2(A[0]), .op(n19) );
  xor2_1 U21 ( .ip1(A[11]), .ip2(n20), .op(SUM[11]) );
  nor2_1 U22 ( .ip1(A[10]), .ip2(n1), .op(n20) );
  xor2_1 U23 ( .ip1(A[10]), .ip2(n21), .op(SUM[10]) );
  nor2_1 U24 ( .ip1(n4), .ip2(A[9]), .op(n21) );
  or2_1 U25 ( .ip1(n6), .ip2(A[8]), .op(n4) );
  or2_1 U26 ( .ip1(n8), .ip2(A[7]), .op(n6) );
  or2_1 U27 ( .ip1(n10), .ip2(A[6]), .op(n8) );
  or2_1 U28 ( .ip1(n12), .ip2(A[5]), .op(n10) );
  or2_1 U29 ( .ip1(n14), .ip2(A[4]), .op(n12) );
  or2_1 U30 ( .ip1(n16), .ip2(A[3]), .op(n14) );
  or2_1 U31 ( .ip1(n18), .ip2(A[2]), .op(n16) );
  or2_1 U32 ( .ip1(A[1]), .ip2(A[0]), .op(n18) );
endmodule


module usbf_ep_rf_2 ( clk, wclk, rst, adr, re, we, din, dout, inta, intb, 
        dma_req, dma_ack, idin, ep_sel, ep_match, buf0_rl, buf0_set, buf1_set, 
        uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, 
        int_crc16_set, int_to_set, int_seqerr_set, out_to_small, csr, buf0, 
        buf1, dma_in_buf_sz1, dma_out_buf_avail );
  input [1:0] adr;
  input [31:0] din;
  output [31:0] dout;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  input clk, wclk, rst, re, we, dma_ack, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small;
  output inta, intb, dma_req, ep_match, dma_in_buf_sz1, dma_out_buf_avail;
  wire   int__29, int__28, int__27, int__26, int__25, int__24, int__21,
         int__20, int__19, int__18, int__17, int__16, ep_match_r, N191, int_re,
         N221, N222, set_r, N229, N230, N231, N232, N233, N234, N235, N236,
         N237, N238, N239, N240, N242, N243, N244, N245, N246, N247, N248,
         N249, N250, N251, N252, N253, N271, N272, dma_req_out_hold, N278,
         N279, N280, N281, N282, N283, N284, N285, N286, N287, N288, N289,
         N291, N292, N293, N294, N295, N296, N297, N298, N299, N300, N301,
         N302, N319, N320, N321, N322, N323, N324, N325, N326, N327, N328,
         N329, N330, N331, N332, N333, N336, N337, N338, N339, N340, N341,
         N342, N343, N344, N345, N346, N347, dma_req_in_hold2, N348,
         dma_req_in_hold, r1, r2, r4, r5, N361, dma_ack_clr1, dma_ack_wr1, n1,
         n2, n3, n4, n5, n7, n8, n11, n12, n13, n14, n15, n16, n17, n18, n19,
         n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33,
         n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47,
         n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61,
         n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75,
         n76, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90,
         n91, n92, n93, n94, n95, n96, n97, n98, n99, n100, n101, n102, n103,
         n104, n105, n106, n107, n108, n109, n110, n111, n112, n113, n114,
         n115, n116, n117, n118, n119, n120, n121, n122, n123, n124, n125,
         n167, n170, n173, n176, n179, n182, n185, n188, n191, n194, n197,
         n200, n203, n204, n221, n540, n541, n706, n707, n708, n709, n710,
         n711, n712, n713, n722, n723, n724, n725, n729, n730, n731, n732,
         n908, n909, n910, n911, n912, n913, n914, n915, n916, n917, n918,
         n919, n920, n921, n922, n923, n924, n925, n926, n927, n928, n929,
         n930, n931, n932, n933, n934, n935, n936, n937, n938, n939, n940,
         n941, n942, n943, n944, n945, n946, n947, n948, n949, n950, n951,
         n952, n953, n954, n955, n956, n957, n958, n959, n960, n961, n962,
         n963, n964, n965, n966, n967, n968, n969, n970, n971, n972, n973,
         n974, n975, n976, n977, n978, n979, n980, n981, n982, n983, n984,
         n985, n986, n987, n988, n989, n990, n991, n992, n993, n994, n995,
         n996, n997, n998, n999, n1000, n1001, n1002, n1003, n1004, n1005,
         n1006, n1007, n1008, n1009, n1010, n1011, n1012, n1013, n1014, n1015,
         n1016, n1017, n1018, n1019, n1020, n1021, n1022, n1023, n1024, n1025,
         n1026, n1027, n1028, n1029, n1030, n1031, n1032, n1033, n1034, n1035,
         n1036, n1037, n1038, n1039, n1040, n1041, n1042, n1043, n1044, n1045,
         n1046, n1047, n1048, n1049, n1050, n1051, n1052, n1053, n1054, n1055,
         n1056, n1057, n1058, n1059, n1060, n1061, n1062, n1063, n1064, n1065,
         n1066, n1067, n1068, n1069, n1070, n1071, n1072, n1073, n1074, n1075,
         n1076, n1077, n1078, n1079, n1080, n1081, n1082, n1083, n1084, n1085,
         n1086, n1087, n1088, n1089, n1090, n1091, n1092, n1093, n1094, n1095,
         n1096, n1097, n1098, n1099, n1100, n1101, n1102, n1103, n1104, n1105,
         n1106, n1107, n1108, n1109, n1110, n1111, n1112, n1113, n1114, n1115,
         n1116, n1117, n1118, n1119, n1120, n1121, n1122, n1123, n1124, n1125,
         n1126, n1127, n1128, n1129, n1130, n1131, n1132, n1133, n1134, n1135,
         n1136, n1137, n1138, n1139, n1140, n1141, n1142, n1143, n1144, n1145,
         n1146, n1147, n1148, n1149, n1150, n1151, n1152, n1153, n1154, n1155,
         n1156, n1157, n1158, n1159, n1160, n1161, n1162, n1163, n1164, n1165,
         n1166, n1167, n1168, n1169, n1170, n1171, n1172, n1173, n1174, n1175,
         n1176, n1177, n1178, n1179, n1180, n1181, n1182, n1183, n1184, n1185,
         n1186, n1187, n1188, n1189, n1190, n1191, n1192, n1193, n1194, n1195,
         n1196, n1197, n1198, n1199, n1200, n1201, n1202, n1203, n1204, n1205,
         n1206, n1207, n1208, n1209, n1210, n1211, n1212, n1213, n1214, n1215,
         n1216, n1217, n1218, n1219, n1220, n1221, n1222, n1223, n1224, n1225,
         n1226, n1227, n1228, n1229, n1230, n1231, n1232, n1233, n1234, n1235,
         n1236, n1237, n1238, n1239, n1240, n1241, n1242, n1243, n1244, n1245,
         n1246, n1247, n1248, n1249, n1250, n1251, n1252, n1253, n1254, n1255,
         n1256, n1257, n1258, n1259, n1260, n1261, n1262, n1263, n1264, n1265,
         n1266, n1267, n1268, n1269, n1270, n1271, n1272, n1273, n1274, n1275,
         n1276, n1277, n1278, n1279, n1280, n1281, n1282, n1283, n1284, n1285,
         n1286, n1287, n1288, n1289, n1290, n1291, n1292, n1293, n1294, n1295,
         n1296, n1297, n1298, n1299, n1300, n1301, n1302, n1303, n1304, n1305,
         n1306, n1307, n1308, n1309, n1310, n1311, n1312, n1313, n1314, n1315,
         n1316, n1317, n1318, n1319, n1320, n1321, n1322, n1323, n1324, n1325,
         n1326, n1327, n1328, n1329, n1330, n1331, n1332, n1333, n1334, n1335,
         n1336, n1337, n1338, n1339, n1340, n1341, n1342, n1343, n1344, n1345,
         n1346, n1347, n1348, n1349, n1350, n1351, n1352, n1353, n1354, n1355,
         n1356, n1357, n1358, n1359, n1360, n1361, n1362, n1363, n1364, n1365,
         n1366, n1367, n1368, n1369, n1370, n1371, n1372, n1373, n1374, n1375,
         n1376, n1377, n1378, n1379, n1380, n1381, n1382, n1383, n1384, n1385,
         n1386, n1387, n1388, n1389, n1390, n1391, n1392, n1393, n1394, n1395,
         n1396, n1397, n1398, n1399, n1400, n1401, n1402, n1403, n1404, n1405,
         n1406, n1407, n1408, n1409, n1410, n1411, n1412, n1413, n1414, n1415,
         n1416, n1417, n1418, n1419, n1420, n1421, n1422, n1423, n1424, n1425,
         n1426, n1427, n1428, n1429, n1430, n1431, n1432, n1433, n1434, n1435,
         n1436, n1437, n1438, n1439, n1440, n1441, n1442, n1443, n1444, n1445,
         n1446, n1447, n1448, n1449, n1450, n1451, n1452, n1453, n1454, n1455,
         n1456, n1457, n1458, n1459, n1460, n1461, n1462, n1463, n1464, n1465,
         n1466, n1467, n1468, n1469, n1470, n1471, n1472, n1473, n1474, n1475,
         n1476, n1477, n1478, n1479, n1480, n1481, n1482, n1483, n1484, n1485,
         n1486, n1487, n1488, n1489, n1490, n1491, n1492, n1493, n1494, n1495,
         n1496, n1497, n1498, n1499, n1500, n1501, n1502, n1503, n1504, n1505,
         n1506, n1507, n1508, n1509, n1510, n1511, n1512, n1513, n1514, n1515,
         n1516, n1517, n1518, n1519, n1520, n1521, n1522, n1523, n1524, n1525,
         n1526, n1527, n1528, n1529, n1530, n1531, n1532, n1533, n1534, n1535,
         n1536, n1537, n1538, n1539, n1540, n1541, n1542, n1543, n1544, n1545,
         n1546, n1547, n1548, n1549, n1550, n1551, n1552, n1553, n1554, n1555,
         n1556, n1557, n1558, n1559, n1560, n1561, n1562, n1563, n1564, n1565,
         n1566, n1567, n1568, n1569, n1570, n1571, n1572, n1573, n1574, n1575,
         n1576, n1577, n1578, n1579, n1580, n1581, n1582, n1583, n1584, n1585,
         n1586, n1587, n1588, n1589, n1590, n1591, n1592, n1593, n1594, n1595,
         n1596, n1597, n1598, n1599, n1600, n1601, n1602, n1603, n1604, n1605,
         n1606, n1607, n1608, n1609, n1610, n1611, n1612, n1613, n1614, n1615,
         n1616, n1617, n1618, n1619, n1620, n1621, n1622, n1623, n1624, n1625,
         n1626, n1627, n1628, n1629, n1630, n1631, n1632, n1633, n1634, n1635,
         n1636, n1637, n1638, n1639, n1640, n1641, n1642, n1643, n1644, n1645,
         n1646, n1647, n1648, n1649, n1650, n1651, n1652, n1653, n1654, n1655,
         n1656, n1657, n1658, n1659, n1660, n1661, n1662, n1663, n1664, n1665,
         n1666, n1667, n1668, n1669, n1670, n1671, n1672, n1673, n1674, n1675,
         n1676, n1677, n1678, n1679, n1680, n1681, n1682, n1683, n1684, n1685,
         n1686, n1687, n1688, n1689, n1690, n1691, n1692, n1693, n1694, n1695,
         n1696, n1697, n1698, n1699, n1700, n1701, n1702, n1703, n1704, n1705,
         n1706, n1707, n1708, n1709, n1710, n1711, n1712, n1713, n1714, n1715,
         n1716, n1717, n1718, n1719, n1720, n1721, n1722, n1723, n1724, n1725,
         n1726, n1727, n1728, n1729, n1730, n1731, n1732, n1733, n1734, n1735,
         n1736, n1737, n1738, n1739, n1740, n1741, n1742, n1743, n1744, n1745,
         n1746, n1747, n1748, n1749, n1750, n1751, n1752, n1753, n1754, n1755,
         n1756, n1757, n1758, n1759, n1760, n1761, n1762, n1763, n1764, n1765,
         n1766, n1767, n1768, n1769, n1770, n1771, n1772, n1773, n1774, n1775,
         n1776, n1777, n1778, n1779, n1780, n1781, n1782, n1783, n1784, n1785,
         n1786, n1787, n1788, n1789, n1790, n1791, n1792, n1793, n1794, n1795,
         n1796, n1797, n1798, n1799, n1800, n1801, n1802, n1803, n1804, n1805,
         n1806, n1807, n1808, n1809, n1810, n1811, n1812, n1813, n1814, n1815,
         n1816, n1817, n1818, n1819, n1820, n1821, n1822, n1823, n1824, n1825,
         n1826, n1827, n1828, n1829, n1830, n1831, n1832, n1833, n1834, n1835,
         n1836, n1837, n1838, n1839, n1840, n1841, n1842, n1843, n1844, n1845,
         n1846, n1847, n1848, n1849, n1850, n1851, n1852, n1853, n1854, n1855,
         n1856, n1857, n1858, n1859, n1860, n1861, n1862, n1863, n1864, n1865,
         n1866, n1867, n1868, n1869, n1870, n1871, n1872, n1873, n1874, n1875,
         n1876, n1877, n1878, n1879, n1880, n1881, n1882, n1883, n1884, n1885,
         n1886, n1887, n1888, n1889;
  wire   [6:0] int_;
  wire   [31:0] buf0_orig;
  wire   [11:0] dma_out_cnt;
  wire   [11:0] dma_in_cnt;
  wire   [11:0] dma_out_left;
  wire   [11:0] buf0_orig_m3;
  assign csr[14] = 1'b0;

  nand3_2 U24 ( .ip1(n1888), .ip2(n1887), .ip3(n1886), .op(n1310) );
  nand2_2 U25 ( .ip1(dma_out_cnt[10]), .ip2(n101), .op(n1886) );
  nand2_2 U26 ( .ip1(N252), .ip2(n36), .op(n1887) );
  nand2_2 U27 ( .ip1(N239), .ip2(n100), .op(n1888) );
  nand3_2 U28 ( .ip1(n1882), .ip2(n1881), .ip3(n1880), .op(n1309) );
  nand2_2 U29 ( .ip1(dma_out_cnt[9]), .ip2(n101), .op(n1880) );
  nand2_2 U30 ( .ip1(N251), .ip2(n1884), .op(n1881) );
  nand2_2 U31 ( .ip1(N238), .ip2(n99), .op(n1882) );
  nand3_2 U32 ( .ip1(n1879), .ip2(n1878), .ip3(n1877), .op(n1308) );
  nand2_2 U33 ( .ip1(dma_out_cnt[8]), .ip2(n101), .op(n1877) );
  nand2_2 U34 ( .ip1(N250), .ip2(n36), .op(n1878) );
  nand2_2 U35 ( .ip1(N237), .ip2(n99), .op(n1879) );
  nand3_2 U36 ( .ip1(n1876), .ip2(n1875), .ip3(n1874), .op(n1307) );
  nand2_2 U37 ( .ip1(dma_out_cnt[7]), .ip2(n101), .op(n1874) );
  nand2_2 U38 ( .ip1(N249), .ip2(n1884), .op(n1875) );
  nand2_2 U39 ( .ip1(N236), .ip2(n99), .op(n1876) );
  nand3_2 U40 ( .ip1(n1873), .ip2(n1872), .ip3(n1871), .op(n1306) );
  nand2_2 U41 ( .ip1(dma_out_cnt[6]), .ip2(n101), .op(n1871) );
  nand2_2 U42 ( .ip1(N248), .ip2(n36), .op(n1872) );
  nand2_2 U43 ( .ip1(N235), .ip2(n99), .op(n1873) );
  nand3_2 U44 ( .ip1(n1870), .ip2(n1869), .ip3(n1868), .op(n1305) );
  nand2_2 U45 ( .ip1(dma_out_cnt[5]), .ip2(n101), .op(n1868) );
  nand2_2 U46 ( .ip1(N247), .ip2(n1884), .op(n1869) );
  nand2_2 U47 ( .ip1(N234), .ip2(n99), .op(n1870) );
  nand3_2 U48 ( .ip1(n1867), .ip2(n1866), .ip3(n1865), .op(n1304) );
  nand2_2 U49 ( .ip1(dma_out_cnt[4]), .ip2(n101), .op(n1865) );
  nand2_2 U50 ( .ip1(N246), .ip2(n36), .op(n1866) );
  nand2_2 U51 ( .ip1(N233), .ip2(n99), .op(n1867) );
  nand3_2 U52 ( .ip1(n1864), .ip2(n1863), .ip3(n1862), .op(n1303) );
  nand2_2 U53 ( .ip1(dma_out_cnt[3]), .ip2(n101), .op(n1862) );
  nand2_2 U54 ( .ip1(N245), .ip2(n1884), .op(n1863) );
  nand2_2 U55 ( .ip1(N232), .ip2(n99), .op(n1864) );
  nand3_2 U56 ( .ip1(n1861), .ip2(n1860), .ip3(n1859), .op(n1302) );
  nand2_2 U57 ( .ip1(dma_out_cnt[2]), .ip2(n101), .op(n1859) );
  nand2_2 U58 ( .ip1(N244), .ip2(n1884), .op(n1860) );
  nand2_2 U59 ( .ip1(N231), .ip2(n99), .op(n1861) );
  nand3_2 U60 ( .ip1(n1858), .ip2(n1857), .ip3(n1856), .op(n1301) );
  nand2_2 U61 ( .ip1(dma_out_cnt[1]), .ip2(n101), .op(n1856) );
  nand2_2 U62 ( .ip1(N243), .ip2(n1884), .op(n1857) );
  nand2_2 U63 ( .ip1(N230), .ip2(n99), .op(n1858) );
  nand3_2 U64 ( .ip1(n1855), .ip2(n1854), .ip3(n1853), .op(n1300) );
  nand2_2 U65 ( .ip1(dma_out_cnt[0]), .ip2(n102), .op(n1853) );
  nand2_2 U66 ( .ip1(N242), .ip2(n1884), .op(n1854) );
  nand2_2 U67 ( .ip1(N229), .ip2(n99), .op(n1855) );
  nand3_2 U68 ( .ip1(n1852), .ip2(n1851), .ip3(n1850), .op(n1299) );
  nand2_2 U69 ( .ip1(dma_out_cnt[11]), .ip2(n101), .op(n1850) );
  nand2_2 U70 ( .ip1(N253), .ip2(n1884), .op(n1851) );
  nand2_2 U71 ( .ip1(N240), .ip2(n99), .op(n1852) );
  nand2_2 U74 ( .ip1(N301), .ip2(n36), .op(n1848) );
  nand2_2 U75 ( .ip1(N288), .ip2(n100), .op(n1849) );
  nand2_2 U78 ( .ip1(N300), .ip2(n1884), .op(n1846) );
  nand2_2 U79 ( .ip1(N287), .ip2(n100), .op(n1847) );
  nand2_2 U82 ( .ip1(N299), .ip2(n36), .op(n1844) );
  nand2_2 U83 ( .ip1(N286), .ip2(n100), .op(n1845) );
  nand2_2 U86 ( .ip1(N298), .ip2(n36), .op(n1842) );
  nand2_2 U87 ( .ip1(N285), .ip2(n100), .op(n1843) );
  nand2_2 U90 ( .ip1(N297), .ip2(n36), .op(n1840) );
  nand2_2 U91 ( .ip1(N284), .ip2(n100), .op(n1841) );
  nand2_2 U94 ( .ip1(N296), .ip2(n1884), .op(n1838) );
  nand2_2 U95 ( .ip1(N283), .ip2(n100), .op(n1839) );
  nand2_2 U98 ( .ip1(N295), .ip2(n36), .op(n1836) );
  nand2_2 U99 ( .ip1(N282), .ip2(n100), .op(n1837) );
  nand2_2 U102 ( .ip1(N294), .ip2(n1884), .op(n1834) );
  nand2_2 U103 ( .ip1(N281), .ip2(n100), .op(n1835) );
  nand2_2 U106 ( .ip1(N293), .ip2(n36), .op(n1832) );
  nand2_2 U107 ( .ip1(N280), .ip2(n100), .op(n1833) );
  nand2_2 U110 ( .ip1(N292), .ip2(n36), .op(n1830) );
  nand2_2 U111 ( .ip1(N279), .ip2(n100), .op(n1831) );
  nand2_2 U114 ( .ip1(N291), .ip2(n36), .op(n1828) );
  nand2_2 U115 ( .ip1(N278), .ip2(n100), .op(n1829) );
  nand2_2 U119 ( .ip1(N302), .ip2(n36), .op(n1826) );
  nor4_2 U120 ( .ip1(n1070), .ip2(n1098), .ip3(r5), .ip4(n1825), .op(n1884) );
  nor2_2 U121 ( .ip1(set_r), .ip2(n1104), .op(n1825) );
  nand2_2 U122 ( .ip1(N289), .ip2(n99), .op(n1827) );
  nor2_2 U123 ( .ip1(n984), .ip2(n1070), .op(n1883) );
  nand2_2 U127 ( .ip1(n1823), .ip2(n1822), .op(n1285) );
  nand3_2 U128 ( .ip1(n1821), .ip2(n1820), .ip3(idin[0]), .op(n1822) );
  nand2_2 U129 ( .ip1(csr[30]), .ip2(n1100), .op(n1823) );
  nand2_2 U130 ( .ip1(n1819), .ip2(n1818), .op(n1284) );
  nand3_2 U131 ( .ip1(n1821), .ip2(n1820), .ip3(idin[1]), .op(n1818) );
  nand2_2 U132 ( .ip1(csr[31]), .ip2(n1100), .op(n1819) );
  nand2_2 U133 ( .ip1(rst), .ip2(n1817), .op(n1820) );
  nand2_2 U134 ( .ip1(uc_bsel_set), .ip2(ep_match_r), .op(n1817) );
  nand2_2 U135 ( .ip1(n1816), .ip2(n1815), .op(n1283) );
  nand3_2 U136 ( .ip1(n1821), .ip2(n1814), .ip3(idin[2]), .op(n1815) );
  nand2_2 U137 ( .ip1(csr[28]), .ip2(n1099), .op(n1816) );
  nand2_2 U138 ( .ip1(n1813), .ip2(n1812), .op(n1282) );
  nand3_2 U139 ( .ip1(n1821), .ip2(n1814), .ip3(idin[3]), .op(n1812) );
  nand2_2 U140 ( .ip1(csr[29]), .ip2(n1099), .op(n1813) );
  nand2_2 U141 ( .ip1(rst), .ip2(n1811), .op(n1814) );
  nand2_2 U142 ( .ip1(uc_dpd_set), .ip2(ep_match_r), .op(n1811) );
  nand2_2 U143 ( .ip1(n1810), .ip2(n1809), .op(n1281) );
  nand2_2 U144 ( .ip1(int_[0]), .ip2(n1097), .op(n1809) );
  nand2_2 U145 ( .ip1(n1), .ip2(n1808), .op(n1810) );
  nand2_2 U146 ( .ip1(n1807), .ip2(n1806), .op(n1808) );
  nand2_2 U147 ( .ip1(int_to_set), .ip2(n1), .op(n1806) );
  nand2_2 U148 ( .ip1(n1805), .ip2(n1804), .op(n1280) );
  nand2_2 U149 ( .ip1(int_[1]), .ip2(n1096), .op(n1804) );
  nand2_2 U150 ( .ip1(n1), .ip2(n1803), .op(n1805) );
  nand2_2 U151 ( .ip1(n1807), .ip2(n1802), .op(n1803) );
  nand2_2 U152 ( .ip1(int_crc16_set), .ip2(n1), .op(n1802) );
  nand2_2 U153 ( .ip1(n1801), .ip2(n1800), .op(n1279) );
  nand2_2 U154 ( .ip1(int_[2]), .ip2(n1095), .op(n1800) );
  nand2_2 U155 ( .ip1(n1), .ip2(n1799), .op(n1801) );
  nand2_2 U156 ( .ip1(n1807), .ip2(n1798), .op(n1799) );
  nand2_2 U157 ( .ip1(int_upid_set), .ip2(n1), .op(n1798) );
  nand2_2 U158 ( .ip1(n1797), .ip2(n1796), .op(n1278) );
  or2_2 U159 ( .ip1(n1102), .ip2(n1795), .op(n1796) );
  nand2_2 U160 ( .ip1(n1), .ip2(n1795), .op(n1797) );
  nand2_2 U161 ( .ip1(n1807), .ip2(n1794), .op(n1795) );
  nand2_2 U162 ( .ip1(int_buf0_set), .ip2(n1), .op(n1794) );
  nand2_2 U163 ( .ip1(n1793), .ip2(n1792), .op(n1277) );
  or2_2 U164 ( .ip1(n1101), .ip2(n1791), .op(n1792) );
  nand2_2 U165 ( .ip1(n1), .ip2(n1791), .op(n1793) );
  nand2_2 U166 ( .ip1(n1807), .ip2(n1790), .op(n1791) );
  nand2_2 U167 ( .ip1(int_buf1_set), .ip2(n1), .op(n1790) );
  nand2_2 U168 ( .ip1(n1789), .ip2(n1788), .op(n1276) );
  nand2_2 U169 ( .ip1(int_[5]), .ip2(n1094), .op(n1788) );
  nand2_2 U170 ( .ip1(n1), .ip2(n1787), .op(n1789) );
  nand2_2 U171 ( .ip1(n1807), .ip2(n1786), .op(n1787) );
  nand2_2 U172 ( .ip1(int_seqerr_set), .ip2(n1), .op(n1786) );
  nand2_2 U173 ( .ip1(n1785), .ip2(n1784), .op(n1275) );
  nand2_2 U174 ( .ip1(int_[6]), .ip2(n1093), .op(n1784) );
  nand2_2 U175 ( .ip1(n1), .ip2(n1783), .op(n1785) );
  nand2_2 U176 ( .ip1(n1807), .ip2(n1782), .op(n1783) );
  nand2_2 U177 ( .ip1(out_to_small), .ip2(n1), .op(n1782) );
  nor2_2 U178 ( .ip1(n1889), .ip2(int_re), .op(n1807) );
  nand4_2 U180 ( .ip1(rst), .ip2(n1781), .ip3(n1780), .ip4(n1779), .op(n1274)
         );
  nand2_2 U181 ( .ip1(idin[31]), .ip2(n97), .op(n1779) );
  nand2_2 U182 ( .ip1(din[31]), .ip2(n52), .op(n1780) );
  nand2_2 U183 ( .ip1(buf1[31]), .ip2(n95), .op(n1781) );
  nand4_2 U184 ( .ip1(rst), .ip2(n1776), .ip3(n1775), .ip4(n1774), .op(n1273)
         );
  nand2_2 U185 ( .ip1(idin[30]), .ip2(n96), .op(n1774) );
  nand2_2 U186 ( .ip1(din[30]), .ip2(n52), .op(n1775) );
  nand2_2 U187 ( .ip1(buf1[30]), .ip2(n95), .op(n1776) );
  nand4_2 U188 ( .ip1(rst), .ip2(n1773), .ip3(n1772), .ip4(n1771), .op(n1272)
         );
  nand2_2 U189 ( .ip1(idin[29]), .ip2(n96), .op(n1771) );
  nand2_2 U190 ( .ip1(din[29]), .ip2(n52), .op(n1772) );
  nand2_2 U191 ( .ip1(buf1[29]), .ip2(n95), .op(n1773) );
  nand4_2 U192 ( .ip1(rst), .ip2(n1770), .ip3(n1769), .ip4(n1768), .op(n1271)
         );
  nand2_2 U193 ( .ip1(idin[28]), .ip2(n96), .op(n1768) );
  nand2_2 U194 ( .ip1(din[28]), .ip2(n52), .op(n1769) );
  nand2_2 U195 ( .ip1(buf1[28]), .ip2(n95), .op(n1770) );
  nand4_2 U196 ( .ip1(rst), .ip2(n1767), .ip3(n1766), .ip4(n1765), .op(n1270)
         );
  nand2_2 U197 ( .ip1(idin[27]), .ip2(n96), .op(n1765) );
  nand2_2 U198 ( .ip1(din[27]), .ip2(n52), .op(n1766) );
  nand2_2 U199 ( .ip1(buf1[27]), .ip2(n95), .op(n1767) );
  nand4_2 U200 ( .ip1(rst), .ip2(n1764), .ip3(n1763), .ip4(n1762), .op(n1269)
         );
  nand2_2 U201 ( .ip1(idin[26]), .ip2(n96), .op(n1762) );
  nand2_2 U202 ( .ip1(din[26]), .ip2(n52), .op(n1763) );
  nand2_2 U203 ( .ip1(buf1[26]), .ip2(n95), .op(n1764) );
  nand4_2 U204 ( .ip1(rst), .ip2(n1761), .ip3(n1760), .ip4(n1759), .op(n1268)
         );
  nand2_2 U205 ( .ip1(idin[25]), .ip2(n96), .op(n1759) );
  nand2_2 U206 ( .ip1(din[25]), .ip2(n52), .op(n1760) );
  nand2_2 U207 ( .ip1(buf1[25]), .ip2(n95), .op(n1761) );
  nand4_2 U208 ( .ip1(rst), .ip2(n1758), .ip3(n1757), .ip4(n1756), .op(n1267)
         );
  nand2_2 U209 ( .ip1(idin[24]), .ip2(n96), .op(n1756) );
  nand2_2 U210 ( .ip1(din[24]), .ip2(n52), .op(n1757) );
  nand2_2 U211 ( .ip1(buf1[24]), .ip2(n95), .op(n1758) );
  nand4_2 U212 ( .ip1(rst), .ip2(n1755), .ip3(n1754), .ip4(n1753), .op(n1266)
         );
  nand2_2 U213 ( .ip1(idin[23]), .ip2(n96), .op(n1753) );
  nand2_2 U214 ( .ip1(din[23]), .ip2(n52), .op(n1754) );
  nand2_2 U215 ( .ip1(buf1[23]), .ip2(n94), .op(n1755) );
  nand4_2 U216 ( .ip1(rst), .ip2(n1752), .ip3(n1751), .ip4(n1750), .op(n1265)
         );
  nand2_2 U217 ( .ip1(idin[22]), .ip2(n96), .op(n1750) );
  nand2_2 U218 ( .ip1(din[22]), .ip2(n52), .op(n1751) );
  nand2_2 U219 ( .ip1(buf1[22]), .ip2(n94), .op(n1752) );
  nand4_2 U220 ( .ip1(rst), .ip2(n1749), .ip3(n1748), .ip4(n1747), .op(n1264)
         );
  nand2_2 U221 ( .ip1(idin[21]), .ip2(n96), .op(n1747) );
  nand2_2 U222 ( .ip1(din[21]), .ip2(n52), .op(n1748) );
  nand2_2 U223 ( .ip1(buf1[21]), .ip2(n94), .op(n1749) );
  nand4_2 U224 ( .ip1(rst), .ip2(n1746), .ip3(n1745), .ip4(n1744), .op(n1263)
         );
  nand2_2 U225 ( .ip1(idin[20]), .ip2(n96), .op(n1744) );
  nand2_2 U226 ( .ip1(din[20]), .ip2(n52), .op(n1745) );
  nand2_2 U227 ( .ip1(buf1[20]), .ip2(n94), .op(n1746) );
  nand4_2 U228 ( .ip1(rst), .ip2(n1743), .ip3(n1742), .ip4(n1741), .op(n1262)
         );
  nand2_2 U229 ( .ip1(idin[19]), .ip2(n96), .op(n1741) );
  nand2_2 U230 ( .ip1(din[19]), .ip2(n53), .op(n1742) );
  nand2_2 U231 ( .ip1(buf1[19]), .ip2(n94), .op(n1743) );
  nand4_2 U232 ( .ip1(rst), .ip2(n1740), .ip3(n1739), .ip4(n1738), .op(n1261)
         );
  nand2_2 U233 ( .ip1(idin[18]), .ip2(n97), .op(n1738) );
  nand2_2 U234 ( .ip1(din[18]), .ip2(n53), .op(n1739) );
  nand2_2 U235 ( .ip1(buf1[18]), .ip2(n94), .op(n1740) );
  nand4_2 U236 ( .ip1(rst), .ip2(n1737), .ip3(n1736), .ip4(n1735), .op(n1260)
         );
  nand2_2 U237 ( .ip1(idin[17]), .ip2(n97), .op(n1735) );
  nand2_2 U238 ( .ip1(din[17]), .ip2(n53), .op(n1736) );
  nand2_2 U239 ( .ip1(buf1[17]), .ip2(n94), .op(n1737) );
  nand4_2 U240 ( .ip1(rst), .ip2(n1734), .ip3(n1733), .ip4(n1732), .op(n1259)
         );
  nand2_2 U241 ( .ip1(idin[16]), .ip2(n97), .op(n1732) );
  nand2_2 U242 ( .ip1(din[16]), .ip2(n53), .op(n1733) );
  nand2_2 U243 ( .ip1(buf1[16]), .ip2(n94), .op(n1734) );
  nand4_2 U244 ( .ip1(rst), .ip2(n1731), .ip3(n1730), .ip4(n1729), .op(n1258)
         );
  nand2_2 U245 ( .ip1(idin[15]), .ip2(n97), .op(n1729) );
  nand2_2 U246 ( .ip1(din[15]), .ip2(n53), .op(n1730) );
  nand2_2 U247 ( .ip1(buf1[15]), .ip2(n94), .op(n1731) );
  nand4_2 U248 ( .ip1(rst), .ip2(n1728), .ip3(n1727), .ip4(n1726), .op(n1257)
         );
  nand2_2 U249 ( .ip1(idin[14]), .ip2(n97), .op(n1726) );
  nand2_2 U250 ( .ip1(din[14]), .ip2(n53), .op(n1727) );
  nand2_2 U251 ( .ip1(buf1[14]), .ip2(n94), .op(n1728) );
  nand4_2 U252 ( .ip1(rst), .ip2(n1725), .ip3(n1724), .ip4(n1723), .op(n1256)
         );
  nand2_2 U253 ( .ip1(idin[13]), .ip2(n97), .op(n1723) );
  nand2_2 U254 ( .ip1(din[13]), .ip2(n53), .op(n1724) );
  nand2_2 U255 ( .ip1(buf1[13]), .ip2(n94), .op(n1725) );
  nand4_2 U256 ( .ip1(rst), .ip2(n1722), .ip3(n1721), .ip4(n1720), .op(n1255)
         );
  nand2_2 U257 ( .ip1(idin[12]), .ip2(n97), .op(n1720) );
  nand2_2 U258 ( .ip1(din[12]), .ip2(n53), .op(n1721) );
  nand2_2 U259 ( .ip1(buf1[12]), .ip2(n94), .op(n1722) );
  nand4_2 U260 ( .ip1(rst), .ip2(n1719), .ip3(n1718), .ip4(n1717), .op(n1254)
         );
  nand2_2 U261 ( .ip1(idin[11]), .ip2(n97), .op(n1717) );
  nand2_2 U262 ( .ip1(din[11]), .ip2(n53), .op(n1718) );
  nand2_2 U263 ( .ip1(buf1[11]), .ip2(n93), .op(n1719) );
  nand4_2 U264 ( .ip1(rst), .ip2(n1716), .ip3(n1715), .ip4(n1714), .op(n1253)
         );
  nand2_2 U265 ( .ip1(idin[10]), .ip2(n97), .op(n1714) );
  nand2_2 U266 ( .ip1(din[10]), .ip2(n53), .op(n1715) );
  nand2_2 U267 ( .ip1(buf1[10]), .ip2(n93), .op(n1716) );
  nand4_2 U268 ( .ip1(rst), .ip2(n1713), .ip3(n1712), .ip4(n1711), .op(n1252)
         );
  nand2_2 U269 ( .ip1(idin[9]), .ip2(n97), .op(n1711) );
  nand2_2 U270 ( .ip1(din[9]), .ip2(n53), .op(n1712) );
  nand2_2 U271 ( .ip1(buf1[9]), .ip2(n93), .op(n1713) );
  nand4_2 U272 ( .ip1(rst), .ip2(n1710), .ip3(n1709), .ip4(n1708), .op(n1251)
         );
  nand2_2 U273 ( .ip1(idin[8]), .ip2(n97), .op(n1708) );
  nand2_2 U274 ( .ip1(din[8]), .ip2(n53), .op(n1709) );
  nand2_2 U275 ( .ip1(buf1[8]), .ip2(n93), .op(n1710) );
  nand4_2 U276 ( .ip1(rst), .ip2(n1707), .ip3(n1706), .ip4(n1705), .op(n1250)
         );
  nand2_2 U277 ( .ip1(idin[7]), .ip2(n98), .op(n1705) );
  nand2_2 U278 ( .ip1(din[7]), .ip2(n54), .op(n1706) );
  nand2_2 U279 ( .ip1(buf1[7]), .ip2(n93), .op(n1707) );
  nand4_2 U280 ( .ip1(rst), .ip2(n1704), .ip3(n1703), .ip4(n1702), .op(n1249)
         );
  nand2_2 U281 ( .ip1(idin[6]), .ip2(n98), .op(n1702) );
  nand2_2 U282 ( .ip1(din[6]), .ip2(n54), .op(n1703) );
  nand2_2 U283 ( .ip1(buf1[6]), .ip2(n93), .op(n1704) );
  nand4_2 U284 ( .ip1(rst), .ip2(n1701), .ip3(n1700), .ip4(n1699), .op(n1248)
         );
  nand2_2 U285 ( .ip1(idin[5]), .ip2(n98), .op(n1699) );
  nand2_2 U286 ( .ip1(din[5]), .ip2(n54), .op(n1700) );
  nand2_2 U287 ( .ip1(buf1[5]), .ip2(n93), .op(n1701) );
  nand4_2 U288 ( .ip1(rst), .ip2(n1698), .ip3(n1697), .ip4(n1696), .op(n1247)
         );
  nand2_2 U289 ( .ip1(idin[4]), .ip2(n98), .op(n1696) );
  nand2_2 U290 ( .ip1(din[4]), .ip2(n54), .op(n1697) );
  nand2_2 U291 ( .ip1(buf1[4]), .ip2(n93), .op(n1698) );
  nand4_2 U292 ( .ip1(rst), .ip2(n1695), .ip3(n1694), .ip4(n1693), .op(n1246)
         );
  nand2_2 U293 ( .ip1(n98), .ip2(idin[3]), .op(n1693) );
  nand2_2 U294 ( .ip1(din[3]), .ip2(n54), .op(n1694) );
  nand2_2 U295 ( .ip1(buf1[3]), .ip2(n93), .op(n1695) );
  nand4_2 U296 ( .ip1(rst), .ip2(n1692), .ip3(n1691), .ip4(n1690), .op(n1245)
         );
  nand2_2 U297 ( .ip1(n98), .ip2(idin[2]), .op(n1690) );
  nand2_2 U298 ( .ip1(din[2]), .ip2(n54), .op(n1691) );
  nand2_2 U299 ( .ip1(buf1[2]), .ip2(n93), .op(n1692) );
  nand4_2 U300 ( .ip1(rst), .ip2(n1689), .ip3(n1688), .ip4(n1687), .op(n1244)
         );
  nand2_2 U301 ( .ip1(n98), .ip2(idin[1]), .op(n1687) );
  nand2_2 U302 ( .ip1(din[1]), .ip2(n54), .op(n1688) );
  nand2_2 U303 ( .ip1(buf1[1]), .ip2(n93), .op(n1689) );
  nand4_2 U304 ( .ip1(rst), .ip2(n1686), .ip3(n1685), .ip4(n1684), .op(n1243)
         );
  nand2_2 U305 ( .ip1(n98), .ip2(idin[0]), .op(n1684) );
  nand2_2 U306 ( .ip1(din[0]), .ip2(n54), .op(n1685) );
  nand2_2 U307 ( .ip1(buf1[0]), .ip2(n93), .op(n1686) );
  nor2_2 U308 ( .ip1(n54), .ip2(n98), .op(n1777) );
  and3_2 U309 ( .ip1(n1683), .ip2(n1682), .ip3(ep_match_r), .op(n1778) );
  or2_2 U310 ( .ip1(buf1_set), .ip2(out_to_small), .op(n1683) );
  nand2_2 U311 ( .ip1(we), .ip2(n90), .op(n1682) );
  nand3_2 U312 ( .ip1(rst), .ip2(n1680), .ip3(n1679), .op(n1242) );
  nor2_2 U313 ( .ip1(n89), .ip2(n1071), .op(n1677) );
  nor2_2 U314 ( .ip1(n1123), .ip2(n86), .op(n1678) );
  nand2_2 U315 ( .ip1(buf0[31]), .ip2(n83), .op(n1680) );
  nand3_2 U316 ( .ip1(rst), .ip2(n1673), .ip3(n1672), .op(n1241) );
  nor2_2 U317 ( .ip1(n89), .ip2(n985), .op(n1670) );
  nor2_2 U318 ( .ip1(n1105), .ip2(n86), .op(n1671) );
  nand2_2 U319 ( .ip1(buf0[30]), .ip2(n83), .op(n1673) );
  nand3_2 U320 ( .ip1(rst), .ip2(n1669), .ip3(n1668), .op(n1240) );
  nor2_2 U321 ( .ip1(n89), .ip2(n1066), .op(n1666) );
  nor2_2 U322 ( .ip1(n1106), .ip2(n86), .op(n1667) );
  nand2_2 U323 ( .ip1(buf0[29]), .ip2(n83), .op(n1669) );
  nand3_2 U324 ( .ip1(rst), .ip2(n1665), .ip3(n1664), .op(n1239) );
  nor2_2 U325 ( .ip1(n89), .ip2(n986), .op(n1662) );
  nor2_2 U326 ( .ip1(n1107), .ip2(n86), .op(n1663) );
  nand2_2 U327 ( .ip1(buf0[28]), .ip2(n83), .op(n1665) );
  nand3_2 U328 ( .ip1(rst), .ip2(n1661), .ip3(n1660), .op(n1238) );
  nor2_2 U329 ( .ip1(n89), .ip2(n992), .op(n1658) );
  nor2_2 U330 ( .ip1(n1126), .ip2(n86), .op(n1659) );
  nand2_2 U331 ( .ip1(buf0[27]), .ip2(n83), .op(n1661) );
  nand3_2 U332 ( .ip1(rst), .ip2(n1657), .ip3(n1656), .op(n1237) );
  nor2_2 U333 ( .ip1(n88), .ip2(n987), .op(n1654) );
  nor2_2 U334 ( .ip1(n1127), .ip2(n86), .op(n1655) );
  nand2_2 U335 ( .ip1(buf0[26]), .ip2(n83), .op(n1657) );
  nand3_2 U336 ( .ip1(rst), .ip2(n1653), .ip3(n1652), .op(n1236) );
  nor2_2 U337 ( .ip1(n88), .ip2(n991), .op(n1650) );
  nor2_2 U338 ( .ip1(n1128), .ip2(n86), .op(n1651) );
  nand2_2 U339 ( .ip1(buf0[25]), .ip2(n83), .op(n1653) );
  nand3_2 U340 ( .ip1(rst), .ip2(n1649), .ip3(n1648), .op(n1235) );
  nor2_2 U341 ( .ip1(n88), .ip2(n988), .op(n1646) );
  nor2_2 U342 ( .ip1(n1129), .ip2(n85), .op(n1647) );
  nand2_2 U343 ( .ip1(buf0[24]), .ip2(n83), .op(n1649) );
  nand3_2 U344 ( .ip1(rst), .ip2(n1645), .ip3(n1644), .op(n1234) );
  nor2_2 U345 ( .ip1(n88), .ip2(n990), .op(n1642) );
  nor2_2 U346 ( .ip1(n1130), .ip2(n85), .op(n1643) );
  nand2_2 U347 ( .ip1(buf0[23]), .ip2(n82), .op(n1645) );
  nand3_2 U348 ( .ip1(rst), .ip2(n1641), .ip3(n1640), .op(n1233) );
  nor2_2 U349 ( .ip1(n88), .ip2(n1072), .op(n1638) );
  nor2_2 U350 ( .ip1(n1131), .ip2(n85), .op(n1639) );
  nand2_2 U351 ( .ip1(buf0[22]), .ip2(n82), .op(n1641) );
  nand3_2 U352 ( .ip1(rst), .ip2(n1637), .ip3(n1636), .op(n1232) );
  nor2_2 U353 ( .ip1(n88), .ip2(n1065), .op(n1634) );
  nor2_2 U354 ( .ip1(n1132), .ip2(n85), .op(n1635) );
  nand2_2 U355 ( .ip1(buf0[21]), .ip2(n82), .op(n1637) );
  nand3_2 U356 ( .ip1(rst), .ip2(n1633), .ip3(n1632), .op(n1231) );
  nor2_2 U357 ( .ip1(n88), .ip2(n1073), .op(n1630) );
  nor2_2 U358 ( .ip1(n1133), .ip2(n85), .op(n1631) );
  nand2_2 U359 ( .ip1(buf0[20]), .ip2(n82), .op(n1633) );
  nand3_2 U360 ( .ip1(rst), .ip2(n1629), .ip3(n1628), .op(n1230) );
  nor2_2 U361 ( .ip1(n88), .ip2(n989), .op(n1626) );
  nor2_2 U362 ( .ip1(n1134), .ip2(n85), .op(n1627) );
  nand2_2 U363 ( .ip1(buf0[19]), .ip2(n82), .op(n1629) );
  nand3_2 U364 ( .ip1(rst), .ip2(n1625), .ip3(n1624), .op(n1229) );
  nor2_2 U365 ( .ip1(n88), .ip2(n1074), .op(n1622) );
  nor2_2 U366 ( .ip1(n1135), .ip2(n85), .op(n1623) );
  nand2_2 U367 ( .ip1(buf0[18]), .ip2(n82), .op(n1625) );
  nand3_2 U368 ( .ip1(rst), .ip2(n1621), .ip3(n1620), .op(n1228) );
  nor2_2 U369 ( .ip1(n88), .ip2(n1075), .op(n1618) );
  nor2_2 U370 ( .ip1(n1136), .ip2(n85), .op(n1619) );
  nand2_2 U371 ( .ip1(buf0[17]), .ip2(n82), .op(n1621) );
  nand3_2 U372 ( .ip1(rst), .ip2(n1617), .ip3(n1616), .op(n1227) );
  nor2_2 U373 ( .ip1(n88), .ip2(n1076), .op(n1614) );
  nor2_2 U374 ( .ip1(n1110), .ip2(n85), .op(n1615) );
  nand2_2 U375 ( .ip1(buf0[16]), .ip2(n82), .op(n1617) );
  nand3_2 U376 ( .ip1(rst), .ip2(n1613), .ip3(n1612), .op(n1226) );
  nor2_2 U377 ( .ip1(n88), .ip2(n1077), .op(n1610) );
  nor2_2 U378 ( .ip1(n1111), .ip2(n85), .op(n1611) );
  nand2_2 U379 ( .ip1(buf0[15]), .ip2(n82), .op(n1613) );
  nand3_2 U380 ( .ip1(rst), .ip2(n1609), .ip3(n1608), .op(n1225) );
  nor2_2 U381 ( .ip1(n88), .ip2(n1078), .op(n1606) );
  nor2_2 U382 ( .ip1(n1112), .ip2(n85), .op(n1607) );
  nand2_2 U383 ( .ip1(buf0[14]), .ip2(n82), .op(n1609) );
  nand3_2 U384 ( .ip1(rst), .ip2(n1605), .ip3(n1604), .op(n1224) );
  nor2_2 U385 ( .ip1(n88), .ip2(n1079), .op(n1602) );
  nor2_2 U386 ( .ip1(n1113), .ip2(n85), .op(n1603) );
  nand2_2 U387 ( .ip1(buf0[13]), .ip2(n82), .op(n1605) );
  nand3_2 U388 ( .ip1(rst), .ip2(n1601), .ip3(n1600), .op(n1223) );
  nor2_2 U389 ( .ip1(n87), .ip2(n1080), .op(n1598) );
  nor2_2 U390 ( .ip1(n1114), .ip2(n85), .op(n1599) );
  nand2_2 U391 ( .ip1(buf0[12]), .ip2(n82), .op(n1601) );
  nand3_2 U392 ( .ip1(rst), .ip2(n1597), .ip3(n1596), .op(n1222) );
  nor2_2 U393 ( .ip1(n87), .ip2(n1081), .op(n1594) );
  nor2_2 U394 ( .ip1(n1115), .ip2(n84), .op(n1595) );
  nand2_2 U395 ( .ip1(buf0[11]), .ip2(n81), .op(n1597) );
  nand3_2 U396 ( .ip1(rst), .ip2(n1593), .ip3(n1592), .op(n1221) );
  nor2_2 U397 ( .ip1(n87), .ip2(n1082), .op(n1590) );
  nor2_2 U398 ( .ip1(n1116), .ip2(n84), .op(n1591) );
  nand2_2 U399 ( .ip1(buf0[10]), .ip2(n81), .op(n1593) );
  nand3_2 U400 ( .ip1(rst), .ip2(n1589), .ip3(n1588), .op(n1220) );
  nor2_2 U401 ( .ip1(n87), .ip2(n1083), .op(n1586) );
  nor2_2 U402 ( .ip1(n1117), .ip2(n84), .op(n1587) );
  nand2_2 U403 ( .ip1(buf0[9]), .ip2(n81), .op(n1589) );
  nand3_2 U404 ( .ip1(rst), .ip2(n1585), .ip3(n1584), .op(n1219) );
  nor2_2 U405 ( .ip1(n87), .ip2(n1084), .op(n1582) );
  nor2_2 U406 ( .ip1(n1118), .ip2(n84), .op(n1583) );
  nand2_2 U407 ( .ip1(buf0[8]), .ip2(n81), .op(n1585) );
  nand3_2 U408 ( .ip1(rst), .ip2(n1581), .ip3(n1580), .op(n1218) );
  nor2_2 U409 ( .ip1(n87), .ip2(n1085), .op(n1578) );
  nor2_2 U410 ( .ip1(n1119), .ip2(n84), .op(n1579) );
  nand2_2 U411 ( .ip1(buf0[7]), .ip2(n81), .op(n1581) );
  nand3_2 U412 ( .ip1(rst), .ip2(n1577), .ip3(n1576), .op(n1217) );
  nor2_2 U413 ( .ip1(n87), .ip2(n1086), .op(n1574) );
  nor2_2 U414 ( .ip1(n1120), .ip2(n84), .op(n1575) );
  nand2_2 U415 ( .ip1(buf0[6]), .ip2(n81), .op(n1577) );
  nand3_2 U416 ( .ip1(rst), .ip2(n1573), .ip3(n1572), .op(n1216) );
  nor2_2 U417 ( .ip1(n87), .ip2(n1087), .op(n1570) );
  nor2_2 U418 ( .ip1(n1121), .ip2(n84), .op(n1571) );
  nand2_2 U419 ( .ip1(buf0[5]), .ip2(n81), .op(n1573) );
  nand3_2 U420 ( .ip1(rst), .ip2(n1569), .ip3(n1568), .op(n1215) );
  nor2_2 U421 ( .ip1(n87), .ip2(n1088), .op(n1566) );
  nor2_2 U422 ( .ip1(n1122), .ip2(n84), .op(n1567) );
  nand2_2 U423 ( .ip1(buf0[4]), .ip2(n81), .op(n1569) );
  nand3_2 U424 ( .ip1(rst), .ip2(n1565), .ip3(n1564), .op(n1214) );
  nor2_2 U425 ( .ip1(n87), .ip2(n1089), .op(n1562) );
  nor2_2 U426 ( .ip1(n1108), .ip2(n84), .op(n1563) );
  nand2_2 U427 ( .ip1(buf0[3]), .ip2(n81), .op(n1565) );
  nand3_2 U428 ( .ip1(rst), .ip2(n1561), .ip3(n1560), .op(n1213) );
  nor2_2 U429 ( .ip1(n87), .ip2(n1090), .op(n1558) );
  nor2_2 U430 ( .ip1(n1109), .ip2(n84), .op(n1559) );
  nand2_2 U431 ( .ip1(buf0[2]), .ip2(n81), .op(n1561) );
  nand3_2 U432 ( .ip1(rst), .ip2(n1557), .ip3(n1556), .op(n1212) );
  nor2_2 U433 ( .ip1(n87), .ip2(n1091), .op(n1554) );
  nor2_2 U434 ( .ip1(n1124), .ip2(n84), .op(n1555) );
  nand2_2 U435 ( .ip1(buf0[1]), .ip2(n81), .op(n1557) );
  nand3_2 U436 ( .ip1(rst), .ip2(n1553), .ip3(n1552), .op(n1211) );
  nor2_2 U437 ( .ip1(n87), .ip2(n1092), .op(n1550) );
  nor2_2 U438 ( .ip1(n1125), .ip2(n84), .op(n1551) );
  nand2_2 U439 ( .ip1(buf0[0]), .ip2(n81), .op(n1553) );
  and3_2 U440 ( .ip1(n87), .ip2(n66), .ip3(n84), .op(n1674) );
  nand4_2 U441 ( .ip1(buf0_set), .ip2(n1821), .ip3(n66), .ip4(n1103), .op(
        n1675) );
  nand3_2 U442 ( .ip1(n1821), .ip2(n67), .ip3(buf0_rl), .op(n1676) );
  nor2_2 U443 ( .ip1(n1098), .ip2(n1889), .op(n1821) );
  and2_2 U444 ( .ip1(din[0]), .ip2(n80), .op(n1548) );
  and2_2 U445 ( .ip1(din[1]), .ip2(n80), .op(n1547) );
  and2_2 U446 ( .ip1(din[2]), .ip2(n80), .op(n1546) );
  and2_2 U447 ( .ip1(din[3]), .ip2(n80), .op(n1545) );
  and2_2 U448 ( .ip1(din[4]), .ip2(n80), .op(n1544) );
  and2_2 U449 ( .ip1(din[5]), .ip2(n80), .op(n1543) );
  and2_2 U450 ( .ip1(din[6]), .ip2(n79), .op(n1542) );
  and2_2 U451 ( .ip1(din[7]), .ip2(n79), .op(n1541) );
  and2_2 U452 ( .ip1(din[8]), .ip2(n79), .op(n1540) );
  and2_2 U453 ( .ip1(din[9]), .ip2(n79), .op(n1539) );
  and2_2 U454 ( .ip1(din[10]), .ip2(n79), .op(n1538) );
  and2_2 U455 ( .ip1(din[11]), .ip2(n79), .op(n1537) );
  and2_2 U456 ( .ip1(din[12]), .ip2(n79), .op(n1536) );
  and2_2 U457 ( .ip1(din[13]), .ip2(n79), .op(n1535) );
  and2_2 U458 ( .ip1(din[14]), .ip2(n79), .op(n1534) );
  and2_2 U459 ( .ip1(din[15]), .ip2(n79), .op(n1533) );
  and2_2 U460 ( .ip1(din[16]), .ip2(n79), .op(n1532) );
  and2_2 U461 ( .ip1(din[17]), .ip2(n79), .op(n1531) );
  and2_2 U462 ( .ip1(din[18]), .ip2(n79), .op(n1530) );
  and2_2 U463 ( .ip1(din[19]), .ip2(n78), .op(n1529) );
  and2_2 U464 ( .ip1(din[20]), .ip2(n78), .op(n1528) );
  and2_2 U465 ( .ip1(din[21]), .ip2(n78), .op(n1527) );
  and2_2 U466 ( .ip1(din[22]), .ip2(n78), .op(n1526) );
  and2_2 U467 ( .ip1(din[23]), .ip2(n78), .op(n1525) );
  and2_2 U468 ( .ip1(din[24]), .ip2(n78), .op(n1524) );
  and2_2 U469 ( .ip1(din[25]), .ip2(n78), .op(n1523) );
  and2_2 U470 ( .ip1(din[26]), .ip2(n78), .op(n1522) );
  and2_2 U471 ( .ip1(din[27]), .ip2(n78), .op(n1521) );
  and2_2 U472 ( .ip1(din[28]), .ip2(n78), .op(n1520) );
  and2_2 U473 ( .ip1(din[29]), .ip2(n78), .op(n1519) );
  and2_2 U474 ( .ip1(din[30]), .ip2(n78), .op(n1518) );
  and2_2 U475 ( .ip1(din[31]), .ip2(n78), .op(n1517) );
  nand2_2 U477 ( .ip1(n1515), .ip2(n1514), .op(n1178) );
  nand2_2 U478 ( .ip1(n1513), .ip2(din[16]), .op(n1514) );
  nand2_2 U479 ( .ip1(int__16), .ip2(n1512), .op(n1515) );
  nand2_2 U480 ( .ip1(n1511), .ip2(n1510), .op(n1177) );
  nand2_2 U481 ( .ip1(n1513), .ip2(din[17]), .op(n1510) );
  nand2_2 U482 ( .ip1(int__17), .ip2(n1512), .op(n1511) );
  nand2_2 U483 ( .ip1(n1509), .ip2(n1508), .op(n1176) );
  nand2_2 U484 ( .ip1(n1513), .ip2(din[18]), .op(n1508) );
  nand2_2 U485 ( .ip1(int__18), .ip2(n1512), .op(n1509) );
  nand2_2 U486 ( .ip1(n1507), .ip2(n1506), .op(n1175) );
  nand2_2 U487 ( .ip1(n1513), .ip2(din[19]), .op(n1506) );
  nand2_2 U488 ( .ip1(int__19), .ip2(n1512), .op(n1507) );
  nand2_2 U489 ( .ip1(n1505), .ip2(n1504), .op(n1174) );
  nand2_2 U490 ( .ip1(n1513), .ip2(din[20]), .op(n1504) );
  nand2_2 U491 ( .ip1(int__20), .ip2(n1512), .op(n1505) );
  nand2_2 U492 ( .ip1(n1503), .ip2(n1502), .op(n1173) );
  nand2_2 U493 ( .ip1(n1513), .ip2(din[21]), .op(n1502) );
  nand2_2 U494 ( .ip1(int__21), .ip2(n1512), .op(n1503) );
  nand2_2 U495 ( .ip1(n1501), .ip2(n1500), .op(n1172) );
  nand2_2 U496 ( .ip1(n1513), .ip2(din[24]), .op(n1500) );
  nand2_2 U497 ( .ip1(int__24), .ip2(n1512), .op(n1501) );
  nand2_2 U498 ( .ip1(n1499), .ip2(n1498), .op(n1171) );
  nand2_2 U499 ( .ip1(n1513), .ip2(din[25]), .op(n1498) );
  nand2_2 U500 ( .ip1(int__25), .ip2(n1512), .op(n1499) );
  nand2_2 U501 ( .ip1(n1497), .ip2(n1496), .op(n1170) );
  nand2_2 U502 ( .ip1(n1513), .ip2(din[26]), .op(n1496) );
  nand2_2 U503 ( .ip1(int__26), .ip2(n1512), .op(n1497) );
  nand2_2 U504 ( .ip1(n1495), .ip2(n1494), .op(n1169) );
  nand2_2 U505 ( .ip1(n1513), .ip2(din[27]), .op(n1494) );
  nand2_2 U506 ( .ip1(int__27), .ip2(n1512), .op(n1495) );
  nand2_2 U507 ( .ip1(n1493), .ip2(n1492), .op(n1168) );
  nand2_2 U508 ( .ip1(n1513), .ip2(din[28]), .op(n1492) );
  nand2_2 U509 ( .ip1(int__28), .ip2(n1512), .op(n1493) );
  nand2_2 U510 ( .ip1(n1491), .ip2(n1490), .op(n1167) );
  nand2_2 U511 ( .ip1(n1513), .ip2(din[29]), .op(n1490) );
  nand2_2 U512 ( .ip1(int__29), .ip2(n1512), .op(n1491) );
  nand2_2 U516 ( .ip1(n61), .ip2(din[15]), .op(n1489) );
  nand2_2 U518 ( .ip1(n1486), .ip2(n1485), .op(n1165) );
  nand2_2 U519 ( .ip1(n60), .ip2(din[16]), .op(n1485) );
  nand2_2 U520 ( .ip1(csr[16]), .ip2(n58), .op(n1486) );
  nand2_2 U521 ( .ip1(n1484), .ip2(n1483), .op(n1164) );
  nand2_2 U522 ( .ip1(n60), .ip2(din[17]), .op(n1483) );
  nand2_2 U523 ( .ip1(csr[17]), .ip2(n58), .op(n1484) );
  nand2_2 U524 ( .ip1(n1482), .ip2(n1481), .op(n1163) );
  nand2_2 U525 ( .ip1(n60), .ip2(din[18]), .op(n1481) );
  nand2_2 U526 ( .ip1(csr[18]), .ip2(n58), .op(n1482) );
  nand2_2 U527 ( .ip1(n1480), .ip2(n1479), .op(n1162) );
  nand2_2 U528 ( .ip1(n60), .ip2(din[19]), .op(n1479) );
  nand2_2 U529 ( .ip1(csr[19]), .ip2(n58), .op(n1480) );
  nand2_2 U530 ( .ip1(n1478), .ip2(n1477), .op(n1161) );
  nand2_2 U531 ( .ip1(n60), .ip2(din[20]), .op(n1477) );
  nand2_2 U532 ( .ip1(csr[20]), .ip2(n58), .op(n1478) );
  nand2_2 U533 ( .ip1(n1476), .ip2(n1475), .op(n1160) );
  nand2_2 U534 ( .ip1(n60), .ip2(din[21]), .op(n1475) );
  nand2_2 U535 ( .ip1(csr[21]), .ip2(n58), .op(n1476) );
  and2_2 U536 ( .ip1(din[22]), .ip2(n60), .op(n1473) );
  nand2_2 U537 ( .ip1(n1471), .ip2(n1470), .op(n1158) );
  nand2_2 U538 ( .ip1(n60), .ip2(din[23]), .op(n1470) );
  nand2_2 U539 ( .ip1(csr[23]), .ip2(n1474), .op(n1471) );
  and2_2 U540 ( .ip1(n59), .ip2(n1472), .op(n1474) );
  nand4_2 U541 ( .ip1(csr[13]), .ip2(out_to_small), .ip3(rst), .ip4(n1469), 
        .op(n1472) );
  nand2_2 U542 ( .ip1(n1468), .ip2(n1467), .op(n1157) );
  nand2_2 U543 ( .ip1(n60), .ip2(din[24]), .op(n1467) );
  nand2_2 U544 ( .ip1(csr[24]), .ip2(n58), .op(n1468) );
  nand2_2 U545 ( .ip1(n1466), .ip2(n1465), .op(n1156) );
  nand2_2 U546 ( .ip1(n60), .ip2(din[25]), .op(n1465) );
  nand2_2 U547 ( .ip1(csr[25]), .ip2(n58), .op(n1466) );
  nand2_2 U548 ( .ip1(n1464), .ip2(n1463), .op(n1155) );
  nand2_2 U549 ( .ip1(n60), .ip2(din[26]), .op(n1463) );
  nand2_2 U550 ( .ip1(csr[26]), .ip2(n58), .op(n1464) );
  nand2_2 U551 ( .ip1(n1462), .ip2(n1461), .op(n1154) );
  nand2_2 U552 ( .ip1(n61), .ip2(din[27]), .op(n1461) );
  nand2_2 U553 ( .ip1(n59), .ip2(csr[27]), .op(n1462) );
  nand2_2 U554 ( .ip1(n1460), .ip2(n1459), .op(n1153) );
  nand2_2 U555 ( .ip1(n61), .ip2(din[0]), .op(n1459) );
  nand2_2 U556 ( .ip1(csr[0]), .ip2(n58), .op(n1460) );
  nand2_2 U557 ( .ip1(n1458), .ip2(n1457), .op(n1152) );
  nand2_2 U558 ( .ip1(n61), .ip2(din[1]), .op(n1457) );
  nand2_2 U559 ( .ip1(csr[1]), .ip2(n58), .op(n1458) );
  nand2_2 U560 ( .ip1(n1456), .ip2(n1455), .op(n1151) );
  nand2_2 U561 ( .ip1(n61), .ip2(din[2]), .op(n1455) );
  nand2_2 U562 ( .ip1(csr[2]), .ip2(n58), .op(n1456) );
  nand2_2 U563 ( .ip1(n1454), .ip2(n1453), .op(n1150) );
  nand2_2 U564 ( .ip1(n61), .ip2(din[3]), .op(n1453) );
  nand2_2 U565 ( .ip1(csr[3]), .ip2(n59), .op(n1454) );
  nand2_2 U566 ( .ip1(n1452), .ip2(n1451), .op(n1149) );
  nand2_2 U567 ( .ip1(n61), .ip2(din[4]), .op(n1451) );
  nand2_2 U568 ( .ip1(csr[4]), .ip2(n59), .op(n1452) );
  nand2_2 U569 ( .ip1(n1450), .ip2(n1449), .op(n1148) );
  nand2_2 U570 ( .ip1(n61), .ip2(din[5]), .op(n1449) );
  nand2_2 U571 ( .ip1(csr[5]), .ip2(n59), .op(n1450) );
  nand2_2 U572 ( .ip1(n1448), .ip2(n1447), .op(n1147) );
  nand2_2 U573 ( .ip1(n61), .ip2(din[6]), .op(n1447) );
  nand2_2 U574 ( .ip1(csr[6]), .ip2(n59), .op(n1448) );
  nand2_2 U575 ( .ip1(n1446), .ip2(n1445), .op(n1146) );
  nand2_2 U576 ( .ip1(n61), .ip2(din[7]), .op(n1445) );
  nand2_2 U577 ( .ip1(csr[7]), .ip2(n59), .op(n1446) );
  nand2_2 U578 ( .ip1(n1444), .ip2(n1443), .op(n1145) );
  nand2_2 U579 ( .ip1(n61), .ip2(din[8]), .op(n1443) );
  nand2_2 U580 ( .ip1(csr[8]), .ip2(n59), .op(n1444) );
  nand2_2 U581 ( .ip1(n1442), .ip2(n1441), .op(n1144) );
  nand2_2 U582 ( .ip1(n61), .ip2(din[9]), .op(n1441) );
  nand2_2 U583 ( .ip1(csr[9]), .ip2(n59), .op(n1442) );
  nand2_2 U584 ( .ip1(n1440), .ip2(n1439), .op(n1143) );
  nand2_2 U585 ( .ip1(n61), .ip2(din[10]), .op(n1439) );
  nand2_2 U586 ( .ip1(csr[10]), .ip2(n59), .op(n1440) );
  nand2_2 U587 ( .ip1(n1438), .ip2(n1437), .op(n1142) );
  nand2_2 U588 ( .ip1(n62), .ip2(din[11]), .op(n1437) );
  nand2_2 U589 ( .ip1(csr[11]), .ip2(n59), .op(n1438) );
  nand2_2 U590 ( .ip1(n1436), .ip2(n1435), .op(n1141) );
  nand2_2 U591 ( .ip1(n62), .ip2(din[12]), .op(n1435) );
  nand2_2 U592 ( .ip1(csr[12]), .ip2(n59), .op(n1436) );
  nand2_2 U593 ( .ip1(n1434), .ip2(n1433), .op(n1140) );
  nand2_2 U594 ( .ip1(n60), .ip2(din[13]), .op(n1433) );
  nand2_2 U595 ( .ip1(csr[13]), .ip2(n59), .op(n1434) );
  nor2_2 U596 ( .ip1(n1889), .ip2(n60), .op(n1487) );
  nor2_2 U597 ( .ip1(n1469), .ip2(n1889), .op(n1488) );
  nand2_2 U598 ( .ip1(n56), .ip2(we), .op(n1469) );
  nor4_2 U599 ( .ip1(n1431), .ip2(n1430), .ip3(n1429), .ip4(n1428), .op(
        ep_match) );
  xor2_2 U600 ( .ip1(ep_sel[1]), .ip2(csr[19]), .op(n1428) );
  xor2_2 U601 ( .ip1(ep_sel[2]), .ip2(csr[20]), .op(n1429) );
  xor2_2 U602 ( .ip1(ep_sel[3]), .ip2(csr[21]), .op(n1430) );
  xor2_2 U603 ( .ip1(ep_sel[0]), .ip2(csr[18]), .op(n1431) );
  and2_2 U604 ( .ip1(n65), .ip2(buf0[9]), .op(n1426) );
  and2_2 U605 ( .ip1(n92), .ip2(buf1[9]), .op(n1427) );
  and2_2 U606 ( .ip1(n65), .ip2(buf0[8]), .op(n1424) );
  and2_2 U607 ( .ip1(n92), .ip2(buf1[8]), .op(n1425) );
  and2_2 U608 ( .ip1(n65), .ip2(buf0[7]), .op(n1422) );
  and2_2 U609 ( .ip1(n92), .ip2(buf1[7]), .op(n1423) );
  nand4_2 U610 ( .ip1(n1421), .ip2(n1420), .ip3(n1419), .ip4(n1418), .op(
        dout[6]) );
  nand2_2 U611 ( .ip1(buf1[6]), .ip2(n91), .op(n1418) );
  nand2_2 U612 ( .ip1(buf0[6]), .ip2(n63), .op(n1419) );
  nand2_2 U613 ( .ip1(n51), .ip2(int_[6]), .op(n1420) );
  nand2_2 U614 ( .ip1(csr[6]), .ip2(n55), .op(n1421) );
  nand4_2 U615 ( .ip1(n1417), .ip2(n1416), .ip3(n1415), .ip4(n1414), .op(
        dout[5]) );
  nand2_2 U616 ( .ip1(buf1[5]), .ip2(n90), .op(n1414) );
  nand2_2 U617 ( .ip1(buf0[5]), .ip2(n63), .op(n1415) );
  nand2_2 U618 ( .ip1(n51), .ip2(int_[5]), .op(n1416) );
  nand2_2 U619 ( .ip1(csr[5]), .ip2(n56), .op(n1417) );
  nand4_2 U620 ( .ip1(n1413), .ip2(n1412), .ip3(n1411), .ip4(n1410), .op(
        dout[4]) );
  nand2_2 U621 ( .ip1(buf1[4]), .ip2(n90), .op(n1410) );
  nand2_2 U622 ( .ip1(buf0[4]), .ip2(n63), .op(n1411) );
  nand2_2 U623 ( .ip1(n50), .ip2(int_[4]), .op(n1412) );
  nand2_2 U624 ( .ip1(csr[4]), .ip2(n56), .op(n1413) );
  nand4_2 U625 ( .ip1(n1409), .ip2(n1408), .ip3(n1407), .ip4(n1406), .op(
        dout[3]) );
  nand2_2 U626 ( .ip1(buf1[3]), .ip2(n90), .op(n1406) );
  nand2_2 U627 ( .ip1(buf0[3]), .ip2(n63), .op(n1407) );
  nand2_2 U628 ( .ip1(n49), .ip2(int_[3]), .op(n1408) );
  nand2_2 U629 ( .ip1(csr[3]), .ip2(n56), .op(n1409) );
  and2_2 U630 ( .ip1(n64), .ip2(buf0[31]), .op(n1404) );
  and2_2 U631 ( .ip1(n92), .ip2(buf1[31]), .op(n1405) );
  and2_2 U632 ( .ip1(n64), .ip2(buf0[30]), .op(n1402) );
  and2_2 U633 ( .ip1(n91), .ip2(buf1[30]), .op(n1403) );
  nand4_2 U634 ( .ip1(n1401), .ip2(n1400), .ip3(n1399), .ip4(n1398), .op(
        dout[2]) );
  nand2_2 U635 ( .ip1(buf1[2]), .ip2(n90), .op(n1398) );
  nand2_2 U636 ( .ip1(buf0[2]), .ip2(n63), .op(n1399) );
  nand2_2 U637 ( .ip1(n48), .ip2(int_[2]), .op(n1400) );
  nand2_2 U638 ( .ip1(csr[2]), .ip2(n56), .op(n1401) );
  nand4_2 U639 ( .ip1(n1397), .ip2(n1396), .ip3(n1395), .ip4(n1394), .op(
        dout[29]) );
  nand2_2 U640 ( .ip1(buf1[29]), .ip2(n90), .op(n1394) );
  nand2_2 U641 ( .ip1(buf0[29]), .ip2(n63), .op(n1395) );
  nand2_2 U642 ( .ip1(int__29), .ip2(n41), .op(n1396) );
  nand2_2 U643 ( .ip1(n56), .ip2(csr[29]), .op(n1397) );
  nand4_2 U644 ( .ip1(n1393), .ip2(n1392), .ip3(n1391), .ip4(n1390), .op(
        dout[28]) );
  nand2_2 U645 ( .ip1(buf1[28]), .ip2(n90), .op(n1390) );
  nand2_2 U646 ( .ip1(buf0[28]), .ip2(n63), .op(n1391) );
  nand2_2 U647 ( .ip1(int__28), .ip2(n43), .op(n1392) );
  nand2_2 U648 ( .ip1(n56), .ip2(csr[28]), .op(n1393) );
  nand4_2 U649 ( .ip1(n1389), .ip2(n1388), .ip3(n1387), .ip4(n1386), .op(
        dout[27]) );
  nand2_2 U650 ( .ip1(buf1[27]), .ip2(n90), .op(n1386) );
  nand2_2 U651 ( .ip1(buf0[27]), .ip2(n63), .op(n1387) );
  nand2_2 U652 ( .ip1(int__27), .ip2(n47), .op(n1388) );
  nand2_2 U653 ( .ip1(n56), .ip2(csr[27]), .op(n1389) );
  nand4_2 U654 ( .ip1(n1385), .ip2(n1384), .ip3(n1383), .ip4(n1382), .op(
        dout[26]) );
  nand2_2 U655 ( .ip1(buf1[26]), .ip2(n90), .op(n1382) );
  nand2_2 U656 ( .ip1(buf0[26]), .ip2(n63), .op(n1383) );
  nand2_2 U657 ( .ip1(int__26), .ip2(n46), .op(n1384) );
  nand2_2 U658 ( .ip1(csr[26]), .ip2(n55), .op(n1385) );
  nand4_2 U659 ( .ip1(n1381), .ip2(n1380), .ip3(n1379), .ip4(n1378), .op(
        dout[25]) );
  nand2_2 U660 ( .ip1(buf1[25]), .ip2(n90), .op(n1378) );
  nand2_2 U661 ( .ip1(buf0[25]), .ip2(n63), .op(n1379) );
  nand2_2 U662 ( .ip1(int__25), .ip2(n43), .op(n1380) );
  nand2_2 U663 ( .ip1(csr[25]), .ip2(n55), .op(n1381) );
  nand4_2 U664 ( .ip1(n1377), .ip2(n1376), .ip3(n1375), .ip4(n1374), .op(
        dout[24]) );
  nand2_2 U665 ( .ip1(buf1[24]), .ip2(n90), .op(n1374) );
  nand2_2 U666 ( .ip1(buf0[24]), .ip2(n63), .op(n1375) );
  nand2_2 U667 ( .ip1(int__24), .ip2(n42), .op(n1376) );
  nand2_2 U668 ( .ip1(csr[24]), .ip2(n55), .op(n1377) );
  and2_2 U669 ( .ip1(n64), .ip2(buf0[23]), .op(n1372) );
  and2_2 U670 ( .ip1(n91), .ip2(buf1[23]), .op(n1373) );
  and2_2 U671 ( .ip1(n64), .ip2(buf0[22]), .op(n1370) );
  and2_2 U672 ( .ip1(n91), .ip2(buf1[22]), .op(n1371) );
  nand4_2 U673 ( .ip1(n1369), .ip2(n1368), .ip3(n1367), .ip4(n1366), .op(
        dout[21]) );
  nand2_2 U674 ( .ip1(buf1[21]), .ip2(n91), .op(n1366) );
  nand2_2 U675 ( .ip1(buf0[21]), .ip2(n63), .op(n1367) );
  nand2_2 U676 ( .ip1(int__21), .ip2(n41), .op(n1368) );
  nand2_2 U677 ( .ip1(csr[21]), .ip2(n55), .op(n1369) );
  nand4_2 U678 ( .ip1(n1365), .ip2(n1364), .ip3(n1363), .ip4(n1362), .op(
        dout[20]) );
  nand2_2 U679 ( .ip1(buf1[20]), .ip2(n91), .op(n1362) );
  nand2_2 U680 ( .ip1(buf0[20]), .ip2(n64), .op(n1363) );
  nand2_2 U681 ( .ip1(int__20), .ip2(n40), .op(n1364) );
  nand2_2 U682 ( .ip1(csr[20]), .ip2(n55), .op(n1365) );
  nand4_2 U683 ( .ip1(n1361), .ip2(n1360), .ip3(n1359), .ip4(n1358), .op(
        dout[1]) );
  nand2_2 U684 ( .ip1(buf1[1]), .ip2(n91), .op(n1358) );
  nand2_2 U685 ( .ip1(buf0[1]), .ip2(n64), .op(n1359) );
  nand2_2 U686 ( .ip1(n47), .ip2(int_[1]), .op(n1360) );
  nand2_2 U687 ( .ip1(csr[1]), .ip2(n55), .op(n1361) );
  nand4_2 U688 ( .ip1(n1357), .ip2(n1356), .ip3(n1355), .ip4(n1354), .op(
        dout[19]) );
  nand2_2 U689 ( .ip1(buf1[19]), .ip2(n91), .op(n1354) );
  nand2_2 U690 ( .ip1(buf0[19]), .ip2(n64), .op(n1355) );
  nand2_2 U691 ( .ip1(int__19), .ip2(n37), .op(n1356) );
  nand2_2 U692 ( .ip1(csr[19]), .ip2(n55), .op(n1357) );
  nand4_2 U693 ( .ip1(n1353), .ip2(n1352), .ip3(n1351), .ip4(n1350), .op(
        dout[18]) );
  nand2_2 U694 ( .ip1(buf1[18]), .ip2(n91), .op(n1350) );
  nand2_2 U695 ( .ip1(buf0[18]), .ip2(n64), .op(n1351) );
  nand2_2 U696 ( .ip1(int__18), .ip2(n50), .op(n1352) );
  nand2_2 U697 ( .ip1(csr[18]), .ip2(n55), .op(n1353) );
  nand4_2 U698 ( .ip1(n1349), .ip2(n1348), .ip3(n1347), .ip4(n1346), .op(
        dout[17]) );
  nand2_2 U699 ( .ip1(buf1[17]), .ip2(n91), .op(n1346) );
  nand2_2 U700 ( .ip1(buf0[17]), .ip2(n64), .op(n1347) );
  nand2_2 U701 ( .ip1(int__17), .ip2(n49), .op(n1348) );
  nand2_2 U702 ( .ip1(csr[17]), .ip2(n55), .op(n1349) );
  nand4_2 U703 ( .ip1(n1345), .ip2(n1344), .ip3(n1343), .ip4(n1342), .op(
        dout[16]) );
  nand2_2 U704 ( .ip1(buf1[16]), .ip2(n91), .op(n1342) );
  nand2_2 U705 ( .ip1(buf0[16]), .ip2(n64), .op(n1343) );
  nand2_2 U706 ( .ip1(int__16), .ip2(n48), .op(n1344) );
  nand2_2 U707 ( .ip1(csr[16]), .ip2(n55), .op(n1345) );
  and2_2 U708 ( .ip1(n64), .ip2(buf0[15]), .op(n1340) );
  and2_2 U709 ( .ip1(n91), .ip2(buf1[15]), .op(n1341) );
  nand2_2 U710 ( .ip1(n1339), .ip2(n1338), .op(dout[14]) );
  nand2_2 U711 ( .ip1(buf1[14]), .ip2(n91), .op(n1338) );
  nand2_2 U712 ( .ip1(buf0[14]), .ip2(n64), .op(n1339) );
  and2_2 U713 ( .ip1(n65), .ip2(buf0[13]), .op(n1336) );
  and2_2 U714 ( .ip1(n92), .ip2(buf1[13]), .op(n1337) );
  and2_2 U715 ( .ip1(n65), .ip2(buf0[12]), .op(n1334) );
  and2_2 U716 ( .ip1(n92), .ip2(buf1[12]), .op(n1335) );
  and2_2 U717 ( .ip1(n65), .ip2(buf0[11]), .op(n1332) );
  and2_2 U718 ( .ip1(n92), .ip2(buf1[11]), .op(n1333) );
  and2_2 U719 ( .ip1(n65), .ip2(buf0[10]), .op(n1330) );
  and2_2 U720 ( .ip1(n92), .ip2(buf1[10]), .op(n1331) );
  nand4_2 U721 ( .ip1(n1329), .ip2(n1328), .ip3(n1327), .ip4(n1326), .op(
        dout[0]) );
  nand2_2 U722 ( .ip1(buf1[0]), .ip2(n90), .op(n1326) );
  nor2_2 U723 ( .ip1(n1138), .ip2(n1139), .op(n1681) );
  nand2_2 U724 ( .ip1(buf0[0]), .ip2(n64), .op(n1327) );
  nor2_2 U725 ( .ip1(n1138), .ip2(adr[0]), .op(n1516) );
  nand2_2 U726 ( .ip1(n46), .ip2(int_[0]), .op(n1328) );
  nand2_2 U727 ( .ip1(csr[0]), .ip2(n55), .op(n1329) );
  nor2_2 U728 ( .ip1(adr[0]), .ip2(adr[1]), .op(n1432) );
  nor2_2 U737 ( .ip1(buf0_orig[30]), .ip2(buf0_orig[29]), .op(n1325) );
  and2_2 U738 ( .ip1(N319), .ip2(n1324), .op(N320) );
  nand4_2 U739 ( .ip1(n1069), .ip2(n1068), .ip3(n1323), .ip4(n1322), .op(n1324) );
  nor4_2 U740 ( .ip1(n1321), .ip2(csr[4]), .ip3(csr[6]), .ip4(csr[5]), .op(
        n1322) );
  or3_2 U741 ( .ip1(csr[8]), .ip2(csr[9]), .ip3(csr[7]), .op(n1321) );
  or3_2 U746 ( .ip1(n1318), .ip2(dma_out_cnt[6]), .ip3(dma_out_cnt[5]), .op(
        n1319) );
  or3_2 U747 ( .ip1(dma_out_cnt[8]), .ip2(dma_out_cnt[9]), .ip3(dma_out_cnt[7]), .op(n1318) );
  or3_2 U748 ( .ip1(dma_out_cnt[2]), .ip2(dma_out_cnt[4]), .ip3(dma_out_cnt[3]), .op(n1320) );
  nor2_2 U749 ( .ip1(n1824), .ip2(n984), .op(N271) );
  nor2_2 U750 ( .ip1(buf0_set), .ip2(buf0_rl), .op(n1824) );
  and2_2 U752 ( .ip1(int_[5]), .ip2(int__20), .op(n1316) );
  and2_2 U753 ( .ip1(int_[2]), .ip2(int__18), .op(n1317) );
  and2_2 U758 ( .ip1(int_[5]), .ip2(int__28), .op(n1313) );
  and2_2 U759 ( .ip1(int_[2]), .ip2(int__26), .op(n1314) );
  nand2_2 U762 ( .ip1(n1101), .ip2(n1102), .op(n1315) );
  and2_2 U764 ( .ip1(re), .ip2(n42), .op(N191) );
  inv_2 U820 ( .ip(rst), .op(n1889) );
  not_ab_or_c_or_d U859 ( .ip1(n80), .ip2(din[31]), .ip3(n1678), .ip4(n1677), 
        .op(n1679) );
  not_ab_or_c_or_d U860 ( .ip1(n80), .ip2(din[30]), .ip3(n1671), .ip4(n1670), 
        .op(n1672) );
  not_ab_or_c_or_d U861 ( .ip1(n80), .ip2(din[29]), .ip3(n1667), .ip4(n1666), 
        .op(n1668) );
  not_ab_or_c_or_d U862 ( .ip1(n80), .ip2(din[28]), .ip3(n1663), .ip4(n1662), 
        .op(n1664) );
  not_ab_or_c_or_d U863 ( .ip1(n80), .ip2(din[27]), .ip3(n1659), .ip4(n1658), 
        .op(n1660) );
  not_ab_or_c_or_d U864 ( .ip1(n80), .ip2(din[26]), .ip3(n1655), .ip4(n1654), 
        .op(n1656) );
  not_ab_or_c_or_d U865 ( .ip1(n80), .ip2(din[25]), .ip3(n1651), .ip4(n1650), 
        .op(n1652) );
  not_ab_or_c_or_d U866 ( .ip1(n80), .ip2(din[24]), .ip3(n1647), .ip4(n1646), 
        .op(n1648) );
  not_ab_or_c_or_d U867 ( .ip1(n80), .ip2(din[23]), .ip3(n1643), .ip4(n1642), 
        .op(n1644) );
  not_ab_or_c_or_d U868 ( .ip1(n80), .ip2(din[22]), .ip3(n1639), .ip4(n1638), 
        .op(n1640) );
  not_ab_or_c_or_d U869 ( .ip1(n80), .ip2(din[21]), .ip3(n1635), .ip4(n1634), 
        .op(n1636) );
  not_ab_or_c_or_d U870 ( .ip1(n80), .ip2(din[20]), .ip3(n1631), .ip4(n1630), 
        .op(n1632) );
  not_ab_or_c_or_d U871 ( .ip1(n80), .ip2(din[19]), .ip3(n1627), .ip4(n1626), 
        .op(n1628) );
  not_ab_or_c_or_d U872 ( .ip1(n80), .ip2(din[18]), .ip3(n1623), .ip4(n1622), 
        .op(n1624) );
  not_ab_or_c_or_d U873 ( .ip1(n79), .ip2(din[17]), .ip3(n1619), .ip4(n1618), 
        .op(n1620) );
  not_ab_or_c_or_d U874 ( .ip1(n80), .ip2(din[16]), .ip3(n1615), .ip4(n1614), 
        .op(n1616) );
  not_ab_or_c_or_d U875 ( .ip1(n78), .ip2(din[15]), .ip3(n1611), .ip4(n1610), 
        .op(n1612) );
  not_ab_or_c_or_d U876 ( .ip1(n79), .ip2(din[14]), .ip3(n1607), .ip4(n1606), 
        .op(n1608) );
  not_ab_or_c_or_d U877 ( .ip1(n80), .ip2(din[13]), .ip3(n1603), .ip4(n1602), 
        .op(n1604) );
  not_ab_or_c_or_d U878 ( .ip1(n78), .ip2(din[12]), .ip3(n1599), .ip4(n1598), 
        .op(n1600) );
  not_ab_or_c_or_d U879 ( .ip1(n79), .ip2(din[11]), .ip3(n1595), .ip4(n1594), 
        .op(n1596) );
  not_ab_or_c_or_d U880 ( .ip1(n80), .ip2(din[10]), .ip3(n1591), .ip4(n1590), 
        .op(n1592) );
  not_ab_or_c_or_d U881 ( .ip1(n78), .ip2(din[9]), .ip3(n1587), .ip4(n1586), 
        .op(n1588) );
  not_ab_or_c_or_d U882 ( .ip1(n79), .ip2(din[8]), .ip3(n1583), .ip4(n1582), 
        .op(n1584) );
  not_ab_or_c_or_d U883 ( .ip1(n80), .ip2(din[7]), .ip3(n1579), .ip4(n1578), 
        .op(n1580) );
  not_ab_or_c_or_d U884 ( .ip1(n78), .ip2(din[6]), .ip3(n1575), .ip4(n1574), 
        .op(n1576) );
  not_ab_or_c_or_d U885 ( .ip1(n79), .ip2(din[5]), .ip3(n1571), .ip4(n1570), 
        .op(n1572) );
  not_ab_or_c_or_d U886 ( .ip1(n80), .ip2(din[4]), .ip3(n1567), .ip4(n1566), 
        .op(n1568) );
  not_ab_or_c_or_d U887 ( .ip1(n78), .ip2(din[3]), .ip3(n1563), .ip4(n1562), 
        .op(n1564) );
  not_ab_or_c_or_d U888 ( .ip1(n79), .ip2(din[2]), .ip3(n1559), .ip4(n1558), 
        .op(n1560) );
  not_ab_or_c_or_d U889 ( .ip1(n80), .ip2(din[1]), .ip3(n1555), .ip4(n1554), 
        .op(n1556) );
  not_ab_or_c_or_d U890 ( .ip1(n80), .ip2(din[0]), .ip3(n1551), .ip4(n1550), 
        .op(n1552) );
  ab_or_c_or_d U891 ( .ip1(buf0_orig[0]), .ip2(n69), .ip3(n1548), .ip4(n1889), 
        .op(n1210) );
  ab_or_c_or_d U892 ( .ip1(buf0_orig[1]), .ip2(n72), .ip3(n1547), .ip4(n1889), 
        .op(n1209) );
  ab_or_c_or_d U893 ( .ip1(buf0_orig[2]), .ip2(n72), .ip3(n1546), .ip4(n1889), 
        .op(n1208) );
  ab_or_c_or_d U894 ( .ip1(buf0_orig[3]), .ip2(n72), .ip3(n1545), .ip4(n1889), 
        .op(n1207) );
  ab_or_c_or_d U895 ( .ip1(buf0_orig[4]), .ip2(n71), .ip3(n1544), .ip4(n1889), 
        .op(n1206) );
  ab_or_c_or_d U896 ( .ip1(buf0_orig[5]), .ip2(n71), .ip3(n1543), .ip4(n1889), 
        .op(n1205) );
  ab_or_c_or_d U897 ( .ip1(buf0_orig[6]), .ip2(n71), .ip3(n1542), .ip4(n1889), 
        .op(n1204) );
  ab_or_c_or_d U898 ( .ip1(buf0_orig[7]), .ip2(n71), .ip3(n1541), .ip4(n1889), 
        .op(n1203) );
  ab_or_c_or_d U899 ( .ip1(buf0_orig[8]), .ip2(n70), .ip3(n1540), .ip4(n1889), 
        .op(n1202) );
  ab_or_c_or_d U900 ( .ip1(buf0_orig[9]), .ip2(n70), .ip3(n1539), .ip4(n1889), 
        .op(n1201) );
  ab_or_c_or_d U901 ( .ip1(buf0_orig[10]), .ip2(n70), .ip3(n1538), .ip4(n1889), 
        .op(n1200) );
  ab_or_c_or_d U902 ( .ip1(buf0_orig[11]), .ip2(n70), .ip3(n1537), .ip4(n1889), 
        .op(n1199) );
  ab_or_c_or_d U903 ( .ip1(buf0_orig[12]), .ip2(n69), .ip3(n1536), .ip4(n1889), 
        .op(n1198) );
  ab_or_c_or_d U904 ( .ip1(buf0_orig[13]), .ip2(n69), .ip3(n1535), .ip4(n1889), 
        .op(n1197) );
  ab_or_c_or_d U905 ( .ip1(buf0_orig[14]), .ip2(n69), .ip3(n1534), .ip4(n1889), 
        .op(n1196) );
  ab_or_c_or_d U906 ( .ip1(buf0_orig[15]), .ip2(n68), .ip3(n1533), .ip4(n1889), 
        .op(n1195) );
  ab_or_c_or_d U907 ( .ip1(buf0_orig[16]), .ip2(n68), .ip3(n1532), .ip4(n1889), 
        .op(n1194) );
  ab_or_c_or_d U908 ( .ip1(buf0_orig[17]), .ip2(n68), .ip3(n1531), .ip4(n1889), 
        .op(n1193) );
  ab_or_c_or_d U909 ( .ip1(buf0_orig[18]), .ip2(n68), .ip3(n1530), .ip4(n1889), 
        .op(n1192) );
  ab_or_c_or_d U922 ( .ip1(buf0_orig[31]), .ip2(n67), .ip3(n1517), .ip4(n1889), 
        .op(n1179) );
  ab_or_c_or_d U923 ( .ip1(csr[22]), .ip2(n1474), .ip3(n1067), .ip4(n1473), 
        .op(n1159) );
  ab_or_c_or_d U924 ( .ip1(csr[9]), .ip2(n56), .ip3(n1427), .ip4(n1426), .op(
        dout[9]) );
  ab_or_c_or_d U925 ( .ip1(csr[8]), .ip2(n56), .ip3(n1425), .ip4(n1424), .op(
        dout[8]) );
  ab_or_c_or_d U926 ( .ip1(csr[7]), .ip2(n56), .ip3(n1423), .ip4(n1422), .op(
        dout[7]) );
  ab_or_c_or_d U927 ( .ip1(n57), .ip2(csr[31]), .ip3(n1405), .ip4(n1404), .op(
        dout[31]) );
  ab_or_c_or_d U928 ( .ip1(n57), .ip2(csr[30]), .ip3(n1403), .ip4(n1402), .op(
        dout[30]) );
  ab_or_c_or_d U929 ( .ip1(csr[23]), .ip2(n56), .ip3(n1373), .ip4(n1372), .op(
        dout[23]) );
  ab_or_c_or_d U930 ( .ip1(csr[22]), .ip2(n56), .ip3(n1371), .ip4(n1370), .op(
        dout[22]) );
  ab_or_c_or_d U931 ( .ip1(n57), .ip2(csr[15]), .ip3(n1341), .ip4(n1340), .op(
        dout[15]) );
  ab_or_c_or_d U932 ( .ip1(csr[13]), .ip2(n56), .ip3(n1337), .ip4(n1336), .op(
        dout[13]) );
  ab_or_c_or_d U933 ( .ip1(csr[12]), .ip2(n56), .ip3(n1335), .ip4(n1334), .op(
        dout[12]) );
  ab_or_c_or_d U934 ( .ip1(csr[11]), .ip2(n56), .ip3(n1333), .ip4(n1332), .op(
        dout[11]) );
  ab_or_c_or_d U935 ( .ip1(csr[10]), .ip2(n56), .ip3(n1331), .ip4(n1330), .op(
        dout[10]) );
  dp_2 inta_reg ( .ip(N221), .ck(wclk), .q(inta) );
  dp_2 intb_reg ( .ip(N222), .ck(wclk), .q(intb) );
  dp_2 \buf0_orig_m3_reg[10]  ( .ip(N345), .ck(wclk), .q(buf0_orig_m3[10]) );
  dp_2 \buf0_orig_m3_reg[9]  ( .ip(N344), .ck(wclk), .q(buf0_orig_m3[9]) );
  dp_2 \buf0_orig_m3_reg[8]  ( .ip(N343), .ck(wclk), .q(buf0_orig_m3[8]) );
  dp_2 \buf0_orig_m3_reg[7]  ( .ip(N342), .ck(wclk), .q(buf0_orig_m3[7]) );
  dp_2 \buf0_orig_m3_reg[0]  ( .ip(n989), .ck(wclk), .q(buf0_orig_m3[0]) );
  usbf_ep_rf_2_DW01_sub_1 sub_7204 ( .A(buf0_orig[30:19]), .B(dma_out_cnt), 
        .CI(1'b0), .DIFF({N332, N331, N330, N329, N328, N327, N326, N325, N324, 
        N323, N322, N321}) );
  usbf_ep_rf_2_DW01_sub_2 sub_7193 ( .A(dma_in_cnt), .B({1'b0, 1'b0, 1'b0, 
        csr[10:2]}), .CI(1'b0), .DIFF({N302, N301, N300, N299, N298, N297, 
        N296, N295, N294, N293, N292, N291}) );
  usbf_ep_rf_2_DW01_inc_0 add_7190_S2 ( .A(dma_in_cnt), .SUM({N289, N288, N287, 
        N286, N285, N284, N283, N282, N281, N280, N279, N278}) );
  usbf_ep_rf_2_DW01_add_0 add_7168 ( .A(dma_out_cnt), .B({1'b0, 1'b0, 1'b0, 
        csr[10:2]}), .CI(1'b0), .SUM({N253, N252, N251, N250, N249, N248, N247, 
        N246, N245, N244, N243, N242}) );
  usbf_ep_rf_2_DW01_dec_0 sub_7165_S2 ( .A(dma_out_cnt), .SUM({N240, N239, 
        N238, N237, N236, N235, N234, N233, N232, N231, N230, N229}) );
  dp_1 dma_in_buf_sz1_reg ( .ip(N320), .ck(n34), .q(dma_in_buf_sz1) );
  dp_1 \dma_out_left_reg[11]  ( .ip(N332), .ck(n33), .q(dma_out_left[11]) );
  dp_1 \dma_out_left_reg[10]  ( .ip(N331), .ck(n33), .q(dma_out_left[10]) );
  dp_1 \dma_out_left_reg[9]  ( .ip(N330), .ck(n32), .q(dma_out_left[9]) );
  dp_1 \dma_out_left_reg[0]  ( .ip(N321), .ck(n32), .q(dma_out_left[0]) );
  dp_1 set_r_reg ( .ip(N271), .ck(n31), .q(set_r) );
  dp_1 \dma_out_left_reg[1]  ( .ip(N322), .ck(n32), .q(dma_out_left[1]) );
  dp_1 \dma_out_left_reg[3]  ( .ip(N324), .ck(n32), .q(dma_out_left[3]) );
  dp_1 \dma_out_left_reg[5]  ( .ip(N326), .ck(n32), .q(dma_out_left[5]) );
  dp_1 \dma_out_left_reg[7]  ( .ip(N328), .ck(n32), .q(dma_out_left[7]) );
  dp_1 dma_out_buf_avail_reg ( .ip(N333), .ck(n33), .q(dma_out_buf_avail) );
  dp_1 \dma_out_left_reg[2]  ( .ip(N323), .ck(n32), .q(dma_out_left[2]) );
  dp_1 \dma_out_left_reg[4]  ( .ip(N325), .ck(n32), .q(dma_out_left[4]) );
  dp_1 \dma_out_left_reg[6]  ( .ip(N327), .ck(n32), .q(dma_out_left[6]) );
  dp_1 \buf0_orig_reg[31]  ( .ip(n1179), .ck(n22), .q(buf0_orig[31]) );
  dp_1 \buf0_orig_reg[18]  ( .ip(n1192), .ck(n23), .q(buf0_orig[18]) );
  dp_1 \buf0_orig_reg[17]  ( .ip(n1193), .ck(n23), .q(buf0_orig[17]) );
  dp_1 \buf0_orig_reg[16]  ( .ip(n1194), .ck(n23), .q(buf0_orig[16]) );
  dp_1 \buf0_orig_reg[15]  ( .ip(n1195), .ck(n23), .q(buf0_orig[15]) );
  dp_1 \buf0_orig_reg[14]  ( .ip(n1196), .ck(n23), .q(buf0_orig[14]) );
  dp_1 \buf0_orig_reg[13]  ( .ip(n1197), .ck(n24), .q(buf0_orig[13]) );
  dp_1 \buf0_orig_reg[12]  ( .ip(n1198), .ck(n24), .q(buf0_orig[12]) );
  dp_1 \buf0_orig_reg[11]  ( .ip(n1199), .ck(n24), .q(buf0_orig[11]) );
  dp_1 \buf0_orig_reg[10]  ( .ip(n1200), .ck(n24), .q(buf0_orig[10]) );
  dp_1 \buf0_orig_reg[9]  ( .ip(n1201), .ck(n24), .q(buf0_orig[9]) );
  dp_1 \buf0_orig_reg[8]  ( .ip(n1202), .ck(n24), .q(buf0_orig[8]) );
  dp_1 \buf0_orig_reg[7]  ( .ip(n1203), .ck(n24), .q(buf0_orig[7]) );
  dp_1 \buf0_orig_reg[6]  ( .ip(n1204), .ck(n24), .q(buf0_orig[6]) );
  dp_1 \buf0_orig_reg[5]  ( .ip(n1205), .ck(n24), .q(buf0_orig[5]) );
  dp_1 \buf0_orig_reg[4]  ( .ip(n1206), .ck(n24), .q(buf0_orig[4]) );
  dp_1 \buf0_orig_reg[3]  ( .ip(n1207), .ck(n24), .q(buf0_orig[3]) );
  dp_1 \buf0_orig_reg[2]  ( .ip(n1208), .ck(n24), .q(buf0_orig[2]) );
  dp_1 \buf0_orig_reg[1]  ( .ip(n1209), .ck(n24), .q(buf0_orig[1]) );
  dp_1 \buf0_orig_reg[0]  ( .ip(n1210), .ck(n25), .q(buf0_orig[0]) );
  dp_1 \dma_out_left_reg[8]  ( .ip(N329), .ck(n32), .q(dma_out_left[8]) );
  dp_1 \csr1_reg[7]  ( .ip(n1159), .ck(n21), .q(csr[22]) );
  dp_1 r5_reg ( .ip(r4), .ck(n31), .q(r5) );
  dp_1 int_re_reg ( .ip(N191), .ck(n30), .q(int_re) );
  dp_1 \int_stat_reg[4]  ( .ip(n1277), .ck(n30), .q(int_[4]) );
  dp_1 \int_stat_reg[3]  ( .ip(n1278), .ck(n30), .q(int_[3]) );
  dp_1 \csr0_reg[12]  ( .ip(n1141), .ck(n20), .q(csr[12]) );
  dp_1 \csr0_reg[11]  ( .ip(n1142), .ck(n20), .q(csr[11]) );
  dp_1 \csr1_reg[8]  ( .ip(n1158), .ck(n21), .q(csr[23]) );
  dp_1 \iena_reg[3]  ( .ip(n1169), .ck(n21), .q(int__27) );
  dp_1 \ienb_reg[3]  ( .ip(n1175), .ck(n22), .q(int__19) );
  dp_1 \uc_bsel_reg[1]  ( .ip(n1284), .ck(n30), .q(csr[31]) );
  dp_1 \uc_bsel_reg[0]  ( .ip(n1285), .ck(n31), .q(csr[30]) );
  dp_1 \csr1_reg[10]  ( .ip(n1156), .ck(n20), .q(csr[25]) );
  dp_1 \csr1_reg[9]  ( .ip(n1157), .ck(n20), .q(csr[24]) );
  dp_1 \csr1_reg[2]  ( .ip(n1164), .ck(n21), .q(csr[17]) );
  dp_1 \csr1_reg[1]  ( .ip(n1165), .ck(n21), .q(csr[16]) );
  dp_1 \iena_reg[5]  ( .ip(n1167), .ck(n21), .q(int__29) );
  dp_1 \iena_reg[1]  ( .ip(n1171), .ck(n22), .q(int__25) );
  dp_1 \iena_reg[0]  ( .ip(n1172), .ck(n22), .q(int__24) );
  dp_1 \ienb_reg[5]  ( .ip(n1173), .ck(n22), .q(int__21) );
  dp_1 \ienb_reg[1]  ( .ip(n1177), .ck(n22), .q(int__17) );
  dp_1 \ienb_reg[0]  ( .ip(n1178), .ck(n22), .q(int__16) );
  dp_1 \buf0_reg[29]  ( .ip(n1240), .ck(n25), .q(buf0[29]) );
  dp_1 \buf0_reg[28]  ( .ip(n1239), .ck(n25), .q(buf0[28]) );
  dp_1 \buf0_reg[27]  ( .ip(n1238), .ck(n25), .q(buf0[27]) );
  dp_1 \buf0_reg[26]  ( .ip(n1237), .ck(n25), .q(buf0[26]) );
  dp_1 \buf0_reg[25]  ( .ip(n1236), .ck(n25), .q(buf0[25]) );
  dp_1 \buf0_reg[24]  ( .ip(n1235), .ck(n25), .q(buf0[24]) );
  dp_1 \buf0_reg[21]  ( .ip(n1232), .ck(n25), .q(buf0[21]) );
  dp_1 \buf0_reg[20]  ( .ip(n1231), .ck(n26), .q(buf0[20]) );
  dp_1 \buf0_reg[19]  ( .ip(n1230), .ck(n26), .q(buf0[19]) );
  dp_1 \buf0_reg[18]  ( .ip(n1229), .ck(n26), .q(buf0[18]) );
  dp_1 \buf0_reg[17]  ( .ip(n1228), .ck(n26), .q(buf0[17]) );
  dp_1 \buf0_reg[16]  ( .ip(n1227), .ck(n26), .q(buf0[16]) );
  dp_1 \buf0_reg[14]  ( .ip(n1225), .ck(n26), .q(buf0[14]) );
  dp_1 \buf0_reg[6]  ( .ip(n1217), .ck(n27), .q(buf0[6]) );
  dp_1 \buf0_reg[5]  ( .ip(n1216), .ck(n27), .q(buf0[5]) );
  dp_1 \buf0_reg[4]  ( .ip(n1215), .ck(n27), .q(buf0[4]) );
  dp_1 \buf0_reg[3]  ( .ip(n1214), .ck(n27), .q(buf0[3]) );
  dp_1 \buf0_reg[2]  ( .ip(n1213), .ck(n27), .q(buf0[2]) );
  dp_1 \buf0_reg[1]  ( .ip(n1212), .ck(n27), .q(buf0[1]) );
  dp_1 \buf0_reg[0]  ( .ip(n1211), .ck(n27), .q(buf0[0]) );
  dp_1 \buf1_reg[29]  ( .ip(n1272), .ck(n27), .q(buf1[29]) );
  dp_1 \buf1_reg[28]  ( .ip(n1271), .ck(n27), .q(buf1[28]) );
  dp_1 \buf1_reg[27]  ( .ip(n1270), .ck(n28), .q(buf1[27]) );
  dp_1 \buf1_reg[26]  ( .ip(n1269), .ck(n28), .q(buf1[26]) );
  dp_1 \buf1_reg[25]  ( .ip(n1268), .ck(n28), .q(buf1[25]) );
  dp_1 \buf1_reg[24]  ( .ip(n1267), .ck(n28), .q(buf1[24]) );
  dp_1 \buf1_reg[21]  ( .ip(n1264), .ck(n28), .q(buf1[21]) );
  dp_1 \buf1_reg[20]  ( .ip(n1263), .ck(n28), .q(buf1[20]) );
  dp_1 \buf1_reg[19]  ( .ip(n1262), .ck(n28), .q(buf1[19]) );
  dp_1 \buf1_reg[18]  ( .ip(n1261), .ck(n28), .q(buf1[18]) );
  dp_1 \buf1_reg[17]  ( .ip(n1260), .ck(n28), .q(buf1[17]) );
  dp_1 \buf1_reg[16]  ( .ip(n1259), .ck(n28), .q(buf1[16]) );
  dp_1 \buf1_reg[14]  ( .ip(n1257), .ck(n29), .q(buf1[14]) );
  dp_1 \buf1_reg[6]  ( .ip(n1249), .ck(n29), .q(buf1[6]) );
  dp_1 \buf1_reg[5]  ( .ip(n1248), .ck(n29), .q(buf1[5]) );
  dp_1 \buf1_reg[4]  ( .ip(n1247), .ck(n29), .q(buf1[4]) );
  dp_1 \buf1_reg[3]  ( .ip(n1246), .ck(n29), .q(buf1[3]) );
  dp_1 \buf1_reg[2]  ( .ip(n1245), .ck(n29), .q(buf1[2]) );
  dp_1 \buf1_reg[1]  ( .ip(n1244), .ck(n30), .q(buf1[1]) );
  dp_1 \buf1_reg[0]  ( .ip(n1243), .ck(n30), .q(buf1[0]) );
  dp_1 \iena_reg[4]  ( .ip(n1168), .ck(n21), .q(int__28) );
  dp_1 \iena_reg[2]  ( .ip(n1170), .ck(n21), .q(int__26) );
  dp_1 \ienb_reg[4]  ( .ip(n1174), .ck(n22), .q(int__20) );
  dp_1 \ienb_reg[2]  ( .ip(n1176), .ck(n22), .q(int__18) );
  dp_1 \buf0_reg[31]  ( .ip(n1242), .ck(n25), .q(buf0[31]) );
  dp_1 \buf0_reg[30]  ( .ip(n1241), .ck(n25), .q(buf0[30]) );
  dp_1 \buf0_reg[23]  ( .ip(n1234), .ck(n25), .q(buf0[23]) );
  dp_1 \buf0_reg[22]  ( .ip(n1233), .ck(n25), .q(buf0[22]) );
  dp_1 \buf0_reg[15]  ( .ip(n1226), .ck(n26), .q(buf0[15]) );
  dp_1 \buf0_reg[13]  ( .ip(n1224), .ck(n26), .q(buf0[13]) );
  dp_1 \buf0_reg[12]  ( .ip(n1223), .ck(n26), .q(buf0[12]) );
  dp_1 \buf0_reg[11]  ( .ip(n1222), .ck(n26), .q(buf0[11]) );
  dp_1 \buf0_reg[10]  ( .ip(n1221), .ck(n26), .q(buf0[10]) );
  dp_1 \buf0_reg[9]  ( .ip(n1220), .ck(n26), .q(buf0[9]) );
  dp_1 \buf0_reg[8]  ( .ip(n1219), .ck(n26), .q(buf0[8]) );
  dp_1 \buf0_reg[7]  ( .ip(n1218), .ck(n27), .q(buf0[7]) );
  dp_1 \buf1_reg[31]  ( .ip(n1274), .ck(n27), .q(buf1[31]) );
  dp_1 \buf1_reg[30]  ( .ip(n1273), .ck(n27), .q(buf1[30]) );
  dp_1 \buf1_reg[23]  ( .ip(n1266), .ck(n28), .q(buf1[23]) );
  dp_1 \buf1_reg[22]  ( .ip(n1265), .ck(n28), .q(buf1[22]) );
  dp_1 \buf1_reg[15]  ( .ip(n1258), .ck(n28), .q(buf1[15]) );
  dp_1 \buf1_reg[13]  ( .ip(n1256), .ck(n29), .q(buf1[13]) );
  dp_1 \buf1_reg[12]  ( .ip(n1255), .ck(n29), .q(buf1[12]) );
  dp_1 \buf1_reg[11]  ( .ip(n1254), .ck(n29), .q(buf1[11]) );
  dp_1 \buf1_reg[10]  ( .ip(n1253), .ck(n29), .q(buf1[10]) );
  dp_1 \buf1_reg[9]  ( .ip(n1252), .ck(n29), .q(buf1[9]) );
  dp_1 \buf1_reg[8]  ( .ip(n1251), .ck(n29), .q(buf1[8]) );
  dp_1 \buf1_reg[7]  ( .ip(n1250), .ck(n29), .q(buf1[7]) );
  dp_1 \uc_dpd_reg[1]  ( .ip(n1282), .ck(n30), .q(csr[29]) );
  dp_1 \uc_dpd_reg[0]  ( .ip(n1283), .ck(n30), .q(csr[28]) );
  dp_1 ots_stop_reg ( .ip(n1140), .ck(n27), .q(csr[13]) );
  dp_1 \csr0_reg[0]  ( .ip(n1153), .ck(n20), .q(csr[0]) );
  dp_1 \csr1_reg[11]  ( .ip(n1155), .ck(n20), .q(csr[26]) );
  dp_1 \int_stat_reg[5]  ( .ip(n1276), .ck(n30), .q(int_[5]) );
  dp_1 \int_stat_reg[2]  ( .ip(n1279), .ck(n30), .q(int_[2]) );
  dp_1 \int_stat_reg[6]  ( .ip(n1275), .ck(n30), .q(int_[6]) );
  dp_1 \int_stat_reg[1]  ( .ip(n1280), .ck(n30), .q(int_[1]) );
  dp_1 \int_stat_reg[0]  ( .ip(n1281), .ck(n30), .q(int_[0]) );
  dp_1 ep_match_r_reg ( .ip(ep_match), .ck(n25), .q(ep_match_r) );
  dp_1 \buf0_orig_reg[27]  ( .ip(n1183), .ck(n22), .q(buf0_orig[27]) );
  dp_1 \csr1_reg[0]  ( .ip(n1166), .ck(n21), .q(csr[15]) );
  dp_1 \csr1_reg[6]  ( .ip(n1160), .ck(n21), .q(csr[21]) );
  dp_1 \csr1_reg[5]  ( .ip(n1161), .ck(n21), .q(csr[20]) );
  dp_1 \csr1_reg[4]  ( .ip(n1162), .ck(n21), .q(csr[19]) );
  dp_1 \csr1_reg[3]  ( .ip(n1163), .ck(n21), .q(csr[18]) );
  dp_1 \csr0_reg[8]  ( .ip(n1145), .ck(n34), .q(csr[8]) );
  dp_1 \csr0_reg[1]  ( .ip(n1152), .ck(n20), .q(csr[1]) );
  dp_1 \csr0_reg[6]  ( .ip(n1147), .ck(n34), .q(csr[6]) );
  dp_1 \csr0_reg[4]  ( .ip(n1149), .ck(n34), .q(csr[4]) );
  dp_1 \buf0_orig_reg[28]  ( .ip(n1182), .ck(n22), .q(buf0_orig[28]) );
  dp_1 \csr0_reg[10]  ( .ip(n1143), .ck(n34), .q(csr[10]) );
  dp_1 \buf0_orig_reg[25]  ( .ip(n1185), .ck(n23), .q(buf0_orig[25]) );
  dp_1 \buf0_orig_reg[23]  ( .ip(n1187), .ck(n23), .q(buf0_orig[23]) );
  dp_1 \dma_in_cnt_reg[8]  ( .ip(n1296), .ck(n34), .q(dma_in_cnt[8]) );
  dp_1 \dma_out_cnt_reg[11]  ( .ip(n1299), .ck(n31), .q(dma_out_cnt[11]) );
  dp_1 \dma_out_cnt_reg[4]  ( .ip(n1304), .ck(n31), .q(dma_out_cnt[4]) );
  dp_1 \dma_out_cnt_reg[6]  ( .ip(n1306), .ck(n31), .q(dma_out_cnt[6]) );
  dp_1 \dma_out_cnt_reg[3]  ( .ip(n1303), .ck(n31), .q(dma_out_cnt[3]) );
  dp_1 \dma_out_cnt_reg[5]  ( .ip(n1305), .ck(n31), .q(dma_out_cnt[5]) );
  dp_1 \dma_out_cnt_reg[7]  ( .ip(n1307), .ck(n32), .q(dma_out_cnt[7]) );
  dp_1 \dma_out_cnt_reg[2]  ( .ip(n1302), .ck(n31), .q(dma_out_cnt[2]) );
  dp_1 \dma_out_cnt_reg[8]  ( .ip(n1308), .ck(n32), .q(dma_out_cnt[8]) );
  dp_1 \dma_out_cnt_reg[1]  ( .ip(n1301), .ck(n31), .q(dma_out_cnt[1]) );
  dp_1 \dma_in_cnt_reg[9]  ( .ip(n1297), .ck(n34), .q(dma_in_cnt[9]) );
  dp_1 \buf0_orig_reg[26]  ( .ip(n1184), .ck(n23), .q(buf0_orig[26]) );
  dp_1 \buf0_orig_reg[24]  ( .ip(n1186), .ck(n23), .q(buf0_orig[24]) );
  dp_1 \dma_out_cnt_reg[9]  ( .ip(n1309), .ck(n32), .q(dma_out_cnt[9]) );
  dp_1 \dma_in_cnt_reg[11]  ( .ip(n1287), .ck(n33), .q(dma_in_cnt[11]) );
  dp_1 \dma_in_cnt_reg[5]  ( .ip(n1293), .ck(n33), .q(dma_in_cnt[5]) );
  dp_1 \dma_in_cnt_reg[10]  ( .ip(n1298), .ck(n33), .q(dma_in_cnt[10]) );
  dp_1 \dma_in_cnt_reg[3]  ( .ip(n1291), .ck(n33), .q(dma_in_cnt[3]) );
  dp_1 \dma_in_cnt_reg[7]  ( .ip(n1295), .ck(n33), .q(dma_in_cnt[7]) );
  dp_1 \buf0_orig_reg[19]  ( .ip(n1191), .ck(n23), .q(buf0_orig[19]) );
  dp_1 \csr1_reg[12]  ( .ip(n1154), .ck(n20), .q(csr[27]) );
  dp_1 \buf0_orig_reg[30]  ( .ip(n1180), .ck(n22), .q(buf0_orig[30]) );
  dp_1 \dma_out_cnt_reg[10]  ( .ip(n1310), .ck(n31), .q(dma_out_cnt[10]) );
  dp_1 \buf0_orig_reg[22]  ( .ip(n1188), .ck(n23), .q(buf0_orig[22]) );
  dp_1 \dma_in_cnt_reg[6]  ( .ip(n1294), .ck(n33), .q(dma_in_cnt[6]) );
  dp_1 \buf0_orig_reg[21]  ( .ip(n1189), .ck(n23), .q(buf0_orig[21]) );
  dp_1 \dma_in_cnt_reg[0]  ( .ip(n1288), .ck(n33), .q(dma_in_cnt[0]) );
  dp_1 \dma_out_cnt_reg[0]  ( .ip(n1300), .ck(n31), .q(dma_out_cnt[0]) );
  dp_1 \buf0_orig_reg[20]  ( .ip(n1190), .ck(n23), .q(buf0_orig[20]) );
  dp_1 \dma_in_cnt_reg[1]  ( .ip(n1289), .ck(n33), .q(dma_in_cnt[1]) );
  dp_1 \csr0_reg[9]  ( .ip(n1144), .ck(n20), .q(csr[9]) );
  dp_1 \csr0_reg[7]  ( .ip(n1146), .ck(n20), .q(csr[7]) );
  dp_1 \buf0_orig_reg[29]  ( .ip(n1181), .ck(n22), .q(buf0_orig[29]) );
  dp_1 \dma_in_cnt_reg[2]  ( .ip(n1290), .ck(n33), .q(dma_in_cnt[2]) );
  dp_1 \csr0_reg[5]  ( .ip(n1148), .ck(n20), .q(csr[5]) );
  dp_1 \dma_in_cnt_reg[4]  ( .ip(n1292), .ck(n33), .q(dma_in_cnt[4]) );
  dp_1 \csr0_reg[3]  ( .ip(n1150), .ck(n20), .q(csr[3]) );
  dp_1 \csr0_reg[2]  ( .ip(n1151), .ck(n20), .q(csr[2]) );
  dp_1 dma_ack_clr1_reg ( .ip(r4), .ck(wclk), .q(dma_ack_clr1) );
  dp_1 dma_req_out_hold_reg ( .ip(N272), .ck(wclk), .q(dma_req_out_hold) );
  dp_1 dma_req_in_hold_reg ( .ip(N348), .ck(wclk), .q(dma_req_in_hold) );
  dp_1 \buf0_orig_m3_reg[1]  ( .ip(N336), .ck(wclk), .q(buf0_orig_m3[1]) );
  dp_1 \buf0_orig_m3_reg[2]  ( .ip(N337), .ck(wclk), .q(buf0_orig_m3[2]) );
  dp_1 r4_reg ( .ip(dma_ack_wr1), .ck(n31), .q(r4) );
  dp_1 r1_reg ( .ip(N361), .ck(wclk), .q(r1) );
  dp_1 dma_ack_wr1_reg ( .ip(n1286), .ck(wclk), .q(dma_ack_wr1) );
  dp_1 r2_reg ( .ip(n1311), .ck(wclk), .q(r2) );
  dp_1 dma_req_r_reg ( .ip(n1312), .ck(wclk), .q(dma_req) );
  dp_2 dma_req_in_hold2_reg ( .ip(N347), .ck(wclk), .q(dma_req_in_hold2) );
  dp_1 \buf0_orig_m3_reg[11]  ( .ip(N346), .ck(wclk), .q(buf0_orig_m3[11]) );
  dp_1 \buf0_orig_m3_reg[6]  ( .ip(N341), .ck(wclk), .q(buf0_orig_m3[6]) );
  dp_1 \buf0_orig_m3_reg[5]  ( .ip(N340), .ck(wclk), .q(buf0_orig_m3[5]) );
  dp_1 \buf0_orig_m3_reg[3]  ( .ip(N338), .ck(wclk), .q(buf0_orig_m3[3]) );
  dp_2 \buf0_orig_m3_reg[4]  ( .ip(N339), .ck(wclk), .q(buf0_orig_m3[4]) );
  and2_2 U3 ( .ip1(n1821), .ip2(n19), .op(n1) );
  and2_1 U4 ( .ip1(buf0_orig_m3[3]), .ip2(n996), .op(n2) );
  and2_2 U5 ( .ip1(csr[27]), .ip2(n959), .op(n3) );
  nand2_1 U6 ( .ip1(dma_ack), .ip2(n971), .op(n972) );
  nand2_1 U7 ( .ip1(dma_req_in_hold2), .ip2(dma_req_in_hold), .op(n970) );
  and2_1 U8 ( .ip1(rst), .ip2(n973), .op(n1312) );
  nand2_1 U9 ( .ip1(n730), .ip2(buf0_orig_m3[4]), .op(n4) );
  nand2_1 U10 ( .ip1(n730), .ip2(buf0_orig_m3[4]), .op(n911) );
  nand2_1 U12 ( .ip1(n995), .ip2(buf0_orig_m3[5]), .op(n5) );
  nand2_1 U13 ( .ip1(n995), .ip2(buf0_orig_m3[5]), .op(n910) );
  nand2_1 U16 ( .ip1(n941), .ip2(n940), .op(n946) );
  nand2_1 U17 ( .ip1(n946), .ip2(n945), .op(n947) );
  inv_1 U18 ( .ip(n5), .op(n732) );
  nand2_1 U19 ( .ip1(n926), .ip2(n927), .op(n948) );
  nand2_1 U20 ( .ip1(r1), .ip2(n968), .op(n11) );
  inv_1 U21 ( .ip(n724), .op(n15) );
  nand2_1 U22 ( .ip1(dma_ack_wr1), .ip2(n949), .op(n14) );
  inv_2 U23 ( .ip(dma_ack), .op(n13) );
  nand2_1 U72 ( .ip1(n7), .ip2(n8), .op(N347) );
  nand2_1 U73 ( .ip1(n947), .ip2(n948), .op(n7) );
  nand2_1 U76 ( .ip1(buf0_orig_m3[11]), .ip2(n960), .op(n8) );
  nand2_1 U77 ( .ip1(dma_in_cnt[3]), .ip2(n729), .op(n908) );
  or2_1 U80 ( .ip1(n730), .ip2(buf0_orig_m3[4]), .op(n909) );
  nand2_1 U81 ( .ip1(n11), .ip2(n12), .op(n973) );
  nand2_1 U84 ( .ip1(dma_req), .ip2(n972), .op(n12) );
  nand2_1 U85 ( .ip1(n14), .ip2(n13), .op(n951) );
  inv_2 U88 ( .ip(int_re), .op(n19) );
  nand3_1 U89 ( .ip1(n925), .ip2(n923), .ip3(n924), .op(n926) );
  nand2_1 U92 ( .ip1(n922), .ip2(n921), .op(n923) );
  nand2_1 U93 ( .ip1(n910), .ip2(n911), .op(n912) );
  inv_1 U96 ( .ip(n4), .op(n731) );
  buf_2 U97 ( .ip(n1137), .op(n53) );
  buf_2 U100 ( .ip(n1137), .op(n52) );
  buf_2 U101 ( .ip(n1137), .op(n54) );
  inv_2 U104 ( .ip(n73), .op(n78) );
  inv_2 U105 ( .ip(n73), .op(n80) );
  inv_2 U108 ( .ip(n72), .op(n79) );
  buf_2 U109 ( .ip(n1777), .op(n93) );
  buf_2 U112 ( .ip(n1777), .op(n94) );
  buf_2 U113 ( .ip(n1674), .op(n81) );
  buf_2 U116 ( .ip(n1674), .op(n82) );
  nand2_2 U117 ( .ip1(n65), .ip2(we), .op(n1549) );
  buf_2 U118 ( .ip(n1883), .op(n99) );
  buf_2 U124 ( .ip(n1681), .op(n91) );
  buf_2 U125 ( .ip(n1676), .op(n87) );
  buf_2 U126 ( .ip(n1676), .op(n88) );
  buf_2 U179 ( .ip(n1681), .op(n90) );
  buf_2 U476 ( .ip(n1883), .op(n100) );
  buf_2 U513 ( .ip(n1885), .op(n101) );
  buf_2 U514 ( .ip(n1885), .op(n102) );
  inv_2 U515 ( .ip(n35), .op(n36) );
  and2_2 U517 ( .ip1(n16), .ip2(n15), .op(n927) );
  and2_1 U729 ( .ip1(n934), .ip2(n935), .op(n16) );
  nor3_2 U730 ( .ip1(n1884), .ip2(n99), .ip3(n1070), .op(n1885) );
  buf_2 U731 ( .ip(n1516), .op(n64) );
  buf_2 U732 ( .ip(n1487), .op(n59) );
  buf_2 U733 ( .ip(n1675), .op(n84) );
  buf_2 U734 ( .ip(n1488), .op(n60) );
  buf_2 U735 ( .ip(n1488), .op(n61) );
  buf_2 U736 ( .ip(n1675), .op(n85) );
  buf_2 U742 ( .ip(n1432), .op(n56) );
  buf_2 U743 ( .ip(n1487), .op(n58) );
  buf_2 U744 ( .ip(n1516), .op(n63) );
  buf_2 U745 ( .ip(n1432), .op(n55) );
  buf_2 U751 ( .ip(n1778), .op(n96) );
  buf_2 U754 ( .ip(n1778), .op(n97) );
  buf_2 U755 ( .ip(n1778), .op(n98) );
  inv_2 U756 ( .ip(n1884), .op(n35) );
  and2_2 U757 ( .ip1(n17), .ip2(n18), .op(n925) );
  or2_1 U760 ( .ip1(buf0_orig_m3[5]), .ip2(n995), .op(n17) );
  or2_1 U761 ( .ip1(buf0_orig_m3[6]), .ip2(n725), .op(n18) );
  nor2_2 U763 ( .ip1(n1889), .ip2(n1513), .op(n1512) );
  and3_2 U765 ( .ip1(we), .ip2(rst), .ip3(n40), .op(n1513) );
  nor3_2 U766 ( .ip1(csr[1]), .ip2(csr[3]), .ip3(csr[2]), .op(n1323) );
  nor3_2 U767 ( .ip1(csr[27]), .ip2(n123), .ip3(n959), .op(N348) );
  buf_1 U768 ( .ip(clk), .op(n20) );
  buf_1 U769 ( .ip(clk), .op(n21) );
  buf_1 U770 ( .ip(clk), .op(n22) );
  buf_1 U771 ( .ip(clk), .op(n23) );
  buf_1 U772 ( .ip(clk), .op(n24) );
  buf_1 U773 ( .ip(clk), .op(n25) );
  buf_1 U774 ( .ip(clk), .op(n26) );
  buf_1 U775 ( .ip(clk), .op(n27) );
  buf_1 U776 ( .ip(clk), .op(n28) );
  buf_1 U777 ( .ip(clk), .op(n29) );
  buf_1 U778 ( .ip(clk), .op(n30) );
  buf_1 U779 ( .ip(clk), .op(n31) );
  buf_1 U780 ( .ip(clk), .op(n32) );
  buf_1 U781 ( .ip(clk), .op(n33) );
  buf_1 U782 ( .ip(clk), .op(n34) );
  nor2_2 U783 ( .ip1(n1139), .ip2(adr[1]), .op(n37) );
  nor2_2 U784 ( .ip1(n1139), .ip2(adr[1]), .op(n38) );
  inv_1 U785 ( .ip(n37), .op(n39) );
  inv_1 U786 ( .ip(n44), .op(n40) );
  inv_1 U787 ( .ip(n45), .op(n41) );
  inv_1 U788 ( .ip(n39), .op(n42) );
  inv_1 U789 ( .ip(n39), .op(n43) );
  inv_1 U790 ( .ip(n38), .op(n44) );
  inv_1 U791 ( .ip(n38), .op(n45) );
  inv_1 U792 ( .ip(n44), .op(n46) );
  inv_1 U793 ( .ip(n44), .op(n47) );
  inv_1 U794 ( .ip(n45), .op(n48) );
  inv_1 U795 ( .ip(n45), .op(n49) );
  inv_1 U796 ( .ip(n39), .op(n50) );
  inv_1 U797 ( .ip(n44), .op(n51) );
  buf_1 U798 ( .ip(n1432), .op(n57) );
  buf_1 U799 ( .ip(n1488), .op(n62) );
  buf_1 U800 ( .ip(n1516), .op(n65) );
  buf_1 U801 ( .ip(n1549), .op(n66) );
  buf_1 U802 ( .ip(n1549), .op(n67) );
  buf_1 U803 ( .ip(n1549), .op(n68) );
  buf_1 U804 ( .ip(n1549), .op(n69) );
  buf_1 U805 ( .ip(n1549), .op(n70) );
  buf_1 U806 ( .ip(n1549), .op(n71) );
  buf_1 U807 ( .ip(n1549), .op(n72) );
  buf_1 U808 ( .ip(n1549), .op(n73) );
  buf_1 U809 ( .ip(n1549), .op(n74) );
  buf_1 U810 ( .ip(n1549), .op(n75) );
  buf_1 U811 ( .ip(n1549), .op(n76) );
  buf_1 U812 ( .ip(n1674), .op(n83) );
  buf_1 U813 ( .ip(n1675), .op(n86) );
  buf_1 U814 ( .ip(n1676), .op(n89) );
  buf_1 U815 ( .ip(n1681), .op(n92) );
  buf_1 U816 ( .ip(n1777), .op(n95) );
  nand2_2 U817 ( .ip1(n102), .ip2(dma_in_cnt[1]), .op(n103) );
  nand3_2 U818 ( .ip1(n1831), .ip2(n1830), .ip3(n103), .op(n1289) );
  nand2_2 U819 ( .ip1(dma_in_cnt[0]), .ip2(n101), .op(n104) );
  nand3_2 U821 ( .ip1(n1829), .ip2(n1828), .ip3(n104), .op(n1288) );
  nand2_2 U822 ( .ip1(n102), .ip2(dma_in_cnt[3]), .op(n105) );
  nand3_2 U823 ( .ip1(n1835), .ip2(n1834), .ip3(n105), .op(n1291) );
  nand2_2 U824 ( .ip1(n102), .ip2(dma_in_cnt[5]), .op(n106) );
  nand3_2 U825 ( .ip1(n1839), .ip2(n1838), .ip3(n106), .op(n1293) );
  nand2_2 U826 ( .ip1(n102), .ip2(dma_in_cnt[7]), .op(n107) );
  nand3_2 U827 ( .ip1(n1843), .ip2(n1842), .ip3(n107), .op(n1295) );
  nand2_2 U828 ( .ip1(n102), .ip2(dma_in_cnt[9]), .op(n108) );
  nand3_2 U829 ( .ip1(n1847), .ip2(n1846), .ip3(n108), .op(n1297) );
  nand2_2 U830 ( .ip1(n102), .ip2(dma_in_cnt[11]), .op(n109) );
  nand3_2 U831 ( .ip1(n1827), .ip2(n1826), .ip3(n109), .op(n1287) );
  nand2_2 U832 ( .ip1(n102), .ip2(dma_in_cnt[10]), .op(n110) );
  nand3_2 U833 ( .ip1(n1849), .ip2(n1848), .ip3(n110), .op(n1298) );
  nand2_2 U834 ( .ip1(n102), .ip2(dma_in_cnt[8]), .op(n111) );
  nand3_2 U835 ( .ip1(n1845), .ip2(n1844), .ip3(n111), .op(n1296) );
  nand2_2 U836 ( .ip1(n102), .ip2(dma_in_cnt[6]), .op(n112) );
  nand3_2 U837 ( .ip1(n1841), .ip2(n1840), .ip3(n112), .op(n1294) );
  nand2_2 U838 ( .ip1(n102), .ip2(dma_in_cnt[4]), .op(n113) );
  nand3_2 U839 ( .ip1(n1837), .ip2(n1836), .ip3(n113), .op(n1292) );
  nand2_2 U840 ( .ip1(n102), .ip2(dma_in_cnt[2]), .op(n114) );
  nand3_2 U841 ( .ip1(n1833), .ip2(n1832), .ip3(n114), .op(n1290) );
  ab_or_c_or_d U842 ( .ip1(n76), .ip2(buf0_orig[27]), .ip3(n1889), .ip4(n1521), 
        .op(n1183) );
  inv_2 U843 ( .ip(buf0_orig[27]), .op(n992) );
  ab_or_c_or_d U844 ( .ip1(n75), .ip2(buf0_orig[25]), .ip3(n1889), .ip4(n1523), 
        .op(n1185) );
  inv_2 U845 ( .ip(buf0_orig[25]), .op(n991) );
  ab_or_c_or_d U846 ( .ip1(n75), .ip2(buf0_orig[23]), .ip3(n1889), .ip4(n1525), 
        .op(n1187) );
  inv_2 U847 ( .ip(buf0_orig[23]), .op(n990) );
  ab_or_c_or_d U848 ( .ip1(n75), .ip2(buf0_orig[19]), .ip3(n1889), .ip4(n1529), 
        .op(n1191) );
  inv_2 U849 ( .ip(buf0_orig[19]), .op(n989) );
  inv_2 U850 ( .ip(dma_in_cnt[1]), .op(n997) );
  ab_or_c_or_d U851 ( .ip1(n75), .ip2(buf0_orig[20]), .ip3(n1889), .ip4(n1528), 
        .op(n1190) );
  ab_or_c_or_d U852 ( .ip1(n74), .ip2(buf0_orig[22]), .ip3(n1889), .ip4(n1526), 
        .op(n1188) );
  inv_2 U853 ( .ip(dma_in_cnt[3]), .op(n996) );
  ab_or_c_or_d U854 ( .ip1(n74), .ip2(buf0_orig[24]), .ip3(n1889), .ip4(n1524), 
        .op(n1186) );
  inv_2 U855 ( .ip(dma_in_cnt[5]), .op(n995) );
  ab_or_c_or_d U856 ( .ip1(n76), .ip2(buf0_orig[26]), .ip3(n1889), .ip4(n1522), 
        .op(n1184) );
  inv_2 U857 ( .ip(dma_in_cnt[7]), .op(n994) );
  ab_or_c_or_d U858 ( .ip1(n74), .ip2(buf0_orig[28]), .ip3(n1889), .ip4(n1520), 
        .op(n1182) );
  inv_2 U910 ( .ip(dma_in_cnt[9]), .op(n993) );
  ab_or_c_or_d U911 ( .ip1(n73), .ip2(buf0_orig[30]), .ip3(n1518), .ip4(n1889), 
        .op(n1180) );
  inv_2 U912 ( .ip(buf0_orig[30]), .op(n985) );
  ab_or_c_or_d U913 ( .ip1(n74), .ip2(buf0_orig[21]), .ip3(n1889), .ip4(n1527), 
        .op(n1189) );
  ab_or_c_or_d U914 ( .ip1(buf0_orig[29]), .ip2(n67), .ip3(n1889), .ip4(n1519), 
        .op(n1181) );
  nand2_2 U915 ( .ip1(n1487), .ip2(csr[15]), .op(n115) );
  nand2_2 U916 ( .ip1(n1489), .ip2(n115), .op(n1166) );
  inv_2 U917 ( .ip(csr[26]), .op(n959) );
  nor2_2 U918 ( .ip1(dma_out_cnt[11]), .ip2(dma_out_cnt[10]), .op(n117) );
  nor2_2 U919 ( .ip1(n1319), .ip2(n1320), .op(n116) );
  nand2_2 U920 ( .ip1(n117), .ip2(n116), .op(n954) );
  and2_2 U921 ( .ip1(n3), .ip2(n954), .op(N272) );
  nor2_2 U936 ( .ip1(buf0_orig[24]), .ip2(buf0_orig[23]), .op(n119) );
  nor2_2 U937 ( .ip1(buf0_orig[21]), .ip2(buf0_orig[22]), .op(n118) );
  nand2_2 U938 ( .ip1(n119), .ip2(n118), .op(n122) );
  inv_2 U939 ( .ip(buf0_orig[28]), .op(n986) );
  nor2_2 U940 ( .ip1(buf0_orig[26]), .ip2(buf0_orig[25]), .op(n120) );
  nand4_2 U941 ( .ip1(n1325), .ip2(n986), .ip3(n992), .ip4(n120), .op(n121) );
  nor2_2 U942 ( .ip1(n122), .ip2(n121), .op(n123) );
  nand2_2 U943 ( .ip1(buf0_orig[20]), .ip2(buf0_orig[19]), .op(n124) );
  nand2_2 U944 ( .ip1(n124), .ip2(n1065), .op(n711) );
  or2_2 U945 ( .ip1(buf0_orig[22]), .ip2(n711), .op(n707) );
  inv_2 U946 ( .ip(n707), .op(n125) );
  nand2_2 U947 ( .ip1(n125), .ip2(n990), .op(n709) );
  inv_2 U948 ( .ip(n709), .op(n167) );
  inv_2 U949 ( .ip(buf0_orig[24]), .op(n988) );
  nand2_2 U950 ( .ip1(n167), .ip2(n988), .op(n221) );
  inv_2 U951 ( .ip(n221), .op(n170) );
  nand2_2 U952 ( .ip1(n170), .ip2(n991), .op(n541) );
  inv_2 U953 ( .ip(n541), .op(n173) );
  inv_2 U954 ( .ip(buf0_orig[26]), .op(n987) );
  nand2_2 U955 ( .ip1(n173), .ip2(n987), .op(n191) );
  inv_2 U956 ( .ip(n191), .op(n176) );
  nand2_2 U957 ( .ip1(n176), .ip2(n992), .op(n197) );
  inv_2 U958 ( .ip(n197), .op(n179) );
  nand2_2 U959 ( .ip1(n179), .ip2(n986), .op(n203) );
  nor2_2 U960 ( .ip1(buf0_orig[29]), .ip2(n203), .op(n182) );
  xor2_2 U961 ( .ip1(buf0_orig[30]), .ip2(n182), .op(N346) );
  inv_2 U962 ( .ip(n203), .op(n185) );
  xor2_2 U963 ( .ip1(buf0_orig[29]), .ip2(n185), .op(N345) );
  nand2_2 U964 ( .ip1(n541), .ip2(buf0_orig[26]), .op(n188) );
  nand2_2 U965 ( .ip1(n191), .ip2(n188), .op(N342) );
  nand2_2 U966 ( .ip1(n191), .ip2(buf0_orig[27]), .op(n194) );
  nand2_2 U967 ( .ip1(n197), .ip2(n194), .op(N343) );
  nand2_2 U968 ( .ip1(n197), .ip2(buf0_orig[28]), .op(n200) );
  nand2_2 U969 ( .ip1(n203), .ip2(n200), .op(N344) );
  nand2_2 U970 ( .ip1(n709), .ip2(buf0_orig[24]), .op(n204) );
  nand2_2 U971 ( .ip1(n221), .ip2(n204), .op(N340) );
  nand2_2 U972 ( .ip1(n221), .ip2(buf0_orig[25]), .op(n540) );
  nand2_2 U973 ( .ip1(n541), .ip2(n540), .op(N341) );
  nand2_2 U974 ( .ip1(buf0_orig[22]), .ip2(n711), .op(n706) );
  nand2_2 U975 ( .ip1(n707), .ip2(n706), .op(N338) );
  nand2_2 U976 ( .ip1(n707), .ip2(buf0_orig[23]), .op(n708) );
  nand2_2 U977 ( .ip1(n709), .ip2(n708), .op(N339) );
  xor2_2 U978 ( .ip1(buf0_orig[20]), .ip2(buf0_orig[19]), .op(N336) );
  nand3_2 U979 ( .ip1(buf0_orig[21]), .ip2(buf0_orig[20]), .ip3(buf0_orig[19]), 
        .op(n710) );
  nand2_2 U980 ( .ip1(n711), .ip2(n710), .op(N337) );
  nand2_2 U981 ( .ip1(buf0_orig_m3[9]), .ip2(n993), .op(n934) );
  inv_2 U982 ( .ip(dma_in_cnt[8]), .op(n712) );
  nand2_2 U983 ( .ip1(buf0_orig_m3[8]), .ip2(n712), .op(n935) );
  inv_2 U984 ( .ip(dma_in_cnt[10]), .op(n713) );
  nand2_2 U985 ( .ip1(buf0_orig_m3[10]), .ip2(n713), .op(n942) );
  inv_2 U986 ( .ip(dma_in_cnt[6]), .op(n725) );
  nand2_2 U987 ( .ip1(buf0_orig_m3[6]), .ip2(n725), .op(n723) );
  nand2_2 U988 ( .ip1(buf0_orig_m3[7]), .ip2(n994), .op(n722) );
  nand3_2 U989 ( .ip1(n942), .ip2(n723), .ip3(n722), .op(n724) );
  inv_2 U990 ( .ip(buf0_orig_m3[3]), .op(n729) );
  inv_2 U991 ( .ip(dma_in_cnt[4]), .op(n730) );
  ab_or_c_or_d U992 ( .ip1(n909), .ip2(n908), .ip3(n732), .ip4(n731), .op(n924) );
  not_ab_or_c_or_d U993 ( .ip1(buf0_orig_m3[2]), .ip2(n1014), .ip3(n912), 
        .ip4(n2), .op(n922) );
  inv_2 U994 ( .ip(dma_in_cnt[0]), .op(n915) );
  nand3_2 U995 ( .ip1(n997), .ip2(n915), .ip3(buf0_orig_m3[0]), .op(n914) );
  inv_2 U996 ( .ip(buf0_orig_m3[1]), .op(n913) );
  nand2_2 U997 ( .ip1(n914), .ip2(n913), .op(n920) );
  nand2_2 U998 ( .ip1(buf0_orig_m3[0]), .ip2(n915), .op(n916) );
  nand2_2 U999 ( .ip1(n916), .ip2(dma_in_cnt[1]), .op(n919) );
  inv_2 U1000 ( .ip(buf0_orig_m3[2]), .op(n917) );
  nand2_2 U1001 ( .ip1(dma_in_cnt[2]), .ip2(n917), .op(n918) );
  nand3_2 U1002 ( .ip1(n920), .ip2(n919), .ip3(n918), .op(n921) );
  inv_2 U1003 ( .ip(buf0_orig_m3[10]), .op(n931) );
  inv_2 U1004 ( .ip(buf0_orig_m3[11]), .op(n928) );
  nand2_2 U1005 ( .ip1(dma_in_cnt[11]), .ip2(n928), .op(n943) );
  inv_2 U1006 ( .ip(n943), .op(n930) );
  nor2_2 U1007 ( .ip1(buf0_orig_m3[9]), .ip2(n993), .op(n929) );
  not_ab_or_c_or_d U1008 ( .ip1(dma_in_cnt[10]), .ip2(n931), .ip3(n930), .ip4(
        n929), .op(n941) );
  inv_2 U1009 ( .ip(buf0_orig_m3[8]), .op(n932) );
  nand2_2 U1010 ( .ip1(dma_in_cnt[8]), .ip2(n932), .op(n939) );
  inv_2 U1011 ( .ip(buf0_orig_m3[7]), .op(n933) );
  nand2_2 U1012 ( .ip1(dma_in_cnt[7]), .ip2(n933), .op(n938) );
  inv_2 U1013 ( .ip(n934), .op(n937) );
  inv_2 U1014 ( .ip(n935), .op(n936) );
  ab_or_c_or_d U1015 ( .ip1(n939), .ip2(n938), .ip3(n937), .ip4(n936), .op(
        n940) );
  inv_2 U1016 ( .ip(n942), .op(n944) );
  nand2_2 U1017 ( .ip1(n944), .ip2(n943), .op(n945) );
  inv_2 U1018 ( .ip(dma_in_cnt[11]), .op(n960) );
  inv_2 U1019 ( .ip(dma_ack_clr1), .op(n949) );
  inv_2 U1020 ( .ip(n1889), .op(n950) );
  and2_2 U1021 ( .ip1(n951), .ip2(n950), .op(n1286) );
  inv_2 U1022 ( .ip(r2), .op(n968) );
  nor2_2 U1023 ( .ip1(r4), .ip2(n968), .op(n952) );
  nor2_2 U1024 ( .ip1(r1), .ip2(n952), .op(n953) );
  nor2_2 U1025 ( .ip1(n1889), .ip2(n953), .op(n1311) );
  inv_2 U1026 ( .ip(n954), .op(n957) );
  inv_2 U1027 ( .ip(dma_out_cnt[1]), .op(n956) );
  inv_2 U1028 ( .ip(dma_out_cnt[0]), .op(n955) );
  nand3_2 U1029 ( .ip1(n957), .ip2(n956), .ip3(n955), .op(n958) );
  nand2_2 U1030 ( .ip1(n3), .ip2(n958), .op(n967) );
  nor2_2 U1031 ( .ip1(csr[27]), .ip2(n959), .op(n963) );
  nand2_2 U1032 ( .ip1(buf0_orig[30]), .ip2(n960), .op(n961) );
  nand2_2 U1033 ( .ip1(n1064), .ip2(n961), .op(n962) );
  nand2_2 U1034 ( .ip1(n963), .ip2(n962), .op(n966) );
  or2_2 U1035 ( .ip1(r2), .ip2(r4), .op(n965) );
  inv_2 U1036 ( .ip(r5), .op(n984) );
  nand2_2 U1037 ( .ip1(csr[15]), .ip2(n984), .op(n964) );
  not_ab_or_c_or_d U1038 ( .ip1(n967), .ip2(n966), .ip3(n965), .ip4(n964), 
        .op(N361) );
  inv_2 U1039 ( .ip(dma_req_out_hold), .op(n969) );
  mux2_1 U1040 ( .ip1(n970), .ip2(n969), .s(n3), .op(n971) );
  ab_or_c_or_d U1041 ( .ip1(int__19), .ip2(n1315), .ip3(n1316), .ip4(n1317), 
        .op(n978) );
  nand2_2 U1042 ( .ip1(int__17), .ip2(int_[1]), .op(n976) );
  nand2_2 U1043 ( .ip1(int__16), .ip2(int_[0]), .op(n975) );
  nand2_2 U1044 ( .ip1(int__21), .ip2(int_[6]), .op(n974) );
  nand3_2 U1045 ( .ip1(n976), .ip2(n975), .ip3(n974), .op(n977) );
  or2_2 U1046 ( .ip1(n978), .ip2(n977), .op(N222) );
  ab_or_c_or_d U1047 ( .ip1(int__27), .ip2(n1315), .ip3(n1313), .ip4(n1314), 
        .op(n983) );
  nand2_2 U1048 ( .ip1(int__25), .ip2(int_[1]), .op(n981) );
  nand2_2 U1049 ( .ip1(int__24), .ip2(int_[0]), .op(n980) );
  nand2_2 U1050 ( .ip1(int__29), .ip2(int_[6]), .op(n979) );
  nand3_2 U1051 ( .ip1(n981), .ip2(n980), .ip3(n979), .op(n982) );
  or2_2 U1052 ( .ip1(n983), .ip2(n982), .op(N221) );
  inv_2 U1053 ( .ip(csr[15]), .op(n1070) );
  nor2_1 U1054 ( .ip1(csr[8]), .ip2(n725), .op(n1009) );
  nor2_1 U1055 ( .ip1(csr[6]), .ip2(n730), .op(n1005) );
  nor2_1 U1056 ( .ip1(csr[4]), .ip2(n1014), .op(n1001) );
  not_ab_or_c_or_d U1057 ( .ip1(dma_in_cnt[1]), .ip2(n1039), .ip3(n1015), 
        .ip4(dma_in_cnt[0]), .op(n999) );
  nor2_1 U1058 ( .ip1(dma_in_cnt[1]), .ip2(n1039), .op(n998) );
  not_ab_or_c_or_d U1059 ( .ip1(csr[4]), .ip2(n1014), .ip3(n999), .ip4(n998), 
        .op(n1000) );
  not_ab_or_c_or_d U1060 ( .ip1(dma_in_cnt[3]), .ip2(n1016), .ip3(n1001), 
        .ip4(n1000), .op(n1003) );
  nor2_1 U1061 ( .ip1(dma_in_cnt[3]), .ip2(n1016), .op(n1002) );
  not_ab_or_c_or_d U1062 ( .ip1(csr[6]), .ip2(n730), .ip3(n1003), .ip4(n1002), 
        .op(n1004) );
  not_ab_or_c_or_d U1063 ( .ip1(dma_in_cnt[5]), .ip2(n1017), .ip3(n1005), 
        .ip4(n1004), .op(n1007) );
  nor2_1 U1064 ( .ip1(dma_in_cnt[5]), .ip2(n1017), .op(n1006) );
  not_ab_or_c_or_d U1065 ( .ip1(csr[8]), .ip2(n725), .ip3(n1007), .ip4(n1006), 
        .op(n1008) );
  not_ab_or_c_or_d U1066 ( .ip1(dma_in_cnt[7]), .ip2(n1018), .ip3(n1009), 
        .ip4(n1008), .op(n1011) );
  nor2_1 U1067 ( .ip1(dma_in_cnt[7]), .ip2(n1018), .op(n1010) );
  not_ab_or_c_or_d U1068 ( .ip1(csr[10]), .ip2(n712), .ip3(n1011), .ip4(n1010), 
        .op(n1013) );
  or3_1 U1069 ( .ip1(dma_in_cnt[9]), .ip2(dma_in_cnt[11]), .ip3(dma_in_cnt[10]), .op(n1012) );
  ab_or_c_or_d U1070 ( .ip1(dma_in_cnt[8]), .ip2(n1068), .ip3(n1013), .ip4(
        n1012), .op(N319) );
  inv_2 U1071 ( .ip(dma_in_cnt[2]), .op(n1014) );
  inv_2 U1072 ( .ip(csr[2]), .op(n1015) );
  inv_2 U1073 ( .ip(csr[5]), .op(n1016) );
  inv_2 U1074 ( .ip(csr[7]), .op(n1017) );
  inv_2 U1075 ( .ip(csr[9]), .op(n1018) );
  nor2_1 U1076 ( .ip1(csr[8]), .ip2(n1036), .op(n1030) );
  nor2_1 U1077 ( .ip1(csr[6]), .ip2(n1037), .op(n1026) );
  nor2_1 U1078 ( .ip1(csr[4]), .ip2(n1038), .op(n1022) );
  not_ab_or_c_or_d U1079 ( .ip1(dma_out_left[1]), .ip2(n1039), .ip3(n1015), 
        .ip4(dma_out_left[0]), .op(n1020) );
  nor2_1 U1080 ( .ip1(dma_out_left[1]), .ip2(n1039), .op(n1019) );
  not_ab_or_c_or_d U1081 ( .ip1(csr[4]), .ip2(n1038), .ip3(n1020), .ip4(n1019), 
        .op(n1021) );
  not_ab_or_c_or_d U1082 ( .ip1(dma_out_left[3]), .ip2(n1016), .ip3(n1022), 
        .ip4(n1021), .op(n1024) );
  nor2_1 U1083 ( .ip1(dma_out_left[3]), .ip2(n1016), .op(n1023) );
  not_ab_or_c_or_d U1084 ( .ip1(csr[6]), .ip2(n1037), .ip3(n1024), .ip4(n1023), 
        .op(n1025) );
  not_ab_or_c_or_d U1085 ( .ip1(dma_out_left[5]), .ip2(n1017), .ip3(n1026), 
        .ip4(n1025), .op(n1028) );
  nor2_1 U1086 ( .ip1(dma_out_left[5]), .ip2(n1017), .op(n1027) );
  not_ab_or_c_or_d U1087 ( .ip1(csr[8]), .ip2(n1036), .ip3(n1028), .ip4(n1027), 
        .op(n1029) );
  not_ab_or_c_or_d U1088 ( .ip1(dma_out_left[7]), .ip2(n1018), .ip3(n1030), 
        .ip4(n1029), .op(n1032) );
  nor2_1 U1089 ( .ip1(dma_out_left[7]), .ip2(n1018), .op(n1031) );
  not_ab_or_c_or_d U1090 ( .ip1(csr[10]), .ip2(n1035), .ip3(n1032), .ip4(n1031), .op(n1034) );
  or3_1 U1091 ( .ip1(dma_out_left[9]), .ip2(dma_out_left[11]), .ip3(
        dma_out_left[10]), .op(n1033) );
  ab_or_c_or_d U1092 ( .ip1(dma_out_left[8]), .ip2(n1068), .ip3(n1034), .ip4(
        n1033), .op(N333) );
  inv_2 U1093 ( .ip(dma_out_left[8]), .op(n1035) );
  inv_2 U1094 ( .ip(dma_out_left[6]), .op(n1036) );
  inv_2 U1095 ( .ip(dma_out_left[4]), .op(n1037) );
  inv_2 U1096 ( .ip(dma_out_left[2]), .op(n1038) );
  inv_2 U1097 ( .ip(csr[3]), .op(n1039) );
  nor2_1 U1098 ( .ip1(dma_in_cnt[10]), .ip2(n1066), .op(n1061) );
  nor2_1 U1099 ( .ip1(dma_in_cnt[8]), .ip2(n992), .op(n1057) );
  nor2_1 U1100 ( .ip1(dma_in_cnt[6]), .ip2(n991), .op(n1053) );
  nor2_1 U1101 ( .ip1(dma_in_cnt[4]), .ip2(n990), .op(n1049) );
  nor2_1 U1102 ( .ip1(dma_in_cnt[2]), .ip2(n1065), .op(n1045) );
  nor2_1 U1103 ( .ip1(n989), .ip2(dma_in_cnt[0]), .op(n1041) );
  and2_1 U1104 ( .ip1(n997), .ip2(n1041), .op(n1040) );
  nor2_1 U1105 ( .ip1(buf0_orig[20]), .ip2(n1040), .op(n1043) );
  nor2_1 U1106 ( .ip1(n1041), .ip2(n997), .op(n1042) );
  not_ab_or_c_or_d U1107 ( .ip1(dma_in_cnt[2]), .ip2(n1065), .ip3(n1043), 
        .ip4(n1042), .op(n1044) );
  not_ab_or_c_or_d U1108 ( .ip1(buf0_orig[22]), .ip2(n996), .ip3(n1045), .ip4(
        n1044), .op(n1047) );
  nor2_1 U1109 ( .ip1(buf0_orig[22]), .ip2(n996), .op(n1046) );
  not_ab_or_c_or_d U1110 ( .ip1(dma_in_cnt[4]), .ip2(n990), .ip3(n1047), .ip4(
        n1046), .op(n1048) );
  not_ab_or_c_or_d U1111 ( .ip1(buf0_orig[24]), .ip2(n995), .ip3(n1049), .ip4(
        n1048), .op(n1051) );
  nor2_1 U1112 ( .ip1(buf0_orig[24]), .ip2(n995), .op(n1050) );
  not_ab_or_c_or_d U1113 ( .ip1(dma_in_cnt[6]), .ip2(n991), .ip3(n1051), .ip4(
        n1050), .op(n1052) );
  not_ab_or_c_or_d U1114 ( .ip1(buf0_orig[26]), .ip2(n994), .ip3(n1053), .ip4(
        n1052), .op(n1055) );
  nor2_1 U1115 ( .ip1(buf0_orig[26]), .ip2(n994), .op(n1054) );
  not_ab_or_c_or_d U1116 ( .ip1(dma_in_cnt[8]), .ip2(n992), .ip3(n1055), .ip4(
        n1054), .op(n1056) );
  not_ab_or_c_or_d U1117 ( .ip1(buf0_orig[28]), .ip2(n993), .ip3(n1057), .ip4(
        n1056), .op(n1059) );
  nor2_1 U1118 ( .ip1(buf0_orig[28]), .ip2(n993), .op(n1058) );
  not_ab_or_c_or_d U1119 ( .ip1(dma_in_cnt[10]), .ip2(n1066), .ip3(n1059), 
        .ip4(n1058), .op(n1060) );
  or2_1 U1120 ( .ip1(n1061), .ip2(n1060), .op(n1063) );
  nand2_1 U1121 ( .ip1(dma_in_cnt[11]), .ip2(n985), .op(n1062) );
  nand2_1 U1122 ( .ip1(n1063), .ip2(n1062), .op(n1064) );
  inv_2 U1123 ( .ip(buf0_orig[21]), .op(n1065) );
  inv_2 U1124 ( .ip(buf0_orig[29]), .op(n1066) );
  inv_2 U1125 ( .ip(n1472), .op(n1067) );
  inv_2 U1126 ( .ip(csr[10]), .op(n1068) );
  inv_2 U1127 ( .ip(csr[0]), .op(n1069) );
  inv_2 U1128 ( .ip(buf0_orig[31]), .op(n1071) );
  inv_2 U1129 ( .ip(buf0_orig[22]), .op(n1072) );
  inv_2 U1130 ( .ip(buf0_orig[20]), .op(n1073) );
  inv_2 U1131 ( .ip(buf0_orig[18]), .op(n1074) );
  inv_2 U1132 ( .ip(buf0_orig[17]), .op(n1075) );
  inv_2 U1133 ( .ip(buf0_orig[16]), .op(n1076) );
  inv_2 U1134 ( .ip(buf0_orig[15]), .op(n1077) );
  inv_2 U1135 ( .ip(buf0_orig[14]), .op(n1078) );
  inv_2 U1136 ( .ip(buf0_orig[13]), .op(n1079) );
  inv_2 U1137 ( .ip(buf0_orig[12]), .op(n1080) );
  inv_2 U1138 ( .ip(buf0_orig[11]), .op(n1081) );
  inv_2 U1139 ( .ip(buf0_orig[10]), .op(n1082) );
  inv_2 U1140 ( .ip(buf0_orig[9]), .op(n1083) );
  inv_2 U1141 ( .ip(buf0_orig[8]), .op(n1084) );
  inv_2 U1142 ( .ip(buf0_orig[7]), .op(n1085) );
  inv_2 U1143 ( .ip(buf0_orig[6]), .op(n1086) );
  inv_2 U1144 ( .ip(buf0_orig[5]), .op(n1087) );
  inv_2 U1145 ( .ip(buf0_orig[4]), .op(n1088) );
  inv_2 U1146 ( .ip(buf0_orig[3]), .op(n1089) );
  inv_2 U1147 ( .ip(buf0_orig[2]), .op(n1090) );
  inv_2 U1148 ( .ip(buf0_orig[1]), .op(n1091) );
  inv_2 U1149 ( .ip(buf0_orig[0]), .op(n1092) );
  inv_2 U1150 ( .ip(n1783), .op(n1093) );
  inv_2 U1151 ( .ip(n1787), .op(n1094) );
  inv_2 U1152 ( .ip(n1799), .op(n1095) );
  inv_2 U1153 ( .ip(n1803), .op(n1096) );
  inv_2 U1154 ( .ip(n1808), .op(n1097) );
  inv_2 U1155 ( .ip(ep_match_r), .op(n1098) );
  inv_2 U1156 ( .ip(n1814), .op(n1099) );
  inv_2 U1157 ( .ip(n1820), .op(n1100) );
  inv_2 U1158 ( .ip(int_[4]), .op(n1101) );
  inv_2 U1159 ( .ip(int_[3]), .op(n1102) );
  inv_2 U1160 ( .ip(buf0_rl), .op(n1103) );
  inv_2 U1161 ( .ip(n1824), .op(n1104) );
  inv_2 U1162 ( .ip(idin[30]), .op(n1105) );
  inv_2 U1163 ( .ip(idin[29]), .op(n1106) );
  inv_2 U1164 ( .ip(idin[28]), .op(n1107) );
  inv_2 U1165 ( .ip(idin[3]), .op(n1108) );
  inv_2 U1166 ( .ip(idin[2]), .op(n1109) );
  inv_2 U1167 ( .ip(idin[16]), .op(n1110) );
  inv_2 U1168 ( .ip(idin[15]), .op(n1111) );
  inv_2 U1169 ( .ip(idin[14]), .op(n1112) );
  inv_2 U1170 ( .ip(idin[13]), .op(n1113) );
  inv_2 U1171 ( .ip(idin[12]), .op(n1114) );
  inv_2 U1172 ( .ip(idin[11]), .op(n1115) );
  inv_2 U1173 ( .ip(idin[10]), .op(n1116) );
  inv_2 U1174 ( .ip(idin[9]), .op(n1117) );
  inv_2 U1175 ( .ip(idin[8]), .op(n1118) );
  inv_2 U1176 ( .ip(idin[7]), .op(n1119) );
  inv_2 U1177 ( .ip(idin[6]), .op(n1120) );
  inv_2 U1178 ( .ip(idin[5]), .op(n1121) );
  inv_2 U1179 ( .ip(idin[4]), .op(n1122) );
  inv_2 U1180 ( .ip(idin[31]), .op(n1123) );
  inv_2 U1181 ( .ip(idin[1]), .op(n1124) );
  inv_2 U1182 ( .ip(idin[0]), .op(n1125) );
  inv_2 U1183 ( .ip(idin[27]), .op(n1126) );
  inv_2 U1184 ( .ip(idin[26]), .op(n1127) );
  inv_2 U1185 ( .ip(idin[25]), .op(n1128) );
  inv_2 U1186 ( .ip(idin[24]), .op(n1129) );
  inv_2 U1187 ( .ip(idin[23]), .op(n1130) );
  inv_2 U1188 ( .ip(idin[22]), .op(n1131) );
  inv_2 U1189 ( .ip(idin[21]), .op(n1132) );
  inv_2 U1190 ( .ip(idin[20]), .op(n1133) );
  inv_2 U1191 ( .ip(idin[19]), .op(n1134) );
  inv_2 U1192 ( .ip(idin[18]), .op(n1135) );
  inv_2 U1193 ( .ip(idin[17]), .op(n1136) );
  inv_2 U1194 ( .ip(n1682), .op(n1137) );
  inv_2 U1195 ( .ip(adr[1]), .op(n1138) );
  inv_2 U1196 ( .ip(adr[0]), .op(n1139) );
endmodule


module usbf_ep_rf_1_DW01_sub_1 ( A, B, CI, DIFF, CO );
  input [11:0] A;
  input [11:0] B;
  output [11:0] DIFF;
  input CI;
  output CO;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13;
  wire   [12:0] carry;

  fulladder U2_11 ( .a(A[11]), .b(n2), .ci(carry[11]), .s(DIFF[11]) );
  fulladder U2_10 ( .a(A[10]), .b(n3), .ci(carry[10]), .co(carry[11]), .s(
        DIFF[10]) );
  fulladder U2_9 ( .a(A[9]), .b(n13), .ci(carry[9]), .co(carry[10]), .s(
        DIFF[9]) );
  fulladder U2_8 ( .a(A[8]), .b(n12), .ci(carry[8]), .co(carry[9]), .s(DIFF[8]) );
  fulladder U2_7 ( .a(A[7]), .b(n11), .ci(carry[7]), .co(carry[8]), .s(DIFF[7]) );
  fulladder U2_6 ( .a(A[6]), .b(n10), .ci(carry[6]), .co(carry[7]), .s(DIFF[6]) );
  fulladder U2_5 ( .a(A[5]), .b(n9), .ci(carry[5]), .co(carry[6]), .s(DIFF[5])
         );
  fulladder U2_4 ( .a(A[4]), .b(n8), .ci(carry[4]), .co(carry[5]), .s(DIFF[4])
         );
  fulladder U2_3 ( .a(A[3]), .b(n7), .ci(carry[3]), .co(carry[4]), .s(DIFF[3])
         );
  fulladder U2_2 ( .a(A[2]), .b(n6), .ci(carry[2]), .co(carry[3]), .s(DIFF[2])
         );
  fulladder U2_1 ( .a(A[1]), .b(n5), .ci(n1), .co(carry[2]), .s(DIFF[1]) );
  or2_2 U1 ( .ip1(A[0]), .ip2(n4), .op(n1) );
  xnor2_1 U2 ( .ip1(n4), .ip2(A[0]), .op(DIFF[0]) );
  inv_2 U3 ( .ip(B[11]), .op(n2) );
  inv_2 U4 ( .ip(B[10]), .op(n3) );
  inv_2 U5 ( .ip(B[0]), .op(n4) );
  inv_2 U6 ( .ip(B[1]), .op(n5) );
  inv_2 U7 ( .ip(B[2]), .op(n6) );
  inv_2 U8 ( .ip(B[3]), .op(n7) );
  inv_2 U9 ( .ip(B[4]), .op(n8) );
  inv_2 U10 ( .ip(B[5]), .op(n9) );
  inv_2 U11 ( .ip(B[6]), .op(n10) );
  inv_2 U12 ( .ip(B[7]), .op(n11) );
  inv_2 U13 ( .ip(B[8]), .op(n12) );
  inv_2 U14 ( .ip(B[9]), .op(n13) );
endmodule


module usbf_ep_rf_1_DW01_sub_2 ( A, B, CI, DIFF, CO );
  input [11:0] A;
  input [11:0] B;
  output [11:0] DIFF;
  input CI;
  output CO;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12;
  wire   [12:0] carry;

  fulladder U2_8 ( .a(A[8]), .b(n4), .ci(carry[8]), .co(carry[9]), .s(DIFF[8])
         );
  fulladder U2_7 ( .a(A[7]), .b(n5), .ci(carry[7]), .co(carry[8]), .s(DIFF[7])
         );
  fulladder U2_6 ( .a(A[6]), .b(n6), .ci(carry[6]), .co(carry[7]), .s(DIFF[6])
         );
  fulladder U2_5 ( .a(A[5]), .b(n7), .ci(carry[5]), .co(carry[6]), .s(DIFF[5])
         );
  fulladder U2_4 ( .a(A[4]), .b(n8), .ci(carry[4]), .co(carry[5]), .s(DIFF[4])
         );
  fulladder U2_3 ( .a(A[3]), .b(n9), .ci(carry[3]), .co(carry[4]), .s(DIFF[3])
         );
  fulladder U2_2 ( .a(A[2]), .b(n10), .ci(carry[2]), .co(carry[3]), .s(DIFF[2]) );
  fulladder U2_1 ( .a(A[1]), .b(n11), .ci(n2), .co(carry[2]), .s(DIFF[1]) );
  or2_2 U1 ( .ip1(carry[9]), .ip2(A[9]), .op(n1) );
  or2_2 U2 ( .ip1(A[0]), .ip2(n12), .op(n2) );
  nor2_2 U3 ( .ip1(n1), .ip2(A[10]), .op(n3) );
  xnor2_1 U4 ( .ip1(A[10]), .ip2(n1), .op(DIFF[10]) );
  xor2_2 U5 ( .ip1(A[11]), .ip2(n3), .op(DIFF[11]) );
  xnor2_1 U6 ( .ip1(A[9]), .ip2(carry[9]), .op(DIFF[9]) );
  xnor2_1 U7 ( .ip1(n12), .ip2(A[0]), .op(DIFF[0]) );
  inv_2 U8 ( .ip(B[8]), .op(n4) );
  inv_2 U9 ( .ip(B[7]), .op(n5) );
  inv_2 U10 ( .ip(B[6]), .op(n6) );
  inv_2 U11 ( .ip(B[5]), .op(n7) );
  inv_2 U12 ( .ip(B[4]), .op(n8) );
  inv_2 U13 ( .ip(B[3]), .op(n9) );
  inv_2 U14 ( .ip(B[2]), .op(n10) );
  inv_2 U15 ( .ip(B[1]), .op(n11) );
  inv_2 U16 ( .ip(B[0]), .op(n12) );
endmodule


module usbf_ep_rf_1_DW01_inc_0 ( A, SUM );
  input [11:0] A;
  output [11:0] SUM;
  wire   n1, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16;

  inv_2 U1 ( .ip(A[10]), .op(n1) );
  inv_2 U2 ( .ip(A[0]), .op(SUM[0]) );
  inv_2 U3 ( .ip(A[3]), .op(n3) );
  inv_2 U4 ( .ip(A[4]), .op(n4) );
  inv_2 U5 ( .ip(A[7]), .op(n5) );
  inv_2 U6 ( .ip(A[8]), .op(n6) );
  xor2_1 U7 ( .ip1(A[9]), .ip2(n7), .op(SUM[9]) );
  xor2_1 U8 ( .ip1(A[8]), .ip2(n8), .op(SUM[8]) );
  nor2_1 U9 ( .ip1(n9), .ip2(n5), .op(n8) );
  xor2_1 U10 ( .ip1(n9), .ip2(n5), .op(SUM[7]) );
  xnor2_1 U11 ( .ip1(n10), .ip2(A[6]), .op(SUM[6]) );
  nand2_1 U12 ( .ip1(A[5]), .ip2(n11), .op(n10) );
  xor2_1 U13 ( .ip1(A[5]), .ip2(n11), .op(SUM[5]) );
  xor2_1 U14 ( .ip1(A[4]), .ip2(n12), .op(SUM[4]) );
  nor2_1 U15 ( .ip1(n13), .ip2(n3), .op(n12) );
  xor2_1 U16 ( .ip1(n13), .ip2(n3), .op(SUM[3]) );
  xnor2_1 U17 ( .ip1(n14), .ip2(A[2]), .op(SUM[2]) );
  nand2_1 U18 ( .ip1(A[1]), .ip2(A[0]), .op(n14) );
  xor2_1 U19 ( .ip1(A[1]), .ip2(A[0]), .op(SUM[1]) );
  xor2_1 U20 ( .ip1(A[11]), .ip2(n15), .op(SUM[11]) );
  nor2_1 U21 ( .ip1(n16), .ip2(n1), .op(n15) );
  xor2_1 U22 ( .ip1(n16), .ip2(n1), .op(SUM[10]) );
  nand2_1 U23 ( .ip1(n7), .ip2(A[9]), .op(n16) );
  nor3_1 U24 ( .ip1(n5), .ip2(n9), .ip3(n6), .op(n7) );
  nand3_1 U25 ( .ip1(A[5]), .ip2(n11), .ip3(A[6]), .op(n9) );
  nor3_1 U26 ( .ip1(n3), .ip2(n13), .ip3(n4), .op(n11) );
  nand3_1 U27 ( .ip1(A[1]), .ip2(A[0]), .ip3(A[2]), .op(n13) );
endmodule


module usbf_ep_rf_1_DW01_add_0 ( A, B, CI, SUM, CO );
  input [11:0] A;
  input [11:0] B;
  output [11:0] SUM;
  input CI;
  output CO;
  wire   n1, n2, n3;
  wire   [11:1] carry;

  fulladder U1_8 ( .a(A[8]), .b(B[8]), .ci(carry[8]), .co(carry[9]), .s(SUM[8]) );
  fulladder U1_7 ( .a(A[7]), .b(B[7]), .ci(carry[7]), .co(carry[8]), .s(SUM[7]) );
  fulladder U1_6 ( .a(A[6]), .b(B[6]), .ci(carry[6]), .co(carry[7]), .s(SUM[6]) );
  fulladder U1_5 ( .a(A[5]), .b(B[5]), .ci(carry[5]), .co(carry[6]), .s(SUM[5]) );
  fulladder U1_4 ( .a(A[4]), .b(B[4]), .ci(carry[4]), .co(carry[5]), .s(SUM[4]) );
  fulladder U1_3 ( .a(A[3]), .b(B[3]), .ci(carry[3]), .co(carry[4]), .s(SUM[3]) );
  fulladder U1_2 ( .a(A[2]), .b(B[2]), .ci(carry[2]), .co(carry[3]), .s(SUM[2]) );
  fulladder U1_1 ( .a(A[1]), .b(B[1]), .ci(n2), .co(carry[2]), .s(SUM[1]) );
  and2_2 U1 ( .ip1(A[9]), .ip2(carry[9]), .op(n1) );
  and2_2 U2 ( .ip1(B[0]), .ip2(A[0]), .op(n2) );
  and2_2 U3 ( .ip1(A[10]), .ip2(n1), .op(n3) );
  xor2_2 U4 ( .ip1(A[11]), .ip2(n3), .op(SUM[11]) );
  xor2_2 U5 ( .ip1(A[10]), .ip2(n1), .op(SUM[10]) );
  xor2_2 U6 ( .ip1(A[9]), .ip2(carry[9]), .op(SUM[9]) );
  xor2_2 U7 ( .ip1(B[0]), .ip2(A[0]), .op(SUM[0]) );
endmodule


module usbf_ep_rf_1_DW01_dec_0 ( A, SUM );
  input [11:0] A;
  output [11:0] SUM;
  wire   n1, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21;

  inv_2 U1 ( .ip(n21), .op(n1) );
  inv_2 U2 ( .ip(A[0]), .op(SUM[0]) );
  nand2_1 U3 ( .ip1(n1), .ip2(n3), .op(SUM[9]) );
  nand2_1 U4 ( .ip1(A[9]), .ip2(n4), .op(n3) );
  nand2_1 U5 ( .ip1(n4), .ip2(n5), .op(SUM[8]) );
  nand2_1 U6 ( .ip1(A[8]), .ip2(n6), .op(n5) );
  nand2_1 U7 ( .ip1(n6), .ip2(n7), .op(SUM[7]) );
  nand2_1 U8 ( .ip1(A[7]), .ip2(n8), .op(n7) );
  nand2_1 U9 ( .ip1(n8), .ip2(n9), .op(SUM[6]) );
  nand2_1 U10 ( .ip1(A[6]), .ip2(n10), .op(n9) );
  nand2_1 U11 ( .ip1(n10), .ip2(n11), .op(SUM[5]) );
  nand2_1 U12 ( .ip1(A[5]), .ip2(n12), .op(n11) );
  nand2_1 U13 ( .ip1(n12), .ip2(n13), .op(SUM[4]) );
  nand2_1 U14 ( .ip1(A[4]), .ip2(n14), .op(n13) );
  nand2_1 U15 ( .ip1(n14), .ip2(n15), .op(SUM[3]) );
  nand2_1 U16 ( .ip1(A[3]), .ip2(n16), .op(n15) );
  nand2_1 U17 ( .ip1(n16), .ip2(n17), .op(SUM[2]) );
  nand2_1 U18 ( .ip1(A[2]), .ip2(n18), .op(n17) );
  nand2_1 U19 ( .ip1(n18), .ip2(n19), .op(SUM[1]) );
  nand2_1 U20 ( .ip1(A[1]), .ip2(A[0]), .op(n19) );
  xor2_1 U21 ( .ip1(A[11]), .ip2(n20), .op(SUM[11]) );
  nor2_1 U22 ( .ip1(A[10]), .ip2(n1), .op(n20) );
  xor2_1 U23 ( .ip1(A[10]), .ip2(n21), .op(SUM[10]) );
  nor2_1 U24 ( .ip1(n4), .ip2(A[9]), .op(n21) );
  or2_1 U25 ( .ip1(n6), .ip2(A[8]), .op(n4) );
  or2_1 U26 ( .ip1(n8), .ip2(A[7]), .op(n6) );
  or2_1 U27 ( .ip1(n10), .ip2(A[6]), .op(n8) );
  or2_1 U28 ( .ip1(n12), .ip2(A[5]), .op(n10) );
  or2_1 U29 ( .ip1(n14), .ip2(A[4]), .op(n12) );
  or2_1 U30 ( .ip1(n16), .ip2(A[3]), .op(n14) );
  or2_1 U31 ( .ip1(n18), .ip2(A[2]), .op(n16) );
  or2_1 U32 ( .ip1(A[1]), .ip2(A[0]), .op(n18) );
endmodule


module usbf_ep_rf_1 ( clk, wclk, rst, adr, re, we, din, dout, inta, intb, 
        dma_req, dma_ack, idin, ep_sel, ep_match, buf0_rl, buf0_set, buf1_set, 
        uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, 
        int_crc16_set, int_to_set, int_seqerr_set, out_to_small, csr, buf0, 
        buf1, dma_in_buf_sz1, dma_out_buf_avail );
  input [1:0] adr;
  input [31:0] din;
  output [31:0] dout;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  input clk, wclk, rst, re, we, dma_ack, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small;
  output inta, intb, dma_req, ep_match, dma_in_buf_sz1, dma_out_buf_avail;
  wire   int__29, int__28, int__27, int__26, int__25, int__24, int__21,
         int__20, int__19, int__18, int__17, int__16, ep_match_r, N191, int_re,
         N221, N222, set_r, N229, N230, N231, N232, N233, N234, N235, N236,
         N237, N238, N239, N240, N242, N243, N244, N245, N246, N247, N248,
         N249, N250, N251, N252, N253, N271, N272, dma_req_out_hold, N278,
         N279, N280, N281, N282, N283, N284, N285, N286, N287, N288, N289,
         N291, N292, N293, N294, N295, N296, N297, N298, N299, N300, N301,
         N302, N319, N320, N321, N322, N323, N324, N325, N326, N327, N328,
         N329, N330, N331, N332, N333, N336, N337, N338, N339, N340, N341,
         N342, N343, N344, N345, N346, N347, dma_req_in_hold2, N348,
         dma_req_in_hold, r1, r2, r4, r5, N361, dma_ack_clr1, dma_ack_wr1, n1,
         n2, n3, n4, n5, n7, n8, n11, n12, n13, n14, n15, n16, n17, n18, n19,
         n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33,
         n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47,
         n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61,
         n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75,
         n76, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90,
         n91, n92, n93, n94, n95, n96, n97, n98, n99, n100, n101, n102, n103,
         n104, n105, n106, n107, n108, n109, n110, n111, n112, n113, n114,
         n115, n116, n117, n118, n119, n120, n121, n122, n123, n124, n125,
         n167, n170, n173, n176, n179, n182, n185, n188, n191, n194, n197,
         n200, n203, n204, n221, n540, n541, n706, n707, n708, n709, n710,
         n711, n712, n713, n722, n723, n724, n725, n729, n730, n731, n732,
         n908, n909, n910, n911, n912, n913, n914, n915, n916, n917, n918,
         n919, n920, n921, n922, n923, n924, n925, n926, n927, n928, n929,
         n930, n931, n932, n933, n934, n935, n936, n937, n938, n939, n940,
         n941, n942, n943, n944, n945, n946, n947, n948, n949, n950, n951,
         n952, n953, n954, n955, n956, n957, n958, n959, n960, n961, n962,
         n963, n964, n965, n966, n967, n968, n969, n970, n971, n972, n973,
         n974, n975, n976, n977, n978, n979, n980, n981, n982, n983, n984,
         n985, n986, n987, n988, n989, n990, n991, n992, n993, n994, n995,
         n996, n997, n998, n999, n1000, n1001, n1002, n1003, n1004, n1005,
         n1006, n1007, n1008, n1009, n1010, n1011, n1012, n1013, n1014, n1015,
         n1016, n1017, n1018, n1019, n1020, n1021, n1022, n1023, n1024, n1025,
         n1026, n1027, n1028, n1029, n1030, n1031, n1032, n1033, n1034, n1035,
         n1036, n1037, n1038, n1039, n1040, n1041, n1042, n1043, n1044, n1045,
         n1046, n1047, n1048, n1049, n1050, n1051, n1052, n1053, n1054, n1055,
         n1056, n1057, n1058, n1059, n1060, n1061, n1062, n1063, n1064, n1065,
         n1066, n1067, n1068, n1069, n1070, n1071, n1072, n1073, n1074, n1075,
         n1076, n1077, n1078, n1079, n1080, n1081, n1082, n1083, n1084, n1085,
         n1086, n1087, n1088, n1089, n1090, n1091, n1092, n1093, n1094, n1095,
         n1096, n1097, n1098, n1099, n1100, n1101, n1102, n1103, n1104, n1105,
         n1106, n1107, n1108, n1109, n1110, n1111, n1112, n1113, n1114, n1115,
         n1116, n1117, n1118, n1119, n1120, n1121, n1122, n1123, n1124, n1125,
         n1126, n1127, n1128, n1129, n1130, n1131, n1132, n1133, n1134, n1135,
         n1136, n1137, n1138, n1139, n1140, n1141, n1142, n1143, n1144, n1145,
         n1146, n1147, n1148, n1149, n1150, n1151, n1152, n1153, n1154, n1155,
         n1156, n1157, n1158, n1159, n1160, n1161, n1162, n1163, n1164, n1165,
         n1166, n1167, n1168, n1169, n1170, n1171, n1172, n1173, n1174, n1175,
         n1176, n1177, n1178, n1179, n1180, n1181, n1182, n1183, n1184, n1185,
         n1186, n1187, n1188, n1189, n1190, n1191, n1192, n1193, n1194, n1195,
         n1196, n1197, n1198, n1199, n1200, n1201, n1202, n1203, n1204, n1205,
         n1206, n1207, n1208, n1209, n1210, n1211, n1212, n1213, n1214, n1215,
         n1216, n1217, n1218, n1219, n1220, n1221, n1222, n1223, n1224, n1225,
         n1226, n1227, n1228, n1229, n1230, n1231, n1232, n1233, n1234, n1235,
         n1236, n1237, n1238, n1239, n1240, n1241, n1242, n1243, n1244, n1245,
         n1246, n1247, n1248, n1249, n1250, n1251, n1252, n1253, n1254, n1255,
         n1256, n1257, n1258, n1259, n1260, n1261, n1262, n1263, n1264, n1265,
         n1266, n1267, n1268, n1269, n1270, n1271, n1272, n1273, n1274, n1275,
         n1276, n1277, n1278, n1279, n1280, n1281, n1282, n1283, n1284, n1285,
         n1286, n1287, n1288, n1289, n1290, n1291, n1292, n1293, n1294, n1295,
         n1296, n1297, n1298, n1299, n1300, n1301, n1302, n1303, n1304, n1305,
         n1306, n1307, n1308, n1309, n1310, n1311, n1312, n1313, n1314, n1315,
         n1316, n1317, n1318, n1319, n1320, n1321, n1322, n1323, n1324, n1325,
         n1326, n1327, n1328, n1329, n1330, n1331, n1332, n1333, n1334, n1335,
         n1336, n1337, n1338, n1339, n1340, n1341, n1342, n1343, n1344, n1345,
         n1346, n1347, n1348, n1349, n1350, n1351, n1352, n1353, n1354, n1355,
         n1356, n1357, n1358, n1359, n1360, n1361, n1362, n1363, n1364, n1365,
         n1366, n1367, n1368, n1369, n1370, n1371, n1372, n1373, n1374, n1375,
         n1376, n1377, n1378, n1379, n1380, n1381, n1382, n1383, n1384, n1385,
         n1386, n1387, n1388, n1389, n1390, n1391, n1392, n1393, n1394, n1395,
         n1396, n1397, n1398, n1399, n1400, n1401, n1402, n1403, n1404, n1405,
         n1406, n1407, n1408, n1409, n1410, n1411, n1412, n1413, n1414, n1415,
         n1416, n1417, n1418, n1419, n1420, n1421, n1422, n1423, n1424, n1425,
         n1426, n1427, n1428, n1429, n1430, n1431, n1432, n1433, n1434, n1435,
         n1436, n1437, n1438, n1439, n1440, n1441, n1442, n1443, n1444, n1445,
         n1446, n1447, n1448, n1449, n1450, n1451, n1452, n1453, n1454, n1455,
         n1456, n1457, n1458, n1459, n1460, n1461, n1462, n1463, n1464, n1465,
         n1466, n1467, n1468, n1469, n1470, n1471, n1472, n1473, n1474, n1475,
         n1476, n1477, n1478, n1479, n1480, n1481, n1482, n1483, n1484, n1485,
         n1486, n1487, n1488, n1489, n1490, n1491, n1492, n1493, n1494, n1495,
         n1496, n1497, n1498, n1499, n1500, n1501, n1502, n1503, n1504, n1505,
         n1506, n1507, n1508, n1509, n1510, n1511, n1512, n1513, n1514, n1515,
         n1516, n1517, n1518, n1519, n1520, n1521, n1522, n1523, n1524, n1525,
         n1526, n1527, n1528, n1529, n1530, n1531, n1532, n1533, n1534, n1535,
         n1536, n1537, n1538, n1539, n1540, n1541, n1542, n1543, n1544, n1545,
         n1546, n1547, n1548, n1549, n1550, n1551, n1552, n1553, n1554, n1555,
         n1556, n1557, n1558, n1559, n1560, n1561, n1562, n1563, n1564, n1565,
         n1566, n1567, n1568, n1569, n1570, n1571, n1572, n1573, n1574, n1575,
         n1576, n1577, n1578, n1579, n1580, n1581, n1582, n1583, n1584, n1585,
         n1586, n1587, n1588, n1589, n1590, n1591, n1592, n1593, n1594, n1595,
         n1596, n1597, n1598, n1599, n1600, n1601, n1602, n1603, n1604, n1605,
         n1606, n1607, n1608, n1609, n1610, n1611, n1612, n1613, n1614, n1615,
         n1616, n1617, n1618, n1619, n1620, n1621, n1622, n1623, n1624, n1625,
         n1626, n1627, n1628, n1629, n1630, n1631, n1632, n1633, n1634, n1635,
         n1636, n1637, n1638, n1639, n1640, n1641, n1642, n1643, n1644, n1645,
         n1646, n1647, n1648, n1649, n1650, n1651, n1652, n1653, n1654, n1655,
         n1656, n1657, n1658, n1659, n1660, n1661, n1662, n1663, n1664, n1665,
         n1666, n1667, n1668, n1669, n1670, n1671, n1672, n1673, n1674, n1675,
         n1676, n1677, n1678, n1679, n1680, n1681, n1682, n1683, n1684, n1685,
         n1686, n1687, n1688, n1689, n1690, n1691, n1692, n1693, n1694, n1695,
         n1696, n1697, n1698, n1699, n1700, n1701, n1702, n1703, n1704, n1705,
         n1706, n1707, n1708, n1709, n1710, n1711, n1712, n1713, n1714, n1715,
         n1716, n1717, n1718, n1719, n1720, n1721, n1722, n1723, n1724, n1725,
         n1726, n1727, n1728, n1729, n1730, n1731, n1732, n1733, n1734, n1735,
         n1736, n1737, n1738, n1739, n1740, n1741, n1742, n1743, n1744, n1745,
         n1746, n1747, n1748, n1749, n1750, n1751, n1752, n1753, n1754, n1755,
         n1756, n1757, n1758, n1759, n1760, n1761, n1762, n1763, n1764, n1765,
         n1766, n1767, n1768, n1769, n1770, n1771, n1772, n1773, n1774, n1775,
         n1776, n1777, n1778, n1779, n1780, n1781, n1782, n1783, n1784, n1785,
         n1786, n1787, n1788, n1789, n1790, n1791, n1792, n1793, n1794, n1795,
         n1796, n1797, n1798, n1799, n1800, n1801, n1802, n1803, n1804, n1805,
         n1806, n1807, n1808, n1809, n1810, n1811, n1812, n1813, n1814, n1815,
         n1816, n1817, n1818, n1819, n1820, n1821, n1822, n1823, n1824, n1825,
         n1826, n1827, n1828, n1829, n1830, n1831, n1832, n1833, n1834, n1835,
         n1836, n1837, n1838, n1839, n1840, n1841, n1842, n1843, n1844, n1845,
         n1846, n1847, n1848, n1849, n1850, n1851, n1852, n1853, n1854, n1855,
         n1856, n1857, n1858, n1859, n1860, n1861, n1862, n1863, n1864, n1865,
         n1866, n1867, n1868, n1869, n1870, n1871, n1872, n1873, n1874, n1875,
         n1876, n1877, n1878, n1879, n1880, n1881, n1882, n1883, n1884, n1885,
         n1886, n1887, n1888, n1889;
  wire   [6:0] int_;
  wire   [31:0] buf0_orig;
  wire   [11:0] dma_out_cnt;
  wire   [11:0] dma_in_cnt;
  wire   [11:0] dma_out_left;
  wire   [11:0] buf0_orig_m3;
  assign csr[14] = 1'b0;

  nand3_2 U24 ( .ip1(n1888), .ip2(n1887), .ip3(n1886), .op(n1310) );
  nand2_2 U25 ( .ip1(dma_out_cnt[10]), .ip2(n101), .op(n1886) );
  nand2_2 U26 ( .ip1(N252), .ip2(n36), .op(n1887) );
  nand2_2 U27 ( .ip1(N239), .ip2(n100), .op(n1888) );
  nand3_2 U28 ( .ip1(n1882), .ip2(n1881), .ip3(n1880), .op(n1309) );
  nand2_2 U29 ( .ip1(dma_out_cnt[9]), .ip2(n101), .op(n1880) );
  nand2_2 U30 ( .ip1(N251), .ip2(n1884), .op(n1881) );
  nand2_2 U31 ( .ip1(N238), .ip2(n99), .op(n1882) );
  nand3_2 U32 ( .ip1(n1879), .ip2(n1878), .ip3(n1877), .op(n1308) );
  nand2_2 U33 ( .ip1(dma_out_cnt[8]), .ip2(n101), .op(n1877) );
  nand2_2 U34 ( .ip1(N250), .ip2(n36), .op(n1878) );
  nand2_2 U35 ( .ip1(N237), .ip2(n99), .op(n1879) );
  nand3_2 U36 ( .ip1(n1876), .ip2(n1875), .ip3(n1874), .op(n1307) );
  nand2_2 U37 ( .ip1(dma_out_cnt[7]), .ip2(n101), .op(n1874) );
  nand2_2 U38 ( .ip1(N249), .ip2(n1884), .op(n1875) );
  nand2_2 U39 ( .ip1(N236), .ip2(n99), .op(n1876) );
  nand3_2 U40 ( .ip1(n1873), .ip2(n1872), .ip3(n1871), .op(n1306) );
  nand2_2 U41 ( .ip1(dma_out_cnt[6]), .ip2(n101), .op(n1871) );
  nand2_2 U42 ( .ip1(N248), .ip2(n36), .op(n1872) );
  nand2_2 U43 ( .ip1(N235), .ip2(n99), .op(n1873) );
  nand3_2 U44 ( .ip1(n1870), .ip2(n1869), .ip3(n1868), .op(n1305) );
  nand2_2 U45 ( .ip1(dma_out_cnt[5]), .ip2(n101), .op(n1868) );
  nand2_2 U46 ( .ip1(N247), .ip2(n1884), .op(n1869) );
  nand2_2 U47 ( .ip1(N234), .ip2(n99), .op(n1870) );
  nand3_2 U48 ( .ip1(n1867), .ip2(n1866), .ip3(n1865), .op(n1304) );
  nand2_2 U49 ( .ip1(dma_out_cnt[4]), .ip2(n101), .op(n1865) );
  nand2_2 U50 ( .ip1(N246), .ip2(n36), .op(n1866) );
  nand2_2 U51 ( .ip1(N233), .ip2(n99), .op(n1867) );
  nand3_2 U52 ( .ip1(n1864), .ip2(n1863), .ip3(n1862), .op(n1303) );
  nand2_2 U53 ( .ip1(dma_out_cnt[3]), .ip2(n101), .op(n1862) );
  nand2_2 U54 ( .ip1(N245), .ip2(n1884), .op(n1863) );
  nand2_2 U55 ( .ip1(N232), .ip2(n99), .op(n1864) );
  nand3_2 U56 ( .ip1(n1861), .ip2(n1860), .ip3(n1859), .op(n1302) );
  nand2_2 U57 ( .ip1(dma_out_cnt[2]), .ip2(n101), .op(n1859) );
  nand2_2 U58 ( .ip1(N244), .ip2(n1884), .op(n1860) );
  nand2_2 U59 ( .ip1(N231), .ip2(n99), .op(n1861) );
  nand3_2 U60 ( .ip1(n1858), .ip2(n1857), .ip3(n1856), .op(n1301) );
  nand2_2 U61 ( .ip1(dma_out_cnt[1]), .ip2(n101), .op(n1856) );
  nand2_2 U62 ( .ip1(N243), .ip2(n1884), .op(n1857) );
  nand2_2 U63 ( .ip1(N230), .ip2(n99), .op(n1858) );
  nand3_2 U64 ( .ip1(n1855), .ip2(n1854), .ip3(n1853), .op(n1300) );
  nand2_2 U65 ( .ip1(dma_out_cnt[0]), .ip2(n102), .op(n1853) );
  nand2_2 U66 ( .ip1(N242), .ip2(n1884), .op(n1854) );
  nand2_2 U67 ( .ip1(N229), .ip2(n99), .op(n1855) );
  nand3_2 U68 ( .ip1(n1852), .ip2(n1851), .ip3(n1850), .op(n1299) );
  nand2_2 U69 ( .ip1(dma_out_cnt[11]), .ip2(n101), .op(n1850) );
  nand2_2 U70 ( .ip1(N253), .ip2(n1884), .op(n1851) );
  nand2_2 U71 ( .ip1(N240), .ip2(n99), .op(n1852) );
  nand2_2 U74 ( .ip1(N301), .ip2(n36), .op(n1848) );
  nand2_2 U75 ( .ip1(N288), .ip2(n100), .op(n1849) );
  nand2_2 U78 ( .ip1(N300), .ip2(n1884), .op(n1846) );
  nand2_2 U79 ( .ip1(N287), .ip2(n100), .op(n1847) );
  nand2_2 U82 ( .ip1(N299), .ip2(n36), .op(n1844) );
  nand2_2 U83 ( .ip1(N286), .ip2(n100), .op(n1845) );
  nand2_2 U86 ( .ip1(N298), .ip2(n36), .op(n1842) );
  nand2_2 U87 ( .ip1(N285), .ip2(n100), .op(n1843) );
  nand2_2 U90 ( .ip1(N297), .ip2(n36), .op(n1840) );
  nand2_2 U91 ( .ip1(N284), .ip2(n100), .op(n1841) );
  nand2_2 U94 ( .ip1(N296), .ip2(n1884), .op(n1838) );
  nand2_2 U95 ( .ip1(N283), .ip2(n100), .op(n1839) );
  nand2_2 U98 ( .ip1(N295), .ip2(n36), .op(n1836) );
  nand2_2 U99 ( .ip1(N282), .ip2(n100), .op(n1837) );
  nand2_2 U102 ( .ip1(N294), .ip2(n1884), .op(n1834) );
  nand2_2 U103 ( .ip1(N281), .ip2(n100), .op(n1835) );
  nand2_2 U106 ( .ip1(N293), .ip2(n36), .op(n1832) );
  nand2_2 U107 ( .ip1(N280), .ip2(n100), .op(n1833) );
  nand2_2 U110 ( .ip1(N292), .ip2(n36), .op(n1830) );
  nand2_2 U111 ( .ip1(N279), .ip2(n100), .op(n1831) );
  nand2_2 U114 ( .ip1(N291), .ip2(n36), .op(n1828) );
  nand2_2 U115 ( .ip1(N278), .ip2(n100), .op(n1829) );
  nand2_2 U119 ( .ip1(N302), .ip2(n36), .op(n1826) );
  nor4_2 U120 ( .ip1(n1070), .ip2(n1098), .ip3(r5), .ip4(n1825), .op(n1884) );
  nor2_2 U121 ( .ip1(set_r), .ip2(n1104), .op(n1825) );
  nand2_2 U122 ( .ip1(N289), .ip2(n99), .op(n1827) );
  nor2_2 U123 ( .ip1(n984), .ip2(n1070), .op(n1883) );
  nand2_2 U127 ( .ip1(n1823), .ip2(n1822), .op(n1285) );
  nand3_2 U128 ( .ip1(n1821), .ip2(n1820), .ip3(idin[0]), .op(n1822) );
  nand2_2 U129 ( .ip1(csr[30]), .ip2(n1100), .op(n1823) );
  nand2_2 U130 ( .ip1(n1819), .ip2(n1818), .op(n1284) );
  nand3_2 U131 ( .ip1(n1821), .ip2(n1820), .ip3(idin[1]), .op(n1818) );
  nand2_2 U132 ( .ip1(csr[31]), .ip2(n1100), .op(n1819) );
  nand2_2 U133 ( .ip1(rst), .ip2(n1817), .op(n1820) );
  nand2_2 U134 ( .ip1(uc_bsel_set), .ip2(ep_match_r), .op(n1817) );
  nand2_2 U135 ( .ip1(n1816), .ip2(n1815), .op(n1283) );
  nand3_2 U136 ( .ip1(n1821), .ip2(n1814), .ip3(idin[2]), .op(n1815) );
  nand2_2 U137 ( .ip1(csr[28]), .ip2(n1099), .op(n1816) );
  nand2_2 U138 ( .ip1(n1813), .ip2(n1812), .op(n1282) );
  nand3_2 U139 ( .ip1(n1821), .ip2(n1814), .ip3(idin[3]), .op(n1812) );
  nand2_2 U140 ( .ip1(csr[29]), .ip2(n1099), .op(n1813) );
  nand2_2 U141 ( .ip1(rst), .ip2(n1811), .op(n1814) );
  nand2_2 U142 ( .ip1(uc_dpd_set), .ip2(ep_match_r), .op(n1811) );
  nand2_2 U143 ( .ip1(n1810), .ip2(n1809), .op(n1281) );
  nand2_2 U144 ( .ip1(int_[0]), .ip2(n1097), .op(n1809) );
  nand2_2 U145 ( .ip1(n1), .ip2(n1808), .op(n1810) );
  nand2_2 U146 ( .ip1(n1807), .ip2(n1806), .op(n1808) );
  nand2_2 U147 ( .ip1(int_to_set), .ip2(n1), .op(n1806) );
  nand2_2 U148 ( .ip1(n1805), .ip2(n1804), .op(n1280) );
  nand2_2 U149 ( .ip1(int_[1]), .ip2(n1096), .op(n1804) );
  nand2_2 U150 ( .ip1(n1), .ip2(n1803), .op(n1805) );
  nand2_2 U151 ( .ip1(n1807), .ip2(n1802), .op(n1803) );
  nand2_2 U152 ( .ip1(int_crc16_set), .ip2(n1), .op(n1802) );
  nand2_2 U153 ( .ip1(n1801), .ip2(n1800), .op(n1279) );
  nand2_2 U154 ( .ip1(int_[2]), .ip2(n1095), .op(n1800) );
  nand2_2 U155 ( .ip1(n1), .ip2(n1799), .op(n1801) );
  nand2_2 U156 ( .ip1(n1807), .ip2(n1798), .op(n1799) );
  nand2_2 U157 ( .ip1(int_upid_set), .ip2(n1), .op(n1798) );
  nand2_2 U158 ( .ip1(n1797), .ip2(n1796), .op(n1278) );
  or2_2 U159 ( .ip1(n1102), .ip2(n1795), .op(n1796) );
  nand2_2 U160 ( .ip1(n1), .ip2(n1795), .op(n1797) );
  nand2_2 U161 ( .ip1(n1807), .ip2(n1794), .op(n1795) );
  nand2_2 U162 ( .ip1(int_buf0_set), .ip2(n1), .op(n1794) );
  nand2_2 U163 ( .ip1(n1793), .ip2(n1792), .op(n1277) );
  or2_2 U164 ( .ip1(n1101), .ip2(n1791), .op(n1792) );
  nand2_2 U165 ( .ip1(n1), .ip2(n1791), .op(n1793) );
  nand2_2 U166 ( .ip1(n1807), .ip2(n1790), .op(n1791) );
  nand2_2 U167 ( .ip1(int_buf1_set), .ip2(n1), .op(n1790) );
  nand2_2 U168 ( .ip1(n1789), .ip2(n1788), .op(n1276) );
  nand2_2 U169 ( .ip1(int_[5]), .ip2(n1094), .op(n1788) );
  nand2_2 U170 ( .ip1(n1), .ip2(n1787), .op(n1789) );
  nand2_2 U171 ( .ip1(n1807), .ip2(n1786), .op(n1787) );
  nand2_2 U172 ( .ip1(int_seqerr_set), .ip2(n1), .op(n1786) );
  nand2_2 U173 ( .ip1(n1785), .ip2(n1784), .op(n1275) );
  nand2_2 U174 ( .ip1(int_[6]), .ip2(n1093), .op(n1784) );
  nand2_2 U175 ( .ip1(n1), .ip2(n1783), .op(n1785) );
  nand2_2 U176 ( .ip1(n1807), .ip2(n1782), .op(n1783) );
  nand2_2 U177 ( .ip1(out_to_small), .ip2(n1), .op(n1782) );
  nor2_2 U178 ( .ip1(n1889), .ip2(int_re), .op(n1807) );
  nand4_2 U180 ( .ip1(rst), .ip2(n1781), .ip3(n1780), .ip4(n1779), .op(n1274)
         );
  nand2_2 U181 ( .ip1(idin[31]), .ip2(n97), .op(n1779) );
  nand2_2 U182 ( .ip1(din[31]), .ip2(n52), .op(n1780) );
  nand2_2 U183 ( .ip1(buf1[31]), .ip2(n95), .op(n1781) );
  nand4_2 U184 ( .ip1(rst), .ip2(n1776), .ip3(n1775), .ip4(n1774), .op(n1273)
         );
  nand2_2 U185 ( .ip1(idin[30]), .ip2(n96), .op(n1774) );
  nand2_2 U186 ( .ip1(din[30]), .ip2(n52), .op(n1775) );
  nand2_2 U187 ( .ip1(buf1[30]), .ip2(n95), .op(n1776) );
  nand4_2 U188 ( .ip1(rst), .ip2(n1773), .ip3(n1772), .ip4(n1771), .op(n1272)
         );
  nand2_2 U189 ( .ip1(idin[29]), .ip2(n96), .op(n1771) );
  nand2_2 U190 ( .ip1(din[29]), .ip2(n52), .op(n1772) );
  nand2_2 U191 ( .ip1(buf1[29]), .ip2(n95), .op(n1773) );
  nand4_2 U192 ( .ip1(rst), .ip2(n1770), .ip3(n1769), .ip4(n1768), .op(n1271)
         );
  nand2_2 U193 ( .ip1(idin[28]), .ip2(n96), .op(n1768) );
  nand2_2 U194 ( .ip1(din[28]), .ip2(n52), .op(n1769) );
  nand2_2 U195 ( .ip1(buf1[28]), .ip2(n95), .op(n1770) );
  nand4_2 U196 ( .ip1(rst), .ip2(n1767), .ip3(n1766), .ip4(n1765), .op(n1270)
         );
  nand2_2 U197 ( .ip1(idin[27]), .ip2(n96), .op(n1765) );
  nand2_2 U198 ( .ip1(din[27]), .ip2(n52), .op(n1766) );
  nand2_2 U199 ( .ip1(buf1[27]), .ip2(n95), .op(n1767) );
  nand4_2 U200 ( .ip1(rst), .ip2(n1764), .ip3(n1763), .ip4(n1762), .op(n1269)
         );
  nand2_2 U201 ( .ip1(idin[26]), .ip2(n96), .op(n1762) );
  nand2_2 U202 ( .ip1(din[26]), .ip2(n52), .op(n1763) );
  nand2_2 U203 ( .ip1(buf1[26]), .ip2(n95), .op(n1764) );
  nand4_2 U204 ( .ip1(rst), .ip2(n1761), .ip3(n1760), .ip4(n1759), .op(n1268)
         );
  nand2_2 U205 ( .ip1(idin[25]), .ip2(n96), .op(n1759) );
  nand2_2 U206 ( .ip1(din[25]), .ip2(n52), .op(n1760) );
  nand2_2 U207 ( .ip1(buf1[25]), .ip2(n95), .op(n1761) );
  nand4_2 U208 ( .ip1(rst), .ip2(n1758), .ip3(n1757), .ip4(n1756), .op(n1267)
         );
  nand2_2 U209 ( .ip1(idin[24]), .ip2(n96), .op(n1756) );
  nand2_2 U210 ( .ip1(din[24]), .ip2(n52), .op(n1757) );
  nand2_2 U211 ( .ip1(buf1[24]), .ip2(n95), .op(n1758) );
  nand4_2 U212 ( .ip1(rst), .ip2(n1755), .ip3(n1754), .ip4(n1753), .op(n1266)
         );
  nand2_2 U213 ( .ip1(idin[23]), .ip2(n96), .op(n1753) );
  nand2_2 U214 ( .ip1(din[23]), .ip2(n52), .op(n1754) );
  nand2_2 U215 ( .ip1(buf1[23]), .ip2(n94), .op(n1755) );
  nand4_2 U216 ( .ip1(rst), .ip2(n1752), .ip3(n1751), .ip4(n1750), .op(n1265)
         );
  nand2_2 U217 ( .ip1(idin[22]), .ip2(n96), .op(n1750) );
  nand2_2 U218 ( .ip1(din[22]), .ip2(n52), .op(n1751) );
  nand2_2 U219 ( .ip1(buf1[22]), .ip2(n94), .op(n1752) );
  nand4_2 U220 ( .ip1(rst), .ip2(n1749), .ip3(n1748), .ip4(n1747), .op(n1264)
         );
  nand2_2 U221 ( .ip1(idin[21]), .ip2(n96), .op(n1747) );
  nand2_2 U222 ( .ip1(din[21]), .ip2(n52), .op(n1748) );
  nand2_2 U223 ( .ip1(buf1[21]), .ip2(n94), .op(n1749) );
  nand4_2 U224 ( .ip1(rst), .ip2(n1746), .ip3(n1745), .ip4(n1744), .op(n1263)
         );
  nand2_2 U225 ( .ip1(idin[20]), .ip2(n96), .op(n1744) );
  nand2_2 U226 ( .ip1(din[20]), .ip2(n52), .op(n1745) );
  nand2_2 U227 ( .ip1(buf1[20]), .ip2(n94), .op(n1746) );
  nand4_2 U228 ( .ip1(rst), .ip2(n1743), .ip3(n1742), .ip4(n1741), .op(n1262)
         );
  nand2_2 U229 ( .ip1(idin[19]), .ip2(n96), .op(n1741) );
  nand2_2 U230 ( .ip1(din[19]), .ip2(n53), .op(n1742) );
  nand2_2 U231 ( .ip1(buf1[19]), .ip2(n94), .op(n1743) );
  nand4_2 U232 ( .ip1(rst), .ip2(n1740), .ip3(n1739), .ip4(n1738), .op(n1261)
         );
  nand2_2 U233 ( .ip1(idin[18]), .ip2(n97), .op(n1738) );
  nand2_2 U234 ( .ip1(din[18]), .ip2(n53), .op(n1739) );
  nand2_2 U235 ( .ip1(buf1[18]), .ip2(n94), .op(n1740) );
  nand4_2 U236 ( .ip1(rst), .ip2(n1737), .ip3(n1736), .ip4(n1735), .op(n1260)
         );
  nand2_2 U237 ( .ip1(idin[17]), .ip2(n97), .op(n1735) );
  nand2_2 U238 ( .ip1(din[17]), .ip2(n53), .op(n1736) );
  nand2_2 U239 ( .ip1(buf1[17]), .ip2(n94), .op(n1737) );
  nand4_2 U240 ( .ip1(rst), .ip2(n1734), .ip3(n1733), .ip4(n1732), .op(n1259)
         );
  nand2_2 U241 ( .ip1(idin[16]), .ip2(n97), .op(n1732) );
  nand2_2 U242 ( .ip1(din[16]), .ip2(n53), .op(n1733) );
  nand2_2 U243 ( .ip1(buf1[16]), .ip2(n94), .op(n1734) );
  nand4_2 U244 ( .ip1(rst), .ip2(n1731), .ip3(n1730), .ip4(n1729), .op(n1258)
         );
  nand2_2 U245 ( .ip1(idin[15]), .ip2(n97), .op(n1729) );
  nand2_2 U246 ( .ip1(din[15]), .ip2(n53), .op(n1730) );
  nand2_2 U247 ( .ip1(buf1[15]), .ip2(n94), .op(n1731) );
  nand4_2 U248 ( .ip1(rst), .ip2(n1728), .ip3(n1727), .ip4(n1726), .op(n1257)
         );
  nand2_2 U249 ( .ip1(idin[14]), .ip2(n97), .op(n1726) );
  nand2_2 U250 ( .ip1(din[14]), .ip2(n53), .op(n1727) );
  nand2_2 U251 ( .ip1(buf1[14]), .ip2(n94), .op(n1728) );
  nand4_2 U252 ( .ip1(rst), .ip2(n1725), .ip3(n1724), .ip4(n1723), .op(n1256)
         );
  nand2_2 U253 ( .ip1(idin[13]), .ip2(n97), .op(n1723) );
  nand2_2 U254 ( .ip1(din[13]), .ip2(n53), .op(n1724) );
  nand2_2 U255 ( .ip1(buf1[13]), .ip2(n94), .op(n1725) );
  nand4_2 U256 ( .ip1(rst), .ip2(n1722), .ip3(n1721), .ip4(n1720), .op(n1255)
         );
  nand2_2 U257 ( .ip1(idin[12]), .ip2(n97), .op(n1720) );
  nand2_2 U258 ( .ip1(din[12]), .ip2(n53), .op(n1721) );
  nand2_2 U259 ( .ip1(buf1[12]), .ip2(n94), .op(n1722) );
  nand4_2 U260 ( .ip1(rst), .ip2(n1719), .ip3(n1718), .ip4(n1717), .op(n1254)
         );
  nand2_2 U261 ( .ip1(idin[11]), .ip2(n97), .op(n1717) );
  nand2_2 U262 ( .ip1(din[11]), .ip2(n53), .op(n1718) );
  nand2_2 U263 ( .ip1(buf1[11]), .ip2(n93), .op(n1719) );
  nand4_2 U264 ( .ip1(rst), .ip2(n1716), .ip3(n1715), .ip4(n1714), .op(n1253)
         );
  nand2_2 U265 ( .ip1(idin[10]), .ip2(n97), .op(n1714) );
  nand2_2 U266 ( .ip1(din[10]), .ip2(n53), .op(n1715) );
  nand2_2 U267 ( .ip1(buf1[10]), .ip2(n93), .op(n1716) );
  nand4_2 U268 ( .ip1(rst), .ip2(n1713), .ip3(n1712), .ip4(n1711), .op(n1252)
         );
  nand2_2 U269 ( .ip1(idin[9]), .ip2(n97), .op(n1711) );
  nand2_2 U270 ( .ip1(din[9]), .ip2(n53), .op(n1712) );
  nand2_2 U271 ( .ip1(buf1[9]), .ip2(n93), .op(n1713) );
  nand4_2 U272 ( .ip1(rst), .ip2(n1710), .ip3(n1709), .ip4(n1708), .op(n1251)
         );
  nand2_2 U273 ( .ip1(idin[8]), .ip2(n97), .op(n1708) );
  nand2_2 U274 ( .ip1(din[8]), .ip2(n53), .op(n1709) );
  nand2_2 U275 ( .ip1(buf1[8]), .ip2(n93), .op(n1710) );
  nand4_2 U276 ( .ip1(rst), .ip2(n1707), .ip3(n1706), .ip4(n1705), .op(n1250)
         );
  nand2_2 U277 ( .ip1(idin[7]), .ip2(n98), .op(n1705) );
  nand2_2 U278 ( .ip1(din[7]), .ip2(n54), .op(n1706) );
  nand2_2 U279 ( .ip1(buf1[7]), .ip2(n93), .op(n1707) );
  nand4_2 U280 ( .ip1(rst), .ip2(n1704), .ip3(n1703), .ip4(n1702), .op(n1249)
         );
  nand2_2 U281 ( .ip1(idin[6]), .ip2(n98), .op(n1702) );
  nand2_2 U282 ( .ip1(din[6]), .ip2(n54), .op(n1703) );
  nand2_2 U283 ( .ip1(buf1[6]), .ip2(n93), .op(n1704) );
  nand4_2 U284 ( .ip1(rst), .ip2(n1701), .ip3(n1700), .ip4(n1699), .op(n1248)
         );
  nand2_2 U285 ( .ip1(idin[5]), .ip2(n98), .op(n1699) );
  nand2_2 U286 ( .ip1(din[5]), .ip2(n54), .op(n1700) );
  nand2_2 U287 ( .ip1(buf1[5]), .ip2(n93), .op(n1701) );
  nand4_2 U288 ( .ip1(rst), .ip2(n1698), .ip3(n1697), .ip4(n1696), .op(n1247)
         );
  nand2_2 U289 ( .ip1(idin[4]), .ip2(n98), .op(n1696) );
  nand2_2 U290 ( .ip1(din[4]), .ip2(n54), .op(n1697) );
  nand2_2 U291 ( .ip1(buf1[4]), .ip2(n93), .op(n1698) );
  nand4_2 U292 ( .ip1(rst), .ip2(n1695), .ip3(n1694), .ip4(n1693), .op(n1246)
         );
  nand2_2 U293 ( .ip1(n98), .ip2(idin[3]), .op(n1693) );
  nand2_2 U294 ( .ip1(din[3]), .ip2(n54), .op(n1694) );
  nand2_2 U295 ( .ip1(buf1[3]), .ip2(n93), .op(n1695) );
  nand4_2 U296 ( .ip1(rst), .ip2(n1692), .ip3(n1691), .ip4(n1690), .op(n1245)
         );
  nand2_2 U297 ( .ip1(n98), .ip2(idin[2]), .op(n1690) );
  nand2_2 U298 ( .ip1(din[2]), .ip2(n54), .op(n1691) );
  nand2_2 U299 ( .ip1(buf1[2]), .ip2(n93), .op(n1692) );
  nand4_2 U300 ( .ip1(rst), .ip2(n1689), .ip3(n1688), .ip4(n1687), .op(n1244)
         );
  nand2_2 U301 ( .ip1(n98), .ip2(idin[1]), .op(n1687) );
  nand2_2 U302 ( .ip1(din[1]), .ip2(n54), .op(n1688) );
  nand2_2 U303 ( .ip1(buf1[1]), .ip2(n93), .op(n1689) );
  nand4_2 U304 ( .ip1(rst), .ip2(n1686), .ip3(n1685), .ip4(n1684), .op(n1243)
         );
  nand2_2 U305 ( .ip1(n98), .ip2(idin[0]), .op(n1684) );
  nand2_2 U306 ( .ip1(din[0]), .ip2(n54), .op(n1685) );
  nand2_2 U307 ( .ip1(buf1[0]), .ip2(n93), .op(n1686) );
  nor2_2 U308 ( .ip1(n54), .ip2(n98), .op(n1777) );
  and3_2 U309 ( .ip1(n1683), .ip2(n1682), .ip3(ep_match_r), .op(n1778) );
  or2_2 U310 ( .ip1(buf1_set), .ip2(out_to_small), .op(n1683) );
  nand2_2 U311 ( .ip1(we), .ip2(n90), .op(n1682) );
  nand3_2 U312 ( .ip1(rst), .ip2(n1680), .ip3(n1679), .op(n1242) );
  nor2_2 U313 ( .ip1(n89), .ip2(n1071), .op(n1677) );
  nor2_2 U314 ( .ip1(n1123), .ip2(n86), .op(n1678) );
  nand2_2 U315 ( .ip1(buf0[31]), .ip2(n83), .op(n1680) );
  nand3_2 U316 ( .ip1(rst), .ip2(n1673), .ip3(n1672), .op(n1241) );
  nor2_2 U317 ( .ip1(n89), .ip2(n985), .op(n1670) );
  nor2_2 U318 ( .ip1(n1105), .ip2(n86), .op(n1671) );
  nand2_2 U319 ( .ip1(buf0[30]), .ip2(n83), .op(n1673) );
  nand3_2 U320 ( .ip1(rst), .ip2(n1669), .ip3(n1668), .op(n1240) );
  nor2_2 U321 ( .ip1(n89), .ip2(n1066), .op(n1666) );
  nor2_2 U322 ( .ip1(n1106), .ip2(n86), .op(n1667) );
  nand2_2 U323 ( .ip1(buf0[29]), .ip2(n83), .op(n1669) );
  nand3_2 U324 ( .ip1(rst), .ip2(n1665), .ip3(n1664), .op(n1239) );
  nor2_2 U325 ( .ip1(n89), .ip2(n986), .op(n1662) );
  nor2_2 U326 ( .ip1(n1107), .ip2(n86), .op(n1663) );
  nand2_2 U327 ( .ip1(buf0[28]), .ip2(n83), .op(n1665) );
  nand3_2 U328 ( .ip1(rst), .ip2(n1661), .ip3(n1660), .op(n1238) );
  nor2_2 U329 ( .ip1(n89), .ip2(n992), .op(n1658) );
  nor2_2 U330 ( .ip1(n1126), .ip2(n86), .op(n1659) );
  nand2_2 U331 ( .ip1(buf0[27]), .ip2(n83), .op(n1661) );
  nand3_2 U332 ( .ip1(rst), .ip2(n1657), .ip3(n1656), .op(n1237) );
  nor2_2 U333 ( .ip1(n88), .ip2(n987), .op(n1654) );
  nor2_2 U334 ( .ip1(n1127), .ip2(n86), .op(n1655) );
  nand2_2 U335 ( .ip1(buf0[26]), .ip2(n83), .op(n1657) );
  nand3_2 U336 ( .ip1(rst), .ip2(n1653), .ip3(n1652), .op(n1236) );
  nor2_2 U337 ( .ip1(n88), .ip2(n991), .op(n1650) );
  nor2_2 U338 ( .ip1(n1128), .ip2(n86), .op(n1651) );
  nand2_2 U339 ( .ip1(buf0[25]), .ip2(n83), .op(n1653) );
  nand3_2 U340 ( .ip1(rst), .ip2(n1649), .ip3(n1648), .op(n1235) );
  nor2_2 U341 ( .ip1(n88), .ip2(n988), .op(n1646) );
  nor2_2 U342 ( .ip1(n1129), .ip2(n85), .op(n1647) );
  nand2_2 U343 ( .ip1(buf0[24]), .ip2(n83), .op(n1649) );
  nand3_2 U344 ( .ip1(rst), .ip2(n1645), .ip3(n1644), .op(n1234) );
  nor2_2 U345 ( .ip1(n88), .ip2(n990), .op(n1642) );
  nor2_2 U346 ( .ip1(n1130), .ip2(n85), .op(n1643) );
  nand2_2 U347 ( .ip1(buf0[23]), .ip2(n82), .op(n1645) );
  nand3_2 U348 ( .ip1(rst), .ip2(n1641), .ip3(n1640), .op(n1233) );
  nor2_2 U349 ( .ip1(n88), .ip2(n1072), .op(n1638) );
  nor2_2 U350 ( .ip1(n1131), .ip2(n85), .op(n1639) );
  nand2_2 U351 ( .ip1(buf0[22]), .ip2(n82), .op(n1641) );
  nand3_2 U352 ( .ip1(rst), .ip2(n1637), .ip3(n1636), .op(n1232) );
  nor2_2 U353 ( .ip1(n88), .ip2(n1065), .op(n1634) );
  nor2_2 U354 ( .ip1(n1132), .ip2(n85), .op(n1635) );
  nand2_2 U355 ( .ip1(buf0[21]), .ip2(n82), .op(n1637) );
  nand3_2 U356 ( .ip1(rst), .ip2(n1633), .ip3(n1632), .op(n1231) );
  nor2_2 U357 ( .ip1(n88), .ip2(n1073), .op(n1630) );
  nor2_2 U358 ( .ip1(n1133), .ip2(n85), .op(n1631) );
  nand2_2 U359 ( .ip1(buf0[20]), .ip2(n82), .op(n1633) );
  nand3_2 U360 ( .ip1(rst), .ip2(n1629), .ip3(n1628), .op(n1230) );
  nor2_2 U361 ( .ip1(n88), .ip2(n989), .op(n1626) );
  nor2_2 U362 ( .ip1(n1134), .ip2(n85), .op(n1627) );
  nand2_2 U363 ( .ip1(buf0[19]), .ip2(n82), .op(n1629) );
  nand3_2 U364 ( .ip1(rst), .ip2(n1625), .ip3(n1624), .op(n1229) );
  nor2_2 U365 ( .ip1(n88), .ip2(n1074), .op(n1622) );
  nor2_2 U366 ( .ip1(n1135), .ip2(n85), .op(n1623) );
  nand2_2 U367 ( .ip1(buf0[18]), .ip2(n82), .op(n1625) );
  nand3_2 U368 ( .ip1(rst), .ip2(n1621), .ip3(n1620), .op(n1228) );
  nor2_2 U369 ( .ip1(n88), .ip2(n1075), .op(n1618) );
  nor2_2 U370 ( .ip1(n1136), .ip2(n85), .op(n1619) );
  nand2_2 U371 ( .ip1(buf0[17]), .ip2(n82), .op(n1621) );
  nand3_2 U372 ( .ip1(rst), .ip2(n1617), .ip3(n1616), .op(n1227) );
  nor2_2 U373 ( .ip1(n88), .ip2(n1076), .op(n1614) );
  nor2_2 U374 ( .ip1(n1110), .ip2(n85), .op(n1615) );
  nand2_2 U375 ( .ip1(buf0[16]), .ip2(n82), .op(n1617) );
  nand3_2 U376 ( .ip1(rst), .ip2(n1613), .ip3(n1612), .op(n1226) );
  nor2_2 U377 ( .ip1(n88), .ip2(n1077), .op(n1610) );
  nor2_2 U378 ( .ip1(n1111), .ip2(n85), .op(n1611) );
  nand2_2 U379 ( .ip1(buf0[15]), .ip2(n82), .op(n1613) );
  nand3_2 U380 ( .ip1(rst), .ip2(n1609), .ip3(n1608), .op(n1225) );
  nor2_2 U381 ( .ip1(n88), .ip2(n1078), .op(n1606) );
  nor2_2 U382 ( .ip1(n1112), .ip2(n85), .op(n1607) );
  nand2_2 U383 ( .ip1(buf0[14]), .ip2(n82), .op(n1609) );
  nand3_2 U384 ( .ip1(rst), .ip2(n1605), .ip3(n1604), .op(n1224) );
  nor2_2 U385 ( .ip1(n88), .ip2(n1079), .op(n1602) );
  nor2_2 U386 ( .ip1(n1113), .ip2(n85), .op(n1603) );
  nand2_2 U387 ( .ip1(buf0[13]), .ip2(n82), .op(n1605) );
  nand3_2 U388 ( .ip1(rst), .ip2(n1601), .ip3(n1600), .op(n1223) );
  nor2_2 U389 ( .ip1(n87), .ip2(n1080), .op(n1598) );
  nor2_2 U390 ( .ip1(n1114), .ip2(n85), .op(n1599) );
  nand2_2 U391 ( .ip1(buf0[12]), .ip2(n82), .op(n1601) );
  nand3_2 U392 ( .ip1(rst), .ip2(n1597), .ip3(n1596), .op(n1222) );
  nor2_2 U393 ( .ip1(n87), .ip2(n1081), .op(n1594) );
  nor2_2 U394 ( .ip1(n1115), .ip2(n84), .op(n1595) );
  nand2_2 U395 ( .ip1(buf0[11]), .ip2(n81), .op(n1597) );
  nand3_2 U396 ( .ip1(rst), .ip2(n1593), .ip3(n1592), .op(n1221) );
  nor2_2 U397 ( .ip1(n87), .ip2(n1082), .op(n1590) );
  nor2_2 U398 ( .ip1(n1116), .ip2(n84), .op(n1591) );
  nand2_2 U399 ( .ip1(buf0[10]), .ip2(n81), .op(n1593) );
  nand3_2 U400 ( .ip1(rst), .ip2(n1589), .ip3(n1588), .op(n1220) );
  nor2_2 U401 ( .ip1(n87), .ip2(n1083), .op(n1586) );
  nor2_2 U402 ( .ip1(n1117), .ip2(n84), .op(n1587) );
  nand2_2 U403 ( .ip1(buf0[9]), .ip2(n81), .op(n1589) );
  nand3_2 U404 ( .ip1(rst), .ip2(n1585), .ip3(n1584), .op(n1219) );
  nor2_2 U405 ( .ip1(n87), .ip2(n1084), .op(n1582) );
  nor2_2 U406 ( .ip1(n1118), .ip2(n84), .op(n1583) );
  nand2_2 U407 ( .ip1(buf0[8]), .ip2(n81), .op(n1585) );
  nand3_2 U408 ( .ip1(rst), .ip2(n1581), .ip3(n1580), .op(n1218) );
  nor2_2 U409 ( .ip1(n87), .ip2(n1085), .op(n1578) );
  nor2_2 U410 ( .ip1(n1119), .ip2(n84), .op(n1579) );
  nand2_2 U411 ( .ip1(buf0[7]), .ip2(n81), .op(n1581) );
  nand3_2 U412 ( .ip1(rst), .ip2(n1577), .ip3(n1576), .op(n1217) );
  nor2_2 U413 ( .ip1(n87), .ip2(n1086), .op(n1574) );
  nor2_2 U414 ( .ip1(n1120), .ip2(n84), .op(n1575) );
  nand2_2 U415 ( .ip1(buf0[6]), .ip2(n81), .op(n1577) );
  nand3_2 U416 ( .ip1(rst), .ip2(n1573), .ip3(n1572), .op(n1216) );
  nor2_2 U417 ( .ip1(n87), .ip2(n1087), .op(n1570) );
  nor2_2 U418 ( .ip1(n1121), .ip2(n84), .op(n1571) );
  nand2_2 U419 ( .ip1(buf0[5]), .ip2(n81), .op(n1573) );
  nand3_2 U420 ( .ip1(rst), .ip2(n1569), .ip3(n1568), .op(n1215) );
  nor2_2 U421 ( .ip1(n87), .ip2(n1088), .op(n1566) );
  nor2_2 U422 ( .ip1(n1122), .ip2(n84), .op(n1567) );
  nand2_2 U423 ( .ip1(buf0[4]), .ip2(n81), .op(n1569) );
  nand3_2 U424 ( .ip1(rst), .ip2(n1565), .ip3(n1564), .op(n1214) );
  nor2_2 U425 ( .ip1(n87), .ip2(n1089), .op(n1562) );
  nor2_2 U426 ( .ip1(n1108), .ip2(n84), .op(n1563) );
  nand2_2 U427 ( .ip1(buf0[3]), .ip2(n81), .op(n1565) );
  nand3_2 U428 ( .ip1(rst), .ip2(n1561), .ip3(n1560), .op(n1213) );
  nor2_2 U429 ( .ip1(n87), .ip2(n1090), .op(n1558) );
  nor2_2 U430 ( .ip1(n1109), .ip2(n84), .op(n1559) );
  nand2_2 U431 ( .ip1(buf0[2]), .ip2(n81), .op(n1561) );
  nand3_2 U432 ( .ip1(rst), .ip2(n1557), .ip3(n1556), .op(n1212) );
  nor2_2 U433 ( .ip1(n87), .ip2(n1091), .op(n1554) );
  nor2_2 U434 ( .ip1(n1124), .ip2(n84), .op(n1555) );
  nand2_2 U435 ( .ip1(buf0[1]), .ip2(n81), .op(n1557) );
  nand3_2 U436 ( .ip1(rst), .ip2(n1553), .ip3(n1552), .op(n1211) );
  nor2_2 U437 ( .ip1(n87), .ip2(n1092), .op(n1550) );
  nor2_2 U438 ( .ip1(n1125), .ip2(n84), .op(n1551) );
  nand2_2 U439 ( .ip1(buf0[0]), .ip2(n81), .op(n1553) );
  and3_2 U440 ( .ip1(n87), .ip2(n66), .ip3(n84), .op(n1674) );
  nand4_2 U441 ( .ip1(buf0_set), .ip2(n1821), .ip3(n66), .ip4(n1103), .op(
        n1675) );
  nand3_2 U442 ( .ip1(n1821), .ip2(n67), .ip3(buf0_rl), .op(n1676) );
  nor2_2 U443 ( .ip1(n1098), .ip2(n1889), .op(n1821) );
  and2_2 U444 ( .ip1(din[0]), .ip2(n80), .op(n1548) );
  and2_2 U445 ( .ip1(din[1]), .ip2(n80), .op(n1547) );
  and2_2 U446 ( .ip1(din[2]), .ip2(n80), .op(n1546) );
  and2_2 U447 ( .ip1(din[3]), .ip2(n80), .op(n1545) );
  and2_2 U448 ( .ip1(din[4]), .ip2(n80), .op(n1544) );
  and2_2 U449 ( .ip1(din[5]), .ip2(n80), .op(n1543) );
  and2_2 U450 ( .ip1(din[6]), .ip2(n79), .op(n1542) );
  and2_2 U451 ( .ip1(din[7]), .ip2(n79), .op(n1541) );
  and2_2 U452 ( .ip1(din[8]), .ip2(n79), .op(n1540) );
  and2_2 U453 ( .ip1(din[9]), .ip2(n79), .op(n1539) );
  and2_2 U454 ( .ip1(din[10]), .ip2(n79), .op(n1538) );
  and2_2 U455 ( .ip1(din[11]), .ip2(n79), .op(n1537) );
  and2_2 U456 ( .ip1(din[12]), .ip2(n79), .op(n1536) );
  and2_2 U457 ( .ip1(din[13]), .ip2(n79), .op(n1535) );
  and2_2 U458 ( .ip1(din[14]), .ip2(n79), .op(n1534) );
  and2_2 U459 ( .ip1(din[15]), .ip2(n79), .op(n1533) );
  and2_2 U460 ( .ip1(din[16]), .ip2(n79), .op(n1532) );
  and2_2 U461 ( .ip1(din[17]), .ip2(n79), .op(n1531) );
  and2_2 U462 ( .ip1(din[18]), .ip2(n79), .op(n1530) );
  and2_2 U463 ( .ip1(din[19]), .ip2(n78), .op(n1529) );
  and2_2 U464 ( .ip1(din[20]), .ip2(n78), .op(n1528) );
  and2_2 U465 ( .ip1(din[21]), .ip2(n78), .op(n1527) );
  and2_2 U466 ( .ip1(din[22]), .ip2(n78), .op(n1526) );
  and2_2 U467 ( .ip1(din[23]), .ip2(n78), .op(n1525) );
  and2_2 U468 ( .ip1(din[24]), .ip2(n78), .op(n1524) );
  and2_2 U469 ( .ip1(din[25]), .ip2(n78), .op(n1523) );
  and2_2 U470 ( .ip1(din[26]), .ip2(n78), .op(n1522) );
  and2_2 U471 ( .ip1(din[27]), .ip2(n78), .op(n1521) );
  and2_2 U472 ( .ip1(din[28]), .ip2(n78), .op(n1520) );
  and2_2 U473 ( .ip1(din[29]), .ip2(n78), .op(n1519) );
  and2_2 U474 ( .ip1(din[30]), .ip2(n78), .op(n1518) );
  and2_2 U475 ( .ip1(din[31]), .ip2(n78), .op(n1517) );
  nand2_2 U477 ( .ip1(n1515), .ip2(n1514), .op(n1178) );
  nand2_2 U478 ( .ip1(n1513), .ip2(din[16]), .op(n1514) );
  nand2_2 U479 ( .ip1(int__16), .ip2(n1512), .op(n1515) );
  nand2_2 U480 ( .ip1(n1511), .ip2(n1510), .op(n1177) );
  nand2_2 U481 ( .ip1(n1513), .ip2(din[17]), .op(n1510) );
  nand2_2 U482 ( .ip1(int__17), .ip2(n1512), .op(n1511) );
  nand2_2 U483 ( .ip1(n1509), .ip2(n1508), .op(n1176) );
  nand2_2 U484 ( .ip1(n1513), .ip2(din[18]), .op(n1508) );
  nand2_2 U485 ( .ip1(int__18), .ip2(n1512), .op(n1509) );
  nand2_2 U486 ( .ip1(n1507), .ip2(n1506), .op(n1175) );
  nand2_2 U487 ( .ip1(n1513), .ip2(din[19]), .op(n1506) );
  nand2_2 U488 ( .ip1(int__19), .ip2(n1512), .op(n1507) );
  nand2_2 U489 ( .ip1(n1505), .ip2(n1504), .op(n1174) );
  nand2_2 U490 ( .ip1(n1513), .ip2(din[20]), .op(n1504) );
  nand2_2 U491 ( .ip1(int__20), .ip2(n1512), .op(n1505) );
  nand2_2 U492 ( .ip1(n1503), .ip2(n1502), .op(n1173) );
  nand2_2 U493 ( .ip1(n1513), .ip2(din[21]), .op(n1502) );
  nand2_2 U494 ( .ip1(int__21), .ip2(n1512), .op(n1503) );
  nand2_2 U495 ( .ip1(n1501), .ip2(n1500), .op(n1172) );
  nand2_2 U496 ( .ip1(n1513), .ip2(din[24]), .op(n1500) );
  nand2_2 U497 ( .ip1(int__24), .ip2(n1512), .op(n1501) );
  nand2_2 U498 ( .ip1(n1499), .ip2(n1498), .op(n1171) );
  nand2_2 U499 ( .ip1(n1513), .ip2(din[25]), .op(n1498) );
  nand2_2 U500 ( .ip1(int__25), .ip2(n1512), .op(n1499) );
  nand2_2 U501 ( .ip1(n1497), .ip2(n1496), .op(n1170) );
  nand2_2 U502 ( .ip1(n1513), .ip2(din[26]), .op(n1496) );
  nand2_2 U503 ( .ip1(int__26), .ip2(n1512), .op(n1497) );
  nand2_2 U504 ( .ip1(n1495), .ip2(n1494), .op(n1169) );
  nand2_2 U505 ( .ip1(n1513), .ip2(din[27]), .op(n1494) );
  nand2_2 U506 ( .ip1(int__27), .ip2(n1512), .op(n1495) );
  nand2_2 U507 ( .ip1(n1493), .ip2(n1492), .op(n1168) );
  nand2_2 U508 ( .ip1(n1513), .ip2(din[28]), .op(n1492) );
  nand2_2 U509 ( .ip1(int__28), .ip2(n1512), .op(n1493) );
  nand2_2 U510 ( .ip1(n1491), .ip2(n1490), .op(n1167) );
  nand2_2 U511 ( .ip1(n1513), .ip2(din[29]), .op(n1490) );
  nand2_2 U512 ( .ip1(int__29), .ip2(n1512), .op(n1491) );
  nand2_2 U516 ( .ip1(n61), .ip2(din[15]), .op(n1489) );
  nand2_2 U518 ( .ip1(n1486), .ip2(n1485), .op(n1165) );
  nand2_2 U519 ( .ip1(n60), .ip2(din[16]), .op(n1485) );
  nand2_2 U520 ( .ip1(csr[16]), .ip2(n58), .op(n1486) );
  nand2_2 U521 ( .ip1(n1484), .ip2(n1483), .op(n1164) );
  nand2_2 U522 ( .ip1(n60), .ip2(din[17]), .op(n1483) );
  nand2_2 U523 ( .ip1(csr[17]), .ip2(n58), .op(n1484) );
  nand2_2 U524 ( .ip1(n1482), .ip2(n1481), .op(n1163) );
  nand2_2 U525 ( .ip1(n60), .ip2(din[18]), .op(n1481) );
  nand2_2 U526 ( .ip1(csr[18]), .ip2(n58), .op(n1482) );
  nand2_2 U527 ( .ip1(n1480), .ip2(n1479), .op(n1162) );
  nand2_2 U528 ( .ip1(n60), .ip2(din[19]), .op(n1479) );
  nand2_2 U529 ( .ip1(csr[19]), .ip2(n58), .op(n1480) );
  nand2_2 U530 ( .ip1(n1478), .ip2(n1477), .op(n1161) );
  nand2_2 U531 ( .ip1(n60), .ip2(din[20]), .op(n1477) );
  nand2_2 U532 ( .ip1(csr[20]), .ip2(n58), .op(n1478) );
  nand2_2 U533 ( .ip1(n1476), .ip2(n1475), .op(n1160) );
  nand2_2 U534 ( .ip1(n60), .ip2(din[21]), .op(n1475) );
  nand2_2 U535 ( .ip1(csr[21]), .ip2(n58), .op(n1476) );
  and2_2 U536 ( .ip1(din[22]), .ip2(n60), .op(n1473) );
  nand2_2 U537 ( .ip1(n1471), .ip2(n1470), .op(n1158) );
  nand2_2 U538 ( .ip1(n60), .ip2(din[23]), .op(n1470) );
  nand2_2 U539 ( .ip1(csr[23]), .ip2(n1474), .op(n1471) );
  and2_2 U540 ( .ip1(n59), .ip2(n1472), .op(n1474) );
  nand4_2 U541 ( .ip1(csr[13]), .ip2(out_to_small), .ip3(rst), .ip4(n1469), 
        .op(n1472) );
  nand2_2 U542 ( .ip1(n1468), .ip2(n1467), .op(n1157) );
  nand2_2 U543 ( .ip1(n60), .ip2(din[24]), .op(n1467) );
  nand2_2 U544 ( .ip1(csr[24]), .ip2(n58), .op(n1468) );
  nand2_2 U545 ( .ip1(n1466), .ip2(n1465), .op(n1156) );
  nand2_2 U546 ( .ip1(n60), .ip2(din[25]), .op(n1465) );
  nand2_2 U547 ( .ip1(csr[25]), .ip2(n58), .op(n1466) );
  nand2_2 U548 ( .ip1(n1464), .ip2(n1463), .op(n1155) );
  nand2_2 U549 ( .ip1(n60), .ip2(din[26]), .op(n1463) );
  nand2_2 U550 ( .ip1(csr[26]), .ip2(n58), .op(n1464) );
  nand2_2 U551 ( .ip1(n1462), .ip2(n1461), .op(n1154) );
  nand2_2 U552 ( .ip1(n61), .ip2(din[27]), .op(n1461) );
  nand2_2 U553 ( .ip1(n59), .ip2(csr[27]), .op(n1462) );
  nand2_2 U554 ( .ip1(n1460), .ip2(n1459), .op(n1153) );
  nand2_2 U555 ( .ip1(n61), .ip2(din[0]), .op(n1459) );
  nand2_2 U556 ( .ip1(csr[0]), .ip2(n58), .op(n1460) );
  nand2_2 U557 ( .ip1(n1458), .ip2(n1457), .op(n1152) );
  nand2_2 U558 ( .ip1(n61), .ip2(din[1]), .op(n1457) );
  nand2_2 U559 ( .ip1(csr[1]), .ip2(n58), .op(n1458) );
  nand2_2 U560 ( .ip1(n1456), .ip2(n1455), .op(n1151) );
  nand2_2 U561 ( .ip1(n61), .ip2(din[2]), .op(n1455) );
  nand2_2 U562 ( .ip1(csr[2]), .ip2(n58), .op(n1456) );
  nand2_2 U563 ( .ip1(n1454), .ip2(n1453), .op(n1150) );
  nand2_2 U564 ( .ip1(n61), .ip2(din[3]), .op(n1453) );
  nand2_2 U565 ( .ip1(csr[3]), .ip2(n59), .op(n1454) );
  nand2_2 U566 ( .ip1(n1452), .ip2(n1451), .op(n1149) );
  nand2_2 U567 ( .ip1(n61), .ip2(din[4]), .op(n1451) );
  nand2_2 U568 ( .ip1(csr[4]), .ip2(n59), .op(n1452) );
  nand2_2 U569 ( .ip1(n1450), .ip2(n1449), .op(n1148) );
  nand2_2 U570 ( .ip1(n61), .ip2(din[5]), .op(n1449) );
  nand2_2 U571 ( .ip1(csr[5]), .ip2(n59), .op(n1450) );
  nand2_2 U572 ( .ip1(n1448), .ip2(n1447), .op(n1147) );
  nand2_2 U573 ( .ip1(n61), .ip2(din[6]), .op(n1447) );
  nand2_2 U574 ( .ip1(csr[6]), .ip2(n59), .op(n1448) );
  nand2_2 U575 ( .ip1(n1446), .ip2(n1445), .op(n1146) );
  nand2_2 U576 ( .ip1(n61), .ip2(din[7]), .op(n1445) );
  nand2_2 U577 ( .ip1(csr[7]), .ip2(n59), .op(n1446) );
  nand2_2 U578 ( .ip1(n1444), .ip2(n1443), .op(n1145) );
  nand2_2 U579 ( .ip1(n61), .ip2(din[8]), .op(n1443) );
  nand2_2 U580 ( .ip1(csr[8]), .ip2(n59), .op(n1444) );
  nand2_2 U581 ( .ip1(n1442), .ip2(n1441), .op(n1144) );
  nand2_2 U582 ( .ip1(n61), .ip2(din[9]), .op(n1441) );
  nand2_2 U583 ( .ip1(csr[9]), .ip2(n59), .op(n1442) );
  nand2_2 U584 ( .ip1(n1440), .ip2(n1439), .op(n1143) );
  nand2_2 U585 ( .ip1(n61), .ip2(din[10]), .op(n1439) );
  nand2_2 U586 ( .ip1(csr[10]), .ip2(n59), .op(n1440) );
  nand2_2 U587 ( .ip1(n1438), .ip2(n1437), .op(n1142) );
  nand2_2 U588 ( .ip1(n62), .ip2(din[11]), .op(n1437) );
  nand2_2 U589 ( .ip1(csr[11]), .ip2(n59), .op(n1438) );
  nand2_2 U590 ( .ip1(n1436), .ip2(n1435), .op(n1141) );
  nand2_2 U591 ( .ip1(n62), .ip2(din[12]), .op(n1435) );
  nand2_2 U592 ( .ip1(csr[12]), .ip2(n59), .op(n1436) );
  nand2_2 U593 ( .ip1(n1434), .ip2(n1433), .op(n1140) );
  nand2_2 U594 ( .ip1(n60), .ip2(din[13]), .op(n1433) );
  nand2_2 U595 ( .ip1(csr[13]), .ip2(n59), .op(n1434) );
  nor2_2 U596 ( .ip1(n1889), .ip2(n60), .op(n1487) );
  nor2_2 U597 ( .ip1(n1469), .ip2(n1889), .op(n1488) );
  nand2_2 U598 ( .ip1(n56), .ip2(we), .op(n1469) );
  nor4_2 U599 ( .ip1(n1431), .ip2(n1430), .ip3(n1429), .ip4(n1428), .op(
        ep_match) );
  xor2_2 U600 ( .ip1(ep_sel[1]), .ip2(csr[19]), .op(n1428) );
  xor2_2 U601 ( .ip1(ep_sel[2]), .ip2(csr[20]), .op(n1429) );
  xor2_2 U602 ( .ip1(ep_sel[3]), .ip2(csr[21]), .op(n1430) );
  xor2_2 U603 ( .ip1(ep_sel[0]), .ip2(csr[18]), .op(n1431) );
  and2_2 U604 ( .ip1(n65), .ip2(buf0[9]), .op(n1426) );
  and2_2 U605 ( .ip1(n92), .ip2(buf1[9]), .op(n1427) );
  and2_2 U606 ( .ip1(n65), .ip2(buf0[8]), .op(n1424) );
  and2_2 U607 ( .ip1(n92), .ip2(buf1[8]), .op(n1425) );
  and2_2 U608 ( .ip1(n65), .ip2(buf0[7]), .op(n1422) );
  and2_2 U609 ( .ip1(n92), .ip2(buf1[7]), .op(n1423) );
  nand4_2 U610 ( .ip1(n1421), .ip2(n1420), .ip3(n1419), .ip4(n1418), .op(
        dout[6]) );
  nand2_2 U611 ( .ip1(buf1[6]), .ip2(n91), .op(n1418) );
  nand2_2 U612 ( .ip1(buf0[6]), .ip2(n63), .op(n1419) );
  nand2_2 U613 ( .ip1(n51), .ip2(int_[6]), .op(n1420) );
  nand2_2 U614 ( .ip1(csr[6]), .ip2(n55), .op(n1421) );
  nand4_2 U615 ( .ip1(n1417), .ip2(n1416), .ip3(n1415), .ip4(n1414), .op(
        dout[5]) );
  nand2_2 U616 ( .ip1(buf1[5]), .ip2(n90), .op(n1414) );
  nand2_2 U617 ( .ip1(buf0[5]), .ip2(n63), .op(n1415) );
  nand2_2 U618 ( .ip1(n51), .ip2(int_[5]), .op(n1416) );
  nand2_2 U619 ( .ip1(csr[5]), .ip2(n56), .op(n1417) );
  nand4_2 U620 ( .ip1(n1413), .ip2(n1412), .ip3(n1411), .ip4(n1410), .op(
        dout[4]) );
  nand2_2 U621 ( .ip1(buf1[4]), .ip2(n90), .op(n1410) );
  nand2_2 U622 ( .ip1(buf0[4]), .ip2(n63), .op(n1411) );
  nand2_2 U623 ( .ip1(n50), .ip2(int_[4]), .op(n1412) );
  nand2_2 U624 ( .ip1(csr[4]), .ip2(n56), .op(n1413) );
  nand4_2 U625 ( .ip1(n1409), .ip2(n1408), .ip3(n1407), .ip4(n1406), .op(
        dout[3]) );
  nand2_2 U626 ( .ip1(buf1[3]), .ip2(n90), .op(n1406) );
  nand2_2 U627 ( .ip1(buf0[3]), .ip2(n63), .op(n1407) );
  nand2_2 U628 ( .ip1(n49), .ip2(int_[3]), .op(n1408) );
  nand2_2 U629 ( .ip1(csr[3]), .ip2(n56), .op(n1409) );
  and2_2 U630 ( .ip1(n64), .ip2(buf0[31]), .op(n1404) );
  and2_2 U631 ( .ip1(n92), .ip2(buf1[31]), .op(n1405) );
  and2_2 U632 ( .ip1(n64), .ip2(buf0[30]), .op(n1402) );
  and2_2 U633 ( .ip1(n91), .ip2(buf1[30]), .op(n1403) );
  nand4_2 U634 ( .ip1(n1401), .ip2(n1400), .ip3(n1399), .ip4(n1398), .op(
        dout[2]) );
  nand2_2 U635 ( .ip1(buf1[2]), .ip2(n90), .op(n1398) );
  nand2_2 U636 ( .ip1(buf0[2]), .ip2(n63), .op(n1399) );
  nand2_2 U637 ( .ip1(n48), .ip2(int_[2]), .op(n1400) );
  nand2_2 U638 ( .ip1(csr[2]), .ip2(n56), .op(n1401) );
  nand4_2 U639 ( .ip1(n1397), .ip2(n1396), .ip3(n1395), .ip4(n1394), .op(
        dout[29]) );
  nand2_2 U640 ( .ip1(buf1[29]), .ip2(n90), .op(n1394) );
  nand2_2 U641 ( .ip1(buf0[29]), .ip2(n63), .op(n1395) );
  nand2_2 U642 ( .ip1(int__29), .ip2(n41), .op(n1396) );
  nand2_2 U643 ( .ip1(n56), .ip2(csr[29]), .op(n1397) );
  nand4_2 U644 ( .ip1(n1393), .ip2(n1392), .ip3(n1391), .ip4(n1390), .op(
        dout[28]) );
  nand2_2 U645 ( .ip1(buf1[28]), .ip2(n90), .op(n1390) );
  nand2_2 U646 ( .ip1(buf0[28]), .ip2(n63), .op(n1391) );
  nand2_2 U647 ( .ip1(int__28), .ip2(n43), .op(n1392) );
  nand2_2 U648 ( .ip1(n56), .ip2(csr[28]), .op(n1393) );
  nand4_2 U649 ( .ip1(n1389), .ip2(n1388), .ip3(n1387), .ip4(n1386), .op(
        dout[27]) );
  nand2_2 U650 ( .ip1(buf1[27]), .ip2(n90), .op(n1386) );
  nand2_2 U651 ( .ip1(buf0[27]), .ip2(n63), .op(n1387) );
  nand2_2 U652 ( .ip1(int__27), .ip2(n47), .op(n1388) );
  nand2_2 U653 ( .ip1(n56), .ip2(csr[27]), .op(n1389) );
  nand4_2 U654 ( .ip1(n1385), .ip2(n1384), .ip3(n1383), .ip4(n1382), .op(
        dout[26]) );
  nand2_2 U655 ( .ip1(buf1[26]), .ip2(n90), .op(n1382) );
  nand2_2 U656 ( .ip1(buf0[26]), .ip2(n63), .op(n1383) );
  nand2_2 U657 ( .ip1(int__26), .ip2(n46), .op(n1384) );
  nand2_2 U658 ( .ip1(csr[26]), .ip2(n55), .op(n1385) );
  nand4_2 U659 ( .ip1(n1381), .ip2(n1380), .ip3(n1379), .ip4(n1378), .op(
        dout[25]) );
  nand2_2 U660 ( .ip1(buf1[25]), .ip2(n90), .op(n1378) );
  nand2_2 U661 ( .ip1(buf0[25]), .ip2(n63), .op(n1379) );
  nand2_2 U662 ( .ip1(int__25), .ip2(n43), .op(n1380) );
  nand2_2 U663 ( .ip1(csr[25]), .ip2(n55), .op(n1381) );
  nand4_2 U664 ( .ip1(n1377), .ip2(n1376), .ip3(n1375), .ip4(n1374), .op(
        dout[24]) );
  nand2_2 U665 ( .ip1(buf1[24]), .ip2(n90), .op(n1374) );
  nand2_2 U666 ( .ip1(buf0[24]), .ip2(n63), .op(n1375) );
  nand2_2 U667 ( .ip1(int__24), .ip2(n42), .op(n1376) );
  nand2_2 U668 ( .ip1(csr[24]), .ip2(n55), .op(n1377) );
  and2_2 U669 ( .ip1(n64), .ip2(buf0[23]), .op(n1372) );
  and2_2 U670 ( .ip1(n91), .ip2(buf1[23]), .op(n1373) );
  and2_2 U671 ( .ip1(n64), .ip2(buf0[22]), .op(n1370) );
  and2_2 U672 ( .ip1(n91), .ip2(buf1[22]), .op(n1371) );
  nand4_2 U673 ( .ip1(n1369), .ip2(n1368), .ip3(n1367), .ip4(n1366), .op(
        dout[21]) );
  nand2_2 U674 ( .ip1(buf1[21]), .ip2(n91), .op(n1366) );
  nand2_2 U675 ( .ip1(buf0[21]), .ip2(n63), .op(n1367) );
  nand2_2 U676 ( .ip1(int__21), .ip2(n41), .op(n1368) );
  nand2_2 U677 ( .ip1(csr[21]), .ip2(n55), .op(n1369) );
  nand4_2 U678 ( .ip1(n1365), .ip2(n1364), .ip3(n1363), .ip4(n1362), .op(
        dout[20]) );
  nand2_2 U679 ( .ip1(buf1[20]), .ip2(n91), .op(n1362) );
  nand2_2 U680 ( .ip1(buf0[20]), .ip2(n64), .op(n1363) );
  nand2_2 U681 ( .ip1(int__20), .ip2(n40), .op(n1364) );
  nand2_2 U682 ( .ip1(csr[20]), .ip2(n55), .op(n1365) );
  nand4_2 U683 ( .ip1(n1361), .ip2(n1360), .ip3(n1359), .ip4(n1358), .op(
        dout[1]) );
  nand2_2 U684 ( .ip1(buf1[1]), .ip2(n91), .op(n1358) );
  nand2_2 U685 ( .ip1(buf0[1]), .ip2(n64), .op(n1359) );
  nand2_2 U686 ( .ip1(n47), .ip2(int_[1]), .op(n1360) );
  nand2_2 U687 ( .ip1(csr[1]), .ip2(n55), .op(n1361) );
  nand4_2 U688 ( .ip1(n1357), .ip2(n1356), .ip3(n1355), .ip4(n1354), .op(
        dout[19]) );
  nand2_2 U689 ( .ip1(buf1[19]), .ip2(n91), .op(n1354) );
  nand2_2 U690 ( .ip1(buf0[19]), .ip2(n64), .op(n1355) );
  nand2_2 U691 ( .ip1(int__19), .ip2(n37), .op(n1356) );
  nand2_2 U692 ( .ip1(csr[19]), .ip2(n55), .op(n1357) );
  nand4_2 U693 ( .ip1(n1353), .ip2(n1352), .ip3(n1351), .ip4(n1350), .op(
        dout[18]) );
  nand2_2 U694 ( .ip1(buf1[18]), .ip2(n91), .op(n1350) );
  nand2_2 U695 ( .ip1(buf0[18]), .ip2(n64), .op(n1351) );
  nand2_2 U696 ( .ip1(int__18), .ip2(n50), .op(n1352) );
  nand2_2 U697 ( .ip1(csr[18]), .ip2(n55), .op(n1353) );
  nand4_2 U698 ( .ip1(n1349), .ip2(n1348), .ip3(n1347), .ip4(n1346), .op(
        dout[17]) );
  nand2_2 U699 ( .ip1(buf1[17]), .ip2(n91), .op(n1346) );
  nand2_2 U700 ( .ip1(buf0[17]), .ip2(n64), .op(n1347) );
  nand2_2 U701 ( .ip1(int__17), .ip2(n49), .op(n1348) );
  nand2_2 U702 ( .ip1(csr[17]), .ip2(n55), .op(n1349) );
  nand4_2 U703 ( .ip1(n1345), .ip2(n1344), .ip3(n1343), .ip4(n1342), .op(
        dout[16]) );
  nand2_2 U704 ( .ip1(buf1[16]), .ip2(n91), .op(n1342) );
  nand2_2 U705 ( .ip1(buf0[16]), .ip2(n64), .op(n1343) );
  nand2_2 U706 ( .ip1(int__16), .ip2(n48), .op(n1344) );
  nand2_2 U707 ( .ip1(csr[16]), .ip2(n55), .op(n1345) );
  and2_2 U708 ( .ip1(n64), .ip2(buf0[15]), .op(n1340) );
  and2_2 U709 ( .ip1(n91), .ip2(buf1[15]), .op(n1341) );
  nand2_2 U710 ( .ip1(n1339), .ip2(n1338), .op(dout[14]) );
  nand2_2 U711 ( .ip1(buf1[14]), .ip2(n91), .op(n1338) );
  nand2_2 U712 ( .ip1(buf0[14]), .ip2(n64), .op(n1339) );
  and2_2 U713 ( .ip1(n65), .ip2(buf0[13]), .op(n1336) );
  and2_2 U714 ( .ip1(n92), .ip2(buf1[13]), .op(n1337) );
  and2_2 U715 ( .ip1(n65), .ip2(buf0[12]), .op(n1334) );
  and2_2 U716 ( .ip1(n92), .ip2(buf1[12]), .op(n1335) );
  and2_2 U717 ( .ip1(n65), .ip2(buf0[11]), .op(n1332) );
  and2_2 U718 ( .ip1(n92), .ip2(buf1[11]), .op(n1333) );
  and2_2 U719 ( .ip1(n65), .ip2(buf0[10]), .op(n1330) );
  and2_2 U720 ( .ip1(n92), .ip2(buf1[10]), .op(n1331) );
  nand4_2 U721 ( .ip1(n1329), .ip2(n1328), .ip3(n1327), .ip4(n1326), .op(
        dout[0]) );
  nand2_2 U722 ( .ip1(buf1[0]), .ip2(n90), .op(n1326) );
  nor2_2 U723 ( .ip1(n1138), .ip2(n1139), .op(n1681) );
  nand2_2 U724 ( .ip1(buf0[0]), .ip2(n64), .op(n1327) );
  nor2_2 U725 ( .ip1(n1138), .ip2(adr[0]), .op(n1516) );
  nand2_2 U726 ( .ip1(n46), .ip2(int_[0]), .op(n1328) );
  nand2_2 U727 ( .ip1(csr[0]), .ip2(n55), .op(n1329) );
  nor2_2 U728 ( .ip1(adr[0]), .ip2(adr[1]), .op(n1432) );
  nor2_2 U737 ( .ip1(buf0_orig[30]), .ip2(buf0_orig[29]), .op(n1325) );
  and2_2 U738 ( .ip1(N319), .ip2(n1324), .op(N320) );
  nand4_2 U739 ( .ip1(n1069), .ip2(n1068), .ip3(n1323), .ip4(n1322), .op(n1324) );
  nor4_2 U740 ( .ip1(n1321), .ip2(csr[4]), .ip3(csr[6]), .ip4(csr[5]), .op(
        n1322) );
  or3_2 U741 ( .ip1(csr[8]), .ip2(csr[9]), .ip3(csr[7]), .op(n1321) );
  or3_2 U746 ( .ip1(n1318), .ip2(dma_out_cnt[6]), .ip3(dma_out_cnt[5]), .op(
        n1319) );
  or3_2 U747 ( .ip1(dma_out_cnt[8]), .ip2(dma_out_cnt[9]), .ip3(dma_out_cnt[7]), .op(n1318) );
  or3_2 U748 ( .ip1(dma_out_cnt[2]), .ip2(dma_out_cnt[4]), .ip3(dma_out_cnt[3]), .op(n1320) );
  nor2_2 U749 ( .ip1(n1824), .ip2(n984), .op(N271) );
  nor2_2 U750 ( .ip1(buf0_set), .ip2(buf0_rl), .op(n1824) );
  and2_2 U752 ( .ip1(int_[5]), .ip2(int__20), .op(n1316) );
  and2_2 U753 ( .ip1(int_[2]), .ip2(int__18), .op(n1317) );
  and2_2 U758 ( .ip1(int_[5]), .ip2(int__28), .op(n1313) );
  and2_2 U759 ( .ip1(int_[2]), .ip2(int__26), .op(n1314) );
  nand2_2 U762 ( .ip1(n1101), .ip2(n1102), .op(n1315) );
  and2_2 U764 ( .ip1(re), .ip2(n42), .op(N191) );
  inv_2 U820 ( .ip(rst), .op(n1889) );
  not_ab_or_c_or_d U859 ( .ip1(n80), .ip2(din[31]), .ip3(n1678), .ip4(n1677), 
        .op(n1679) );
  not_ab_or_c_or_d U860 ( .ip1(n80), .ip2(din[30]), .ip3(n1671), .ip4(n1670), 
        .op(n1672) );
  not_ab_or_c_or_d U861 ( .ip1(n80), .ip2(din[29]), .ip3(n1667), .ip4(n1666), 
        .op(n1668) );
  not_ab_or_c_or_d U862 ( .ip1(n80), .ip2(din[28]), .ip3(n1663), .ip4(n1662), 
        .op(n1664) );
  not_ab_or_c_or_d U863 ( .ip1(n80), .ip2(din[27]), .ip3(n1659), .ip4(n1658), 
        .op(n1660) );
  not_ab_or_c_or_d U864 ( .ip1(n80), .ip2(din[26]), .ip3(n1655), .ip4(n1654), 
        .op(n1656) );
  not_ab_or_c_or_d U865 ( .ip1(n80), .ip2(din[25]), .ip3(n1651), .ip4(n1650), 
        .op(n1652) );
  not_ab_or_c_or_d U866 ( .ip1(n80), .ip2(din[24]), .ip3(n1647), .ip4(n1646), 
        .op(n1648) );
  not_ab_or_c_or_d U867 ( .ip1(n80), .ip2(din[23]), .ip3(n1643), .ip4(n1642), 
        .op(n1644) );
  not_ab_or_c_or_d U868 ( .ip1(n80), .ip2(din[22]), .ip3(n1639), .ip4(n1638), 
        .op(n1640) );
  not_ab_or_c_or_d U869 ( .ip1(n80), .ip2(din[21]), .ip3(n1635), .ip4(n1634), 
        .op(n1636) );
  not_ab_or_c_or_d U870 ( .ip1(n80), .ip2(din[20]), .ip3(n1631), .ip4(n1630), 
        .op(n1632) );
  not_ab_or_c_or_d U871 ( .ip1(n80), .ip2(din[19]), .ip3(n1627), .ip4(n1626), 
        .op(n1628) );
  not_ab_or_c_or_d U872 ( .ip1(n80), .ip2(din[18]), .ip3(n1623), .ip4(n1622), 
        .op(n1624) );
  not_ab_or_c_or_d U873 ( .ip1(n79), .ip2(din[17]), .ip3(n1619), .ip4(n1618), 
        .op(n1620) );
  not_ab_or_c_or_d U874 ( .ip1(n80), .ip2(din[16]), .ip3(n1615), .ip4(n1614), 
        .op(n1616) );
  not_ab_or_c_or_d U875 ( .ip1(n78), .ip2(din[15]), .ip3(n1611), .ip4(n1610), 
        .op(n1612) );
  not_ab_or_c_or_d U876 ( .ip1(n79), .ip2(din[14]), .ip3(n1607), .ip4(n1606), 
        .op(n1608) );
  not_ab_or_c_or_d U877 ( .ip1(n80), .ip2(din[13]), .ip3(n1603), .ip4(n1602), 
        .op(n1604) );
  not_ab_or_c_or_d U878 ( .ip1(n78), .ip2(din[12]), .ip3(n1599), .ip4(n1598), 
        .op(n1600) );
  not_ab_or_c_or_d U879 ( .ip1(n79), .ip2(din[11]), .ip3(n1595), .ip4(n1594), 
        .op(n1596) );
  not_ab_or_c_or_d U880 ( .ip1(n80), .ip2(din[10]), .ip3(n1591), .ip4(n1590), 
        .op(n1592) );
  not_ab_or_c_or_d U881 ( .ip1(n78), .ip2(din[9]), .ip3(n1587), .ip4(n1586), 
        .op(n1588) );
  not_ab_or_c_or_d U882 ( .ip1(n79), .ip2(din[8]), .ip3(n1583), .ip4(n1582), 
        .op(n1584) );
  not_ab_or_c_or_d U883 ( .ip1(n80), .ip2(din[7]), .ip3(n1579), .ip4(n1578), 
        .op(n1580) );
  not_ab_or_c_or_d U884 ( .ip1(n78), .ip2(din[6]), .ip3(n1575), .ip4(n1574), 
        .op(n1576) );
  not_ab_or_c_or_d U885 ( .ip1(n79), .ip2(din[5]), .ip3(n1571), .ip4(n1570), 
        .op(n1572) );
  not_ab_or_c_or_d U886 ( .ip1(n80), .ip2(din[4]), .ip3(n1567), .ip4(n1566), 
        .op(n1568) );
  not_ab_or_c_or_d U887 ( .ip1(n78), .ip2(din[3]), .ip3(n1563), .ip4(n1562), 
        .op(n1564) );
  not_ab_or_c_or_d U888 ( .ip1(n79), .ip2(din[2]), .ip3(n1559), .ip4(n1558), 
        .op(n1560) );
  not_ab_or_c_or_d U889 ( .ip1(n80), .ip2(din[1]), .ip3(n1555), .ip4(n1554), 
        .op(n1556) );
  not_ab_or_c_or_d U890 ( .ip1(n80), .ip2(din[0]), .ip3(n1551), .ip4(n1550), 
        .op(n1552) );
  ab_or_c_or_d U891 ( .ip1(buf0_orig[0]), .ip2(n69), .ip3(n1548), .ip4(n1889), 
        .op(n1210) );
  ab_or_c_or_d U892 ( .ip1(buf0_orig[1]), .ip2(n72), .ip3(n1547), .ip4(n1889), 
        .op(n1209) );
  ab_or_c_or_d U893 ( .ip1(buf0_orig[2]), .ip2(n72), .ip3(n1546), .ip4(n1889), 
        .op(n1208) );
  ab_or_c_or_d U894 ( .ip1(buf0_orig[3]), .ip2(n72), .ip3(n1545), .ip4(n1889), 
        .op(n1207) );
  ab_or_c_or_d U895 ( .ip1(buf0_orig[4]), .ip2(n71), .ip3(n1544), .ip4(n1889), 
        .op(n1206) );
  ab_or_c_or_d U896 ( .ip1(buf0_orig[5]), .ip2(n71), .ip3(n1543), .ip4(n1889), 
        .op(n1205) );
  ab_or_c_or_d U897 ( .ip1(buf0_orig[6]), .ip2(n71), .ip3(n1542), .ip4(n1889), 
        .op(n1204) );
  ab_or_c_or_d U898 ( .ip1(buf0_orig[7]), .ip2(n71), .ip3(n1541), .ip4(n1889), 
        .op(n1203) );
  ab_or_c_or_d U899 ( .ip1(buf0_orig[8]), .ip2(n70), .ip3(n1540), .ip4(n1889), 
        .op(n1202) );
  ab_or_c_or_d U900 ( .ip1(buf0_orig[9]), .ip2(n70), .ip3(n1539), .ip4(n1889), 
        .op(n1201) );
  ab_or_c_or_d U901 ( .ip1(buf0_orig[10]), .ip2(n70), .ip3(n1538), .ip4(n1889), 
        .op(n1200) );
  ab_or_c_or_d U902 ( .ip1(buf0_orig[11]), .ip2(n70), .ip3(n1537), .ip4(n1889), 
        .op(n1199) );
  ab_or_c_or_d U903 ( .ip1(buf0_orig[12]), .ip2(n69), .ip3(n1536), .ip4(n1889), 
        .op(n1198) );
  ab_or_c_or_d U904 ( .ip1(buf0_orig[13]), .ip2(n69), .ip3(n1535), .ip4(n1889), 
        .op(n1197) );
  ab_or_c_or_d U905 ( .ip1(buf0_orig[14]), .ip2(n69), .ip3(n1534), .ip4(n1889), 
        .op(n1196) );
  ab_or_c_or_d U906 ( .ip1(buf0_orig[15]), .ip2(n68), .ip3(n1533), .ip4(n1889), 
        .op(n1195) );
  ab_or_c_or_d U907 ( .ip1(buf0_orig[16]), .ip2(n68), .ip3(n1532), .ip4(n1889), 
        .op(n1194) );
  ab_or_c_or_d U908 ( .ip1(buf0_orig[17]), .ip2(n68), .ip3(n1531), .ip4(n1889), 
        .op(n1193) );
  ab_or_c_or_d U909 ( .ip1(buf0_orig[18]), .ip2(n68), .ip3(n1530), .ip4(n1889), 
        .op(n1192) );
  ab_or_c_or_d U922 ( .ip1(buf0_orig[31]), .ip2(n67), .ip3(n1517), .ip4(n1889), 
        .op(n1179) );
  ab_or_c_or_d U923 ( .ip1(csr[22]), .ip2(n1474), .ip3(n1067), .ip4(n1473), 
        .op(n1159) );
  ab_or_c_or_d U924 ( .ip1(csr[9]), .ip2(n56), .ip3(n1427), .ip4(n1426), .op(
        dout[9]) );
  ab_or_c_or_d U925 ( .ip1(csr[8]), .ip2(n56), .ip3(n1425), .ip4(n1424), .op(
        dout[8]) );
  ab_or_c_or_d U926 ( .ip1(csr[7]), .ip2(n56), .ip3(n1423), .ip4(n1422), .op(
        dout[7]) );
  ab_or_c_or_d U927 ( .ip1(n57), .ip2(csr[31]), .ip3(n1405), .ip4(n1404), .op(
        dout[31]) );
  ab_or_c_or_d U928 ( .ip1(n57), .ip2(csr[30]), .ip3(n1403), .ip4(n1402), .op(
        dout[30]) );
  ab_or_c_or_d U929 ( .ip1(csr[23]), .ip2(n56), .ip3(n1373), .ip4(n1372), .op(
        dout[23]) );
  ab_or_c_or_d U930 ( .ip1(csr[22]), .ip2(n56), .ip3(n1371), .ip4(n1370), .op(
        dout[22]) );
  ab_or_c_or_d U931 ( .ip1(n57), .ip2(csr[15]), .ip3(n1341), .ip4(n1340), .op(
        dout[15]) );
  ab_or_c_or_d U932 ( .ip1(csr[13]), .ip2(n56), .ip3(n1337), .ip4(n1336), .op(
        dout[13]) );
  ab_or_c_or_d U933 ( .ip1(csr[12]), .ip2(n56), .ip3(n1335), .ip4(n1334), .op(
        dout[12]) );
  ab_or_c_or_d U934 ( .ip1(csr[11]), .ip2(n56), .ip3(n1333), .ip4(n1332), .op(
        dout[11]) );
  ab_or_c_or_d U935 ( .ip1(csr[10]), .ip2(n56), .ip3(n1331), .ip4(n1330), .op(
        dout[10]) );
  dp_2 intb_reg ( .ip(N222), .ck(wclk), .q(intb) );
  dp_2 \buf0_orig_m3_reg[10]  ( .ip(N345), .ck(wclk), .q(buf0_orig_m3[10]) );
  dp_2 \buf0_orig_m3_reg[9]  ( .ip(N344), .ck(wclk), .q(buf0_orig_m3[9]) );
  dp_2 \buf0_orig_m3_reg[8]  ( .ip(N343), .ck(wclk), .q(buf0_orig_m3[8]) );
  dp_2 \buf0_orig_m3_reg[7]  ( .ip(N342), .ck(wclk), .q(buf0_orig_m3[7]) );
  dp_2 \buf0_orig_m3_reg[0]  ( .ip(n989), .ck(wclk), .q(buf0_orig_m3[0]) );
  usbf_ep_rf_1_DW01_sub_1 sub_7204 ( .A(buf0_orig[30:19]), .B(dma_out_cnt), 
        .CI(1'b0), .DIFF({N332, N331, N330, N329, N328, N327, N326, N325, N324, 
        N323, N322, N321}) );
  usbf_ep_rf_1_DW01_sub_2 sub_7193 ( .A(dma_in_cnt), .B({1'b0, 1'b0, 1'b0, 
        csr[10:2]}), .CI(1'b0), .DIFF({N302, N301, N300, N299, N298, N297, 
        N296, N295, N294, N293, N292, N291}) );
  usbf_ep_rf_1_DW01_inc_0 add_7190_S2 ( .A(dma_in_cnt), .SUM({N289, N288, N287, 
        N286, N285, N284, N283, N282, N281, N280, N279, N278}) );
  usbf_ep_rf_1_DW01_add_0 add_7168 ( .A(dma_out_cnt), .B({1'b0, 1'b0, 1'b0, 
        csr[10:2]}), .CI(1'b0), .SUM({N253, N252, N251, N250, N249, N248, N247, 
        N246, N245, N244, N243, N242}) );
  usbf_ep_rf_1_DW01_dec_0 sub_7165_S2 ( .A(dma_out_cnt), .SUM({N240, N239, 
        N238, N237, N236, N235, N234, N233, N232, N231, N230, N229}) );
  dp_1 dma_in_buf_sz1_reg ( .ip(N320), .ck(n34), .q(dma_in_buf_sz1) );
  dp_1 \dma_out_left_reg[11]  ( .ip(N332), .ck(n33), .q(dma_out_left[11]) );
  dp_1 \dma_out_left_reg[10]  ( .ip(N331), .ck(n33), .q(dma_out_left[10]) );
  dp_1 \dma_out_left_reg[9]  ( .ip(N330), .ck(n32), .q(dma_out_left[9]) );
  dp_1 \dma_out_left_reg[0]  ( .ip(N321), .ck(n32), .q(dma_out_left[0]) );
  dp_1 set_r_reg ( .ip(N271), .ck(n31), .q(set_r) );
  dp_1 \dma_out_left_reg[1]  ( .ip(N322), .ck(n32), .q(dma_out_left[1]) );
  dp_1 \dma_out_left_reg[3]  ( .ip(N324), .ck(n32), .q(dma_out_left[3]) );
  dp_1 \dma_out_left_reg[5]  ( .ip(N326), .ck(n32), .q(dma_out_left[5]) );
  dp_1 \dma_out_left_reg[7]  ( .ip(N328), .ck(n32), .q(dma_out_left[7]) );
  dp_1 dma_out_buf_avail_reg ( .ip(N333), .ck(n33), .q(dma_out_buf_avail) );
  dp_1 \dma_out_left_reg[2]  ( .ip(N323), .ck(n32), .q(dma_out_left[2]) );
  dp_1 \dma_out_left_reg[4]  ( .ip(N325), .ck(n32), .q(dma_out_left[4]) );
  dp_1 \dma_out_left_reg[6]  ( .ip(N327), .ck(n32), .q(dma_out_left[6]) );
  dp_1 \buf0_orig_reg[31]  ( .ip(n1179), .ck(n22), .q(buf0_orig[31]) );
  dp_1 \buf0_orig_reg[18]  ( .ip(n1192), .ck(n23), .q(buf0_orig[18]) );
  dp_1 \buf0_orig_reg[17]  ( .ip(n1193), .ck(n23), .q(buf0_orig[17]) );
  dp_1 \buf0_orig_reg[16]  ( .ip(n1194), .ck(n23), .q(buf0_orig[16]) );
  dp_1 \buf0_orig_reg[15]  ( .ip(n1195), .ck(n23), .q(buf0_orig[15]) );
  dp_1 \buf0_orig_reg[14]  ( .ip(n1196), .ck(n23), .q(buf0_orig[14]) );
  dp_1 \buf0_orig_reg[13]  ( .ip(n1197), .ck(n24), .q(buf0_orig[13]) );
  dp_1 \buf0_orig_reg[12]  ( .ip(n1198), .ck(n24), .q(buf0_orig[12]) );
  dp_1 \buf0_orig_reg[11]  ( .ip(n1199), .ck(n24), .q(buf0_orig[11]) );
  dp_1 \buf0_orig_reg[10]  ( .ip(n1200), .ck(n24), .q(buf0_orig[10]) );
  dp_1 \buf0_orig_reg[9]  ( .ip(n1201), .ck(n24), .q(buf0_orig[9]) );
  dp_1 \buf0_orig_reg[8]  ( .ip(n1202), .ck(n24), .q(buf0_orig[8]) );
  dp_1 \buf0_orig_reg[7]  ( .ip(n1203), .ck(n24), .q(buf0_orig[7]) );
  dp_1 \buf0_orig_reg[6]  ( .ip(n1204), .ck(n24), .q(buf0_orig[6]) );
  dp_1 \buf0_orig_reg[5]  ( .ip(n1205), .ck(n24), .q(buf0_orig[5]) );
  dp_1 \buf0_orig_reg[4]  ( .ip(n1206), .ck(n24), .q(buf0_orig[4]) );
  dp_1 \buf0_orig_reg[3]  ( .ip(n1207), .ck(n24), .q(buf0_orig[3]) );
  dp_1 \buf0_orig_reg[2]  ( .ip(n1208), .ck(n24), .q(buf0_orig[2]) );
  dp_1 \buf0_orig_reg[1]  ( .ip(n1209), .ck(n24), .q(buf0_orig[1]) );
  dp_1 \buf0_orig_reg[0]  ( .ip(n1210), .ck(n25), .q(buf0_orig[0]) );
  dp_1 \dma_out_left_reg[8]  ( .ip(N329), .ck(n32), .q(dma_out_left[8]) );
  dp_1 \csr1_reg[7]  ( .ip(n1159), .ck(n21), .q(csr[22]) );
  dp_1 r5_reg ( .ip(r4), .ck(n31), .q(r5) );
  dp_1 int_re_reg ( .ip(N191), .ck(n30), .q(int_re) );
  dp_1 \int_stat_reg[4]  ( .ip(n1277), .ck(n30), .q(int_[4]) );
  dp_1 \int_stat_reg[3]  ( .ip(n1278), .ck(n30), .q(int_[3]) );
  dp_1 \csr0_reg[12]  ( .ip(n1141), .ck(n20), .q(csr[12]) );
  dp_1 \csr0_reg[11]  ( .ip(n1142), .ck(n20), .q(csr[11]) );
  dp_1 \csr1_reg[8]  ( .ip(n1158), .ck(n21), .q(csr[23]) );
  dp_1 \iena_reg[3]  ( .ip(n1169), .ck(n21), .q(int__27) );
  dp_1 \ienb_reg[3]  ( .ip(n1175), .ck(n22), .q(int__19) );
  dp_1 \uc_bsel_reg[1]  ( .ip(n1284), .ck(n30), .q(csr[31]) );
  dp_1 \uc_bsel_reg[0]  ( .ip(n1285), .ck(n31), .q(csr[30]) );
  dp_1 \csr1_reg[10]  ( .ip(n1156), .ck(n20), .q(csr[25]) );
  dp_1 \csr1_reg[9]  ( .ip(n1157), .ck(n20), .q(csr[24]) );
  dp_1 \csr1_reg[2]  ( .ip(n1164), .ck(n21), .q(csr[17]) );
  dp_1 \csr1_reg[1]  ( .ip(n1165), .ck(n21), .q(csr[16]) );
  dp_1 \iena_reg[5]  ( .ip(n1167), .ck(n21), .q(int__29) );
  dp_1 \iena_reg[1]  ( .ip(n1171), .ck(n22), .q(int__25) );
  dp_1 \iena_reg[0]  ( .ip(n1172), .ck(n22), .q(int__24) );
  dp_1 \ienb_reg[5]  ( .ip(n1173), .ck(n22), .q(int__21) );
  dp_1 \ienb_reg[1]  ( .ip(n1177), .ck(n22), .q(int__17) );
  dp_1 \ienb_reg[0]  ( .ip(n1178), .ck(n22), .q(int__16) );
  dp_1 \buf0_reg[29]  ( .ip(n1240), .ck(n25), .q(buf0[29]) );
  dp_1 \buf0_reg[28]  ( .ip(n1239), .ck(n25), .q(buf0[28]) );
  dp_1 \buf0_reg[27]  ( .ip(n1238), .ck(n25), .q(buf0[27]) );
  dp_1 \buf0_reg[26]  ( .ip(n1237), .ck(n25), .q(buf0[26]) );
  dp_1 \buf0_reg[25]  ( .ip(n1236), .ck(n25), .q(buf0[25]) );
  dp_1 \buf0_reg[24]  ( .ip(n1235), .ck(n25), .q(buf0[24]) );
  dp_1 \buf0_reg[21]  ( .ip(n1232), .ck(n25), .q(buf0[21]) );
  dp_1 \buf0_reg[20]  ( .ip(n1231), .ck(n26), .q(buf0[20]) );
  dp_1 \buf0_reg[19]  ( .ip(n1230), .ck(n26), .q(buf0[19]) );
  dp_1 \buf0_reg[18]  ( .ip(n1229), .ck(n26), .q(buf0[18]) );
  dp_1 \buf0_reg[17]  ( .ip(n1228), .ck(n26), .q(buf0[17]) );
  dp_1 \buf0_reg[16]  ( .ip(n1227), .ck(n26), .q(buf0[16]) );
  dp_1 \buf0_reg[14]  ( .ip(n1225), .ck(n26), .q(buf0[14]) );
  dp_1 \buf0_reg[6]  ( .ip(n1217), .ck(n27), .q(buf0[6]) );
  dp_1 \buf0_reg[5]  ( .ip(n1216), .ck(n27), .q(buf0[5]) );
  dp_1 \buf0_reg[4]  ( .ip(n1215), .ck(n27), .q(buf0[4]) );
  dp_1 \buf0_reg[3]  ( .ip(n1214), .ck(n27), .q(buf0[3]) );
  dp_1 \buf0_reg[2]  ( .ip(n1213), .ck(n27), .q(buf0[2]) );
  dp_1 \buf0_reg[1]  ( .ip(n1212), .ck(n27), .q(buf0[1]) );
  dp_1 \buf0_reg[0]  ( .ip(n1211), .ck(n27), .q(buf0[0]) );
  dp_1 \buf1_reg[29]  ( .ip(n1272), .ck(n27), .q(buf1[29]) );
  dp_1 \buf1_reg[28]  ( .ip(n1271), .ck(n27), .q(buf1[28]) );
  dp_1 \buf1_reg[27]  ( .ip(n1270), .ck(n28), .q(buf1[27]) );
  dp_1 \buf1_reg[26]  ( .ip(n1269), .ck(n28), .q(buf1[26]) );
  dp_1 \buf1_reg[25]  ( .ip(n1268), .ck(n28), .q(buf1[25]) );
  dp_1 \buf1_reg[24]  ( .ip(n1267), .ck(n28), .q(buf1[24]) );
  dp_1 \buf1_reg[21]  ( .ip(n1264), .ck(n28), .q(buf1[21]) );
  dp_1 \buf1_reg[20]  ( .ip(n1263), .ck(n28), .q(buf1[20]) );
  dp_1 \buf1_reg[19]  ( .ip(n1262), .ck(n28), .q(buf1[19]) );
  dp_1 \buf1_reg[18]  ( .ip(n1261), .ck(n28), .q(buf1[18]) );
  dp_1 \buf1_reg[17]  ( .ip(n1260), .ck(n28), .q(buf1[17]) );
  dp_1 \buf1_reg[16]  ( .ip(n1259), .ck(n28), .q(buf1[16]) );
  dp_1 \buf1_reg[14]  ( .ip(n1257), .ck(n29), .q(buf1[14]) );
  dp_1 \buf1_reg[6]  ( .ip(n1249), .ck(n29), .q(buf1[6]) );
  dp_1 \buf1_reg[5]  ( .ip(n1248), .ck(n29), .q(buf1[5]) );
  dp_1 \buf1_reg[4]  ( .ip(n1247), .ck(n29), .q(buf1[4]) );
  dp_1 \buf1_reg[3]  ( .ip(n1246), .ck(n29), .q(buf1[3]) );
  dp_1 \buf1_reg[2]  ( .ip(n1245), .ck(n29), .q(buf1[2]) );
  dp_1 \buf1_reg[1]  ( .ip(n1244), .ck(n30), .q(buf1[1]) );
  dp_1 \buf1_reg[0]  ( .ip(n1243), .ck(n30), .q(buf1[0]) );
  dp_1 \iena_reg[4]  ( .ip(n1168), .ck(n21), .q(int__28) );
  dp_1 \iena_reg[2]  ( .ip(n1170), .ck(n21), .q(int__26) );
  dp_1 \ienb_reg[4]  ( .ip(n1174), .ck(n22), .q(int__20) );
  dp_1 \ienb_reg[2]  ( .ip(n1176), .ck(n22), .q(int__18) );
  dp_1 \buf0_reg[31]  ( .ip(n1242), .ck(n25), .q(buf0[31]) );
  dp_1 \buf0_reg[30]  ( .ip(n1241), .ck(n25), .q(buf0[30]) );
  dp_1 \buf0_reg[23]  ( .ip(n1234), .ck(n25), .q(buf0[23]) );
  dp_1 \buf0_reg[22]  ( .ip(n1233), .ck(n25), .q(buf0[22]) );
  dp_1 \buf0_reg[15]  ( .ip(n1226), .ck(n26), .q(buf0[15]) );
  dp_1 \buf0_reg[13]  ( .ip(n1224), .ck(n26), .q(buf0[13]) );
  dp_1 \buf0_reg[12]  ( .ip(n1223), .ck(n26), .q(buf0[12]) );
  dp_1 \buf0_reg[11]  ( .ip(n1222), .ck(n26), .q(buf0[11]) );
  dp_1 \buf0_reg[10]  ( .ip(n1221), .ck(n26), .q(buf0[10]) );
  dp_1 \buf0_reg[9]  ( .ip(n1220), .ck(n26), .q(buf0[9]) );
  dp_1 \buf0_reg[8]  ( .ip(n1219), .ck(n26), .q(buf0[8]) );
  dp_1 \buf0_reg[7]  ( .ip(n1218), .ck(n27), .q(buf0[7]) );
  dp_1 \buf1_reg[31]  ( .ip(n1274), .ck(n27), .q(buf1[31]) );
  dp_1 \buf1_reg[30]  ( .ip(n1273), .ck(n27), .q(buf1[30]) );
  dp_1 \buf1_reg[23]  ( .ip(n1266), .ck(n28), .q(buf1[23]) );
  dp_1 \buf1_reg[22]  ( .ip(n1265), .ck(n28), .q(buf1[22]) );
  dp_1 \buf1_reg[15]  ( .ip(n1258), .ck(n28), .q(buf1[15]) );
  dp_1 \buf1_reg[13]  ( .ip(n1256), .ck(n29), .q(buf1[13]) );
  dp_1 \buf1_reg[12]  ( .ip(n1255), .ck(n29), .q(buf1[12]) );
  dp_1 \buf1_reg[11]  ( .ip(n1254), .ck(n29), .q(buf1[11]) );
  dp_1 \buf1_reg[10]  ( .ip(n1253), .ck(n29), .q(buf1[10]) );
  dp_1 \buf1_reg[9]  ( .ip(n1252), .ck(n29), .q(buf1[9]) );
  dp_1 \buf1_reg[8]  ( .ip(n1251), .ck(n29), .q(buf1[8]) );
  dp_1 \buf1_reg[7]  ( .ip(n1250), .ck(n29), .q(buf1[7]) );
  dp_1 \uc_dpd_reg[1]  ( .ip(n1282), .ck(n30), .q(csr[29]) );
  dp_1 \uc_dpd_reg[0]  ( .ip(n1283), .ck(n30), .q(csr[28]) );
  dp_1 ots_stop_reg ( .ip(n1140), .ck(n27), .q(csr[13]) );
  dp_1 \csr0_reg[0]  ( .ip(n1153), .ck(n20), .q(csr[0]) );
  dp_1 \csr1_reg[11]  ( .ip(n1155), .ck(n20), .q(csr[26]) );
  dp_1 \int_stat_reg[5]  ( .ip(n1276), .ck(n30), .q(int_[5]) );
  dp_1 \int_stat_reg[2]  ( .ip(n1279), .ck(n30), .q(int_[2]) );
  dp_1 \int_stat_reg[6]  ( .ip(n1275), .ck(n30), .q(int_[6]) );
  dp_1 \int_stat_reg[1]  ( .ip(n1280), .ck(n30), .q(int_[1]) );
  dp_1 \int_stat_reg[0]  ( .ip(n1281), .ck(n30), .q(int_[0]) );
  dp_1 ep_match_r_reg ( .ip(ep_match), .ck(n25), .q(ep_match_r) );
  dp_1 \buf0_orig_reg[27]  ( .ip(n1183), .ck(n22), .q(buf0_orig[27]) );
  dp_1 \csr1_reg[0]  ( .ip(n1166), .ck(n21), .q(csr[15]) );
  dp_1 \csr1_reg[6]  ( .ip(n1160), .ck(n21), .q(csr[21]) );
  dp_1 \csr1_reg[5]  ( .ip(n1161), .ck(n21), .q(csr[20]) );
  dp_1 \csr1_reg[4]  ( .ip(n1162), .ck(n21), .q(csr[19]) );
  dp_1 \csr1_reg[3]  ( .ip(n1163), .ck(n21), .q(csr[18]) );
  dp_1 \csr0_reg[8]  ( .ip(n1145), .ck(n34), .q(csr[8]) );
  dp_1 \csr0_reg[1]  ( .ip(n1152), .ck(n20), .q(csr[1]) );
  dp_1 \csr0_reg[6]  ( .ip(n1147), .ck(n34), .q(csr[6]) );
  dp_1 \csr0_reg[4]  ( .ip(n1149), .ck(n34), .q(csr[4]) );
  dp_1 \buf0_orig_reg[28]  ( .ip(n1182), .ck(n22), .q(buf0_orig[28]) );
  dp_1 \csr0_reg[10]  ( .ip(n1143), .ck(n34), .q(csr[10]) );
  dp_1 \buf0_orig_reg[25]  ( .ip(n1185), .ck(n23), .q(buf0_orig[25]) );
  dp_1 \buf0_orig_reg[23]  ( .ip(n1187), .ck(n23), .q(buf0_orig[23]) );
  dp_1 \dma_in_cnt_reg[8]  ( .ip(n1296), .ck(n34), .q(dma_in_cnt[8]) );
  dp_1 \dma_out_cnt_reg[11]  ( .ip(n1299), .ck(n31), .q(dma_out_cnt[11]) );
  dp_1 \dma_out_cnt_reg[4]  ( .ip(n1304), .ck(n31), .q(dma_out_cnt[4]) );
  dp_1 \dma_out_cnt_reg[6]  ( .ip(n1306), .ck(n31), .q(dma_out_cnt[6]) );
  dp_1 \dma_out_cnt_reg[3]  ( .ip(n1303), .ck(n31), .q(dma_out_cnt[3]) );
  dp_1 \dma_out_cnt_reg[5]  ( .ip(n1305), .ck(n31), .q(dma_out_cnt[5]) );
  dp_1 \dma_out_cnt_reg[7]  ( .ip(n1307), .ck(n32), .q(dma_out_cnt[7]) );
  dp_1 \dma_out_cnt_reg[2]  ( .ip(n1302), .ck(n31), .q(dma_out_cnt[2]) );
  dp_1 \dma_out_cnt_reg[8]  ( .ip(n1308), .ck(n32), .q(dma_out_cnt[8]) );
  dp_1 \dma_out_cnt_reg[1]  ( .ip(n1301), .ck(n31), .q(dma_out_cnt[1]) );
  dp_1 \dma_in_cnt_reg[9]  ( .ip(n1297), .ck(n34), .q(dma_in_cnt[9]) );
  dp_1 \buf0_orig_reg[26]  ( .ip(n1184), .ck(n23), .q(buf0_orig[26]) );
  dp_1 \buf0_orig_reg[24]  ( .ip(n1186), .ck(n23), .q(buf0_orig[24]) );
  dp_1 \dma_out_cnt_reg[9]  ( .ip(n1309), .ck(n32), .q(dma_out_cnt[9]) );
  dp_1 \dma_in_cnt_reg[11]  ( .ip(n1287), .ck(n33), .q(dma_in_cnt[11]) );
  dp_1 \dma_in_cnt_reg[5]  ( .ip(n1293), .ck(n33), .q(dma_in_cnt[5]) );
  dp_1 \dma_in_cnt_reg[10]  ( .ip(n1298), .ck(n33), .q(dma_in_cnt[10]) );
  dp_1 \dma_in_cnt_reg[3]  ( .ip(n1291), .ck(n33), .q(dma_in_cnt[3]) );
  dp_1 \dma_in_cnt_reg[7]  ( .ip(n1295), .ck(n33), .q(dma_in_cnt[7]) );
  dp_1 \buf0_orig_reg[19]  ( .ip(n1191), .ck(n23), .q(buf0_orig[19]) );
  dp_1 \csr1_reg[12]  ( .ip(n1154), .ck(n20), .q(csr[27]) );
  dp_1 \buf0_orig_reg[30]  ( .ip(n1180), .ck(n22), .q(buf0_orig[30]) );
  dp_1 \dma_out_cnt_reg[10]  ( .ip(n1310), .ck(n31), .q(dma_out_cnt[10]) );
  dp_1 \buf0_orig_reg[22]  ( .ip(n1188), .ck(n23), .q(buf0_orig[22]) );
  dp_1 \dma_in_cnt_reg[6]  ( .ip(n1294), .ck(n33), .q(dma_in_cnt[6]) );
  dp_1 \buf0_orig_reg[21]  ( .ip(n1189), .ck(n23), .q(buf0_orig[21]) );
  dp_1 \dma_in_cnt_reg[0]  ( .ip(n1288), .ck(n33), .q(dma_in_cnt[0]) );
  dp_1 \dma_out_cnt_reg[0]  ( .ip(n1300), .ck(n31), .q(dma_out_cnt[0]) );
  dp_1 \buf0_orig_reg[20]  ( .ip(n1190), .ck(n23), .q(buf0_orig[20]) );
  dp_1 \dma_in_cnt_reg[1]  ( .ip(n1289), .ck(n33), .q(dma_in_cnt[1]) );
  dp_1 \csr0_reg[9]  ( .ip(n1144), .ck(n20), .q(csr[9]) );
  dp_1 \csr0_reg[7]  ( .ip(n1146), .ck(n20), .q(csr[7]) );
  dp_1 \buf0_orig_reg[29]  ( .ip(n1181), .ck(n22), .q(buf0_orig[29]) );
  dp_1 \dma_in_cnt_reg[2]  ( .ip(n1290), .ck(n33), .q(dma_in_cnt[2]) );
  dp_1 \csr0_reg[5]  ( .ip(n1148), .ck(n20), .q(csr[5]) );
  dp_1 \dma_in_cnt_reg[4]  ( .ip(n1292), .ck(n33), .q(dma_in_cnt[4]) );
  dp_1 \csr0_reg[3]  ( .ip(n1150), .ck(n20), .q(csr[3]) );
  dp_1 \csr0_reg[2]  ( .ip(n1151), .ck(n20), .q(csr[2]) );
  dp_1 dma_ack_clr1_reg ( .ip(r4), .ck(wclk), .q(dma_ack_clr1) );
  dp_1 inta_reg ( .ip(N221), .ck(wclk), .q(inta) );
  dp_1 dma_req_out_hold_reg ( .ip(N272), .ck(wclk), .q(dma_req_out_hold) );
  dp_1 dma_req_in_hold_reg ( .ip(N348), .ck(wclk), .q(dma_req_in_hold) );
  dp_1 \buf0_orig_m3_reg[1]  ( .ip(N336), .ck(wclk), .q(buf0_orig_m3[1]) );
  dp_1 \buf0_orig_m3_reg[2]  ( .ip(N337), .ck(wclk), .q(buf0_orig_m3[2]) );
  dp_1 r4_reg ( .ip(dma_ack_wr1), .ck(n31), .q(r4) );
  dp_1 r1_reg ( .ip(N361), .ck(wclk), .q(r1) );
  dp_1 dma_ack_wr1_reg ( .ip(n1286), .ck(wclk), .q(dma_ack_wr1) );
  dp_1 r2_reg ( .ip(n1311), .ck(wclk), .q(r2) );
  dp_1 dma_req_r_reg ( .ip(n1312), .ck(wclk), .q(dma_req) );
  dp_2 dma_req_in_hold2_reg ( .ip(N347), .ck(wclk), .q(dma_req_in_hold2) );
  dp_1 \buf0_orig_m3_reg[11]  ( .ip(N346), .ck(wclk), .q(buf0_orig_m3[11]) );
  dp_1 \buf0_orig_m3_reg[6]  ( .ip(N341), .ck(wclk), .q(buf0_orig_m3[6]) );
  dp_1 \buf0_orig_m3_reg[5]  ( .ip(N340), .ck(wclk), .q(buf0_orig_m3[5]) );
  dp_1 \buf0_orig_m3_reg[3]  ( .ip(N338), .ck(wclk), .q(buf0_orig_m3[3]) );
  dp_2 \buf0_orig_m3_reg[4]  ( .ip(N339), .ck(wclk), .q(buf0_orig_m3[4]) );
  and2_2 U3 ( .ip1(n1821), .ip2(n19), .op(n1) );
  and2_1 U4 ( .ip1(buf0_orig_m3[3]), .ip2(n996), .op(n2) );
  and2_2 U5 ( .ip1(csr[27]), .ip2(n959), .op(n3) );
  nand2_1 U6 ( .ip1(dma_ack), .ip2(n971), .op(n972) );
  nand2_1 U7 ( .ip1(dma_req_in_hold2), .ip2(dma_req_in_hold), .op(n970) );
  and2_1 U8 ( .ip1(rst), .ip2(n973), .op(n1312) );
  nand2_1 U9 ( .ip1(n730), .ip2(buf0_orig_m3[4]), .op(n4) );
  nand2_1 U10 ( .ip1(n730), .ip2(buf0_orig_m3[4]), .op(n911) );
  nand2_1 U12 ( .ip1(n995), .ip2(buf0_orig_m3[5]), .op(n5) );
  nand2_1 U13 ( .ip1(n995), .ip2(buf0_orig_m3[5]), .op(n910) );
  nand2_1 U16 ( .ip1(n941), .ip2(n940), .op(n946) );
  nand2_1 U17 ( .ip1(n946), .ip2(n945), .op(n947) );
  inv_1 U18 ( .ip(n5), .op(n732) );
  nand2_1 U19 ( .ip1(n926), .ip2(n927), .op(n948) );
  nand2_1 U20 ( .ip1(r1), .ip2(n968), .op(n11) );
  inv_1 U21 ( .ip(n724), .op(n15) );
  nand2_1 U22 ( .ip1(dma_ack_wr1), .ip2(n949), .op(n14) );
  inv_2 U23 ( .ip(dma_ack), .op(n13) );
  nand2_1 U72 ( .ip1(n7), .ip2(n8), .op(N347) );
  nand2_1 U73 ( .ip1(n947), .ip2(n948), .op(n7) );
  nand2_1 U76 ( .ip1(buf0_orig_m3[11]), .ip2(n960), .op(n8) );
  nand2_1 U77 ( .ip1(dma_in_cnt[3]), .ip2(n729), .op(n908) );
  or2_1 U80 ( .ip1(n730), .ip2(buf0_orig_m3[4]), .op(n909) );
  nand2_1 U81 ( .ip1(n11), .ip2(n12), .op(n973) );
  nand2_1 U84 ( .ip1(dma_req), .ip2(n972), .op(n12) );
  nand2_1 U85 ( .ip1(n14), .ip2(n13), .op(n951) );
  inv_2 U88 ( .ip(int_re), .op(n19) );
  nand3_1 U89 ( .ip1(n925), .ip2(n923), .ip3(n924), .op(n926) );
  nand2_1 U92 ( .ip1(n922), .ip2(n921), .op(n923) );
  nand2_1 U93 ( .ip1(n910), .ip2(n911), .op(n912) );
  inv_1 U96 ( .ip(n4), .op(n731) );
  buf_2 U97 ( .ip(n1137), .op(n53) );
  buf_2 U100 ( .ip(n1137), .op(n52) );
  buf_2 U101 ( .ip(n1137), .op(n54) );
  inv_2 U104 ( .ip(n73), .op(n78) );
  inv_2 U105 ( .ip(n73), .op(n80) );
  inv_2 U108 ( .ip(n72), .op(n79) );
  buf_2 U109 ( .ip(n1777), .op(n93) );
  buf_2 U112 ( .ip(n1777), .op(n94) );
  buf_2 U113 ( .ip(n1674), .op(n81) );
  buf_2 U116 ( .ip(n1674), .op(n82) );
  nand2_2 U117 ( .ip1(n65), .ip2(we), .op(n1549) );
  buf_2 U118 ( .ip(n1883), .op(n99) );
  buf_2 U124 ( .ip(n1681), .op(n91) );
  buf_2 U125 ( .ip(n1676), .op(n87) );
  buf_2 U126 ( .ip(n1676), .op(n88) );
  buf_2 U179 ( .ip(n1681), .op(n90) );
  buf_2 U476 ( .ip(n1883), .op(n100) );
  buf_2 U513 ( .ip(n1885), .op(n101) );
  buf_2 U514 ( .ip(n1885), .op(n102) );
  inv_2 U515 ( .ip(n35), .op(n36) );
  and2_2 U517 ( .ip1(n16), .ip2(n15), .op(n927) );
  and2_1 U729 ( .ip1(n934), .ip2(n935), .op(n16) );
  nor3_2 U730 ( .ip1(n1884), .ip2(n99), .ip3(n1070), .op(n1885) );
  buf_2 U731 ( .ip(n1516), .op(n64) );
  buf_2 U732 ( .ip(n1487), .op(n59) );
  buf_2 U733 ( .ip(n1675), .op(n84) );
  buf_2 U734 ( .ip(n1488), .op(n60) );
  buf_2 U735 ( .ip(n1488), .op(n61) );
  buf_2 U736 ( .ip(n1675), .op(n85) );
  buf_2 U742 ( .ip(n1432), .op(n56) );
  buf_2 U743 ( .ip(n1487), .op(n58) );
  buf_2 U744 ( .ip(n1516), .op(n63) );
  buf_2 U745 ( .ip(n1432), .op(n55) );
  buf_2 U751 ( .ip(n1778), .op(n96) );
  buf_2 U754 ( .ip(n1778), .op(n97) );
  buf_2 U755 ( .ip(n1778), .op(n98) );
  inv_2 U756 ( .ip(n1884), .op(n35) );
  and2_2 U757 ( .ip1(n17), .ip2(n18), .op(n925) );
  or2_1 U760 ( .ip1(buf0_orig_m3[5]), .ip2(n995), .op(n17) );
  or2_1 U761 ( .ip1(buf0_orig_m3[6]), .ip2(n725), .op(n18) );
  nor2_2 U763 ( .ip1(n1889), .ip2(n1513), .op(n1512) );
  and3_2 U765 ( .ip1(we), .ip2(rst), .ip3(n40), .op(n1513) );
  nor3_2 U766 ( .ip1(csr[1]), .ip2(csr[3]), .ip3(csr[2]), .op(n1323) );
  nor3_2 U767 ( .ip1(csr[27]), .ip2(n123), .ip3(n959), .op(N348) );
  buf_1 U768 ( .ip(clk), .op(n20) );
  buf_1 U769 ( .ip(clk), .op(n21) );
  buf_1 U770 ( .ip(clk), .op(n22) );
  buf_1 U771 ( .ip(clk), .op(n23) );
  buf_1 U772 ( .ip(clk), .op(n24) );
  buf_1 U773 ( .ip(clk), .op(n25) );
  buf_1 U774 ( .ip(clk), .op(n26) );
  buf_1 U775 ( .ip(clk), .op(n27) );
  buf_1 U776 ( .ip(clk), .op(n28) );
  buf_1 U777 ( .ip(clk), .op(n29) );
  buf_1 U778 ( .ip(clk), .op(n30) );
  buf_1 U779 ( .ip(clk), .op(n31) );
  buf_1 U780 ( .ip(clk), .op(n32) );
  buf_1 U781 ( .ip(clk), .op(n33) );
  buf_1 U782 ( .ip(clk), .op(n34) );
  nor2_2 U783 ( .ip1(n1139), .ip2(adr[1]), .op(n37) );
  nor2_2 U784 ( .ip1(n1139), .ip2(adr[1]), .op(n38) );
  inv_1 U785 ( .ip(n37), .op(n39) );
  inv_1 U786 ( .ip(n44), .op(n40) );
  inv_1 U787 ( .ip(n45), .op(n41) );
  inv_1 U788 ( .ip(n39), .op(n42) );
  inv_1 U789 ( .ip(n39), .op(n43) );
  inv_1 U790 ( .ip(n38), .op(n44) );
  inv_1 U791 ( .ip(n38), .op(n45) );
  inv_1 U792 ( .ip(n44), .op(n46) );
  inv_1 U793 ( .ip(n44), .op(n47) );
  inv_1 U794 ( .ip(n45), .op(n48) );
  inv_1 U795 ( .ip(n45), .op(n49) );
  inv_1 U796 ( .ip(n39), .op(n50) );
  inv_1 U797 ( .ip(n44), .op(n51) );
  buf_1 U798 ( .ip(n1432), .op(n57) );
  buf_1 U799 ( .ip(n1488), .op(n62) );
  buf_1 U800 ( .ip(n1516), .op(n65) );
  buf_1 U801 ( .ip(n1549), .op(n66) );
  buf_1 U802 ( .ip(n1549), .op(n67) );
  buf_1 U803 ( .ip(n1549), .op(n68) );
  buf_1 U804 ( .ip(n1549), .op(n69) );
  buf_1 U805 ( .ip(n1549), .op(n70) );
  buf_1 U806 ( .ip(n1549), .op(n71) );
  buf_1 U807 ( .ip(n1549), .op(n72) );
  buf_1 U808 ( .ip(n1549), .op(n73) );
  buf_1 U809 ( .ip(n1549), .op(n74) );
  buf_1 U810 ( .ip(n1549), .op(n75) );
  buf_1 U811 ( .ip(n1549), .op(n76) );
  buf_1 U812 ( .ip(n1674), .op(n83) );
  buf_1 U813 ( .ip(n1675), .op(n86) );
  buf_1 U814 ( .ip(n1676), .op(n89) );
  buf_1 U815 ( .ip(n1681), .op(n92) );
  buf_1 U816 ( .ip(n1777), .op(n95) );
  nand2_2 U817 ( .ip1(n102), .ip2(dma_in_cnt[1]), .op(n103) );
  nand3_2 U818 ( .ip1(n1831), .ip2(n1830), .ip3(n103), .op(n1289) );
  nand2_2 U819 ( .ip1(dma_in_cnt[0]), .ip2(n101), .op(n104) );
  nand3_2 U821 ( .ip1(n1829), .ip2(n1828), .ip3(n104), .op(n1288) );
  nand2_2 U822 ( .ip1(n102), .ip2(dma_in_cnt[3]), .op(n105) );
  nand3_2 U823 ( .ip1(n1835), .ip2(n1834), .ip3(n105), .op(n1291) );
  nand2_2 U824 ( .ip1(n102), .ip2(dma_in_cnt[5]), .op(n106) );
  nand3_2 U825 ( .ip1(n1839), .ip2(n1838), .ip3(n106), .op(n1293) );
  nand2_2 U826 ( .ip1(n102), .ip2(dma_in_cnt[7]), .op(n107) );
  nand3_2 U827 ( .ip1(n1843), .ip2(n1842), .ip3(n107), .op(n1295) );
  nand2_2 U828 ( .ip1(n102), .ip2(dma_in_cnt[9]), .op(n108) );
  nand3_2 U829 ( .ip1(n1847), .ip2(n1846), .ip3(n108), .op(n1297) );
  nand2_2 U830 ( .ip1(n102), .ip2(dma_in_cnt[11]), .op(n109) );
  nand3_2 U831 ( .ip1(n1827), .ip2(n1826), .ip3(n109), .op(n1287) );
  nand2_2 U832 ( .ip1(n102), .ip2(dma_in_cnt[10]), .op(n110) );
  nand3_2 U833 ( .ip1(n1849), .ip2(n1848), .ip3(n110), .op(n1298) );
  nand2_2 U834 ( .ip1(n102), .ip2(dma_in_cnt[8]), .op(n111) );
  nand3_2 U835 ( .ip1(n1845), .ip2(n1844), .ip3(n111), .op(n1296) );
  nand2_2 U836 ( .ip1(n102), .ip2(dma_in_cnt[6]), .op(n112) );
  nand3_2 U837 ( .ip1(n1841), .ip2(n1840), .ip3(n112), .op(n1294) );
  nand2_2 U838 ( .ip1(n102), .ip2(dma_in_cnt[4]), .op(n113) );
  nand3_2 U839 ( .ip1(n1837), .ip2(n1836), .ip3(n113), .op(n1292) );
  nand2_2 U840 ( .ip1(n102), .ip2(dma_in_cnt[2]), .op(n114) );
  nand3_2 U841 ( .ip1(n1833), .ip2(n1832), .ip3(n114), .op(n1290) );
  ab_or_c_or_d U842 ( .ip1(n76), .ip2(buf0_orig[27]), .ip3(n1889), .ip4(n1521), 
        .op(n1183) );
  inv_2 U843 ( .ip(buf0_orig[27]), .op(n992) );
  ab_or_c_or_d U844 ( .ip1(n75), .ip2(buf0_orig[25]), .ip3(n1889), .ip4(n1523), 
        .op(n1185) );
  inv_2 U845 ( .ip(buf0_orig[25]), .op(n991) );
  ab_or_c_or_d U846 ( .ip1(n75), .ip2(buf0_orig[23]), .ip3(n1889), .ip4(n1525), 
        .op(n1187) );
  inv_2 U847 ( .ip(buf0_orig[23]), .op(n990) );
  ab_or_c_or_d U848 ( .ip1(n75), .ip2(buf0_orig[19]), .ip3(n1889), .ip4(n1529), 
        .op(n1191) );
  inv_2 U849 ( .ip(buf0_orig[19]), .op(n989) );
  inv_2 U850 ( .ip(dma_in_cnt[1]), .op(n997) );
  ab_or_c_or_d U851 ( .ip1(n75), .ip2(buf0_orig[20]), .ip3(n1889), .ip4(n1528), 
        .op(n1190) );
  ab_or_c_or_d U852 ( .ip1(n74), .ip2(buf0_orig[22]), .ip3(n1889), .ip4(n1526), 
        .op(n1188) );
  inv_2 U853 ( .ip(dma_in_cnt[3]), .op(n996) );
  ab_or_c_or_d U854 ( .ip1(n74), .ip2(buf0_orig[24]), .ip3(n1889), .ip4(n1524), 
        .op(n1186) );
  inv_2 U855 ( .ip(dma_in_cnt[5]), .op(n995) );
  ab_or_c_or_d U856 ( .ip1(n76), .ip2(buf0_orig[26]), .ip3(n1889), .ip4(n1522), 
        .op(n1184) );
  inv_2 U857 ( .ip(dma_in_cnt[7]), .op(n994) );
  ab_or_c_or_d U858 ( .ip1(n74), .ip2(buf0_orig[28]), .ip3(n1889), .ip4(n1520), 
        .op(n1182) );
  inv_2 U910 ( .ip(dma_in_cnt[9]), .op(n993) );
  ab_or_c_or_d U911 ( .ip1(n73), .ip2(buf0_orig[30]), .ip3(n1518), .ip4(n1889), 
        .op(n1180) );
  inv_2 U912 ( .ip(buf0_orig[30]), .op(n985) );
  ab_or_c_or_d U913 ( .ip1(n74), .ip2(buf0_orig[21]), .ip3(n1889), .ip4(n1527), 
        .op(n1189) );
  ab_or_c_or_d U914 ( .ip1(buf0_orig[29]), .ip2(n67), .ip3(n1889), .ip4(n1519), 
        .op(n1181) );
  nand2_2 U915 ( .ip1(n1487), .ip2(csr[15]), .op(n115) );
  nand2_2 U916 ( .ip1(n1489), .ip2(n115), .op(n1166) );
  inv_2 U917 ( .ip(csr[26]), .op(n959) );
  nor2_2 U918 ( .ip1(dma_out_cnt[11]), .ip2(dma_out_cnt[10]), .op(n117) );
  nor2_2 U919 ( .ip1(n1319), .ip2(n1320), .op(n116) );
  nand2_2 U920 ( .ip1(n117), .ip2(n116), .op(n954) );
  and2_2 U921 ( .ip1(n3), .ip2(n954), .op(N272) );
  nor2_2 U936 ( .ip1(buf0_orig[24]), .ip2(buf0_orig[23]), .op(n119) );
  nor2_2 U937 ( .ip1(buf0_orig[21]), .ip2(buf0_orig[22]), .op(n118) );
  nand2_2 U938 ( .ip1(n119), .ip2(n118), .op(n122) );
  inv_2 U939 ( .ip(buf0_orig[28]), .op(n986) );
  nor2_2 U940 ( .ip1(buf0_orig[26]), .ip2(buf0_orig[25]), .op(n120) );
  nand4_2 U941 ( .ip1(n1325), .ip2(n986), .ip3(n992), .ip4(n120), .op(n121) );
  nor2_2 U942 ( .ip1(n122), .ip2(n121), .op(n123) );
  nand2_2 U943 ( .ip1(buf0_orig[20]), .ip2(buf0_orig[19]), .op(n124) );
  nand2_2 U944 ( .ip1(n124), .ip2(n1065), .op(n711) );
  or2_2 U945 ( .ip1(buf0_orig[22]), .ip2(n711), .op(n707) );
  inv_2 U946 ( .ip(n707), .op(n125) );
  nand2_2 U947 ( .ip1(n125), .ip2(n990), .op(n709) );
  inv_2 U948 ( .ip(n709), .op(n167) );
  inv_2 U949 ( .ip(buf0_orig[24]), .op(n988) );
  nand2_2 U950 ( .ip1(n167), .ip2(n988), .op(n221) );
  inv_2 U951 ( .ip(n221), .op(n170) );
  nand2_2 U952 ( .ip1(n170), .ip2(n991), .op(n541) );
  inv_2 U953 ( .ip(n541), .op(n173) );
  inv_2 U954 ( .ip(buf0_orig[26]), .op(n987) );
  nand2_2 U955 ( .ip1(n173), .ip2(n987), .op(n191) );
  inv_2 U956 ( .ip(n191), .op(n176) );
  nand2_2 U957 ( .ip1(n176), .ip2(n992), .op(n197) );
  inv_2 U958 ( .ip(n197), .op(n179) );
  nand2_2 U959 ( .ip1(n179), .ip2(n986), .op(n203) );
  nor2_2 U960 ( .ip1(buf0_orig[29]), .ip2(n203), .op(n182) );
  xor2_2 U961 ( .ip1(buf0_orig[30]), .ip2(n182), .op(N346) );
  inv_2 U962 ( .ip(n203), .op(n185) );
  xor2_2 U963 ( .ip1(buf0_orig[29]), .ip2(n185), .op(N345) );
  nand2_2 U964 ( .ip1(n541), .ip2(buf0_orig[26]), .op(n188) );
  nand2_2 U965 ( .ip1(n191), .ip2(n188), .op(N342) );
  nand2_2 U966 ( .ip1(n191), .ip2(buf0_orig[27]), .op(n194) );
  nand2_2 U967 ( .ip1(n197), .ip2(n194), .op(N343) );
  nand2_2 U968 ( .ip1(n197), .ip2(buf0_orig[28]), .op(n200) );
  nand2_2 U969 ( .ip1(n203), .ip2(n200), .op(N344) );
  nand2_2 U970 ( .ip1(n709), .ip2(buf0_orig[24]), .op(n204) );
  nand2_2 U971 ( .ip1(n221), .ip2(n204), .op(N340) );
  nand2_2 U972 ( .ip1(n221), .ip2(buf0_orig[25]), .op(n540) );
  nand2_2 U973 ( .ip1(n541), .ip2(n540), .op(N341) );
  nand2_2 U974 ( .ip1(buf0_orig[22]), .ip2(n711), .op(n706) );
  nand2_2 U975 ( .ip1(n707), .ip2(n706), .op(N338) );
  nand2_2 U976 ( .ip1(n707), .ip2(buf0_orig[23]), .op(n708) );
  nand2_2 U977 ( .ip1(n709), .ip2(n708), .op(N339) );
  xor2_2 U978 ( .ip1(buf0_orig[20]), .ip2(buf0_orig[19]), .op(N336) );
  nand3_2 U979 ( .ip1(buf0_orig[21]), .ip2(buf0_orig[20]), .ip3(buf0_orig[19]), 
        .op(n710) );
  nand2_2 U980 ( .ip1(n711), .ip2(n710), .op(N337) );
  nand2_2 U981 ( .ip1(buf0_orig_m3[9]), .ip2(n993), .op(n934) );
  inv_2 U982 ( .ip(dma_in_cnt[8]), .op(n712) );
  nand2_2 U983 ( .ip1(buf0_orig_m3[8]), .ip2(n712), .op(n935) );
  inv_2 U984 ( .ip(dma_in_cnt[10]), .op(n713) );
  nand2_2 U985 ( .ip1(buf0_orig_m3[10]), .ip2(n713), .op(n942) );
  inv_2 U986 ( .ip(dma_in_cnt[6]), .op(n725) );
  nand2_2 U987 ( .ip1(buf0_orig_m3[6]), .ip2(n725), .op(n723) );
  nand2_2 U988 ( .ip1(buf0_orig_m3[7]), .ip2(n994), .op(n722) );
  nand3_2 U989 ( .ip1(n942), .ip2(n723), .ip3(n722), .op(n724) );
  inv_2 U990 ( .ip(buf0_orig_m3[3]), .op(n729) );
  inv_2 U991 ( .ip(dma_in_cnt[4]), .op(n730) );
  ab_or_c_or_d U992 ( .ip1(n909), .ip2(n908), .ip3(n732), .ip4(n731), .op(n924) );
  not_ab_or_c_or_d U993 ( .ip1(buf0_orig_m3[2]), .ip2(n1014), .ip3(n912), 
        .ip4(n2), .op(n922) );
  inv_2 U994 ( .ip(dma_in_cnt[0]), .op(n915) );
  nand3_2 U995 ( .ip1(n997), .ip2(n915), .ip3(buf0_orig_m3[0]), .op(n914) );
  inv_2 U996 ( .ip(buf0_orig_m3[1]), .op(n913) );
  nand2_2 U997 ( .ip1(n914), .ip2(n913), .op(n920) );
  nand2_2 U998 ( .ip1(buf0_orig_m3[0]), .ip2(n915), .op(n916) );
  nand2_2 U999 ( .ip1(n916), .ip2(dma_in_cnt[1]), .op(n919) );
  inv_2 U1000 ( .ip(buf0_orig_m3[2]), .op(n917) );
  nand2_2 U1001 ( .ip1(dma_in_cnt[2]), .ip2(n917), .op(n918) );
  nand3_2 U1002 ( .ip1(n920), .ip2(n919), .ip3(n918), .op(n921) );
  inv_2 U1003 ( .ip(buf0_orig_m3[10]), .op(n931) );
  inv_2 U1004 ( .ip(buf0_orig_m3[11]), .op(n928) );
  nand2_2 U1005 ( .ip1(dma_in_cnt[11]), .ip2(n928), .op(n943) );
  inv_2 U1006 ( .ip(n943), .op(n930) );
  nor2_2 U1007 ( .ip1(buf0_orig_m3[9]), .ip2(n993), .op(n929) );
  not_ab_or_c_or_d U1008 ( .ip1(dma_in_cnt[10]), .ip2(n931), .ip3(n930), .ip4(
        n929), .op(n941) );
  inv_2 U1009 ( .ip(buf0_orig_m3[8]), .op(n932) );
  nand2_2 U1010 ( .ip1(dma_in_cnt[8]), .ip2(n932), .op(n939) );
  inv_2 U1011 ( .ip(buf0_orig_m3[7]), .op(n933) );
  nand2_2 U1012 ( .ip1(dma_in_cnt[7]), .ip2(n933), .op(n938) );
  inv_2 U1013 ( .ip(n934), .op(n937) );
  inv_2 U1014 ( .ip(n935), .op(n936) );
  ab_or_c_or_d U1015 ( .ip1(n939), .ip2(n938), .ip3(n937), .ip4(n936), .op(
        n940) );
  inv_2 U1016 ( .ip(n942), .op(n944) );
  nand2_2 U1017 ( .ip1(n944), .ip2(n943), .op(n945) );
  inv_2 U1018 ( .ip(dma_in_cnt[11]), .op(n960) );
  inv_2 U1019 ( .ip(dma_ack_clr1), .op(n949) );
  inv_2 U1020 ( .ip(n1889), .op(n950) );
  and2_2 U1021 ( .ip1(n951), .ip2(n950), .op(n1286) );
  inv_2 U1022 ( .ip(r2), .op(n968) );
  nor2_2 U1023 ( .ip1(r4), .ip2(n968), .op(n952) );
  nor2_2 U1024 ( .ip1(r1), .ip2(n952), .op(n953) );
  nor2_2 U1025 ( .ip1(n1889), .ip2(n953), .op(n1311) );
  inv_2 U1026 ( .ip(n954), .op(n957) );
  inv_2 U1027 ( .ip(dma_out_cnt[1]), .op(n956) );
  inv_2 U1028 ( .ip(dma_out_cnt[0]), .op(n955) );
  nand3_2 U1029 ( .ip1(n957), .ip2(n956), .ip3(n955), .op(n958) );
  nand2_2 U1030 ( .ip1(n3), .ip2(n958), .op(n967) );
  nor2_2 U1031 ( .ip1(csr[27]), .ip2(n959), .op(n963) );
  nand2_2 U1032 ( .ip1(buf0_orig[30]), .ip2(n960), .op(n961) );
  nand2_2 U1033 ( .ip1(n1064), .ip2(n961), .op(n962) );
  nand2_2 U1034 ( .ip1(n963), .ip2(n962), .op(n966) );
  or2_2 U1035 ( .ip1(r2), .ip2(r4), .op(n965) );
  inv_2 U1036 ( .ip(r5), .op(n984) );
  nand2_2 U1037 ( .ip1(csr[15]), .ip2(n984), .op(n964) );
  not_ab_or_c_or_d U1038 ( .ip1(n967), .ip2(n966), .ip3(n965), .ip4(n964), 
        .op(N361) );
  inv_2 U1039 ( .ip(dma_req_out_hold), .op(n969) );
  mux2_1 U1040 ( .ip1(n970), .ip2(n969), .s(n3), .op(n971) );
  ab_or_c_or_d U1041 ( .ip1(int__19), .ip2(n1315), .ip3(n1316), .ip4(n1317), 
        .op(n978) );
  nand2_2 U1042 ( .ip1(int__17), .ip2(int_[1]), .op(n976) );
  nand2_2 U1043 ( .ip1(int__16), .ip2(int_[0]), .op(n975) );
  nand2_2 U1044 ( .ip1(int__21), .ip2(int_[6]), .op(n974) );
  nand3_2 U1045 ( .ip1(n976), .ip2(n975), .ip3(n974), .op(n977) );
  or2_2 U1046 ( .ip1(n978), .ip2(n977), .op(N222) );
  ab_or_c_or_d U1047 ( .ip1(int__27), .ip2(n1315), .ip3(n1313), .ip4(n1314), 
        .op(n983) );
  nand2_2 U1048 ( .ip1(int__25), .ip2(int_[1]), .op(n981) );
  nand2_2 U1049 ( .ip1(int__24), .ip2(int_[0]), .op(n980) );
  nand2_2 U1050 ( .ip1(int__29), .ip2(int_[6]), .op(n979) );
  nand3_2 U1051 ( .ip1(n981), .ip2(n980), .ip3(n979), .op(n982) );
  or2_2 U1052 ( .ip1(n983), .ip2(n982), .op(N221) );
  inv_2 U1053 ( .ip(csr[15]), .op(n1070) );
  nor2_1 U1054 ( .ip1(csr[8]), .ip2(n725), .op(n1009) );
  nor2_1 U1055 ( .ip1(csr[6]), .ip2(n730), .op(n1005) );
  nor2_1 U1056 ( .ip1(csr[4]), .ip2(n1014), .op(n1001) );
  not_ab_or_c_or_d U1057 ( .ip1(dma_in_cnt[1]), .ip2(n1039), .ip3(n1015), 
        .ip4(dma_in_cnt[0]), .op(n999) );
  nor2_1 U1058 ( .ip1(dma_in_cnt[1]), .ip2(n1039), .op(n998) );
  not_ab_or_c_or_d U1059 ( .ip1(csr[4]), .ip2(n1014), .ip3(n999), .ip4(n998), 
        .op(n1000) );
  not_ab_or_c_or_d U1060 ( .ip1(dma_in_cnt[3]), .ip2(n1016), .ip3(n1001), 
        .ip4(n1000), .op(n1003) );
  nor2_1 U1061 ( .ip1(dma_in_cnt[3]), .ip2(n1016), .op(n1002) );
  not_ab_or_c_or_d U1062 ( .ip1(csr[6]), .ip2(n730), .ip3(n1003), .ip4(n1002), 
        .op(n1004) );
  not_ab_or_c_or_d U1063 ( .ip1(dma_in_cnt[5]), .ip2(n1017), .ip3(n1005), 
        .ip4(n1004), .op(n1007) );
  nor2_1 U1064 ( .ip1(dma_in_cnt[5]), .ip2(n1017), .op(n1006) );
  not_ab_or_c_or_d U1065 ( .ip1(csr[8]), .ip2(n725), .ip3(n1007), .ip4(n1006), 
        .op(n1008) );
  not_ab_or_c_or_d U1066 ( .ip1(dma_in_cnt[7]), .ip2(n1018), .ip3(n1009), 
        .ip4(n1008), .op(n1011) );
  nor2_1 U1067 ( .ip1(dma_in_cnt[7]), .ip2(n1018), .op(n1010) );
  not_ab_or_c_or_d U1068 ( .ip1(csr[10]), .ip2(n712), .ip3(n1011), .ip4(n1010), 
        .op(n1013) );
  or3_1 U1069 ( .ip1(dma_in_cnt[9]), .ip2(dma_in_cnt[11]), .ip3(dma_in_cnt[10]), .op(n1012) );
  ab_or_c_or_d U1070 ( .ip1(dma_in_cnt[8]), .ip2(n1068), .ip3(n1013), .ip4(
        n1012), .op(N319) );
  inv_2 U1071 ( .ip(dma_in_cnt[2]), .op(n1014) );
  inv_2 U1072 ( .ip(csr[2]), .op(n1015) );
  inv_2 U1073 ( .ip(csr[5]), .op(n1016) );
  inv_2 U1074 ( .ip(csr[7]), .op(n1017) );
  inv_2 U1075 ( .ip(csr[9]), .op(n1018) );
  nor2_1 U1076 ( .ip1(csr[8]), .ip2(n1036), .op(n1030) );
  nor2_1 U1077 ( .ip1(csr[6]), .ip2(n1037), .op(n1026) );
  nor2_1 U1078 ( .ip1(csr[4]), .ip2(n1038), .op(n1022) );
  not_ab_or_c_or_d U1079 ( .ip1(dma_out_left[1]), .ip2(n1039), .ip3(n1015), 
        .ip4(dma_out_left[0]), .op(n1020) );
  nor2_1 U1080 ( .ip1(dma_out_left[1]), .ip2(n1039), .op(n1019) );
  not_ab_or_c_or_d U1081 ( .ip1(csr[4]), .ip2(n1038), .ip3(n1020), .ip4(n1019), 
        .op(n1021) );
  not_ab_or_c_or_d U1082 ( .ip1(dma_out_left[3]), .ip2(n1016), .ip3(n1022), 
        .ip4(n1021), .op(n1024) );
  nor2_1 U1083 ( .ip1(dma_out_left[3]), .ip2(n1016), .op(n1023) );
  not_ab_or_c_or_d U1084 ( .ip1(csr[6]), .ip2(n1037), .ip3(n1024), .ip4(n1023), 
        .op(n1025) );
  not_ab_or_c_or_d U1085 ( .ip1(dma_out_left[5]), .ip2(n1017), .ip3(n1026), 
        .ip4(n1025), .op(n1028) );
  nor2_1 U1086 ( .ip1(dma_out_left[5]), .ip2(n1017), .op(n1027) );
  not_ab_or_c_or_d U1087 ( .ip1(csr[8]), .ip2(n1036), .ip3(n1028), .ip4(n1027), 
        .op(n1029) );
  not_ab_or_c_or_d U1088 ( .ip1(dma_out_left[7]), .ip2(n1018), .ip3(n1030), 
        .ip4(n1029), .op(n1032) );
  nor2_1 U1089 ( .ip1(dma_out_left[7]), .ip2(n1018), .op(n1031) );
  not_ab_or_c_or_d U1090 ( .ip1(csr[10]), .ip2(n1035), .ip3(n1032), .ip4(n1031), .op(n1034) );
  or3_1 U1091 ( .ip1(dma_out_left[9]), .ip2(dma_out_left[11]), .ip3(
        dma_out_left[10]), .op(n1033) );
  ab_or_c_or_d U1092 ( .ip1(dma_out_left[8]), .ip2(n1068), .ip3(n1034), .ip4(
        n1033), .op(N333) );
  inv_2 U1093 ( .ip(dma_out_left[8]), .op(n1035) );
  inv_2 U1094 ( .ip(dma_out_left[6]), .op(n1036) );
  inv_2 U1095 ( .ip(dma_out_left[4]), .op(n1037) );
  inv_2 U1096 ( .ip(dma_out_left[2]), .op(n1038) );
  inv_2 U1097 ( .ip(csr[3]), .op(n1039) );
  nor2_1 U1098 ( .ip1(dma_in_cnt[10]), .ip2(n1066), .op(n1061) );
  nor2_1 U1099 ( .ip1(dma_in_cnt[8]), .ip2(n992), .op(n1057) );
  nor2_1 U1100 ( .ip1(dma_in_cnt[6]), .ip2(n991), .op(n1053) );
  nor2_1 U1101 ( .ip1(dma_in_cnt[4]), .ip2(n990), .op(n1049) );
  nor2_1 U1102 ( .ip1(dma_in_cnt[2]), .ip2(n1065), .op(n1045) );
  nor2_1 U1103 ( .ip1(n989), .ip2(dma_in_cnt[0]), .op(n1041) );
  and2_1 U1104 ( .ip1(n997), .ip2(n1041), .op(n1040) );
  nor2_1 U1105 ( .ip1(buf0_orig[20]), .ip2(n1040), .op(n1043) );
  nor2_1 U1106 ( .ip1(n1041), .ip2(n997), .op(n1042) );
  not_ab_or_c_or_d U1107 ( .ip1(dma_in_cnt[2]), .ip2(n1065), .ip3(n1043), 
        .ip4(n1042), .op(n1044) );
  not_ab_or_c_or_d U1108 ( .ip1(buf0_orig[22]), .ip2(n996), .ip3(n1045), .ip4(
        n1044), .op(n1047) );
  nor2_1 U1109 ( .ip1(buf0_orig[22]), .ip2(n996), .op(n1046) );
  not_ab_or_c_or_d U1110 ( .ip1(dma_in_cnt[4]), .ip2(n990), .ip3(n1047), .ip4(
        n1046), .op(n1048) );
  not_ab_or_c_or_d U1111 ( .ip1(buf0_orig[24]), .ip2(n995), .ip3(n1049), .ip4(
        n1048), .op(n1051) );
  nor2_1 U1112 ( .ip1(buf0_orig[24]), .ip2(n995), .op(n1050) );
  not_ab_or_c_or_d U1113 ( .ip1(dma_in_cnt[6]), .ip2(n991), .ip3(n1051), .ip4(
        n1050), .op(n1052) );
  not_ab_or_c_or_d U1114 ( .ip1(buf0_orig[26]), .ip2(n994), .ip3(n1053), .ip4(
        n1052), .op(n1055) );
  nor2_1 U1115 ( .ip1(buf0_orig[26]), .ip2(n994), .op(n1054) );
  not_ab_or_c_or_d U1116 ( .ip1(dma_in_cnt[8]), .ip2(n992), .ip3(n1055), .ip4(
        n1054), .op(n1056) );
  not_ab_or_c_or_d U1117 ( .ip1(buf0_orig[28]), .ip2(n993), .ip3(n1057), .ip4(
        n1056), .op(n1059) );
  nor2_1 U1118 ( .ip1(buf0_orig[28]), .ip2(n993), .op(n1058) );
  not_ab_or_c_or_d U1119 ( .ip1(dma_in_cnt[10]), .ip2(n1066), .ip3(n1059), 
        .ip4(n1058), .op(n1060) );
  or2_1 U1120 ( .ip1(n1061), .ip2(n1060), .op(n1063) );
  nand2_1 U1121 ( .ip1(dma_in_cnt[11]), .ip2(n985), .op(n1062) );
  nand2_1 U1122 ( .ip1(n1063), .ip2(n1062), .op(n1064) );
  inv_2 U1123 ( .ip(buf0_orig[21]), .op(n1065) );
  inv_2 U1124 ( .ip(buf0_orig[29]), .op(n1066) );
  inv_2 U1125 ( .ip(n1472), .op(n1067) );
  inv_2 U1126 ( .ip(csr[10]), .op(n1068) );
  inv_2 U1127 ( .ip(csr[0]), .op(n1069) );
  inv_2 U1128 ( .ip(buf0_orig[31]), .op(n1071) );
  inv_2 U1129 ( .ip(buf0_orig[22]), .op(n1072) );
  inv_2 U1130 ( .ip(buf0_orig[20]), .op(n1073) );
  inv_2 U1131 ( .ip(buf0_orig[18]), .op(n1074) );
  inv_2 U1132 ( .ip(buf0_orig[17]), .op(n1075) );
  inv_2 U1133 ( .ip(buf0_orig[16]), .op(n1076) );
  inv_2 U1134 ( .ip(buf0_orig[15]), .op(n1077) );
  inv_2 U1135 ( .ip(buf0_orig[14]), .op(n1078) );
  inv_2 U1136 ( .ip(buf0_orig[13]), .op(n1079) );
  inv_2 U1137 ( .ip(buf0_orig[12]), .op(n1080) );
  inv_2 U1138 ( .ip(buf0_orig[11]), .op(n1081) );
  inv_2 U1139 ( .ip(buf0_orig[10]), .op(n1082) );
  inv_2 U1140 ( .ip(buf0_orig[9]), .op(n1083) );
  inv_2 U1141 ( .ip(buf0_orig[8]), .op(n1084) );
  inv_2 U1142 ( .ip(buf0_orig[7]), .op(n1085) );
  inv_2 U1143 ( .ip(buf0_orig[6]), .op(n1086) );
  inv_2 U1144 ( .ip(buf0_orig[5]), .op(n1087) );
  inv_2 U1145 ( .ip(buf0_orig[4]), .op(n1088) );
  inv_2 U1146 ( .ip(buf0_orig[3]), .op(n1089) );
  inv_2 U1147 ( .ip(buf0_orig[2]), .op(n1090) );
  inv_2 U1148 ( .ip(buf0_orig[1]), .op(n1091) );
  inv_2 U1149 ( .ip(buf0_orig[0]), .op(n1092) );
  inv_2 U1150 ( .ip(n1783), .op(n1093) );
  inv_2 U1151 ( .ip(n1787), .op(n1094) );
  inv_2 U1152 ( .ip(n1799), .op(n1095) );
  inv_2 U1153 ( .ip(n1803), .op(n1096) );
  inv_2 U1154 ( .ip(n1808), .op(n1097) );
  inv_2 U1155 ( .ip(ep_match_r), .op(n1098) );
  inv_2 U1156 ( .ip(n1814), .op(n1099) );
  inv_2 U1157 ( .ip(n1820), .op(n1100) );
  inv_2 U1158 ( .ip(int_[4]), .op(n1101) );
  inv_2 U1159 ( .ip(int_[3]), .op(n1102) );
  inv_2 U1160 ( .ip(buf0_rl), .op(n1103) );
  inv_2 U1161 ( .ip(n1824), .op(n1104) );
  inv_2 U1162 ( .ip(idin[30]), .op(n1105) );
  inv_2 U1163 ( .ip(idin[29]), .op(n1106) );
  inv_2 U1164 ( .ip(idin[28]), .op(n1107) );
  inv_2 U1165 ( .ip(idin[3]), .op(n1108) );
  inv_2 U1166 ( .ip(idin[2]), .op(n1109) );
  inv_2 U1167 ( .ip(idin[16]), .op(n1110) );
  inv_2 U1168 ( .ip(idin[15]), .op(n1111) );
  inv_2 U1169 ( .ip(idin[14]), .op(n1112) );
  inv_2 U1170 ( .ip(idin[13]), .op(n1113) );
  inv_2 U1171 ( .ip(idin[12]), .op(n1114) );
  inv_2 U1172 ( .ip(idin[11]), .op(n1115) );
  inv_2 U1173 ( .ip(idin[10]), .op(n1116) );
  inv_2 U1174 ( .ip(idin[9]), .op(n1117) );
  inv_2 U1175 ( .ip(idin[8]), .op(n1118) );
  inv_2 U1176 ( .ip(idin[7]), .op(n1119) );
  inv_2 U1177 ( .ip(idin[6]), .op(n1120) );
  inv_2 U1178 ( .ip(idin[5]), .op(n1121) );
  inv_2 U1179 ( .ip(idin[4]), .op(n1122) );
  inv_2 U1180 ( .ip(idin[31]), .op(n1123) );
  inv_2 U1181 ( .ip(idin[1]), .op(n1124) );
  inv_2 U1182 ( .ip(idin[0]), .op(n1125) );
  inv_2 U1183 ( .ip(idin[27]), .op(n1126) );
  inv_2 U1184 ( .ip(idin[26]), .op(n1127) );
  inv_2 U1185 ( .ip(idin[25]), .op(n1128) );
  inv_2 U1186 ( .ip(idin[24]), .op(n1129) );
  inv_2 U1187 ( .ip(idin[23]), .op(n1130) );
  inv_2 U1188 ( .ip(idin[22]), .op(n1131) );
  inv_2 U1189 ( .ip(idin[21]), .op(n1132) );
  inv_2 U1190 ( .ip(idin[20]), .op(n1133) );
  inv_2 U1191 ( .ip(idin[19]), .op(n1134) );
  inv_2 U1192 ( .ip(idin[18]), .op(n1135) );
  inv_2 U1193 ( .ip(idin[17]), .op(n1136) );
  inv_2 U1194 ( .ip(n1682), .op(n1137) );
  inv_2 U1195 ( .ip(adr[1]), .op(n1138) );
  inv_2 U1196 ( .ip(adr[0]), .op(n1139) );
endmodule


module usbf_ep_rf_dummy_0 ( clk, wclk, rst, adr, re, we, din, dout, inta, intb, 
        dma_req, dma_ack, idin, ep_sel, ep_match, buf0_rl, buf0_set, buf1_set, 
        uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, 
        int_crc16_set, int_to_set, int_seqerr_set, out_to_small, csr, buf0, 
        buf1, dma_in_buf_sz1, dma_out_buf_avail );
  input [1:0] adr;
  input [31:0] din;
  output [31:0] dout;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  input clk, wclk, rst, re, we, dma_ack, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small;
  output inta, intb, dma_req, ep_match, dma_in_buf_sz1, dma_out_buf_avail;

  assign buf1[0] = 1'b1;
  assign buf1[1] = 1'b1;
  assign buf1[2] = 1'b1;
  assign buf1[3] = 1'b1;
  assign buf1[4] = 1'b1;
  assign buf1[5] = 1'b1;
  assign buf1[6] = 1'b1;
  assign buf1[7] = 1'b1;
  assign buf1[8] = 1'b1;
  assign buf1[9] = 1'b1;
  assign buf1[10] = 1'b1;
  assign buf1[11] = 1'b1;
  assign buf1[12] = 1'b1;
  assign buf1[13] = 1'b1;
  assign buf1[14] = 1'b1;
  assign buf1[15] = 1'b1;
  assign buf1[16] = 1'b1;
  assign buf1[17] = 1'b1;
  assign buf1[18] = 1'b1;
  assign buf1[19] = 1'b1;
  assign buf1[20] = 1'b1;
  assign buf1[21] = 1'b1;
  assign buf1[22] = 1'b1;
  assign buf1[23] = 1'b1;
  assign buf1[24] = 1'b1;
  assign buf1[25] = 1'b1;
  assign buf1[26] = 1'b1;
  assign buf1[27] = 1'b1;
  assign buf1[28] = 1'b1;
  assign buf1[29] = 1'b1;
  assign buf1[30] = 1'b1;
  assign buf1[31] = 1'b1;
  assign buf0[0] = 1'b1;
  assign buf0[1] = 1'b1;
  assign buf0[2] = 1'b1;
  assign buf0[3] = 1'b1;
  assign buf0[4] = 1'b1;
  assign buf0[5] = 1'b1;
  assign buf0[6] = 1'b1;
  assign buf0[7] = 1'b1;
  assign buf0[8] = 1'b1;
  assign buf0[9] = 1'b1;
  assign buf0[10] = 1'b1;
  assign buf0[11] = 1'b1;
  assign buf0[12] = 1'b1;
  assign buf0[13] = 1'b1;
  assign buf0[14] = 1'b1;
  assign buf0[15] = 1'b1;
  assign buf0[16] = 1'b1;
  assign buf0[17] = 1'b1;
  assign buf0[18] = 1'b1;
  assign buf0[19] = 1'b1;
  assign buf0[20] = 1'b1;
  assign buf0[21] = 1'b1;
  assign buf0[22] = 1'b1;
  assign buf0[23] = 1'b1;
  assign buf0[24] = 1'b1;
  assign buf0[25] = 1'b1;
  assign buf0[26] = 1'b1;
  assign buf0[27] = 1'b1;
  assign buf0[28] = 1'b1;
  assign buf0[29] = 1'b1;
  assign buf0[30] = 1'b1;
  assign buf0[31] = 1'b1;
  assign dma_out_buf_avail = 1'b0;
  assign dma_in_buf_sz1 = 1'b0;
  assign csr[0] = 1'b0;
  assign csr[1] = 1'b0;
  assign csr[2] = 1'b0;
  assign csr[3] = 1'b0;
  assign csr[4] = 1'b0;
  assign csr[5] = 1'b0;
  assign csr[6] = 1'b0;
  assign csr[7] = 1'b0;
  assign csr[8] = 1'b0;
  assign csr[9] = 1'b0;
  assign csr[10] = 1'b0;
  assign csr[11] = 1'b0;
  assign csr[12] = 1'b0;
  assign csr[13] = 1'b0;
  assign csr[14] = 1'b0;
  assign csr[15] = 1'b0;
  assign csr[16] = 1'b0;
  assign csr[17] = 1'b0;
  assign csr[18] = 1'b0;
  assign csr[19] = 1'b0;
  assign csr[20] = 1'b0;
  assign csr[21] = 1'b0;
  assign csr[22] = 1'b0;
  assign csr[23] = 1'b0;
  assign csr[24] = 1'b0;
  assign csr[25] = 1'b0;
  assign csr[26] = 1'b0;
  assign csr[27] = 1'b0;
  assign csr[28] = 1'b0;
  assign csr[29] = 1'b0;
  assign csr[30] = 1'b0;
  assign csr[31] = 1'b0;
  assign ep_match = 1'b0;
  assign dma_req = 1'b0;
  assign intb = 1'b0;
  assign inta = 1'b0;
  assign dout[0] = 1'b0;
  assign dout[1] = 1'b0;
  assign dout[2] = 1'b0;
  assign dout[3] = 1'b0;
  assign dout[4] = 1'b0;
  assign dout[5] = 1'b0;
  assign dout[6] = 1'b0;
  assign dout[7] = 1'b0;
  assign dout[8] = 1'b0;
  assign dout[9] = 1'b0;
  assign dout[10] = 1'b0;
  assign dout[11] = 1'b0;
  assign dout[12] = 1'b0;
  assign dout[13] = 1'b0;
  assign dout[14] = 1'b0;
  assign dout[15] = 1'b0;
  assign dout[16] = 1'b0;
  assign dout[17] = 1'b0;
  assign dout[18] = 1'b0;
  assign dout[19] = 1'b0;
  assign dout[20] = 1'b0;
  assign dout[21] = 1'b0;
  assign dout[22] = 1'b0;
  assign dout[23] = 1'b0;
  assign dout[24] = 1'b0;
  assign dout[25] = 1'b0;
  assign dout[26] = 1'b0;
  assign dout[27] = 1'b0;
  assign dout[28] = 1'b0;
  assign dout[29] = 1'b0;
  assign dout[30] = 1'b0;
  assign dout[31] = 1'b0;

endmodule


module usbf_ep_rf_dummy_11 ( clk, wclk, rst, adr, re, we, din, dout, inta, 
        intb, dma_req, dma_ack, idin, ep_sel, ep_match, buf0_rl, buf0_set, 
        buf1_set, uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, 
        int_upid_set, int_crc16_set, int_to_set, int_seqerr_set, out_to_small, 
        csr, buf0, buf1, dma_in_buf_sz1, dma_out_buf_avail );
  input [1:0] adr;
  input [31:0] din;
  output [31:0] dout;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  input clk, wclk, rst, re, we, dma_ack, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small;
  output inta, intb, dma_req, ep_match, dma_in_buf_sz1, dma_out_buf_avail;

  assign buf1[0] = 1'b1;
  assign buf1[1] = 1'b1;
  assign buf1[2] = 1'b1;
  assign buf1[3] = 1'b1;
  assign buf1[4] = 1'b1;
  assign buf1[5] = 1'b1;
  assign buf1[6] = 1'b1;
  assign buf1[7] = 1'b1;
  assign buf1[8] = 1'b1;
  assign buf1[9] = 1'b1;
  assign buf1[10] = 1'b1;
  assign buf1[11] = 1'b1;
  assign buf1[12] = 1'b1;
  assign buf1[13] = 1'b1;
  assign buf1[14] = 1'b1;
  assign buf1[15] = 1'b1;
  assign buf1[16] = 1'b1;
  assign buf1[17] = 1'b1;
  assign buf1[18] = 1'b1;
  assign buf1[19] = 1'b1;
  assign buf1[20] = 1'b1;
  assign buf1[21] = 1'b1;
  assign buf1[22] = 1'b1;
  assign buf1[23] = 1'b1;
  assign buf1[24] = 1'b1;
  assign buf1[25] = 1'b1;
  assign buf1[26] = 1'b1;
  assign buf1[27] = 1'b1;
  assign buf1[28] = 1'b1;
  assign buf1[29] = 1'b1;
  assign buf1[30] = 1'b1;
  assign buf1[31] = 1'b1;
  assign buf0[0] = 1'b1;
  assign buf0[1] = 1'b1;
  assign buf0[2] = 1'b1;
  assign buf0[3] = 1'b1;
  assign buf0[4] = 1'b1;
  assign buf0[5] = 1'b1;
  assign buf0[6] = 1'b1;
  assign buf0[7] = 1'b1;
  assign buf0[8] = 1'b1;
  assign buf0[9] = 1'b1;
  assign buf0[10] = 1'b1;
  assign buf0[11] = 1'b1;
  assign buf0[12] = 1'b1;
  assign buf0[13] = 1'b1;
  assign buf0[14] = 1'b1;
  assign buf0[15] = 1'b1;
  assign buf0[16] = 1'b1;
  assign buf0[17] = 1'b1;
  assign buf0[18] = 1'b1;
  assign buf0[19] = 1'b1;
  assign buf0[20] = 1'b1;
  assign buf0[21] = 1'b1;
  assign buf0[22] = 1'b1;
  assign buf0[23] = 1'b1;
  assign buf0[24] = 1'b1;
  assign buf0[25] = 1'b1;
  assign buf0[26] = 1'b1;
  assign buf0[27] = 1'b1;
  assign buf0[28] = 1'b1;
  assign buf0[29] = 1'b1;
  assign buf0[30] = 1'b1;
  assign buf0[31] = 1'b1;
  assign dma_out_buf_avail = 1'b0;
  assign dma_in_buf_sz1 = 1'b0;
  assign csr[0] = 1'b0;
  assign csr[1] = 1'b0;
  assign csr[2] = 1'b0;
  assign csr[3] = 1'b0;
  assign csr[4] = 1'b0;
  assign csr[5] = 1'b0;
  assign csr[6] = 1'b0;
  assign csr[7] = 1'b0;
  assign csr[8] = 1'b0;
  assign csr[9] = 1'b0;
  assign csr[10] = 1'b0;
  assign csr[11] = 1'b0;
  assign csr[12] = 1'b0;
  assign csr[13] = 1'b0;
  assign csr[14] = 1'b0;
  assign csr[15] = 1'b0;
  assign csr[16] = 1'b0;
  assign csr[17] = 1'b0;
  assign csr[18] = 1'b0;
  assign csr[19] = 1'b0;
  assign csr[20] = 1'b0;
  assign csr[21] = 1'b0;
  assign csr[22] = 1'b0;
  assign csr[23] = 1'b0;
  assign csr[24] = 1'b0;
  assign csr[25] = 1'b0;
  assign csr[26] = 1'b0;
  assign csr[27] = 1'b0;
  assign csr[28] = 1'b0;
  assign csr[29] = 1'b0;
  assign csr[30] = 1'b0;
  assign csr[31] = 1'b0;
  assign ep_match = 1'b0;
  assign dma_req = 1'b0;
  assign intb = 1'b0;
  assign inta = 1'b0;
  assign dout[0] = 1'b0;
  assign dout[1] = 1'b0;
  assign dout[2] = 1'b0;
  assign dout[3] = 1'b0;
  assign dout[4] = 1'b0;
  assign dout[5] = 1'b0;
  assign dout[6] = 1'b0;
  assign dout[7] = 1'b0;
  assign dout[8] = 1'b0;
  assign dout[9] = 1'b0;
  assign dout[10] = 1'b0;
  assign dout[11] = 1'b0;
  assign dout[12] = 1'b0;
  assign dout[13] = 1'b0;
  assign dout[14] = 1'b0;
  assign dout[15] = 1'b0;
  assign dout[16] = 1'b0;
  assign dout[17] = 1'b0;
  assign dout[18] = 1'b0;
  assign dout[19] = 1'b0;
  assign dout[20] = 1'b0;
  assign dout[21] = 1'b0;
  assign dout[22] = 1'b0;
  assign dout[23] = 1'b0;
  assign dout[24] = 1'b0;
  assign dout[25] = 1'b0;
  assign dout[26] = 1'b0;
  assign dout[27] = 1'b0;
  assign dout[28] = 1'b0;
  assign dout[29] = 1'b0;
  assign dout[30] = 1'b0;
  assign dout[31] = 1'b0;

endmodule


module usbf_ep_rf_dummy_10 ( clk, wclk, rst, adr, re, we, din, dout, inta, 
        intb, dma_req, dma_ack, idin, ep_sel, ep_match, buf0_rl, buf0_set, 
        buf1_set, uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, 
        int_upid_set, int_crc16_set, int_to_set, int_seqerr_set, out_to_small, 
        csr, buf0, buf1, dma_in_buf_sz1, dma_out_buf_avail );
  input [1:0] adr;
  input [31:0] din;
  output [31:0] dout;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  input clk, wclk, rst, re, we, dma_ack, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small;
  output inta, intb, dma_req, ep_match, dma_in_buf_sz1, dma_out_buf_avail;

  assign buf1[0] = 1'b1;
  assign buf1[1] = 1'b1;
  assign buf1[2] = 1'b1;
  assign buf1[3] = 1'b1;
  assign buf1[4] = 1'b1;
  assign buf1[5] = 1'b1;
  assign buf1[6] = 1'b1;
  assign buf1[7] = 1'b1;
  assign buf1[8] = 1'b1;
  assign buf1[9] = 1'b1;
  assign buf1[10] = 1'b1;
  assign buf1[11] = 1'b1;
  assign buf1[12] = 1'b1;
  assign buf1[13] = 1'b1;
  assign buf1[14] = 1'b1;
  assign buf1[15] = 1'b1;
  assign buf1[16] = 1'b1;
  assign buf1[17] = 1'b1;
  assign buf1[18] = 1'b1;
  assign buf1[19] = 1'b1;
  assign buf1[20] = 1'b1;
  assign buf1[21] = 1'b1;
  assign buf1[22] = 1'b1;
  assign buf1[23] = 1'b1;
  assign buf1[24] = 1'b1;
  assign buf1[25] = 1'b1;
  assign buf1[26] = 1'b1;
  assign buf1[27] = 1'b1;
  assign buf1[28] = 1'b1;
  assign buf1[29] = 1'b1;
  assign buf1[30] = 1'b1;
  assign buf1[31] = 1'b1;
  assign buf0[0] = 1'b1;
  assign buf0[1] = 1'b1;
  assign buf0[2] = 1'b1;
  assign buf0[3] = 1'b1;
  assign buf0[4] = 1'b1;
  assign buf0[5] = 1'b1;
  assign buf0[6] = 1'b1;
  assign buf0[7] = 1'b1;
  assign buf0[8] = 1'b1;
  assign buf0[9] = 1'b1;
  assign buf0[10] = 1'b1;
  assign buf0[11] = 1'b1;
  assign buf0[12] = 1'b1;
  assign buf0[13] = 1'b1;
  assign buf0[14] = 1'b1;
  assign buf0[15] = 1'b1;
  assign buf0[16] = 1'b1;
  assign buf0[17] = 1'b1;
  assign buf0[18] = 1'b1;
  assign buf0[19] = 1'b1;
  assign buf0[20] = 1'b1;
  assign buf0[21] = 1'b1;
  assign buf0[22] = 1'b1;
  assign buf0[23] = 1'b1;
  assign buf0[24] = 1'b1;
  assign buf0[25] = 1'b1;
  assign buf0[26] = 1'b1;
  assign buf0[27] = 1'b1;
  assign buf0[28] = 1'b1;
  assign buf0[29] = 1'b1;
  assign buf0[30] = 1'b1;
  assign buf0[31] = 1'b1;
  assign dma_out_buf_avail = 1'b0;
  assign dma_in_buf_sz1 = 1'b0;
  assign csr[0] = 1'b0;
  assign csr[1] = 1'b0;
  assign csr[2] = 1'b0;
  assign csr[3] = 1'b0;
  assign csr[4] = 1'b0;
  assign csr[5] = 1'b0;
  assign csr[6] = 1'b0;
  assign csr[7] = 1'b0;
  assign csr[8] = 1'b0;
  assign csr[9] = 1'b0;
  assign csr[10] = 1'b0;
  assign csr[11] = 1'b0;
  assign csr[12] = 1'b0;
  assign csr[13] = 1'b0;
  assign csr[14] = 1'b0;
  assign csr[15] = 1'b0;
  assign csr[16] = 1'b0;
  assign csr[17] = 1'b0;
  assign csr[18] = 1'b0;
  assign csr[19] = 1'b0;
  assign csr[20] = 1'b0;
  assign csr[21] = 1'b0;
  assign csr[22] = 1'b0;
  assign csr[23] = 1'b0;
  assign csr[24] = 1'b0;
  assign csr[25] = 1'b0;
  assign csr[26] = 1'b0;
  assign csr[27] = 1'b0;
  assign csr[28] = 1'b0;
  assign csr[29] = 1'b0;
  assign csr[30] = 1'b0;
  assign csr[31] = 1'b0;
  assign ep_match = 1'b0;
  assign dma_req = 1'b0;
  assign intb = 1'b0;
  assign inta = 1'b0;
  assign dout[0] = 1'b0;
  assign dout[1] = 1'b0;
  assign dout[2] = 1'b0;
  assign dout[3] = 1'b0;
  assign dout[4] = 1'b0;
  assign dout[5] = 1'b0;
  assign dout[6] = 1'b0;
  assign dout[7] = 1'b0;
  assign dout[8] = 1'b0;
  assign dout[9] = 1'b0;
  assign dout[10] = 1'b0;
  assign dout[11] = 1'b0;
  assign dout[12] = 1'b0;
  assign dout[13] = 1'b0;
  assign dout[14] = 1'b0;
  assign dout[15] = 1'b0;
  assign dout[16] = 1'b0;
  assign dout[17] = 1'b0;
  assign dout[18] = 1'b0;
  assign dout[19] = 1'b0;
  assign dout[20] = 1'b0;
  assign dout[21] = 1'b0;
  assign dout[22] = 1'b0;
  assign dout[23] = 1'b0;
  assign dout[24] = 1'b0;
  assign dout[25] = 1'b0;
  assign dout[26] = 1'b0;
  assign dout[27] = 1'b0;
  assign dout[28] = 1'b0;
  assign dout[29] = 1'b0;
  assign dout[30] = 1'b0;
  assign dout[31] = 1'b0;

endmodule


module usbf_ep_rf_dummy_9 ( clk, wclk, rst, adr, re, we, din, dout, inta, intb, 
        dma_req, dma_ack, idin, ep_sel, ep_match, buf0_rl, buf0_set, buf1_set, 
        uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, 
        int_crc16_set, int_to_set, int_seqerr_set, out_to_small, csr, buf0, 
        buf1, dma_in_buf_sz1, dma_out_buf_avail );
  input [1:0] adr;
  input [31:0] din;
  output [31:0] dout;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  input clk, wclk, rst, re, we, dma_ack, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small;
  output inta, intb, dma_req, ep_match, dma_in_buf_sz1, dma_out_buf_avail;

  assign buf1[0] = 1'b1;
  assign buf1[1] = 1'b1;
  assign buf1[2] = 1'b1;
  assign buf1[3] = 1'b1;
  assign buf1[4] = 1'b1;
  assign buf1[5] = 1'b1;
  assign buf1[6] = 1'b1;
  assign buf1[7] = 1'b1;
  assign buf1[8] = 1'b1;
  assign buf1[9] = 1'b1;
  assign buf1[10] = 1'b1;
  assign buf1[11] = 1'b1;
  assign buf1[12] = 1'b1;
  assign buf1[13] = 1'b1;
  assign buf1[14] = 1'b1;
  assign buf1[15] = 1'b1;
  assign buf1[16] = 1'b1;
  assign buf1[17] = 1'b1;
  assign buf1[18] = 1'b1;
  assign buf1[19] = 1'b1;
  assign buf1[20] = 1'b1;
  assign buf1[21] = 1'b1;
  assign buf1[22] = 1'b1;
  assign buf1[23] = 1'b1;
  assign buf1[24] = 1'b1;
  assign buf1[25] = 1'b1;
  assign buf1[26] = 1'b1;
  assign buf1[27] = 1'b1;
  assign buf1[28] = 1'b1;
  assign buf1[29] = 1'b1;
  assign buf1[30] = 1'b1;
  assign buf1[31] = 1'b1;
  assign buf0[0] = 1'b1;
  assign buf0[1] = 1'b1;
  assign buf0[2] = 1'b1;
  assign buf0[3] = 1'b1;
  assign buf0[4] = 1'b1;
  assign buf0[5] = 1'b1;
  assign buf0[6] = 1'b1;
  assign buf0[7] = 1'b1;
  assign buf0[8] = 1'b1;
  assign buf0[9] = 1'b1;
  assign buf0[10] = 1'b1;
  assign buf0[11] = 1'b1;
  assign buf0[12] = 1'b1;
  assign buf0[13] = 1'b1;
  assign buf0[14] = 1'b1;
  assign buf0[15] = 1'b1;
  assign buf0[16] = 1'b1;
  assign buf0[17] = 1'b1;
  assign buf0[18] = 1'b1;
  assign buf0[19] = 1'b1;
  assign buf0[20] = 1'b1;
  assign buf0[21] = 1'b1;
  assign buf0[22] = 1'b1;
  assign buf0[23] = 1'b1;
  assign buf0[24] = 1'b1;
  assign buf0[25] = 1'b1;
  assign buf0[26] = 1'b1;
  assign buf0[27] = 1'b1;
  assign buf0[28] = 1'b1;
  assign buf0[29] = 1'b1;
  assign buf0[30] = 1'b1;
  assign buf0[31] = 1'b1;
  assign dma_out_buf_avail = 1'b0;
  assign dma_in_buf_sz1 = 1'b0;
  assign csr[0] = 1'b0;
  assign csr[1] = 1'b0;
  assign csr[2] = 1'b0;
  assign csr[3] = 1'b0;
  assign csr[4] = 1'b0;
  assign csr[5] = 1'b0;
  assign csr[6] = 1'b0;
  assign csr[7] = 1'b0;
  assign csr[8] = 1'b0;
  assign csr[9] = 1'b0;
  assign csr[10] = 1'b0;
  assign csr[11] = 1'b0;
  assign csr[12] = 1'b0;
  assign csr[13] = 1'b0;
  assign csr[14] = 1'b0;
  assign csr[15] = 1'b0;
  assign csr[16] = 1'b0;
  assign csr[17] = 1'b0;
  assign csr[18] = 1'b0;
  assign csr[19] = 1'b0;
  assign csr[20] = 1'b0;
  assign csr[21] = 1'b0;
  assign csr[22] = 1'b0;
  assign csr[23] = 1'b0;
  assign csr[24] = 1'b0;
  assign csr[25] = 1'b0;
  assign csr[26] = 1'b0;
  assign csr[27] = 1'b0;
  assign csr[28] = 1'b0;
  assign csr[29] = 1'b0;
  assign csr[30] = 1'b0;
  assign csr[31] = 1'b0;
  assign ep_match = 1'b0;
  assign dma_req = 1'b0;
  assign intb = 1'b0;
  assign inta = 1'b0;
  assign dout[0] = 1'b0;
  assign dout[1] = 1'b0;
  assign dout[2] = 1'b0;
  assign dout[3] = 1'b0;
  assign dout[4] = 1'b0;
  assign dout[5] = 1'b0;
  assign dout[6] = 1'b0;
  assign dout[7] = 1'b0;
  assign dout[8] = 1'b0;
  assign dout[9] = 1'b0;
  assign dout[10] = 1'b0;
  assign dout[11] = 1'b0;
  assign dout[12] = 1'b0;
  assign dout[13] = 1'b0;
  assign dout[14] = 1'b0;
  assign dout[15] = 1'b0;
  assign dout[16] = 1'b0;
  assign dout[17] = 1'b0;
  assign dout[18] = 1'b0;
  assign dout[19] = 1'b0;
  assign dout[20] = 1'b0;
  assign dout[21] = 1'b0;
  assign dout[22] = 1'b0;
  assign dout[23] = 1'b0;
  assign dout[24] = 1'b0;
  assign dout[25] = 1'b0;
  assign dout[26] = 1'b0;
  assign dout[27] = 1'b0;
  assign dout[28] = 1'b0;
  assign dout[29] = 1'b0;
  assign dout[30] = 1'b0;
  assign dout[31] = 1'b0;

endmodule


module usbf_ep_rf_dummy_8 ( clk, wclk, rst, adr, re, we, din, dout, inta, intb, 
        dma_req, dma_ack, idin, ep_sel, ep_match, buf0_rl, buf0_set, buf1_set, 
        uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, 
        int_crc16_set, int_to_set, int_seqerr_set, out_to_small, csr, buf0, 
        buf1, dma_in_buf_sz1, dma_out_buf_avail );
  input [1:0] adr;
  input [31:0] din;
  output [31:0] dout;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  input clk, wclk, rst, re, we, dma_ack, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small;
  output inta, intb, dma_req, ep_match, dma_in_buf_sz1, dma_out_buf_avail;

  assign buf1[0] = 1'b1;
  assign buf1[1] = 1'b1;
  assign buf1[2] = 1'b1;
  assign buf1[3] = 1'b1;
  assign buf1[4] = 1'b1;
  assign buf1[5] = 1'b1;
  assign buf1[6] = 1'b1;
  assign buf1[7] = 1'b1;
  assign buf1[8] = 1'b1;
  assign buf1[9] = 1'b1;
  assign buf1[10] = 1'b1;
  assign buf1[11] = 1'b1;
  assign buf1[12] = 1'b1;
  assign buf1[13] = 1'b1;
  assign buf1[14] = 1'b1;
  assign buf1[15] = 1'b1;
  assign buf1[16] = 1'b1;
  assign buf1[17] = 1'b1;
  assign buf1[18] = 1'b1;
  assign buf1[19] = 1'b1;
  assign buf1[20] = 1'b1;
  assign buf1[21] = 1'b1;
  assign buf1[22] = 1'b1;
  assign buf1[23] = 1'b1;
  assign buf1[24] = 1'b1;
  assign buf1[25] = 1'b1;
  assign buf1[26] = 1'b1;
  assign buf1[27] = 1'b1;
  assign buf1[28] = 1'b1;
  assign buf1[29] = 1'b1;
  assign buf1[30] = 1'b1;
  assign buf1[31] = 1'b1;
  assign buf0[0] = 1'b1;
  assign buf0[1] = 1'b1;
  assign buf0[2] = 1'b1;
  assign buf0[3] = 1'b1;
  assign buf0[4] = 1'b1;
  assign buf0[5] = 1'b1;
  assign buf0[6] = 1'b1;
  assign buf0[7] = 1'b1;
  assign buf0[8] = 1'b1;
  assign buf0[9] = 1'b1;
  assign buf0[10] = 1'b1;
  assign buf0[11] = 1'b1;
  assign buf0[12] = 1'b1;
  assign buf0[13] = 1'b1;
  assign buf0[14] = 1'b1;
  assign buf0[15] = 1'b1;
  assign buf0[16] = 1'b1;
  assign buf0[17] = 1'b1;
  assign buf0[18] = 1'b1;
  assign buf0[19] = 1'b1;
  assign buf0[20] = 1'b1;
  assign buf0[21] = 1'b1;
  assign buf0[22] = 1'b1;
  assign buf0[23] = 1'b1;
  assign buf0[24] = 1'b1;
  assign buf0[25] = 1'b1;
  assign buf0[26] = 1'b1;
  assign buf0[27] = 1'b1;
  assign buf0[28] = 1'b1;
  assign buf0[29] = 1'b1;
  assign buf0[30] = 1'b1;
  assign buf0[31] = 1'b1;
  assign dma_out_buf_avail = 1'b0;
  assign dma_in_buf_sz1 = 1'b0;
  assign csr[0] = 1'b0;
  assign csr[1] = 1'b0;
  assign csr[2] = 1'b0;
  assign csr[3] = 1'b0;
  assign csr[4] = 1'b0;
  assign csr[5] = 1'b0;
  assign csr[6] = 1'b0;
  assign csr[7] = 1'b0;
  assign csr[8] = 1'b0;
  assign csr[9] = 1'b0;
  assign csr[10] = 1'b0;
  assign csr[11] = 1'b0;
  assign csr[12] = 1'b0;
  assign csr[13] = 1'b0;
  assign csr[14] = 1'b0;
  assign csr[15] = 1'b0;
  assign csr[16] = 1'b0;
  assign csr[17] = 1'b0;
  assign csr[18] = 1'b0;
  assign csr[19] = 1'b0;
  assign csr[20] = 1'b0;
  assign csr[21] = 1'b0;
  assign csr[22] = 1'b0;
  assign csr[23] = 1'b0;
  assign csr[24] = 1'b0;
  assign csr[25] = 1'b0;
  assign csr[26] = 1'b0;
  assign csr[27] = 1'b0;
  assign csr[28] = 1'b0;
  assign csr[29] = 1'b0;
  assign csr[30] = 1'b0;
  assign csr[31] = 1'b0;
  assign ep_match = 1'b0;
  assign dma_req = 1'b0;
  assign intb = 1'b0;
  assign inta = 1'b0;
  assign dout[0] = 1'b0;
  assign dout[1] = 1'b0;
  assign dout[2] = 1'b0;
  assign dout[3] = 1'b0;
  assign dout[4] = 1'b0;
  assign dout[5] = 1'b0;
  assign dout[6] = 1'b0;
  assign dout[7] = 1'b0;
  assign dout[8] = 1'b0;
  assign dout[9] = 1'b0;
  assign dout[10] = 1'b0;
  assign dout[11] = 1'b0;
  assign dout[12] = 1'b0;
  assign dout[13] = 1'b0;
  assign dout[14] = 1'b0;
  assign dout[15] = 1'b0;
  assign dout[16] = 1'b0;
  assign dout[17] = 1'b0;
  assign dout[18] = 1'b0;
  assign dout[19] = 1'b0;
  assign dout[20] = 1'b0;
  assign dout[21] = 1'b0;
  assign dout[22] = 1'b0;
  assign dout[23] = 1'b0;
  assign dout[24] = 1'b0;
  assign dout[25] = 1'b0;
  assign dout[26] = 1'b0;
  assign dout[27] = 1'b0;
  assign dout[28] = 1'b0;
  assign dout[29] = 1'b0;
  assign dout[30] = 1'b0;
  assign dout[31] = 1'b0;

endmodule


module usbf_ep_rf_dummy_7 ( clk, wclk, rst, adr, re, we, din, dout, inta, intb, 
        dma_req, dma_ack, idin, ep_sel, ep_match, buf0_rl, buf0_set, buf1_set, 
        uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, 
        int_crc16_set, int_to_set, int_seqerr_set, out_to_small, csr, buf0, 
        buf1, dma_in_buf_sz1, dma_out_buf_avail );
  input [1:0] adr;
  input [31:0] din;
  output [31:0] dout;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  input clk, wclk, rst, re, we, dma_ack, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small;
  output inta, intb, dma_req, ep_match, dma_in_buf_sz1, dma_out_buf_avail;

  assign buf1[0] = 1'b1;
  assign buf1[1] = 1'b1;
  assign buf1[2] = 1'b1;
  assign buf1[3] = 1'b1;
  assign buf1[4] = 1'b1;
  assign buf1[5] = 1'b1;
  assign buf1[6] = 1'b1;
  assign buf1[7] = 1'b1;
  assign buf1[8] = 1'b1;
  assign buf1[9] = 1'b1;
  assign buf1[10] = 1'b1;
  assign buf1[11] = 1'b1;
  assign buf1[12] = 1'b1;
  assign buf1[13] = 1'b1;
  assign buf1[14] = 1'b1;
  assign buf1[15] = 1'b1;
  assign buf1[16] = 1'b1;
  assign buf1[17] = 1'b1;
  assign buf1[18] = 1'b1;
  assign buf1[19] = 1'b1;
  assign buf1[20] = 1'b1;
  assign buf1[21] = 1'b1;
  assign buf1[22] = 1'b1;
  assign buf1[23] = 1'b1;
  assign buf1[24] = 1'b1;
  assign buf1[25] = 1'b1;
  assign buf1[26] = 1'b1;
  assign buf1[27] = 1'b1;
  assign buf1[28] = 1'b1;
  assign buf1[29] = 1'b1;
  assign buf1[30] = 1'b1;
  assign buf1[31] = 1'b1;
  assign buf0[0] = 1'b1;
  assign buf0[1] = 1'b1;
  assign buf0[2] = 1'b1;
  assign buf0[3] = 1'b1;
  assign buf0[4] = 1'b1;
  assign buf0[5] = 1'b1;
  assign buf0[6] = 1'b1;
  assign buf0[7] = 1'b1;
  assign buf0[8] = 1'b1;
  assign buf0[9] = 1'b1;
  assign buf0[10] = 1'b1;
  assign buf0[11] = 1'b1;
  assign buf0[12] = 1'b1;
  assign buf0[13] = 1'b1;
  assign buf0[14] = 1'b1;
  assign buf0[15] = 1'b1;
  assign buf0[16] = 1'b1;
  assign buf0[17] = 1'b1;
  assign buf0[18] = 1'b1;
  assign buf0[19] = 1'b1;
  assign buf0[20] = 1'b1;
  assign buf0[21] = 1'b1;
  assign buf0[22] = 1'b1;
  assign buf0[23] = 1'b1;
  assign buf0[24] = 1'b1;
  assign buf0[25] = 1'b1;
  assign buf0[26] = 1'b1;
  assign buf0[27] = 1'b1;
  assign buf0[28] = 1'b1;
  assign buf0[29] = 1'b1;
  assign buf0[30] = 1'b1;
  assign buf0[31] = 1'b1;
  assign dma_out_buf_avail = 1'b0;
  assign dma_in_buf_sz1 = 1'b0;
  assign csr[0] = 1'b0;
  assign csr[1] = 1'b0;
  assign csr[2] = 1'b0;
  assign csr[3] = 1'b0;
  assign csr[4] = 1'b0;
  assign csr[5] = 1'b0;
  assign csr[6] = 1'b0;
  assign csr[7] = 1'b0;
  assign csr[8] = 1'b0;
  assign csr[9] = 1'b0;
  assign csr[10] = 1'b0;
  assign csr[11] = 1'b0;
  assign csr[12] = 1'b0;
  assign csr[13] = 1'b0;
  assign csr[14] = 1'b0;
  assign csr[15] = 1'b0;
  assign csr[16] = 1'b0;
  assign csr[17] = 1'b0;
  assign csr[18] = 1'b0;
  assign csr[19] = 1'b0;
  assign csr[20] = 1'b0;
  assign csr[21] = 1'b0;
  assign csr[22] = 1'b0;
  assign csr[23] = 1'b0;
  assign csr[24] = 1'b0;
  assign csr[25] = 1'b0;
  assign csr[26] = 1'b0;
  assign csr[27] = 1'b0;
  assign csr[28] = 1'b0;
  assign csr[29] = 1'b0;
  assign csr[30] = 1'b0;
  assign csr[31] = 1'b0;
  assign ep_match = 1'b0;
  assign dma_req = 1'b0;
  assign intb = 1'b0;
  assign inta = 1'b0;
  assign dout[0] = 1'b0;
  assign dout[1] = 1'b0;
  assign dout[2] = 1'b0;
  assign dout[3] = 1'b0;
  assign dout[4] = 1'b0;
  assign dout[5] = 1'b0;
  assign dout[6] = 1'b0;
  assign dout[7] = 1'b0;
  assign dout[8] = 1'b0;
  assign dout[9] = 1'b0;
  assign dout[10] = 1'b0;
  assign dout[11] = 1'b0;
  assign dout[12] = 1'b0;
  assign dout[13] = 1'b0;
  assign dout[14] = 1'b0;
  assign dout[15] = 1'b0;
  assign dout[16] = 1'b0;
  assign dout[17] = 1'b0;
  assign dout[18] = 1'b0;
  assign dout[19] = 1'b0;
  assign dout[20] = 1'b0;
  assign dout[21] = 1'b0;
  assign dout[22] = 1'b0;
  assign dout[23] = 1'b0;
  assign dout[24] = 1'b0;
  assign dout[25] = 1'b0;
  assign dout[26] = 1'b0;
  assign dout[27] = 1'b0;
  assign dout[28] = 1'b0;
  assign dout[29] = 1'b0;
  assign dout[30] = 1'b0;
  assign dout[31] = 1'b0;

endmodule


module usbf_ep_rf_dummy_6 ( clk, wclk, rst, adr, re, we, din, dout, inta, intb, 
        dma_req, dma_ack, idin, ep_sel, ep_match, buf0_rl, buf0_set, buf1_set, 
        uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, 
        int_crc16_set, int_to_set, int_seqerr_set, out_to_small, csr, buf0, 
        buf1, dma_in_buf_sz1, dma_out_buf_avail );
  input [1:0] adr;
  input [31:0] din;
  output [31:0] dout;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  input clk, wclk, rst, re, we, dma_ack, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small;
  output inta, intb, dma_req, ep_match, dma_in_buf_sz1, dma_out_buf_avail;

  assign buf1[0] = 1'b1;
  assign buf1[1] = 1'b1;
  assign buf1[2] = 1'b1;
  assign buf1[3] = 1'b1;
  assign buf1[4] = 1'b1;
  assign buf1[5] = 1'b1;
  assign buf1[6] = 1'b1;
  assign buf1[7] = 1'b1;
  assign buf1[8] = 1'b1;
  assign buf1[9] = 1'b1;
  assign buf1[10] = 1'b1;
  assign buf1[11] = 1'b1;
  assign buf1[12] = 1'b1;
  assign buf1[13] = 1'b1;
  assign buf1[14] = 1'b1;
  assign buf1[15] = 1'b1;
  assign buf1[16] = 1'b1;
  assign buf1[17] = 1'b1;
  assign buf1[18] = 1'b1;
  assign buf1[19] = 1'b1;
  assign buf1[20] = 1'b1;
  assign buf1[21] = 1'b1;
  assign buf1[22] = 1'b1;
  assign buf1[23] = 1'b1;
  assign buf1[24] = 1'b1;
  assign buf1[25] = 1'b1;
  assign buf1[26] = 1'b1;
  assign buf1[27] = 1'b1;
  assign buf1[28] = 1'b1;
  assign buf1[29] = 1'b1;
  assign buf1[30] = 1'b1;
  assign buf1[31] = 1'b1;
  assign buf0[0] = 1'b1;
  assign buf0[1] = 1'b1;
  assign buf0[2] = 1'b1;
  assign buf0[3] = 1'b1;
  assign buf0[4] = 1'b1;
  assign buf0[5] = 1'b1;
  assign buf0[6] = 1'b1;
  assign buf0[7] = 1'b1;
  assign buf0[8] = 1'b1;
  assign buf0[9] = 1'b1;
  assign buf0[10] = 1'b1;
  assign buf0[11] = 1'b1;
  assign buf0[12] = 1'b1;
  assign buf0[13] = 1'b1;
  assign buf0[14] = 1'b1;
  assign buf0[15] = 1'b1;
  assign buf0[16] = 1'b1;
  assign buf0[17] = 1'b1;
  assign buf0[18] = 1'b1;
  assign buf0[19] = 1'b1;
  assign buf0[20] = 1'b1;
  assign buf0[21] = 1'b1;
  assign buf0[22] = 1'b1;
  assign buf0[23] = 1'b1;
  assign buf0[24] = 1'b1;
  assign buf0[25] = 1'b1;
  assign buf0[26] = 1'b1;
  assign buf0[27] = 1'b1;
  assign buf0[28] = 1'b1;
  assign buf0[29] = 1'b1;
  assign buf0[30] = 1'b1;
  assign buf0[31] = 1'b1;
  assign dma_out_buf_avail = 1'b0;
  assign dma_in_buf_sz1 = 1'b0;
  assign csr[0] = 1'b0;
  assign csr[1] = 1'b0;
  assign csr[2] = 1'b0;
  assign csr[3] = 1'b0;
  assign csr[4] = 1'b0;
  assign csr[5] = 1'b0;
  assign csr[6] = 1'b0;
  assign csr[7] = 1'b0;
  assign csr[8] = 1'b0;
  assign csr[9] = 1'b0;
  assign csr[10] = 1'b0;
  assign csr[11] = 1'b0;
  assign csr[12] = 1'b0;
  assign csr[13] = 1'b0;
  assign csr[14] = 1'b0;
  assign csr[15] = 1'b0;
  assign csr[16] = 1'b0;
  assign csr[17] = 1'b0;
  assign csr[18] = 1'b0;
  assign csr[19] = 1'b0;
  assign csr[20] = 1'b0;
  assign csr[21] = 1'b0;
  assign csr[22] = 1'b0;
  assign csr[23] = 1'b0;
  assign csr[24] = 1'b0;
  assign csr[25] = 1'b0;
  assign csr[26] = 1'b0;
  assign csr[27] = 1'b0;
  assign csr[28] = 1'b0;
  assign csr[29] = 1'b0;
  assign csr[30] = 1'b0;
  assign csr[31] = 1'b0;
  assign ep_match = 1'b0;
  assign dma_req = 1'b0;
  assign intb = 1'b0;
  assign inta = 1'b0;
  assign dout[0] = 1'b0;
  assign dout[1] = 1'b0;
  assign dout[2] = 1'b0;
  assign dout[3] = 1'b0;
  assign dout[4] = 1'b0;
  assign dout[5] = 1'b0;
  assign dout[6] = 1'b0;
  assign dout[7] = 1'b0;
  assign dout[8] = 1'b0;
  assign dout[9] = 1'b0;
  assign dout[10] = 1'b0;
  assign dout[11] = 1'b0;
  assign dout[12] = 1'b0;
  assign dout[13] = 1'b0;
  assign dout[14] = 1'b0;
  assign dout[15] = 1'b0;
  assign dout[16] = 1'b0;
  assign dout[17] = 1'b0;
  assign dout[18] = 1'b0;
  assign dout[19] = 1'b0;
  assign dout[20] = 1'b0;
  assign dout[21] = 1'b0;
  assign dout[22] = 1'b0;
  assign dout[23] = 1'b0;
  assign dout[24] = 1'b0;
  assign dout[25] = 1'b0;
  assign dout[26] = 1'b0;
  assign dout[27] = 1'b0;
  assign dout[28] = 1'b0;
  assign dout[29] = 1'b0;
  assign dout[30] = 1'b0;
  assign dout[31] = 1'b0;

endmodule


module usbf_ep_rf_dummy_5 ( clk, wclk, rst, adr, re, we, din, dout, inta, intb, 
        dma_req, dma_ack, idin, ep_sel, ep_match, buf0_rl, buf0_set, buf1_set, 
        uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, 
        int_crc16_set, int_to_set, int_seqerr_set, out_to_small, csr, buf0, 
        buf1, dma_in_buf_sz1, dma_out_buf_avail );
  input [1:0] adr;
  input [31:0] din;
  output [31:0] dout;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  input clk, wclk, rst, re, we, dma_ack, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small;
  output inta, intb, dma_req, ep_match, dma_in_buf_sz1, dma_out_buf_avail;

  assign buf1[0] = 1'b1;
  assign buf1[1] = 1'b1;
  assign buf1[2] = 1'b1;
  assign buf1[3] = 1'b1;
  assign buf1[4] = 1'b1;
  assign buf1[5] = 1'b1;
  assign buf1[6] = 1'b1;
  assign buf1[7] = 1'b1;
  assign buf1[8] = 1'b1;
  assign buf1[9] = 1'b1;
  assign buf1[10] = 1'b1;
  assign buf1[11] = 1'b1;
  assign buf1[12] = 1'b1;
  assign buf1[13] = 1'b1;
  assign buf1[14] = 1'b1;
  assign buf1[15] = 1'b1;
  assign buf1[16] = 1'b1;
  assign buf1[17] = 1'b1;
  assign buf1[18] = 1'b1;
  assign buf1[19] = 1'b1;
  assign buf1[20] = 1'b1;
  assign buf1[21] = 1'b1;
  assign buf1[22] = 1'b1;
  assign buf1[23] = 1'b1;
  assign buf1[24] = 1'b1;
  assign buf1[25] = 1'b1;
  assign buf1[26] = 1'b1;
  assign buf1[27] = 1'b1;
  assign buf1[28] = 1'b1;
  assign buf1[29] = 1'b1;
  assign buf1[30] = 1'b1;
  assign buf1[31] = 1'b1;
  assign buf0[0] = 1'b1;
  assign buf0[1] = 1'b1;
  assign buf0[2] = 1'b1;
  assign buf0[3] = 1'b1;
  assign buf0[4] = 1'b1;
  assign buf0[5] = 1'b1;
  assign buf0[6] = 1'b1;
  assign buf0[7] = 1'b1;
  assign buf0[8] = 1'b1;
  assign buf0[9] = 1'b1;
  assign buf0[10] = 1'b1;
  assign buf0[11] = 1'b1;
  assign buf0[12] = 1'b1;
  assign buf0[13] = 1'b1;
  assign buf0[14] = 1'b1;
  assign buf0[15] = 1'b1;
  assign buf0[16] = 1'b1;
  assign buf0[17] = 1'b1;
  assign buf0[18] = 1'b1;
  assign buf0[19] = 1'b1;
  assign buf0[20] = 1'b1;
  assign buf0[21] = 1'b1;
  assign buf0[22] = 1'b1;
  assign buf0[23] = 1'b1;
  assign buf0[24] = 1'b1;
  assign buf0[25] = 1'b1;
  assign buf0[26] = 1'b1;
  assign buf0[27] = 1'b1;
  assign buf0[28] = 1'b1;
  assign buf0[29] = 1'b1;
  assign buf0[30] = 1'b1;
  assign buf0[31] = 1'b1;
  assign dma_out_buf_avail = 1'b0;
  assign dma_in_buf_sz1 = 1'b0;
  assign csr[0] = 1'b0;
  assign csr[1] = 1'b0;
  assign csr[2] = 1'b0;
  assign csr[3] = 1'b0;
  assign csr[4] = 1'b0;
  assign csr[5] = 1'b0;
  assign csr[6] = 1'b0;
  assign csr[7] = 1'b0;
  assign csr[8] = 1'b0;
  assign csr[9] = 1'b0;
  assign csr[10] = 1'b0;
  assign csr[11] = 1'b0;
  assign csr[12] = 1'b0;
  assign csr[13] = 1'b0;
  assign csr[14] = 1'b0;
  assign csr[15] = 1'b0;
  assign csr[16] = 1'b0;
  assign csr[17] = 1'b0;
  assign csr[18] = 1'b0;
  assign csr[19] = 1'b0;
  assign csr[20] = 1'b0;
  assign csr[21] = 1'b0;
  assign csr[22] = 1'b0;
  assign csr[23] = 1'b0;
  assign csr[24] = 1'b0;
  assign csr[25] = 1'b0;
  assign csr[26] = 1'b0;
  assign csr[27] = 1'b0;
  assign csr[28] = 1'b0;
  assign csr[29] = 1'b0;
  assign csr[30] = 1'b0;
  assign csr[31] = 1'b0;
  assign ep_match = 1'b0;
  assign dma_req = 1'b0;
  assign intb = 1'b0;
  assign inta = 1'b0;
  assign dout[0] = 1'b0;
  assign dout[1] = 1'b0;
  assign dout[2] = 1'b0;
  assign dout[3] = 1'b0;
  assign dout[4] = 1'b0;
  assign dout[5] = 1'b0;
  assign dout[6] = 1'b0;
  assign dout[7] = 1'b0;
  assign dout[8] = 1'b0;
  assign dout[9] = 1'b0;
  assign dout[10] = 1'b0;
  assign dout[11] = 1'b0;
  assign dout[12] = 1'b0;
  assign dout[13] = 1'b0;
  assign dout[14] = 1'b0;
  assign dout[15] = 1'b0;
  assign dout[16] = 1'b0;
  assign dout[17] = 1'b0;
  assign dout[18] = 1'b0;
  assign dout[19] = 1'b0;
  assign dout[20] = 1'b0;
  assign dout[21] = 1'b0;
  assign dout[22] = 1'b0;
  assign dout[23] = 1'b0;
  assign dout[24] = 1'b0;
  assign dout[25] = 1'b0;
  assign dout[26] = 1'b0;
  assign dout[27] = 1'b0;
  assign dout[28] = 1'b0;
  assign dout[29] = 1'b0;
  assign dout[30] = 1'b0;
  assign dout[31] = 1'b0;

endmodule


module usbf_ep_rf_dummy_4 ( clk, wclk, rst, adr, re, we, din, dout, inta, intb, 
        dma_req, dma_ack, idin, ep_sel, ep_match, buf0_rl, buf0_set, buf1_set, 
        uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, 
        int_crc16_set, int_to_set, int_seqerr_set, out_to_small, csr, buf0, 
        buf1, dma_in_buf_sz1, dma_out_buf_avail );
  input [1:0] adr;
  input [31:0] din;
  output [31:0] dout;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  input clk, wclk, rst, re, we, dma_ack, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small;
  output inta, intb, dma_req, ep_match, dma_in_buf_sz1, dma_out_buf_avail;

  assign buf1[0] = 1'b1;
  assign buf1[1] = 1'b1;
  assign buf1[2] = 1'b1;
  assign buf1[3] = 1'b1;
  assign buf1[4] = 1'b1;
  assign buf1[5] = 1'b1;
  assign buf1[6] = 1'b1;
  assign buf1[7] = 1'b1;
  assign buf1[8] = 1'b1;
  assign buf1[9] = 1'b1;
  assign buf1[10] = 1'b1;
  assign buf1[11] = 1'b1;
  assign buf1[12] = 1'b1;
  assign buf1[13] = 1'b1;
  assign buf1[14] = 1'b1;
  assign buf1[15] = 1'b1;
  assign buf1[16] = 1'b1;
  assign buf1[17] = 1'b1;
  assign buf1[18] = 1'b1;
  assign buf1[19] = 1'b1;
  assign buf1[20] = 1'b1;
  assign buf1[21] = 1'b1;
  assign buf1[22] = 1'b1;
  assign buf1[23] = 1'b1;
  assign buf1[24] = 1'b1;
  assign buf1[25] = 1'b1;
  assign buf1[26] = 1'b1;
  assign buf1[27] = 1'b1;
  assign buf1[28] = 1'b1;
  assign buf1[29] = 1'b1;
  assign buf1[30] = 1'b1;
  assign buf1[31] = 1'b1;
  assign buf0[0] = 1'b1;
  assign buf0[1] = 1'b1;
  assign buf0[2] = 1'b1;
  assign buf0[3] = 1'b1;
  assign buf0[4] = 1'b1;
  assign buf0[5] = 1'b1;
  assign buf0[6] = 1'b1;
  assign buf0[7] = 1'b1;
  assign buf0[8] = 1'b1;
  assign buf0[9] = 1'b1;
  assign buf0[10] = 1'b1;
  assign buf0[11] = 1'b1;
  assign buf0[12] = 1'b1;
  assign buf0[13] = 1'b1;
  assign buf0[14] = 1'b1;
  assign buf0[15] = 1'b1;
  assign buf0[16] = 1'b1;
  assign buf0[17] = 1'b1;
  assign buf0[18] = 1'b1;
  assign buf0[19] = 1'b1;
  assign buf0[20] = 1'b1;
  assign buf0[21] = 1'b1;
  assign buf0[22] = 1'b1;
  assign buf0[23] = 1'b1;
  assign buf0[24] = 1'b1;
  assign buf0[25] = 1'b1;
  assign buf0[26] = 1'b1;
  assign buf0[27] = 1'b1;
  assign buf0[28] = 1'b1;
  assign buf0[29] = 1'b1;
  assign buf0[30] = 1'b1;
  assign buf0[31] = 1'b1;
  assign dma_out_buf_avail = 1'b0;
  assign dma_in_buf_sz1 = 1'b0;
  assign csr[0] = 1'b0;
  assign csr[1] = 1'b0;
  assign csr[2] = 1'b0;
  assign csr[3] = 1'b0;
  assign csr[4] = 1'b0;
  assign csr[5] = 1'b0;
  assign csr[6] = 1'b0;
  assign csr[7] = 1'b0;
  assign csr[8] = 1'b0;
  assign csr[9] = 1'b0;
  assign csr[10] = 1'b0;
  assign csr[11] = 1'b0;
  assign csr[12] = 1'b0;
  assign csr[13] = 1'b0;
  assign csr[14] = 1'b0;
  assign csr[15] = 1'b0;
  assign csr[16] = 1'b0;
  assign csr[17] = 1'b0;
  assign csr[18] = 1'b0;
  assign csr[19] = 1'b0;
  assign csr[20] = 1'b0;
  assign csr[21] = 1'b0;
  assign csr[22] = 1'b0;
  assign csr[23] = 1'b0;
  assign csr[24] = 1'b0;
  assign csr[25] = 1'b0;
  assign csr[26] = 1'b0;
  assign csr[27] = 1'b0;
  assign csr[28] = 1'b0;
  assign csr[29] = 1'b0;
  assign csr[30] = 1'b0;
  assign csr[31] = 1'b0;
  assign ep_match = 1'b0;
  assign dma_req = 1'b0;
  assign intb = 1'b0;
  assign inta = 1'b0;
  assign dout[0] = 1'b0;
  assign dout[1] = 1'b0;
  assign dout[2] = 1'b0;
  assign dout[3] = 1'b0;
  assign dout[4] = 1'b0;
  assign dout[5] = 1'b0;
  assign dout[6] = 1'b0;
  assign dout[7] = 1'b0;
  assign dout[8] = 1'b0;
  assign dout[9] = 1'b0;
  assign dout[10] = 1'b0;
  assign dout[11] = 1'b0;
  assign dout[12] = 1'b0;
  assign dout[13] = 1'b0;
  assign dout[14] = 1'b0;
  assign dout[15] = 1'b0;
  assign dout[16] = 1'b0;
  assign dout[17] = 1'b0;
  assign dout[18] = 1'b0;
  assign dout[19] = 1'b0;
  assign dout[20] = 1'b0;
  assign dout[21] = 1'b0;
  assign dout[22] = 1'b0;
  assign dout[23] = 1'b0;
  assign dout[24] = 1'b0;
  assign dout[25] = 1'b0;
  assign dout[26] = 1'b0;
  assign dout[27] = 1'b0;
  assign dout[28] = 1'b0;
  assign dout[29] = 1'b0;
  assign dout[30] = 1'b0;
  assign dout[31] = 1'b0;

endmodule


module usbf_ep_rf_dummy_3 ( clk, wclk, rst, adr, re, we, din, dout, inta, intb, 
        dma_req, dma_ack, idin, ep_sel, ep_match, buf0_rl, buf0_set, buf1_set, 
        uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, 
        int_crc16_set, int_to_set, int_seqerr_set, out_to_small, csr, buf0, 
        buf1, dma_in_buf_sz1, dma_out_buf_avail );
  input [1:0] adr;
  input [31:0] din;
  output [31:0] dout;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  input clk, wclk, rst, re, we, dma_ack, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small;
  output inta, intb, dma_req, ep_match, dma_in_buf_sz1, dma_out_buf_avail;

  assign buf1[0] = 1'b1;
  assign buf1[1] = 1'b1;
  assign buf1[2] = 1'b1;
  assign buf1[3] = 1'b1;
  assign buf1[4] = 1'b1;
  assign buf1[5] = 1'b1;
  assign buf1[6] = 1'b1;
  assign buf1[7] = 1'b1;
  assign buf1[8] = 1'b1;
  assign buf1[9] = 1'b1;
  assign buf1[10] = 1'b1;
  assign buf1[11] = 1'b1;
  assign buf1[12] = 1'b1;
  assign buf1[13] = 1'b1;
  assign buf1[14] = 1'b1;
  assign buf1[15] = 1'b1;
  assign buf1[16] = 1'b1;
  assign buf1[17] = 1'b1;
  assign buf1[18] = 1'b1;
  assign buf1[19] = 1'b1;
  assign buf1[20] = 1'b1;
  assign buf1[21] = 1'b1;
  assign buf1[22] = 1'b1;
  assign buf1[23] = 1'b1;
  assign buf1[24] = 1'b1;
  assign buf1[25] = 1'b1;
  assign buf1[26] = 1'b1;
  assign buf1[27] = 1'b1;
  assign buf1[28] = 1'b1;
  assign buf1[29] = 1'b1;
  assign buf1[30] = 1'b1;
  assign buf1[31] = 1'b1;
  assign buf0[0] = 1'b1;
  assign buf0[1] = 1'b1;
  assign buf0[2] = 1'b1;
  assign buf0[3] = 1'b1;
  assign buf0[4] = 1'b1;
  assign buf0[5] = 1'b1;
  assign buf0[6] = 1'b1;
  assign buf0[7] = 1'b1;
  assign buf0[8] = 1'b1;
  assign buf0[9] = 1'b1;
  assign buf0[10] = 1'b1;
  assign buf0[11] = 1'b1;
  assign buf0[12] = 1'b1;
  assign buf0[13] = 1'b1;
  assign buf0[14] = 1'b1;
  assign buf0[15] = 1'b1;
  assign buf0[16] = 1'b1;
  assign buf0[17] = 1'b1;
  assign buf0[18] = 1'b1;
  assign buf0[19] = 1'b1;
  assign buf0[20] = 1'b1;
  assign buf0[21] = 1'b1;
  assign buf0[22] = 1'b1;
  assign buf0[23] = 1'b1;
  assign buf0[24] = 1'b1;
  assign buf0[25] = 1'b1;
  assign buf0[26] = 1'b1;
  assign buf0[27] = 1'b1;
  assign buf0[28] = 1'b1;
  assign buf0[29] = 1'b1;
  assign buf0[30] = 1'b1;
  assign buf0[31] = 1'b1;
  assign dma_out_buf_avail = 1'b0;
  assign dma_in_buf_sz1 = 1'b0;
  assign csr[0] = 1'b0;
  assign csr[1] = 1'b0;
  assign csr[2] = 1'b0;
  assign csr[3] = 1'b0;
  assign csr[4] = 1'b0;
  assign csr[5] = 1'b0;
  assign csr[6] = 1'b0;
  assign csr[7] = 1'b0;
  assign csr[8] = 1'b0;
  assign csr[9] = 1'b0;
  assign csr[10] = 1'b0;
  assign csr[11] = 1'b0;
  assign csr[12] = 1'b0;
  assign csr[13] = 1'b0;
  assign csr[14] = 1'b0;
  assign csr[15] = 1'b0;
  assign csr[16] = 1'b0;
  assign csr[17] = 1'b0;
  assign csr[18] = 1'b0;
  assign csr[19] = 1'b0;
  assign csr[20] = 1'b0;
  assign csr[21] = 1'b0;
  assign csr[22] = 1'b0;
  assign csr[23] = 1'b0;
  assign csr[24] = 1'b0;
  assign csr[25] = 1'b0;
  assign csr[26] = 1'b0;
  assign csr[27] = 1'b0;
  assign csr[28] = 1'b0;
  assign csr[29] = 1'b0;
  assign csr[30] = 1'b0;
  assign csr[31] = 1'b0;
  assign ep_match = 1'b0;
  assign dma_req = 1'b0;
  assign intb = 1'b0;
  assign inta = 1'b0;
  assign dout[0] = 1'b0;
  assign dout[1] = 1'b0;
  assign dout[2] = 1'b0;
  assign dout[3] = 1'b0;
  assign dout[4] = 1'b0;
  assign dout[5] = 1'b0;
  assign dout[6] = 1'b0;
  assign dout[7] = 1'b0;
  assign dout[8] = 1'b0;
  assign dout[9] = 1'b0;
  assign dout[10] = 1'b0;
  assign dout[11] = 1'b0;
  assign dout[12] = 1'b0;
  assign dout[13] = 1'b0;
  assign dout[14] = 1'b0;
  assign dout[15] = 1'b0;
  assign dout[16] = 1'b0;
  assign dout[17] = 1'b0;
  assign dout[18] = 1'b0;
  assign dout[19] = 1'b0;
  assign dout[20] = 1'b0;
  assign dout[21] = 1'b0;
  assign dout[22] = 1'b0;
  assign dout[23] = 1'b0;
  assign dout[24] = 1'b0;
  assign dout[25] = 1'b0;
  assign dout[26] = 1'b0;
  assign dout[27] = 1'b0;
  assign dout[28] = 1'b0;
  assign dout[29] = 1'b0;
  assign dout[30] = 1'b0;
  assign dout[31] = 1'b0;

endmodule


module usbf_ep_rf_dummy_2 ( clk, wclk, rst, adr, re, we, din, dout, inta, intb, 
        dma_req, dma_ack, idin, ep_sel, ep_match, buf0_rl, buf0_set, buf1_set, 
        uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, 
        int_crc16_set, int_to_set, int_seqerr_set, out_to_small, csr, buf0, 
        buf1, dma_in_buf_sz1, dma_out_buf_avail );
  input [1:0] adr;
  input [31:0] din;
  output [31:0] dout;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  input clk, wclk, rst, re, we, dma_ack, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small;
  output inta, intb, dma_req, ep_match, dma_in_buf_sz1, dma_out_buf_avail;

  assign buf1[0] = 1'b1;
  assign buf1[1] = 1'b1;
  assign buf1[2] = 1'b1;
  assign buf1[3] = 1'b1;
  assign buf1[4] = 1'b1;
  assign buf1[5] = 1'b1;
  assign buf1[6] = 1'b1;
  assign buf1[7] = 1'b1;
  assign buf1[8] = 1'b1;
  assign buf1[9] = 1'b1;
  assign buf1[10] = 1'b1;
  assign buf1[11] = 1'b1;
  assign buf1[12] = 1'b1;
  assign buf1[13] = 1'b1;
  assign buf1[14] = 1'b1;
  assign buf1[15] = 1'b1;
  assign buf1[16] = 1'b1;
  assign buf1[17] = 1'b1;
  assign buf1[18] = 1'b1;
  assign buf1[19] = 1'b1;
  assign buf1[20] = 1'b1;
  assign buf1[21] = 1'b1;
  assign buf1[22] = 1'b1;
  assign buf1[23] = 1'b1;
  assign buf1[24] = 1'b1;
  assign buf1[25] = 1'b1;
  assign buf1[26] = 1'b1;
  assign buf1[27] = 1'b1;
  assign buf1[28] = 1'b1;
  assign buf1[29] = 1'b1;
  assign buf1[30] = 1'b1;
  assign buf1[31] = 1'b1;
  assign buf0[0] = 1'b1;
  assign buf0[1] = 1'b1;
  assign buf0[2] = 1'b1;
  assign buf0[3] = 1'b1;
  assign buf0[4] = 1'b1;
  assign buf0[5] = 1'b1;
  assign buf0[6] = 1'b1;
  assign buf0[7] = 1'b1;
  assign buf0[8] = 1'b1;
  assign buf0[9] = 1'b1;
  assign buf0[10] = 1'b1;
  assign buf0[11] = 1'b1;
  assign buf0[12] = 1'b1;
  assign buf0[13] = 1'b1;
  assign buf0[14] = 1'b1;
  assign buf0[15] = 1'b1;
  assign buf0[16] = 1'b1;
  assign buf0[17] = 1'b1;
  assign buf0[18] = 1'b1;
  assign buf0[19] = 1'b1;
  assign buf0[20] = 1'b1;
  assign buf0[21] = 1'b1;
  assign buf0[22] = 1'b1;
  assign buf0[23] = 1'b1;
  assign buf0[24] = 1'b1;
  assign buf0[25] = 1'b1;
  assign buf0[26] = 1'b1;
  assign buf0[27] = 1'b1;
  assign buf0[28] = 1'b1;
  assign buf0[29] = 1'b1;
  assign buf0[30] = 1'b1;
  assign buf0[31] = 1'b1;
  assign dma_out_buf_avail = 1'b0;
  assign dma_in_buf_sz1 = 1'b0;
  assign csr[0] = 1'b0;
  assign csr[1] = 1'b0;
  assign csr[2] = 1'b0;
  assign csr[3] = 1'b0;
  assign csr[4] = 1'b0;
  assign csr[5] = 1'b0;
  assign csr[6] = 1'b0;
  assign csr[7] = 1'b0;
  assign csr[8] = 1'b0;
  assign csr[9] = 1'b0;
  assign csr[10] = 1'b0;
  assign csr[11] = 1'b0;
  assign csr[12] = 1'b0;
  assign csr[13] = 1'b0;
  assign csr[14] = 1'b0;
  assign csr[15] = 1'b0;
  assign csr[16] = 1'b0;
  assign csr[17] = 1'b0;
  assign csr[18] = 1'b0;
  assign csr[19] = 1'b0;
  assign csr[20] = 1'b0;
  assign csr[21] = 1'b0;
  assign csr[22] = 1'b0;
  assign csr[23] = 1'b0;
  assign csr[24] = 1'b0;
  assign csr[25] = 1'b0;
  assign csr[26] = 1'b0;
  assign csr[27] = 1'b0;
  assign csr[28] = 1'b0;
  assign csr[29] = 1'b0;
  assign csr[30] = 1'b0;
  assign csr[31] = 1'b0;
  assign ep_match = 1'b0;
  assign dma_req = 1'b0;
  assign intb = 1'b0;
  assign inta = 1'b0;
  assign dout[0] = 1'b0;
  assign dout[1] = 1'b0;
  assign dout[2] = 1'b0;
  assign dout[3] = 1'b0;
  assign dout[4] = 1'b0;
  assign dout[5] = 1'b0;
  assign dout[6] = 1'b0;
  assign dout[7] = 1'b0;
  assign dout[8] = 1'b0;
  assign dout[9] = 1'b0;
  assign dout[10] = 1'b0;
  assign dout[11] = 1'b0;
  assign dout[12] = 1'b0;
  assign dout[13] = 1'b0;
  assign dout[14] = 1'b0;
  assign dout[15] = 1'b0;
  assign dout[16] = 1'b0;
  assign dout[17] = 1'b0;
  assign dout[18] = 1'b0;
  assign dout[19] = 1'b0;
  assign dout[20] = 1'b0;
  assign dout[21] = 1'b0;
  assign dout[22] = 1'b0;
  assign dout[23] = 1'b0;
  assign dout[24] = 1'b0;
  assign dout[25] = 1'b0;
  assign dout[26] = 1'b0;
  assign dout[27] = 1'b0;
  assign dout[28] = 1'b0;
  assign dout[29] = 1'b0;
  assign dout[30] = 1'b0;
  assign dout[31] = 1'b0;

endmodule


module usbf_ep_rf_dummy_1 ( clk, wclk, rst, adr, re, we, din, dout, inta, intb, 
        dma_req, dma_ack, idin, ep_sel, ep_match, buf0_rl, buf0_set, buf1_set, 
        uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, 
        int_crc16_set, int_to_set, int_seqerr_set, out_to_small, csr, buf0, 
        buf1, dma_in_buf_sz1, dma_out_buf_avail );
  input [1:0] adr;
  input [31:0] din;
  output [31:0] dout;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  input clk, wclk, rst, re, we, dma_ack, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small;
  output inta, intb, dma_req, ep_match, dma_in_buf_sz1, dma_out_buf_avail;

  assign buf1[0] = 1'b1;
  assign buf1[1] = 1'b1;
  assign buf1[2] = 1'b1;
  assign buf1[3] = 1'b1;
  assign buf1[4] = 1'b1;
  assign buf1[5] = 1'b1;
  assign buf1[6] = 1'b1;
  assign buf1[7] = 1'b1;
  assign buf1[8] = 1'b1;
  assign buf1[9] = 1'b1;
  assign buf1[10] = 1'b1;
  assign buf1[11] = 1'b1;
  assign buf1[12] = 1'b1;
  assign buf1[13] = 1'b1;
  assign buf1[14] = 1'b1;
  assign buf1[15] = 1'b1;
  assign buf1[16] = 1'b1;
  assign buf1[17] = 1'b1;
  assign buf1[18] = 1'b1;
  assign buf1[19] = 1'b1;
  assign buf1[20] = 1'b1;
  assign buf1[21] = 1'b1;
  assign buf1[22] = 1'b1;
  assign buf1[23] = 1'b1;
  assign buf1[24] = 1'b1;
  assign buf1[25] = 1'b1;
  assign buf1[26] = 1'b1;
  assign buf1[27] = 1'b1;
  assign buf1[28] = 1'b1;
  assign buf1[29] = 1'b1;
  assign buf1[30] = 1'b1;
  assign buf1[31] = 1'b1;
  assign buf0[0] = 1'b1;
  assign buf0[1] = 1'b1;
  assign buf0[2] = 1'b1;
  assign buf0[3] = 1'b1;
  assign buf0[4] = 1'b1;
  assign buf0[5] = 1'b1;
  assign buf0[6] = 1'b1;
  assign buf0[7] = 1'b1;
  assign buf0[8] = 1'b1;
  assign buf0[9] = 1'b1;
  assign buf0[10] = 1'b1;
  assign buf0[11] = 1'b1;
  assign buf0[12] = 1'b1;
  assign buf0[13] = 1'b1;
  assign buf0[14] = 1'b1;
  assign buf0[15] = 1'b1;
  assign buf0[16] = 1'b1;
  assign buf0[17] = 1'b1;
  assign buf0[18] = 1'b1;
  assign buf0[19] = 1'b1;
  assign buf0[20] = 1'b1;
  assign buf0[21] = 1'b1;
  assign buf0[22] = 1'b1;
  assign buf0[23] = 1'b1;
  assign buf0[24] = 1'b1;
  assign buf0[25] = 1'b1;
  assign buf0[26] = 1'b1;
  assign buf0[27] = 1'b1;
  assign buf0[28] = 1'b1;
  assign buf0[29] = 1'b1;
  assign buf0[30] = 1'b1;
  assign buf0[31] = 1'b1;
  assign dma_out_buf_avail = 1'b0;
  assign dma_in_buf_sz1 = 1'b0;
  assign csr[0] = 1'b0;
  assign csr[1] = 1'b0;
  assign csr[2] = 1'b0;
  assign csr[3] = 1'b0;
  assign csr[4] = 1'b0;
  assign csr[5] = 1'b0;
  assign csr[6] = 1'b0;
  assign csr[7] = 1'b0;
  assign csr[8] = 1'b0;
  assign csr[9] = 1'b0;
  assign csr[10] = 1'b0;
  assign csr[11] = 1'b0;
  assign csr[12] = 1'b0;
  assign csr[13] = 1'b0;
  assign csr[14] = 1'b0;
  assign csr[15] = 1'b0;
  assign csr[16] = 1'b0;
  assign csr[17] = 1'b0;
  assign csr[18] = 1'b0;
  assign csr[19] = 1'b0;
  assign csr[20] = 1'b0;
  assign csr[21] = 1'b0;
  assign csr[22] = 1'b0;
  assign csr[23] = 1'b0;
  assign csr[24] = 1'b0;
  assign csr[25] = 1'b0;
  assign csr[26] = 1'b0;
  assign csr[27] = 1'b0;
  assign csr[28] = 1'b0;
  assign csr[29] = 1'b0;
  assign csr[30] = 1'b0;
  assign csr[31] = 1'b0;
  assign ep_match = 1'b0;
  assign dma_req = 1'b0;
  assign intb = 1'b0;
  assign inta = 1'b0;
  assign dout[0] = 1'b0;
  assign dout[1] = 1'b0;
  assign dout[2] = 1'b0;
  assign dout[3] = 1'b0;
  assign dout[4] = 1'b0;
  assign dout[5] = 1'b0;
  assign dout[6] = 1'b0;
  assign dout[7] = 1'b0;
  assign dout[8] = 1'b0;
  assign dout[9] = 1'b0;
  assign dout[10] = 1'b0;
  assign dout[11] = 1'b0;
  assign dout[12] = 1'b0;
  assign dout[13] = 1'b0;
  assign dout[14] = 1'b0;
  assign dout[15] = 1'b0;
  assign dout[16] = 1'b0;
  assign dout[17] = 1'b0;
  assign dout[18] = 1'b0;
  assign dout[19] = 1'b0;
  assign dout[20] = 1'b0;
  assign dout[21] = 1'b0;
  assign dout[22] = 1'b0;
  assign dout[23] = 1'b0;
  assign dout[24] = 1'b0;
  assign dout[25] = 1'b0;
  assign dout[26] = 1'b0;
  assign dout[27] = 1'b0;
  assign dout[28] = 1'b0;
  assign dout[29] = 1'b0;
  assign dout[30] = 1'b0;
  assign dout[31] = 1'b0;

endmodule


module usbf_rf ( clk, wclk, rst, adr, re, we, din, dout, inta, intb, dma_req, 
        dma_ack, idin, ep_sel, match, buf0_rl, buf0_set, buf1_set, uc_bsel_set, 
        uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, int_crc16_set, 
        int_to_set, int_seqerr_set, out_to_small, csr, buf0, buf1, funct_adr, 
        dma_in_buf_sz1, dma_out_buf_avail, frm_nat, utmi_vend_stat, 
        utmi_vend_ctrl, utmi_vend_wr, line_stat, usb_attached, mode_hs, 
        suspend, attached, usb_reset, pid_cs_err, nse_err, crc5_err, rx_err, 
        rf_resume_req );
  input [6:0] adr;
  input [31:0] din;
  output [31:0] dout;
  output [15:0] dma_req;
  input [15:0] dma_ack;
  input [31:0] idin;
  input [3:0] ep_sel;
  output [31:0] csr;
  output [31:0] buf0;
  output [31:0] buf1;
  output [6:0] funct_adr;
  input [31:0] frm_nat;
  input [7:0] utmi_vend_stat;
  output [3:0] utmi_vend_ctrl;
  input [1:0] line_stat;
  input clk, wclk, rst, re, we, buf0_rl, buf0_set, buf1_set, uc_bsel_set,
         uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set, int_crc16_set,
         int_to_set, int_seqerr_set, out_to_small, usb_attached, mode_hs,
         suspend, attached, usb_reset, pid_cs_err, nse_err, crc5_err, rx_err;
  output inta, intb, match, dma_in_buf_sz1, dma_out_buf_avail, utmi_vend_wr,
         rf_resume_req;
  wire   n875, n876, n877, n878, N70, N71, N72, N73, N74, N75, N76, N77, N78,
         N80, N81, N82, N83, N84, N85, N86, N87, N88, N89, N90, N93, N94, N95,
         N96, N97, N98, N99, N100, N101, N102, N103, int_src_re,
         utmi_vend_wr_r, rf_resume_req_r, ep0_re, ep1_re, ep2_re, ep3_re,
         ep4_re, ep5_re, ep6_re, ep7_re, ep8_re, ep9_re, ep10_re, ep11_re,
         ep12_re, ep13_re, ep14_re, ep15_re, ep0_we, ep1_we, ep2_we, ep3_we,
         ep4_we, ep5_we, ep6_we, ep7_we, ep8_we, ep9_we, ep10_we, ep11_we,
         ep12_we, ep13_we, ep14_we, ep15_we, ep0_match, ep1_match, ep2_match,
         ep3_match, ep0_dma_in_buf_sz1, ep1_dma_in_buf_sz1, ep2_dma_in_buf_sz1,
         ep3_dma_in_buf_sz1, ep0_dma_out_buf_avail, ep1_dma_out_buf_avail,
         ep2_dma_out_buf_avail, ep3_dma_out_buf_avail, attach_r, attach_r1,
         suspend_r, suspend_r1, usb_reset_r, rx_err_r, nse_err_r, pid_cs_err_r,
         crc5_err_r, ep3_inta, ep3_intb, ep2_inta, ep2_intb, N728, ep1_inta,
         ep1_intb, N729, ep0_inta, ep0_intb, N731, N732, n712, n717, n719,
         n721, n722, n724, n725, n726, n727, n728, n733, n734, n735, n742,
         n747, n764, n779, n780, n781, n782, n783, n785, n788, n789, n794,
         n813, n814, n815, n816, n821, n824, n825, n842, n843, n844, n845,
         n847, n848, n865, n866, n867, n868, n869, n870, n887, n888, n889,
         n890, n891, n892, n909, n910, n911, n912, n913, n914, n931, n932,
         n933, n934, n935, n936, n953, n954, n955, n956, n957, n958, n975,
         n976, n977, n978, n979, n980, n997, n998, n999, n1000, n1001, n1002,
         n1019, n1020, n1021, n1022, n1023, n1024, n1041, n1042, n1043, n1044,
         n1045, n1046, n1063, n1064, n1065, n1066, n1067, n1068, n1085, n1086,
         n1087, n1088, n1089, n1090, n1107, n1108, n1109, n1110, n1111, n1112,
         n1129, n1130, n1131, n1132, n1133, n1134, n1151, n1152, n1153, n1154,
         n1155, n1156, n1173, n1174, n1175, n1176, n1177, n1178, n1195, n1196,
         n1197, n1198, n1199, n1200, n1217, n1218, n1219, n1220, n1221, n1222,
         n1239, n1240, n1241, n1242, n1243, n1244, n1261, n1262, n1263, n1264,
         n1265, n1266, n1283, n1284, n1285, n1286, n1287, n1288, n1305, n1306,
         n1307, n1308, n1309, n1310, n1327, n1328, n1329, n1330, n1331, n1332,
         n1349, n1350, n1351, n1352, n1353, n1354, n1371, n1372, n1373, n1374,
         n1375, n1376, n1393, n1394, n1395, n1396, n1397, n1398, n1415, n1416,
         n1417, n1418, n1419, n1420, n1437, n1438, n1439, n1440, n1441, n1442,
         n1459, n1460, n1461, n1462, n1463, n1464, n1481, n1482, n1483, n1484,
         n1485, n1486, n1503, n1504, n1505, n1506, n1507, n1508, n1525, n1526,
         n1527, n1528, n1529, n1530, n1547, n1548, n1549, n1550, n1551, n1552,
         n1569, n1570, n1571, n1572, n1573, n1574, n1591, n1592, n1593, n1594,
         n1595, n1596, n1613, n1614, n1615, n1616, n1617, n1618, n1635, n1636,
         n1637, n1638, n1639, n1640, n1657, n1658, n1659, n1660, n1661, n1662,
         n1679, n1680, n1681, n1682, n1683, n1684, n1701, n1702, n1703, n1704,
         n1705, n1706, n1723, n1724, n1725, n1726, n1727, n1728, n1745, n1746,
         n1747, n1748, n1749, n1750, n1767, n1768, n1769, n1770, n1771, n1772,
         n1789, n1790, n1791, n1792, n1793, n1794, n1811, n1812, n1813, n1814,
         n1815, n1816, n1833, n1834, n1835, n1836, n1837, n1838, n1855, n1856,
         n1857, n1858, n1859, n1860, n1877, n1878, n1879, n1880, n1881, n1882,
         n1899, n1900, n1901, n1902, n1903, n1904, n1921, n1922, n1923, n1924,
         n1925, n1926, n1943, n1944, n1945, n1946, n1947, n1948, n1965, n1966,
         n1967, n1968, n1969, n1970, n1987, n1988, n1989, n1990, n1991, n1992,
         n2009, n2010, n2011, n2012, n2013, n2014, n2031, n2032, n2033, n2034,
         n2035, n2036, n2053, n2054, n2055, n2056, n2057, n2058, n2075, n2076,
         n2077, n2078, n2079, n2080, n2097, n2098, n2099, n2100, n2101, n2102,
         n2119, n2120, n2121, n2122, n2123, n2124, n2141, n2142, n2143, n2144,
         n2145, n2146, n2163, n2164, n2165, n2166, n2167, n2168, n2185, n2186,
         n2187, n2188, n2189, n2190, n2207, n2208, n2209, n2210, n2211, n2212,
         n2229, n2230, n2231, n2232, n2233, n2234, n2251, n2252, n2253, n2254,
         n2255, n2256, n2273, n2274, n2275, n2276, n2277, n2278, n2295, n2296,
         n2297, n2298, n2299, n2300, n2317, n2318, n2319, n2320, n2321, n2322,
         n2339, n2340, n2341, n2342, n2343, n2344, n2361, n2362, n2363, n2364,
         n2365, n2366, n2383, n2384, n2385, n2386, n2387, n2388, n2405, n2406,
         n2407, n2408, n2409, n2410, n2427, n2428, n2429, n2430, n2431, n2432,
         n2449, n2450, n2451, n2452, n2453, n2454, n2471, n2472, n2473, n2474,
         n2475, n2476, n2493, n2494, n2495, n2496, n2497, n2498, n2515, n2516,
         n2517, n2518, n2519, n2520, n2537, n2538, n2539, n2540, n2541, n2542,
         n2581, n2582, n2583, n2584, n2585, n2586, n2603, n2604, n2605, n2606,
         n2607, n2608, n2625, n2626, n2627, n2628, n2629, n2630, n2647, n2648,
         n2649, n2650, n2651, n2652, n2669, n2670, n2671, n2672, n2673, n2674,
         n2691, n2692, n2693, n2694, n2695, n2696, n2713, n2714, n2715, n2716,
         n2717, n2718, n2735, n2736, n2737, n2738, n2739, n2740, n2757, n2758,
         n2759, n2760, n2761, n2762, n2779, n2780, n2781, n2782, n2783, n2784,
         n2801, n2802, n2803, n2804, n2805, n2806, n2823, n2824, n2825, n2826,
         n2827, n2828, n2845, n2846, n2847, n2848, n2849, n2850, n2867, n2868,
         n2869, n2870, n2871, n2872, n2889, n2890, n2891, n2892, n2893, n2894,
         n2911, n2912, n2913, n2914, n2915, n2916, n2933, n2934, n2935, n2936,
         n2937, n2940, n2949, n2950, n2953, n3045, n3046, n3047, n3048, n3049,
         n3051, n3052, n3195, n3196, n3197, n3198, n3208, n3209, n3210, n3215,
         n3216, n3217, n3218, n3219, n3224, n3225, n3226, n3227, n3228, n3229,
         n3230, n3231, n3232, n3233, n3234, n3235, n3236, n3237, n3238, n3239,
         n3241, n3242, n3243, n3244, n3245, n3246, n3247, n3248, n3249, n3250,
         n3251, n3252, n3253, n3254, n3255, n3256, n3257, n3258, n3259, n3260,
         n3261, n3262, n3263, n3264, n3265, n3266, n3267, n3268, n3269, n3270,
         n3271, n3272, n3273, n3274, n3275, n3276, n3277, n3278, n3279, n3280,
         n3281, n3282, n3283, n3284, n3285, n3286, n3287, n3288, n3289, n3290,
         n3291, n3292, n3293, n3294, n3295, n3296, n3297, n3298, n3299, n3300,
         n3301, n3302, n3303, n3304, n3305, n3306, n3307, n3308, n3309, n3310,
         n3311, n3312, n3313, n3314, n3315, n3316, n3317, n3318, n3319, n3320,
         n3321, n3322, n3323, n3324, n3325, n3326, n3327, n3328, n3330, n3331,
         n3332, n3333, n3334, n3335, n3336, n3337, n3338, n3339, n3340, n3341,
         n3342, n3343, n3344, n3345, n3346, n3347, n3348, n3349, n3350, n3351,
         n3352, n3353, n3354, n3355, n3356, n3357, n3358, n3359, n3360, n3361,
         n3362, n3363, n3364, n3365, n3366, n3367, n3368, n3369, n3370, n3371,
         n3372, n3373, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14,
         n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28,
         n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42,
         n43, n44, n45, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56,
         n57, n58, n59, n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70,
         n71, n72, n73, n74, n75, n76, n77, n78, n79, n80, n81, n82, n83, n84,
         n85, n86, n87, n88, n89, n90, n91, n92, n93, n94, n95, n96, n97, n98,
         n99, n100, n101, n102, n103, n104, n105, n106, n107, n108, n109, n110,
         n111, n112, n113, n114, n115, n116, n117, n118, n119, n120, n121,
         n122, n123, n124, n125, n126, n127, n128, n129, n130, n131, n132,
         n133, n134, n135, n137, n138, n139, n140, n141, n142, n143, n144,
         n145, n146, n147, n148, n149, n150, n151, n152, n153, n154, n155,
         n156, n157, n158, n159, n160, n161, n162, n163, n164, n165, n166,
         n167, n168, n169, n170, n171, n172, n173, n174, n175, n176, n177,
         n178, n179, n180, n181, n182, n183, n184, n185, n186, n187, n188,
         n189, n190, n191, n192, n193, n194, n195, n196, n197, n198, n199,
         n200, n201, n202, n203, n204, n205, n206, n207, n208, n209, n210,
         n211, n212, n213, n214, n215, n216, n217, n218, n219, n220, n221,
         n222, n223, n224, n225, n226, n227, n228, n229, n230, n231, n232,
         n233, n234, n235, n236, n237, n238, n239, n240, n241, n242, n243,
         n244, n245, n246, n247, n248, n249, n250, n251, n252, n253, n254,
         n255, n256, n257, n258, n260, n261, n262, n263, n264, n265, n267,
         n269, n270, n271, n272, n273, n274, n275, n276, n277, n278, n279,
         n280, n281, n282, n283, n284, n285, n286, n287, n288, n289, n290,
         n291, n292, n293, n294, n295, n296, n297, n298, n299, n300, n301,
         n302, n303, n304, n305, n306, n307, n308, n309, n310, n311, n312,
         n313, n314, n315, n316, n317, n318, n319, n320, n321, n322, n323,
         n324, n325, n326, n327, n328, n329, n330, n331, n332, n333, n334,
         n335, n336, n337, n338, n339, n340, n341, n342, n343, n344, n345,
         n346, n347, n348, n349, n350, n351, n352, n353, n354, n355, n356,
         n357, n358, n359, n360, n361, n362, n363, n364, n365, n366, n367,
         n368, n369, n370, n371, n372, n373, n374, n375, n376, n377, n378,
         n379, n380, n381, n382, n383, n384, n385, n386, n387, n388, n389,
         n390, n391, n392, n393, n394, n395, n396, n397, n398, n399, n400,
         n401, n402, n403, n404, n405, n406, n407, n408, n409, n410, n411,
         n412, n413, n414, n415, n416, n417, n418, n419, n420, n421, n422,
         n423, n424, n425, n426, n427, n428, n429, n430, n431, n432, n433,
         n434, n435, n436, n437, n438, n439, n440, n441, n442, n443, n444,
         n445, n446, n447, n448, n449, n450, n451, n452, n453, n454, n455,
         n456, n457, n458, n459, n460, n461, n462, n463, n464, n465, n466,
         n467, n468, n469, n470, n471, n472, n473, n474, n475, n476, n477,
         n478, n479, n480, n481, n482, n483, n484, n485, n486, n487, n488,
         n489, n490, n491, n492, n493, n494, n495, n496, n497, n498, n499,
         n500, n501, n502, n503, n504, n505, n506, n507, n508, n509, n510,
         n511, n512, n513, n514, n515, n516, n517, n518, n519, n520, n521,
         n522, n523, n524, n525, n526, n527, n528, n529, n530, n531, n532,
         n533, n534, n535, n536, n537, n538, n539, n540, n541, n542, n543,
         n544, n545, n546, n547, n548, n549, n550, n551, n552, n553, n554,
         n555, n556, n557, n558, n559, n560, n561, n562, n563, n564, n565,
         n566, n567, n568, n569, n570, n571, n572, n573, n574, n575, n576,
         n577, n578, n579, n580, n581, n582, n583, n584, n585, n586, n587,
         n588, n589, n590, n591, n592, n593, n594, n595, n596, n597, n598,
         n599, n600, n601, n602, n603, n604, n605, n606, n607, n608, n609,
         n610, n611, n612, n613, n614, n615, n616, n617, n618, n619, n620,
         n621, n622, n623, n624, n625, n626, n627, n628, n629, n630, n631,
         n632, n633, n634, n635, n636, n637, n638, n639, n640, n641, n642,
         n643, n644, n645, n646, n647, n648, n649, n650, n651, n652, n653,
         n654, n655, n656, n657, n658, n659, n660, n661, n662, n663, n664,
         n665, n666, n667, n668, n669, n670, n671, n672, n673, n674, n675,
         n676, n677, n678, n679, n680, n681, n682, n683, n684, n685, n686,
         n687, n688, n689, n690, n691, n692, n693, n694, n695, n696, n697,
         n698, n699, n700, n701, n702, n703, n704, n705, n706, n707, n708,
         n709, n710, n711, n713, n714, n715, n716, n718, n720, n723, n729,
         n730, n731, n732, n736, n737, n738, n739, n740, n741, n743, n744,
         n745, n746, n748, n749, n750, n751, n752, n753, n754, n755, n756,
         n757, n758, n759, n760, n761, n762, n763, n765, n766, n767, n768,
         n769, n770, n771, n772, n773, n774, n775, n776, n777, n778, n784,
         n786, n787, n790, n791, n792, n793, n795, n796, n797, n798, n799,
         n800, n801, n802, n803, n804, n805, n806, n807, n808, n809, n810,
         n811, n812, n817, n818, n819, n820, n822, n823, n826, n827, n828,
         n829, n830, n831, n832, n833, n834, n835, n836, n837, n838, n839,
         n840, n841, n846, n849, n850, n851, n852, n853, n854, n855, n856,
         n857, n858, n859, n860, n861, n862, n863, n864, n871, n872, n873,
         n874;
  wire   [31:0] dtmp;
  wire   [8:0] intb_msk;
  wire   [8:0] inta_msk;
  wire   [8:0] int_srcb;
  wire   [15:0] int_srca;
  wire   [7:0] utmi_vend_stat_r;
  wire   [3:0] utmi_vend_ctrl_r;
  wire   [31:0] ep0_dout;
  wire   [31:0] ep1_dout;
  wire   [31:0] ep2_dout;
  wire   [31:0] ep3_dout;
  wire   [31:0] ep0_csr;
  wire   [31:0] ep1_csr;
  wire   [31:0] ep2_csr;
  wire   [31:0] ep3_csr;
  wire   [31:0] ep0_buf0;
  wire   [31:0] ep1_buf0;
  wire   [31:0] ep2_buf0;
  wire   [31:0] ep3_buf0;
  wire   [31:0] ep0_buf1;
  wire   [31:0] ep1_buf1;
  wire   [31:0] ep2_buf1;
  wire   [31:0] ep3_buf1;
  wire   SYNOPSYS_UNCONNECTED__0, SYNOPSYS_UNCONNECTED__1, 
        SYNOPSYS_UNCONNECTED__2, SYNOPSYS_UNCONNECTED__3;
  assign dma_req[15] = 1'b0;
  assign dma_req[14] = 1'b0;
  assign dma_req[13] = 1'b0;
  assign dma_req[12] = 1'b0;
  assign dma_req[11] = 1'b0;
  assign dma_req[10] = 1'b0;
  assign dma_req[9] = 1'b0;
  assign dma_req[8] = 1'b0;
  assign dma_req[7] = 1'b0;
  assign dma_req[6] = 1'b0;
  assign dma_req[5] = 1'b0;
  assign dma_req[4] = 1'b0;

  inv_1 U3164 ( .ip(rst), .op(n764) );
  dp_2 \funct_adr_reg[6]  ( .ip(n3371), .ck(wclk), .q(funct_adr[6]) );
  dp_2 \funct_adr_reg[5]  ( .ip(n3370), .ck(wclk), .q(funct_adr[5]) );
  dp_2 \funct_adr_reg[1]  ( .ip(n3366), .ck(wclk), .q(funct_adr[1]) );
  dp_2 \intb_msk_reg[8]  ( .ip(n3364), .ck(wclk), .q(intb_msk[8]) );
  dp_2 \intb_msk_reg[7]  ( .ip(n3363), .ck(wclk), .q(intb_msk[7]) );
  dp_2 \intb_msk_reg[6]  ( .ip(n3362), .ck(wclk), .q(intb_msk[6]) );
  dp_2 attach_r_reg ( .ip(usb_attached), .ck(wclk), .q(attach_r) );
  dp_2 suspend_r_reg ( .ip(suspend), .ck(wclk), .q(suspend_r) );
  dp_2 \int_srcb_reg[8]  ( .ip(n3248), .ck(wclk), .q(int_srcb[8]) );
  dp_2 \int_srcb_reg[7]  ( .ip(n3247), .ck(wclk), .q(int_srcb[7]) );
  dp_2 \int_srcb_reg[6]  ( .ip(n3246), .ck(wclk), .q(int_srcb[6]) );
  dp_2 \int_srcb_reg[4]  ( .ip(n3244), .ck(wclk), .q(int_srcb[4]) );
  dp_2 \int_srcb_reg[3]  ( .ip(n3243), .ck(wclk), .q(int_srcb[3]) );
  dp_2 \int_srcb_reg[2]  ( .ip(n3242), .ck(wclk), .q(int_srcb[2]) );
  dp_2 \int_srcb_reg[1]  ( .ip(n3241), .ck(wclk), .q(int_srcb[1]) );
  dp_2 \dout_reg[6]  ( .ip(n3233), .ck(wclk), .q(dout[6]) );
  dp_2 \dout_reg[5]  ( .ip(n3234), .ck(wclk), .q(dout[5]) );
  dp_2 \dout_reg[4]  ( .ip(n3235), .ck(wclk), .q(dout[4]) );
  dp_2 \dout_reg[3]  ( .ip(n3236), .ck(wclk), .q(dout[3]) );
  dp_2 \dout_reg[2]  ( .ip(n3237), .ck(wclk), .q(dout[2]) );
  dp_2 \dout_reg[1]  ( .ip(n3238), .ck(wclk), .q(dout[1]) );
  dp_2 \dout_reg[0]  ( .ip(n3239), .ck(wclk), .q(dout[0]) );
  nand4_2 U728 ( .ip1(n318), .ip2(n316), .ip3(n724), .ip4(n725), .op(n717) );
  and3_2 U795 ( .ip1(n780), .ip2(n781), .ip3(n782), .op(n779) );
  nand2_2 U796 ( .ip1(ep2_dma_out_buf_avail), .ip2(n313), .op(n782) );
  nand2_2 U797 ( .ip1(dma_out_buf_avail), .ip2(n2953), .op(n781) );
  nand2_2 U798 ( .ip1(ep1_dma_out_buf_avail), .ip2(n304), .op(n780) );
  nand2_2 U804 ( .ip1(ep3_dma_out_buf_avail), .ip2(n295), .op(n789) );
  nand2_2 U805 ( .ip1(n327), .ip2(ep0_dma_out_buf_avail), .op(n788) );
  and3_2 U818 ( .ip1(n814), .ip2(n815), .ip3(n816), .op(n813) );
  nand2_2 U819 ( .ip1(ep2_dma_in_buf_sz1), .ip2(n312), .op(n816) );
  nand2_2 U820 ( .ip1(dma_in_buf_sz1), .ip2(n2953), .op(n815) );
  nand2_2 U825 ( .ip1(ep1_dma_in_buf_sz1), .ip2(n303), .op(n814) );
  nand2_2 U831 ( .ip1(ep3_dma_in_buf_sz1), .ip2(n294), .op(n825) );
  nand2_2 U832 ( .ip1(ep0_dma_in_buf_sz1), .ip2(n327), .op(n824) );
  and3_2 U845 ( .ip1(n843), .ip2(n844), .ip3(n845), .op(n842) );
  nand2_2 U846 ( .ip1(ep2_buf1[0]), .ip2(n312), .op(n845) );
  nand2_2 U847 ( .ip1(buf1[0]), .ip2(n279), .op(n844) );
  nand2_2 U848 ( .ip1(ep1_buf1[0]), .ip2(n303), .op(n843) );
  nand2_2 U854 ( .ip1(ep3_buf1[0]), .ip2(n294), .op(n848) );
  nand2_2 U855 ( .ip1(ep0_buf1[0]), .ip2(n327), .op(n847) );
  and3_2 U868 ( .ip1(n866), .ip2(n867), .ip3(n868), .op(n865) );
  nand2_2 U869 ( .ip1(ep2_buf1[1]), .ip2(n312), .op(n868) );
  nand2_2 U870 ( .ip1(buf1[1]), .ip2(n279), .op(n867) );
  nand2_2 U871 ( .ip1(ep1_buf1[1]), .ip2(n303), .op(n866) );
  nand2_2 U877 ( .ip1(ep3_buf1[1]), .ip2(n294), .op(n870) );
  nand2_2 U878 ( .ip1(ep0_buf1[1]), .ip2(n326), .op(n869) );
  and3_2 U891 ( .ip1(n888), .ip2(n889), .ip3(n890), .op(n887) );
  nand2_2 U892 ( .ip1(ep2_buf1[2]), .ip2(n312), .op(n890) );
  nand2_2 U893 ( .ip1(buf1[2]), .ip2(n279), .op(n889) );
  nand2_2 U894 ( .ip1(ep1_buf1[2]), .ip2(n303), .op(n888) );
  nand2_2 U900 ( .ip1(ep3_buf1[2]), .ip2(n294), .op(n892) );
  nand2_2 U901 ( .ip1(ep0_buf1[2]), .ip2(n326), .op(n891) );
  and3_2 U914 ( .ip1(n910), .ip2(n911), .ip3(n912), .op(n909) );
  nand2_2 U915 ( .ip1(ep2_buf1[3]), .ip2(n312), .op(n912) );
  nand2_2 U916 ( .ip1(buf1[3]), .ip2(n279), .op(n911) );
  nand2_2 U917 ( .ip1(ep1_buf1[3]), .ip2(n303), .op(n910) );
  nand2_2 U923 ( .ip1(ep3_buf1[3]), .ip2(n294), .op(n914) );
  nand2_2 U924 ( .ip1(ep0_buf1[3]), .ip2(n326), .op(n913) );
  and3_2 U937 ( .ip1(n932), .ip2(n933), .ip3(n934), .op(n931) );
  nand2_2 U938 ( .ip1(ep2_buf1[4]), .ip2(n312), .op(n934) );
  nand2_2 U939 ( .ip1(buf1[4]), .ip2(n279), .op(n933) );
  nand2_2 U940 ( .ip1(ep1_buf1[4]), .ip2(n303), .op(n932) );
  nand2_2 U946 ( .ip1(ep3_buf1[4]), .ip2(n294), .op(n936) );
  nand2_2 U947 ( .ip1(ep0_buf1[4]), .ip2(n326), .op(n935) );
  and3_2 U960 ( .ip1(n954), .ip2(n955), .ip3(n956), .op(n953) );
  nand2_2 U961 ( .ip1(ep2_buf1[5]), .ip2(n312), .op(n956) );
  nand2_2 U962 ( .ip1(buf1[5]), .ip2(n279), .op(n955) );
  nand2_2 U963 ( .ip1(ep1_buf1[5]), .ip2(n303), .op(n954) );
  nand2_2 U969 ( .ip1(ep3_buf1[5]), .ip2(n294), .op(n958) );
  nand2_2 U970 ( .ip1(ep0_buf1[5]), .ip2(n326), .op(n957) );
  and3_2 U983 ( .ip1(n976), .ip2(n977), .ip3(n978), .op(n975) );
  nand2_2 U984 ( .ip1(ep2_buf1[6]), .ip2(n312), .op(n978) );
  nand2_2 U985 ( .ip1(buf1[6]), .ip2(n279), .op(n977) );
  nand2_2 U986 ( .ip1(ep1_buf1[6]), .ip2(n303), .op(n976) );
  nand2_2 U992 ( .ip1(ep3_buf1[6]), .ip2(n294), .op(n980) );
  nand2_2 U993 ( .ip1(ep0_buf1[6]), .ip2(n326), .op(n979) );
  and3_2 U1006 ( .ip1(n998), .ip2(n999), .ip3(n1000), .op(n997) );
  nand2_2 U1007 ( .ip1(ep2_buf1[7]), .ip2(n312), .op(n1000) );
  nand2_2 U1008 ( .ip1(buf1[7]), .ip2(n279), .op(n999) );
  nand2_2 U1009 ( .ip1(ep1_buf1[7]), .ip2(n303), .op(n998) );
  nand2_2 U1015 ( .ip1(ep3_buf1[7]), .ip2(n294), .op(n1002) );
  nand2_2 U1016 ( .ip1(ep0_buf1[7]), .ip2(n326), .op(n1001) );
  and3_2 U1029 ( .ip1(n1020), .ip2(n1021), .ip3(n1022), .op(n1019) );
  nand2_2 U1030 ( .ip1(ep2_buf1[8]), .ip2(n312), .op(n1022) );
  nand2_2 U1031 ( .ip1(buf1[8]), .ip2(n279), .op(n1021) );
  nand2_2 U1032 ( .ip1(ep1_buf1[8]), .ip2(n303), .op(n1020) );
  nand2_2 U1038 ( .ip1(ep3_buf1[8]), .ip2(n294), .op(n1024) );
  nand2_2 U1039 ( .ip1(ep0_buf1[8]), .ip2(n326), .op(n1023) );
  and3_2 U1052 ( .ip1(n1042), .ip2(n1043), .ip3(n1044), .op(n1041) );
  nand2_2 U1053 ( .ip1(ep2_buf1[9]), .ip2(n312), .op(n1044) );
  nand2_2 U1054 ( .ip1(buf1[9]), .ip2(n279), .op(n1043) );
  nand2_2 U1055 ( .ip1(ep1_buf1[9]), .ip2(n303), .op(n1042) );
  nand2_2 U1061 ( .ip1(ep3_buf1[9]), .ip2(n294), .op(n1046) );
  nand2_2 U1062 ( .ip1(ep0_buf1[9]), .ip2(n326), .op(n1045) );
  and3_2 U1075 ( .ip1(n1064), .ip2(n1065), .ip3(n1066), .op(n1063) );
  nand2_2 U1076 ( .ip1(ep2_buf1[10]), .ip2(n312), .op(n1066) );
  nand2_2 U1077 ( .ip1(buf1[10]), .ip2(n279), .op(n1065) );
  nand2_2 U1078 ( .ip1(ep1_buf1[10]), .ip2(n303), .op(n1064) );
  nand2_2 U1084 ( .ip1(ep3_buf1[10]), .ip2(n294), .op(n1068) );
  nand2_2 U1085 ( .ip1(ep0_buf1[10]), .ip2(n326), .op(n1067) );
  and3_2 U1098 ( .ip1(n1086), .ip2(n1087), .ip3(n1088), .op(n1085) );
  nand2_2 U1099 ( .ip1(ep2_buf1[11]), .ip2(n311), .op(n1088) );
  nand2_2 U1100 ( .ip1(buf1[11]), .ip2(n279), .op(n1087) );
  nand2_2 U1101 ( .ip1(ep1_buf1[11]), .ip2(n302), .op(n1086) );
  nand2_2 U1107 ( .ip1(ep3_buf1[11]), .ip2(n293), .op(n1090) );
  nand2_2 U1108 ( .ip1(ep0_buf1[11]), .ip2(n326), .op(n1089) );
  and3_2 U1121 ( .ip1(n1108), .ip2(n1109), .ip3(n1110), .op(n1107) );
  nand2_2 U1122 ( .ip1(ep2_buf1[12]), .ip2(n311), .op(n1110) );
  nand2_2 U1123 ( .ip1(buf1[12]), .ip2(n280), .op(n1109) );
  nand2_2 U1124 ( .ip1(ep1_buf1[12]), .ip2(n302), .op(n1108) );
  nand2_2 U1130 ( .ip1(ep3_buf1[12]), .ip2(n293), .op(n1112) );
  nand2_2 U1131 ( .ip1(ep0_buf1[12]), .ip2(n326), .op(n1111) );
  and3_2 U1144 ( .ip1(n1130), .ip2(n1131), .ip3(n1132), .op(n1129) );
  nand2_2 U1145 ( .ip1(ep2_buf1[13]), .ip2(n311), .op(n1132) );
  nand2_2 U1146 ( .ip1(buf1[13]), .ip2(n280), .op(n1131) );
  nand2_2 U1147 ( .ip1(ep1_buf1[13]), .ip2(n302), .op(n1130) );
  nand2_2 U1153 ( .ip1(ep3_buf1[13]), .ip2(n293), .op(n1134) );
  nand2_2 U1154 ( .ip1(ep0_buf1[13]), .ip2(n326), .op(n1133) );
  and3_2 U1167 ( .ip1(n1152), .ip2(n1153), .ip3(n1154), .op(n1151) );
  nand2_2 U1168 ( .ip1(ep2_buf1[14]), .ip2(n311), .op(n1154) );
  nand2_2 U1169 ( .ip1(buf1[14]), .ip2(n280), .op(n1153) );
  nand2_2 U1170 ( .ip1(ep1_buf1[14]), .ip2(n302), .op(n1152) );
  nand2_2 U1176 ( .ip1(ep3_buf1[14]), .ip2(n293), .op(n1156) );
  nand2_2 U1177 ( .ip1(ep0_buf1[14]), .ip2(n326), .op(n1155) );
  and3_2 U1190 ( .ip1(n1174), .ip2(n1175), .ip3(n1176), .op(n1173) );
  nand2_2 U1191 ( .ip1(ep2_buf1[15]), .ip2(n311), .op(n1176) );
  nand2_2 U1192 ( .ip1(buf1[15]), .ip2(n280), .op(n1175) );
  nand2_2 U1193 ( .ip1(ep1_buf1[15]), .ip2(n302), .op(n1174) );
  nand2_2 U1199 ( .ip1(ep3_buf1[15]), .ip2(n293), .op(n1178) );
  nand2_2 U1200 ( .ip1(ep0_buf1[15]), .ip2(n325), .op(n1177) );
  and3_2 U1213 ( .ip1(n1196), .ip2(n1197), .ip3(n1198), .op(n1195) );
  nand2_2 U1214 ( .ip1(ep2_buf1[16]), .ip2(n311), .op(n1198) );
  nand2_2 U1215 ( .ip1(buf1[16]), .ip2(n280), .op(n1197) );
  nand2_2 U1216 ( .ip1(ep1_buf1[16]), .ip2(n302), .op(n1196) );
  nand2_2 U1222 ( .ip1(ep3_buf1[16]), .ip2(n293), .op(n1200) );
  nand2_2 U1223 ( .ip1(ep0_buf1[16]), .ip2(n325), .op(n1199) );
  and3_2 U1236 ( .ip1(n1218), .ip2(n1219), .ip3(n1220), .op(n1217) );
  nand2_2 U1237 ( .ip1(ep2_buf1[17]), .ip2(n311), .op(n1220) );
  nand2_2 U1238 ( .ip1(buf1[17]), .ip2(n280), .op(n1219) );
  nand2_2 U1239 ( .ip1(ep1_buf1[17]), .ip2(n302), .op(n1218) );
  nand2_2 U1245 ( .ip1(ep3_buf1[17]), .ip2(n293), .op(n1222) );
  nand2_2 U1246 ( .ip1(ep0_buf1[17]), .ip2(n325), .op(n1221) );
  and3_2 U1259 ( .ip1(n1240), .ip2(n1241), .ip3(n1242), .op(n1239) );
  nand2_2 U1260 ( .ip1(ep2_buf1[18]), .ip2(n311), .op(n1242) );
  nand2_2 U1261 ( .ip1(buf1[18]), .ip2(n280), .op(n1241) );
  nand2_2 U1262 ( .ip1(ep1_buf1[18]), .ip2(n302), .op(n1240) );
  nand2_2 U1268 ( .ip1(ep3_buf1[18]), .ip2(n293), .op(n1244) );
  nand2_2 U1269 ( .ip1(ep0_buf1[18]), .ip2(n325), .op(n1243) );
  and3_2 U1282 ( .ip1(n1262), .ip2(n1263), .ip3(n1264), .op(n1261) );
  nand2_2 U1283 ( .ip1(ep2_buf1[19]), .ip2(n311), .op(n1264) );
  nand2_2 U1284 ( .ip1(buf1[19]), .ip2(n280), .op(n1263) );
  nand2_2 U1285 ( .ip1(ep1_buf1[19]), .ip2(n302), .op(n1262) );
  nand2_2 U1291 ( .ip1(ep3_buf1[19]), .ip2(n293), .op(n1266) );
  nand2_2 U1292 ( .ip1(ep0_buf1[19]), .ip2(n325), .op(n1265) );
  and3_2 U1305 ( .ip1(n1284), .ip2(n1285), .ip3(n1286), .op(n1283) );
  nand2_2 U1306 ( .ip1(ep2_buf1[20]), .ip2(n311), .op(n1286) );
  nand2_2 U1307 ( .ip1(buf1[20]), .ip2(n280), .op(n1285) );
  nand2_2 U1308 ( .ip1(ep1_buf1[20]), .ip2(n302), .op(n1284) );
  nand2_2 U1314 ( .ip1(ep3_buf1[20]), .ip2(n293), .op(n1288) );
  nand2_2 U1315 ( .ip1(ep0_buf1[20]), .ip2(n325), .op(n1287) );
  and3_2 U1328 ( .ip1(n1306), .ip2(n1307), .ip3(n1308), .op(n1305) );
  nand2_2 U1329 ( .ip1(ep2_buf1[21]), .ip2(n311), .op(n1308) );
  nand2_2 U1330 ( .ip1(buf1[21]), .ip2(n280), .op(n1307) );
  nand2_2 U1331 ( .ip1(ep1_buf1[21]), .ip2(n302), .op(n1306) );
  nand2_2 U1337 ( .ip1(ep3_buf1[21]), .ip2(n293), .op(n1310) );
  nand2_2 U1338 ( .ip1(ep0_buf1[21]), .ip2(n325), .op(n1309) );
  and3_2 U1351 ( .ip1(n1328), .ip2(n1329), .ip3(n1330), .op(n1327) );
  nand2_2 U1352 ( .ip1(ep2_buf1[22]), .ip2(n311), .op(n1330) );
  nand2_2 U1353 ( .ip1(buf1[22]), .ip2(n280), .op(n1329) );
  nand2_2 U1354 ( .ip1(ep1_buf1[22]), .ip2(n302), .op(n1328) );
  nand2_2 U1360 ( .ip1(ep3_buf1[22]), .ip2(n293), .op(n1332) );
  nand2_2 U1361 ( .ip1(ep0_buf1[22]), .ip2(n325), .op(n1331) );
  and3_2 U1374 ( .ip1(n1350), .ip2(n1351), .ip3(n1352), .op(n1349) );
  nand2_2 U1375 ( .ip1(ep2_buf1[23]), .ip2(n310), .op(n1352) );
  nand2_2 U1376 ( .ip1(buf1[23]), .ip2(n280), .op(n1351) );
  nand2_2 U1377 ( .ip1(ep1_buf1[23]), .ip2(n301), .op(n1350) );
  nand2_2 U1383 ( .ip1(ep3_buf1[23]), .ip2(n292), .op(n1354) );
  nand2_2 U1384 ( .ip1(ep0_buf1[23]), .ip2(n325), .op(n1353) );
  and3_2 U1397 ( .ip1(n1372), .ip2(n1373), .ip3(n1374), .op(n1371) );
  nand2_2 U1398 ( .ip1(ep2_buf1[24]), .ip2(n310), .op(n1374) );
  nand2_2 U1399 ( .ip1(buf1[24]), .ip2(n281), .op(n1373) );
  nand2_2 U1400 ( .ip1(ep1_buf1[24]), .ip2(n301), .op(n1372) );
  nand2_2 U1406 ( .ip1(ep3_buf1[24]), .ip2(n292), .op(n1376) );
  nand2_2 U1407 ( .ip1(ep0_buf1[24]), .ip2(n325), .op(n1375) );
  and3_2 U1420 ( .ip1(n1394), .ip2(n1395), .ip3(n1396), .op(n1393) );
  nand2_2 U1421 ( .ip1(ep2_buf1[25]), .ip2(n310), .op(n1396) );
  nand2_2 U1422 ( .ip1(buf1[25]), .ip2(n281), .op(n1395) );
  nand2_2 U1423 ( .ip1(ep1_buf1[25]), .ip2(n301), .op(n1394) );
  nand2_2 U1429 ( .ip1(ep3_buf1[25]), .ip2(n292), .op(n1398) );
  nand2_2 U1430 ( .ip1(ep0_buf1[25]), .ip2(n325), .op(n1397) );
  and3_2 U1443 ( .ip1(n1416), .ip2(n1417), .ip3(n1418), .op(n1415) );
  nand2_2 U1444 ( .ip1(ep2_buf1[26]), .ip2(n310), .op(n1418) );
  nand2_2 U1445 ( .ip1(buf1[26]), .ip2(n281), .op(n1417) );
  nand2_2 U1446 ( .ip1(ep1_buf1[26]), .ip2(n301), .op(n1416) );
  nand2_2 U1452 ( .ip1(ep3_buf1[26]), .ip2(n292), .op(n1420) );
  nand2_2 U1453 ( .ip1(ep0_buf1[26]), .ip2(n325), .op(n1419) );
  and3_2 U1466 ( .ip1(n1438), .ip2(n1439), .ip3(n1440), .op(n1437) );
  nand2_2 U1467 ( .ip1(ep2_buf1[27]), .ip2(n310), .op(n1440) );
  nand2_2 U1468 ( .ip1(buf1[27]), .ip2(n281), .op(n1439) );
  nand2_2 U1469 ( .ip1(ep1_buf1[27]), .ip2(n301), .op(n1438) );
  nand2_2 U1475 ( .ip1(ep3_buf1[27]), .ip2(n292), .op(n1442) );
  nand2_2 U1476 ( .ip1(ep0_buf1[27]), .ip2(n325), .op(n1441) );
  and3_2 U1489 ( .ip1(n1460), .ip2(n1461), .ip3(n1462), .op(n1459) );
  nand2_2 U1490 ( .ip1(ep2_buf1[28]), .ip2(n310), .op(n1462) );
  nand2_2 U1491 ( .ip1(buf1[28]), .ip2(n281), .op(n1461) );
  nand2_2 U1492 ( .ip1(ep1_buf1[28]), .ip2(n301), .op(n1460) );
  nand2_2 U1498 ( .ip1(ep3_buf1[28]), .ip2(n292), .op(n1464) );
  nand2_2 U1499 ( .ip1(ep0_buf1[28]), .ip2(n325), .op(n1463) );
  and3_2 U1512 ( .ip1(n1482), .ip2(n1483), .ip3(n1484), .op(n1481) );
  nand2_2 U1513 ( .ip1(ep2_buf1[29]), .ip2(n310), .op(n1484) );
  nand2_2 U1514 ( .ip1(buf1[29]), .ip2(n281), .op(n1483) );
  nand2_2 U1515 ( .ip1(ep1_buf1[29]), .ip2(n301), .op(n1482) );
  nand2_2 U1521 ( .ip1(ep3_buf1[29]), .ip2(n292), .op(n1486) );
  nand2_2 U1522 ( .ip1(ep0_buf1[29]), .ip2(n324), .op(n1485) );
  and3_2 U1535 ( .ip1(n1504), .ip2(n1505), .ip3(n1506), .op(n1503) );
  nand2_2 U1536 ( .ip1(ep2_buf1[30]), .ip2(n310), .op(n1506) );
  nand2_2 U1537 ( .ip1(buf1[30]), .ip2(n281), .op(n1505) );
  nand2_2 U1538 ( .ip1(ep1_buf1[30]), .ip2(n301), .op(n1504) );
  nand2_2 U1544 ( .ip1(ep3_buf1[30]), .ip2(n292), .op(n1508) );
  nand2_2 U1545 ( .ip1(ep0_buf1[30]), .ip2(n324), .op(n1507) );
  and3_2 U1558 ( .ip1(n1526), .ip2(n1527), .ip3(n1528), .op(n1525) );
  nand2_2 U1559 ( .ip1(ep2_buf1[31]), .ip2(n310), .op(n1528) );
  nand2_2 U1560 ( .ip1(buf1[31]), .ip2(n281), .op(n1527) );
  nand2_2 U1561 ( .ip1(ep1_buf1[31]), .ip2(n301), .op(n1526) );
  nand2_2 U1567 ( .ip1(ep3_buf1[31]), .ip2(n292), .op(n1530) );
  nand2_2 U1568 ( .ip1(ep0_buf1[31]), .ip2(n324), .op(n1529) );
  and3_2 U1581 ( .ip1(n1548), .ip2(n1549), .ip3(n1550), .op(n1547) );
  nand2_2 U1582 ( .ip1(ep2_buf0[0]), .ip2(n310), .op(n1550) );
  nand2_2 U1583 ( .ip1(buf0[0]), .ip2(n281), .op(n1549) );
  nand2_2 U1584 ( .ip1(ep1_buf0[0]), .ip2(n301), .op(n1548) );
  nand2_2 U1590 ( .ip1(ep3_buf0[0]), .ip2(n292), .op(n1552) );
  nand2_2 U1591 ( .ip1(ep0_buf0[0]), .ip2(n324), .op(n1551) );
  and3_2 U1604 ( .ip1(n1570), .ip2(n1571), .ip3(n1572), .op(n1569) );
  nand2_2 U1605 ( .ip1(ep2_buf0[1]), .ip2(n310), .op(n1572) );
  nand2_2 U1606 ( .ip1(buf0[1]), .ip2(n281), .op(n1571) );
  nand2_2 U1607 ( .ip1(ep1_buf0[1]), .ip2(n301), .op(n1570) );
  nand2_2 U1613 ( .ip1(ep3_buf0[1]), .ip2(n292), .op(n1574) );
  nand2_2 U1614 ( .ip1(ep0_buf0[1]), .ip2(n324), .op(n1573) );
  and3_2 U1627 ( .ip1(n1592), .ip2(n1593), .ip3(n1594), .op(n1591) );
  nand2_2 U1628 ( .ip1(ep2_buf0[2]), .ip2(n310), .op(n1594) );
  nand2_2 U1629 ( .ip1(buf0[2]), .ip2(n281), .op(n1593) );
  nand2_2 U1630 ( .ip1(ep1_buf0[2]), .ip2(n301), .op(n1592) );
  nand2_2 U1636 ( .ip1(ep3_buf0[2]), .ip2(n292), .op(n1596) );
  nand2_2 U1637 ( .ip1(ep0_buf0[2]), .ip2(n324), .op(n1595) );
  and3_2 U1650 ( .ip1(n1614), .ip2(n1615), .ip3(n1616), .op(n1613) );
  nand2_2 U1651 ( .ip1(ep2_buf0[3]), .ip2(n309), .op(n1616) );
  nand2_2 U1652 ( .ip1(buf0[3]), .ip2(n281), .op(n1615) );
  nand2_2 U1653 ( .ip1(ep1_buf0[3]), .ip2(n300), .op(n1614) );
  nand2_2 U1659 ( .ip1(ep3_buf0[3]), .ip2(n291), .op(n1618) );
  nand2_2 U1660 ( .ip1(ep0_buf0[3]), .ip2(n324), .op(n1617) );
  and3_2 U1673 ( .ip1(n1636), .ip2(n1637), .ip3(n1638), .op(n1635) );
  nand2_2 U1674 ( .ip1(ep2_buf0[4]), .ip2(n309), .op(n1638) );
  nand2_2 U1675 ( .ip1(buf0[4]), .ip2(n282), .op(n1637) );
  nand2_2 U1676 ( .ip1(ep1_buf0[4]), .ip2(n300), .op(n1636) );
  nand2_2 U1682 ( .ip1(ep3_buf0[4]), .ip2(n291), .op(n1640) );
  nand2_2 U1683 ( .ip1(ep0_buf0[4]), .ip2(n324), .op(n1639) );
  and3_2 U1696 ( .ip1(n1658), .ip2(n1659), .ip3(n1660), .op(n1657) );
  nand2_2 U1697 ( .ip1(ep2_buf0[5]), .ip2(n309), .op(n1660) );
  nand2_2 U1698 ( .ip1(buf0[5]), .ip2(n282), .op(n1659) );
  nand2_2 U1699 ( .ip1(ep1_buf0[5]), .ip2(n300), .op(n1658) );
  nand2_2 U1705 ( .ip1(ep3_buf0[5]), .ip2(n291), .op(n1662) );
  nand2_2 U1706 ( .ip1(ep0_buf0[5]), .ip2(n324), .op(n1661) );
  and3_2 U1719 ( .ip1(n1680), .ip2(n1681), .ip3(n1682), .op(n1679) );
  nand2_2 U1720 ( .ip1(ep2_buf0[6]), .ip2(n309), .op(n1682) );
  nand2_2 U1721 ( .ip1(buf0[6]), .ip2(n282), .op(n1681) );
  nand2_2 U1722 ( .ip1(ep1_buf0[6]), .ip2(n300), .op(n1680) );
  nand2_2 U1728 ( .ip1(ep3_buf0[6]), .ip2(n291), .op(n1684) );
  nand2_2 U1729 ( .ip1(ep0_buf0[6]), .ip2(n324), .op(n1683) );
  and3_2 U1742 ( .ip1(n1702), .ip2(n1703), .ip3(n1704), .op(n1701) );
  nand2_2 U1743 ( .ip1(ep2_buf0[7]), .ip2(n309), .op(n1704) );
  nand2_2 U1744 ( .ip1(buf0[7]), .ip2(n282), .op(n1703) );
  nand2_2 U1745 ( .ip1(ep1_buf0[7]), .ip2(n300), .op(n1702) );
  nand2_2 U1751 ( .ip1(ep3_buf0[7]), .ip2(n291), .op(n1706) );
  nand2_2 U1752 ( .ip1(ep0_buf0[7]), .ip2(n324), .op(n1705) );
  and3_2 U1765 ( .ip1(n1724), .ip2(n1725), .ip3(n1726), .op(n1723) );
  nand2_2 U1766 ( .ip1(ep2_buf0[8]), .ip2(n309), .op(n1726) );
  nand2_2 U1767 ( .ip1(buf0[8]), .ip2(n282), .op(n1725) );
  nand2_2 U1768 ( .ip1(ep1_buf0[8]), .ip2(n300), .op(n1724) );
  nand2_2 U1774 ( .ip1(ep3_buf0[8]), .ip2(n291), .op(n1728) );
  nand2_2 U1775 ( .ip1(ep0_buf0[8]), .ip2(n324), .op(n1727) );
  and3_2 U1788 ( .ip1(n1746), .ip2(n1747), .ip3(n1748), .op(n1745) );
  nand2_2 U1789 ( .ip1(ep2_buf0[9]), .ip2(n309), .op(n1748) );
  nand2_2 U1790 ( .ip1(buf0[9]), .ip2(n282), .op(n1747) );
  nand2_2 U1791 ( .ip1(ep1_buf0[9]), .ip2(n300), .op(n1746) );
  nand2_2 U1797 ( .ip1(ep3_buf0[9]), .ip2(n291), .op(n1750) );
  nand2_2 U1798 ( .ip1(ep0_buf0[9]), .ip2(n324), .op(n1749) );
  and3_2 U1811 ( .ip1(n1768), .ip2(n1769), .ip3(n1770), .op(n1767) );
  nand2_2 U1812 ( .ip1(ep2_buf0[10]), .ip2(n309), .op(n1770) );
  nand2_2 U1813 ( .ip1(buf0[10]), .ip2(n282), .op(n1769) );
  nand2_2 U1814 ( .ip1(ep1_buf0[10]), .ip2(n300), .op(n1768) );
  nand2_2 U1820 ( .ip1(ep3_buf0[10]), .ip2(n291), .op(n1772) );
  nand2_2 U1821 ( .ip1(ep0_buf0[10]), .ip2(n324), .op(n1771) );
  and3_2 U1834 ( .ip1(n1790), .ip2(n1791), .ip3(n1792), .op(n1789) );
  nand2_2 U1835 ( .ip1(ep2_buf0[11]), .ip2(n309), .op(n1792) );
  nand2_2 U1836 ( .ip1(buf0[11]), .ip2(n282), .op(n1791) );
  nand2_2 U1837 ( .ip1(ep1_buf0[11]), .ip2(n300), .op(n1790) );
  nand2_2 U1843 ( .ip1(ep3_buf0[11]), .ip2(n291), .op(n1794) );
  nand2_2 U1844 ( .ip1(ep0_buf0[11]), .ip2(n323), .op(n1793) );
  and3_2 U1857 ( .ip1(n1812), .ip2(n1813), .ip3(n1814), .op(n1811) );
  nand2_2 U1858 ( .ip1(ep2_buf0[12]), .ip2(n309), .op(n1814) );
  nand2_2 U1859 ( .ip1(buf0[12]), .ip2(n282), .op(n1813) );
  nand2_2 U1860 ( .ip1(ep1_buf0[12]), .ip2(n300), .op(n1812) );
  nand2_2 U1866 ( .ip1(ep3_buf0[12]), .ip2(n291), .op(n1816) );
  nand2_2 U1867 ( .ip1(ep0_buf0[12]), .ip2(n323), .op(n1815) );
  and3_2 U1880 ( .ip1(n1834), .ip2(n1835), .ip3(n1836), .op(n1833) );
  nand2_2 U1881 ( .ip1(ep2_buf0[13]), .ip2(n309), .op(n1836) );
  nand2_2 U1882 ( .ip1(buf0[13]), .ip2(n282), .op(n1835) );
  nand2_2 U1883 ( .ip1(ep1_buf0[13]), .ip2(n300), .op(n1834) );
  nand2_2 U1889 ( .ip1(ep3_buf0[13]), .ip2(n291), .op(n1838) );
  nand2_2 U1890 ( .ip1(ep0_buf0[13]), .ip2(n323), .op(n1837) );
  and3_2 U1903 ( .ip1(n1856), .ip2(n1857), .ip3(n1858), .op(n1855) );
  nand2_2 U1904 ( .ip1(ep2_buf0[14]), .ip2(n309), .op(n1858) );
  nand2_2 U1905 ( .ip1(buf0[14]), .ip2(n282), .op(n1857) );
  nand2_2 U1906 ( .ip1(ep1_buf0[14]), .ip2(n300), .op(n1856) );
  nand2_2 U1912 ( .ip1(ep3_buf0[14]), .ip2(n291), .op(n1860) );
  nand2_2 U1913 ( .ip1(ep0_buf0[14]), .ip2(n323), .op(n1859) );
  and3_2 U1926 ( .ip1(n1878), .ip2(n1879), .ip3(n1880), .op(n1877) );
  nand2_2 U1927 ( .ip1(ep2_buf0[15]), .ip2(n308), .op(n1880) );
  nand2_2 U1928 ( .ip1(buf0[15]), .ip2(n282), .op(n1879) );
  nand2_2 U1929 ( .ip1(ep1_buf0[15]), .ip2(n299), .op(n1878) );
  nand2_2 U1935 ( .ip1(ep3_buf0[15]), .ip2(n290), .op(n1882) );
  nand2_2 U1936 ( .ip1(ep0_buf0[15]), .ip2(n323), .op(n1881) );
  and3_2 U1949 ( .ip1(n1900), .ip2(n1901), .ip3(n1902), .op(n1899) );
  nand2_2 U1950 ( .ip1(ep2_buf0[16]), .ip2(n308), .op(n1902) );
  nand2_2 U1951 ( .ip1(buf0[16]), .ip2(n283), .op(n1901) );
  nand2_2 U1952 ( .ip1(ep1_buf0[16]), .ip2(n299), .op(n1900) );
  nand2_2 U1958 ( .ip1(ep3_buf0[16]), .ip2(n290), .op(n1904) );
  nand2_2 U1959 ( .ip1(ep0_buf0[16]), .ip2(n323), .op(n1903) );
  and3_2 U1972 ( .ip1(n1922), .ip2(n1923), .ip3(n1924), .op(n1921) );
  nand2_2 U1973 ( .ip1(ep2_buf0[17]), .ip2(n308), .op(n1924) );
  nand2_2 U1974 ( .ip1(buf0[17]), .ip2(n283), .op(n1923) );
  nand2_2 U1975 ( .ip1(ep1_buf0[17]), .ip2(n299), .op(n1922) );
  nand2_2 U1981 ( .ip1(ep3_buf0[17]), .ip2(n290), .op(n1926) );
  nand2_2 U1982 ( .ip1(ep0_buf0[17]), .ip2(n323), .op(n1925) );
  and3_2 U1995 ( .ip1(n1944), .ip2(n1945), .ip3(n1946), .op(n1943) );
  nand2_2 U1996 ( .ip1(ep2_buf0[18]), .ip2(n308), .op(n1946) );
  nand2_2 U1997 ( .ip1(buf0[18]), .ip2(n283), .op(n1945) );
  nand2_2 U1998 ( .ip1(ep1_buf0[18]), .ip2(n299), .op(n1944) );
  nand2_2 U2004 ( .ip1(ep3_buf0[18]), .ip2(n290), .op(n1948) );
  nand2_2 U2005 ( .ip1(ep0_buf0[18]), .ip2(n323), .op(n1947) );
  and3_2 U2018 ( .ip1(n1966), .ip2(n1967), .ip3(n1968), .op(n1965) );
  nand2_2 U2019 ( .ip1(ep2_buf0[19]), .ip2(n308), .op(n1968) );
  nand2_2 U2020 ( .ip1(buf0[19]), .ip2(n283), .op(n1967) );
  nand2_2 U2021 ( .ip1(ep1_buf0[19]), .ip2(n299), .op(n1966) );
  nand2_2 U2027 ( .ip1(ep3_buf0[19]), .ip2(n290), .op(n1970) );
  nand2_2 U2028 ( .ip1(ep0_buf0[19]), .ip2(n323), .op(n1969) );
  and3_2 U2041 ( .ip1(n1988), .ip2(n1989), .ip3(n1990), .op(n1987) );
  nand2_2 U2042 ( .ip1(ep2_buf0[20]), .ip2(n308), .op(n1990) );
  nand2_2 U2043 ( .ip1(buf0[20]), .ip2(n283), .op(n1989) );
  nand2_2 U2044 ( .ip1(ep1_buf0[20]), .ip2(n299), .op(n1988) );
  nand2_2 U2050 ( .ip1(ep3_buf0[20]), .ip2(n290), .op(n1992) );
  nand2_2 U2051 ( .ip1(ep0_buf0[20]), .ip2(n323), .op(n1991) );
  and3_2 U2064 ( .ip1(n2010), .ip2(n2011), .ip3(n2012), .op(n2009) );
  nand2_2 U2065 ( .ip1(ep2_buf0[21]), .ip2(n308), .op(n2012) );
  nand2_2 U2066 ( .ip1(buf0[21]), .ip2(n283), .op(n2011) );
  nand2_2 U2067 ( .ip1(ep1_buf0[21]), .ip2(n299), .op(n2010) );
  nand2_2 U2073 ( .ip1(ep3_buf0[21]), .ip2(n290), .op(n2014) );
  nand2_2 U2074 ( .ip1(ep0_buf0[21]), .ip2(n323), .op(n2013) );
  and3_2 U2087 ( .ip1(n2032), .ip2(n2033), .ip3(n2034), .op(n2031) );
  nand2_2 U2088 ( .ip1(ep2_buf0[22]), .ip2(n308), .op(n2034) );
  nand2_2 U2089 ( .ip1(buf0[22]), .ip2(n283), .op(n2033) );
  nand2_2 U2090 ( .ip1(ep1_buf0[22]), .ip2(n299), .op(n2032) );
  nand2_2 U2096 ( .ip1(ep3_buf0[22]), .ip2(n290), .op(n2036) );
  nand2_2 U2097 ( .ip1(ep0_buf0[22]), .ip2(n323), .op(n2035) );
  and3_2 U2110 ( .ip1(n2054), .ip2(n2055), .ip3(n2056), .op(n2053) );
  nand2_2 U2111 ( .ip1(ep2_buf0[23]), .ip2(n308), .op(n2056) );
  nand2_2 U2112 ( .ip1(buf0[23]), .ip2(n283), .op(n2055) );
  nand2_2 U2113 ( .ip1(ep1_buf0[23]), .ip2(n299), .op(n2054) );
  nand2_2 U2119 ( .ip1(ep3_buf0[23]), .ip2(n290), .op(n2058) );
  nand2_2 U2120 ( .ip1(ep0_buf0[23]), .ip2(n323), .op(n2057) );
  and3_2 U2133 ( .ip1(n2076), .ip2(n2077), .ip3(n2078), .op(n2075) );
  nand2_2 U2134 ( .ip1(ep2_buf0[24]), .ip2(n308), .op(n2078) );
  nand2_2 U2135 ( .ip1(buf0[24]), .ip2(n283), .op(n2077) );
  nand2_2 U2136 ( .ip1(ep1_buf0[24]), .ip2(n299), .op(n2076) );
  nand2_2 U2142 ( .ip1(ep3_buf0[24]), .ip2(n290), .op(n2080) );
  nand2_2 U2143 ( .ip1(ep0_buf0[24]), .ip2(n322), .op(n2079) );
  and3_2 U2156 ( .ip1(n2098), .ip2(n2099), .ip3(n2100), .op(n2097) );
  nand2_2 U2157 ( .ip1(ep2_buf0[25]), .ip2(n308), .op(n2100) );
  nand2_2 U2158 ( .ip1(buf0[25]), .ip2(n283), .op(n2099) );
  nand2_2 U2159 ( .ip1(ep1_buf0[25]), .ip2(n299), .op(n2098) );
  nand2_2 U2165 ( .ip1(ep3_buf0[25]), .ip2(n290), .op(n2102) );
  nand2_2 U2166 ( .ip1(ep0_buf0[25]), .ip2(n322), .op(n2101) );
  and3_2 U2179 ( .ip1(n2120), .ip2(n2121), .ip3(n2122), .op(n2119) );
  nand2_2 U2180 ( .ip1(ep2_buf0[26]), .ip2(n308), .op(n2122) );
  nand2_2 U2181 ( .ip1(buf0[26]), .ip2(n283), .op(n2121) );
  nand2_2 U2182 ( .ip1(ep1_buf0[26]), .ip2(n299), .op(n2120) );
  nand2_2 U2188 ( .ip1(ep3_buf0[26]), .ip2(n290), .op(n2124) );
  nand2_2 U2189 ( .ip1(ep0_buf0[26]), .ip2(n322), .op(n2123) );
  and3_2 U2202 ( .ip1(n2142), .ip2(n2143), .ip3(n2144), .op(n2141) );
  nand2_2 U2203 ( .ip1(ep2_buf0[27]), .ip2(n307), .op(n2144) );
  nand2_2 U2204 ( .ip1(buf0[27]), .ip2(n283), .op(n2143) );
  nand2_2 U2205 ( .ip1(ep1_buf0[27]), .ip2(n298), .op(n2142) );
  nand2_2 U2211 ( .ip1(ep3_buf0[27]), .ip2(n289), .op(n2146) );
  nand2_2 U2212 ( .ip1(ep0_buf0[27]), .ip2(n322), .op(n2145) );
  and3_2 U2225 ( .ip1(n2164), .ip2(n2165), .ip3(n2166), .op(n2163) );
  nand2_2 U2226 ( .ip1(ep2_buf0[28]), .ip2(n307), .op(n2166) );
  nand2_2 U2227 ( .ip1(buf0[28]), .ip2(n284), .op(n2165) );
  nand2_2 U2228 ( .ip1(ep1_buf0[28]), .ip2(n298), .op(n2164) );
  nand2_2 U2234 ( .ip1(ep3_buf0[28]), .ip2(n289), .op(n2168) );
  nand2_2 U2235 ( .ip1(ep0_buf0[28]), .ip2(n322), .op(n2167) );
  and3_2 U2248 ( .ip1(n2186), .ip2(n2187), .ip3(n2188), .op(n2185) );
  nand2_2 U2249 ( .ip1(ep2_buf0[29]), .ip2(n307), .op(n2188) );
  nand2_2 U2250 ( .ip1(buf0[29]), .ip2(n284), .op(n2187) );
  nand2_2 U2251 ( .ip1(ep1_buf0[29]), .ip2(n298), .op(n2186) );
  nand2_2 U2257 ( .ip1(ep3_buf0[29]), .ip2(n289), .op(n2190) );
  nand2_2 U2258 ( .ip1(ep0_buf0[29]), .ip2(n322), .op(n2189) );
  and3_2 U2271 ( .ip1(n2208), .ip2(n2209), .ip3(n2210), .op(n2207) );
  nand2_2 U2272 ( .ip1(ep2_buf0[30]), .ip2(n307), .op(n2210) );
  nand2_2 U2273 ( .ip1(buf0[30]), .ip2(n284), .op(n2209) );
  nand2_2 U2274 ( .ip1(ep1_buf0[30]), .ip2(n298), .op(n2208) );
  nand2_2 U2280 ( .ip1(ep3_buf0[30]), .ip2(n289), .op(n2212) );
  nand2_2 U2281 ( .ip1(ep0_buf0[30]), .ip2(n322), .op(n2211) );
  and3_2 U2294 ( .ip1(n2230), .ip2(n2231), .ip3(n2232), .op(n2229) );
  nand2_2 U2295 ( .ip1(ep2_buf0[31]), .ip2(n307), .op(n2232) );
  nand2_2 U2296 ( .ip1(buf0[31]), .ip2(n284), .op(n2231) );
  nand2_2 U2297 ( .ip1(ep1_buf0[31]), .ip2(n298), .op(n2230) );
  nand2_2 U2303 ( .ip1(ep3_buf0[31]), .ip2(n289), .op(n2234) );
  nand2_2 U2304 ( .ip1(ep0_buf0[31]), .ip2(n322), .op(n2233) );
  and3_2 U2317 ( .ip1(n2252), .ip2(n2253), .ip3(n2254), .op(n2251) );
  nand2_2 U2318 ( .ip1(ep2_csr[0]), .ip2(n307), .op(n2254) );
  nand2_2 U2319 ( .ip1(csr[0]), .ip2(n284), .op(n2253) );
  nand2_2 U2320 ( .ip1(ep1_csr[0]), .ip2(n298), .op(n2252) );
  nand2_2 U2326 ( .ip1(ep3_csr[0]), .ip2(n289), .op(n2256) );
  nand2_2 U2327 ( .ip1(ep0_csr[0]), .ip2(n322), .op(n2255) );
  and3_2 U2340 ( .ip1(n2274), .ip2(n2275), .ip3(n2276), .op(n2273) );
  nand2_2 U2341 ( .ip1(ep2_csr[1]), .ip2(n307), .op(n2276) );
  nand2_2 U2342 ( .ip1(csr[1]), .ip2(n284), .op(n2275) );
  nand2_2 U2343 ( .ip1(ep1_csr[1]), .ip2(n298), .op(n2274) );
  nand2_2 U2349 ( .ip1(ep3_csr[1]), .ip2(n289), .op(n2278) );
  nand2_2 U2350 ( .ip1(ep0_csr[1]), .ip2(n322), .op(n2277) );
  and3_2 U2363 ( .ip1(n2296), .ip2(n2297), .ip3(n2298), .op(n2295) );
  nand2_2 U2364 ( .ip1(ep2_csr[2]), .ip2(n307), .op(n2298) );
  nand2_2 U2365 ( .ip1(csr[2]), .ip2(n284), .op(n2297) );
  nand2_2 U2366 ( .ip1(ep1_csr[2]), .ip2(n298), .op(n2296) );
  nand2_2 U2372 ( .ip1(ep3_csr[2]), .ip2(n289), .op(n2300) );
  nand2_2 U2373 ( .ip1(ep0_csr[2]), .ip2(n322), .op(n2299) );
  and3_2 U2386 ( .ip1(n2318), .ip2(n2319), .ip3(n2320), .op(n2317) );
  nand2_2 U2387 ( .ip1(ep2_csr[3]), .ip2(n307), .op(n2320) );
  nand2_2 U2388 ( .ip1(csr[3]), .ip2(n284), .op(n2319) );
  nand2_2 U2389 ( .ip1(ep1_csr[3]), .ip2(n298), .op(n2318) );
  nand2_2 U2395 ( .ip1(ep3_csr[3]), .ip2(n289), .op(n2322) );
  nand2_2 U2396 ( .ip1(ep0_csr[3]), .ip2(n322), .op(n2321) );
  and3_2 U2409 ( .ip1(n2340), .ip2(n2341), .ip3(n2342), .op(n2339) );
  nand2_2 U2410 ( .ip1(ep2_csr[4]), .ip2(n307), .op(n2342) );
  nand2_2 U2411 ( .ip1(csr[4]), .ip2(n284), .op(n2341) );
  nand2_2 U2412 ( .ip1(ep1_csr[4]), .ip2(n298), .op(n2340) );
  nand2_2 U2418 ( .ip1(ep3_csr[4]), .ip2(n289), .op(n2344) );
  nand2_2 U2419 ( .ip1(ep0_csr[4]), .ip2(n322), .op(n2343) );
  and3_2 U2432 ( .ip1(n2362), .ip2(n2363), .ip3(n2364), .op(n2361) );
  nand2_2 U2433 ( .ip1(ep2_csr[5]), .ip2(n307), .op(n2364) );
  nand2_2 U2434 ( .ip1(csr[5]), .ip2(n284), .op(n2363) );
  nand2_2 U2435 ( .ip1(ep1_csr[5]), .ip2(n298), .op(n2362) );
  nand2_2 U2441 ( .ip1(ep3_csr[5]), .ip2(n289), .op(n2366) );
  nand2_2 U2442 ( .ip1(ep0_csr[5]), .ip2(n322), .op(n2365) );
  and3_2 U2455 ( .ip1(n2384), .ip2(n2385), .ip3(n2386), .op(n2383) );
  nand2_2 U2456 ( .ip1(ep2_csr[6]), .ip2(n307), .op(n2386) );
  nand2_2 U2457 ( .ip1(csr[6]), .ip2(n284), .op(n2385) );
  nand2_2 U2458 ( .ip1(ep1_csr[6]), .ip2(n298), .op(n2384) );
  nand2_2 U2464 ( .ip1(ep3_csr[6]), .ip2(n289), .op(n2388) );
  nand2_2 U2465 ( .ip1(ep0_csr[6]), .ip2(n321), .op(n2387) );
  and3_2 U2478 ( .ip1(n2406), .ip2(n2407), .ip3(n2408), .op(n2405) );
  nand2_2 U2479 ( .ip1(ep2_csr[7]), .ip2(n306), .op(n2408) );
  nand2_2 U2480 ( .ip1(csr[7]), .ip2(n284), .op(n2407) );
  nand2_2 U2481 ( .ip1(ep1_csr[7]), .ip2(n297), .op(n2406) );
  nand2_2 U2487 ( .ip1(ep3_csr[7]), .ip2(n288), .op(n2410) );
  nand2_2 U2488 ( .ip1(ep0_csr[7]), .ip2(n321), .op(n2409) );
  and3_2 U2501 ( .ip1(n2428), .ip2(n2429), .ip3(n2430), .op(n2427) );
  nand2_2 U2502 ( .ip1(ep2_csr[8]), .ip2(n306), .op(n2430) );
  nand2_2 U2503 ( .ip1(csr[8]), .ip2(n285), .op(n2429) );
  nand2_2 U2504 ( .ip1(ep1_csr[8]), .ip2(n297), .op(n2428) );
  nand2_2 U2510 ( .ip1(ep3_csr[8]), .ip2(n288), .op(n2432) );
  nand2_2 U2511 ( .ip1(ep0_csr[8]), .ip2(n321), .op(n2431) );
  and3_2 U2524 ( .ip1(n2450), .ip2(n2451), .ip3(n2452), .op(n2449) );
  nand2_2 U2525 ( .ip1(ep2_csr[9]), .ip2(n306), .op(n2452) );
  nand2_2 U2526 ( .ip1(csr[9]), .ip2(n285), .op(n2451) );
  nand2_2 U2527 ( .ip1(ep1_csr[9]), .ip2(n297), .op(n2450) );
  nand2_2 U2533 ( .ip1(ep3_csr[9]), .ip2(n288), .op(n2454) );
  nand2_2 U2534 ( .ip1(ep0_csr[9]), .ip2(n323), .op(n2453) );
  and3_2 U2547 ( .ip1(n2472), .ip2(n2473), .ip3(n2474), .op(n2471) );
  nand2_2 U2548 ( .ip1(ep2_csr[10]), .ip2(n306), .op(n2474) );
  nand2_2 U2549 ( .ip1(csr[10]), .ip2(n285), .op(n2473) );
  nand2_2 U2550 ( .ip1(ep1_csr[10]), .ip2(n297), .op(n2472) );
  nand2_2 U2556 ( .ip1(ep3_csr[10]), .ip2(n288), .op(n2476) );
  nand2_2 U2557 ( .ip1(ep0_csr[10]), .ip2(n321), .op(n2475) );
  and3_2 U2570 ( .ip1(n2494), .ip2(n2495), .ip3(n2496), .op(n2493) );
  nand2_2 U2571 ( .ip1(ep2_csr[11]), .ip2(n306), .op(n2496) );
  nand2_2 U2572 ( .ip1(csr[11]), .ip2(n285), .op(n2495) );
  nand2_2 U2573 ( .ip1(ep1_csr[11]), .ip2(n297), .op(n2494) );
  nand2_2 U2579 ( .ip1(ep3_csr[11]), .ip2(n288), .op(n2498) );
  nand2_2 U2580 ( .ip1(ep0_csr[11]), .ip2(n321), .op(n2497) );
  and3_2 U2593 ( .ip1(n2516), .ip2(n2517), .ip3(n2518), .op(n2515) );
  nand2_2 U2594 ( .ip1(ep2_csr[12]), .ip2(n306), .op(n2518) );
  nand2_2 U2595 ( .ip1(csr[12]), .ip2(n285), .op(n2517) );
  nand2_2 U2596 ( .ip1(ep1_csr[12]), .ip2(n297), .op(n2516) );
  nand2_2 U2602 ( .ip1(ep3_csr[12]), .ip2(n288), .op(n2520) );
  nand2_2 U2603 ( .ip1(ep0_csr[12]), .ip2(n321), .op(n2519) );
  and3_2 U2616 ( .ip1(n2538), .ip2(n2539), .ip3(n2540), .op(n2537) );
  nand2_2 U2617 ( .ip1(ep2_csr[13]), .ip2(n306), .op(n2540) );
  nand2_2 U2618 ( .ip1(csr[13]), .ip2(n285), .op(n2539) );
  nand2_2 U2619 ( .ip1(ep1_csr[13]), .ip2(n297), .op(n2538) );
  nand2_2 U2625 ( .ip1(ep3_csr[13]), .ip2(n288), .op(n2542) );
  nand2_2 U2626 ( .ip1(ep0_csr[13]), .ip2(n321), .op(n2541) );
  and3_2 U2662 ( .ip1(n2582), .ip2(n2583), .ip3(n2584), .op(n2581) );
  nand2_2 U2663 ( .ip1(ep2_csr[15]), .ip2(n306), .op(n2584) );
  nand2_2 U2664 ( .ip1(csr[15]), .ip2(n285), .op(n2583) );
  nand2_2 U2665 ( .ip1(ep1_csr[15]), .ip2(n297), .op(n2582) );
  nand2_2 U2671 ( .ip1(ep3_csr[15]), .ip2(n288), .op(n2586) );
  nand2_2 U2672 ( .ip1(ep0_csr[15]), .ip2(n321), .op(n2585) );
  and3_2 U2685 ( .ip1(n2604), .ip2(n2605), .ip3(n2606), .op(n2603) );
  nand2_2 U2686 ( .ip1(ep2_csr[16]), .ip2(n306), .op(n2606) );
  nand2_2 U2687 ( .ip1(csr[16]), .ip2(n285), .op(n2605) );
  nand2_2 U2688 ( .ip1(ep1_csr[16]), .ip2(n297), .op(n2604) );
  nand2_2 U2694 ( .ip1(ep3_csr[16]), .ip2(n288), .op(n2608) );
  nand2_2 U2695 ( .ip1(ep0_csr[16]), .ip2(n321), .op(n2607) );
  and3_2 U2708 ( .ip1(n2626), .ip2(n2627), .ip3(n2628), .op(n2625) );
  nand2_2 U2709 ( .ip1(ep2_csr[17]), .ip2(n306), .op(n2628) );
  nand2_2 U2710 ( .ip1(csr[17]), .ip2(n285), .op(n2627) );
  nand2_2 U2711 ( .ip1(ep1_csr[17]), .ip2(n297), .op(n2626) );
  nand2_2 U2717 ( .ip1(ep3_csr[17]), .ip2(n288), .op(n2630) );
  nand2_2 U2718 ( .ip1(ep0_csr[17]), .ip2(n321), .op(n2629) );
  and3_2 U2731 ( .ip1(n2648), .ip2(n2649), .ip3(n2650), .op(n2647) );
  nand2_2 U2732 ( .ip1(ep2_csr[18]), .ip2(n306), .op(n2650) );
  nand2_2 U2733 ( .ip1(csr[18]), .ip2(n285), .op(n2649) );
  nand2_2 U2734 ( .ip1(ep1_csr[18]), .ip2(n297), .op(n2648) );
  nand2_2 U2740 ( .ip1(ep3_csr[18]), .ip2(n288), .op(n2652) );
  nand2_2 U2741 ( .ip1(ep0_csr[18]), .ip2(n321), .op(n2651) );
  and3_2 U2754 ( .ip1(n2670), .ip2(n2671), .ip3(n2672), .op(n2669) );
  nand2_2 U2755 ( .ip1(ep2_csr[19]), .ip2(n306), .op(n2672) );
  nand2_2 U2756 ( .ip1(csr[19]), .ip2(n285), .op(n2671) );
  nand2_2 U2757 ( .ip1(ep1_csr[19]), .ip2(n297), .op(n2670) );
  nand2_2 U2763 ( .ip1(ep3_csr[19]), .ip2(n288), .op(n2674) );
  nand2_2 U2764 ( .ip1(ep0_csr[19]), .ip2(n321), .op(n2673) );
  and3_2 U2777 ( .ip1(n2692), .ip2(n2693), .ip3(n2694), .op(n2691) );
  nand2_2 U2778 ( .ip1(ep2_csr[20]), .ip2(n305), .op(n2694) );
  nand2_2 U2779 ( .ip1(csr[20]), .ip2(n286), .op(n2693) );
  nand2_2 U2780 ( .ip1(ep1_csr[20]), .ip2(n296), .op(n2692) );
  nand2_2 U2786 ( .ip1(ep3_csr[20]), .ip2(n287), .op(n2696) );
  nand2_2 U2787 ( .ip1(ep0_csr[20]), .ip2(n321), .op(n2695) );
  and3_2 U2800 ( .ip1(n2714), .ip2(n2715), .ip3(n2716), .op(n2713) );
  nand2_2 U2801 ( .ip1(ep2_csr[21]), .ip2(n305), .op(n2716) );
  nand2_2 U2802 ( .ip1(csr[21]), .ip2(n286), .op(n2715) );
  nand2_2 U2803 ( .ip1(ep1_csr[21]), .ip2(n296), .op(n2714) );
  nand2_2 U2809 ( .ip1(ep3_csr[21]), .ip2(n287), .op(n2718) );
  nand2_2 U2810 ( .ip1(ep0_csr[21]), .ip2(n321), .op(n2717) );
  and3_2 U2823 ( .ip1(n2736), .ip2(n2737), .ip3(n2738), .op(n2735) );
  nand2_2 U2824 ( .ip1(ep2_csr[22]), .ip2(n305), .op(n2738) );
  nand2_2 U2825 ( .ip1(csr[22]), .ip2(n286), .op(n2737) );
  nand2_2 U2826 ( .ip1(ep1_csr[22]), .ip2(n296), .op(n2736) );
  nand2_2 U2832 ( .ip1(ep3_csr[22]), .ip2(n287), .op(n2740) );
  nand2_2 U2833 ( .ip1(ep0_csr[22]), .ip2(n320), .op(n2739) );
  and3_2 U2846 ( .ip1(n2758), .ip2(n2759), .ip3(n2760), .op(n2757) );
  nand2_2 U2847 ( .ip1(ep2_csr[23]), .ip2(n305), .op(n2760) );
  nand2_2 U2848 ( .ip1(csr[23]), .ip2(n286), .op(n2759) );
  nand2_2 U2849 ( .ip1(ep1_csr[23]), .ip2(n296), .op(n2758) );
  nand2_2 U2855 ( .ip1(ep3_csr[23]), .ip2(n287), .op(n2762) );
  nand2_2 U2856 ( .ip1(ep0_csr[23]), .ip2(n320), .op(n2761) );
  and3_2 U2869 ( .ip1(n2780), .ip2(n2781), .ip3(n2782), .op(n2779) );
  nand2_2 U2870 ( .ip1(ep2_csr[24]), .ip2(n305), .op(n2782) );
  nand2_2 U2871 ( .ip1(csr[24]), .ip2(n286), .op(n2781) );
  nand2_2 U2872 ( .ip1(ep1_csr[24]), .ip2(n296), .op(n2780) );
  nand2_2 U2878 ( .ip1(ep3_csr[24]), .ip2(n287), .op(n2784) );
  nand2_2 U2879 ( .ip1(ep0_csr[24]), .ip2(n320), .op(n2783) );
  and3_2 U2892 ( .ip1(n2802), .ip2(n2803), .ip3(n2804), .op(n2801) );
  nand2_2 U2893 ( .ip1(ep2_csr[25]), .ip2(n305), .op(n2804) );
  nand2_2 U2894 ( .ip1(csr[25]), .ip2(n286), .op(n2803) );
  nand2_2 U2895 ( .ip1(ep1_csr[25]), .ip2(n296), .op(n2802) );
  nand2_2 U2901 ( .ip1(ep3_csr[25]), .ip2(n287), .op(n2806) );
  nand2_2 U2902 ( .ip1(ep0_csr[25]), .ip2(n320), .op(n2805) );
  and3_2 U2915 ( .ip1(n2824), .ip2(n2825), .ip3(n2826), .op(n2823) );
  nand2_2 U2916 ( .ip1(ep2_csr[26]), .ip2(n305), .op(n2826) );
  nand2_2 U2917 ( .ip1(csr[26]), .ip2(n286), .op(n2825) );
  nand2_2 U2918 ( .ip1(ep1_csr[26]), .ip2(n296), .op(n2824) );
  nand2_2 U2924 ( .ip1(ep3_csr[26]), .ip2(n287), .op(n2828) );
  nand2_2 U2925 ( .ip1(ep0_csr[26]), .ip2(n320), .op(n2827) );
  and3_2 U2938 ( .ip1(n2846), .ip2(n2847), .ip3(n2848), .op(n2845) );
  nand2_2 U2939 ( .ip1(ep2_csr[27]), .ip2(n305), .op(n2848) );
  nand2_2 U2940 ( .ip1(csr[27]), .ip2(n286), .op(n2847) );
  nand2_2 U2941 ( .ip1(ep1_csr[27]), .ip2(n296), .op(n2846) );
  nand2_2 U2947 ( .ip1(ep3_csr[27]), .ip2(n287), .op(n2850) );
  nand2_2 U2948 ( .ip1(ep0_csr[27]), .ip2(n320), .op(n2849) );
  and3_2 U2961 ( .ip1(n2868), .ip2(n2869), .ip3(n2870), .op(n2867) );
  nand2_2 U2962 ( .ip1(ep2_csr[28]), .ip2(n305), .op(n2870) );
  nand2_2 U2963 ( .ip1(csr[28]), .ip2(n286), .op(n2869) );
  nand2_2 U2964 ( .ip1(ep1_csr[28]), .ip2(n296), .op(n2868) );
  nand2_2 U2970 ( .ip1(ep3_csr[28]), .ip2(n287), .op(n2872) );
  nand2_2 U2971 ( .ip1(ep0_csr[28]), .ip2(n320), .op(n2871) );
  and3_2 U2984 ( .ip1(n2890), .ip2(n2891), .ip3(n2892), .op(n2889) );
  nand2_2 U2985 ( .ip1(ep2_csr[29]), .ip2(n305), .op(n2892) );
  nand2_2 U2986 ( .ip1(csr[29]), .ip2(n286), .op(n2891) );
  nand2_2 U2987 ( .ip1(ep1_csr[29]), .ip2(n296), .op(n2890) );
  nand2_2 U2993 ( .ip1(ep3_csr[29]), .ip2(n287), .op(n2894) );
  nand2_2 U2994 ( .ip1(ep0_csr[29]), .ip2(n320), .op(n2893) );
  and3_2 U3007 ( .ip1(n2912), .ip2(n2913), .ip3(n2914), .op(n2911) );
  nand2_2 U3008 ( .ip1(ep2_csr[30]), .ip2(n305), .op(n2914) );
  nand2_2 U3009 ( .ip1(csr[30]), .ip2(n286), .op(n2913) );
  nand2_2 U3010 ( .ip1(ep1_csr[30]), .ip2(n296), .op(n2912) );
  nand2_2 U3016 ( .ip1(ep3_csr[30]), .ip2(n287), .op(n2916) );
  nand2_2 U3017 ( .ip1(ep0_csr[30]), .ip2(n320), .op(n2915) );
  and3_2 U3030 ( .ip1(n2934), .ip2(n2935), .ip3(n2936), .op(n2933) );
  nand2_2 U3031 ( .ip1(ep2_csr[31]), .ip2(n305), .op(n2936) );
  nand2_2 U3032 ( .ip1(csr[31]), .ip2(n286), .op(n2935) );
  nand2_2 U3048 ( .ip1(ep1_csr[31]), .ip2(n296), .op(n2934) );
  nand2_2 U3061 ( .ip1(ep3_csr[31]), .ip2(n287), .op(n2950) );
  nand2_2 U3064 ( .ip1(ep0_csr[31]), .ip2(n320), .op(n2949) );
  nor2_2 U3182 ( .ip1(n726), .ip2(n861), .op(ep9_we) );
  nor2_2 U3183 ( .ip1(n726), .ip2(n860), .op(ep9_re) );
  nand2_2 U3184 ( .ip1(n3045), .ip2(n712), .op(n726) );
  nor2_2 U3185 ( .ip1(n727), .ip2(n861), .op(ep8_we) );
  nor2_2 U3186 ( .ip1(n727), .ip2(n860), .op(ep8_re) );
  nand2_2 U3187 ( .ip1(n3045), .ip2(n3046), .op(n727) );
  nor2_2 U3188 ( .ip1(n728), .ip2(n861), .op(ep7_we) );
  nor2_2 U3189 ( .ip1(n728), .ip2(n860), .op(ep7_re) );
  nand2_2 U3190 ( .ip1(n3047), .ip2(n3048), .op(n728) );
  nor2_2 U3191 ( .ip1(n733), .ip2(n861), .op(ep6_we) );
  nor2_2 U3192 ( .ip1(n733), .ip2(n860), .op(ep6_re) );
  nand2_2 U3193 ( .ip1(n3047), .ip2(n3049), .op(n733) );
  nor2_2 U3194 ( .ip1(n734), .ip2(n861), .op(ep5_we) );
  nor2_2 U3195 ( .ip1(n734), .ip2(n860), .op(ep5_re) );
  nand2_2 U3196 ( .ip1(n3047), .ip2(n712), .op(n734) );
  nor2_2 U3197 ( .ip1(n735), .ip2(n861), .op(ep4_we) );
  nor2_2 U3198 ( .ip1(n735), .ip2(n860), .op(ep4_re) );
  nand2_2 U3199 ( .ip1(n3047), .ip2(n3046), .op(n735) );
  nor2_2 U3201 ( .ip1(n315), .ip2(n861), .op(ep3_we) );
  nor2_2 U3202 ( .ip1(n315), .ip2(n860), .op(ep3_re) );
  nor2_2 U3204 ( .ip1(n318), .ip2(n861), .op(ep2_we) );
  nor2_2 U3205 ( .ip1(n722), .ip2(n860), .op(ep2_re) );
  nand2_2 U3206 ( .ip1(n3049), .ip2(n3051), .op(n722) );
  nor2_2 U3207 ( .ip1(n316), .ip2(n861), .op(ep1_we) );
  nor2_2 U3208 ( .ip1(n316), .ip2(n860), .op(ep1_re) );
  nor2_2 U3210 ( .ip1(n724), .ip2(n861), .op(ep15_we) );
  nor2_2 U3211 ( .ip1(n724), .ip2(n860), .op(ep15_re) );
  nand2_2 U3212 ( .ip1(n3052), .ip2(n3048), .op(n724) );
  nor2_2 U3213 ( .ip1(n725), .ip2(n861), .op(ep14_we) );
  nor2_2 U3214 ( .ip1(n725), .ip2(n860), .op(ep14_re) );
  nand2_2 U3215 ( .ip1(n3052), .ip2(n3049), .op(n725) );
  nor2_2 U3216 ( .ip1(n742), .ip2(n861), .op(ep13_we) );
  nor2_2 U3217 ( .ip1(n742), .ip2(n860), .op(ep13_re) );
  nand2_2 U3218 ( .ip1(n3052), .ip2(n712), .op(n742) );
  nor2_2 U3219 ( .ip1(n859), .ip2(adr[3]), .op(n712) );
  nor2_2 U3220 ( .ip1(n747), .ip2(n861), .op(ep12_we) );
  nor2_2 U3221 ( .ip1(n747), .ip2(n860), .op(ep12_re) );
  nand2_2 U3222 ( .ip1(n3052), .ip2(n3046), .op(n747) );
  and3_2 U3223 ( .ip1(n874), .ip2(n873), .ip3(adr[6]), .op(n3052) );
  nor2_2 U3224 ( .ip1(n721), .ip2(n861), .op(ep11_we) );
  nor2_2 U3225 ( .ip1(n721), .ip2(n860), .op(ep11_re) );
  nand2_2 U3226 ( .ip1(n3048), .ip2(n3045), .op(n721) );
  nor2_2 U3227 ( .ip1(n858), .ip2(n859), .op(n3048) );
  nor2_2 U3228 ( .ip1(n719), .ip2(n861), .op(ep10_we) );
  nor2_2 U3229 ( .ip1(n719), .ip2(n860), .op(ep10_re) );
  nand2_2 U3230 ( .ip1(n3045), .ip2(n3049), .op(n719) );
  nor2_2 U3231 ( .ip1(n858), .ip2(adr[2]), .op(n3049) );
  nor2_2 U3235 ( .ip1(n273), .ip2(n861), .op(ep0_we) );
  nor2_2 U3237 ( .ip1(n273), .ip2(n860), .op(ep0_re) );
  nor2_2 U3442 ( .ip1(adr[2]), .ip2(adr[3]), .op(n3046) );
  usbf_ep_rf_0 u0 ( .clk(n255), .wclk(wclk), .rst(rst), .adr(adr[1:0]), .re(
        ep0_re), .we(ep0_we), .din(din), .dout(ep0_dout), .inta(ep0_inta), 
        .intb(ep0_intb), .dma_req(dma_req[0]), .dma_ack(dma_ack[0]), .idin(
        idin), .ep_sel(ep_sel), .ep_match(ep0_match), .buf0_rl(buf0_rl), 
        .buf0_set(buf0_set), .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), 
        .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(
        int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(
        int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr({ep0_csr[31:15], 
        SYNOPSYS_UNCONNECTED__0, ep0_csr[13:0]}), .buf0(ep0_buf0), .buf1(
        ep0_buf1), .dma_in_buf_sz1(ep0_dma_in_buf_sz1), .dma_out_buf_avail(
        ep0_dma_out_buf_avail) );
  usbf_ep_rf_3 u1 ( .clk(n255), .wclk(wclk), .rst(rst), .adr(adr[1:0]), .re(
        ep1_re), .we(ep1_we), .din(din), .dout(ep1_dout), .inta(ep1_inta), 
        .intb(ep1_intb), .dma_req(dma_req[1]), .dma_ack(dma_ack[1]), .idin(
        idin), .ep_sel(ep_sel), .ep_match(ep1_match), .buf0_rl(buf0_rl), 
        .buf0_set(buf0_set), .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), 
        .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(
        int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(
        int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr({ep1_csr[31:15], 
        SYNOPSYS_UNCONNECTED__1, ep1_csr[13:0]}), .buf0(ep1_buf0), .buf1(
        ep1_buf1), .dma_in_buf_sz1(ep1_dma_in_buf_sz1), .dma_out_buf_avail(
        ep1_dma_out_buf_avail) );
  usbf_ep_rf_2 u2 ( .clk(n255), .wclk(wclk), .rst(rst), .adr(adr[1:0]), .re(
        ep2_re), .we(ep2_we), .din(din), .dout(ep2_dout), .inta(ep2_inta), 
        .intb(ep2_intb), .dma_req(dma_req[2]), .dma_ack(dma_ack[2]), .idin(
        idin), .ep_sel(ep_sel), .ep_match(ep2_match), .buf0_rl(buf0_rl), 
        .buf0_set(buf0_set), .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), 
        .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(
        int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(
        int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr({ep2_csr[31:15], 
        SYNOPSYS_UNCONNECTED__2, ep2_csr[13:0]}), .buf0(ep2_buf0), .buf1(
        ep2_buf1), .dma_in_buf_sz1(ep2_dma_in_buf_sz1), .dma_out_buf_avail(
        ep2_dma_out_buf_avail) );
  usbf_ep_rf_1 u3 ( .clk(n255), .wclk(wclk), .rst(rst), .adr(adr[1:0]), .re(
        ep3_re), .we(ep3_we), .din(din), .dout(ep3_dout), .inta(ep3_inta), 
        .intb(ep3_intb), .dma_req(dma_req[3]), .dma_ack(dma_ack[3]), .idin(
        idin), .ep_sel(ep_sel), .ep_match(ep3_match), .buf0_rl(buf0_rl), 
        .buf0_set(buf0_set), .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), 
        .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(
        int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(
        int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr({ep3_csr[31:15], 
        SYNOPSYS_UNCONNECTED__3, ep3_csr[13:0]}), .buf0(ep3_buf0), .buf1(
        ep3_buf1), .dma_in_buf_sz1(ep3_dma_in_buf_sz1), .dma_out_buf_avail(
        ep3_dma_out_buf_avail) );
  usbf_ep_rf_dummy_0 u4 ( .clk(n332), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep4_re), .we(ep4_we), .din(din), .dma_ack(dma_ack[4]), .idin(idin), 
        .ep_sel(ep_sel), .buf0_rl(buf0_rl), .buf0_set(buf0_set), .buf1_set(
        buf1_set), .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), 
        .int_buf1_set(int_buf1_set), .int_buf0_set(int_buf0_set), 
        .int_upid_set(int_upid_set), .int_crc16_set(int_crc16_set), 
        .int_to_set(int_to_set), .int_seqerr_set(int_seqerr_set), 
        .out_to_small(out_to_small) );
  usbf_ep_rf_dummy_11 u5 ( .clk(n332), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep5_re), .we(ep5_we), .din(din), .dma_ack(dma_ack[5]), .idin(idin), 
        .ep_sel(ep_sel), .buf0_rl(buf0_rl), .buf0_set(buf0_set), .buf1_set(
        buf1_set), .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), 
        .int_buf1_set(int_buf1_set), .int_buf0_set(int_buf0_set), 
        .int_upid_set(int_upid_set), .int_crc16_set(int_crc16_set), 
        .int_to_set(int_to_set), .int_seqerr_set(int_seqerr_set), 
        .out_to_small(out_to_small) );
  usbf_ep_rf_dummy_10 u6 ( .clk(n332), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep6_re), .we(ep6_we), .din(din), .dma_ack(dma_ack[6]), .idin(idin), 
        .ep_sel(ep_sel), .buf0_rl(buf0_rl), .buf0_set(buf0_set), .buf1_set(
        buf1_set), .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), 
        .int_buf1_set(int_buf1_set), .int_buf0_set(int_buf0_set), 
        .int_upid_set(int_upid_set), .int_crc16_set(int_crc16_set), 
        .int_to_set(int_to_set), .int_seqerr_set(int_seqerr_set), 
        .out_to_small(out_to_small) );
  usbf_ep_rf_dummy_9 u7 ( .clk(n332), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep7_re), .we(ep7_we), .din(din), .dma_ack(dma_ack[7]), .idin(idin), 
        .ep_sel(ep_sel), .buf0_rl(buf0_rl), .buf0_set(buf0_set), .buf1_set(
        buf1_set), .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), 
        .int_buf1_set(int_buf1_set), .int_buf0_set(int_buf0_set), 
        .int_upid_set(int_upid_set), .int_crc16_set(int_crc16_set), 
        .int_to_set(int_to_set), .int_seqerr_set(int_seqerr_set), 
        .out_to_small(out_to_small) );
  usbf_ep_rf_dummy_8 u8 ( .clk(n332), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep8_re), .we(ep8_we), .din(din), .dma_ack(dma_ack[8]), .idin(idin), 
        .ep_sel(ep_sel), .buf0_rl(buf0_rl), .buf0_set(buf0_set), .buf1_set(
        buf1_set), .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), 
        .int_buf1_set(int_buf1_set), .int_buf0_set(int_buf0_set), 
        .int_upid_set(int_upid_set), .int_crc16_set(int_crc16_set), 
        .int_to_set(int_to_set), .int_seqerr_set(int_seqerr_set), 
        .out_to_small(out_to_small) );
  usbf_ep_rf_dummy_7 u9 ( .clk(n332), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep9_re), .we(ep9_we), .din(din), .dma_ack(dma_ack[9]), .idin(idin), 
        .ep_sel(ep_sel), .buf0_rl(buf0_rl), .buf0_set(buf0_set), .buf1_set(
        buf1_set), .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), 
        .int_buf1_set(int_buf1_set), .int_buf0_set(int_buf0_set), 
        .int_upid_set(int_upid_set), .int_crc16_set(int_crc16_set), 
        .int_to_set(int_to_set), .int_seqerr_set(int_seqerr_set), 
        .out_to_small(out_to_small) );
  usbf_ep_rf_dummy_6 u10 ( .clk(n332), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep10_re), .we(ep10_we), .din(din), .dma_ack(dma_ack[10]), .idin(
        idin), .ep_sel(ep_sel), .buf0_rl(buf0_rl), .buf0_set(buf0_set), 
        .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(int_buf0_set), .int_upid_set(
        int_upid_set), .int_crc16_set(int_crc16_set), .int_to_set(int_to_set), 
        .int_seqerr_set(int_seqerr_set), .out_to_small(out_to_small) );
  usbf_ep_rf_dummy_5 u11 ( .clk(n332), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep11_re), .we(ep11_we), .din(din), .dma_ack(dma_ack[11]), .idin(
        idin), .ep_sel(ep_sel), .buf0_rl(buf0_rl), .buf0_set(buf0_set), 
        .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(int_buf0_set), .int_upid_set(
        int_upid_set), .int_crc16_set(int_crc16_set), .int_to_set(int_to_set), 
        .int_seqerr_set(int_seqerr_set), .out_to_small(out_to_small) );
  usbf_ep_rf_dummy_4 u12 ( .clk(n332), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep12_re), .we(ep12_we), .din(din), .dma_ack(dma_ack[12]), .idin(
        idin), .ep_sel(ep_sel), .buf0_rl(buf0_rl), .buf0_set(buf0_set), 
        .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(int_buf0_set), .int_upid_set(
        int_upid_set), .int_crc16_set(int_crc16_set), .int_to_set(int_to_set), 
        .int_seqerr_set(int_seqerr_set), .out_to_small(out_to_small) );
  usbf_ep_rf_dummy_3 u13 ( .clk(n332), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep13_re), .we(ep13_we), .din(din), .dma_ack(dma_ack[13]), .idin(
        idin), .ep_sel(ep_sel), .buf0_rl(buf0_rl), .buf0_set(buf0_set), 
        .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(int_buf0_set), .int_upid_set(
        int_upid_set), .int_crc16_set(int_crc16_set), .int_to_set(int_to_set), 
        .int_seqerr_set(int_seqerr_set), .out_to_small(out_to_small) );
  usbf_ep_rf_dummy_2 u14 ( .clk(n332), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep14_re), .we(ep14_we), .din(din), .dma_ack(dma_ack[14]), .idin(
        idin), .ep_sel(ep_sel), .buf0_rl(buf0_rl), .buf0_set(buf0_set), 
        .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(int_buf0_set), .int_upid_set(
        int_upid_set), .int_crc16_set(int_crc16_set), .int_to_set(int_to_set), 
        .int_seqerr_set(int_seqerr_set), .out_to_small(out_to_small) );
  usbf_ep_rf_dummy_1 u15 ( .clk(n332), .wclk(wclk), .rst(rst), .adr(adr[1:0]), 
        .re(ep15_re), .we(ep15_we), .din(din), .dma_ack(dma_ack[15]), .idin(
        idin), .ep_sel(ep_sel), .buf0_rl(buf0_rl), .buf0_set(buf0_set), 
        .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(int_buf0_set), .int_upid_set(
        int_upid_set), .int_crc16_set(int_crc16_set), .int_to_set(int_to_set), 
        .int_seqerr_set(int_seqerr_set), .out_to_small(out_to_small) );
  dp_1 \funct_adr_reg[4]  ( .ip(n3369), .ck(wclk), .q(n875) );
  dp_1 \funct_adr_reg[3]  ( .ip(n3368), .ck(wclk), .q(n876) );
  lp_1 \dtmp_reg[5]  ( .ck(n328), .ip(N76), .q(dtmp[5]) );
  lp_1 \dtmp_reg[0]  ( .ck(n328), .ip(N71), .q(dtmp[0]) );
  dp_1 \inta_msk_reg[5]  ( .ip(n3352), .ck(wclk), .q(inta_msk[5]) );
  lp_1 \dtmp_reg[4]  ( .ck(n328), .ip(N75), .q(dtmp[4]) );
  lp_1 \dtmp_reg[3]  ( .ck(n328), .ip(N74), .q(dtmp[3]) );
  lp_1 \dtmp_reg[1]  ( .ck(n328), .ip(N72), .q(dtmp[1]) );
  dp_1 \utmi_vend_stat_r_reg[0]  ( .ip(utmi_vend_stat[0]), .ck(wclk), .q(
        utmi_vend_stat_r[0]) );
  dp_1 \inta_msk_reg[2]  ( .ip(n3349), .ck(wclk), .q(inta_msk[2]) );
  dp_1 \funct_adr_reg[2]  ( .ip(n3367), .ck(wclk), .q(n877) );
  dp_1 \inta_msk_reg[4]  ( .ip(n3351), .ck(wclk), .q(inta_msk[4]) );
  dp_1 \inta_msk_reg[3]  ( .ip(n3350), .ck(wclk), .q(inta_msk[3]) );
  dp_1 \inta_msk_reg[1]  ( .ip(n3348), .ck(wclk), .q(inta_msk[1]) );
  dp_1 \utmi_vend_stat_r_reg[6]  ( .ip(utmi_vend_stat[6]), .ck(wclk), .q(
        utmi_vend_stat_r[6]) );
  dp_1 \csr_reg[21]  ( .ip(n3336), .ck(n332), .q(csr[21]) );
  dp_1 \csr_reg[20]  ( .ip(n3335), .ck(n332), .q(csr[20]) );
  dp_1 \csr_reg[19]  ( .ip(n3334), .ck(n332), .q(csr[19]) );
  dp_1 \csr_reg[18]  ( .ip(n3333), .ck(n332), .q(csr[18]) );
  dp_1 \csr_reg[14]  ( .ip(n109), .ck(n332), .q(csr[14]) );
  dp_1 \csr_reg[13]  ( .ip(n3328), .ck(n332), .q(csr[13]) );
  dp_1 utmi_vend_wr_reg ( .ip(utmi_vend_wr_r), .ck(n332), .q(utmi_vend_wr) );
  dp_1 rf_resume_req_reg ( .ip(rf_resume_req_r), .ck(n332), .q(rf_resume_req)
         );
  dp_1 match_r1_reg ( .ip(n821), .ck(n332), .q(match) );
  dp_1 \buf1_reg[28]  ( .ip(n3279), .ck(n332), .q(buf1[28]) );
  dp_1 \buf1_reg[30]  ( .ip(n3281), .ck(n255), .q(buf1[30]) );
  dp_1 \buf1_reg[9]  ( .ip(n3260), .ck(n332), .q(buf1[9]) );
  dp_1 \buf1_reg[5]  ( .ip(n3256), .ck(n255), .q(buf1[5]) );
  dp_1 \buf1_reg[1]  ( .ip(n3252), .ck(n255), .q(buf1[1]) );
  dp_1 \buf1_reg[26]  ( .ip(n3277), .ck(n332), .q(buf1[26]) );
  dp_1 \buf1_reg[24]  ( .ip(n3275), .ck(n332), .q(buf1[24]) );
  dp_1 \buf1_reg[22]  ( .ip(n3273), .ck(n255), .q(buf1[22]) );
  dp_1 \buf1_reg[20]  ( .ip(n3271), .ck(n332), .q(buf1[20]) );
  dp_1 \buf1_reg[18]  ( .ip(n3269), .ck(n255), .q(buf1[18]) );
  dp_1 \buf1_reg[12]  ( .ip(n3263), .ck(n255), .q(buf1[12]) );
  dp_1 \buf1_reg[0]  ( .ip(n3251), .ck(n332), .q(buf1[0]) );
  dp_1 \buf1_reg[16]  ( .ip(n3267), .ck(n332), .q(buf1[16]) );
  dp_1 \buf1_reg[7]  ( .ip(n3258), .ck(n255), .q(buf1[7]) );
  dp_1 \buf0_reg[31]  ( .ip(n3314), .ck(n332), .q(buf0[31]) );
  dp_1 \csr_reg[25]  ( .ip(n3340), .ck(n332), .q(csr[25]) );
  dp_1 \csr_reg[17]  ( .ip(n3332), .ck(n332), .q(csr[17]) );
  dp_1 \csr_reg[16]  ( .ip(n3331), .ck(n332), .q(csr[16]) );
  dp_1 dma_out_buf_avail_reg ( .ip(n3249), .ck(n332), .q(dma_out_buf_avail) );
  dp_1 \buf1_reg[14]  ( .ip(n3265), .ck(n332), .q(buf1[14]) );
  dp_1 \buf1_reg[2]  ( .ip(n3253), .ck(n255), .q(buf1[2]) );
  dp_1 \csr_reg[15]  ( .ip(n3330), .ck(n332), .q(csr[15]) );
  dp_1 \csr_reg[12]  ( .ip(n3327), .ck(n332), .q(csr[12]) );
  dp_1 \csr_reg[11]  ( .ip(n3326), .ck(n332), .q(csr[11]) );
  dp_1 \csr_reg[31]  ( .ip(n3346), .ck(n332), .q(csr[31]) );
  dp_1 \csr_reg[24]  ( .ip(n3339), .ck(n332), .q(csr[24]) );
  dp_1 \csr_reg[23]  ( .ip(n3338), .ck(n332), .q(csr[23]) );
  dp_1 \csr_reg[22]  ( .ip(n3337), .ck(n332), .q(csr[22]) );
  dp_1 \buf0_reg[29]  ( .ip(n3312), .ck(n332), .q(buf0[29]) );
  dp_1 \buf0_reg[28]  ( .ip(n3311), .ck(n332), .q(buf0[28]) );
  dp_1 \buf0_reg[30]  ( .ip(n3313), .ck(n332), .q(buf0[30]) );
  dp_1 \buf0_reg[17]  ( .ip(n3300), .ck(n332), .q(buf0[17]) );
  dp_1 \buf0_reg[27]  ( .ip(n3310), .ck(n332), .q(buf0[27]) );
  dp_1 \buf0_reg[25]  ( .ip(n3308), .ck(n332), .q(buf0[25]) );
  dp_1 \buf0_reg[23]  ( .ip(n3306), .ck(n332), .q(buf0[23]) );
  dp_1 \buf0_reg[21]  ( .ip(n3304), .ck(n332), .q(buf0[21]) );
  dp_1 \buf0_reg[19]  ( .ip(n3302), .ck(n332), .q(buf0[19]) );
  dp_1 \buf0_reg[0]  ( .ip(n3283), .ck(n255), .q(buf0[0]) );
  lp_1 \dtmp_reg[31]  ( .ck(n331), .ip(N102), .q(dtmp[31]) );
  lp_1 \dtmp_reg[30]  ( .ck(n331), .ip(N101), .q(dtmp[30]) );
  lp_1 \dtmp_reg[29]  ( .ck(n331), .ip(N100), .q(dtmp[29]) );
  dp_1 \buf0_reg[26]  ( .ip(n3309), .ck(n332), .q(buf0[26]) );
  dp_1 \buf0_reg[24]  ( .ip(n3307), .ck(n332), .q(buf0[24]) );
  dp_1 \buf0_reg[22]  ( .ip(n3305), .ck(n332), .q(buf0[22]) );
  dp_1 \buf0_reg[20]  ( .ip(n3303), .ck(n332), .q(buf0[20]) );
  dp_1 \buf0_reg[18]  ( .ip(n3301), .ck(n332), .q(buf0[18]) );
  dp_1 \buf0_reg[1]  ( .ip(n3284), .ck(n255), .q(buf0[1]) );
  dp_1 \buf0_reg[9]  ( .ip(n3292), .ck(n255), .q(buf0[9]) );
  dp_1 \buf0_reg[5]  ( .ip(n3288), .ck(n255), .q(buf0[5]) );
  dp_1 \buf0_reg[3]  ( .ip(n3286), .ck(n255), .q(buf0[3]) );
  dp_1 \buf0_reg[2]  ( .ip(n3285), .ck(n255), .q(buf0[2]) );
  dp_1 \buf0_reg[8]  ( .ip(n3291), .ck(n255), .q(buf0[8]) );
  dp_1 \buf0_reg[4]  ( .ip(n3287), .ck(n255), .q(buf0[4]) );
  dp_1 \buf0_reg[7]  ( .ip(n3290), .ck(n255), .q(buf0[7]) );
  dp_1 \buf0_reg[10]  ( .ip(n3293), .ck(n255), .q(buf0[10]) );
  dp_1 \buf0_reg[6]  ( .ip(n3289), .ck(n255), .q(buf0[6]) );
  lp_1 \dtmp_reg[15]  ( .ck(n329), .ip(N86), .q(dtmp[15]) );
  lp_1 \dtmp_reg[14]  ( .ck(n329), .ip(N85), .q(dtmp[14]) );
  lp_1 \dtmp_reg[13]  ( .ck(n329), .ip(N84), .q(dtmp[13]) );
  lp_1 \dtmp_reg[12]  ( .ck(n329), .ip(N83), .q(dtmp[12]) );
  lp_1 \dtmp_reg[11]  ( .ck(n329), .ip(N82), .q(dtmp[11]) );
  lp_1 \dtmp_reg[10]  ( .ck(n329), .ip(N81), .q(dtmp[10]) );
  lp_1 \dtmp_reg[9]  ( .ck(n329), .ip(N80), .q(dtmp[9]) );
  dp_1 \csr_reg[27]  ( .ip(n3342), .ck(n332), .q(csr[27]) );
  dp_1 \buf0_reg[12]  ( .ip(n3295), .ck(n332), .q(buf0[12]) );
  dp_1 \buf0_reg[13]  ( .ip(n3296), .ck(n332), .q(buf0[13]) );
  dp_1 \buf0_reg[11]  ( .ip(n3294), .ck(n332), .q(buf0[11]) );
  dp_1 \buf0_reg[16]  ( .ip(n3299), .ck(n332), .q(buf0[16]) );
  dp_1 \buf0_reg[15]  ( .ip(n3298), .ck(n332), .q(buf0[15]) );
  dp_1 \buf0_reg[14]  ( .ip(n3297), .ck(n332), .q(buf0[14]) );
  dp_1 \csr_reg[30]  ( .ip(n3345), .ck(n332), .q(csr[30]) );
  dp_1 \csr_reg[26]  ( .ip(n3341), .ck(n332), .q(csr[26]) );
  dp_1 \csr_reg[29]  ( .ip(n3344), .ck(n332), .q(csr[29]) );
  dp_1 \csr_reg[1]  ( .ip(n3316), .ck(n332), .q(csr[1]) );
  dp_1 \csr_reg[9]  ( .ip(n3324), .ck(n332), .q(csr[9]) );
  dp_1 \csr_reg[3]  ( .ip(n3318), .ck(n332), .q(csr[3]) );
  dp_1 \csr_reg[5]  ( .ip(n3320), .ck(n332), .q(csr[5]) );
  dp_1 \csr_reg[7]  ( .ip(n3322), .ck(n332), .q(csr[7]) );
  dp_1 \csr_reg[28]  ( .ip(n3343), .ck(n332), .q(csr[28]) );
  dp_1 \csr_reg[6]  ( .ip(n3321), .ck(n332), .q(csr[6]) );
  dp_1 \csr_reg[4]  ( .ip(n3319), .ck(n332), .q(csr[4]) );
  dp_1 \csr_reg[8]  ( .ip(n3323), .ck(n332), .q(csr[8]) );
  dp_1 \csr_reg[2]  ( .ip(n3317), .ck(n332), .q(csr[2]) );
  dp_1 \csr_reg[0]  ( .ip(n3315), .ck(n332), .q(csr[0]) );
  dp_1 \csr_reg[10]  ( .ip(n3325), .ck(n332), .q(csr[10]) );
  dp_1 \utmi_vend_ctrl_r_reg[3]  ( .ip(n3198), .ck(wclk), .q(
        utmi_vend_ctrl_r[3]) );
  dp_1 \utmi_vend_ctrl_r_reg[2]  ( .ip(n3197), .ck(wclk), .q(
        utmi_vend_ctrl_r[2]) );
  dp_1 \utmi_vend_ctrl_r_reg[1]  ( .ip(n3196), .ck(wclk), .q(
        utmi_vend_ctrl_r[1]) );
  dp_1 \utmi_vend_ctrl_r_reg[0]  ( .ip(n3195), .ck(wclk), .q(
        utmi_vend_ctrl_r[0]) );
  dp_1 usb_reset_r_reg ( .ip(usb_reset), .ck(wclk), .q(usb_reset_r) );
  dp_1 pid_cs_err_r_reg ( .ip(pid_cs_err), .ck(wclk), .q(pid_cs_err_r) );
  dp_1 rx_err_r_reg ( .ip(rx_err), .ck(wclk), .q(rx_err_r) );
  dp_1 nse_err_r_reg ( .ip(nse_err), .ck(wclk), .q(nse_err_r) );
  dp_1 crc5_err_r_reg ( .ip(crc5_err), .ck(wclk), .q(crc5_err_r) );
  dp_1 \dout_reg[28]  ( .ip(n127), .ck(wclk), .q(dout[28]) );
  dp_1 suspend_r1_reg ( .ip(suspend_r), .ck(wclk), .q(suspend_r1) );
  dp_1 attach_r1_reg ( .ip(attach_r), .ck(wclk), .q(attach_r1) );
  dp_1 int_src_re_reg ( .ip(N103), .ck(wclk), .q(int_src_re) );
  lp_1 \dtmp_reg[19]  ( .ck(n331), .ip(N90), .q(dtmp[19]) );
  lp_1 \dtmp_reg[18]  ( .ck(n331), .ip(N89), .q(dtmp[18]) );
  lp_1 \dtmp_reg[17]  ( .ck(n330), .ip(N88), .q(dtmp[17]) );
  lp_1 \dtmp_reg[16]  ( .ck(n330), .ip(N87), .q(dtmp[16]) );
  lp_1 \dtmp_reg[28]  ( .ck(n330), .ip(N99), .q(dtmp[28]) );
  dp_1 \int_srca_reg[1]  ( .ip(N729), .ck(wclk), .q(int_srca[1]) );
  dp_1 \utmi_vend_stat_r_reg[4]  ( .ip(utmi_vend_stat[4]), .ck(wclk), .q(
        utmi_vend_stat_r[4]) );
  dp_1 \utmi_vend_stat_r_reg[3]  ( .ip(utmi_vend_stat[3]), .ck(wclk), .q(
        utmi_vend_stat_r[3]) );
  lp_1 \dtmp_reg[27]  ( .ck(n330), .ip(N98), .q(dtmp[27]) );
  lp_1 \dtmp_reg[26]  ( .ck(n330), .ip(N97), .q(dtmp[26]) );
  lp_1 \dtmp_reg[25]  ( .ck(n330), .ip(N96), .q(dtmp[25]) );
  lp_1 \dtmp_reg[8]  ( .ck(n328), .ip(n153), .q(dtmp[8]) );
  dp_1 \utmi_vend_stat_r_reg[7]  ( .ip(utmi_vend_stat[7]), .ck(wclk), .q(
        utmi_vend_stat_r[7]) );
  lp_1 \dtmp_reg[21]  ( .ck(n329), .ip(n149), .q(dtmp[21]) );
  lp_1 \dtmp_reg[23]  ( .ck(n330), .ip(N94), .q(dtmp[23]) );
  lp_1 \dtmp_reg[24]  ( .ck(n330), .ip(N95), .q(dtmp[24]) );
  lp_1 \dtmp_reg[22]  ( .ck(n330), .ip(N93), .q(dtmp[22]) );
  lp_1 \dtmp_reg[20]  ( .ck(n329), .ip(n139), .q(dtmp[20]) );
  lp_1 \dtmp_reg[7]  ( .ck(n328), .ip(N78), .q(dtmp[7]) );
  dp_1 \int_srca_reg[2]  ( .ip(N728), .ck(wclk), .q(int_srca[2]) );
  dp_1 \utmi_vend_stat_r_reg[2]  ( .ip(utmi_vend_stat[2]), .ck(wclk), .q(
        utmi_vend_stat_r[2]) );
  dp_1 \utmi_vend_stat_r_reg[1]  ( .ip(utmi_vend_stat[1]), .ck(wclk), .q(
        utmi_vend_stat_r[1]) );
  dp_1 \utmi_vend_stat_r_reg[5]  ( .ip(utmi_vend_stat[5]), .ck(wclk), .q(
        utmi_vend_stat_r[5]) );
  dp_1 \utmi_vend_ctrl_reg[3]  ( .ip(utmi_vend_ctrl_r[3]), .ck(n332), .q(
        utmi_vend_ctrl[3]) );
  dp_1 \utmi_vend_ctrl_reg[2]  ( .ip(utmi_vend_ctrl_r[2]), .ck(n332), .q(
        utmi_vend_ctrl[2]) );
  dp_1 \utmi_vend_ctrl_reg[1]  ( .ip(utmi_vend_ctrl_r[1]), .ck(n332), .q(
        utmi_vend_ctrl[1]) );
  dp_1 \utmi_vend_ctrl_reg[0]  ( .ip(utmi_vend_ctrl_r[0]), .ck(n332), .q(
        utmi_vend_ctrl[0]) );
  dp_1 \int_srcb_reg[0]  ( .ip(n107), .ck(wclk), .q(int_srcb[0]) );
  dp_1 \funct_adr_reg[0]  ( .ip(n3365), .ck(wclk), .q(n878) );
  lp_1 \dtmp_reg[6]  ( .ck(N70), .ip(N77), .q(dtmp[6]) );
  lp_1 \dtmp_reg[2]  ( .ck(N70), .ip(N73), .q(dtmp[2]) );
  dp_1 \dout_reg[9]  ( .ip(n3230), .ck(wclk), .q(dout[9]) );
  dp_1 \dout_reg[31]  ( .ip(n3208), .ck(wclk), .q(dout[31]) );
  dp_1 \dout_reg[30]  ( .ip(n3209), .ck(wclk), .q(dout[30]) );
  dp_1 \dout_reg[29]  ( .ip(n3210), .ck(wclk), .q(dout[29]) );
  dp_1 \dout_reg[15]  ( .ip(n3224), .ck(wclk), .q(dout[15]) );
  dp_1 utmi_vend_wr_r_reg ( .ip(n3373), .ck(wclk), .q(utmi_vend_wr_r) );
  dp_1 rf_resume_req_r_reg ( .ip(n3372), .ck(wclk), .q(rf_resume_req_r) );
  dp_1 \dout_reg[14]  ( .ip(n3225), .ck(wclk), .q(dout[14]) );
  dp_1 \dout_reg[13]  ( .ip(n3226), .ck(wclk), .q(dout[13]) );
  dp_1 \dout_reg[12]  ( .ip(n3227), .ck(wclk), .q(dout[12]) );
  dp_1 \dout_reg[11]  ( .ip(n3228), .ck(wclk), .q(dout[11]) );
  dp_1 \dout_reg[10]  ( .ip(n3229), .ck(wclk), .q(dout[10]) );
  dp_1 intb_reg ( .ip(N732), .ck(wclk), .q(intb) );
  dp_1 inta_reg ( .ip(N731), .ck(wclk), .q(inta) );
  dp_1 \dout_reg[27]  ( .ip(n161), .ck(wclk), .q(dout[27]) );
  dp_1 \dout_reg[26]  ( .ip(n159), .ck(wclk), .q(dout[26]) );
  dp_1 \inta_msk_reg[6]  ( .ip(n3353), .ck(wclk), .q(inta_msk[6]) );
  inv_1 \int_srca_reg[0]/U4  ( .ip(ep0_inta), .op(n132) );
  nand2_1 \int_srca_reg[0]/U2  ( .ip1(n862), .ip2(n132), .op(n133) );
  dp_1 \int_srca_reg[0]  ( .ip(n133), .ck(wclk), .q(n131) );
  inv_1 \int_srca_reg[3]/U4  ( .ip(ep3_inta), .op(n129) );
  nand2_1 \int_srca_reg[3]/U2  ( .ip1(n863), .ip2(n129), .op(n130) );
  dp_1 \int_srca_reg[3]  ( .ip(n130), .ck(wclk), .q(int_srca[3]) );
  dp_2 \dout_reg[7]  ( .ip(n3232), .ck(wclk), .q(dout[7]) );
  dp_1 \inta_msk_reg[7]  ( .ip(n3354), .ck(wclk), .q(inta_msk[7]) );
  dp_2 \dout_reg[24]  ( .ip(n3215), .ck(wclk), .q(dout[24]) );
  dp_2 \dout_reg[21]  ( .ip(n3218), .ck(wclk), .q(dout[21]) );
  dp_2 \dout_reg[22]  ( .ip(n3217), .ck(wclk), .q(dout[22]) );
  dp_2 \dout_reg[23]  ( .ip(n3216), .ck(wclk), .q(dout[23]) );
  dp_2 \dout_reg[8]  ( .ip(n3231), .ck(wclk), .q(dout[8]) );
  dp_2 \dout_reg[20]  ( .ip(n3219), .ck(wclk), .q(dout[20]) );
  dp_1 \inta_msk_reg[8]  ( .ip(n3355), .ck(wclk), .q(inta_msk[8]) );
  dp_2 \dout_reg[16]  ( .ip(n163), .ck(wclk), .q(dout[16]) );
  dp_2 \dout_reg[17]  ( .ip(n166), .ck(wclk), .q(dout[17]) );
  dp_1 \intb_msk_reg[0]  ( .ip(n3356), .ck(wclk), .q(intb_msk[0]) );
  dp_2 \dout_reg[25]  ( .ip(n156), .ck(wclk), .q(dout[25]) );
  dp_2 \dout_reg[19]  ( .ip(n172), .ck(wclk), .q(dout[19]) );
  dp_2 \dout_reg[18]  ( .ip(n169), .ck(wclk), .q(dout[18]) );
  dp_1 \intb_msk_reg[5]  ( .ip(n3361), .ck(wclk), .q(intb_msk[5]) );
  dp_1 \buf1_reg[6]  ( .ip(n3257), .ck(n332), .q(buf1[6]) );
  dp_1 \buf1_reg[10]  ( .ip(n3261), .ck(n255), .q(buf1[10]) );
  dp_1 \buf1_reg[3]  ( .ip(n3254), .ck(n255), .q(buf1[3]) );
  dp_1 \buf1_reg[15]  ( .ip(n3266), .ck(n332), .q(buf1[15]) );
  dp_1 \buf1_reg[4]  ( .ip(n3255), .ck(n255), .q(buf1[4]) );
  dp_1 \buf1_reg[8]  ( .ip(n3259), .ck(n332), .q(buf1[8]) );
  dp_1 \buf1_reg[11]  ( .ip(n3262), .ck(n332), .q(buf1[11]) );
  dp_1 \buf1_reg[13]  ( .ip(n3264), .ck(clk), .q(buf1[13]) );
  dp_1 \buf1_reg[19]  ( .ip(n3270), .ck(clk), .q(buf1[19]) );
  dp_1 \buf1_reg[21]  ( .ip(n3272), .ck(n332), .q(buf1[21]) );
  dp_1 \buf1_reg[23]  ( .ip(n3274), .ck(n255), .q(buf1[23]) );
  dp_1 \buf1_reg[25]  ( .ip(n3276), .ck(clk), .q(buf1[25]) );
  dp_1 \buf1_reg[27]  ( .ip(n3278), .ck(n332), .q(buf1[27]) );
  dp_1 \buf1_reg[17]  ( .ip(n3268), .ck(clk), .q(buf1[17]) );
  dp_1 \buf1_reg[29]  ( .ip(n3280), .ck(clk), .q(buf1[29]) );
  dp_1 dma_in_buf_sz1_reg ( .ip(n3250), .ck(clk), .q(dma_in_buf_sz1) );
  dp_1 \buf1_reg[31]  ( .ip(n3282), .ck(n255), .q(buf1[31]) );
  dp_1 \inta_msk_reg[0]  ( .ip(n3347), .ck(wclk), .q(inta_msk[0]) );
  dp_2 \intb_msk_reg[4]  ( .ip(n3360), .ck(wclk), .q(intb_msk[4]) );
  dp_2 \int_srcb_reg[5]  ( .ip(n3245), .ck(wclk), .q(int_srcb[5]) );
  dp_2 \intb_msk_reg[1]  ( .ip(n3357), .ck(wclk), .q(intb_msk[1]) );
  dp_2 \intb_msk_reg[3]  ( .ip(n3359), .ck(wclk), .q(intb_msk[3]) );
  dp_2 \intb_msk_reg[2]  ( .ip(n3358), .ck(wclk), .q(intb_msk[2]) );
  and2_1 U3 ( .ip1(1'b1), .ip2(n138), .op(n517) );
  inv_2 U4 ( .ip(n533), .op(n134) );
  nand3_2 U5 ( .ip1(n552), .ip2(n550), .ip3(n551), .op(N78) );
  nand2_2 U6 ( .ip1(n155), .ip2(n154), .op(n153) );
  inv_2 U7 ( .ip(n575), .op(N80) );
  inv_2 U8 ( .ip(n646), .op(N86) );
  nand2_2 U9 ( .ip1(n499), .ip2(n500), .op(n144) );
  inv_2 U10 ( .ip(n586), .op(N81) );
  inv_2 U11 ( .ip(n598), .op(N82) );
  inv_2 U12 ( .ip(n610), .op(N83) );
  inv_2 U13 ( .ip(n622), .op(N84) );
  inv_2 U14 ( .ip(n634), .op(N85) );
  and2_1 U16 ( .ip1(n363), .ip2(n359), .op(n3243) );
  nand2_1 U17 ( .ip1(n754), .ip2(inta_msk[0]), .op(n398) );
  nor2_2 U18 ( .ip1(n495), .ip2(n494), .op(n500) );
  inv_1 U19 ( .ip(n131), .op(n137) );
  and4_2 U20 ( .ip1(n415), .ip2(n414), .ip3(n413), .ip4(n412), .op(n2) );
  and2_2 U21 ( .ip1(n3048), .ip2(n3051), .op(n3) );
  and2_2 U22 ( .ip1(n3051), .ip2(n712), .op(n4) );
  nand2_1 U23 ( .ip1(dout[28]), .ip2(n2), .op(n5) );
  nand2_1 U24 ( .ip1(dout[27]), .ip2(n2), .op(n6) );
  nand2_1 U25 ( .ip1(dout[26]), .ip2(n2), .op(n7) );
  and2_2 U26 ( .ip1(n2299), .ip2(n2300), .op(n8) );
  and2_2 U27 ( .ip1(n2343), .ip2(n2344), .op(n9) );
  and2_2 U28 ( .ip1(n2387), .ip2(n2388), .op(n10) );
  and2_2 U29 ( .ip1(n2475), .ip2(n2476), .op(n11) );
  and2_2 U30 ( .ip1(n2431), .ip2(n2432), .op(n12) );
  and2_2 U31 ( .ip1(n2321), .ip2(n2322), .op(n13) );
  and2_2 U32 ( .ip1(n2365), .ip2(n2366), .op(n14) );
  and2_2 U33 ( .ip1(n2409), .ip2(n2410), .op(n15) );
  and2_2 U34 ( .ip1(n2453), .ip2(n2454), .op(n16) );
  and2_2 U35 ( .ip1(n2849), .ip2(n2850), .op(n17) );
  and2_2 U36 ( .ip1(n2651), .ip2(n2652), .op(n18) );
  and2_2 U37 ( .ip1(n2673), .ip2(n2674), .op(n19) );
  and2_2 U38 ( .ip1(n2695), .ip2(n2696), .op(n20) );
  and2_2 U39 ( .ip1(n2717), .ip2(n2718), .op(n21) );
  and2_2 U40 ( .ip1(n2585), .ip2(n2586), .op(n22) );
  and2_2 U41 ( .ip1(n824), .ip2(n825), .op(n23) );
  and2_2 U42 ( .ip1(n847), .ip2(n848), .op(n24) );
  and2_2 U43 ( .ip1(n869), .ip2(n870), .op(n25) );
  and2_2 U44 ( .ip1(n891), .ip2(n892), .op(n26) );
  and2_2 U45 ( .ip1(n913), .ip2(n914), .op(n27) );
  and2_2 U46 ( .ip1(n935), .ip2(n936), .op(n28) );
  and2_2 U47 ( .ip1(n957), .ip2(n958), .op(n29) );
  and2_2 U48 ( .ip1(n979), .ip2(n980), .op(n30) );
  and2_2 U49 ( .ip1(n1001), .ip2(n1002), .op(n31) );
  and2_2 U50 ( .ip1(n1023), .ip2(n1024), .op(n32) );
  and2_2 U51 ( .ip1(n1045), .ip2(n1046), .op(n33) );
  and2_2 U52 ( .ip1(n1067), .ip2(n1068), .op(n34) );
  and2_2 U53 ( .ip1(n1089), .ip2(n1090), .op(n35) );
  and2_2 U54 ( .ip1(n1111), .ip2(n1112), .op(n36) );
  and2_2 U55 ( .ip1(n1133), .ip2(n1134), .op(n37) );
  and2_2 U56 ( .ip1(n1155), .ip2(n1156), .op(n38) );
  and2_2 U57 ( .ip1(n1177), .ip2(n1178), .op(n39) );
  and2_2 U58 ( .ip1(n1199), .ip2(n1200), .op(n40) );
  and2_2 U59 ( .ip1(n1221), .ip2(n1222), .op(n41) );
  and2_2 U60 ( .ip1(n1243), .ip2(n1244), .op(n42) );
  and2_2 U61 ( .ip1(n1265), .ip2(n1266), .op(n43) );
  and2_2 U62 ( .ip1(n1287), .ip2(n1288), .op(n44) );
  and2_2 U63 ( .ip1(n1309), .ip2(n1310), .op(n45) );
  and2_2 U64 ( .ip1(n1331), .ip2(n1332), .op(n46) );
  and2_2 U65 ( .ip1(n1353), .ip2(n1354), .op(n47) );
  and2_2 U66 ( .ip1(n1375), .ip2(n1376), .op(n48) );
  and2_2 U67 ( .ip1(n1397), .ip2(n1398), .op(n49) );
  and2_2 U68 ( .ip1(n1419), .ip2(n1420), .op(n50) );
  and2_2 U69 ( .ip1(n1441), .ip2(n1442), .op(n51) );
  and2_2 U70 ( .ip1(n1463), .ip2(n1464), .op(n52) );
  and2_2 U71 ( .ip1(n1485), .ip2(n1486), .op(n53) );
  and2_2 U72 ( .ip1(n1507), .ip2(n1508), .op(n54) );
  and2_2 U73 ( .ip1(n1529), .ip2(n1530), .op(n55) );
  and2_2 U74 ( .ip1(n1551), .ip2(n1552), .op(n56) );
  and2_2 U75 ( .ip1(n1573), .ip2(n1574), .op(n57) );
  and2_2 U76 ( .ip1(n1595), .ip2(n1596), .op(n58) );
  and2_2 U77 ( .ip1(n1617), .ip2(n1618), .op(n59) );
  and2_2 U78 ( .ip1(n1639), .ip2(n1640), .op(n60) );
  and2_2 U79 ( .ip1(n1661), .ip2(n1662), .op(n61) );
  and2_2 U80 ( .ip1(n1683), .ip2(n1684), .op(n62) );
  and2_2 U81 ( .ip1(n1705), .ip2(n1706), .op(n63) );
  and2_2 U82 ( .ip1(n1727), .ip2(n1728), .op(n64) );
  and2_2 U83 ( .ip1(n1749), .ip2(n1750), .op(n65) );
  and2_2 U84 ( .ip1(n1771), .ip2(n1772), .op(n66) );
  and2_2 U85 ( .ip1(n1793), .ip2(n1794), .op(n67) );
  and2_2 U86 ( .ip1(n1815), .ip2(n1816), .op(n68) );
  and2_2 U87 ( .ip1(n1837), .ip2(n1838), .op(n69) );
  and2_2 U88 ( .ip1(n1859), .ip2(n1860), .op(n70) );
  and2_2 U89 ( .ip1(n1881), .ip2(n1882), .op(n71) );
  and2_2 U90 ( .ip1(n1903), .ip2(n1904), .op(n72) );
  and2_2 U91 ( .ip1(n1925), .ip2(n1926), .op(n73) );
  and2_2 U92 ( .ip1(n1947), .ip2(n1948), .op(n74) );
  and2_2 U93 ( .ip1(n1969), .ip2(n1970), .op(n75) );
  and2_2 U94 ( .ip1(n1991), .ip2(n1992), .op(n76) );
  and2_2 U95 ( .ip1(n2013), .ip2(n2014), .op(n77) );
  and2_2 U96 ( .ip1(n2035), .ip2(n2036), .op(n78) );
  and2_2 U97 ( .ip1(n2057), .ip2(n2058), .op(n79) );
  and2_2 U98 ( .ip1(n2079), .ip2(n2080), .op(n80) );
  and2_2 U99 ( .ip1(n2101), .ip2(n2102), .op(n81) );
  and2_2 U100 ( .ip1(n2123), .ip2(n2124), .op(n82) );
  and2_2 U101 ( .ip1(n2145), .ip2(n2146), .op(n83) );
  and2_2 U102 ( .ip1(n2167), .ip2(n2168), .op(n84) );
  and2_2 U103 ( .ip1(n2189), .ip2(n2190), .op(n85) );
  and2_2 U104 ( .ip1(n2211), .ip2(n2212), .op(n86) );
  and2_2 U105 ( .ip1(n2233), .ip2(n2234), .op(n87) );
  and2_2 U106 ( .ip1(n2255), .ip2(n2256), .op(n88) );
  and2_2 U107 ( .ip1(n2277), .ip2(n2278), .op(n89) );
  and2_2 U108 ( .ip1(n2497), .ip2(n2498), .op(n90) );
  and2_2 U109 ( .ip1(n2519), .ip2(n2520), .op(n91) );
  and2_2 U110 ( .ip1(n2541), .ip2(n2542), .op(n92) );
  and2_2 U111 ( .ip1(n2607), .ip2(n2608), .op(n93) );
  and2_2 U112 ( .ip1(n2629), .ip2(n2630), .op(n94) );
  and2_2 U113 ( .ip1(n2739), .ip2(n2740), .op(n95) );
  and2_2 U114 ( .ip1(n2761), .ip2(n2762), .op(n96) );
  and2_2 U115 ( .ip1(n2783), .ip2(n2784), .op(n97) );
  and2_2 U116 ( .ip1(n2805), .ip2(n2806), .op(n98) );
  and2_2 U117 ( .ip1(n2827), .ip2(n2828), .op(n99) );
  and2_2 U118 ( .ip1(n2871), .ip2(n2872), .op(n100) );
  and2_2 U119 ( .ip1(n2893), .ip2(n2894), .op(n101) );
  and2_2 U120 ( .ip1(n2915), .ip2(n2916), .op(n102) );
  and2_2 U121 ( .ip1(n2949), .ip2(n2950), .op(n103) );
  and2_2 U122 ( .ip1(n788), .ip2(n789), .op(n104) );
  and2_1 U123 ( .ip1(n437), .ip2(n438), .op(n105) );
  and2_1 U124 ( .ip1(n478), .ip2(n479), .op(n106) );
  nor2_1 U125 ( .ip1(n184), .ip2(n366), .op(n107) );
  and2_2 U126 ( .ip1(n2937), .ip2(n2940), .op(n108) );
  and2_2 U127 ( .ip1(csr[14]), .ip2(n285), .op(n109) );
  nor2_2 U128 ( .ip1(n793), .ip2(n792), .op(n110) );
  nor2_2 U129 ( .ip1(n802), .ip2(n801), .op(n111) );
  nor2_2 U130 ( .ip1(n777), .ip2(n776), .op(n112) );
  nor2_2 U131 ( .ip1(n664), .ip2(n663), .op(n113) );
  nor2_2 U132 ( .ip1(n672), .ip2(n671), .op(n114) );
  nor2_2 U133 ( .ip1(n680), .ip2(n679), .op(n115) );
  nor2_2 U134 ( .ip1(n688), .ip2(n687), .op(n116) );
  nand2_2 U135 ( .ip1(frm_nat[20]), .ip2(n275), .op(n117) );
  nand2_2 U136 ( .ip1(frm_nat[28]), .ip2(n274), .op(n118) );
  nor2_2 U137 ( .ip1(n809), .ip2(n808), .op(n119) );
  nor2_1 U138 ( .ip1(n387), .ip2(n388), .op(n389) );
  nand3_1 U139 ( .ip1(n175), .ip2(n176), .ip3(n177), .op(n387) );
  nand2_1 U140 ( .ip1(n389), .ip2(n390), .op(N731) );
  nand2_1 U141 ( .ip1(n754), .ip2(intb_msk[2]), .op(n674) );
  nand2_1 U142 ( .ip1(n754), .ip2(intb_msk[3]), .op(n682) );
  nand2_1 U143 ( .ip1(n277), .ip2(dtmp[18]), .op(n171) );
  nand2_1 U144 ( .ip1(n277), .ip2(dtmp[19]), .op(n174) );
  inv_1 U145 ( .ip(intb_msk[0]), .op(n120) );
  inv_1 U146 ( .ip(n120), .op(n121) );
  nand2_1 U147 ( .ip1(n754), .ip2(intb_msk[1]), .op(n666) );
  nand2_1 U148 ( .ip1(n754), .ip2(intb_msk[0]), .op(n658) );
  nand2_1 U149 ( .ip1(n674), .ip2(n673), .op(N89) );
  nand2_1 U150 ( .ip1(n682), .ip2(n681), .op(N90) );
  nand2_1 U151 ( .ip1(n803), .ip2(int_srcb[8]), .op(n146) );
  buf_1 U152 ( .ip(int_srcb[5]), .op(n122) );
  nand2_1 U153 ( .ip1(int_srcb[5]), .ip2(n803), .op(n770) );
  and2_1 U154 ( .ip1(n363), .ip2(n362), .op(n3245) );
  nand2_1 U155 ( .ip1(n803), .ip2(int_srcb[6]), .op(n778) );
  nand2_1 U156 ( .ip1(n666), .ip2(n665), .op(N88) );
  nand2_1 U157 ( .ip1(n277), .ip2(dtmp[17]), .op(n168) );
  nand2_1 U158 ( .ip1(n658), .ip2(n657), .op(N87) );
  nand2_1 U159 ( .ip1(n277), .ip2(dtmp[16]), .op(n165) );
  nand2_1 U160 ( .ip1(n803), .ip2(int_srcb[7]), .op(n795) );
  nand2_1 U161 ( .ip1(n278), .ip2(dtmp[28]), .op(n128) );
  nand2_1 U162 ( .ip1(n770), .ip2(n771), .op(N96) );
  nand2_1 U163 ( .ip1(n278), .ip2(dtmp[25]), .op(n158) );
  nand2_1 U164 ( .ip1(n778), .ip2(n784), .op(N97) );
  nand2_1 U165 ( .ip1(n278), .ip2(dtmp[26]), .op(n160) );
  nand2_1 U166 ( .ip1(n795), .ip2(n796), .op(N98) );
  nand2_1 U167 ( .ip1(n278), .ip2(dtmp[27]), .op(n162) );
  inv_1 U168 ( .ip(inta_msk[8]), .op(n123) );
  inv_1 U169 ( .ip(n123), .op(n124) );
  nand2_1 U170 ( .ip1(n276), .ip2(dtmp[8]), .op(n572) );
  nand3_1 U171 ( .ip1(n572), .ip2(n573), .ip3(n574), .op(n3231) );
  nand2_1 U172 ( .ip1(n277), .ip2(dtmp[20]), .op(n697) );
  nand3_1 U173 ( .ip1(n697), .ip2(n698), .ip3(n699), .op(n3219) );
  nand2_1 U174 ( .ip1(n277), .ip2(dtmp[23]), .op(n751) );
  nand2_1 U175 ( .ip1(n277), .ip2(dtmp[22]), .op(n732) );
  nand2_1 U176 ( .ip1(n277), .ip2(dtmp[21]), .op(n708) );
  nand3_1 U177 ( .ip1(n751), .ip2(n752), .ip3(n753), .op(n3216) );
  nand3_1 U178 ( .ip1(n732), .ip2(n736), .ip3(n737), .op(n3217) );
  nand3_1 U179 ( .ip1(n708), .ip2(n709), .ip3(n710), .op(n3218) );
  nand2_1 U180 ( .ip1(n278), .ip2(dtmp[24]), .op(n767) );
  nand3_1 U181 ( .ip1(n767), .ip2(n768), .ip3(n769), .op(n3215) );
  nand3_2 U182 ( .ip1(n151), .ip2(n150), .ip3(n152), .op(n149) );
  inv_1 U183 ( .ip(inta_msk[7]), .op(n125) );
  inv_1 U184 ( .ip(n125), .op(n126) );
  nand2_1 U185 ( .ip1(n754), .ip2(inta_msk[7]), .op(n551) );
  nand3_1 U186 ( .ip1(n561), .ip2(n562), .ip3(n563), .op(n3232) );
  nand2_1 U187 ( .ip1(n276), .ip2(dtmp[7]), .op(n561) );
  nand2_1 U188 ( .ip1(n549), .ip2(utmi_vend_stat_r[4]), .op(n499) );
  nand3_1 U189 ( .ip1(n128), .ip2(n5), .ip3(n119), .op(n127) );
  nand2_1 U190 ( .ip1(n349), .ip2(n348), .op(n3367) );
  nand3_1 U191 ( .ip1(n455), .ip2(n454), .ip3(n456), .op(n457) );
  nand2_1 U192 ( .ip1(n514), .ip2(n877), .op(n454) );
  nor2_2 U193 ( .ip1(n400), .ip2(n256), .op(n407) );
  nand2_1 U194 ( .ip1(n841), .ip2(dtmp[0]), .op(n426) );
  nand2_1 U195 ( .ip1(n754), .ip2(inta_msk[2]), .op(n455) );
  nand3_1 U196 ( .ip1(n467), .ip2(n468), .ip3(n469), .op(n3237) );
  inv_1 U197 ( .ip(n134), .op(n534) );
  nand2_1 U198 ( .ip1(n841), .ip2(dtmp[6]), .op(n546) );
  inv_1 U199 ( .ip(n878), .op(n135) );
  inv_2 U200 ( .ip(n135), .op(funct_adr[0]) );
  nand4_1 U201 ( .ip1(n534), .ip2(n536), .ip3(n535), .ip4(n537), .op(N77) );
  nor2_2 U202 ( .ip1(n515), .ip2(n137), .op(n256) );
  nand2_1 U203 ( .ip1(n754), .ip2(intb_msk[4]), .op(n141) );
  nand3_1 U204 ( .ip1(n141), .ip2(n117), .ip3(n140), .op(n139) );
  nand2_1 U205 ( .ip1(funct_adr[1]), .ip2(n514), .op(n435) );
  nand2_1 U206 ( .ip1(n549), .ip2(utmi_vend_stat_r[5]), .op(n138) );
  nand2_1 U207 ( .ip1(int_srcb[0]), .ip2(n803), .op(n140) );
  and2_1 U208 ( .ip1(n142), .ip2(n143), .op(n519) );
  nand2_1 U209 ( .ip1(n754), .ip2(inta_msk[5]), .op(n142) );
  or2_2 U210 ( .ip1(n840), .ip2(n513), .op(n143) );
  nand3_1 U211 ( .ip1(n436), .ip2(n435), .ip3(n105), .op(N72) );
  nand2_1 U212 ( .ip1(int_srca[3]), .ip2(n803), .op(n476) );
  inv_1 U213 ( .ip(inta_msk[5]), .op(n512) );
  nand2_1 U214 ( .ip1(n145), .ip2(n498), .op(N75) );
  inv_1 U215 ( .ip(n144), .op(n145) );
  nand2_1 U216 ( .ip1(n146), .ip2(n118), .op(N99) );
  nand2_1 U217 ( .ip1(intb_msk[5]), .ip2(n754), .op(n151) );
  nand2_1 U218 ( .ip1(n754), .ip2(inta_msk[8]), .op(n155) );
  nand3_1 U219 ( .ip1(n160), .ip2(n7), .ip3(n110), .op(n159) );
  nand3_1 U220 ( .ip1(n162), .ip2(n6), .ip3(n111), .op(n161) );
  inv_1 U221 ( .ip(n241), .op(n157) );
  nand3_1 U222 ( .ip1(n158), .ip2(n157), .ip3(n112), .op(n156) );
  inv_1 U223 ( .ip(n242), .op(n164) );
  nand3_1 U224 ( .ip1(n165), .ip2(n164), .ip3(n113), .op(n163) );
  inv_1 U225 ( .ip(n243), .op(n167) );
  nand3_1 U226 ( .ip1(n168), .ip2(n167), .ip3(n114), .op(n166) );
  inv_1 U227 ( .ip(n244), .op(n170) );
  nand3_1 U228 ( .ip1(n171), .ip2(n170), .ip3(n115), .op(n169) );
  inv_1 U229 ( .ip(n245), .op(n173) );
  nand3_1 U230 ( .ip1(n174), .ip2(n173), .ip3(n116), .op(n172) );
  inv_1 U231 ( .ip(n250), .op(n187) );
  inv_1 U232 ( .ip(n251), .op(n188) );
  nor2_1 U233 ( .ip1(n238), .ip2(crc5_err_r), .op(n184) );
  and2_1 U234 ( .ip1(suspend_r), .ip2(n360), .op(n246) );
  and2_1 U235 ( .ip1(attach_r), .ip2(n364), .op(n247) );
  inv_1 U236 ( .ip(n248), .op(n185) );
  inv_1 U237 ( .ip(n249), .op(n186) );
  inv_1 U238 ( .ip(n252), .op(n189) );
  inv_1 U239 ( .ip(n253), .op(n190) );
  inv_1 U240 ( .ip(inta_msk[0]), .op(n147) );
  inv_1 U241 ( .ip(n147), .op(n148) );
  nand3_1 U242 ( .ip1(n477), .ip2(n476), .ip3(n106), .op(N74) );
  nand2_2 U243 ( .ip1(frm_nat[21]), .ip2(n275), .op(n150) );
  nand2_2 U244 ( .ip1(int_srcb[1]), .ip2(n803), .op(n152) );
  nand2_2 U245 ( .ip1(frm_nat[8]), .ip2(n275), .op(n154) );
  nand2_1 U246 ( .ip1(inta_msk[6]), .ip2(int_srcb[6]), .op(n175) );
  nand2_1 U247 ( .ip1(n257), .ip2(n122), .op(n176) );
  nand2_1 U248 ( .ip1(int_srcb[7]), .ip2(n126), .op(n177) );
  nand3_2 U249 ( .ip1(n178), .ip2(n179), .ip3(n180), .op(n386) );
  nand2_1 U250 ( .ip1(n270), .ip2(int_srcb[3]), .op(n178) );
  nand2_1 U251 ( .ip1(n261), .ip2(int_srcb[2]), .op(n179) );
  nand2_1 U252 ( .ip1(n272), .ip2(int_srcb[4]), .op(n180) );
  or2_1 U253 ( .ip1(n246), .ip2(int_srcb[3]), .op(n359) );
  or2_1 U254 ( .ip1(n247), .ip2(n122), .op(n362) );
  nand3_2 U255 ( .ip1(n181), .ip2(n182), .ip3(n183), .op(n371) );
  nand2_1 U256 ( .ip1(intb_msk[4]), .ip2(int_srcb[4]), .op(n181) );
  nand2_1 U257 ( .ip1(intb_msk[3]), .ip2(int_srcb[3]), .op(n182) );
  nand2_1 U258 ( .ip1(intb_msk[5]), .ip2(int_srcb[5]), .op(n183) );
  nor2_1 U259 ( .ip1(n185), .ip2(n366), .op(n3241) );
  nor2_1 U260 ( .ip1(n186), .ip2(n366), .op(n3242) );
  nor2_1 U261 ( .ip1(n187), .ip2(n366), .op(n3244) );
  nor2_1 U262 ( .ip1(n188), .ip2(n366), .op(n3246) );
  nor2_1 U263 ( .ip1(n189), .ip2(n366), .op(n3247) );
  nor2_1 U264 ( .ip1(n190), .ip2(n366), .op(n3248) );
  nand3_2 U265 ( .ip1(n191), .ip2(n192), .ip3(n193), .op(n372) );
  nand2_1 U266 ( .ip1(intb_msk[7]), .ip2(int_srcb[7]), .op(n191) );
  nand2_1 U267 ( .ip1(intb_msk[6]), .ip2(int_srcb[6]), .op(n192) );
  nand2_1 U268 ( .ip1(intb_msk[8]), .ip2(int_srcb[8]), .op(n193) );
  nand3_2 U269 ( .ip1(n194), .ip2(n195), .ip3(n196), .op(n370) );
  nand2_1 U270 ( .ip1(intb_msk[1]), .ip2(int_srcb[1]), .op(n194) );
  nand2_1 U271 ( .ip1(n121), .ip2(n238), .op(n195) );
  nand2_1 U272 ( .ip1(intb_msk[2]), .ip2(int_srcb[2]), .op(n196) );
  nand2_1 U273 ( .ip1(n197), .ip2(n198), .op(n3352) );
  nand2_1 U274 ( .ip1(n254), .ip2(n257), .op(n197) );
  nand2_2 U275 ( .ip1(din[5]), .ip2(n381), .op(n198) );
  nand2_1 U276 ( .ip1(n199), .ip2(n200), .op(n3349) );
  nand2_1 U277 ( .ip1(n254), .ip2(n261), .op(n199) );
  nand2_2 U278 ( .ip1(din[2]), .ip2(n381), .op(n200) );
  nand2_1 U279 ( .ip1(n201), .ip2(n202), .op(n3366) );
  nand2_1 U280 ( .ip1(n235), .ip2(funct_adr[1]), .op(n201) );
  nand2_2 U281 ( .ip1(n354), .ip2(din[1]), .op(n202) );
  nand2_1 U282 ( .ip1(n203), .ip2(n204), .op(n3347) );
  nand2_1 U283 ( .ip1(n254), .ip2(n148), .op(n203) );
  nand2_2 U284 ( .ip1(din[0]), .ip2(n381), .op(n204) );
  nand2_1 U285 ( .ip1(n205), .ip2(n206), .op(n3365) );
  nand2_1 U286 ( .ip1(n235), .ip2(funct_adr[0]), .op(n205) );
  nand2_2 U287 ( .ip1(n354), .ip2(din[0]), .op(n206) );
  nand2_1 U288 ( .ip1(n207), .ip2(n208), .op(n3370) );
  nand2_1 U289 ( .ip1(n235), .ip2(funct_adr[5]), .op(n207) );
  nand2_2 U290 ( .ip1(n354), .ip2(din[5]), .op(n208) );
  nand2_1 U291 ( .ip1(n209), .ip2(n210), .op(n3371) );
  nand2_1 U292 ( .ip1(n235), .ip2(funct_adr[6]), .op(n209) );
  nand2_2 U293 ( .ip1(n354), .ip2(din[6]), .op(n210) );
  nand2_1 U294 ( .ip1(n211), .ip2(n212), .op(n3360) );
  nand2_1 U295 ( .ip1(n254), .ip2(intb_msk[4]), .op(n211) );
  nand2_2 U296 ( .ip1(din[20]), .ip2(n381), .op(n212) );
  nand2_1 U297 ( .ip1(n213), .ip2(n214), .op(n3361) );
  nand2_1 U298 ( .ip1(n254), .ip2(intb_msk[5]), .op(n213) );
  nand2_2 U299 ( .ip1(din[21]), .ip2(n381), .op(n214) );
  nand2_1 U300 ( .ip1(n215), .ip2(n216), .op(n3353) );
  nand2_1 U301 ( .ip1(n254), .ip2(inta_msk[6]), .op(n215) );
  nand2_2 U302 ( .ip1(din[6]), .ip2(n381), .op(n216) );
  nand2_1 U303 ( .ip1(n217), .ip2(n218), .op(n3354) );
  nand2_1 U304 ( .ip1(n254), .ip2(n126), .op(n217) );
  nand2_2 U305 ( .ip1(din[7]), .ip2(n381), .op(n218) );
  nand2_1 U306 ( .ip1(n219), .ip2(n220), .op(n3356) );
  nand2_1 U307 ( .ip1(n254), .ip2(n121), .op(n219) );
  nand2_2 U308 ( .ip1(din[16]), .ip2(n381), .op(n220) );
  nand2_1 U309 ( .ip1(n221), .ip2(n222), .op(n3357) );
  nand2_1 U310 ( .ip1(n254), .ip2(intb_msk[1]), .op(n221) );
  nand2_2 U311 ( .ip1(din[17]), .ip2(n381), .op(n222) );
  nand2_1 U312 ( .ip1(n223), .ip2(n224), .op(n3358) );
  nand2_1 U313 ( .ip1(n254), .ip2(intb_msk[2]), .op(n223) );
  nand2_2 U314 ( .ip1(din[18]), .ip2(n381), .op(n224) );
  nand2_1 U315 ( .ip1(n225), .ip2(n226), .op(n3359) );
  nand2_1 U316 ( .ip1(n254), .ip2(intb_msk[3]), .op(n225) );
  nand2_2 U317 ( .ip1(din[19]), .ip2(n381), .op(n226) );
  nand2_1 U318 ( .ip1(n227), .ip2(n228), .op(n3362) );
  nand2_1 U319 ( .ip1(n254), .ip2(intb_msk[6]), .op(n227) );
  nand2_2 U320 ( .ip1(din[22]), .ip2(n381), .op(n228) );
  nand2_1 U321 ( .ip1(n229), .ip2(n230), .op(n3363) );
  nand2_1 U322 ( .ip1(n254), .ip2(intb_msk[7]), .op(n229) );
  nand2_2 U323 ( .ip1(din[23]), .ip2(n381), .op(n230) );
  nand2_1 U324 ( .ip1(n231), .ip2(n232), .op(n3364) );
  nand2_1 U325 ( .ip1(n254), .ip2(intb_msk[8]), .op(n231) );
  nand2_2 U326 ( .ip1(din[24]), .ip2(n381), .op(n232) );
  nand2_1 U327 ( .ip1(n233), .ip2(n234), .op(n3355) );
  nand2_1 U328 ( .ip1(n254), .ip2(n124), .op(n233) );
  nand2_2 U329 ( .ip1(din[8]), .ip2(n381), .op(n234) );
  and2_2 U330 ( .ip1(n347), .ip2(n357), .op(n235) );
  nor3_2 U331 ( .ip1(adr[5]), .ip2(adr[4]), .ip3(adr[6]), .op(n236) );
  inv_1 U332 ( .ip(int_srcb[0]), .op(n237) );
  inv_2 U333 ( .ip(n237), .op(n238) );
  inv_2 U334 ( .ip(n4), .op(n316) );
  inv_2 U335 ( .ip(n3), .op(n314) );
  inv_2 U336 ( .ip(n4), .op(n317) );
  inv_2 U337 ( .ip(n3), .op(n315) );
  inv_2 U338 ( .ip(n319), .op(n318) );
  buf_2 U339 ( .ip(n108), .op(n279) );
  buf_2 U340 ( .ip(n108), .op(n280) );
  buf_2 U341 ( .ip(n108), .op(n281) );
  buf_2 U342 ( .ip(n108), .op(n282) );
  buf_2 U343 ( .ip(n108), .op(n283) );
  buf_2 U344 ( .ip(n108), .op(n284) );
  buf_2 U345 ( .ip(n108), .op(n285) );
  buf_2 U346 ( .ip(n108), .op(n286) );
  inv_2 U347 ( .ip(we), .op(n861) );
  inv_2 U348 ( .ip(re), .op(n860) );
  buf_2 U349 ( .ip(n783), .op(n312) );
  buf_2 U350 ( .ip(n783), .op(n311) );
  buf_2 U351 ( .ip(n783), .op(n310) );
  buf_2 U352 ( .ip(n783), .op(n309) );
  buf_2 U353 ( .ip(n783), .op(n308) );
  buf_2 U354 ( .ip(n783), .op(n307) );
  buf_2 U355 ( .ip(n783), .op(n306) );
  buf_2 U356 ( .ip(n783), .op(n305) );
  buf_2 U357 ( .ip(n785), .op(n303) );
  buf_2 U358 ( .ip(n785), .op(n302) );
  buf_2 U359 ( .ip(n785), .op(n301) );
  buf_2 U360 ( .ip(n785), .op(n300) );
  buf_2 U361 ( .ip(n785), .op(n299) );
  buf_2 U362 ( .ip(n785), .op(n298) );
  buf_2 U363 ( .ip(n785), .op(n297) );
  buf_2 U364 ( .ip(n785), .op(n296) );
  buf_2 U365 ( .ip(n794), .op(n294) );
  buf_2 U366 ( .ip(n794), .op(n293) );
  buf_2 U367 ( .ip(n794), .op(n292) );
  buf_2 U368 ( .ip(n794), .op(n291) );
  buf_2 U369 ( .ip(n794), .op(n290) );
  buf_2 U370 ( .ip(n794), .op(n289) );
  buf_2 U371 ( .ip(n794), .op(n288) );
  buf_2 U372 ( .ip(n794), .op(n287) );
  buf_2 U373 ( .ip(n841), .op(n277) );
  inv_2 U374 ( .ip(n722), .op(n319) );
  inv_2 U375 ( .ip(n840), .op(n274) );
  buf_2 U376 ( .ip(n841), .op(n276) );
  inv_2 U377 ( .ip(n240), .op(n273) );
  inv_2 U378 ( .ip(n840), .op(n275) );
  nor3_2 U379 ( .ip1(n405), .ip2(n404), .ip3(n403), .op(n406) );
  nor3_2 U380 ( .ip1(n320), .ip2(ep1_match), .ip3(n871), .op(n783) );
  nor2_2 U381 ( .ip1(n864), .ip2(n327), .op(n785) );
  nor2_2 U382 ( .ip1(n872), .ip2(n239), .op(n794) );
  inv_2 U383 ( .ip(n358), .op(n381) );
  inv_2 U384 ( .ip(n531), .op(n754) );
  buf_2 U385 ( .ip(ep0_match), .op(n326) );
  buf_2 U386 ( .ip(ep0_match), .op(n325) );
  buf_2 U387 ( .ip(ep0_match), .op(n324) );
  buf_2 U388 ( .ip(ep0_match), .op(n322) );
  buf_2 U389 ( .ip(ep0_match), .op(n323) );
  buf_2 U390 ( .ip(ep0_match), .op(n321) );
  buf_2 U391 ( .ip(ep0_match), .op(n320) );
  inv_2 U392 ( .ip(n515), .op(n803) );
  or3_2 U393 ( .ip1(ep1_match), .ip2(ep2_match), .ip3(n320), .op(n239) );
  and2_2 U394 ( .ip1(n3051), .ip2(n3046), .op(n240) );
  nor3_2 U395 ( .ip1(n392), .ip2(n860), .ip3(n424), .op(N103) );
  inv_2 U396 ( .ip(n262), .op(n405) );
  and2_1 U397 ( .ip1(dout[25]), .ip2(n2), .op(n241) );
  and2_1 U398 ( .ip1(dout[16]), .ip2(n2), .op(n242) );
  and2_1 U399 ( .ip1(dout[17]), .ip2(n2), .op(n243) );
  and2_1 U400 ( .ip1(dout[18]), .ip2(n2), .op(n244) );
  and2_1 U401 ( .ip1(dout[19]), .ip2(n2), .op(n245) );
  or2_1 U402 ( .ip1(int_srcb[1]), .ip2(pid_cs_err_r), .op(n248) );
  or2_1 U403 ( .ip1(int_srcb[2]), .ip2(nse_err_r), .op(n249) );
  or2_1 U404 ( .ip1(int_srcb[4]), .ip2(n361), .op(n250) );
  or2_1 U405 ( .ip1(int_srcb[6]), .ip2(n365), .op(n251) );
  or2_1 U406 ( .ip1(int_srcb[7]), .ip2(rx_err_r), .op(n252) );
  or2_1 U407 ( .ip1(usb_reset_r), .ip2(int_srcb[8]), .op(n253) );
  nor3_2 U408 ( .ip1(n820), .ip2(n819), .ip3(n818), .op(n822) );
  nor3_2 U409 ( .ip1(n834), .ip2(n833), .ip3(n832), .op(n835) );
  nor3_2 U410 ( .ip1(n853), .ip2(n852), .ip3(n851), .op(n854) );
  nor3_2 U411 ( .ip1(n652), .ip2(n651), .ip3(n650), .op(n653) );
  nor3_2 U412 ( .ip1(n581), .ip2(n580), .ip3(n579), .op(n582) );
  nand2_2 U413 ( .ip1(n394), .ip2(adr[2]), .op(n840) );
  and2_2 U414 ( .ip1(n358), .ip2(n357), .op(n254) );
  nor3_2 U415 ( .ip1(adr[5]), .ip2(adr[6]), .ip3(n874), .op(n3051) );
  nor3_2 U416 ( .ip1(n874), .ip2(adr[6]), .ip3(n873), .op(n3045) );
  nor3_2 U417 ( .ip1(adr[4]), .ip2(adr[6]), .ip3(n873), .op(n3047) );
  cd_8 U418 ( .ip(n332), .op(n255) );
  nand2_1 U419 ( .ip1(n376), .ip2(n375), .op(n3348) );
  nand2_1 U420 ( .ip1(n378), .ip2(n377), .op(n3350) );
  nand2_1 U421 ( .ip1(n380), .ip2(n379), .op(n3351) );
  nand2_1 U422 ( .ip1(n549), .ip2(utmi_vend_stat_r[6]), .op(n533) );
  inv_1 U423 ( .ip(n512), .op(n257) );
  nand2_1 U424 ( .ip1(n754), .ip2(inta_msk[1]), .op(n434) );
  nand2_1 U425 ( .ip1(n754), .ip2(inta_msk[3]), .op(n474) );
  nand2_1 U426 ( .ip1(n514), .ip2(n876), .op(n475) );
  nand2_1 U427 ( .ip1(n754), .ip2(inta_msk[4]), .op(n496) );
  nand2_1 U428 ( .ip1(n514), .ip2(n875), .op(n497) );
  inv_1 U429 ( .ip(n877), .op(n258) );
  inv_2 U430 ( .ip(n258), .op(funct_adr[2]) );
  and2_1 U431 ( .ip1(n434), .ip2(n433), .op(n436) );
  and2_1 U432 ( .ip1(n474), .ip2(n475), .op(n477) );
  and2_1 U433 ( .ip1(n496), .ip2(n497), .op(n498) );
  nand3_1 U434 ( .ip1(n546), .ip2(n547), .ip3(n548), .op(n3233) );
  inv_1 U435 ( .ip(inta_msk[2]), .op(n260) );
  inv_2 U436 ( .ip(n260), .op(n261) );
  nand2_1 U437 ( .ip1(n276), .ip2(dtmp[2]), .op(n467) );
  nand2_1 U438 ( .ip1(n276), .ip2(dtmp[1]), .op(n447) );
  nand2_1 U439 ( .ip1(n276), .ip2(dtmp[3]), .op(n488) );
  nand2_1 U440 ( .ip1(n276), .ip2(dtmp[4]), .op(n509) );
  nand2_2 U441 ( .ip1(n549), .ip2(utmi_vend_stat_r[0]), .op(n262) );
  inv_1 U442 ( .ip(n269), .op(n270) );
  nand2_1 U443 ( .ip1(n254), .ip2(n270), .op(n378) );
  inv_1 U444 ( .ip(n271), .op(n272) );
  nand2_1 U445 ( .ip1(n254), .ip2(n272), .op(n380) );
  inv_1 U446 ( .ip(n263), .op(n264) );
  nand2_1 U447 ( .ip1(n254), .ip2(n264), .op(n376) );
  nand2_1 U448 ( .ip1(n264), .ip2(int_srcb[1]), .op(n384) );
  or2_1 U449 ( .ip1(n458), .ip2(n457), .op(N73) );
  nand3_1 U450 ( .ip1(n488), .ip2(n489), .ip3(n490), .op(n3236) );
  nand3_1 U451 ( .ip1(n509), .ip2(n510), .ip3(n511), .op(n3235) );
  nand3_1 U452 ( .ip1(n447), .ip2(n448), .ip3(n449), .op(n3238) );
  nand3_1 U453 ( .ip1(n426), .ip2(n427), .ip3(n428), .op(n3239) );
  nand2_1 U454 ( .ip1(n276), .ip2(dtmp[5]), .op(n528) );
  nand3_1 U455 ( .ip1(n530), .ip2(n529), .ip3(n528), .op(n3234) );
  nand2_1 U456 ( .ip1(n407), .ip2(n406), .op(N71) );
  nand3_1 U457 ( .ip1(n519), .ip2(n518), .ip3(n517), .op(N76) );
  inv_1 U458 ( .ip(inta_msk[1]), .op(n263) );
  inv_1 U459 ( .ip(n876), .op(n265) );
  inv_2 U460 ( .ip(n265), .op(funct_adr[3]) );
  inv_1 U461 ( .ip(n875), .op(n267) );
  inv_2 U462 ( .ip(n267), .op(funct_adr[4]) );
  inv_1 U463 ( .ip(inta_msk[3]), .op(n269) );
  inv_1 U464 ( .ip(inta_msk[4]), .op(n271) );
  buf_1 U465 ( .ip(n841), .op(n278) );
  buf_1 U466 ( .ip(n794), .op(n295) );
  buf_1 U467 ( .ip(n785), .op(n304) );
  buf_1 U468 ( .ip(n783), .op(n313) );
  buf_1 U469 ( .ip(ep0_match), .op(n327) );
  buf_1 U470 ( .ip(N70), .op(n328) );
  buf_1 U471 ( .ip(N70), .op(n329) );
  buf_1 U472 ( .ip(N70), .op(n330) );
  buf_1 U473 ( .ip(N70), .op(n331) );
  cd_8 U474 ( .ip(clk), .op(n332) );
  inv_2 U475 ( .ip(adr[3]), .op(n858) );
  inv_2 U476 ( .ip(adr[2]), .op(n859) );
  inv_2 U477 ( .ip(rf_resume_req), .op(n333) );
  nand2_2 U478 ( .ip1(rf_resume_req_r), .ip2(n333), .op(n337) );
  inv_2 U479 ( .ip(din[5]), .op(n336) );
  or2_2 U480 ( .ip1(adr[1]), .ip2(adr[0]), .op(n391) );
  nand2_2 U481 ( .ip1(n3046), .ip2(n236), .op(n424) );
  inv_2 U482 ( .ip(n424), .op(n334) );
  nand2_2 U483 ( .ip1(n334), .ip2(we), .op(n345) );
  nor2_2 U484 ( .ip1(n391), .ip2(n345), .op(n335) );
  mux2_1 U485 ( .ip1(n337), .ip2(n336), .s(n335), .op(n338) );
  nor2_2 U486 ( .ip1(n764), .ip2(n338), .op(n3372) );
  inv_2 U487 ( .ip(utmi_vend_wr), .op(n339) );
  nand2_2 U488 ( .ip1(utmi_vend_wr_r), .ip2(n339), .op(n341) );
  inv_2 U489 ( .ip(adr[1]), .op(n393) );
  nand3_2 U490 ( .ip1(adr[0]), .ip2(adr[2]), .ip3(n393), .op(n516) );
  inv_2 U491 ( .ip(n516), .op(n549) );
  nor2_2 U492 ( .ip1(adr[3]), .ip2(n861), .op(n340) );
  nand3_2 U493 ( .ip1(n549), .ip2(n236), .ip3(n340), .op(n343) );
  nand2_2 U494 ( .ip1(n341), .ip2(n343), .op(n342) );
  inv_2 U495 ( .ip(n764), .op(n357) );
  and2_2 U496 ( .ip1(n342), .ip2(n357), .op(n3373) );
  inv_2 U497 ( .ip(n343), .op(n344) );
  mux2_1 U498 ( .ip1(utmi_vend_ctrl_r[0]), .ip2(din[0]), .s(n344), .op(n3195)
         );
  mux2_1 U499 ( .ip1(utmi_vend_ctrl_r[1]), .ip2(din[1]), .s(n344), .op(n3196)
         );
  mux2_1 U500 ( .ip1(utmi_vend_ctrl_r[2]), .ip2(din[2]), .s(n344), .op(n3197)
         );
  mux2_1 U501 ( .ip1(utmi_vend_ctrl_r[3]), .ip2(din[3]), .s(n344), .op(n3198)
         );
  inv_2 U502 ( .ip(n345), .op(n356) );
  nor2_2 U503 ( .ip1(n764), .ip2(adr[1]), .op(n346) );
  nand3_2 U504 ( .ip1(n356), .ip2(adr[0]), .ip3(n346), .op(n347) );
  inv_2 U505 ( .ip(n347), .op(n354) );
  nand2_2 U506 ( .ip1(n235), .ip2(funct_adr[2]), .op(n349) );
  nand2_2 U507 ( .ip1(n354), .ip2(din[2]), .op(n348) );
  nand2_2 U508 ( .ip1(n235), .ip2(funct_adr[3]), .op(n351) );
  nand2_2 U509 ( .ip1(n354), .ip2(din[3]), .op(n350) );
  nand2_2 U510 ( .ip1(n351), .ip2(n350), .op(n3368) );
  nand2_2 U511 ( .ip1(n235), .ip2(funct_adr[4]), .op(n353) );
  nand2_2 U512 ( .ip1(n354), .ip2(din[4]), .op(n352) );
  nand2_2 U513 ( .ip1(n353), .ip2(n352), .op(n3369) );
  nor2_2 U514 ( .ip1(n764), .ip2(adr[0]), .op(n355) );
  nand3_2 U515 ( .ip1(n356), .ip2(adr[1]), .ip3(n355), .op(n358) );
  nand2_2 U516 ( .ip1(adr[1]), .ip2(adr[0]), .op(n392) );
  or2_2 U517 ( .ip1(int_src_re), .ip2(n764), .op(n366) );
  inv_2 U518 ( .ip(n366), .op(n363) );
  inv_2 U519 ( .ip(suspend_r1), .op(n360) );
  nor2_2 U520 ( .ip1(suspend_r), .ip2(n360), .op(n361) );
  inv_2 U521 ( .ip(attach_r1), .op(n364) );
  nor2_2 U522 ( .ip1(attach_r), .ip2(n364), .op(n365) );
  nor2_2 U523 ( .ip1(ep0_intb), .ip2(ep3_intb), .op(n368) );
  nor2_2 U524 ( .ip1(ep1_intb), .ip2(ep2_intb), .op(n367) );
  nand2_2 U525 ( .ip1(n368), .ip2(n367), .op(n369) );
  nor2_2 U526 ( .ip1(n370), .ip2(n369), .op(n374) );
  nor2_2 U527 ( .ip1(n372), .ip2(n371), .op(n373) );
  nand2_2 U528 ( .ip1(n374), .ip2(n373), .op(N732) );
  nand2_2 U529 ( .ip1(din[1]), .ip2(n381), .op(n375) );
  nand2_2 U530 ( .ip1(din[3]), .ip2(n381), .op(n377) );
  nand2_2 U531 ( .ip1(din[4]), .ip2(n381), .op(n379) );
  nand2_2 U532 ( .ip1(n148), .ip2(n238), .op(n383) );
  nor2_2 U533 ( .ip1(ep1_inta), .ip2(ep2_inta), .op(n382) );
  nand3_2 U534 ( .ip1(n384), .ip2(n383), .ip3(n382), .op(n385) );
  nor2_2 U535 ( .ip1(n386), .ip2(n385), .op(n390) );
  ab_or_c_or_d U536 ( .ip1(n124), .ip2(int_srcb[8]), .ip3(ep0_inta), .ip4(
        ep3_inta), .op(n388) );
  inv_2 U537 ( .ip(n391), .op(n394) );
  nand2_2 U538 ( .ip1(n394), .ip2(n859), .op(n493) );
  or2_2 U539 ( .ip1(adr[2]), .ip2(n392), .op(n515) );
  nand3_2 U540 ( .ip1(n493), .ip2(n515), .ip3(n516), .op(n397) );
  or3_2 U541 ( .ip1(adr[2]), .ip2(n393), .ip3(adr[0]), .op(n531) );
  inv_2 U542 ( .ip(adr[0]), .op(n395) );
  or3_2 U543 ( .ip1(adr[2]), .ip2(n395), .ip3(adr[1]), .op(n532) );
  nand3_2 U544 ( .ip1(n531), .ip2(n840), .ip3(n532), .op(n396) );
  or2_2 U545 ( .ip1(n397), .ip2(n396), .op(N70) );
  inv_2 U546 ( .ip(ep0_intb), .op(n862) );
  nand2_2 U547 ( .ip1(n878), .ip2(n514), .op(n399) );
  nand2_2 U548 ( .ip1(n398), .ip2(n399), .op(n400) );
  inv_2 U549 ( .ip(frm_nat[0]), .op(n401) );
  nor2_2 U550 ( .ip1(n840), .ip2(n401), .op(n404) );
  inv_2 U551 ( .ip(suspend), .op(n402) );
  nor2_2 U552 ( .ip1(n493), .ip2(n402), .op(n403) );
  and2_2 U553 ( .ip1(n721), .ip2(n747), .op(n415) );
  inv_2 U554 ( .ip(n719), .op(n408) );
  nor2_2 U555 ( .ip1(n236), .ip2(n408), .op(n414) );
  nand2_2 U556 ( .ip1(n273), .ip2(n314), .op(n409) );
  nor2_2 U557 ( .ip1(n717), .ip2(n409), .op(n413) );
  nand3_2 U558 ( .ip1(n735), .ip2(n734), .ip3(n742), .op(n411) );
  nand4_2 U559 ( .ip1(n727), .ip2(n726), .ip3(n733), .ip4(n728), .op(n410) );
  nor2_2 U560 ( .ip1(n411), .ip2(n410), .op(n412) );
  inv_2 U561 ( .ip(ep3_dout[0]), .op(n416) );
  nor2_2 U562 ( .ip1(n314), .ip2(n416), .op(n419) );
  inv_2 U563 ( .ip(ep0_dout[0]), .op(n417) );
  nor2_2 U564 ( .ip1(n273), .ip2(n417), .op(n418) );
  not_ab_or_c_or_d U565 ( .ip1(dout[0]), .ip2(n2), .ip3(n419), .ip4(n418), 
        .op(n428) );
  inv_2 U566 ( .ip(ep2_dout[0]), .op(n420) );
  nor2_2 U567 ( .ip1(n318), .ip2(n420), .op(n423) );
  inv_2 U568 ( .ip(ep1_dout[0]), .op(n421) );
  nor2_2 U569 ( .ip1(n316), .ip2(n421), .op(n422) );
  nor2_2 U570 ( .ip1(n423), .ip2(n422), .op(n427) );
  nand2_2 U571 ( .ip1(n712), .ip2(n236), .op(n425) );
  nand2_2 U572 ( .ip1(n425), .ip2(n424), .op(n841) );
  or2_2 U573 ( .ip1(ep1_intb), .ip2(ep1_inta), .op(N729) );
  inv_2 U574 ( .ip(frm_nat[1]), .op(n429) );
  nor2_2 U575 ( .ip1(n840), .ip2(n429), .op(n432) );
  inv_2 U576 ( .ip(mode_hs), .op(n430) );
  nor2_2 U577 ( .ip1(n493), .ip2(n430), .op(n431) );
  nor2_2 U578 ( .ip1(n432), .ip2(n431), .op(n438) );
  nand2_2 U579 ( .ip1(int_srca[1]), .ip2(n803), .op(n437) );
  nand2_2 U580 ( .ip1(utmi_vend_stat_r[1]), .ip2(n549), .op(n433) );
  inv_2 U581 ( .ip(ep3_dout[1]), .op(n439) );
  nor2_2 U582 ( .ip1(n314), .ip2(n439), .op(n442) );
  inv_2 U583 ( .ip(ep0_dout[1]), .op(n440) );
  nor2_2 U584 ( .ip1(n273), .ip2(n440), .op(n441) );
  not_ab_or_c_or_d U585 ( .ip1(dout[1]), .ip2(n2), .ip3(n442), .ip4(n441), 
        .op(n449) );
  inv_2 U586 ( .ip(ep2_dout[1]), .op(n443) );
  nor2_2 U587 ( .ip1(n722), .ip2(n443), .op(n446) );
  inv_2 U588 ( .ip(ep1_dout[1]), .op(n444) );
  nor2_2 U589 ( .ip1(n317), .ip2(n444), .op(n445) );
  nor2_2 U590 ( .ip1(n446), .ip2(n445), .op(n448) );
  or2_2 U591 ( .ip1(ep2_intb), .ip2(ep2_inta), .op(N728) );
  nand2_2 U592 ( .ip1(int_srca[2]), .ip2(n803), .op(n453) );
  nand2_2 U593 ( .ip1(utmi_vend_stat_r[2]), .ip2(n549), .op(n452) );
  inv_2 U594 ( .ip(n493), .op(n450) );
  nand2_2 U595 ( .ip1(usb_attached), .ip2(n450), .op(n451) );
  nand3_2 U596 ( .ip1(n453), .ip2(n452), .ip3(n451), .op(n458) );
  nand2_2 U597 ( .ip1(frm_nat[2]), .ip2(n274), .op(n456) );
  inv_2 U598 ( .ip(ep3_dout[2]), .op(n459) );
  nor2_2 U599 ( .ip1(n314), .ip2(n459), .op(n462) );
  inv_2 U600 ( .ip(ep0_dout[2]), .op(n460) );
  nor2_2 U601 ( .ip1(n273), .ip2(n460), .op(n461) );
  not_ab_or_c_or_d U602 ( .ip1(dout[2]), .ip2(n2), .ip3(n462), .ip4(n461), 
        .op(n469) );
  inv_2 U603 ( .ip(ep2_dout[2]), .op(n463) );
  nor2_2 U604 ( .ip1(n722), .ip2(n463), .op(n466) );
  inv_2 U605 ( .ip(ep1_dout[2]), .op(n464) );
  nor2_2 U606 ( .ip1(n317), .ip2(n464), .op(n465) );
  nor2_2 U607 ( .ip1(n466), .ip2(n465), .op(n468) );
  inv_2 U608 ( .ip(ep3_intb), .op(n863) );
  inv_2 U609 ( .ip(frm_nat[3]), .op(n470) );
  nor2_2 U610 ( .ip1(n840), .ip2(n470), .op(n473) );
  inv_2 U611 ( .ip(line_stat[0]), .op(n471) );
  nor2_2 U612 ( .ip1(n493), .ip2(n471), .op(n472) );
  nor2_2 U613 ( .ip1(n473), .ip2(n472), .op(n479) );
  nand2_2 U614 ( .ip1(utmi_vend_stat_r[3]), .ip2(n549), .op(n478) );
  inv_2 U615 ( .ip(ep3_dout[3]), .op(n480) );
  nor2_2 U616 ( .ip1(n314), .ip2(n480), .op(n483) );
  inv_2 U617 ( .ip(ep0_dout[3]), .op(n481) );
  nor2_2 U618 ( .ip1(n273), .ip2(n481), .op(n482) );
  not_ab_or_c_or_d U619 ( .ip1(dout[3]), .ip2(n2), .ip3(n483), .ip4(n482), 
        .op(n490) );
  inv_2 U620 ( .ip(ep2_dout[3]), .op(n484) );
  nor2_2 U621 ( .ip1(n722), .ip2(n484), .op(n487) );
  inv_2 U622 ( .ip(ep1_dout[3]), .op(n485) );
  nor2_2 U623 ( .ip1(n317), .ip2(n485), .op(n486) );
  nor2_2 U624 ( .ip1(n487), .ip2(n486), .op(n489) );
  inv_2 U625 ( .ip(frm_nat[4]), .op(n491) );
  nor2_2 U626 ( .ip1(n840), .ip2(n491), .op(n495) );
  inv_2 U627 ( .ip(line_stat[1]), .op(n492) );
  nor2_2 U628 ( .ip1(n493), .ip2(n492), .op(n494) );
  inv_2 U629 ( .ip(ep3_dout[4]), .op(n501) );
  nor2_2 U630 ( .ip1(n314), .ip2(n501), .op(n504) );
  inv_2 U631 ( .ip(ep0_dout[4]), .op(n502) );
  nor2_2 U632 ( .ip1(n273), .ip2(n502), .op(n503) );
  not_ab_or_c_or_d U633 ( .ip1(dout[4]), .ip2(n2), .ip3(n504), .ip4(n503), 
        .op(n511) );
  inv_2 U634 ( .ip(ep2_dout[4]), .op(n505) );
  nor2_2 U635 ( .ip1(n722), .ip2(n505), .op(n508) );
  inv_2 U636 ( .ip(ep1_dout[4]), .op(n506) );
  nor2_2 U637 ( .ip1(n317), .ip2(n506), .op(n507) );
  nor2_2 U638 ( .ip1(n508), .ip2(n507), .op(n510) );
  inv_2 U639 ( .ip(frm_nat[5]), .op(n513) );
  inv_2 U640 ( .ip(n532), .op(n514) );
  nand2_2 U641 ( .ip1(funct_adr[5]), .ip2(n514), .op(n518) );
  inv_2 U642 ( .ip(ep3_dout[5]), .op(n520) );
  nor2_2 U643 ( .ip1(n314), .ip2(n520), .op(n523) );
  inv_2 U644 ( .ip(ep0_dout[5]), .op(n521) );
  nor2_2 U645 ( .ip1(n273), .ip2(n521), .op(n522) );
  not_ab_or_c_or_d U646 ( .ip1(dout[5]), .ip2(n2), .ip3(n523), .ip4(n522), 
        .op(n530) );
  inv_2 U647 ( .ip(ep2_dout[5]), .op(n524) );
  nor2_2 U648 ( .ip1(n722), .ip2(n524), .op(n527) );
  inv_2 U649 ( .ip(ep1_dout[5]), .op(n525) );
  nor2_2 U650 ( .ip1(n317), .ip2(n525), .op(n526) );
  nor2_2 U651 ( .ip1(n527), .ip2(n526), .op(n529) );
  nand2_2 U652 ( .ip1(frm_nat[6]), .ip2(n275), .op(n537) );
  nand2_2 U653 ( .ip1(inta_msk[6]), .ip2(n754), .op(n536) );
  nand2_2 U654 ( .ip1(funct_adr[6]), .ip2(n514), .op(n535) );
  inv_2 U655 ( .ip(ep3_dout[6]), .op(n538) );
  nor2_2 U656 ( .ip1(n314), .ip2(n538), .op(n541) );
  inv_2 U657 ( .ip(ep0_dout[6]), .op(n539) );
  nor2_2 U658 ( .ip1(n273), .ip2(n539), .op(n540) );
  not_ab_or_c_or_d U659 ( .ip1(dout[6]), .ip2(n2), .ip3(n541), .ip4(n540), 
        .op(n548) );
  inv_2 U660 ( .ip(ep2_dout[6]), .op(n542) );
  nor2_2 U661 ( .ip1(n722), .ip2(n542), .op(n545) );
  inv_2 U662 ( .ip(ep1_dout[6]), .op(n543) );
  nor2_2 U663 ( .ip1(n317), .ip2(n543), .op(n544) );
  nor2_2 U664 ( .ip1(n545), .ip2(n544), .op(n547) );
  nand2_2 U665 ( .ip1(frm_nat[7]), .ip2(n274), .op(n552) );
  nand2_2 U666 ( .ip1(utmi_vend_stat_r[7]), .ip2(n549), .op(n550) );
  inv_2 U667 ( .ip(ep3_dout[7]), .op(n553) );
  nor2_2 U668 ( .ip1(n314), .ip2(n553), .op(n556) );
  inv_2 U669 ( .ip(ep0_dout[7]), .op(n554) );
  nor2_2 U670 ( .ip1(n273), .ip2(n554), .op(n555) );
  not_ab_or_c_or_d U671 ( .ip1(dout[7]), .ip2(n2), .ip3(n556), .ip4(n555), 
        .op(n563) );
  inv_2 U672 ( .ip(ep2_dout[7]), .op(n557) );
  nor2_2 U673 ( .ip1(n722), .ip2(n557), .op(n560) );
  inv_2 U674 ( .ip(ep1_dout[7]), .op(n558) );
  nor2_2 U675 ( .ip1(n317), .ip2(n558), .op(n559) );
  nor2_2 U676 ( .ip1(n560), .ip2(n559), .op(n562) );
  inv_2 U677 ( .ip(ep3_dout[8]), .op(n564) );
  nor2_2 U678 ( .ip1(n314), .ip2(n564), .op(n567) );
  inv_2 U679 ( .ip(ep0_dout[8]), .op(n565) );
  nor2_2 U680 ( .ip1(n273), .ip2(n565), .op(n566) );
  not_ab_or_c_or_d U681 ( .ip1(dout[8]), .ip2(n2), .ip3(n567), .ip4(n566), 
        .op(n574) );
  inv_2 U682 ( .ip(ep2_dout[8]), .op(n568) );
  nor2_2 U683 ( .ip1(n722), .ip2(n568), .op(n571) );
  inv_2 U684 ( .ip(ep1_dout[8]), .op(n569) );
  nor2_2 U685 ( .ip1(n317), .ip2(n569), .op(n570) );
  nor2_2 U686 ( .ip1(n571), .ip2(n570), .op(n573) );
  nand2_2 U687 ( .ip1(frm_nat[9]), .ip2(n275), .op(n575) );
  nand2_2 U688 ( .ip1(dout[9]), .ip2(n2), .op(n585) );
  nand2_2 U689 ( .ip1(dtmp[9]), .ip2(n276), .op(n584) );
  nand2_2 U690 ( .ip1(ep0_dout[9]), .ip2(n240), .op(n583) );
  inv_2 U691 ( .ip(ep1_dout[9]), .op(n576) );
  nor2_2 U692 ( .ip1(n317), .ip2(n576), .op(n581) );
  inv_2 U693 ( .ip(ep3_dout[9]), .op(n577) );
  nor2_2 U694 ( .ip1(n314), .ip2(n577), .op(n580) );
  inv_2 U695 ( .ip(ep2_dout[9]), .op(n578) );
  nor2_2 U696 ( .ip1(n722), .ip2(n578), .op(n579) );
  nand4_2 U697 ( .ip1(n585), .ip2(n584), .ip3(n583), .ip4(n582), .op(n3230) );
  nand2_2 U698 ( .ip1(frm_nat[10]), .ip2(n274), .op(n586) );
  inv_2 U699 ( .ip(ep3_dout[10]), .op(n587) );
  nor2_2 U700 ( .ip1(n314), .ip2(n587), .op(n590) );
  inv_2 U701 ( .ip(ep0_dout[10]), .op(n588) );
  nor2_2 U702 ( .ip1(n273), .ip2(n588), .op(n589) );
  not_ab_or_c_or_d U703 ( .ip1(dout[10]), .ip2(n2), .ip3(n590), .ip4(n589), 
        .op(n597) );
  inv_2 U704 ( .ip(ep2_dout[10]), .op(n591) );
  nor2_2 U705 ( .ip1(n722), .ip2(n591), .op(n594) );
  inv_2 U706 ( .ip(ep1_dout[10]), .op(n592) );
  nor2_2 U707 ( .ip1(n317), .ip2(n592), .op(n593) );
  nor2_2 U708 ( .ip1(n594), .ip2(n593), .op(n596) );
  nand2_2 U709 ( .ip1(dtmp[10]), .ip2(n276), .op(n595) );
  nand3_2 U710 ( .ip1(n597), .ip2(n596), .ip3(n595), .op(n3229) );
  nand2_2 U711 ( .ip1(frm_nat[11]), .ip2(n275), .op(n598) );
  inv_2 U712 ( .ip(ep3_dout[11]), .op(n599) );
  nor2_2 U713 ( .ip1(n314), .ip2(n599), .op(n602) );
  inv_2 U714 ( .ip(ep0_dout[11]), .op(n600) );
  nor2_2 U715 ( .ip1(n273), .ip2(n600), .op(n601) );
  not_ab_or_c_or_d U716 ( .ip1(dout[11]), .ip2(n2), .ip3(n602), .ip4(n601), 
        .op(n609) );
  inv_2 U717 ( .ip(ep2_dout[11]), .op(n603) );
  nor2_2 U718 ( .ip1(n722), .ip2(n603), .op(n606) );
  inv_2 U719 ( .ip(ep1_dout[11]), .op(n604) );
  nor2_2 U720 ( .ip1(n317), .ip2(n604), .op(n605) );
  nor2_2 U721 ( .ip1(n606), .ip2(n605), .op(n608) );
  nand2_2 U722 ( .ip1(dtmp[11]), .ip2(n276), .op(n607) );
  nand3_2 U723 ( .ip1(n609), .ip2(n608), .ip3(n607), .op(n3228) );
  nand2_2 U724 ( .ip1(frm_nat[12]), .ip2(n274), .op(n610) );
  inv_2 U725 ( .ip(ep3_dout[12]), .op(n611) );
  nor2_2 U726 ( .ip1(n314), .ip2(n611), .op(n614) );
  inv_2 U727 ( .ip(ep0_dout[12]), .op(n612) );
  nor2_2 U729 ( .ip1(n273), .ip2(n612), .op(n613) );
  not_ab_or_c_or_d U730 ( .ip1(dout[12]), .ip2(n2), .ip3(n614), .ip4(n613), 
        .op(n621) );
  inv_2 U731 ( .ip(ep2_dout[12]), .op(n615) );
  nor2_2 U732 ( .ip1(n722), .ip2(n615), .op(n618) );
  inv_2 U733 ( .ip(ep1_dout[12]), .op(n616) );
  nor2_2 U734 ( .ip1(n317), .ip2(n616), .op(n617) );
  nor2_2 U735 ( .ip1(n618), .ip2(n617), .op(n620) );
  nand2_2 U736 ( .ip1(dtmp[12]), .ip2(n277), .op(n619) );
  nand3_2 U737 ( .ip1(n621), .ip2(n620), .ip3(n619), .op(n3227) );
  nand2_2 U738 ( .ip1(frm_nat[13]), .ip2(n274), .op(n622) );
  inv_2 U739 ( .ip(ep3_dout[13]), .op(n623) );
  nor2_2 U740 ( .ip1(n315), .ip2(n623), .op(n626) );
  inv_2 U741 ( .ip(ep0_dout[13]), .op(n624) );
  nor2_2 U742 ( .ip1(n273), .ip2(n624), .op(n625) );
  not_ab_or_c_or_d U743 ( .ip1(dout[13]), .ip2(n2), .ip3(n626), .ip4(n625), 
        .op(n633) );
  inv_2 U744 ( .ip(ep2_dout[13]), .op(n627) );
  nor2_2 U745 ( .ip1(n318), .ip2(n627), .op(n630) );
  inv_2 U746 ( .ip(ep1_dout[13]), .op(n628) );
  nor2_2 U747 ( .ip1(n317), .ip2(n628), .op(n629) );
  nor2_2 U748 ( .ip1(n630), .ip2(n629), .op(n632) );
  nand2_2 U749 ( .ip1(dtmp[13]), .ip2(n277), .op(n631) );
  nand3_2 U750 ( .ip1(n633), .ip2(n632), .ip3(n631), .op(n3226) );
  nand2_2 U751 ( .ip1(frm_nat[14]), .ip2(n275), .op(n634) );
  inv_2 U752 ( .ip(ep3_dout[14]), .op(n635) );
  nor2_2 U753 ( .ip1(n315), .ip2(n635), .op(n638) );
  inv_2 U754 ( .ip(ep0_dout[14]), .op(n636) );
  nor2_2 U755 ( .ip1(n273), .ip2(n636), .op(n637) );
  not_ab_or_c_or_d U756 ( .ip1(dout[14]), .ip2(n2), .ip3(n638), .ip4(n637), 
        .op(n645) );
  inv_2 U757 ( .ip(ep2_dout[14]), .op(n639) );
  nor2_2 U758 ( .ip1(n318), .ip2(n639), .op(n642) );
  inv_2 U759 ( .ip(ep1_dout[14]), .op(n640) );
  nor2_2 U760 ( .ip1(n316), .ip2(n640), .op(n641) );
  nor2_2 U761 ( .ip1(n642), .ip2(n641), .op(n644) );
  nand2_2 U762 ( .ip1(dtmp[14]), .ip2(n277), .op(n643) );
  nand3_2 U763 ( .ip1(n645), .ip2(n644), .ip3(n643), .op(n3225) );
  nand2_2 U764 ( .ip1(frm_nat[15]), .ip2(n275), .op(n646) );
  nand2_2 U765 ( .ip1(dout[15]), .ip2(n2), .op(n656) );
  nand2_2 U766 ( .ip1(dtmp[15]), .ip2(n277), .op(n655) );
  nand2_2 U767 ( .ip1(ep0_dout[15]), .ip2(n240), .op(n654) );
  inv_2 U768 ( .ip(ep1_dout[15]), .op(n647) );
  nor2_2 U769 ( .ip1(n316), .ip2(n647), .op(n652) );
  inv_2 U770 ( .ip(ep3_dout[15]), .op(n648) );
  nor2_2 U771 ( .ip1(n315), .ip2(n648), .op(n651) );
  inv_2 U772 ( .ip(ep2_dout[15]), .op(n649) );
  nor2_2 U773 ( .ip1(n318), .ip2(n649), .op(n650) );
  nand4_2 U774 ( .ip1(n656), .ip2(n655), .ip3(n654), .ip4(n653), .op(n3224) );
  nand2_2 U775 ( .ip1(frm_nat[16]), .ip2(n274), .op(n657) );
  nand2_2 U776 ( .ip1(ep0_dout[16]), .ip2(n240), .op(n660) );
  nand2_2 U777 ( .ip1(ep1_dout[16]), .ip2(n4), .op(n659) );
  nand2_2 U778 ( .ip1(n660), .ip2(n659), .op(n664) );
  nand2_2 U779 ( .ip1(ep3_dout[16]), .ip2(n3), .op(n662) );
  nand2_2 U780 ( .ip1(ep2_dout[16]), .ip2(n319), .op(n661) );
  nand2_2 U781 ( .ip1(n662), .ip2(n661), .op(n663) );
  nand2_2 U782 ( .ip1(frm_nat[17]), .ip2(n275), .op(n665) );
  nand2_2 U783 ( .ip1(ep0_dout[17]), .ip2(n240), .op(n668) );
  nand2_2 U784 ( .ip1(ep1_dout[17]), .ip2(n4), .op(n667) );
  nand2_2 U785 ( .ip1(n668), .ip2(n667), .op(n672) );
  nand2_2 U786 ( .ip1(ep3_dout[17]), .ip2(n3), .op(n670) );
  nand2_2 U787 ( .ip1(ep2_dout[17]), .ip2(n319), .op(n669) );
  nand2_2 U788 ( .ip1(n670), .ip2(n669), .op(n671) );
  nand2_2 U789 ( .ip1(frm_nat[18]), .ip2(n274), .op(n673) );
  nand2_2 U790 ( .ip1(ep0_dout[18]), .ip2(n240), .op(n676) );
  nand2_2 U791 ( .ip1(ep1_dout[18]), .ip2(n4), .op(n675) );
  nand2_2 U792 ( .ip1(n676), .ip2(n675), .op(n680) );
  nand2_2 U793 ( .ip1(ep3_dout[18]), .ip2(n3), .op(n678) );
  nand2_2 U794 ( .ip1(ep2_dout[18]), .ip2(n319), .op(n677) );
  nand2_2 U799 ( .ip1(n678), .ip2(n677), .op(n679) );
  nand2_2 U800 ( .ip1(frm_nat[19]), .ip2(n274), .op(n681) );
  nand2_2 U801 ( .ip1(ep0_dout[19]), .ip2(n240), .op(n684) );
  nand2_2 U802 ( .ip1(ep1_dout[19]), .ip2(n4), .op(n683) );
  nand2_2 U803 ( .ip1(n684), .ip2(n683), .op(n688) );
  nand2_2 U806 ( .ip1(ep3_dout[19]), .ip2(n3), .op(n686) );
  nand2_2 U807 ( .ip1(ep2_dout[19]), .ip2(n319), .op(n685) );
  nand2_2 U808 ( .ip1(n686), .ip2(n685), .op(n687) );
  inv_2 U809 ( .ip(ep3_dout[20]), .op(n689) );
  nor2_2 U810 ( .ip1(n315), .ip2(n689), .op(n692) );
  inv_2 U811 ( .ip(ep0_dout[20]), .op(n690) );
  nor2_2 U812 ( .ip1(n273), .ip2(n690), .op(n691) );
  not_ab_or_c_or_d U813 ( .ip1(dout[20]), .ip2(n2), .ip3(n692), .ip4(n691), 
        .op(n699) );
  inv_2 U814 ( .ip(ep2_dout[20]), .op(n693) );
  nor2_2 U815 ( .ip1(n318), .ip2(n693), .op(n696) );
  inv_2 U816 ( .ip(ep1_dout[20]), .op(n694) );
  nor2_2 U817 ( .ip1(n316), .ip2(n694), .op(n695) );
  nor2_2 U821 ( .ip1(n696), .ip2(n695), .op(n698) );
  inv_2 U822 ( .ip(ep3_dout[21]), .op(n700) );
  nor2_2 U823 ( .ip1(n315), .ip2(n700), .op(n703) );
  inv_2 U824 ( .ip(ep0_dout[21]), .op(n701) );
  nor2_2 U826 ( .ip1(n273), .ip2(n701), .op(n702) );
  not_ab_or_c_or_d U827 ( .ip1(dout[21]), .ip2(n2), .ip3(n703), .ip4(n702), 
        .op(n710) );
  inv_2 U828 ( .ip(ep2_dout[21]), .op(n704) );
  nor2_2 U829 ( .ip1(n318), .ip2(n704), .op(n707) );
  inv_2 U830 ( .ip(ep1_dout[21]), .op(n705) );
  nor2_2 U833 ( .ip1(n316), .ip2(n705), .op(n706) );
  nor2_2 U834 ( .ip1(n707), .ip2(n706), .op(n709) );
  nand2_2 U835 ( .ip1(frm_nat[22]), .ip2(n274), .op(n714) );
  nand2_2 U836 ( .ip1(intb_msk[6]), .ip2(n754), .op(n713) );
  nand2_2 U837 ( .ip1(int_srcb[2]), .ip2(n803), .op(n711) );
  nand3_2 U838 ( .ip1(n714), .ip2(n713), .ip3(n711), .op(N93) );
  inv_2 U839 ( .ip(ep3_dout[22]), .op(n715) );
  nor2_2 U840 ( .ip1(n315), .ip2(n715), .op(n720) );
  inv_2 U841 ( .ip(ep0_dout[22]), .op(n716) );
  nor2_2 U842 ( .ip1(n273), .ip2(n716), .op(n718) );
  not_ab_or_c_or_d U843 ( .ip1(dout[22]), .ip2(n2), .ip3(n720), .ip4(n718), 
        .op(n737) );
  inv_2 U844 ( .ip(ep2_dout[22]), .op(n723) );
  nor2_2 U849 ( .ip1(n318), .ip2(n723), .op(n731) );
  inv_2 U850 ( .ip(ep1_dout[22]), .op(n729) );
  nor2_2 U851 ( .ip1(n316), .ip2(n729), .op(n730) );
  nor2_2 U852 ( .ip1(n731), .ip2(n730), .op(n736) );
  nand2_2 U853 ( .ip1(frm_nat[23]), .ip2(n275), .op(n740) );
  nand2_2 U856 ( .ip1(intb_msk[7]), .ip2(n754), .op(n739) );
  nand2_2 U857 ( .ip1(int_srcb[3]), .ip2(n803), .op(n738) );
  nand3_2 U858 ( .ip1(n740), .ip2(n739), .ip3(n738), .op(N94) );
  inv_2 U859 ( .ip(ep3_dout[23]), .op(n741) );
  nor2_2 U860 ( .ip1(n315), .ip2(n741), .op(n745) );
  inv_2 U861 ( .ip(ep0_dout[23]), .op(n743) );
  nor2_2 U862 ( .ip1(n273), .ip2(n743), .op(n744) );
  not_ab_or_c_or_d U863 ( .ip1(dout[23]), .ip2(n2), .ip3(n745), .ip4(n744), 
        .op(n753) );
  inv_2 U864 ( .ip(ep2_dout[23]), .op(n746) );
  nor2_2 U865 ( .ip1(n318), .ip2(n746), .op(n750) );
  inv_2 U866 ( .ip(ep1_dout[23]), .op(n748) );
  nor2_2 U867 ( .ip1(n316), .ip2(n748), .op(n749) );
  nor2_2 U872 ( .ip1(n750), .ip2(n749), .op(n752) );
  nand2_2 U873 ( .ip1(frm_nat[24]), .ip2(n275), .op(n757) );
  nand2_2 U874 ( .ip1(intb_msk[8]), .ip2(n754), .op(n756) );
  nand2_2 U875 ( .ip1(int_srcb[4]), .ip2(n803), .op(n755) );
  nand3_2 U876 ( .ip1(n757), .ip2(n756), .ip3(n755), .op(N95) );
  inv_2 U879 ( .ip(ep3_dout[24]), .op(n758) );
  nor2_2 U880 ( .ip1(n315), .ip2(n758), .op(n761) );
  inv_2 U881 ( .ip(ep0_dout[24]), .op(n759) );
  nor2_2 U882 ( .ip1(n273), .ip2(n759), .op(n760) );
  not_ab_or_c_or_d U883 ( .ip1(dout[24]), .ip2(n2), .ip3(n761), .ip4(n760), 
        .op(n769) );
  inv_2 U884 ( .ip(ep2_dout[24]), .op(n762) );
  nor2_2 U885 ( .ip1(n318), .ip2(n762), .op(n766) );
  inv_2 U886 ( .ip(ep1_dout[24]), .op(n763) );
  nor2_2 U887 ( .ip1(n316), .ip2(n763), .op(n765) );
  nor2_2 U888 ( .ip1(n766), .ip2(n765), .op(n768) );
  nand2_2 U889 ( .ip1(frm_nat[25]), .ip2(n274), .op(n771) );
  nand2_2 U890 ( .ip1(ep0_dout[25]), .ip2(n240), .op(n773) );
  nand2_2 U895 ( .ip1(ep1_dout[25]), .ip2(n4), .op(n772) );
  nand2_2 U896 ( .ip1(n773), .ip2(n772), .op(n777) );
  nand2_2 U897 ( .ip1(ep3_dout[25]), .ip2(n3), .op(n775) );
  nand2_2 U898 ( .ip1(ep2_dout[25]), .ip2(n319), .op(n774) );
  nand2_2 U899 ( .ip1(n775), .ip2(n774), .op(n776) );
  nand2_2 U902 ( .ip1(frm_nat[26]), .ip2(n275), .op(n784) );
  nand2_2 U903 ( .ip1(ep0_dout[26]), .ip2(n240), .op(n787) );
  nand2_2 U904 ( .ip1(ep1_dout[26]), .ip2(n4), .op(n786) );
  nand2_2 U905 ( .ip1(n787), .ip2(n786), .op(n793) );
  nand2_2 U906 ( .ip1(ep3_dout[26]), .ip2(n3), .op(n791) );
  nand2_2 U907 ( .ip1(ep2_dout[26]), .ip2(n319), .op(n790) );
  nand2_2 U908 ( .ip1(n791), .ip2(n790), .op(n792) );
  nand2_2 U909 ( .ip1(frm_nat[27]), .ip2(n274), .op(n796) );
  nand2_2 U910 ( .ip1(ep0_dout[27]), .ip2(n240), .op(n798) );
  nand2_2 U911 ( .ip1(ep1_dout[27]), .ip2(n4), .op(n797) );
  nand2_2 U912 ( .ip1(n798), .ip2(n797), .op(n802) );
  nand2_2 U913 ( .ip1(ep3_dout[27]), .ip2(n3), .op(n800) );
  nand2_2 U918 ( .ip1(ep2_dout[27]), .ip2(n319), .op(n799) );
  nand2_2 U919 ( .ip1(n800), .ip2(n799), .op(n801) );
  nand2_2 U920 ( .ip1(ep0_dout[28]), .ip2(n240), .op(n805) );
  nand2_2 U921 ( .ip1(ep1_dout[28]), .ip2(n4), .op(n804) );
  nand2_2 U922 ( .ip1(n805), .ip2(n804), .op(n809) );
  nand2_2 U925 ( .ip1(ep3_dout[28]), .ip2(n3), .op(n807) );
  nand2_2 U926 ( .ip1(ep2_dout[28]), .ip2(n319), .op(n806) );
  nand2_2 U927 ( .ip1(n807), .ip2(n806), .op(n808) );
  inv_2 U928 ( .ip(frm_nat[29]), .op(n810) );
  nor2_2 U929 ( .ip1(n840), .ip2(n810), .op(N100) );
  nand2_2 U930 ( .ip1(dout[29]), .ip2(n2), .op(n827) );
  nand2_2 U931 ( .ip1(dtmp[29]), .ip2(n278), .op(n826) );
  nand2_2 U932 ( .ip1(ep0_dout[29]), .ip2(n240), .op(n823) );
  inv_2 U933 ( .ip(ep1_dout[29]), .op(n811) );
  nor2_2 U934 ( .ip1(n316), .ip2(n811), .op(n820) );
  inv_2 U935 ( .ip(ep3_dout[29]), .op(n812) );
  nor2_2 U936 ( .ip1(n315), .ip2(n812), .op(n819) );
  inv_2 U941 ( .ip(ep2_dout[29]), .op(n817) );
  nor2_2 U942 ( .ip1(n318), .ip2(n817), .op(n818) );
  nand4_2 U943 ( .ip1(n827), .ip2(n826), .ip3(n823), .ip4(n822), .op(n3210) );
  inv_2 U944 ( .ip(frm_nat[30]), .op(n828) );
  nor2_2 U945 ( .ip1(n840), .ip2(n828), .op(N101) );
  nand2_2 U948 ( .ip1(dout[30]), .ip2(n2), .op(n838) );
  nand2_2 U949 ( .ip1(dtmp[30]), .ip2(n278), .op(n837) );
  nand2_2 U950 ( .ip1(ep0_dout[30]), .ip2(n240), .op(n836) );
  inv_2 U951 ( .ip(ep1_dout[30]), .op(n829) );
  nor2_2 U952 ( .ip1(n316), .ip2(n829), .op(n834) );
  inv_2 U953 ( .ip(ep3_dout[30]), .op(n830) );
  nor2_2 U954 ( .ip1(n315), .ip2(n830), .op(n833) );
  inv_2 U955 ( .ip(ep2_dout[30]), .op(n831) );
  nor2_2 U956 ( .ip1(n318), .ip2(n831), .op(n832) );
  nand4_2 U957 ( .ip1(n838), .ip2(n837), .ip3(n836), .ip4(n835), .op(n3209) );
  inv_2 U958 ( .ip(frm_nat[31]), .op(n839) );
  nor2_2 U959 ( .ip1(n840), .ip2(n839), .op(N102) );
  nand2_2 U964 ( .ip1(dout[31]), .ip2(n2), .op(n857) );
  nand2_2 U965 ( .ip1(dtmp[31]), .ip2(n278), .op(n856) );
  nand2_2 U966 ( .ip1(ep0_dout[31]), .ip2(n240), .op(n855) );
  inv_2 U967 ( .ip(ep1_dout[31]), .op(n846) );
  nor2_2 U968 ( .ip1(n316), .ip2(n846), .op(n853) );
  inv_2 U971 ( .ip(ep3_dout[31]), .op(n849) );
  nor2_2 U972 ( .ip1(n315), .ip2(n849), .op(n852) );
  inv_2 U973 ( .ip(ep2_dout[31]), .op(n850) );
  nor2_2 U974 ( .ip1(n318), .ip2(n850), .op(n851) );
  nand4_2 U975 ( .ip1(n857), .ip2(n856), .ip3(n855), .ip4(n854), .op(n3208) );
  inv_2 U976 ( .ip(n2953), .op(n821) );
  nor2_2 U977 ( .ip1(n327), .ip2(n295), .op(n2940) );
  nand2_2 U978 ( .ip1(n104), .ip2(n779), .op(n3249) );
  nand2_2 U979 ( .ip1(n23), .ip2(n813), .op(n3250) );
  nand2_2 U980 ( .ip1(n24), .ip2(n842), .op(n3251) );
  nand2_2 U981 ( .ip1(n25), .ip2(n865), .op(n3252) );
  nand2_2 U982 ( .ip1(n26), .ip2(n887), .op(n3253) );
  nand2_2 U987 ( .ip1(n27), .ip2(n909), .op(n3254) );
  nand2_2 U988 ( .ip1(n28), .ip2(n931), .op(n3255) );
  nand2_2 U989 ( .ip1(n29), .ip2(n953), .op(n3256) );
  nand2_2 U990 ( .ip1(n30), .ip2(n975), .op(n3257) );
  nand2_2 U991 ( .ip1(n31), .ip2(n997), .op(n3258) );
  nand2_2 U994 ( .ip1(n32), .ip2(n1019), .op(n3259) );
  nand2_2 U995 ( .ip1(n33), .ip2(n1041), .op(n3260) );
  nand2_2 U996 ( .ip1(n34), .ip2(n1063), .op(n3261) );
  nand2_2 U997 ( .ip1(n35), .ip2(n1085), .op(n3262) );
  nand2_2 U998 ( .ip1(n36), .ip2(n1107), .op(n3263) );
  nand2_2 U999 ( .ip1(n37), .ip2(n1129), .op(n3264) );
  nand2_2 U1000 ( .ip1(n38), .ip2(n1151), .op(n3265) );
  nand2_2 U1001 ( .ip1(n39), .ip2(n1173), .op(n3266) );
  nand2_2 U1002 ( .ip1(n40), .ip2(n1195), .op(n3267) );
  nand2_2 U1003 ( .ip1(n41), .ip2(n1217), .op(n3268) );
  nand2_2 U1004 ( .ip1(n42), .ip2(n1239), .op(n3269) );
  nand2_2 U1005 ( .ip1(n43), .ip2(n1261), .op(n3270) );
  nand2_2 U1010 ( .ip1(n44), .ip2(n1283), .op(n3271) );
  nand2_2 U1011 ( .ip1(n45), .ip2(n1305), .op(n3272) );
  nand2_2 U1012 ( .ip1(n46), .ip2(n1327), .op(n3273) );
  nand2_2 U1013 ( .ip1(n47), .ip2(n1349), .op(n3274) );
  nand2_2 U1014 ( .ip1(n48), .ip2(n1371), .op(n3275) );
  nand2_2 U1017 ( .ip1(n49), .ip2(n1393), .op(n3276) );
  nand2_2 U1018 ( .ip1(n50), .ip2(n1415), .op(n3277) );
  nand2_2 U1019 ( .ip1(n51), .ip2(n1437), .op(n3278) );
  nand2_2 U1020 ( .ip1(n52), .ip2(n1459), .op(n3279) );
  nand2_2 U1021 ( .ip1(n53), .ip2(n1481), .op(n3280) );
  nand2_2 U1022 ( .ip1(n54), .ip2(n1503), .op(n3281) );
  nand2_2 U1023 ( .ip1(n55), .ip2(n1525), .op(n3282) );
  nand2_2 U1024 ( .ip1(n56), .ip2(n1547), .op(n3283) );
  nand2_2 U1025 ( .ip1(n57), .ip2(n1569), .op(n3284) );
  nand2_2 U1026 ( .ip1(n58), .ip2(n1591), .op(n3285) );
  nand2_2 U1027 ( .ip1(n59), .ip2(n1613), .op(n3286) );
  nand2_2 U1028 ( .ip1(n60), .ip2(n1635), .op(n3287) );
  nand2_2 U1033 ( .ip1(n61), .ip2(n1657), .op(n3288) );
  nand2_2 U1034 ( .ip1(n62), .ip2(n1679), .op(n3289) );
  nand2_2 U1035 ( .ip1(n63), .ip2(n1701), .op(n3290) );
  nand2_2 U1036 ( .ip1(n64), .ip2(n1723), .op(n3291) );
  nand2_2 U1037 ( .ip1(n65), .ip2(n1745), .op(n3292) );
  nand2_2 U1040 ( .ip1(n66), .ip2(n1767), .op(n3293) );
  nand2_2 U1041 ( .ip1(n67), .ip2(n1789), .op(n3294) );
  nand2_2 U1042 ( .ip1(n68), .ip2(n1811), .op(n3295) );
  nand2_2 U1043 ( .ip1(n69), .ip2(n1833), .op(n3296) );
  nand2_2 U1044 ( .ip1(n70), .ip2(n1855), .op(n3297) );
  nand2_2 U1045 ( .ip1(n71), .ip2(n1877), .op(n3298) );
  nand2_2 U1046 ( .ip1(n72), .ip2(n1899), .op(n3299) );
  nand2_2 U1047 ( .ip1(n73), .ip2(n1921), .op(n3300) );
  nand2_2 U1048 ( .ip1(n74), .ip2(n1943), .op(n3301) );
  nand2_2 U1049 ( .ip1(n75), .ip2(n1965), .op(n3302) );
  nand2_2 U1050 ( .ip1(n76), .ip2(n1987), .op(n3303) );
  nand2_2 U1051 ( .ip1(n77), .ip2(n2009), .op(n3304) );
  nand2_2 U1056 ( .ip1(n78), .ip2(n2031), .op(n3305) );
  nand2_2 U1057 ( .ip1(n79), .ip2(n2053), .op(n3306) );
  nand2_2 U1058 ( .ip1(n80), .ip2(n2075), .op(n3307) );
  nand2_2 U1059 ( .ip1(n81), .ip2(n2097), .op(n3308) );
  nand2_2 U1060 ( .ip1(n82), .ip2(n2119), .op(n3309) );
  nand2_2 U1063 ( .ip1(n83), .ip2(n2141), .op(n3310) );
  nand2_2 U1064 ( .ip1(n84), .ip2(n2163), .op(n3311) );
  nand2_2 U1065 ( .ip1(n85), .ip2(n2185), .op(n3312) );
  nand2_2 U1066 ( .ip1(n86), .ip2(n2207), .op(n3313) );
  nand2_2 U1067 ( .ip1(n87), .ip2(n2229), .op(n3314) );
  nand2_2 U1068 ( .ip1(n88), .ip2(n2251), .op(n3315) );
  nand2_2 U1069 ( .ip1(n89), .ip2(n2273), .op(n3316) );
  nand2_2 U1070 ( .ip1(n8), .ip2(n2295), .op(n3317) );
  nand2_2 U1071 ( .ip1(n13), .ip2(n2317), .op(n3318) );
  nand2_2 U1072 ( .ip1(n9), .ip2(n2339), .op(n3319) );
  nand2_2 U1073 ( .ip1(n14), .ip2(n2361), .op(n3320) );
  nand2_2 U1074 ( .ip1(n10), .ip2(n2383), .op(n3321) );
  nand2_2 U1079 ( .ip1(n15), .ip2(n2405), .op(n3322) );
  nand2_2 U1080 ( .ip1(n12), .ip2(n2427), .op(n3323) );
  nand2_2 U1081 ( .ip1(n16), .ip2(n2449), .op(n3324) );
  nand2_2 U1082 ( .ip1(n11), .ip2(n2471), .op(n3325) );
  nand2_2 U1083 ( .ip1(n90), .ip2(n2493), .op(n3326) );
  nand2_2 U1086 ( .ip1(n91), .ip2(n2515), .op(n3327) );
  nand2_2 U1087 ( .ip1(n92), .ip2(n2537), .op(n3328) );
  nand2_2 U1088 ( .ip1(n22), .ip2(n2581), .op(n3330) );
  nand2_2 U1089 ( .ip1(n93), .ip2(n2603), .op(n3331) );
  nand2_2 U1090 ( .ip1(n94), .ip2(n2625), .op(n3332) );
  nand2_2 U1091 ( .ip1(n18), .ip2(n2647), .op(n3333) );
  nand2_2 U1092 ( .ip1(n19), .ip2(n2669), .op(n3334) );
  nand2_2 U1093 ( .ip1(n20), .ip2(n2691), .op(n3335) );
  nand2_2 U1094 ( .ip1(n21), .ip2(n2713), .op(n3336) );
  nand2_2 U1095 ( .ip1(n95), .ip2(n2735), .op(n3337) );
  nand2_2 U1096 ( .ip1(n96), .ip2(n2757), .op(n3338) );
  nand2_2 U1097 ( .ip1(n97), .ip2(n2779), .op(n3339) );
  nand2_2 U1102 ( .ip1(n98), .ip2(n2801), .op(n3340) );
  nand2_2 U1103 ( .ip1(n99), .ip2(n2823), .op(n3341) );
  nand2_2 U1104 ( .ip1(n17), .ip2(n2845), .op(n3342) );
  nand2_2 U1105 ( .ip1(n100), .ip2(n2867), .op(n3343) );
  nand2_2 U1106 ( .ip1(n101), .ip2(n2889), .op(n3344) );
  nand2_2 U1109 ( .ip1(n102), .ip2(n2911), .op(n3345) );
  nand2_2 U1110 ( .ip1(n103), .ip2(n2933), .op(n3346) );
  nor2_2 U1111 ( .ip1(n313), .ip2(n304), .op(n2937) );
  nor2_2 U1112 ( .ip1(ep3_match), .ip2(n239), .op(n2953) );
  inv_2 U1129 ( .ip(ep1_match), .op(n864) );
  inv_2 U1132 ( .ip(ep2_match), .op(n871) );
  inv_2 U1133 ( .ip(ep3_match), .op(n872) );
  inv_2 U1134 ( .ip(adr[5]), .op(n873) );
  inv_2 U1135 ( .ip(adr[4]), .op(n874) );
endmodule


module usbf_wb ( wb_clk, phy_clk, rst, wb_addr_i, wb_data_i, wb_data_o, 
        wb_ack_o, wb_we_i, wb_stb_i, wb_cyc_i, ma_adr, ma_dout, ma_din, ma_we, 
        ma_req, ma_ack, rf_re, rf_we, rf_din, rf_dout );
  input [17:0] wb_addr_i;
  input [31:0] wb_data_i;
  output [31:0] wb_data_o;
  output [17:0] ma_adr;
  output [31:0] ma_dout;
  input [31:0] ma_din;
  input [31:0] rf_din;
  output [31:0] rf_dout;
  input wb_clk, phy_clk, rst, wb_we_i, wb_stb_i, wb_cyc_i, ma_ack;
  output wb_ack_o, ma_we, ma_req, rf_re, rf_we;
  wire   N14, N15, N16, N17, N18, N19, N20, N21, N22, N23, N24, N25, N26, N27,
         N28, N29, N30, N31, N32, N33, N34, N35, N36, N37, N38, N39, N40, N41,
         N42, N43, N44, N45, N46, wb_req_s1, wb_ack_s1, wb_ack_d, wb_ack_s2,
         N47, wb_ack_s1a, n12, n16, n18, n19, n20, n21, n22, n23, n24, n25,
         n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39,
         n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51, n52, n53,
         n54, n119, n120, n121, n122, n123, n124, n1, n2, n3, n4, n5, n6, n7,
         n8, n9, n10, n11, n13, n14, n15, n17, n55, n56;
  wire   [5:0] state;
  assign ma_adr[17] = wb_addr_i[17];
  assign ma_adr[16] = wb_addr_i[16];
  assign ma_adr[15] = wb_addr_i[15];
  assign ma_adr[14] = wb_addr_i[14];
  assign ma_adr[13] = wb_addr_i[13];
  assign ma_adr[12] = wb_addr_i[12];
  assign ma_adr[11] = wb_addr_i[11];
  assign ma_adr[10] = wb_addr_i[10];
  assign ma_adr[9] = wb_addr_i[9];
  assign ma_adr[8] = wb_addr_i[8];
  assign ma_adr[7] = wb_addr_i[7];
  assign ma_adr[6] = wb_addr_i[6];
  assign ma_adr[5] = wb_addr_i[5];
  assign ma_adr[4] = wb_addr_i[4];
  assign ma_adr[3] = wb_addr_i[3];
  assign ma_adr[2] = wb_addr_i[2];
  assign ma_adr[1] = wb_addr_i[1];
  assign ma_adr[0] = wb_addr_i[0];
  assign rf_dout[31] = wb_data_i[31];
  assign ma_dout[31] = wb_data_i[31];
  assign rf_dout[30] = wb_data_i[30];
  assign ma_dout[30] = wb_data_i[30];
  assign rf_dout[29] = wb_data_i[29];
  assign ma_dout[29] = wb_data_i[29];
  assign rf_dout[28] = wb_data_i[28];
  assign ma_dout[28] = wb_data_i[28];
  assign rf_dout[27] = wb_data_i[27];
  assign ma_dout[27] = wb_data_i[27];
  assign rf_dout[26] = wb_data_i[26];
  assign ma_dout[26] = wb_data_i[26];
  assign rf_dout[25] = wb_data_i[25];
  assign ma_dout[25] = wb_data_i[25];
  assign rf_dout[24] = wb_data_i[24];
  assign ma_dout[24] = wb_data_i[24];
  assign rf_dout[23] = wb_data_i[23];
  assign ma_dout[23] = wb_data_i[23];
  assign rf_dout[22] = wb_data_i[22];
  assign ma_dout[22] = wb_data_i[22];
  assign rf_dout[21] = wb_data_i[21];
  assign ma_dout[21] = wb_data_i[21];
  assign rf_dout[20] = wb_data_i[20];
  assign ma_dout[20] = wb_data_i[20];
  assign rf_dout[19] = wb_data_i[19];
  assign ma_dout[19] = wb_data_i[19];
  assign rf_dout[18] = wb_data_i[18];
  assign ma_dout[18] = wb_data_i[18];
  assign rf_dout[17] = wb_data_i[17];
  assign ma_dout[17] = wb_data_i[17];
  assign rf_dout[16] = wb_data_i[16];
  assign ma_dout[16] = wb_data_i[16];
  assign rf_dout[15] = wb_data_i[15];
  assign ma_dout[15] = wb_data_i[15];
  assign rf_dout[14] = wb_data_i[14];
  assign ma_dout[14] = wb_data_i[14];
  assign rf_dout[13] = wb_data_i[13];
  assign ma_dout[13] = wb_data_i[13];
  assign rf_dout[12] = wb_data_i[12];
  assign ma_dout[12] = wb_data_i[12];
  assign rf_dout[11] = wb_data_i[11];
  assign ma_dout[11] = wb_data_i[11];
  assign rf_dout[10] = wb_data_i[10];
  assign ma_dout[10] = wb_data_i[10];
  assign rf_dout[9] = wb_data_i[9];
  assign ma_dout[9] = wb_data_i[9];
  assign rf_dout[8] = wb_data_i[8];
  assign ma_dout[8] = wb_data_i[8];
  assign rf_dout[7] = wb_data_i[7];
  assign ma_dout[7] = wb_data_i[7];
  assign rf_dout[6] = wb_data_i[6];
  assign ma_dout[6] = wb_data_i[6];
  assign rf_dout[5] = wb_data_i[5];
  assign ma_dout[5] = wb_data_i[5];
  assign rf_dout[4] = wb_data_i[4];
  assign ma_dout[4] = wb_data_i[4];
  assign rf_dout[3] = wb_data_i[3];
  assign ma_dout[3] = wb_data_i[3];
  assign rf_dout[2] = wb_data_i[2];
  assign ma_dout[2] = wb_data_i[2];
  assign rf_dout[1] = wb_data_i[1];
  assign ma_dout[1] = wb_data_i[1];
  assign rf_dout[0] = wb_data_i[0];
  assign ma_dout[0] = wb_data_i[0];

  nand2_2 U5 ( .ip1(n19), .ip2(n20), .op(n119) );
  nand3_2 U6 ( .ip1(n21), .ip2(n22), .ip3(n23), .op(n20) );
  nand2_2 U7 ( .ip1(state[1]), .ip2(n24), .op(n19) );
  nand2_2 U8 ( .ip1(n25), .ip2(n26), .op(n120) );
  nand2_2 U9 ( .ip1(n9), .ip2(n21), .op(n26) );
  nand2_2 U10 ( .ip1(state[2]), .ip2(n24), .op(n25) );
  nand2_2 U11 ( .ip1(n27), .ip2(n28), .op(n121) );
  nand2_2 U12 ( .ip1(n29), .ip2(n30), .op(n28) );
  or2_2 U13 ( .ip1(rf_we), .ip2(rf_re), .op(n30) );
  nor2_2 U14 ( .ip1(n31), .ip2(n32), .op(rf_re) );
  nor2_2 U15 ( .ip1(n33), .ip2(n32), .op(rf_we) );
  nand2_2 U16 ( .ip1(state[3]), .ip2(n24), .op(n27) );
  nand2_2 U17 ( .ip1(n34), .ip2(n35), .op(n122) );
  nand2_2 U18 ( .ip1(n29), .ip2(n36), .op(n35) );
  nand2_2 U19 ( .ip1(state[4]), .ip2(n24), .op(n34) );
  nand2_2 U20 ( .ip1(n37), .ip2(n38), .op(n123) );
  nand2_2 U21 ( .ip1(n39), .ip2(n29), .op(n38) );
  and2_2 U22 ( .ip1(n23), .ip2(rst), .op(n29) );
  nand2_2 U23 ( .ip1(state[5]), .ip2(n24), .op(n37) );
  nand2_2 U24 ( .ip1(n23), .ip2(n40), .op(n24) );
  nand2_2 U25 ( .ip1(n41), .ip2(rst), .op(n40) );
  nand2_2 U26 ( .ip1(n23), .ip2(n10), .op(n42) );
  nor4_2 U27 ( .ip1(n36), .ip2(n43), .ip3(n39), .ip4(n14), .op(n41) );
  and4_2 U28 ( .ip1(state[4]), .ip2(n44), .ip3(n15), .ip4(n6), .op(n39) );
  and4_2 U29 ( .ip1(state[5]), .ip2(n44), .ip3(n15), .ip4(n7), .op(n43) );
  nand2_2 U30 ( .ip1(n18), .ip2(n16), .op(n36) );
  nor2_2 U33 ( .ip1(n45), .ip2(n11), .op(n18) );
  and3_2 U34 ( .ip1(n46), .ip2(n47), .ip3(n48), .op(n23) );
  nand3_2 U35 ( .ip1(rst), .ip2(n8), .ip3(n11), .op(n48) );
  nand3_2 U36 ( .ip1(n22), .ip2(n50), .ip3(n21), .op(n47) );
  and4_2 U37 ( .ip1(rst), .ip2(n14), .ip3(n31), .ip4(n33), .op(n21) );
  nand3_2 U38 ( .ip1(wb_req_s1), .ip2(n1), .ip3(wb_we_i), .op(n33) );
  nand3_2 U39 ( .ip1(n1), .ip2(n56), .ip3(wb_req_s1), .op(n31) );
  nand3_2 U40 ( .ip1(rst), .ip2(n8), .ip3(n45), .op(n46) );
  nor2_2 U41 ( .ip1(ma_ack), .ip2(n49), .op(n51) );
  nand4_2 U42 ( .ip1(state[2]), .ip2(n52), .ip3(n13), .ip4(n17), .op(n49) );
  nand2_2 U43 ( .ip1(n53), .ip2(n54), .op(ma_we) );
  nand2_2 U44 ( .ip1(n45), .ip2(n8), .op(n54) );
  and4_2 U45 ( .ip1(state[1]), .ip2(n52), .ip3(n13), .ip4(n55), .op(n45) );
  or2_2 U46 ( .ip1(n50), .ip2(n32), .op(n53) );
  nand3_2 U47 ( .ip1(wb_we_i), .ip2(wb_req_s1), .ip3(wb_addr_i[17]), .op(n50)
         );
  nand4_2 U48 ( .ip1(state[0]), .ip2(n52), .ip3(n17), .ip4(n55), .op(n32) );
  nand3_2 U50 ( .ip1(wb_req_s1), .ip2(n56), .ip3(wb_addr_i[17]), .op(n22) );
  and2_2 U52 ( .ip1(wb_stb_i), .ip2(wb_cyc_i), .op(N46) );
  inv_2 U160 ( .ip(rst), .op(n12) );
  ab_or_c_or_d U164 ( .ip1(state[0]), .ip2(n42), .ip3(n43), .ip4(n12), .op(
        n124) );
  ab_or_c_or_d U165 ( .ip1(n9), .ip2(n14), .ip3(ma_we), .ip4(n51), .op(ma_req)
         );
  dp_1 wb_req_s1_reg ( .ip(N46), .ck(phy_clk), .q(wb_req_s1) );
  dp_1 \state_reg[0]  ( .ip(n124), .ck(phy_clk), .q(state[0]) );
  dp_1 \state_reg[3]  ( .ip(n121), .ck(phy_clk), .q(state[3]) );
  dp_1 \state_reg[2]  ( .ip(n120), .ck(phy_clk), .q(state[2]) );
  dp_1 \state_reg[1]  ( .ip(n119), .ck(phy_clk), .q(state[1]) );
  dp_1 \state_reg[4]  ( .ip(n122), .ck(phy_clk), .q(state[4]) );
  dp_1 \state_reg[5]  ( .ip(n123), .ck(phy_clk), .q(state[5]) );
  dp_1 wb_ack_o_reg ( .ip(N47), .ck(wb_clk), .q(wb_ack_o) );
  dp_1 wb_ack_s1_reg ( .ip(wb_ack_d), .ck(wb_clk), .q(wb_ack_s1) );
  dp_1 \wb_data_o_reg[31]  ( .ip(N45), .ck(wb_clk), .q(wb_data_o[31]) );
  dp_1 \wb_data_o_reg[30]  ( .ip(N44), .ck(wb_clk), .q(wb_data_o[30]) );
  dp_1 \wb_data_o_reg[29]  ( .ip(N43), .ck(wb_clk), .q(wb_data_o[29]) );
  dp_1 \wb_data_o_reg[28]  ( .ip(N42), .ck(wb_clk), .q(wb_data_o[28]) );
  dp_1 \wb_data_o_reg[27]  ( .ip(N41), .ck(wb_clk), .q(wb_data_o[27]) );
  dp_1 \wb_data_o_reg[26]  ( .ip(N40), .ck(wb_clk), .q(wb_data_o[26]) );
  dp_1 \wb_data_o_reg[25]  ( .ip(N39), .ck(wb_clk), .q(wb_data_o[25]) );
  dp_1 \wb_data_o_reg[24]  ( .ip(N38), .ck(wb_clk), .q(wb_data_o[24]) );
  dp_1 \wb_data_o_reg[23]  ( .ip(N37), .ck(wb_clk), .q(wb_data_o[23]) );
  dp_1 \wb_data_o_reg[22]  ( .ip(N36), .ck(wb_clk), .q(wb_data_o[22]) );
  dp_1 \wb_data_o_reg[21]  ( .ip(N35), .ck(wb_clk), .q(wb_data_o[21]) );
  dp_1 \wb_data_o_reg[20]  ( .ip(N34), .ck(wb_clk), .q(wb_data_o[20]) );
  dp_1 \wb_data_o_reg[19]  ( .ip(N33), .ck(wb_clk), .q(wb_data_o[19]) );
  dp_1 \wb_data_o_reg[18]  ( .ip(N32), .ck(wb_clk), .q(wb_data_o[18]) );
  dp_1 \wb_data_o_reg[17]  ( .ip(N31), .ck(wb_clk), .q(wb_data_o[17]) );
  dp_1 \wb_data_o_reg[16]  ( .ip(N30), .ck(wb_clk), .q(wb_data_o[16]) );
  dp_1 \wb_data_o_reg[15]  ( .ip(N29), .ck(wb_clk), .q(wb_data_o[15]) );
  dp_1 \wb_data_o_reg[14]  ( .ip(N28), .ck(wb_clk), .q(wb_data_o[14]) );
  dp_1 \wb_data_o_reg[13]  ( .ip(N27), .ck(wb_clk), .q(wb_data_o[13]) );
  dp_1 \wb_data_o_reg[12]  ( .ip(N26), .ck(wb_clk), .q(wb_data_o[12]) );
  dp_1 \wb_data_o_reg[11]  ( .ip(N25), .ck(wb_clk), .q(wb_data_o[11]) );
  dp_1 \wb_data_o_reg[10]  ( .ip(N24), .ck(wb_clk), .q(wb_data_o[10]) );
  dp_1 \wb_data_o_reg[9]  ( .ip(N23), .ck(wb_clk), .q(wb_data_o[9]) );
  dp_1 \wb_data_o_reg[8]  ( .ip(N22), .ck(wb_clk), .q(wb_data_o[8]) );
  dp_1 \wb_data_o_reg[7]  ( .ip(N21), .ck(wb_clk), .q(wb_data_o[7]) );
  dp_1 \wb_data_o_reg[6]  ( .ip(N20), .ck(wb_clk), .q(wb_data_o[6]) );
  dp_1 \wb_data_o_reg[5]  ( .ip(N19), .ck(wb_clk), .q(wb_data_o[5]) );
  dp_1 \wb_data_o_reg[4]  ( .ip(N18), .ck(wb_clk), .q(wb_data_o[4]) );
  dp_1 \wb_data_o_reg[3]  ( .ip(N17), .ck(wb_clk), .q(wb_data_o[3]) );
  dp_1 \wb_data_o_reg[2]  ( .ip(N16), .ck(wb_clk), .q(wb_data_o[2]) );
  dp_1 \wb_data_o_reg[1]  ( .ip(N15), .ck(wb_clk), .q(wb_data_o[1]) );
  dp_1 \wb_data_o_reg[0]  ( .ip(N14), .ck(wb_clk), .q(wb_data_o[0]) );
  dp_1 wb_ack_s1a_reg ( .ip(wb_ack_s1), .ck(wb_clk), .q(wb_ack_s1a) );
  dp_1 wb_ack_s2_reg ( .ip(wb_ack_s1a), .ck(wb_clk), .q(wb_ack_s2) );
  nor3_2 U3 ( .ip1(wb_ack_s2), .ip2(wb_ack_o), .ip3(n5), .op(N47) );
  nor3_2 U4 ( .ip1(state[4]), .ip2(state[5]), .ip3(state[3]), .op(n52) );
  nor3_2 U31 ( .ip1(state[1]), .ip2(state[2]), .ip3(state[0]), .op(n44) );
  inv_2 U32 ( .ip(wb_addr_i[17]), .op(n1) );
  nor2_2 U49 ( .ip1(state[4]), .ip2(state[5]), .op(n2) );
  nand3_2 U51 ( .ip1(state[3]), .ip2(n44), .ip3(n2), .op(n16) );
  inv_2 U53 ( .ip(n18), .op(n3) );
  nand2_2 U54 ( .ip1(ma_ack), .ip2(n3), .op(n4) );
  nand2_2 U55 ( .ip1(n16), .ip2(n4), .op(wb_ack_d) );
  inv_2 U56 ( .ip(wb_ack_s1), .op(n5) );
  mux2_1 U57 ( .ip1(rf_din[0]), .ip2(ma_din[0]), .s(wb_addr_i[17]), .op(N14)
         );
  mux2_1 U58 ( .ip1(rf_din[1]), .ip2(ma_din[1]), .s(wb_addr_i[17]), .op(N15)
         );
  mux2_1 U59 ( .ip1(rf_din[2]), .ip2(ma_din[2]), .s(wb_addr_i[17]), .op(N16)
         );
  mux2_1 U60 ( .ip1(rf_din[3]), .ip2(ma_din[3]), .s(wb_addr_i[17]), .op(N17)
         );
  mux2_1 U61 ( .ip1(rf_din[4]), .ip2(ma_din[4]), .s(wb_addr_i[17]), .op(N18)
         );
  mux2_1 U62 ( .ip1(rf_din[5]), .ip2(ma_din[5]), .s(wb_addr_i[17]), .op(N19)
         );
  mux2_1 U63 ( .ip1(rf_din[6]), .ip2(ma_din[6]), .s(wb_addr_i[17]), .op(N20)
         );
  mux2_1 U64 ( .ip1(rf_din[7]), .ip2(ma_din[7]), .s(wb_addr_i[17]), .op(N21)
         );
  mux2_1 U65 ( .ip1(rf_din[8]), .ip2(ma_din[8]), .s(wb_addr_i[17]), .op(N22)
         );
  mux2_1 U66 ( .ip1(rf_din[9]), .ip2(ma_din[9]), .s(wb_addr_i[17]), .op(N23)
         );
  mux2_1 U67 ( .ip1(rf_din[10]), .ip2(ma_din[10]), .s(wb_addr_i[17]), .op(N24)
         );
  mux2_1 U68 ( .ip1(rf_din[11]), .ip2(ma_din[11]), .s(wb_addr_i[17]), .op(N25)
         );
  mux2_1 U69 ( .ip1(rf_din[12]), .ip2(ma_din[12]), .s(wb_addr_i[17]), .op(N26)
         );
  mux2_1 U70 ( .ip1(rf_din[13]), .ip2(ma_din[13]), .s(wb_addr_i[17]), .op(N27)
         );
  mux2_1 U71 ( .ip1(rf_din[14]), .ip2(ma_din[14]), .s(wb_addr_i[17]), .op(N28)
         );
  mux2_1 U72 ( .ip1(rf_din[15]), .ip2(ma_din[15]), .s(wb_addr_i[17]), .op(N29)
         );
  mux2_1 U73 ( .ip1(rf_din[16]), .ip2(ma_din[16]), .s(wb_addr_i[17]), .op(N30)
         );
  mux2_1 U74 ( .ip1(rf_din[17]), .ip2(ma_din[17]), .s(wb_addr_i[17]), .op(N31)
         );
  mux2_1 U75 ( .ip1(rf_din[18]), .ip2(ma_din[18]), .s(wb_addr_i[17]), .op(N32)
         );
  mux2_1 U76 ( .ip1(rf_din[19]), .ip2(ma_din[19]), .s(wb_addr_i[17]), .op(N33)
         );
  mux2_1 U77 ( .ip1(rf_din[20]), .ip2(ma_din[20]), .s(wb_addr_i[17]), .op(N34)
         );
  mux2_1 U78 ( .ip1(rf_din[21]), .ip2(ma_din[21]), .s(wb_addr_i[17]), .op(N35)
         );
  mux2_1 U79 ( .ip1(rf_din[22]), .ip2(ma_din[22]), .s(wb_addr_i[17]), .op(N36)
         );
  mux2_1 U80 ( .ip1(rf_din[23]), .ip2(ma_din[23]), .s(wb_addr_i[17]), .op(N37)
         );
  mux2_1 U81 ( .ip1(rf_din[24]), .ip2(ma_din[24]), .s(wb_addr_i[17]), .op(N38)
         );
  mux2_1 U82 ( .ip1(rf_din[25]), .ip2(ma_din[25]), .s(wb_addr_i[17]), .op(N39)
         );
  mux2_1 U83 ( .ip1(rf_din[26]), .ip2(ma_din[26]), .s(wb_addr_i[17]), .op(N40)
         );
  mux2_1 U84 ( .ip1(rf_din[27]), .ip2(ma_din[27]), .s(wb_addr_i[17]), .op(N41)
         );
  mux2_1 U85 ( .ip1(rf_din[28]), .ip2(ma_din[28]), .s(wb_addr_i[17]), .op(N42)
         );
  mux2_1 U86 ( .ip1(rf_din[29]), .ip2(ma_din[29]), .s(wb_addr_i[17]), .op(N43)
         );
  mux2_1 U87 ( .ip1(rf_din[30]), .ip2(ma_din[30]), .s(wb_addr_i[17]), .op(N44)
         );
  mux2_1 U88 ( .ip1(rf_din[31]), .ip2(ma_din[31]), .s(wb_addr_i[17]), .op(N45)
         );
  inv_2 U89 ( .ip(ma_ack), .op(n8) );
  inv_2 U90 ( .ip(state[4]), .op(n7) );
  inv_2 U91 ( .ip(state[5]), .op(n6) );
  inv_2 U92 ( .ip(n22), .op(n9) );
  inv_2 U93 ( .ip(n41), .op(n10) );
  inv_2 U94 ( .ip(n49), .op(n11) );
  inv_2 U95 ( .ip(state[0]), .op(n13) );
  inv_2 U96 ( .ip(n32), .op(n14) );
  inv_2 U97 ( .ip(state[3]), .op(n15) );
  inv_2 U98 ( .ip(state[1]), .op(n17) );
  inv_2 U99 ( .ip(state[2]), .op(n55) );
  inv_2 U100 ( .ip(wb_we_i), .op(n56) );
endmodule


module usbf_top ( clk_i, rst_i, wb_addr_i, wb_data_i, wb_data_o, wb_ack_o, 
        wb_we_i, wb_stb_i, wb_cyc_i, inta_o, intb_o, dma_req_o, dma_ack_i, 
        susp_o, resume_req_i, phy_clk_pad_i, phy_rst_pad_o, DataOut_pad_o, 
        TxValid_pad_o, TxReady_pad_i, RxValid_pad_i, RxActive_pad_i, 
        RxError_pad_i, DataIn_pad_i, XcvSelect_pad_o, TermSel_pad_o, 
        SuspendM_pad_o, LineState_pad_i, OpMode_pad_o, usb_vbus_pad_i, 
        VControl_Load_pad_o, VControl_pad_o, VStatus_pad_i, sram_adr_o, 
        sram_data_i, sram_data_o, sram_re_o, sram_we_o );
  input [17:0] wb_addr_i;
  input [31:0] wb_data_i;
  output [31:0] wb_data_o;
  output [15:0] dma_req_o;
  input [15:0] dma_ack_i;
  output [7:0] DataOut_pad_o;
  input [7:0] DataIn_pad_i;
  input [1:0] LineState_pad_i;
  output [1:0] OpMode_pad_o;
  output [3:0] VControl_pad_o;
  input [7:0] VStatus_pad_i;
  output [14:0] sram_adr_o;
  input [31:0] sram_data_i;
  output [31:0] sram_data_o;
  input clk_i, rst_i, wb_we_i, wb_stb_i, wb_cyc_i, resume_req_i, phy_clk_pad_i,
         TxReady_pad_i, RxValid_pad_i, RxActive_pad_i, RxError_pad_i,
         usb_vbus_pad_i;
  output wb_ack_o, inta_o, intb_o, susp_o, phy_rst_pad_o, TxValid_pad_o,
         XcvSelect_pad_o, TermSel_pad_o, SuspendM_pad_o, VControl_Load_pad_o,
         sram_re_o, sram_we_o;
  wire   N0, phy_rst_pad_o, usb_suspend, suspend_clr_wr, suspend_clr,
         resume_req_r, rx_valid, rx_active, rx_err, tx_valid, tx_valid_last,
         tx_ready, tx_first, mode_hs, usb_reset, usb_attached, mwe, mreq, mack,
         dma_in_buf_sz1, dma_out_buf_avail, match, buf0_rl, buf0_set, buf1_set,
         uc_bsel_set, uc_dpd_set, int_buf1_set, int_buf0_set, int_upid_set,
         int_crc16_set, int_to_set, int_seqerr_set, out_to_small, pid_cs_err,
         nse_err, crc5_err, ma_we, ma_req, ma_ack, rf_re, rf_we, n4, n6, n7;
  wire   [1:0] LineState_r;
  wire   [7:0] VStatus_r;
  wire   [7:0] rx_data;
  wire   [7:0] tx_data;
  wire   [14:0] madr;
  wire   [31:0] mdout;
  wire   [31:0] mdin;
  wire   [6:0] funct_adr;
  wire   [31:0] idin;
  wire   [3:0] ep_sel;
  wire   [31:0] csr;
  wire   [31:0] buf0;
  wire   [31:0] buf1;
  wire   [31:0] frm_nat;
  wire   [17:0] ma_adr;
  wire   [31:0] ma2wb_d;
  wire   [31:0] wb2ma_d;
  wire   [31:0] wb2rf_d;
  wire   [31:0] rf2wb_d;
  wire   SYNOPSYS_UNCONNECTED__0, SYNOPSYS_UNCONNECTED__1, 
        SYNOPSYS_UNCONNECTED__2, SYNOPSYS_UNCONNECTED__3, 
        SYNOPSYS_UNCONNECTED__4, SYNOPSYS_UNCONNECTED__5, 
        SYNOPSYS_UNCONNECTED__6, SYNOPSYS_UNCONNECTED__7, 
        SYNOPSYS_UNCONNECTED__8, SYNOPSYS_UNCONNECTED__9, 
        SYNOPSYS_UNCONNECTED__10, SYNOPSYS_UNCONNECTED__11, 
        SYNOPSYS_UNCONNECTED__12, SYNOPSYS_UNCONNECTED__13, 
        SYNOPSYS_UNCONNECTED__14, SYNOPSYS_UNCONNECTED__15, 
        SYNOPSYS_UNCONNECTED__16, SYNOPSYS_UNCONNECTED__17, 
        SYNOPSYS_UNCONNECTED__18, SYNOPSYS_UNCONNECTED__19, 
        SYNOPSYS_UNCONNECTED__20;
  assign phy_rst_pad_o = rst_i;
  assign sram_re_o = 1'b1;
  assign dma_req_o[4] = 1'b0;
  assign dma_req_o[15] = 1'b0;
  assign dma_req_o[14] = 1'b0;
  assign dma_req_o[13] = 1'b0;
  assign dma_req_o[12] = 1'b0;
  assign dma_req_o[11] = 1'b0;
  assign dma_req_o[10] = 1'b0;
  assign dma_req_o[9] = 1'b0;
  assign dma_req_o[8] = 1'b0;
  assign dma_req_o[7] = 1'b0;
  assign dma_req_o[6] = 1'b0;
  assign dma_req_o[5] = 1'b0;
  assign OpMode_pad_o[0] = 1'b0;

  inv_2 U5 ( .ip(phy_rst_pad_o), .op(N0) );
  usbf_utmi_if u0 ( .phy_clk(phy_clk_pad_i), .rst(phy_rst_pad_o), .DataOut(
        DataOut_pad_o), .TxValid(TxValid_pad_o), .TxReady(TxReady_pad_i), 
        .RxValid(RxValid_pad_i), .RxActive(RxActive_pad_i), .RxError(
        RxError_pad_i), .DataIn(DataIn_pad_i), .XcvSelect(XcvSelect_pad_o), 
        .TermSel(TermSel_pad_o), .SuspendM(SuspendM_pad_o), .LineState(
        LineState_pad_i), .OpMode({OpMode_pad_o[1], SYNOPSYS_UNCONNECTED__0}), 
        .usb_vbus(usb_vbus_pad_i), .rx_data(rx_data), .rx_valid(rx_valid), 
        .rx_active(rx_active), .rx_err(rx_err), .tx_data(tx_data), .tx_valid(
        tx_valid), .tx_valid_last(tx_valid_last), .tx_ready(tx_ready), 
        .tx_first(tx_first), .mode_hs(mode_hs), .usb_reset(usb_reset), 
        .usb_suspend(usb_suspend), .usb_attached(usb_attached), .resume_req(
        resume_req_r), .suspend_clr(suspend_clr) );
  usbf_pl_SSRAM_HADR14 u1 ( .clk(phy_clk_pad_i), .rst(phy_rst_pad_o), 
        .rx_data(rx_data), .rx_valid(rx_valid), .rx_active(rx_active), 
        .rx_err(rx_err), .tx_data(tx_data), .tx_valid(tx_valid), 
        .tx_valid_last(tx_valid_last), .tx_ready(tx_ready), .tx_first(tx_first), .tx_valid_out(TxValid_pad_o), .mode_hs(mode_hs), .usb_reset(usb_reset), 
        .usb_suspend(usb_suspend), .usb_attached(usb_attached), .madr(madr), 
        .mdout(mdout), .mdin(mdin), .mwe(mwe), .mreq(mreq), .mack(mack), .fa(
        funct_adr), .idin(idin), .ep_sel(ep_sel), .match(match), 
        .dma_in_buf_sz1(dma_in_buf_sz1), .dma_out_buf_avail(dma_out_buf_avail), 
        .buf0_rl(buf0_rl), .buf0_set(buf0_set), .buf1_set(buf1_set), 
        .uc_bsel_set(uc_bsel_set), .uc_dpd_set(uc_dpd_set), .int_buf1_set(
        int_buf1_set), .int_buf0_set(int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(csr), .buf0(buf0), 
        .buf1(buf1), .frm_nat({frm_nat[31:28], SYNOPSYS_UNCONNECTED__1, 
        frm_nat[26:16], SYNOPSYS_UNCONNECTED__2, SYNOPSYS_UNCONNECTED__3, 
        SYNOPSYS_UNCONNECTED__4, SYNOPSYS_UNCONNECTED__5, frm_nat[11:0]}), 
        .pid_cs_err(pid_cs_err), .nse_err(nse_err), .crc5_err(crc5_err) );
  usbf_mem_arb_SSRAM_HADR14 u2 ( .phy_clk(phy_clk_pad_i), .wclk(clk_i), .rst(
        phy_rst_pad_o), .sram_adr(sram_adr_o), .sram_din(sram_data_i), 
        .sram_dout(sram_data_o), .sram_we(sram_we_o), .madr(madr), .mdout(mdin), .mdin(mdout), .mwe(mwe), .mreq(mreq), .mack(mack), .wadr(ma_adr[16:2]), 
        .wdout(ma2wb_d), .wdin(wb2ma_d), .wwe(ma_we), .wreq(ma_req), .wack(
        ma_ack) );
  usbf_rf u4 ( .clk(phy_clk_pad_i), .wclk(clk_i), .rst(phy_rst_pad_o), .adr(
        ma_adr[8:2]), .re(rf_re), .we(rf_we), .din(wb2rf_d), .dout(rf2wb_d), 
        .inta(inta_o), .intb(intb_o), .dma_req({SYNOPSYS_UNCONNECTED__6, 
        SYNOPSYS_UNCONNECTED__7, SYNOPSYS_UNCONNECTED__8, 
        SYNOPSYS_UNCONNECTED__9, SYNOPSYS_UNCONNECTED__10, 
        SYNOPSYS_UNCONNECTED__11, SYNOPSYS_UNCONNECTED__12, 
        SYNOPSYS_UNCONNECTED__13, SYNOPSYS_UNCONNECTED__14, 
        SYNOPSYS_UNCONNECTED__15, SYNOPSYS_UNCONNECTED__16, 
        SYNOPSYS_UNCONNECTED__17, dma_req_o[3:0]}), .dma_ack(dma_ack_i), 
        .idin(idin), .ep_sel(ep_sel), .match(match), .buf0_rl(buf0_rl), 
        .buf0_set(buf0_set), .buf1_set(buf1_set), .uc_bsel_set(uc_bsel_set), 
        .uc_dpd_set(uc_dpd_set), .int_buf1_set(int_buf1_set), .int_buf0_set(
        int_buf0_set), .int_upid_set(int_upid_set), .int_crc16_set(
        int_crc16_set), .int_to_set(int_to_set), .int_seqerr_set(
        int_seqerr_set), .out_to_small(out_to_small), .csr(csr), .buf0(buf0), 
        .buf1(buf1), .funct_adr(funct_adr), .dma_in_buf_sz1(dma_in_buf_sz1), 
        .dma_out_buf_avail(dma_out_buf_avail), .frm_nat({frm_nat[31:28], 1'b0, 
        frm_nat[26:16], 1'b0, 1'b0, 1'b0, 1'b0, frm_nat[11:0]}), 
        .utmi_vend_stat(VStatus_r), .utmi_vend_ctrl(VControl_pad_o), 
        .utmi_vend_wr(VControl_Load_pad_o), .line_stat(LineState_r), 
        .usb_attached(usb_attached), .mode_hs(mode_hs), .suspend(usb_suspend), 
        .attached(usb_attached), .usb_reset(usb_reset), .pid_cs_err(pid_cs_err), .nse_err(nse_err), .crc5_err(crc5_err), .rx_err(rx_err) );
  usbf_wb u5 ( .wb_clk(clk_i), .phy_clk(phy_clk_pad_i), .rst(phy_rst_pad_o), 
        .wb_addr_i(wb_addr_i), .wb_data_i(wb_data_i), .wb_data_o(wb_data_o), 
        .wb_ack_o(wb_ack_o), .wb_we_i(wb_we_i), .wb_stb_i(wb_stb_i), 
        .wb_cyc_i(wb_cyc_i), .ma_adr({SYNOPSYS_UNCONNECTED__18, ma_adr[16:2], 
        SYNOPSYS_UNCONNECTED__19, SYNOPSYS_UNCONNECTED__20}), .ma_dout(wb2ma_d), .ma_din(ma2wb_d), .ma_we(ma_we), .ma_req(ma_req), .ma_ack(ma_ack), .rf_re(
        rf_re), .rf_we(rf_we), .rf_din(rf2wb_d), .rf_dout(wb2rf_d) );
  dp_1 \LineState_r_reg[1]  ( .ip(LineState_pad_i[1]), .ck(phy_clk_pad_i), .q(
        LineState_r[1]) );
  dp_1 \LineState_r_reg[0]  ( .ip(LineState_pad_i[0]), .ck(phy_clk_pad_i), .q(
        LineState_r[0]) );
  dp_1 resume_req_r_reg ( .ip(n4), .ck(clk_i), .q(resume_req_r) );
  dp_1 suspend_clr_wr_reg ( .ip(suspend_clr), .ck(clk_i), .q(suspend_clr_wr)
         );
  dp_1 susp_o_reg ( .ip(usb_suspend), .ck(clk_i), .q(susp_o) );
  dp_1 \VStatus_r_reg[7]  ( .ip(VStatus_pad_i[7]), .ck(phy_clk_pad_i), .q(
        VStatus_r[7]) );
  dp_1 \VStatus_r_reg[6]  ( .ip(VStatus_pad_i[6]), .ck(phy_clk_pad_i), .q(
        VStatus_r[6]) );
  dp_1 \VStatus_r_reg[5]  ( .ip(VStatus_pad_i[5]), .ck(phy_clk_pad_i), .q(
        VStatus_r[5]) );
  dp_1 \VStatus_r_reg[4]  ( .ip(VStatus_pad_i[4]), .ck(phy_clk_pad_i), .q(
        VStatus_r[4]) );
  dp_1 \VStatus_r_reg[3]  ( .ip(VStatus_pad_i[3]), .ck(phy_clk_pad_i), .q(
        VStatus_r[3]) );
  dp_1 \VStatus_r_reg[2]  ( .ip(VStatus_pad_i[2]), .ck(phy_clk_pad_i), .q(
        VStatus_r[2]) );
  dp_1 \VStatus_r_reg[1]  ( .ip(VStatus_pad_i[1]), .ck(phy_clk_pad_i), .q(
        VStatus_r[1]) );
  dp_1 \VStatus_r_reg[0]  ( .ip(VStatus_pad_i[0]), .ck(phy_clk_pad_i), .q(
        VStatus_r[0]) );
  nor2_2 U7 ( .ip1(resume_req_r), .ip2(resume_req_i), .op(n7) );
  or2_2 U8 ( .ip1(N0), .ip2(suspend_clr_wr), .op(n6) );
  nor2_2 U9 ( .ip1(n7), .ip2(n6), .op(n4) );
endmodule


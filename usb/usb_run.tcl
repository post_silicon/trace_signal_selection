analyze -library WORK -format sverilog {/home/debjit/Work/Post_Silicon/usb/usb_original_unmodified_code.v}

elaborate usbf_top -architecture verilog -library WORK
check_design -multiple_designs
write_file -format ddc -hierarchy -output ./usb_netlist_gtech.ddc
write_file -hierarchy -format verilog -output /home/debjit/Work/Post_Silicon/usb/usb_netlist_gtech.v
create_clock -name "clk_i" -period 10 -waveform {0 2} {clk_i}
set_clock_uncertainty 0.1 clk_i
set_clock_latency 0.1 clk_i
set_clock_transition 0.1 clk_i
set_dont_touch_network clk_i
set_dont_touch rst_i
set_ideal_network rst_i
compile
write_file -format verilog -hierarchy -output ./usb_netlist_tech_grouped.v
remove_design -all
read_file -format ddc ./usb_netlist_gtech.ddc
set_ungroup usbf_top
set_flatten true -design usbf_top -effort high -minimize single_output -phase false
ungroup -all -flatten
compile
write_file -format verilog -hierarchy -output ./usb_netlist_tech_ungrouped.v
quit
